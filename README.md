# topic.ndla.no #

The topic.ndla.no repository is comprised of 4 different code bases:

1. services - which implements a wrapper around the main dependency of this project: the Ontopica.net (https://code.google.com/p/ontopia/downloads/list) Topic Map Engine API, and provides CRUD functions to interact with the Ontopia topic map database.
2. rest - a RESTFul Java WebService implemented with https://jersey.java.net/.  It implements functionality that interacts with the service layer, exposing the topic to resource relations as well as ontologies through GET as well as providing functionality for POST, PUT and DELETE.
3. web - is a GRAILS application implementing interfaces needed to create and maintain topics and ontologies

Both the "rest" and the "web" projects are dependent on the "service" project compiled and packaged as a JAR - file to be included as a library.

Further dependencies of are met by inlcuding these libraries:

* Jettison: (http://jettison.codehaus.org/)
* hamcrest-core: (https://code.google.com/p/hamcrest/)
* jsr-311: (https://jcp.org/en/jsr/detail?id=311)
* junit : (http://junit.org/)
* commons-lang (https://commons.apache.org/proper/commons-lang/)

To deploy the two projects, one needs to ensure they have the topicservice jar as a dependency and compile them into a war file, and deploy them into a java web container of choice