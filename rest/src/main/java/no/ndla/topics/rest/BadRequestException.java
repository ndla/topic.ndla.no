/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 15, 2012
 */

package no.ndla.topics.rest;

import javax.ws.rs.WebApplicationException;

public class BadRequestException extends WebApplicationException {

}
