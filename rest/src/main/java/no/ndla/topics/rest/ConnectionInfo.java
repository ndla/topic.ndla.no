/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 12, 2012
 */

package no.ndla.topics.rest;

import javax.naming.InitialContext;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

public class ConnectionInfo {

  // Members.
  private String _configFilePath;
  private String _topicmapReferenceKey;
  private String _solrServerUrl;
  private String _indexServerUrl;
  private String _indexServerUser;
  private String _indexServerPass;

  // Constructors.
  public ConnectionInfo() {
    try {
      InitialContext context = new InitialContext();
      this._configFilePath = (String)context.lookup("java:comp/env/config-file-path");
      this._topicmapReferenceKey = (String)context.lookup("java:comp/env/topicmap-reference-key");
      this._solrServerUrl = (String)context.lookup("java:comp/env/solr-server-url");
      this._indexServerUrl = (String)context.lookup("java:comp/env/index-server-url");
      this._indexServerUser = (String)context.lookup("java:comp/env/index-server-user");
      this._indexServerPass = (String)context.lookup("java:comp/env/index-server-pass");
    } catch (Exception e) {
      throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
    }
  }

  // Properties.
  public String getConfigFilePath() {
    return _configFilePath;
  }

  public String getTopicmapReferenceKey() {
    return _topicmapReferenceKey;
  }

  public String getIndexServerPass() {
    return _indexServerPass;
  }

  public String getSolrServerUrl() {
    return _solrServerUrl;
  }

  public String getIndexServerUrl() {
    return _indexServerUrl;
  }

  public String getIndexServerUser() {
    return _indexServerUser;
  }
	
}
