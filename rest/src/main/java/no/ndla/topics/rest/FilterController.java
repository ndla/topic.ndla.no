/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 13, 2012
 */

package no.ndla.topics.rest;

import javax.ws.rs.ext.ContextResolver;

import no.ndla.topics.service.TopicService;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class FilterController {
	private FilterDispatcher _filterDispatcher;
	
	public FilterController() {
		_filterDispatcher = new FilterDispatcher();
	}
	
	private boolean isAuthenticatedUser() {
		// TODO: Pending implementation.
		return true;
	}
	
	private void logRequest(NdlaRequest request) {
		// TODO: Pending implementation.
	}
	
	public JSONObject dispatchRequest(ContextResolver<TopicService> topicServiceResolver, NdlaRequest ndlaRequest) throws JSONException {
		JSONObject representation = null;
		logRequest(ndlaRequest);
		if (isAuthenticatedUser()) {
			representation = _filterDispatcher.dispatch(topicServiceResolver, ndlaRequest);
		}
		return representation;
	}
}
