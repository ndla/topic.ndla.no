/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 13, 2013
 */

package no.ndla.topics.rest;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.representation.AimTopicsBySubjectRepresentation;
import no.ndla.topics.rest.representation.AimTopicsRepresentation;
import no.ndla.topics.rest.representation.DomainRepresentation;
import no.ndla.topics.rest.representation.HierarchicalRelationshipsRepresentation;
import no.ndla.topics.rest.representation.HorizontalRelationshipsRepresentation;
import no.ndla.topics.rest.representation.KeywordsByNodeIdRepresentation;
import no.ndla.topics.rest.representation.KeywordsBySearchTermRepresentation;
import no.ndla.topics.rest.representation.KeywordsBySubjectMatterRepresentation;
import no.ndla.topics.rest.representation.KeywordsRepresentation;
import no.ndla.topics.rest.representation.LanguagesRepresentation;
import no.ndla.topics.rest.representation.NodesByKeywordIdRepresentation;
import no.ndla.topics.rest.representation.NodesByTopicIdRepresentation;
import no.ndla.topics.rest.representation.NodesRepresentation;
import no.ndla.topics.rest.representation.OntologyTopicRepresentation;
import no.ndla.topics.rest.representation.PersonRepresentation;
import no.ndla.topics.rest.representation.ReifiedTopicsByNodeIdRepresentation;
import no.ndla.topics.rest.representation.SubjectOntologyRepresentation;
import no.ndla.topics.rest.representation.SubjectOntologyTopicByTopicIdRepresentation;
import no.ndla.topics.rest.representation.SubjectsRepresentation;
import no.ndla.topics.rest.representation.TopicRelationsByDomainOntologyRepresentation;
import no.ndla.topics.rest.representation.TopicsByAimIdRepresentation;
import no.ndla.topics.rest.representation.TopicsByDomainOntologyRepresentation;
import no.ndla.topics.rest.representation.TopicsByNodeIdRepresentation;
import no.ndla.topics.rest.representation.SiteTopicsByNodeIdRepresentation;
import no.ndla.topics.rest.representation.TopicsByPersonOntologyRepresentation;
import no.ndla.topics.rest.representation.TopicsBySearchTermRepresentation;
import no.ndla.topics.rest.representation.TopicsBySubjectMatterRepresentation;
import no.ndla.topics.rest.representation.SiteTopicsRepresentation;
import no.ndla.topics.rest.representation.TopicsRepresentation;
import no.ndla.topics.rest.representation.WordClassesRepresentation;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class FilterDispatcher {
	
	// Tokens.
	private static final String NODE_TOKEN = "node";
	private static final String SUBJECT_TOKEN = "subject";
	private static final String TERM_TOKEN = "term";
	private static final String TOPIC_TOKEN = "topic";
	private static final String REIFIER_TOKEN = "reifiers";
	private static final String SITETOPIC_TOKEN = "sitetopic";
	private static final String KEYWORD_TOKEN = "keyword";
	private static final String WORDCLASS_TOKEN = "wordclass";
	private static final String RELATIONTYPE_TOKEN = "relationtype";
	private static final String AIM_TOKEN = "aim";
	private static final String LANGUAGE_TOKEN = "language";
	private static final String SITE_TOKEN = "site";	
	private static final String OFFSET_TOKEN = "offset";
	private static final String LIMIT_TOKEN = "limit";
	private static final String DOMAIN_TOKEN = "domain";
	private static final String DOMAINTYPE_TOKEN = "domainType";
	private static final String PERSON_TOKEN = "person";
	
	// Defaults.
	private static final int DEFAULT_OFFSET = 0;
	private static final int DEFAULT_LIMIT = 25;
	private static final String DEFAULT_SITE = "ndla";
	private static final String DEFAULT_WORDCLASS = "neutral";
	
	private static final int FILTER_VALUE_IDX = 0;
	private static final int FILTER_OPERATOR_IDX = 1;
	
	private static final String FILTER_DEFAULT_OPERATOR = "=";
	
	// Topic representations.
	private SiteTopicsByNodeIdRepresentation _siteTopicsByNodeIdRepresentation;
	private TopicsByNodeIdRepresentation _topicsByNodeIdRepresentation;
	private ReifiedTopicsByNodeIdRepresentation _reifiedTopicsByNodeIdRepresentation;
	private TopicsBySubjectMatterRepresentation _topicsBySubjectMatterRepresentation;
	private TopicsByAimIdRepresentation _topicsByAimIdRepresentation;
	private TopicsBySearchTermRepresentation _topicsBySearchTermRepresentation;
	private SiteTopicsRepresentation _siteTopicsRepresentation;
	private TopicsRepresentation _topicsRepresentation;
	private AimTopicsBySubjectRepresentation _aimTopicsBySubjectRepresentation;
	private AimTopicsRepresentation _aimTopicsRepresentation;
	private SubjectOntologyRepresentation _subjectOntologyRepresentation;
	private SubjectOntologyTopicByTopicIdRepresentation _subjectOntologyTopicByTopicIdRepresentation;
	private OntologyTopicRepresentation _ontologyTopicRepresentation;
	private TopicsByDomainOntologyRepresentation _topicsByDomainOntologyRepresentation;
	private TopicsByPersonOntologyRepresentation _topicsByPersonOntologyRepresentation;
	private TopicRelationsByDomainOntologyRepresentation _topicRelationsByDomainOntologyRepresentation;
	private DomainRepresentation _domainRepresentation;
	private PersonRepresentation _personRepresentation;
	
	// Keyword representations.
	private KeywordsByNodeIdRepresentation _keywordsByNodeIdRepresentation;
	private KeywordsBySubjectMatterRepresentation _keywordsBySubjectMatterRepresentation;
	private KeywordsBySearchTermRepresentation _keywordsBySearchTermRepresentation;
	private KeywordsRepresentation _keywordsRepresentation;
	
	// Node representations.
	private NodesByTopicIdRepresentation _nodesByTopicIdRepresentation;
	private NodesByKeywordIdRepresentation _nodesByKeywordIdRepresentation;
	private NodesRepresentation _nodesRepresentation;
	
	// Subject representations. 
	private SubjectsRepresentation _subjectsRepresentation;
	
	private HierarchicalRelationshipsRepresentation _hierarchicalRelationshipsRepresentation;
	private HorizontalRelationshipsRepresentation _horizontalRelationshipsRepresentation;
	private WordClassesRepresentation _wordClassesRepresentation;
	private LanguagesRepresentation _languagesRepresentation;
	
	// Constructor.
	public FilterDispatcher() {
		
	}
	
	/*
	 * Methods to dispatch on:
	 * 		Topics (/topics)
	 * 			getTopics							(token: N/A)
	 *    		getTopicsByNodeId 					(token: node)
	 *    		getTopicsBySubjectMatter 			(token: subject)
	 *    		getTopicsByAimId					(token: aim)
	 *    		lookupTopicsBySearchTerm			(token: term)
	 *    	Keywords (/keywords)
	 *    		getKeywords							(token: N/A)
	 *    		getKeywordsByNodeId 				(token: node)
	 *    		getKeywordsBySubjectMatter 			(token: subject)
	 *    		lookupKeywordsBySearchTerm			(token: term)
	 *    	Nodes (/nodes)
	 *    		getNodes							(token: N/A)
	 *    		getNodesByTopicId					(token: topic)
	 *    		getNodesByKeywordId					(token: keyword)
	 *    	Subjects (/subjects)
	 *    		getSubjects							(token: N/A)
	 *    		getSubjectsByTopicId				(token: topic)
	 *    		getSubjectsByKeywordId				(token: keyword)
	 *    		getSubjectBySubjectId				(token: subject)
	 *    	MetadataSets (/metadatasets)    
	 *    		getMetadataSets						(token: N/A)
	 *    		getMetadataSetsByNodeId				(token: node)
	 *    		lookupMetadataSetBySearchTerm		(token: term)
	 *    	MetadataTerms (/metadataterms)
	 *    		getMetadataTerms					(token: N/A)
	 *    		getMetadataTermsByNodeId			(token: node)
	 *    		getMetadataTermsBySubjectMatter		(token: subject)
	 *    		lookupMetadataTermBySearchTerm		(token: term)
	 */	
		
	public JSONObject dispatch(ContextResolver<TopicService> topicServiceResolver, NdlaRequest ndlaRequest) throws WebApplicationException {
		
		/*
		 * Dispatch based on request.resourceType (TOPIC, KEYWORD, NODE, SUBJECT, AIM, METADATASET, METADATATERM) 
		 * and request.filters.
		 */	
		JSONObject representation = null;
		Map<String, ArrayList<String>> filters = ndlaRequest.getFilters();
		
		String siteId = filters.containsKey(SITE_TOKEN) ? filters.get(SITE_TOKEN).get(FILTER_VALUE_IDX) : DEFAULT_SITE;
		String wordclassId = filters.containsKey(WORDCLASS_TOKEN) ? filters.get(WORDCLASS_TOKEN).get(FILTER_VALUE_IDX) : DEFAULT_WORDCLASS;
		int limit = filters.containsKey(LIMIT_TOKEN) ? Integer.parseInt(filters.get(LIMIT_TOKEN).get(FILTER_VALUE_IDX)) : DEFAULT_LIMIT;
		int offset = filters.containsKey(OFFSET_TOKEN) ? Integer.parseInt(filters.get(OFFSET_TOKEN).get(FILTER_VALUE_IDX)) : DEFAULT_OFFSET;
		
		switch (ndlaRequest.getResourceType()) {
		case SITETOPIC:
			if (filters.containsKey(NODE_TOKEN)) {
				String nodeId = filters.get(NODE_TOKEN).get(FILTER_VALUE_IDX);
				_siteTopicsByNodeIdRepresentation = new SiteTopicsByNodeIdRepresentation(topicServiceResolver, limit, offset, nodeId, siteId);
				representation = _siteTopicsByNodeIdRepresentation.represent(ndlaRequest);
			} else if (filters.containsKey(SUBJECT_TOKEN)) {
				String subjectId = filters.get(SUBJECT_TOKEN).get(FILTER_VALUE_IDX);
				_topicsBySubjectMatterRepresentation = new TopicsBySubjectMatterRepresentation(topicServiceResolver, limit, offset, subjectId, siteId);
				representation = _topicsBySubjectMatterRepresentation.represent(ndlaRequest);
			} else if (filters.containsKey(AIM_TOKEN)) {
				String aimId = filters.get(AIM_TOKEN).get(FILTER_VALUE_IDX);
				_topicsByAimIdRepresentation = new TopicsByAimIdRepresentation(topicServiceResolver, limit, offset, aimId, siteId);
				representation = _topicsByAimIdRepresentation.represent(ndlaRequest);
			}
			/*
			else if (filters.containsKey(TERM_TOKEN)) {
				String term = filters.get(TERM_TOKEN).get(FILTER_VALUE_IDX);
				_topicsBySearchTermRepresentation = new TopicsBySearchTermRepresentation(topicServiceResolver); // TODO: Pass in appropriate parameters.
				representation = _topicsBySearchTermRepresentation.represent(ndlaRequest);
			}
			*/
			else if (filters.isEmpty() || (filters.containsKey(LIMIT_TOKEN) && filters.containsKey(OFFSET_TOKEN))) {
				// If the filter doesn't contain a node token, a subject token, or a term token but it does contain a limit and offset token or it is completely empty, then 
				// return an unfiltered (but paginated) representation of a topics collection.
				_siteTopicsRepresentation = new SiteTopicsRepresentation(topicServiceResolver, limit, offset, siteId); 
				representation = _siteTopicsRepresentation.represent(ndlaRequest);
			} else {
				// Return HTTP status code 400 (Bad Request): The request could not be understood by the server due 
				// to malformed syntax. The client SHOULD NOT repeat the request without modifications.
				throw new WebApplicationException(Status.BAD_REQUEST); // TODO: Look into mapping generic exceptions to responses (see URL: http://jersey.java.net/nonav/documentation/latest/user-guide.html#d4e435).
			}
			break;
		case TOPIC:
			if (filters.containsKey(NODE_TOKEN) && !filters.containsKey(REIFIER_TOKEN)) {
				String nodeId = filters.get(NODE_TOKEN).get(FILTER_VALUE_IDX);
				_topicsByNodeIdRepresentation = new TopicsByNodeIdRepresentation(topicServiceResolver, limit, offset, nodeId, siteId);
				representation = _topicsByNodeIdRepresentation.represent(ndlaRequest);
			}
			else if (filters.containsKey(NODE_TOKEN) && filters.containsKey(REIFIER_TOKEN)) {
				String nid = filters.get(NODE_TOKEN).get(FILTER_VALUE_IDX);
				String nodeId  = "http://ndla.no/node/"+nid.substring(nid.lastIndexOf("_")+1);
				_reifiedTopicsByNodeIdRepresentation = new ReifiedTopicsByNodeIdRepresentation(topicServiceResolver, nodeId, siteId);
				representation = _reifiedTopicsByNodeIdRepresentation.represent(ndlaRequest);
			}
			else if (filters.containsKey(SUBJECT_TOKEN)) {
				String subjectId = filters.get(SUBJECT_TOKEN).get(FILTER_VALUE_IDX);
				_topicsBySubjectMatterRepresentation = new TopicsBySubjectMatterRepresentation(topicServiceResolver, limit, offset, subjectId, siteId);
				representation = _topicsBySubjectMatterRepresentation.represent(ndlaRequest);
			} else if (filters.containsKey(AIM_TOKEN)) {
				String aimId = filters.get(AIM_TOKEN).get(FILTER_VALUE_IDX);
				_topicsByAimIdRepresentation = new TopicsByAimIdRepresentation(topicServiceResolver, limit, offset, aimId, siteId);
				representation = _topicsByAimIdRepresentation.represent(ndlaRequest);
			}
			/*
			else if (filters.containsKey(TERM_TOKEN)) {
				String term = filters.get(TERM_TOKEN).get(FILTER_VALUE_IDX);
				_topicsBySearchTermRepresentation = new TopicsBySearchTermRepresentation(topicServiceResolver); // TODO: Pass in appropriate parameters.
				representation = _topicsBySearchTermRepresentation.represent(ndlaRequest);
			}
			*/
			else if (filters.isEmpty() || (filters.containsKey(LIMIT_TOKEN) && filters.containsKey(OFFSET_TOKEN))) {
				// If the filter doesn't contain a node token, a subject token, or a term token but it does contain a limit and offset token or it is completely empty, then 
				// return an unfiltered (but paginated) representation of a topics collection.
				_topicsRepresentation = new TopicsRepresentation(topicServiceResolver, limit, offset); 
				representation = _topicsRepresentation.represent(ndlaRequest);
			} else {
				// Return HTTP status code 400 (Bad Request): The request could not be understood by the server due 
				// to malformed syntax. The client SHOULD NOT repeat the request without modifications.
				throw new WebApplicationException(Status.BAD_REQUEST); // TODO: Look into mapping generic exceptions to responses (see URL: http://jersey.java.net/nonav/documentation/latest/user-guide.html#d4e435).
			}
			break;
		case KEYWORD:
			if (filters.containsKey(NODE_TOKEN)) {
				String nodeId = filters.get(NODE_TOKEN).get(FILTER_VALUE_IDX);
				_keywordsByNodeIdRepresentation = new KeywordsByNodeIdRepresentation(topicServiceResolver, limit, offset, nodeId, siteId);
				representation = _keywordsByNodeIdRepresentation.represent(ndlaRequest);
			} else if (filters.containsKey(SUBJECT_TOKEN)) {
				String subjectId = filters.get(SUBJECT_TOKEN).get(FILTER_VALUE_IDX);
				_keywordsBySubjectMatterRepresentation = new KeywordsBySubjectMatterRepresentation(topicServiceResolver, limit, offset, subjectId, siteId);
				representation = _keywordsBySubjectMatterRepresentation.represent(ndlaRequest);
			} else if (filters.containsKey(TERM_TOKEN)) {
				String term = filters.get(TERM_TOKEN).get(FILTER_VALUE_IDX);
				// TODO: Implementation.
			} else if (filters.isEmpty() || (filters.containsKey(LIMIT_TOKEN) && filters.containsKey(OFFSET_TOKEN))) {
				// If the filter doesn't contain a node token, a subject token, or a term token but it does contain a limit and offset token, then 
				// return an unfiltered (but paginated) representation of a keywords collection.
				_keywordsRepresentation = new KeywordsRepresentation(topicServiceResolver, limit, offset, siteId); 
				representation = _keywordsRepresentation.represent(ndlaRequest);
			} else {
				// Return HTTP status code 400 (Bad Request): The request could not be understood by the server due 
				// to malformed syntax. The client SHOULD NOT repeat the request without modifications.
				throw new WebApplicationException(Status.BAD_REQUEST); // TODO: Look into mapping generic exceptions to responses (see URL: http://jersey.java.net/nonav/documentation/latest/user-guide.html#d4e435).
			}
			break;
		case NODE:
			if (filters.containsKey(TOPIC_TOKEN)) {
				String topicId = filters.get(TOPIC_TOKEN).get(FILTER_VALUE_IDX);
				_nodesByTopicIdRepresentation = new NodesByTopicIdRepresentation(topicServiceResolver, limit, offset, topicId, siteId);
				representation = _nodesByTopicIdRepresentation.represent(ndlaRequest);
			} else if (filters.containsKey(KEYWORD_TOKEN)) {
				String keywordId = filters.get(KEYWORD_TOKEN).get(FILTER_VALUE_IDX);
				_nodesByKeywordIdRepresentation = new NodesByKeywordIdRepresentation(topicServiceResolver, limit, offset, keywordId, siteId);
				representation = _nodesByKeywordIdRepresentation.represent(ndlaRequest);
			} else if (filters.isEmpty() || (filters.containsKey(LIMIT_TOKEN) && filters.containsKey(OFFSET_TOKEN))) {
				// If the filter doesn't contain a topic token or a keyword token but it does contain a limit and offset token, then 
				// return an unfiltered (but paginated) representation of a nodes collection.
				_nodesRepresentation = new NodesRepresentation(topicServiceResolver, limit, offset, siteId);
				representation = _nodesRepresentation.represent(ndlaRequest);
			} else {
				// Return HTTP status code 400 (Bad Request): The request could not be understood by the server due 
				// to malformed syntax. The client SHOULD NOT repeat the request without modifications.
				throw new WebApplicationException(Status.BAD_REQUEST);
			}
			break;
		case SUBJECT:
			_subjectsRepresentation = new SubjectsRepresentation(topicServiceResolver, limit, offset, siteId);
			representation = _subjectsRepresentation.represent(ndlaRequest);
			/*
			if (filters.containsKey(TOPIC_TOKEN)) {
				// TODO: Implementation.
			} else if (filters.containsKey(KEYWORD_TOKEN)) {
				// TODO: Implementation.
			} else if (filters.containsKey(SUBJECT_TOKEN)) {
				// TODO: Implementation.
			} else if (filters.isEmpty() || (filters.containsKey(LIMIT_TOKEN) && filters.containsKey(OFFSET_TOKEN))) {
				_subjectsRepresentation = new SubjectsRepresentation(topicServiceResolver, limit, offset, siteId);
				representation = _subjectsRepresentation.represent(ndlaRequest);
			} else {
				// Return HTTP status code 400 (Bad Request): The request could not be understood by the server due 
				// to malformed syntax. The client SHOULD NOT repeat the request without modifications.
				throw new WebApplicationException(Status.BAD_REQUEST);
			}
			*/
			break;
		case METADATASET:
			// TODO: Pending implementation.
			break;
		case METADATATERM:
			// TODO: Pending implementation.
			break;
		case HIERARCHICALRELATIONTYPE:
			_hierarchicalRelationshipsRepresentation = new HierarchicalRelationshipsRepresentation(topicServiceResolver);
			representation = _hierarchicalRelationshipsRepresentation.represent(ndlaRequest);
			break;
		case HORIZONTALRELATIONTYPE:
			_horizontalRelationshipsRepresentation = new HorizontalRelationshipsRepresentation(topicServiceResolver);
			representation = _horizontalRelationshipsRepresentation.represent(ndlaRequest);
			break;
		case WORDCLASS:
			_wordClassesRepresentation = new WordClassesRepresentation(topicServiceResolver);
			representation = _wordClassesRepresentation.represent(ndlaRequest);
			break;
		case LANGUAGE:
			_languagesRepresentation = new LanguagesRepresentation(topicServiceResolver);
			representation = _languagesRepresentation.represent(ndlaRequest);
			break;
		case TOPICBYAIM:
			if (filters.containsKey(SUBJECT_TOKEN)) {
				String subjectId = filters.get(SUBJECT_TOKEN).get(FILTER_VALUE_IDX);
				_aimTopicsBySubjectRepresentation = new AimTopicsBySubjectRepresentation(topicServiceResolver, subjectId, siteId);
				representation = _aimTopicsBySubjectRepresentation.represent(ndlaRequest);	
			}
			else if(filters.containsKey(AIM_TOKEN) && filters.containsKey(LANGUAGE_TOKEN)){
				String aimId = filters.get(AIM_TOKEN).get(FILTER_VALUE_IDX);
				String langId = filters.get(LANGUAGE_TOKEN).get(FILTER_VALUE_IDX);
				_aimTopicsRepresentation = new AimTopicsRepresentation(topicServiceResolver,aimId, langId);
				representation = _aimTopicsRepresentation.represent(ndlaRequest);
			}
			
			break;
		case SUBJECTONTOLOGY:
			if (filters.containsKey(TOPIC_TOKEN) && filters.containsKey(SUBJECT_TOKEN)) {
				String topicId = filters.get(TOPIC_TOKEN).get(FILTER_VALUE_IDX);
				String subjectOntologyId = filters.get(SUBJECT_TOKEN).get(FILTER_VALUE_IDX);
				_subjectOntologyTopicByTopicIdRepresentation = new SubjectOntologyTopicByTopicIdRepresentation(topicServiceResolver,topicId,subjectOntologyId,siteId);
				representation = _subjectOntologyTopicByTopicIdRepresentation.represent(ndlaRequest);
			}
			else if (filters.containsKey(TOPIC_TOKEN) && !filters.containsKey(SUBJECT_TOKEN)) {
				String topicId = filters.get(TOPIC_TOKEN).get(FILTER_VALUE_IDX);
				_ontologyTopicRepresentation = new OntologyTopicRepresentation(topicServiceResolver,topicId,siteId);
				representation = _ontologyTopicRepresentation.represent(ndlaRequest);
			}
			else{
				String subjectOntologyId = filters.get(SUBJECT_TOKEN).get(FILTER_VALUE_IDX);
				_subjectOntologyRepresentation = new SubjectOntologyRepresentation(topicServiceResolver, subjectOntologyId, siteId);
				representation = _subjectOntologyRepresentation.represent(ndlaRequest);	
			}
			
			break;
		case DOMAINONTOLOGY:
			if (filters.containsKey(DOMAIN_TOKEN)) {
				String domainId = filters.get(DOMAIN_TOKEN).get(FILTER_VALUE_IDX);
				_topicsByDomainOntologyRepresentation = new TopicsByDomainOntologyRepresentation(topicServiceResolver,domainId);
				representation = _topicsByDomainOntologyRepresentation.represent(ndlaRequest);
			}
			break;
		case DOMAINTOPICRELATIONS:
			if (filters.containsKey(DOMAIN_TOKEN) && filters.containsKey(DOMAINTYPE_TOKEN)) {
				String domainId = filters.get(DOMAIN_TOKEN).get(FILTER_VALUE_IDX);
				String domainType = filters.get(DOMAINTYPE_TOKEN).get(FILTER_VALUE_IDX);
				_topicRelationsByDomainOntologyRepresentation = new TopicRelationsByDomainOntologyRepresentation(topicServiceResolver,domainId,domainType);
				representation = _topicRelationsByDomainOntologyRepresentation.represent(ndlaRequest);
			}
			break;
		case PERSONONTOLOGY:
			if (filters.containsKey(PERSON_TOKEN)) {
				String personId = filters.get(PERSON_TOKEN).get(FILTER_VALUE_IDX);
				_topicsByPersonOntologyRepresentation = new TopicsByPersonOntologyRepresentation(topicServiceResolver,personId);
				representation = _topicsByPersonOntologyRepresentation.represent(ndlaRequest);
			}
			break;
		case DOMAIN:
			if(filters.containsKey(LIMIT_TOKEN) && filters.containsKey(OFFSET_TOKEN)){
				_domainRepresentation = new DomainRepresentation(topicServiceResolver,limit, offset);
				representation = _domainRepresentation.represent(ndlaRequest);	
			}
			break;
		case PERSON:
			if(filters.containsKey(LIMIT_TOKEN) && filters.containsKey(OFFSET_TOKEN)){
				_personRepresentation = new PersonRepresentation(topicServiceResolver,limit, offset);
				representation = _personRepresentation.represent(ndlaRequest);	
			}
			break;
		default:
			// Return HTTP status code 400 (Bad Request): The request could not be understood by the server due 
			// to malformed syntax. The client SHOULD NOT repeat the request without modifications.
			throw new WebApplicationException(Status.BAD_REQUEST);
		}
		return representation;
	}
}
