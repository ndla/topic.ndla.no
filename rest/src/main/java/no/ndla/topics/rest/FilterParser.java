/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 11, 2013
 */

package no.ndla.topics.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.UriInfo;

public class FilterParser {
	
	private static final String FILTER_PATTERN = "^filter\\[\\s*[-_a-zA-Z0-9]*\\s*(,\\s*<|,\\s*>|,\\s*:)?\\s*\\]";	
	
	private static final int FILTER_VALUE_IDX = 0;
	private static final int FILTER_OPERATOR_IDX = 1;
	private static final String FILTER_DEFAULT_OPERATOR = "=";
	
	private static Pattern _filterPattern;
		
	// Static methods.
	public static boolean isFiltered(UriInfo uriInfo) {
		boolean result = false;
		for (String key : uriInfo.getQueryParameters().keySet()) {
			if (null == _filterPattern) {
				_filterPattern = Pattern.compile(FILTER_PATTERN);
			}
			Matcher matcher = _filterPattern.matcher(key);	
			if (matcher.find()) {
				result = true;
				break;
			}
		}
		return result;
	}
	
	public static String normalizeFilterKey(String filterKey) {
		return filterKey.
				replaceAll("^filter\\[", "").
				replaceAll("\\]$", "");
	}
	
	public static boolean isFilter(String parameter) {
		boolean result = false;
		Matcher matcher = _filterPattern.matcher(parameter);
		if (matcher.find()) {
			result = true;
		}
		return result;
	}
	
	public static Map<String, ArrayList<String>> fromUri(UriInfo uriInfo) {
		Map<String, ArrayList<String>> filters = new HashMap<String, ArrayList<String>>();
		
		if (FilterParser.isFiltered(uriInfo)) {
			for (String key : uriInfo.getQueryParameters().keySet()) {
				if (isFilter(key)) {
					// Only the last value of multiple parameters with the same key is extracted.
					// That is, "OR" filters are not supported. 
					for (String value : uriInfo.getQueryParameters().get(key)) {
						ArrayList<String> filterItems = new ArrayList<String>(); // Each item is a combination of the parameter itself and the accompanying operator. 
						String[] filter = normalizeFilterKey(key).split(",");
						filterItems.add(value);
						if (filter.length == 1) { // No operator was provided; default to "=" operator.												
							filterItems.add(FILTER_DEFAULT_OPERATOR);
						} else {
							filterItems.add(filter[FILTER_OPERATOR_IDX]);
						}
						filters.put(normalizeFilterKey(filter[FILTER_VALUE_IDX]), filterItems);
					}
				}
			}			
		}
		
		return filters;
	}	
}