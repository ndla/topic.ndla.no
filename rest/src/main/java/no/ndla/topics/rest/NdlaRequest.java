/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 07, 2012
 */

package no.ndla.topics.rest;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.core.Request;
import javax.ws.rs.core.UriInfo;

public class NdlaRequest {
	private ResourceType _resourceType;
	private Map<String, ArrayList<String>> _filters;
	private UriInfo _uriInfo;
	private Request _request;

	// Constructor.
	public NdlaRequest(ResourceType resourceType,
			Map<String, ArrayList<String>> filters, UriInfo uriInfo, Request request) {
		this._resourceType = resourceType;
		this._filters = filters;
		this._uriInfo = uriInfo;
		this._request = request;
	}

	// Properties.
	public ResourceType getResourceType() {
		return _resourceType;
	}

	public Map<String, ArrayList<String>> getFilters() {
		return _filters;
	}
	
	public UriInfo getUriInfo() {
		return _uriInfo;
	}
	
	public Request getRequest() {
		return _request;
	}
}
