/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 08, 2012
 */

package no.ndla.topics.rest;

public class NdlaWebServiceException extends Exception {

	private static final long serialVersionUID = 1L; // For serialization purposes.

	public NdlaWebServiceException() {
        super();
    }

    public NdlaWebServiceException(String message) {
        super(message);
    }

    public NdlaWebServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public NdlaWebServiceException(Throwable cause) {
        super(cause);
    }
}