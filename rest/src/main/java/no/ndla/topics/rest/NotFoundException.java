/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 15, 2012
 */

package no.ndla.topics.rest;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import com.sun.jersey.api.Responses;

public class NotFoundException extends WebApplicationException {

	private static final long serialVersionUID = 1L;

	public NotFoundException() {
		super(Responses.notFound().build());
	}

	public NotFoundException(String message) {
		super(Response.status(Responses.NOT_FOUND).entity(message)
				.type("application/json").build());
	}
}
