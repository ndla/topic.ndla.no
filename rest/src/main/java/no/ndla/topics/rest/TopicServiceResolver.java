/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * May 31, 2013
 */

package no.ndla.topics.rest;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import no.ndla.topics.service.TopicService;

/*
 A provider is an application supplied class used to extend a JAX-RS run-time such as Jersey. A JAX-RS
 implementation is required to load only one instance of a provider class in a JAX-RS run-time instance.
 */

@Provider
public class TopicServiceResolver implements ContextResolver<TopicService> {
	private TopicService _topicService;

	public TopicServiceResolver() {
		super();
		ConnectionInfo connectionInfo = new ConnectionInfo();

		this._topicService = new TopicService(
				connectionInfo.getConfigFilePath(),
				connectionInfo.getTopicmapReferenceKey(),
				connectionInfo.getIndexServerUrl(),
				connectionInfo.getSolrServerUrl(),
				connectionInfo.getIndexServerUser(),
				connectionInfo.getIndexServerPass());
	}

	@Override
	public TopicService getContext(Class<?> type) {
		return _topicService;
	}
}
