/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 13, 2012
 */

package no.ndla.topics.rest.client;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class KeywordsResourceClient {

	public static void main(String[] args) {
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());

		// Get response.
		String payload = "{payload: \"test\"}";
		String response = service
				.path("rest/v1")
				.path("sites/4321/nodes/1234/keywords")
				.type(MediaType.APPLICATION_JSON)
				.entity(payload, MediaType.TEXT_PLAIN)
				.post(String.class);
		System.out.println(response);
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8080/no.ndla.topics")
				.build();
	}
}
