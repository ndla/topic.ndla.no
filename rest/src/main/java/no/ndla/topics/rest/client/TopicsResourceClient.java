/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 14, 2012
 */

package no.ndla.topics.rest.client;

import java.net.URI;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

public class TopicsResourceClient {

	private static final String TOPIC_IDENTIFIER = "ndla-topic-26368";

	public static void main(String[] args) {
		ClientConfig config = new DefaultClientConfig();
		Client client = Client.create(config);
		WebResource service = client.resource(getBaseURI());

		// Get JSON representation.
		System.out.println(service.path("rest/v1")
				.path("topics/" + TOPIC_IDENTIFIER)
				.accept(MediaType.APPLICATION_JSON).get(String.class));
	}

	private static URI getBaseURI() {
		return UriBuilder.fromUri("http://localhost:8080/no.ndla.topics")
				.build();
	}

}
