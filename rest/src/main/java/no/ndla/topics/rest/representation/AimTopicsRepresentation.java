/*
 * Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
 * June 10, 2013
 */

package no.ndla.topics.rest.representation;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.ConnectionInfo;

public class AimTopicsRepresentation extends CompoundRepresentation {

	public AimTopicsRepresentation(ContextResolver<TopicService> topicServiceResolver,
			String aimId, String language) throws WebApplicationException {		
		try {
			// Retrieve AimTopics.
			language = "http://psi.oasis-open.org/iso/639/#"+language;
			System.out.println("AIMID: "+aimId+" LANGUAGE: "+language);
			_entities = topicServiceResolver.getContext(TopicService.class).getAimTopicAssociations(aimId, language);
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
}
