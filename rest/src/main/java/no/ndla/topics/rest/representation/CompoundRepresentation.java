/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * April 05, 2012
 */

package no.ndla.topics.rest.representation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import no.ndla.topics.service.model.NdlaWord;
import no.ndla.topics.rest.ConnectionInfo;
import no.ndla.topics.rest.NdlaRequest;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class CompoundRepresentation implements Representable {
	
	protected JSONObject _entities;

	@Override
	public JSONObject represent(NdlaRequest ndlaRequest) throws WebApplicationException {
		JSONObject representation = new JSONObject();
		
		try {			
			
			JSONArray topics = new JSONArray();
			JSONObject topic = new JSONObject();
			topics.put(topic);
			representation.put(ndlaRequest.getResourceType().toString()
					.toLowerCase(), _entities);
		} catch (NullPointerException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return representation;
	}
}
