/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 14, 2013
 */

package no.ndla.topics.rest.representation;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.ConnectionInfo;
import no.ndla.topics.rest.resource.SitesResource;

public class KeywordsByNodeIdRepresentation extends Representation {
	public KeywordsByNodeIdRepresentation(ContextResolver<TopicService> topicServiceResolver, int limit, int offset, String nodeId,
			String siteId) throws WebApplicationException {
		try {
			// Retrieve keywords.
			_entities = topicServiceResolver.getContext(TopicService.class).getKeywordsByNodeId(limit, offset,
					nodeId, NdlaSite.valueOf(siteId.toUpperCase()));
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
}
