/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 14, 2012
 */

package no.ndla.topics.rest.representation;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.ConnectionInfo;

public class KeywordsBySearchTermRepresentation extends Representation {
	/*
	public KeywordsBySearchTermRepresentation(ContextResolver<TopicService> topicServiceResolver, String searchTerm,
			String siteId) throws WebApplicationException {
		try {
			// Retrieve keywords.
			_entities = topicServiceResolver.getContext(TopicService.class).lookUpKeywordsBySearchTerm(searchTerm, NdlaSite.valueOf(siteId.toUpperCase()));
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	*/
}