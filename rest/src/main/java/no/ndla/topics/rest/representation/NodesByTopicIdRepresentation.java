/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 14, 2012
 */

package no.ndla.topics.rest.representation;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.ConnectionInfo;

public class NodesByTopicIdRepresentation extends Representation {

	public NodesByTopicIdRepresentation(ContextResolver<TopicService> topicServiceResolver, int limit, int offset, String topicId,
			String siteId) throws WebApplicationException {
		try {
			// Retrieve nodes.
			_entities = topicServiceResolver.getContext(TopicService.class).getNodesByTopicId(limit, offset, topicId,
					NdlaSite.valueOf(siteId.toUpperCase()));
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
}
