/*
 * Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
 * June 10, 2013
 */

package no.ndla.topics.rest.representation;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.ConnectionInfo;

public class OntologyTopicRepresentation extends CompoundRepresentation {

	public OntologyTopicRepresentation(ContextResolver<TopicService> topicServiceResolver,
			String identifier, String siteId) throws WebApplicationException {		
		try {
			// Retrieve SubjectOntology.
			_entities = topicServiceResolver.getContext(TopicService.class).getOntologyTopicsByTopicId(identifier, NdlaSite.valueOf(siteId.toUpperCase()));
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
}
