/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * March 13, 2012
 */

package no.ndla.topics.rest.representation;

import no.ndla.topics.rest.NdlaRequest;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public interface Representable {

	JSONObject represent(NdlaRequest ndlaRequest) throws JSONException;
}