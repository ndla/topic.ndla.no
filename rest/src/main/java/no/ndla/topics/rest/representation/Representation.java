/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * April 05, 2012
 */

package no.ndla.topics.rest.representation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;

import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import no.ndla.topics.service.model.NdlaWord;
import no.ndla.topics.rest.ConnectionInfo;
import no.ndla.topics.rest.NdlaRequest;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class Representation implements Representable {
	/*
	 * protected ConnectionInfo _connectionInfo; protected TopicService
	 * _topicService;
	 */

	protected ArrayList<NdlaTopic> _entities;

	@Override
	public JSONObject represent(NdlaRequest ndlaRequest) throws WebApplicationException {
		JSONObject representation = new JSONObject();
		try {	
			String path = ndlaRequest.getUriInfo().getPath();
			// Build JSON representation.
			HashMap<String, String> descriptions;
			HashMap<String, String> ingresses;
			HashMap<String, String> licenses;
			HashMap<String, String> names;
			HashMap<String, HashMap<String,String>> types;
			ArrayList<NdlaWord> wordNames;
			ArrayList<String> authors;
			ArrayList<String> psis;
			HashMap<String, String> images;
			ArrayList<String> originatingSites;

			JSONArray topics = new JSONArray();
			for (NdlaTopic _entity : _entities) {
				psis = _entity.getPsis();
				String psiTemp = "";
				JSONObject topic = new JSONObject();
				String topicId = _entity.getIdentifier();
				psiTemp = _entity.getPsi();
				
				if(psiTemp.length() > 0){
					topic.put("psi", psiTemp);	
					if(topicId.length() == 0){
						topicId = psiTemp.substring(psiTemp.lastIndexOf("#")+1);
					}
				}
				else{
					if(psis.size() > 0){
						for(String psiString : psis) {
							if(psiString.contains(path)){
								topic.put("psi", psiString);
								if(topicId.length() == 0){
									topicId = psiString.substring(psiString.lastIndexOf("#")+1);
								}
							}
						}
					}
				}
				
				if(topicId.isEmpty()){
					
					if(path.equals("subjects")){
						if(psis.size() > 1){
							for(String psiString : psis) {
								String[] psiFragments = psiString.split("#");
								String[] idFragments = psiFragments[1].split("_");
								if(isPrimitive(idFragments[1])){
									topic.put("psi", psiString);
									if(topicId.length() == 0){
										topicId = psiFragments[1];
									}
								}
							}
						}
					}
				}
				
				topic.put("topicId", topicId);
				
				topic.put("visibility", _entity.getVisibility());
				topic.put("nodeType", _entity.getNodeType());
				
				if(null != _entity.getUdirPsi()){
					topic.put("udirPsi",_entity.getUdirPsi());
				}

				if(null != _entity.getApproved()){
					String approved = _entity.getApproved();
					topic.put("approved", approved);
					
					if(approved.equals("true")) {
						topic.put("approval_date", _entity.getApprovalDate());
					}
				}
				
				if(null != _entity.getProcessState()){
					String processState = _entity.getProcessState();
					topic.put("processState", processState);
				}
				
				descriptions = _entity.getDescriptions();
				ingresses = _entity.getIngresses();
				licenses = _entity.getLicenses();
				names = _entity.getNames();
				wordNames = _entity.getWordNames();
				authors = _entity.getAuthors();
				types = _entity.getTypes();
				
				images = _entity.getImages();
				originatingSites = _entity.getOriginatingSites();

				if (!descriptions.isEmpty()) {
					JSONObject jsonDescriptions = new JSONObject();
					for (Map.Entry<String, String> entry : descriptions
							.entrySet()) {
						jsonDescriptions.put(entry.getKey(),
								entry.getValue());
					}
					topic.put("descriptions", jsonDescriptions);
				}
				if (!ingresses.isEmpty()) {
					JSONObject jsonIngresses = new JSONObject();
					for (Map.Entry<String, String> entry : ingresses
							.entrySet()) {
						jsonIngresses.put(entry.getKey(), entry.getValue());
					}
					topic.put("ingresses", jsonIngresses);
				}
				if (!licenses.isEmpty()) {
					JSONObject jsonLicenses = new JSONObject();
					for (Map.Entry<String, String> entry : licenses
							.entrySet()) {
						jsonLicenses.put(entry.getKey(), entry.getValue());
					}
					topic.put("licenses", jsonLicenses);
				}
				if (!authors.isEmpty()) {
					JSONArray jsonAuthors = new JSONArray();
					for (String author : authors) {
						jsonAuthors.put(author);
					}
					topic.put("authors", jsonAuthors);
				}
		
				if(null!= psis && psis.size() > 0){
					JSONArray jsonPsis = new JSONArray();
					for(String psi : psis) {
						jsonPsis.put(psi);
					}
					topic.put("psis", jsonPsis);
				}
				
				if(null != images && images.size() > 0) {
					JSONObject jsonImages = new JSONObject();
					for (Map.Entry<String, String> entry : images.entrySet()) {
						jsonImages.put(entry.getKey(), entry.getValue());
					}
					topic.put("images", jsonImages);
				}
				else{
					topic.put("image", _entity.getImage());
				}
				
				if(null != originatingSites && originatingSites.size() > 0) {
					JSONArray jsonSites = new JSONArray();
					for(String site : originatingSites) {
						jsonSites.put(site);
					}
					topic.put("originatingSites", jsonSites);
				}
				else{
					topic.put("originatingSite", _entity.getOriginatingSite());
				}
				
				if (null != names && !names.isEmpty()) {
					JSONObject jsonNames = new JSONObject();
					for (Map.Entry<String, String> entry : names.entrySet()) {
						jsonNames.put(entry.getKey(), entry.getValue());
					}
					topic.put("names", jsonNames);
				}
				
				if (types.size() > 0) {
					JSONArray jsonTypes = new JSONArray();
					Iterator it = types.keySet().iterator();
					
					while(it.hasNext()) {
						JSONObject typeObject = new JSONObject();
						String typeTopicId = (String)it.next();
						typeObject.put("typeId", typeTopicId);
						HashMap<String,String> typeNames = types.get(typeTopicId);
						Iterator nit = typeNames.keySet().iterator();
						JSONArray typeNameArray = new JSONArray();
						
						while(nit.hasNext()) {
							String language = (String) nit.next();
							String typeName = typeNames.get(language);
							JSONObject typeNameObject = new JSONObject();
							typeNameObject.put(language,typeName);
							typeNameArray.put(typeNameObject);
						}
						typeObject.put("names", typeNameArray);
						jsonTypes.put(typeObject);
					}
					topic.put("types", jsonTypes);
				}
				if (null != wordNames && !wordNames.isEmpty()) {
					JSONArray jsonNames = new JSONArray();
					ArrayList<String> wordclasses = new ArrayList<>();
					for (NdlaWord wordName : wordNames) {
						String wordClass = wordName.getWordClass();
						JSONObject currName = new JSONObject();

						if (!wordclasses.contains(wordClass)) {
							wordclasses.add(wordClass);
							currName.put("wordclass", wordClass);
							currName.put(
									"data",
									resolveNamesByWordClass(wordClass,
											wordNames));
						}
						if (currName.has("wordclass")) {
							jsonNames.put(currName);
						}
					}
					topic.put("names", jsonNames);
				}
				
				topics.put(topic);
			}
			representation.put(ndlaRequest.getResourceType().toString()
					.toLowerCase(), topics);
		} catch (NullPointerException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return representation;
	}

	/**
	 * Helper methods
	 */
	private JSONArray resolveNamesByWordClass(String wordclass,
			ArrayList<NdlaWord> names) {
		JSONArray result = new JSONArray();
		try {
			for (NdlaWord name : names) {
				String currWordClass = name.getWordClass();
				HashMap<String, String> currentNames = name.getNames();
				Iterator<String> it = currentNames.keySet().iterator();
				while (it.hasNext()) {
					String lang = (String) it.next();
					String nameString = (String) currentNames.get(lang);
					if (currWordClass.equals(wordclass)) {
						JSONObject nameObj = new JSONObject();
						nameObj.put(lang, nameString);
						result.put(nameObj);
					}
				}
			}
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	private boolean isPrimitive(String value){
        boolean status=true;
        if(value.length()<1)
            return false;
        for(int i = 0;i<value.length();i++){
            char c=value.charAt(i);
            if(Character.isDigit(c) || c=='.'){

            }else{
                status=false;
                break;
            }
        }
        return status;
    }
}
