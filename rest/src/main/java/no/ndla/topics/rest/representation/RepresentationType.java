/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * April 05, 2012
 */

package no.ndla.topics.rest.representation;

public enum RepresentationType {
	AIM,
	KEYWORD,
	NODE,
	SUBJECT,
	TOPIC,
	SITETOPIC,
	LANGUAGE,
	TOPICBYAIM,
	SUBJECTONTOLOGY
}
