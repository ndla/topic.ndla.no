/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * April 02, 2012
 */

package no.ndla.topics.rest.representation;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.ConnectionInfo;
import no.ndla.topics.rest.resource.SubjectsResource;

public class SubjectsRepresentation extends Representation {
	private Logger _logger = LoggerFactory.getLogger(SubjectsRepresentation.class);
	public SubjectsRepresentation(ContextResolver<TopicService> topicServiceResolver, int limit, int offset, String siteId) throws WebApplicationException {	
		try {
			// Retrieve subjects.
			_entities = topicServiceResolver.getContext(TopicService.class).getSubjects(limit, offset,
					NdlaSite.valueOf(siteId.toUpperCase()));
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

}
