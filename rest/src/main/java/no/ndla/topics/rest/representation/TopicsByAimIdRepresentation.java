/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * April 09, 2012
 */

package no.ndla.topics.rest.representation;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.ConnectionInfo;

public class TopicsByAimIdRepresentation extends Representation {
	
	public TopicsByAimIdRepresentation(ContextResolver<TopicService> topicServiceResolver, int limit, int offset, String aimId, String siteId) throws WebApplicationException {
		try {
			// Retrieve topics.
			_entities = topicServiceResolver.getContext(TopicService.class).getTopicsByAimId(limit, offset, aimId,
					NdlaSite.valueOf(siteId.toUpperCase()));
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
}
