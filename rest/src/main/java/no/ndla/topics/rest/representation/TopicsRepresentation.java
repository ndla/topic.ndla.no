/*
 * Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
 * June 4, 2014
 */
package no.ndla.topics.rest.representation;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.TopicService;

public class TopicsRepresentation extends Representation {
	public TopicsRepresentation(ContextResolver<TopicService> topicServiceResolver, int limit, int offset) throws WebApplicationException {
		try {		
			// Retrieve topics.
			_entities = topicServiceResolver.getContext(TopicService.class).getTopics(limit, offset);
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
}
