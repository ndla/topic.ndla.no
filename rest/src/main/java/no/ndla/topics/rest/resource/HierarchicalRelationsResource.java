/*
 * Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
 * June 10, 2013
 */

package no.ndla.topics.rest.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ContextResolver;

import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.FilterController;
import no.ndla.topics.rest.FilterParser;
import no.ndla.topics.rest.NdlaRequest;
import no.ndla.topics.rest.ResourceType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

// Map the resource to the URL 'keywords'.
@Path("/hierarchicalrelationtypes")
// Default media type (can be overridden at the method level).
@Produces(MediaType.APPLICATION_JSON)
public class HierarchicalRelationsResource {

	private NdlaRequest _ndlaRequest;

	public HierarchicalRelationsResource(@Context UriInfo uriInfo,
			@Context Request request) {
		this._ndlaRequest = new NdlaRequest(
				ResourceType.HIERARCHICALRELATIONTYPE,
				FilterParser.fromUri(uriInfo), uriInfo, request);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getHierarchicalAssociationTypes(
			final @Context ContextResolver<TopicService> topicServiceResolver) throws WebApplicationException {
		JSONObject representation = new JSONObject();
		FilterController filterController = new FilterController();
		try {
			representation = filterController.dispatchRequest(
					topicServiceResolver, _ndlaRequest);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return representation;
	}

	@Path("{hierarchicalrelationtype}")
	public HierarchicalRelationResource getHierarchicalAssociationType(
			@PathParam("hierarchicalrelationtype") String id,
			final @Context ContextResolver<TopicService> topicServiceResolver) {
		return new HierarchicalRelationResource(topicServiceResolver,
				_ndlaRequest, id);
	}
}
