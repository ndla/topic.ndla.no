/*
 * Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
 * June 10, 2013
 */

package no.ndla.topics.rest.resource;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import no.ndla.topics.rest.ConnectionInfo;
import no.ndla.topics.rest.NdlaRequest;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class HorizontalRelationResource {
	private NdlaTopic _ndlaTopic;
	private NdlaRequest _ndlaRequest;

	public HorizontalRelationResource(
			ContextResolver<TopicService> topicServiceResolver,
			NdlaRequest ndlaRequest, String id) {
		this._ndlaRequest = ndlaRequest;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getHorisontalAssociationType() throws JSONException {
		JSONObject representation = new JSONObject();
		// TODO: Implementation.
		return representation;
	}
}
