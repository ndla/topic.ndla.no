package no.ndla.topics.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import no.ndla.topics.service.model.NdlaWord;
import no.ndla.topics.rest.NdlaRequest;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeywordResource {
	private NdlaTopic _ndlaTopic;
	private NdlaRequest _ndlaRequest;
	private Logger _logger = LoggerFactory.getLogger(KeywordResource.class);

	public KeywordResource(ContextResolver<TopicService> topicServiceResolver,
			NdlaRequest ndlaRequest, String id) throws WebApplicationException {
		this._ndlaRequest = ndlaRequest;
		try {
			_ndlaTopic = topicServiceResolver.getContext(TopicService.class)
					.getKeywordByKeywordId(id);
			if (null == _ndlaTopic) {
				throw new WebApplicationException(Status.NOT_FOUND);
			}
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getKeyword() throws WebApplicationException {
		JSONObject representation = new JSONObject();
		try {
			HashMap<String, String> names = _ndlaTopic.getNames();
			ArrayList<NdlaWord> wordNames = _ndlaTopic.getWordNames();
			HashMap<String, HashMap<String,String>> types = _ndlaTopic.getTypes();

			ArrayList<String> originatingSites = _ndlaTopic.getOriginatingSites();
			
			if(null != originatingSites && originatingSites.size() > 1) {
				JSONArray psiArray = new JSONArray();
				for (String site : originatingSites) {
					psiArray.put(site);
				}
				representation.put("originatingSites", psiArray);
			}
			else{
				representation.put("originatingSite",
						_ndlaTopic.getOriginatingSite());
			}

			
			ArrayList<String> psis = _ndlaTopic.getPsis();
			String psi = _ndlaTopic.getPsi();
			String topicIdString = _ndlaTopic.getIdentifier();
			
			if(psi.length() > 0){
				representation.put("psi", psi);
				if(topicIdString.length() == 0){
					topicIdString = psi.substring(psi.lastIndexOf("#")+1);
				}
			}
			else{
				if(psis.size() > 0){
					JSONArray jsonPsis = new JSONArray();
					for(String psiString : psis) {
						jsonPsis.put(psiString);
						if(psiString.contains("keywords")){
							representation.put("psi", psiString);
							if(topicIdString.length() == 0){
								topicIdString = psiString.substring(psiString.lastIndexOf("#")+1);
							}
						}
					}
					representation.put("psis",jsonPsis);
				}
			}
			representation.put("topicId", topicIdString);
			
			representation.put("visibility", _ndlaTopic.getVisibility());
			
			if(null != _ndlaTopic.getApproved()){
				String approved = _ndlaTopic.getApproved();
				representation.put("approved", approved);
				if(approved.equals("true")) {
					representation.put("approval_date", _ndlaTopic.getApprovalDate());
				}
			}
			
			if(null != _ndlaTopic.getProcessState()){
				String processState = _ndlaTopic.getProcessState();
				representation.put("processState", processState);
			}

			if (null != names && !names.isEmpty()) {
				JSONObject jsonNames = new JSONObject();
				for (Map.Entry<String, String> entry : names.entrySet()) {
					jsonNames.put(entry.getKey(), entry.getValue());
				}
				representation.put("names", jsonNames);
			}
			if (null != wordNames && !wordNames.isEmpty()) {
				JSONArray jsonNames = new JSONArray();
				ArrayList<String> wordclasses = new ArrayList<>();
				for (NdlaWord wordName : wordNames) {
					String wordClass = wordName.getWordClass();
					JSONObject currName = new JSONObject();

					if (!wordclasses.contains(wordClass)) {
						wordclasses.add(wordClass);
						currName.put("wordclass", wordClass);
						currName.put("data",
								resolveNamesByWordClass(wordClass, wordNames));
					}
					if (currName.has("wordclass")) {
						jsonNames.put(currName);
					}
				}
				representation.put("names", jsonNames);
			}
			
			if (types.size() > 0) {
				JSONArray jsonTypes = new JSONArray();
				Iterator it = types.keySet().iterator();
				
				while(it.hasNext()) {
					JSONObject typeObject = new JSONObject();
					String topicId = (String)it.next();
					typeObject.put("typeId", topicId);
					HashMap<String,String> typeNames = types.get(topicId);
					Iterator nit = typeNames.keySet().iterator();
					JSONArray typeNameArray = new JSONArray();
					
					while(nit.hasNext()) {
						String language = (String) nit.next();
						String typeName = typeNames.get(language);
						JSONObject typeNameObject = new JSONObject();
						typeNameObject.put(language,typeName);
						typeNameArray.put(typeNameObject);
					}
					typeObject.put("names", typeNameArray);
					jsonTypes.put(typeObject);
				}
				representation.put("types", jsonTypes);
			}
			
		} catch (NullPointerException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return representation;
	}

	/**
	 * Helper methods
	 */
	private JSONArray resolveNamesByWordClass(String wordclass,
			ArrayList<NdlaWord> names) {
		JSONArray result = new JSONArray();
		try {
			for (NdlaWord name : names) {
				String currWordClass = name.getWordClass();
				HashMap<String, String> currentNames = name.getNames();
				Iterator<String> it = currentNames.keySet().iterator();
				while (it.hasNext()) {
					String lang = (String) it.next();
					String nameString = (String) currentNames.get(lang);
					if (currWordClass.equals(wordclass)) {
						JSONObject nameObj = new JSONObject();
						nameObj.put(lang, nameString);
						result.put(nameObj);
					}
				}
			}
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}

		return result;
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putKeyword(JSONObject keyword) throws JSONException {
		// 200 (OK) or 204 (No Content). 404 (Not Found), if ID not found or
		// invalid.
		return Response.noContent().build(); // TODO: Implement method.
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postKeyword() {
		// 404 (Not Found).
		return Response.status(Status.NOT_FOUND).build();
	}

	@DELETE
	public Response deleteKeyword() {
		// 200 (OK). 404 (Not Found), if ID not found or invalid.
		return Response.noContent().build(); // TODO: Implement method.
	}
}
