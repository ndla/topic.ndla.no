/*
 * Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
 * June 10, 2013
 */
package no.ndla.topics.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import no.ndla.topics.rest.ConnectionInfo;
import no.ndla.topics.rest.NdlaRequest;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LanguageResource {
	private NdlaTopic _ndlaTopic;
	private NdlaRequest _ndlaRequest;
	private Logger _logger = LoggerFactory.getLogger(LanguageResource.class);

	public LanguageResource(
			ContextResolver<TopicService> topicServiceResolver,
			NdlaRequest ndlaRequest, String id) throws WebApplicationException {
		this._ndlaRequest = ndlaRequest;

		try {
			_ndlaTopic = topicServiceResolver.getContext(TopicService.class)
					.getLanguage(id);
			_logger.info("Lang: " + _ndlaTopic.toString());
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NullPointerException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getLanguage() throws WebApplicationException {
		JSONObject representation = new JSONObject();
		try {
			_logger.info("NdlaTopic: " + _ndlaTopic.toString());
			HashMap<String, String> names = _ndlaTopic.getNames();

			representation.put("topicId", _ndlaTopic.getIdentifier());
			representation.put("psi", _ndlaTopic.getPsi());

			if (!names.isEmpty()) {
				JSONObject jsonNames = new JSONObject();
				for (Map.Entry<String, String> entry : names.entrySet()) {
					jsonNames.put(entry.getKey(), entry.getValue());
				}
				representation.put("names", jsonNames);
			}
		} catch (NullPointerException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return representation;
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putLanguage(JSONObject keyword) throws JSONException {
		// 200 (OK) or 204 (No Content). 404 (Not Found), if ID not found or
		// invalid.
		return Response.noContent().build(); // TODO: Implement method.
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postLanguage() {
		// 404 (Not Found).
		return Response.status(Status.NOT_FOUND).build();
	}

	@DELETE
	public Response deleteLanguage() {
		// 200 (OK). 404 (Not Found), if ID not found or invalid.
		return Response.noContent().build(); // TODO: Implement method.
	}

}
