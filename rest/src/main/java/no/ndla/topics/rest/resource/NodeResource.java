/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * April 09, 2012
 */

package no.ndla.topics.rest.resource;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import no.ndla.topics.rest.ConnectionInfo;
import no.ndla.topics.rest.NdlaRequest;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class NodeResource {
	private NdlaTopic _ndlaTopic;
	private NdlaRequest _ndlaRequest;

	public NodeResource(ContextResolver<TopicService> topicServiceResolver,
			NdlaRequest ndlaRequest, String id) throws WebApplicationException {
		this._ndlaRequest = ndlaRequest;

		try {
			_ndlaTopic = topicServiceResolver.getContext(TopicService.class)
					.getNodeByNodeId(id, NdlaSite.NDLA); // TODO: Default NdlaSite parameter.
			if (null == _ndlaTopic) {
				throw new WebApplicationException(Status.NOT_FOUND);
			}
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getNode() throws JSONException {
		JSONObject representation = new JSONObject();
		try {
			HashMap<String, String> descriptions = _ndlaTopic.getDescriptions();
			HashMap<String, String> ingresses = _ndlaTopic.getIngresses();
			HashMap<String, String> licenses = _ndlaTopic.getLicenses();
			HashMap<String, String> names = _ndlaTopic.getNames();
			ArrayList<String> authors = _ndlaTopic.getAuthors();

			representation.put("topicId", _ndlaTopic.getIdentifier());
			representation.put("psi", _ndlaTopic.getPsi());
			representation.put("originatingSite",
					_ndlaTopic.getOriginatingSite());
			representation.put("nodeType", _ndlaTopic.getNodeType());

			if (!descriptions.isEmpty()) {
				JSONObject jsonDescriptions = new JSONObject();
				for (Map.Entry<String, String> entry : descriptions.entrySet()) {
					jsonDescriptions.put(entry.getKey(), entry.getValue());
				}
				representation.put("descriptions", jsonDescriptions);
			}
			if (!ingresses.isEmpty()) {
				JSONObject jsonIngresses = new JSONObject();
				for (Map.Entry<String, String> entry : ingresses.entrySet()) {
					jsonIngresses.put(entry.getKey(), entry.getValue());
				}
				representation.put("ingresses", jsonIngresses);
			}
			if (!licenses.isEmpty()) {
				JSONObject jsonLicenses = new JSONObject();
				for (Map.Entry<String, String> entry : licenses.entrySet()) {
					jsonLicenses.put(entry.getKey(), entry.getValue());
				}
				representation.put("licenses", jsonLicenses);
			}
			if (!names.isEmpty()) {
				JSONObject jsonNames = new JSONObject();
				for (Map.Entry<String, String> entry : names.entrySet()) {
					jsonNames.put(entry.getKey(), entry.getValue());
				}
				representation.put("names", jsonNames);
			}

			if (!authors.isEmpty()) {
				JSONArray jsonAuthors = new JSONArray();
				for (String author : authors) {
					jsonAuthors.put(author);
				}
				representation.put("authors", jsonAuthors);
			}
			representation.put("image", _ndlaTopic.getImage());
		} catch (NullPointerException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return representation;
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putNode(JSONObject node) throws JSONException {
		// 200 (OK) or 204 (No Content). 404 (Not Found), if ID not found or
		// invalid.
		return Response.noContent().build(); // TODO: Implement method.
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postNode() {
		// 404 (Not Found).
		return Response.status(Status.NOT_FOUND).build();
	}

	@DELETE
	public Response deleteNode() {
		// 200 (OK). 404 (Not Found), if ID not found or invalid.
		return Response.noContent().build(); // TODO: Implement method.
	}
}