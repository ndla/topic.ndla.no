package no.ndla.topics.rest.resource;

/*
 * Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
 * June 10, 2013
 */
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import no.ndla.topics.rest.ConnectionInfo;
import no.ndla.topics.rest.NdlaRequest;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class OntologyTopicBySubjectResource {
	private JSONObject _ndlaTopic;
	private NdlaRequest _ndlaRequest;

	public OntologyTopicBySubjectResource(
			ContextResolver<TopicService> topicServiceResolver,
			NdlaRequest ndlaRequest, String subjectMatterId,String topicId) {
		this._ndlaRequest = ndlaRequest;
		/*
		try {
			_ndlaTopic = topicServiceResolver.getContext(TopicService.class)
					.getSubjectOntologyTopicsByTopicId(subjectMatterId, topicId);
			if (null == _ndlaTopic) {
				throw new WebApplicationException(Status.NOT_FOUND);
			}
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		*/
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getOntologyTopic() throws JSONException {
		JSONObject representation = new JSONObject();
		// TODO: Implementation.
		return representation;
	}
}
