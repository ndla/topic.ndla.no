package no.ndla.topics.rest.resource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.NdlaWebServiceException;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

// Map the resource to the URL 'sites'.
@Path("/sites")
// Default media type (can be overridden at the method level).
@Produces(MediaType.APPLICATION_JSON)
public class SitesResource {
	Logger _logger = LoggerFactory.getLogger(SitesResource.class);

	// ***** KEYWORDS *****

	@POST
	@Path("{siteId}/nodes/{nodeId}/keywords")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject createKeywordsForNodeId(
			@PathParam("siteId") String siteId,
			@PathParam("nodeId") String nodeId, String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		try {
			HashMap<String, TopicIF> keywords = topicServiceResolver
					.getContext(TopicService.class).createKeywordsByNodeId(
							nodeId, payload,
							NdlaSite.valueOf(siteId.toUpperCase()));

			// TODO: Refactor to a createMessage method with 'status' and
			// 'message'
			// parameters.
			result.put("status", "Ok");
			result.put(
					"message",
					String.format(
							"The keywords were successfully added to the [%s] node in the [%s] site.",
							nodeId, siteId));
			String itemIdentifier;
			JSONArray itemIdentifiers = new JSONArray();
			ArrayList<String> subjectIds = topicServiceResolver.getContext(
					TopicService.class).getKeywordSubjectIdentifiers(nodeId,
					NdlaSite.valueOf(siteId.toUpperCase()));

			for (String subjectId : subjectIds) {
				itemIdentifiers.put(subjectId.substring(subjectId
						.lastIndexOf("#") + 1));
			}
			result.put("itemIdentifiers", itemIdentifiers);

			JSONObject wordData = topicServiceResolver.getContext(
					TopicService.class).getWordData(nodeId,
					NdlaSite.valueOf(siteId.toUpperCase()));
			result.put("wordclasses", wordData.getString("wordclasses"));
			result.put("visibilities", wordData.getString("visibilities"));
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@POST
	@Path("{siteId}/keywords")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject createKeywords(
			String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		try {

			HashMap<String, TopicIF> keywords = topicServiceResolver
					.getContext(TopicService.class).createKeyword(payload);

			// TODO: Refactor to a createMessage method with 'status' and
			// 'message' parameters.
			result.put("status", "Ok");
			result.put("message", "The keywords were successfully created");
			
			JSONArray itemIdentifiers = new JSONArray();

			Iterator it = keywords.keySet().iterator();
			while (it.hasNext()) {
				String itemIdentifier = (String) it.next();
				itemIdentifiers.put(itemIdentifier);
			}
			result.put("itemIdentifiers", itemIdentifiers);
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@PUT
	@Path("{siteId}/nodes/{nodeId}/keywords")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject updateKeywordsForNodeId(
			@PathParam("siteId") String siteId,
			@PathParam("nodeId") String nodeId, String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		try {
			HashMap<String, TopicIF> keywords = topicServiceResolver
					.getContext(TopicService.class).updateKeywordsByNodeId(
							nodeId, payload,
							NdlaSite.valueOf(siteId.toUpperCase()));

			// TODO: Refactor to a createMessage method with 'status' and
			// 'message' parameters.
			
			result.put("status", "Ok");
			result.put(
					"message",
					String.format(
							"The keywords were successfully updated to the [%s] node in the [%s] site.",
							nodeId, siteId));
			String itemIdentifier;
			JSONArray itemIdentifiers = new JSONArray();
			result.put("itemIdentifiers", itemIdentifiers);
			
			ArrayList<String> subjectIds = topicServiceResolver.getContext(
					TopicService.class).getKeywordSubjectIdentifiers(nodeId,
					NdlaSite.valueOf(siteId.toUpperCase()));
			
			for (String subjectId : subjectIds) {
				itemIdentifiers
						.put(subjectId.substring(subjectId.lastIndexOf("#") + 1));
			}
			

			JSONObject wordData = topicServiceResolver.getContext(
					TopicService.class).getWordData(nodeId,
					NdlaSite.valueOf(siteId.toUpperCase()));
			result.put("wordclasses", wordData.getString("wordclasses"));
			result.put("visibilities", wordData.getString("visibilities"));
			
		} catch (BadObjectReferenceException e) {
			System.out.println("WEBAPPLIKEX: "+e.toString());
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException e) {
			System.out.println("NDLASERVEX: "+e.toString());
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			System.out.println("JSONEX: "+e.toString());
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	@DELETE
	@Path("{siteId}/nodes/{nodeId}/keywords")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject deleteKeywordsForNodeId(
			@PathParam("siteId") String siteId,
			@PathParam("nodeId") String nodeId, String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver) throws WebApplicationException {
		JSONObject result = new JSONObject();
		try {
			// HashMap<String, TopicIF> keywords =
			// topicServiceResolver.getContext(TopicService.class).updateKeywordsByNodeId(nodeId,
			// payload, NdlaSite.valueOf(siteId.toUpperCase()));
			JSONArray itemIdentifiers = new JSONArray();
			ArrayList<String> subjectIds = topicServiceResolver.getContext(
					TopicService.class).getKeywordSubjectIdentifiers(nodeId,
					NdlaSite.valueOf(siteId.toUpperCase()));

			for (String subjectId : subjectIds) {
				itemIdentifiers
						.put(subjectId.substring(subjectId.lastIndexOf("#") + 1));
			}
			result.put("itemIdentifiers", itemIdentifiers);
			
			HashMap<String, TopicIF> keywords = topicServiceResolver
					.getContext(TopicService.class)
					.removeAssociation(payload,
							NdlaSite.valueOf(siteId.toUpperCase()));
			// TODO: Refactor to a createMessage method with 'status' and 'message' parameters.
			result.put("status", "Ok");
			result.put(
					"message",
					String.format(
							"The keywords were successfully deleted from the [%s] node in the [%s] site.",
							nodeId, siteId));
			String itemIdentifier;
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	// saveKeywordsBySubjectMatter
	// (/sites/ndla/subjects/{subject-identifier)/keywords)
	@POST
	@Path("{siteId}/subjects/{subjectId}/keywords")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject saveKeywordsBySubjectMatter(
			@PathParam("siteId") String siteId,
			@PathParam("subjectId") String subjectId, String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver) throws WebApplicationException {
		JSONObject result = new JSONObject();
		try {
			// TODO: Deserialize JSON payload.
			ArrayList<String> data = new ArrayList<String>();

			JSONObject json = new JSONObject(payload);
			JSONArray jsonKeywords = json.getJSONArray("keywords");
			for (int i = 0; i < jsonKeywords.length(); i++) {
				data.add(jsonKeywords.get(i).toString());
			}
			ArrayList<TopicIF> keywords = topicServiceResolver.getContext(
					TopicService.class)
					.saveKeywordsBySubjectMatter(subjectId, data,
							NdlaSite.valueOf(siteId.toUpperCase()));

			// TODO: Refactor to a createMessage method with 'status' and
			// 'message' parameters.
			result.put("status", "Ok");
			result.put(
					"message",
					String.format(
							"The keywords were successfully added to the [%s] subject in the [%s] site.",
							subjectId, siteId));
			String itemIdentifier;
			JSONArray itemIdentifiers = new JSONArray();
			for (TopicIF topicIf : keywords) {
				Collection<LocatorIF> ids = topicIf.getItemIdentifiers();
				Iterator<LocatorIF> it = ids.iterator();
				while (it.hasNext()) {
					LocatorIF loc = (LocatorIF) it.next();
					itemIdentifier = loc.getAddress();
					itemIdentifiers
							.put(itemIdentifier.substring(itemIdentifier
									.lastIndexOf("#") + 1));
				}
			}
			result.put("itemIdentifiers", itemIdentifiers);
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	// lookupKeywordsBySearchTerm (/keywords/?filter[search]=search+term)
	
	
	// ****** LANGUAGES ************
	// createLanguage
		@POST
		@Path("{siteId}/languages")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public JSONObject createLanguage(String payload,
				final @Context ContextResolver<TopicService> topicServiceResolver)
				throws WebApplicationException {
			
			JSONObject result = new JSONObject();
			try{
				TopicIF language = topicServiceResolver.getContext(TopicService.class).createLanguage(payload);
				if (null != language) {
					JSONObject data = new JSONObject(payload);
					String languageId = data.getString("languageId");
					result.put("status", "Ok");
					result.put(
							"message",
							String.format(
									"The language [%S] was successfully created.", languageId));
				}
			} catch (BadObjectReferenceException e) {
				throw new WebApplicationException(Status.NOT_FOUND);
			} catch (NdlaServiceException e) {
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			} catch (JSONException e) {
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
			return result;
		}
		
		
		// deleteLanguageByLanguageId
		@DELETE
		@Path("{siteId}/languages")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public JSONObject deleteLanguageByLanguageId(String payload,
				final @Context ContextResolver<TopicService> topicServiceResolver)
				throws WebApplicationException {
			JSONObject result = new JSONObject();
			try {
				TopicIF deletedLanguage = topicServiceResolver.getContext(TopicService.class).deleteLanguageByLanguageId(payload);

				if (null != deletedLanguage) {
					result.put("status", "Ok");
					JSONArray jData = new JSONArray(payload);
					result.put(
							"message",
							String.format(
									"The language [%s] was successfully deleted.",
									jData.getString(0)));
					
				}
			} catch (BadObjectReferenceException e) {
				throw new WebApplicationException(Status.NOT_FOUND);
			} catch (NdlaServiceException e) {
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			} catch (JSONException e) {
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
			return result;
		}
	// *****END LANGUAGES **********

		
	// ********** SUBJECTS **************
		// createSubject
				@POST
				@Path("{siteId}/subjects")
				@Consumes(MediaType.APPLICATION_JSON)
				@Produces(MediaType.APPLICATION_JSON)
				public JSONObject createSubject(String payload,
						final @Context ContextResolver<TopicService> topicServiceResolver)
						throws WebApplicationException {
					
					JSONObject result = new JSONObject();
					try{
						TopicIF subject = topicServiceResolver.getContext(TopicService.class).createSubject(payload);
						if (null != subject) {
							JSONObject data = new JSONObject(payload);
							String languageId = data.getString("subjectId");
							result.put("status", "Ok");
							result.put(
									"message",
									String.format(
											"The subject [%S] was successfully created.", languageId));
						}
					} catch (BadObjectReferenceException e) {
						throw new WebApplicationException(Status.NOT_FOUND);
					} catch (NdlaServiceException e) {
						throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
					} catch (JSONException e) {
						throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
					}
					return result;
				}
				
				
				// deleteSubjectsBySubjectId
				@DELETE
				@Path("{siteId}/subjects")
				@Consumes(MediaType.APPLICATION_JSON)
				@Produces(MediaType.APPLICATION_JSON)
				public JSONObject deleteSubjectsBySubjectId(String payload,
						final @Context ContextResolver<TopicService> topicServiceResolver)
						throws WebApplicationException {
					JSONObject result = new JSONObject();
					try {
						ArrayList<TopicIF> deletedSubjects = topicServiceResolver.getContext(TopicService.class).deleteSubjectBySubjectId(payload);

						if (deletedSubjects.size() > 0) {
							result.put("status", "Ok");
							JSONArray jData = new JSONArray(payload);
							for(int i = 0; i< deletedSubjects.size(); i++) {
								result.put(
										"message",
										String.format(
												"The subject [%s] was successfully deleted.",
												jData.getString(i)));	
							}
							
						}
					} catch (BadObjectReferenceException e) {
						throw new WebApplicationException(Status.NOT_FOUND);
					} catch (NdlaServiceException e) {
						throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
					} catch (JSONException e) {
						throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
					}
					return result;
				}
	// ********** END SUBJECTS **************
		
		
	// ***** NODES *****

	// createNode
	@POST
	@Path("{siteId}/nodes/{nodeId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject createNodeForSite(@PathParam("siteId") String siteId,
			@PathParam("nodeId") String nodeId, String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		try {
			TopicIF node = topicServiceResolver.getContext(TopicService.class)
					.createNewNode(nodeId, payload,
							NdlaSite.valueOf(siteId.toUpperCase()));
			// TODO: Refactor to a createMessage method with 'status' and 'message'
			// parameters.
			if (null != node) {
				result.put("status", "Ok");
				result.put(
						"message",
						String.format(
								"The node [%s] was successfully created for the [%s] site.",
								nodeId, siteId));
				//System.out.println("createNodeForSite: " + node.toString());
				Collection<String> ids = topicServiceResolver.getContext(
						TopicService.class).getItemIdentifiers(nodeId,
						NdlaSite.valueOf(siteId.toUpperCase()));
	
				Iterator<String> it = ids.iterator();
				while (it.hasNext()) {
					String address = (String) it.next();
					result.put("itemIdentifiers",
							address.substring(address.lastIndexOf("#") + 1));
				}
				//System.out.println("createNodeForSite [itemIdentifiers]: "+ ids.toString());
			}
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	// deleteNodeByNodeId
	@DELETE
	@Path("{siteId}/nodes/{nodeId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject deleteNodeByNodeId(@PathParam("siteId") String siteId,
			@PathParam("nodeId") String nodeId,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		try {
			TopicIF node = topicServiceResolver.getContext(TopicService.class)
					.deleteNodeByNodeId(nodeId,
							NdlaSite.valueOf(siteId.toUpperCase()));

			if (null != node) {
				result.put("status", "Ok");
				result.put(
						"message",
						String.format(
								"The node [%s] was successfully deleted for the [%s] site.",
								nodeId, siteId));
			}
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	// ***** TOPICS *****

	// createSiteTopics
	@POST
	@Path("{siteId}/sitetopics")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject createSiteTopics(@PathParam("siteId") String siteId,
			String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		HashMap<String, TopicIF> topics = null;
		
		try {
			HashMap<String, HashMap<String, HashMap<String, String>>> data = deserializeJsonForTopic(
					payload, siteId);
			topics = topicServiceResolver.getContext(TopicService.class)
					.createSiteTopics(data, NdlaSite.valueOf(siteId.toUpperCase()));

			if (topics.size() > 0) {
				result.put("status", "Ok");
				result.put(
						"message",
						String.format(
								"The topics were successfully created for the [%s] site.",
								siteId));
				String itemIdentifier;
				JSONArray itemIdentifiers = new JSONArray();
				for (TopicIF topicIf : topics.values()) {
					itemIdentifier = topicIf.getItemIdentifiers().iterator()
							.next().getAddress();
					itemIdentifiers.put(itemIdentifier.substring(itemIdentifier
							.lastIndexOf("#") + 1));
				}
				result.put("itemIdentifiers", itemIdentifiers);
			}
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	// deleteTopicsByTopicId

	@DELETE
	@Path("{siteId}/sitetopics")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject deleteSiteTopicsByTopicId(@PathParam("siteId") String siteId,
			String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		ArrayList<TopicIF> topics = null;
		ArrayList<String> data = new ArrayList<String>();

		try {
			JSONArray json = new JSONArray(payload);
			for (int i = 0; i < json.length(); i++) {
				data.add(json.get(i).toString());
			}		
			topics = topicServiceResolver.getContext(TopicService.class)
					.deleteSiteTopicsByTopicId(data,
							NdlaSite.valueOf(siteId.toUpperCase()));

			if (topics.size() > 0) {
				result.put("status", "Ok");
				result.put(
						"message",
						String.format(
								"The topics were successfully deleted for the [%s] site.",
								siteId));
				String itemIdentifier;
				JSONArray itemIdentifiers = new JSONArray();
				for (String topicid : data) {
					itemIdentifiers.put(topicid);
				}
				result.put("itemIdentifiers", itemIdentifiers);
			}
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	// saveTopicsByNodeId
	@POST
	@Path("{siteId}/nodes/{nodeId}/topics")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject saveTopicsByNodeId(@PathParam("siteId") String siteId,
			@PathParam("nodeId") String nodeId, String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		ArrayList<String> topics = null;
		
		try {
			//System.out.println("GOT: ID: "+nodeId+" PAYLOAD: "+payload);
			topics = topicServiceResolver.getContext(TopicService.class)
					.saveTopicsByNodeId(nodeId, payload,
							NdlaSite.valueOf(siteId.toUpperCase()));
			if (topics.size() > 0) {
				result.put("status", "Ok");
				result.put(
						"message",
						String.format(
								"The topics were successfully related for the [%s] node and for the [%s] site.",
								nodeId, siteId));
				String itemIdentifier;
				JSONArray itemIdentifiers = new JSONArray();
				for (String topicid : topics) {
					itemIdentifiers.put(topicid);
				}
				result.put("itemIdentifiers", itemIdentifiers);
			}
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	

	// relateTopicsByNodeId
		@DELETE
		@Path("{siteId}/nodes/{nodeId}/topics")
		@Consumes(MediaType.APPLICATION_JSON)
		@Produces(MediaType.APPLICATION_JSON)
		public JSONObject deleteTopicsByNodeId(@PathParam("siteId") String siteId,
				@PathParam("nodeId") String nodeId, String payload,
				final @Context ContextResolver<TopicService> topicServiceResolver)
				throws WebApplicationException {
			JSONObject result = new JSONObject();
			ArrayList<String> topics = null;
			
			try {
				//System.out.println("DELETE GOT: ID: "+nodeId+" PAYLOAD: "+payload);
				topics = topicServiceResolver.getContext(TopicService.class)
						.deleteTopicsByNodeId(nodeId, payload,
								NdlaSite.valueOf(siteId.toUpperCase()));
				if (topics.size() > 0) {
					result.put("status", "Ok");
					result.put(
							"message",
							String.format(
									"The topics were successfully deleted from the [%s] node",
									nodeId, siteId));
					String itemIdentifier;
					JSONArray itemIdentifiers = new JSONArray();
					for (String topicid : topics) {
						itemIdentifiers.put(topicid);
					}
					result.put("itemIdentifiers", itemIdentifiers);
				}
			} catch (NdlaServiceException e) {
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			} catch (BadObjectReferenceException e) {
				throw new WebApplicationException(Status.NOT_FOUND);
			} catch (JSONException e) {
				throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
			}
			return result;
		}

	//addAssociation
	@PUT
	@Path("{siteId}/nodes/{nodeId}/associations")
	public JSONObject addAssociation(@PathParam("siteId") String siteId, 
			@PathParam("nodeId")String nodeId, 
			String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver) throws WebApplicationException{
		JSONObject result = new JSONObject();
		try{
			HashMap<String, TopicIF> topics = topicServiceResolver.getContext(TopicService.class).addAssociation(payload, NdlaSite.valueOf(siteId.toUpperCase()));
			if (topics.size() > 0) {
				Iterator it = topics.keySet().iterator();
				result.put("status", "Ok");
				result.put(
						"message",
						String.format(
								"The topics were successfully associated"));
				
				JSONArray itemIdentifiers = new JSONArray();
				while(it.hasNext()) {
					String topicId = (String) it.next();
					itemIdentifiers.put(topicId);
				}
				result.put("itemIdentifiers", itemIdentifiers);
			}
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}


	// updateRelateTopicsByNodeId
	@PUT
	@Path("{siteId}/nodes/{nodeId}/sitetopics")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject updateSiteTopicRelationsByNodeId(
			@PathParam("siteId") String siteId,
			@PathParam("nodeId") String nodeId, String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		ArrayList<TopicIF> topics = null;
		ArrayList<String> data = new ArrayList<String>();

		try {
			JSONArray jsonTopics = new JSONArray(payload);
	
			for (int i = 0; i < jsonTopics.length(); i++) {
				data.add(jsonTopics.get(i).toString());
			}
			topics = topicServiceResolver.getContext(TopicService.class)
					.updateSiteTopicRelationsByNodeId(nodeId, data,
							NdlaSite.valueOf(siteId.toUpperCase()));
			if (topics.size() > 0) {
				result.put("status", "Ok");
				result.put(
						"message",
						String.format(
								"The topics were successfully updated for the [%s] node and for the [%s] site.",
								nodeId, siteId));
				String itemIdentifier;
				JSONArray itemIdentifiers = new JSONArray();
				for (String topicid : data) {
					itemIdentifiers.put(topicid);
				}
				result.put("itemIdentifiers", itemIdentifiers);
			}
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	// updateTopicsByTopicId
	@PUT
	@Path("{siteId}/topics/{topicId}")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject updateTopicsByTopicId(@PathParam("siteId") String siteId,
			@PathParam("topicId") String topicId, String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		HashMap<String, TopicIF> topics = null;
		
		try {
			HashMap<String, HashMap<String, HashMap<String, String>>> data = deserializeJsonForTopic(
					payload, siteId);
			topics = topicServiceResolver.getContext(TopicService.class)
					.updateTopicsByTopicId(topicId, data,
							NdlaSite.valueOf(siteId.toUpperCase()));
			if (topics.size() > 0) {
				result.put("status", "Ok");
				result.put(
						"message",
						String.format(
								"The topics were successfully updated for the [%s] site.",
								siteId));
				String itemIdentifier;
				JSONArray itemIdentifiers = new JSONArray();
				for (TopicIF topicIf : topics.values()) {
					itemIdentifier = topicIf.getItemIdentifiers().iterator()
							.next().getAddress();
					itemIdentifiers.put(itemIdentifier.substring(itemIdentifier
							.lastIndexOf("#") + 1));
				}
				result.put("itemIdentifiers", itemIdentifiers);
			}
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	
	
	// saveTopicsBySubjectMatter
	@POST
	@Path("{siteId}/subjects/{subjectId}/topics")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject saveTopicsBySubjectMatter(
			@PathParam("siteId") String siteId,
			@PathParam("subjectId") String subjectId, String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		
		ArrayList<String> topics = new ArrayList<String>();
		try {
			JSONObject json = new JSONObject(payload);
			JSONArray jsonTopics = json.getJSONArray("topics");
		
			topics = topicServiceResolver.getContext(TopicService.class)
					.saveTopicsBySubjectMatter(subjectId, payload,
							NdlaSite.valueOf(siteId.toUpperCase()));	
			if (topics.size() > 0) {
				result.put("status", "Ok");
				result.put(
						"message",
						String.format(
								"The topics were successfully saved for the [%s] and for the [%s] site.",
								subjectId, siteId));
				String itemIdentifier;
				JSONArray itemIdentifiers = new JSONArray();
				for (String topicid : topics) {
					itemIdentifiers.put(topicid);
				}
				result.put("itemIdentifiers", itemIdentifiers);
			}
		} catch (NdlaServiceException  | BadObjectReferenceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}

	// TODO: Remove unused method (BK).
	private HashMap<String, HashMap<String, HashMap<String, String>>> deserializeJsonForNode(
			String payload, String identifier) throws JSONException {
		HashMap<String, HashMap<String, HashMap<String, String>>> result = new HashMap<String, HashMap<String, HashMap<String, String>>>();
		HashMap<String, HashMap<String, String>> nameSet = new HashMap<String, HashMap<String, String>>();
		HashMap<String, HashMap<String, String>> dataSet = new HashMap<String, HashMap<String, String>>();
		try {
			JSONObject json = new JSONObject(payload);
			JSONArray nodeName = json.getJSONArray("names");

			JSONObject nodeData = json.getJSONObject("data");

			HashMap<String, String> names = new HashMap<String, String>();
			for (int i = 0; i < nodeName.length(); i++) {
				JSONObject name = nodeName.getJSONObject(i);
				Iterator<?> nit = name.keys();
				while (nit.hasNext()) {
					String lang = (String) nit.next();
					String title = name.getString(lang);
					names.put(lang, title);
				}
			}
			nameSet.put("namedata", names);

			HashMap<String, String> data_elm1 = new HashMap<String, String>();
			HashMap<String, String> data_elm2 = new HashMap<String, String>();
			HashMap<String, String> data_elm3 = new HashMap<String, String>();
			HashMap<String, String> data_elm4 = new HashMap<String, String>();
			HashMap<String, String> data_elm5 = new HashMap<String, String>();

			Iterator<?> dit = nodeData.keys();
			while (dit.hasNext()) {
				String dataKey = (String) dit.next();
				switch (dataKey) {
				case "author":
					JSONArray authorData = nodeData.getJSONArray(dataKey);
					for (int j = 0; j < authorData.length(); j++) {
						JSONObject author = authorData.getJSONObject(j);
						Iterator<?> auIt = author.keys();
						while (auIt.hasNext()) {
							String auLang = (String) auIt.next();
							;
							data_elm1.put(auLang, author.getString(auLang));
							dataSet.put(dataKey, data_elm1);
						}
					}
					break;
				case "author_id":
					JSONArray authorIdData = nodeData.getJSONArray(dataKey);
					for (int k = 0; k < authorIdData.length(); k++) {
						JSONObject authorId = authorIdData.getJSONObject(k);
						Iterator<?> auIdIt = authorId.keys();
						while (auIdIt.hasNext()) {
							String auIdLang = (String) auIdIt.next();
							data_elm2.put(auIdLang,
									authorId.getString(auIdLang));
							dataSet.put(dataKey, data_elm2);
						}
					}
					break;
				case "node_type":
					JSONArray node_typeData = nodeData.getJSONArray(dataKey);
					for (int kn = 0; kn < node_typeData.length(); kn++) {
						JSONObject node_type = node_typeData.getJSONObject(kn);
						Iterator<?> node_typeIt = node_type.keys();
						while (node_typeIt.hasNext()) {
							String node_typeLang = (String) node_typeIt.next();
							data_elm5.put(node_typeLang,
									node_type.getString(node_typeLang));
							dataSet.put(dataKey, data_elm5);
						}
					}
					break;
				case "license":
					JSONArray licenseData = nodeData.getJSONArray(dataKey);
					for (int l = 0; l < licenseData.length(); l++) {
						JSONObject license = licenseData.getJSONObject(l);
						Iterator<?> licenseIt = license.keys();
						while (licenseIt.hasNext()) {
							String licenseLang = (String) licenseIt.next();
							data_elm3.put(licenseLang,
									license.getString(licenseLang));
							dataSet.put(dataKey, data_elm3);
						}
					}
					break;
				case "ingress":
					JSONArray ingressData = nodeData.getJSONArray(dataKey);
					for (int m = 0; m < ingressData.length(); m++) {
						JSONObject ingress = ingressData.getJSONObject(m);
						Iterator<?> ingressIt = ingress.keys();
						while (ingressIt.hasNext()) {
							String ingressLang = (String) ingressIt.next();
							data_elm4.put(ingressLang,
									ingress.getString(ingressLang));
							dataSet.put(dataKey, data_elm4);
						}
					}
					break;
				}
			}
			// _logger.info(names.toString());
			result.put("names", nameSet);
			result.put("data", dataSet);
		} catch (JSONException e) {
			System.out.println(e.getMessage());
			throw new JSONException(e.getMessage());
		}
		return result;
	}

	private HashMap<String, HashMap<String, HashMap<String, String>>> deserializeJsonForTopic(
			String payload, String siteIdentifier) throws JSONException {
		HashMap<String, HashMap<String, HashMap<String, String>>> result = new HashMap<String, HashMap<String, HashMap<String, String>>>();

		try {
			JSONObject json = new JSONObject(payload);
			JSONArray topics = json.getJSONArray("topics");
			for (int i = 0; i < topics.length(); i++) {
				JSONObject topicData = topics.getJSONObject(i);
				Iterator<?> topicIterator = topicData.keys();
				while (topicIterator.hasNext()) {
					String setId = (String) topicIterator.next();
					JSONObject topicDataObj = topicData.getJSONObject(setId);
					HashMap<String, HashMap<String, String>> data = new HashMap<String, HashMap<String, String>>();
					HashMap<String, String> names = new HashMap<String, String>();
					HashMap<String, String> occurrences = new HashMap<String, String>();
					Iterator<?> topicDataIterator = topicDataObj.keys();
					while (topicDataIterator.hasNext()) {
						String topicKey = (String) topicDataIterator.next();
						switch (topicKey) {
						case "names":
							JSONArray topicNames = topicDataObj
									.getJSONArray(topicKey);
							for (int j = 0; j < topicNames.length(); j++) {
								JSONObject topicName = topicNames
										.getJSONObject(j);
								String language = (String) topicName.keys()
										.next();
								String nameString = topicName
										.getString(language);
								names.put(language, nameString);
							}
							data.put("names", names);
							break;
						case "occurrences":
							JSONArray topicOccurrences = topicDataObj
									.getJSONArray(topicKey);
							for (int k = 0; k < topicOccurrences.length(); k++) {
								JSONObject topicOccurrence = topicOccurrences
										.getJSONObject(k);
								String occurrenceType = (String) topicOccurrence
										.keys().next();
								switch (occurrenceType) {
								case "description":
									JSONArray descriptions = topicOccurrence
											.getJSONArray(occurrenceType);
									for (int l = 0; l < descriptions.length(); l++) {
										JSONObject description = descriptions
												.getJSONObject(l);
										String descriptionLang = (String) description
												.keys().next();
										String descriptionString = description
												.getString(descriptionLang);
										occurrences.put(descriptionLang + "#"
												+ "description",
												descriptionString);
									}
									break;
								case "image":

									JSONArray images = topicOccurrence
											.getJSONArray(occurrenceType);

									for (int n = 0; n < images.length(); n++) {
										JSONObject image = images
												.getJSONObject(n);
										String imageKey = (String) image.keys()
												.next();
										occurrences.put(occurrenceType + "_"
												+ n, image.getString(imageKey));
									}
									break;
								}// end switch occurrenceType
							}
							data.put("occurrences", occurrences);
							break;
						}// end switch typename
					}// end while topicData
					result.put(setId, data);
				}// end while topicData
			}// end for topics
		} catch (JSONException e) {
			System.out.println(e.getMessage());
			throw new JSONException(e.getMessage());
		}
		return result;
	}
}