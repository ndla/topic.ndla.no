/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * April 09, 2012
 */

package no.ndla.topics.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import no.ndla.topics.service.model.NdlaWord;
import no.ndla.topics.rest.NdlaRequest;

public class SubjectResource {
	private NdlaTopic _ndlaTopic;
	private NdlaRequest _ndlaRequest;
	private Logger _logger = LoggerFactory.getLogger(SubjectResource.class);
	
	public SubjectResource(ContextResolver<TopicService> topicServiceResolver,
			NdlaRequest ndlaRequest, String id) throws WebApplicationException {
		this._ndlaRequest = ndlaRequest;
		
		
		try {
			_ndlaTopic = topicServiceResolver.getContext(TopicService.class)
					.getSubjectBySubjectId(id, NdlaSite.NDLA);
			if (null == _ndlaTopic) {
				throw new WebApplicationException(Status.NOT_FOUND);
			}
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getSubject() throws JSONException {
		JSONObject representation = new JSONObject();
		try {
			HashMap<String, String> names = _ndlaTopic.getNames();
			ArrayList<String> psis = _ndlaTopic.getPsis();
			ArrayList<String> originatingSites = _ndlaTopic.getOriginatingSites();
			
			representation.put("topicId", _ndlaTopic.getIdentifier());
			representation.put("udirPsi",_ndlaTopic.getUdirPsi());
			
			
			if (null != names && !names.isEmpty()) {
				JSONObject jsonNames = new JSONObject();
				for (Map.Entry<String, String> entry : names.entrySet()) {
					jsonNames.put(entry.getKey(), entry.getValue());
				}
				representation.put("names", jsonNames);
			}
			
			if(null != psis && psis.size() > 0) {
				JSONArray psiArray = new JSONArray();
				for (String psi : psis) {
					psiArray.put(psi);
				}
				representation.put("psis", psiArray);
			}
			
			if(null != originatingSites && originatingSites.size() > 1) {
				JSONArray psiArray = new JSONArray();
				for (String site : originatingSites) {
					psiArray.put(site);
				}
				representation.put("originatingSites", psiArray);
			}
			else{
				representation.put("originatingSite",
						_ndlaTopic.getOriginatingSite());
			}
		} catch (NullPointerException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return representation;
	}//end get
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putSubject(JSONObject keyword) throws JSONException {
		// 200 (OK) or 204 (No Content). 404 (Not Found), if ID not found or
		// invalid.
		return Response.noContent().build(); // TODO: Implement method.
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postSubject() {
		// 404 (Not Found).
		return Response.status(Status.NOT_FOUND).build();
	}

	@DELETE
	public Response deleteSubject() {
		// 200 (OK). 404 (Not Found), if ID not found or invalid.
		return Response.noContent().build(); // TODO: Implement method.
	}
}
