/*
 * Implemented by Brett Kromkamp (brett@seria.no)
 * April 08, 2012
 */

package no.ndla.topics.rest.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ContextResolver;

import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.FilterController;
import no.ndla.topics.rest.FilterParser;
import no.ndla.topics.rest.NdlaRequest;
import no.ndla.topics.rest.ResourceType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

//Map the resource to the URL 'subjects'.
@Path("/subjects")
// Default media type (can be overridden at the method level).
public class SubjectsResource {

	private NdlaRequest _ndlaRequest;
	private Logger _logger = LoggerFactory.getLogger(SubjectsResource.class);
	public SubjectsResource(@Context UriInfo uriInfo, @Context Request request) {
		this._ndlaRequest = new NdlaRequest(ResourceType.SUBJECT,
				FilterParser.fromUri(uriInfo), uriInfo, request);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getSubjects(
			final @Context ContextResolver<TopicService> topicServiceResolver) throws WebApplicationException {
		JSONObject representation = new JSONObject();
		FilterController filterController = new FilterController();
		try {
			representation = filterController.dispatchRequest(
					topicServiceResolver, _ndlaRequest);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return representation;
	}

	@PUT
	public Response putSubjects() {
		// 404 (Not Found), unless you want to update/replace every resource in
		// the entire collection.
		return Response.status(Status.NOT_FOUND).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postSubjects() {
		// 201 (Created), 'Location' header with link to /subjects/{id}
		// containing new ID.
		return Response.noContent().build(); // TODO: Implement method.
	}

	@DELETE
	public Response deleteSubjects() {
		// 404 (Not Found), unless you want to delete the whole collection���not
		// often desirable.
		return Response.status(Status.NOT_FOUND).build();
	}
	
	@Path("{subject}")
	public SubjectResource getSubject(@PathParam("subject") String id,
			final @Context ContextResolver<TopicService> topicServiceResolver) {
		return new SubjectResource(topicServiceResolver, _ndlaRequest, id);
	}
}
