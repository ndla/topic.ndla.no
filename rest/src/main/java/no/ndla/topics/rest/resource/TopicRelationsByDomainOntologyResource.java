/*
 * Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
 * June 10, 2013
 */

package no.ndla.topics.rest.resource;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.FilterController;
import no.ndla.topics.rest.FilterParser;
import no.ndla.topics.rest.NdlaRequest;
import no.ndla.topics.rest.ResourceType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

// Map the resource to the URL 'keywords'.
@Path("/domainontologytopicrelations")
// Default media type (can be overridden at the method level).
@Produces(MediaType.APPLICATION_JSON)
public class TopicRelationsByDomainOntologyResource {

	private NdlaRequest _ndlaRequest;

	public TopicRelationsByDomainOntologyResource(@Context UriInfo uriInfo,
			@Context Request request) {
		this._ndlaRequest = new NdlaRequest(
				ResourceType.DOMAINTOPICRELATIONS,
				FilterParser.fromUri(uriInfo), uriInfo, request);
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getDomainOntologyTopicRelations(
			final @Context ContextResolver<TopicService> topicServiceResolver) throws WebApplicationException {
		JSONObject representation = new JSONObject();
		FilterController filterController = new FilterController();
		try {
			representation = filterController.dispatchRequest(
					topicServiceResolver, _ndlaRequest);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return representation;
	}
	
	
	@POST
	@Path("{domainId}/{domainType}/topics")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject saveDomainOntologyTopicAssociations(@PathParam("domainId") String domainIdentifier,@PathParam("domainType") String domainType,String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		try {
			
			JSONArray savedTopics = topicServiceResolver
					.getContext(TopicService.class).saveDomainOntologyTopicAssociations(domainIdentifier,
							domainType,
							payload);
			
			if(savedTopics.length() > 0){
				result.put("status", "Ok");
				result.put(
						"message",
						String.format(
								"The topics were successfully added to the [%s] domain.",
								domainIdentifier));
			}
			else{
				result.put("status", "Error");
				result.put(
						"message",
						String.format(
								"There was a problem adding topics to the [%s] domain.",
								domainIdentifier));
			}
			
			
			String itemIdentifier;
			JSONArray topicIdentifiers = new JSONArray();
			
			for(int i = 0; i < savedTopics.length(); i ++){
				topicIdentifiers.put(savedTopics.get(i));
			}
			
			result.put("topicIdentifiers", topicIdentifiers);
			
			
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
	@DELETE
	@Path("{domainId}/{domainType}/topics")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject deleteDomainOntologyTopicAssociations(String payload,
			final @Context ContextResolver<TopicService> topicServiceResolver)
			throws WebApplicationException {
		JSONObject result = new JSONObject();
		try {
			
			ArrayList<String> savedTopics = topicServiceResolver
					.getContext(TopicService.class).removeAssociationByReifier(payload,NdlaSite.valueOf("NDLA"));
			
			if(savedTopics.size() > 0){
				result.put("status", "Ok");
				result.put(
						"message",
						String.format(
								"The topic relations were successfully removed."));
			}
			else{
				result.put("status", "Error");
				result.put(
						"message",
						String.format(
								"There was a problem removing the topic relations."));
			}
			
			
			String itemIdentifier;
			JSONArray topicIdentifiers = new JSONArray();
			
			for(int i = 0; i < savedTopics.size(); i ++){
				topicIdentifiers.put(savedTopics.get(i));
			}
			
			result.put("topicIdentifiers", topicIdentifiers);
			
			
		} catch (NdlaServiceException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return result;
	}
	
}
