/*
 * Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
 * June 4, 2014
 */

package no.ndla.topics.rest.resource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ContextResolver;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import no.ndla.topics.service.model.NdlaWord;
import no.ndla.topics.rest.NdlaRequest;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

public class TopicResource {
	private NdlaTopic _ndlaTopic;
	private NdlaRequest _ndlaRequest;
	
	public TopicResource(ContextResolver<TopicService> topicServiceResolver,
			NdlaRequest ndlaRequest, String id) throws WebApplicationException {
		this._ndlaRequest = ndlaRequest;

		try {
			_ndlaTopic = topicServiceResolver.getContext(TopicService.class)
					.getTopicByTopicId(id);
			if (null == _ndlaTopic) {
				throw new WebApplicationException(Status.NOT_FOUND);
			}
		} catch (BadObjectReferenceException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (NdlaServiceException error) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getTopic() throws WebApplicationException {
		JSONObject representation = new JSONObject();
		try {
			HashMap<String, String> descriptions = _ndlaTopic.getDescriptions();
			HashMap<String, String> ingresses = _ndlaTopic.getIngresses();
			HashMap<String, String> licenses = _ndlaTopic.getLicenses();
			HashMap<String, String> names = _ndlaTopic.getNames();
			ArrayList<NdlaWord> wordNames = _ndlaTopic.getWordNames();
			ArrayList<String> psis = _ndlaTopic.getPsis();
			String psi = _ndlaTopic.getPsi();
			String topicId = _ndlaTopic.getIdentifier();
			if(psi.length() > 0){
				representation.put("psi", psi);
				if(topicId.length() == 0){
					topicId = psi.substring(psi.lastIndexOf("#")+1);
				}
			}
			else{
				if(psis.size() > 0){
					JSONArray jsonPsis = new JSONArray();
					for(String psiString : psis) {
						jsonPsis.put(psiString);
						if(psiString.contains("topics")){
							representation.put("psi", psiString);
							if(topicId.length() == 0){
								topicId = psiString.substring(psiString.lastIndexOf("#")+1);
							}
						}
					}
					representation.put("psis",jsonPsis);
				}
			}
			representation.put("topicId", topicId);
			
			ArrayList<String> authors = _ndlaTopic.getAuthors();
			HashMap<String, String> images = _ndlaTopic.getImages();
			
			
			representation.put("originatingSite",
					_ndlaTopic.getOriginatingSite());
			
			if(null != _ndlaTopic.getApproved()){
				String approved = _ndlaTopic.getApproved();
				representation.put("approved", approved);
				if(approved.equals("true")) {
					representation.put("approval_date", _ndlaTopic.getApprovalDate());
				}
			}
			
			if(null != _ndlaTopic.getProcessState()){
				String processState = _ndlaTopic.getProcessState();
				representation.put("processState", processState);
			}
			
			if (!descriptions.isEmpty()) {
				JSONObject jsonDescriptions = new JSONObject();
				for (Map.Entry<String, String> entry : descriptions.entrySet()) {
					jsonDescriptions.put(entry.getKey(), entry.getValue());
				}
				representation.put("descriptions", jsonDescriptions);
			}
			
			if(null != images && images.size() > 0){
				JSONObject jsonImages = new JSONObject();
				for(Map.Entry<String, String> entry : images.entrySet()){
					jsonImages.put(entry.getKey(), entry.getValue());
				}
				representation.put("images", jsonImages);
			}
			else{
				representation.put("image", _ndlaTopic.getImage());
			}
			
			if (!ingresses.isEmpty()) {
				JSONObject jsonIngresses = new JSONObject();
				for (Map.Entry<String, String> entry : ingresses.entrySet()) {
					jsonIngresses.put(entry.getKey(), entry.getValue());
				}
				representation.put("ingresses", jsonIngresses);
			}
			
			if (!licenses.isEmpty()) {
				JSONObject jsonLicenses = new JSONObject();
				for (Map.Entry<String, String> entry : licenses.entrySet()) {
					jsonLicenses.put(entry.getKey(), entry.getValue());
				}
				representation.put("licenses", jsonLicenses);
			}
			if (null != names && !names.isEmpty()) {
				JSONObject jsonNames = new JSONObject();
				for (Map.Entry<String, String> entry : names.entrySet()) {
					jsonNames.put(entry.getKey(), entry.getValue());
				}
				representation.put("names", jsonNames);
			}
			
			if (null != wordNames && !wordNames.isEmpty()) {
				JSONArray jsonNames = new JSONArray();
				ArrayList<String> wordclasses = new ArrayList<>();
				for (NdlaWord wordName : wordNames) {
					String wordClass = wordName.getWordClass();
					JSONObject currName = new JSONObject();

					if (!wordclasses.contains(wordClass)) {
						wordclasses.add(wordClass);
						currName.put("wordclass", wordClass);
						currName.put("data",
								resolveNamesByWordClass(wordClass, wordNames));
					}
					if (currName.has("wordclass")) {
						jsonNames.put(currName);
					}
				}
				representation.put("names", jsonNames);
			}
			
			if (!authors.isEmpty()) {
				JSONArray jsonAuthors = new JSONArray();
				for (String author : authors) {
					jsonAuthors.put(author);
				}
				representation.put("authors", jsonAuthors);
			}
			
		} catch (NullPointerException e) {
			throw new WebApplicationException(Status.NOT_FOUND);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		
		return representation;
	}
	
	/**
	 * Helper methods
	 */
	private JSONArray resolveNamesByWordClass(String wordclass,
			ArrayList<NdlaWord> names) {
		JSONArray result = new JSONArray();
		try {
			for (NdlaWord name : names) {
				String currWordClass = name.getWordClass();
				HashMap<String, String> currentNames = name.getNames();
				Iterator<String> it = currentNames.keySet().iterator();
				while (it.hasNext()) {
					String lang = (String) it.next();
					String nameString = (String) currentNames.get(lang);
					if (currWordClass.equals(wordclass)) {
						JSONObject nameObj = new JSONObject();
						nameObj.put(lang, nameString);
						result.put(nameObj);
					}
				}
			}
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}

		return result;
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public Response putTopic(JSONObject topic) throws JSONException {
		// 200 (OK) or 204 (No Content). 404 (Not Found), if ID not found or
		// invalid.
		return Response.noContent().build(); // TODO: Implement method.
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postTopic() {
		// 404 (Not Found).
		return Response.status(Status.NOT_FOUND).build();
	}

	@DELETE
	public Response deleteTopic() {
		// 200 (OK). 404 (Not Found), if ID not found or invalid.
		return Response.noContent().build(); // TODO: Implement method.
	}
}
