/*
 * Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
 * June 4, 2014
 */

package no.ndla.topics.rest.resource;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.ext.ContextResolver;

import no.ndla.topics.service.TopicService;
import no.ndla.topics.rest.FilterController;
import no.ndla.topics.rest.FilterParser;
import no.ndla.topics.rest.NdlaRequest;
import no.ndla.topics.rest.ResourceType;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;


// Map the resource to the URL 'topics'.
@Path("/topics")
// Default media type (can be overridden at the method level).
@Produces(MediaType.APPLICATION_JSON)
public class TopicsResource {
	private NdlaRequest _ndlaRequest;

	public TopicsResource(@Context UriInfo uriInfo, @Context Request request) {
		this._ndlaRequest = new NdlaRequest(ResourceType.TOPIC,
				FilterParser.fromUri(uriInfo), uriInfo, request);
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public JSONObject getTopics(
			final @Context ContextResolver<TopicService> topicServiceResolver) throws WebApplicationException {
		JSONObject representation = new JSONObject();
		FilterController filterController = new FilterController();
		try {
			representation = filterController.dispatchRequest(
					topicServiceResolver, _ndlaRequest);
		} catch (JSONException e) {
			throw new WebApplicationException(Status.INTERNAL_SERVER_ERROR);
		}
		return representation;
	}

	@PUT
	public Response putTopics() {
		// 404 (Not Found), unless you want to update/replace every resource in
		// the entire collection.
		return Response.status(Status.NOT_FOUND).build();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	public Response postTopics() {
		// 201 (Created), 'Location' header with link to /topics/{id} containing
		// new ID.
		return Response.noContent().build(); // TODO: Implement method.
	}

	@DELETE
	public Response deleteTopics() {
		// 404 (Not Found), unless you want to delete the whole collection???not
		// often desirable.
		return Response.status(Status.NOT_FOUND).build();
	}

	@Path("{topic}")
	public TopicResource getTopic(@PathParam("topic") String id,
			final @Context ContextResolver<TopicService> topicServiceResolver) {
		// TODO: Need to ensure that authentication and logging is done (in
		// TopicResource).
		return new TopicResource(topicServiceResolver, _ndlaRequest, id);
	}
}
