package no.ndla.topics.service;


import com.sun.jersey.api.client.Client;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.NewCookie;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: rolf
 * Date: 21.08.13
 * Time: 13:25
 * To change this template use File | Settings | File Templates.
 */
public class Indexer {
    private Logger _logger = LoggerFactory.getLogger(Indexer.class);
    private Client client;

    private JSONObject userObject;
    private ClientResponse loginResponse;
    private WebResource loginResource;

    private String host;
    private String solrHost;
    private String user;
    private String pass;

    public Indexer(String host, String user, String pass) {
        this.host = host;
        this.user = user;
        this.pass = pass;
        this.client = Client.create();
    }

    public Indexer(String host, String solrHost, String user, String pass) {
      this.host = host;
      this.solrHost = solrHost;
      this.user = user;
      this.pass = pass;
      this.client = Client.create();
    }

    public boolean postToIndex(String payload,boolean dont_delete, String index)  throws NdlaServiceException{
        boolean success = false;
        indexServiceLogin(this.user,this.pass);
        boolean indexSuccess = indexKeyword(payload,dont_delete,index);
        boolean commitSuccess = commitIndex(index);
        if(indexSuccess && commitSuccess){
          success = true;
        }
        return success;
    }

    public boolean postToIndexNoLogin(String payload, boolean dont_delete, String index)  throws NdlaServiceException{
      boolean success = false;
      boolean indexSuccess = indexKeyword(payload,dont_delete,index);
      boolean commitSuccess = commitIndex(index);
      if(indexSuccess && commitSuccess){
        success = true;
      }
      return success;
    }

    public boolean deleteFromIndex(String id, String index)  throws NdlaServiceException{
      boolean success = false;
      indexServiceLogin(this.user,this.pass);
      boolean indexSuccess = deleteIndexedKeyword(id, index);
      boolean commitSuccess = commitIndex(index);
      if(indexSuccess && commitSuccess){
        success = true;
      }
      return success;
    }

    public boolean deleteFromIndex(ArrayList<String> ids, String index)  throws NdlaServiceException{
        indexServiceLogin(this.user,this.pass);
        boolean success = false;

        for( String id : ids) {
            success = deleteIndexedKeyword(id,index);
        }

        return success;
    }

    public boolean deleteAllFromIndex(String index) throws NdlaServiceException{
        indexServiceLogin(this.user,this.pass);
        boolean success = false;

        success = deleteIndexedKeyword("all",index);

        return success;
    }

    public void indexServiceLogin(String username, String password) throws NdlaServiceException{
        ClientResponse response;

        String loginEndPoint = "/api/topics/user/login";

        MultivaluedMap<String,String> queryParams = new MultivaluedMapImpl();
        queryParams.add("username",username);
        queryParams.add("password",password);

        try {

            response = jerseyRestClientPost(loginEndPoint,queryParams);
            String responseString = response.getEntity(String.class);
            //_logger.info("USEROBJECT: "+responseString);
            if(!responseString.equals("")){

                this.userObject = new JSONObject(responseString);

                this.loginResponse = response;
                //_logger.info("LOGINRESPONSE: "+this.loginResponse);
            }
        }
        catch (JSONException je) {
            throw new NdlaServiceException(je.getMessage());
        }
    }

    public String indexServiceGetToken() throws NdlaServiceException{
        String output = "";
        ClientResponse response;
        String tokenEndPoint = "/services/session/token";
        WebResource webResource = this.client.resource(this.host+tokenEndPoint);

        try{
            String session_name = this.userObject.getString("session_name");
            String sessid = this.userObject.getString("sessid");
            //_logger.info("SESNAME: "+session_name+" SESSID: "+sessid);
            WebResource.Builder builder = webResource.getRequestBuilder();
            builder.cookie(new Cookie(session_name,sessid));

            response =  builder.accept("application/json").get(ClientResponse.class);
            String token = response.getEntity(String.class);
            //_logger.info("TOKENRES: "+ token);
            if(response.getStatus() != 200) {
                throw new NdlaServiceException("Failed contacting SOLR index server for GET : HTTP Error code: "+response.getStatus());
            }
            //output = "X-CSRF-Token: " + token;
            output = token;
        }
        catch (JSONException je) {
            throw new NdlaServiceException(je.getMessage());
        }

        return output;
    }


    public boolean indexKeyword(String payload, boolean dont_delete, String index) throws NdlaServiceException {
        /*
        https://blogs.oracle.com/enterprisetechtips/entry/consuming_restful_web_services_with
         */
        boolean success = false;
        String indexEndPoint = "/api/topics/"+index; //POST
        MultivaluedMap<String,String> queryParams = new MultivaluedMapImpl();
        if(dont_delete){
            queryParams.add("keywords",payload);
            queryParams.add("dont_delete","true");
        }
        else{
            queryParams.add("keywords",payload);
        }

        ClientResponse response = jerseyRestClientPost(indexEndPoint,queryParams);
        String response_string = response.getEntity(String.class);
        //_logger.info("INDEXRESPONSE: "+response_string);
        //_logger.info("QPARAAM: "+queryParams);
        try{
            JSONObject responseJSON = new JSONObject(response_string);

            String message = responseJSON.getString("success");
            //_logger.info("MESSAGE: "+message);
            if(message.equals("true")) {
             success = true;
            }
        }
        catch (JSONException je) {
            throw  new NdlaServiceException("ERROR parsing returned JSON from the indexing server: "+je.getMessage());
        }
        return success;
    }

    public boolean deleteIndexedKeyword(String keywordid, String index) throws NdlaServiceException {
        boolean success = false;
        String deleteEndpoint = "/api/topics/"+index; // DELETE


        ClientResponse response = jerseyRestClientDelete(deleteEndpoint, keywordid);
        String responseJSONString = response.getEntity(String.class);
        _logger.info("INDEXRESPONSE: "+responseJSONString);
        try{
            JSONObject responseJSON = new JSONObject(responseJSONString);

            String message = responseJSON.getString("success");
            //_logger.info("MESSAGE: "+message);
            if(message.equals("true")) {
                success = true;
            }
        }
        catch (JSONException je) {
            throw  new NdlaServiceException("ERROR parsing returned JSON from the indexing server: "+je.getMessage());
        }
        return success;
    }

    public ClientResponse jerseyRestClientGet(String endpoint, MultivaluedMap<String,String> queryParams) throws NdlaServiceException {
        String output = "";
        ClientResponse response = null;

        try{
            //base web resource
            WebResource webResource = this.client.resource(this.host + endpoint);


            if(queryParams.size() > 0 ) {
                webResource.queryParams(queryParams);
            }


            //get
            response =  webResource.accept("application/json").get(ClientResponse.class);

            if(response.getStatus() != 200) {
                throw new NdlaServiceException("Failed contacting SOLR index server for GET : HTTP Error code: "+response.getStatus());
            }
        }
        catch (Exception e) {
            throw new NdlaServiceException(e.getMessage());
        }
        return response;
    }

    public ClientResponse jerseyRestClientPost(String endpoint, MultivaluedMap<String,String> queryParams) throws NdlaServiceException {
        String output = "";
        ClientResponse response = null;
        try{
            //base web resource
            WebResource webResource = this.client.resource(this.host+endpoint);

            if(queryParams.size() > 0 ) {
                webResource.queryParams(queryParams);
            }


            if(!endpoint.equals("/api/topics/user/login")){
                WebResource.Builder builder = webResource.getRequestBuilder();

                String session_name = this.userObject.getString("session_name");
                String sessid = this.userObject.getString("sessid");
                //_logger.info("SESNAME: "+session_name+" SESSID: "+sessid);

                builder = builder.cookie(new NewCookie(session_name,sessid));
                //_logger.info("COOKIE; "+c.getName());

                String token =  indexServiceGetToken();
                //_logger.info("TOKEN: "+token);

                builder = builder.header("X-CSRF-Token",token);

                response = builder.type(MediaType.APPLICATION_FORM_URLENCODED_TYPE).accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, queryParams);
                //_logger.info("BUILDER: "+response.getHeaders());
            }
            else{

                //response =  webResource.type("application/x-www-form-urlencoded").accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, queryParams);
                response =  webResource.type("application/x-www-form-urlencoded").accept(MediaType.APPLICATION_JSON).post(ClientResponse.class, queryParams);
                //response =  webResource.accept("application/json").post(ClientResponse.class, queryParams);
            }
            //_logger.info("RESPONSE: " + response);

            if(response.getStatus() != 200) {
                throw new NdlaServiceException("Failed contacting SOLR index server for POST : HTTP Error code: "+response.getStatus());
            }


            if(endpoint.equals("/api/topics/user/login")){
            this.loginResource = webResource;
            }
        }
        catch (Exception e) {
            throw new NdlaServiceException(e.getMessage());
        }
        return response;
    }

    public ClientResponse jerseyRestClientDelete(String endpoint,String id) throws NdlaServiceException {
        String output = "";
        ClientResponse response = null;

        try{
            //base web resource
            WebResource webResource = this.client.resource(this.host+endpoint + "/" + id);

            //delete
            WebResource.Builder builder = webResource.getRequestBuilder();

            String session_name = this.userObject.getString("session_name");
            String sessid = this.userObject.getString("sessid");
            //_logger.info("SESNAME: "+session_name+" SESSID: "+sessid);

            NewCookie c = new NewCookie(session_name,sessid);
            builder = builder.cookie(c);
            //_logger.info("COOKIE; "+c.getName());

            String token =  indexServiceGetToken();
            //_logger.info("TOKEN: "+token);

            builder = builder.header("X-CSRF-Token",token);

            response = builder.accept(MediaType.APPLICATION_JSON).delete(ClientResponse.class);
            //_logger.info("BUILDER: "+response.getHeaders());

            //ClientResponse response =  webResource.path(endpoint).delete(ClientResponse.class);
            if(response.getStatus() != 200) {
                throw new NdlaServiceException("Failed contacting SOLR index server for DELETE : HTTP Error code: "+response.getStatus());
            }
        }
        catch (Exception e) {
            throw new NdlaServiceException(e.getMessage());
        }
        return response;
    }


  public boolean commitIndex(String index) throws NdlaServiceException{
    boolean success = false;
    ClientResponse response = null;

    String tokenEndPoint = "/solr/"+index+"/update?commit=true";
    String solrHost = this.solrHost;
    String commitEndPoint = solrHost+tokenEndPoint;

    try{
      WebResource webResource = this.client.resource(commitEndPoint);
      WebResource.Builder builder = webResource.getRequestBuilder();
      String session_name = this.userObject.getString("session_name");
      String sessid = this.userObject.getString("sessid");
      NewCookie c = new NewCookie(session_name,sessid);
      builder = builder.cookie(c);
      String token =  indexServiceGetToken();
      builder = builder.header("X-CSRF-Token",token);
      response = builder.accept(MediaType.APPLICATION_XML).get(ClientResponse.class);
      String response_string = response.getEntity(String.class);
      

      if(response.getStatus() != 200) {
        throw new NdlaServiceException("Failed contacting SOLR index server for GET: commit : HTTP Error code: "+response.getStatus());
      }

      if(response.getStatus() == 200) {
        success = true;
      }
    }
    catch (Exception e) {
      throw new NdlaServiceException(e.getMessage());
    }
    return success;
  }
}
