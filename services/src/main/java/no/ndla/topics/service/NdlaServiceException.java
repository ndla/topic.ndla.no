/*  Implemented by Brett Kromkamp (brett@seria.no)
    Date: February 08, 2013
 */

package no.ndla.topics.service;

public class NdlaServiceException extends Exception {
    public NdlaServiceException() {
        super();
    }

    public NdlaServiceException(String message) {
        super(message);
    }

    public NdlaServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public NdlaServiceException(Throwable cause) {
        super(cause);
    }
}