/*  Implemented by Brett Kromkamp (brett@seria.no)
    Date: February 11, 2013
 */

package no.ndla.topics.service;

public enum NdlaSite {
    NDLA,
    DELING,
    NYGIV
}
