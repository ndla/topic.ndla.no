/*  Implemented by Brett Kromkamp (brett@seria.no)
    Date: February 08, 2013
 */

package no.ndla.topics.service;

import com.sun.jersey.api.client.ClientResponse;
import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.infoset.impl.basic.URILocator;
import net.ontopia.topicmaps.core.*;
import net.ontopia.topicmaps.entry.TopicMapReferenceIF;
import net.ontopia.topicmaps.entry.TopicMapRepositoryIF;
import net.ontopia.topicmaps.entry.XMLConfigSource;
import net.ontopia.topicmaps.impl.utils.DeletionUtils;
import net.ontopia.topicmaps.query.core.*;
import net.ontopia.topicmaps.query.utils.QueryUtils;
import no.ndla.topics.service.model.NdlaLanguage;
import no.ndla.topics.service.model.NdlaTopic;
import no.ndla.topics.service.model.NdlaWord;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.util.*;

import java.sql.Timestamp;

public class TopicService {

    private static final String NDLA_SITE_URL = "http://ndla.no/";
    private static final String DELING_SITE_URL = "http://deling.ndla.no/";
    private static final String NYGIV_SITE_URL = "http://nygiv.ndla.no/";
    private static final String FYR_SITE_URL = "http://fyr.ndla.no/";

    private String _indexHostUrl;
    private String _indexSOLRHostUrl;
    private String _indexHostUrlUser;
    private String _indexHostUrlPass;
    private String _configFile;
    private String _topicmapReferenceKey;

    private HashMap<String,HashMap<String,String>> languages;

    private Logger _logger = LoggerFactory.getLogger(TopicService.class);

    // Constructor(s).

    public TopicService(final String configFile, final String topicmapReferenceKey) {
        this._configFile = configFile;
        this._topicmapReferenceKey = topicmapReferenceKey;
    }

    public TopicService(final String configFile, final String topicmapReferenceKey, String indexServer, String indexUser, String indexPass) {
        this._indexHostUrl = indexServer;
        this._indexHostUrlUser = indexUser;
        this._indexHostUrlPass = indexPass;

        this._configFile = configFile;
        this._topicmapReferenceKey = topicmapReferenceKey;
    }

    public TopicService(final String configFile, final String topicmapReferenceKey, String indexServer, String solrServer, String indexUser, String indexPass) {
      this._indexHostUrl = indexServer;
      this._indexSOLRHostUrl = solrServer;
      this._indexHostUrlUser = indexUser;
      this._indexHostUrlPass = indexPass;

      this._configFile = configFile;
      this._topicmapReferenceKey = topicmapReferenceKey;
    }

    // Service methods.

    public String deleteAllKeywordsFromAutoCompleteIndex(String index) throws NdlaServiceException { // TODO: Determine if method needs to synchronized.
        JSONObject jsonMessage = new JSONObject();
        Indexer indexer = new Indexer(_indexHostUrl,_indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
        boolean indexed = indexer.deleteAllFromIndex(index);
        boolean commitSuccess = indexer.commitIndex(index);
        if (indexed && commitSuccess) {
            try {
                jsonMessage.put("success", "true");
                if(index == "keywords"){
                  jsonMessage.put("message", "All keywords were deleted successfully from the index");
                }
                else if(index == "topics"){
                  jsonMessage.put("message", "All topics were deleted successfully from the index");
                }


            } catch (JSONException je) {
                throw new NdlaServiceException(je.getMessage());
            }

        }
        return jsonMessage.toString();
    }

    public String reIndexAllKeywords(String site) throws NdlaServiceException, BadObjectReferenceException { // TODO: Determine if method needs to synchronized.
        JSONObject jsonMessage = new JSONObject();
        Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
        ClientResponse postResponse = null;
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;


        boolean done = false;
        int offset = 0;
        int limit = 200;
        int ndlaCountObject = 0;
        int nygivCountObject = 0;
        int delingCountObject = 0;

        int processedKeywords = 0;

        switch (site) {
            case "NDLA":
                ndlaCountObject = (int) executeObjectQuery("select count($COLUMN) from instance-of($COLUMN,keyword), originating-site($COLUMN,\"http://ndla.no/\")?");
                //_logger.info("NDLACOUNTOBJECT: "+ndlaCountObject);
                break;
            case "DELING":
                delingCountObject = (int) executeObjectQuery("select count($COLUMN) from instance-of($COLUMN,keyword), originating-site($COLUMN,\"http://deling.ndla.no/\")?");
                //_logger.info("DELINGCOUNTOBJECT: "+delingCountObject);
                break;
            case "NYGIV":
                nygivCountObject = (int) executeObjectQuery("select count($COLUMN) from instance-of($COLUMN,keyword), originating-site($COLUMN,\"http://nygiv.ndla.no/\")?");
                //_logger.info("NYGIVCOUNTOBJECT: "+nygivCountObject);
                break;
        }

        ArrayList<NdlaTopic> keywords = null;
        // process ndlakeywords
        indexer.indexServiceLogin(_indexHostUrlUser, _indexHostUrlPass);
        try {
          store = reference.createStore(false);
          TopicMapIF topicMap = store.getTopicMap();
            do {
                int keywordcount = 0;

                if (ndlaCountObject > 0) {
                    keywords = getKeywords(limit, (limit * offset), NdlaSite.NDLA);
                } else if (delingCountObject > 0) {
                    keywords = getKeywords(limit, (limit * offset), NdlaSite.DELING);
                } else if (nygivCountObject > 0) {
                    keywords = getKeywords(limit, (limit * offset), NdlaSite.NYGIV);
                }

                JSONArray payloadArray = new JSONArray();

                for (NdlaTopic keyword : keywords) {
                  /*
                  jsonKeyword.put("topicId", keyword.getIdentifier());
                  jsonKeyword.put("psi", keyword.getPsi());
                  jsonKeyword.put("originatingSite", keyword.getOriginatingSite());
                  jsonKeyword.put("visibility", keyword.getVisibility());

                  String approved = keyword.getApproved();
                  String approval_date = keyword.getApprovalDate();
                  if(null != approved && approved.length() > 0){
                    if(approved.equals("false")) {
                      jsonKeyword.put("approved","false");
                    }
                    else{
                      jsonKeyword.put("approved",approved);
                      if(approval_date.length() > 0) {
                        jsonKeyword.put("approval_date",approval_date);
                      }
                    }
                  }
                  else{
                    jsonKeyword.put("approved","false");
                  }

                  JSONArray jsonNames = new JSONArray();
                  ArrayList<NdlaWord> wordNames = keyword.getWordNames();
                  ArrayList<String> wordclasses = new ArrayList<>();
                  for (NdlaWord wordName : wordNames) {
                      String wordClass = wordName.getWordClass();
                      JSONObject currName = new JSONObject();

                      if (!wordclasses.contains(wordClass)) {
                          wordclasses.add(wordClass);
                          currName.put("wordclass", wordClass);
                          currName.put("data", resolveNamesByWordClass(wordClass, wordNames));
                      }

                      if (currName.has("wordclass")) {
                          jsonNames.put(currName);
                      }
                  }//end for wordNames

                  jsonKeyword.put("names", jsonNames);
                  */
                  String psi = keyword.getPsi();
                  if(psi == ""){
                    ArrayList<String> psis = keyword.getPsis();
                    for(String psiTemp :  psis){
                      if(psiTemp.contains("#topic")){
                        psi = psiTemp;
                        break;
                      }
                    }
                  }

                  JSONArray indexSet = Utils.buildIndexJSONArray(psi, topicMap);
                  JSONObject jsonKeyword = (JSONObject) indexSet.get(0);

                  String processStateValue = keyword.getProcessState();
                  if(processStateValue == ""){
                    processStateValue = "0";
                  }
                  jsonKeyword.put("processState",processStateValue);


                  String approved = keyword.getApproved();
                  String approval_date = keyword.getApprovalDate();
                  if(null != approved && approved.length() > 0){
                    if(approved.equals("false")) {
                      jsonKeyword.put("approved","false");
                    }
                    else{
                      jsonKeyword.put("approved",approved);
                      if(approval_date.length() > 0) {
                        jsonKeyword.put("approval_date",approval_date);
                      }
                    }
                  }
                  else{
                    jsonKeyword.put("approved","false");
                  }

                  payloadArray.put(jsonKeyword);
                  keywordcount++;
              }//end for keywords
              _logger.info(payloadArray.toString());

              offset++;

              if (payloadArray.length() > 0) {
                  boolean postSuccess = false;
                  postSuccess = indexer.postToIndexNoLogin(payloadArray.toString(), true,"keywords");
                  if (!postSuccess) {
                      throw new NdlaServiceException("Something went wrong while updating the autocomplete index for all keywords");
                  }
                  processedKeywords = (processedKeywords + keywordcount);
              } else {
                  done = true;
              }
            } while (keywords.size() > 0);

            if (done) {
              if (processedKeywords > 0) {
                  boolean commitSuccess = indexer.commitIndex("keywords");
                  if(commitSuccess){
                    jsonMessage.put("success", "true");
                    if (ndlaCountObject > 0) {
                      jsonMessage.put("message", "Added " + processedKeywords + " of " + ndlaCountObject + " ndla keywords");
                    } else if (delingCountObject > 0) {
                      jsonMessage.put("message", "Added " + processedKeywords + " of " + delingCountObject + " deling keywords");
                    } else if (nygivCountObject > 0) {
                      jsonMessage.put("message", "Added " + processedKeywords + " of " + nygivCountObject + " nygiv keywords");
                    }
                  }
                  else{
                    jsonMessage.put("message","Something went wrong while comitting the update to the autocomplete index for all keywords");
                  }

              }
            }
        } catch (Exception e) {
          if (store != null) {
            store.abort(); // Roll-back changes and deactivate (top-level) transaction.
            if (e instanceof BadObjectReferenceException) {
              throw new BadObjectReferenceException(e.getMessage());
            } else {
              throw new NdlaServiceException(e.getMessage(),e);
            }
          }
        } finally { // Release all topic map engine resources.
          if (store != null) {
            store.close();
          }
          if (repository != null) {
            repository.close();
          }
        }
        return jsonMessage.toString();
    }

  public String reIndexAllTopics() throws NdlaServiceException, BadObjectReferenceException { // TODO: Determine if method needs to synchronized.
    JSONObject jsonMessage = new JSONObject();
    Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
    ClientResponse postResponse = null;

    boolean done = false;
    int offset = 0;
    int limit = 200;
    int ndlaCountObject = 0;
    int nygivCountObject = 0;
    int delingCountObject = 0;

    int processedTopics = 0;

    ndlaCountObject = (int) executeObjectQuery("select count($COLUMN) from instance-of($COLUMN,topic)?");
    ArrayList<NdlaTopic> topics = null;
    // process ndlakeywords
    indexer.indexServiceLogin(_indexHostUrlUser, _indexHostUrlPass);
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;

    try {
      if(ndlaCountObject > 0){
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap();
        do {
          int topiccount = 0;

          topics = getTopics(limit, (limit * offset));
          JSONArray payloadArray = new JSONArray();


          for (NdlaTopic topic : topics) {
            String psi = topic.getPsi();
            if(psi == ""){
              ArrayList<String> psis = topic.getPsis();
              for(String psiTemp :  psis){
                if(psiTemp.contains("#topic")){
                  psi = psiTemp;
                  break;
                }
              }
            }
            JSONArray indexSet = Utils.buildTopicIndexJSONArray(psi,"",topicMap);
            JSONObject jsonTopic = (JSONObject) indexSet.get(0);

            String processStateValue = topic.getProcessState();
            if(processStateValue == ""){
              processStateValue = "0";
            }
            jsonTopic.put("processState",processStateValue);


            String approved = topic.getApproved();
            String approval_date = topic.getApprovalDate();
            if(null != approved && approved.length() > 0){
              if(approved.equals("false")) {
                jsonTopic.put("approved","false");
              }
              else{
                jsonTopic.put("approved",approved);
                if(approval_date.length() > 0) {
                  jsonTopic.put("approval_date",approval_date);
                }
              }
            }
            else{
              jsonTopic.put("approved","false");
            }


            payloadArray.put(jsonTopic);
            topiccount++;
          }//end for topics

          offset++;

          if (payloadArray.length() > 0) {
            boolean postSuccess = false;
            postSuccess = indexer.postToIndexNoLogin(payloadArray.toString(), true,"topics");
            if (!postSuccess) {
              throw new NdlaServiceException("Something went wrong while updating the autocomplete index for all topics");
            }
            processedTopics = (processedTopics + topiccount);
          } else {
            done = true;
          }
        } while (topics.size() > 0);

        if (done) {
          if (processedTopics > 0) {
            boolean commitSuccess = indexer.commitIndex("topics");
            if(commitSuccess){
              jsonMessage.put("success", "true");
              if (ndlaCountObject > 0) {
                jsonMessage.put("message", "Added " + processedTopics + " of " + ndlaCountObject + " ndla topics");
              } else if (delingCountObject > 0) {
                jsonMessage.put("message", "Added " + processedTopics + " of " + delingCountObject + " deling topics");
              } else if (nygivCountObject > 0) {
                jsonMessage.put("message", "Added " + processedTopics + " of " + nygivCountObject + " nygiv topics");
              }
            }
            else{
              jsonMessage.put("message", "Something went wrong while comitting the update to the autocomplete index for all topics");
            }

          }
        }//end if done
      }//end if hasTopics


    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return jsonMessage.toString();
  }

    private JSONArray resolveNamesByWordClass(String wordClass, ArrayList<NdlaWord> names) throws NdlaServiceException {
        JSONArray result = new JSONArray();
        try {
            for (NdlaWord name : names) {
                String currWordClass = name.getWordClass();
                HashMap<String, String> currentNames = name.getNames();
                Iterator it = currentNames.keySet().iterator();
                while (it.hasNext()) {
                    String lang = (String) it.next();
                    String nameString = currentNames.get(lang);
                    if (currWordClass.equals(wordClass)) {
                        JSONObject nameObj = new JSONObject();
                        nameObj.put(lang, nameString);
                        result.put(nameObj);
                    }
                }
            }
        } catch (JSONException e) {
            throw new NdlaServiceException(e.getMessage(),e);
        }
        return result;
    }

    protected ArrayList<NdlaTopic> executeCollectionQuery(String query) throws NdlaServiceException, BadObjectReferenceException {
        ArrayList<NdlaTopic> result = new ArrayList<NdlaTopic>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            ParsedStatementIF statement;
            QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
            QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

            QueryResultIF tologResult = null;
            try {
                statement = queryProcessor.parse(query);
                if (null != statement) {
                    tologResult = ((ParsedQueryIF) statement).execute();
                    while (tologResult.next()) {
                        result.add(new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), queryProcessor));
                    }
                }
            } catch (BadObjectReferenceException e) {
                throw new BadObjectReferenceException(e.getMessage());
            } catch (InvalidQueryException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            } finally {
                if (null != tologResult) {
                    tologResult.close();
                }
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                if (e instanceof BadObjectReferenceException) {
                    throw new BadObjectReferenceException(e.getMessage());
                } else {
                    throw new NdlaServiceException(e.getMessage(),e);
                }
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    protected ArrayList<String> executeStringCollectionQuery(String query) throws NdlaServiceException, BadObjectReferenceException {
        ArrayList<String> result = new ArrayList<String>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            ParsedStatementIF statement;
            QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
            QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

            QueryResultIF tologResult = null;
            try {
                statement = queryProcessor.parse(query);
                if (null != statement) {
                    tologResult = ((ParsedQueryIF) statement).execute();
                    while (tologResult.next()) {
                        result.add((String) tologResult.getValue("COLUMN"));
                    }
                }
            } catch (BadObjectReferenceException e) {
                throw new BadObjectReferenceException(e.getMessage());
            } catch (InvalidQueryException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            } finally {
                if (null != tologResult) {
                    tologResult.close();
                }
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                if (e instanceof BadObjectReferenceException) {
                    throw new BadObjectReferenceException(e.getMessage());
                } else {
                    throw new NdlaServiceException(e.getMessage(),e);
                }
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    protected Object executeObjectQuery(String query) throws NdlaServiceException, BadObjectReferenceException {
        Object result = null;

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            ParsedStatementIF statement = null;
            QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
            QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

            QueryResultIF tologResult = null;
            try {
                statement = queryProcessor.parse(query);
                if (null != statement) {
                    tologResult = ((ParsedQueryIF) statement).execute();
                    if (tologResult.next()) {
                        result = tologResult.getValue("COLUMN");
                    }
                    else {
                      result = null;
                    }
                }
            } catch (BadObjectReferenceException e) {
                throw new BadObjectReferenceException(e.getMessage());
            } catch (InvalidQueryException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            } finally {
                if (null != tologResult) {
                    tologResult.close();
                }
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                if (e instanceof BadObjectReferenceException) {
                    throw new BadObjectReferenceException(e.getMessage());
                } else {
                    throw new NdlaServiceException(e.getMessage(),e);
                }
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }


  protected ArrayList<Object> executeObjectCollectionQuery(String query) throws NdlaServiceException, BadObjectReferenceException {
    ArrayList<Object> result = new ArrayList<Object>();

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      ParsedStatementIF statement;
      QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
      QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

      QueryResultIF tologResult = null;
      try {
        statement = queryProcessor.parse(query);
        if (null != statement) {
          tologResult = ((ParsedQueryIF) statement).execute();
          while (tologResult.next()) {
            result.add(tologResult.getValue("COLUMN"));
          }
        }
      } catch (BadObjectReferenceException e) {
        throw new BadObjectReferenceException(e.getMessage());
      } catch (InvalidQueryException e) {
        throw new NdlaServiceException(e.getMessage());
      } finally {
        if (null != tologResult) {
          tologResult.close();
        }
      }
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage());
        }
      }
    }
    return result;
  }

    // **** Admin functions **** //

    /* SubordinateRoleTypes */
    public JSONArray getSubordinateRoleTypes() throws NdlaServiceException, BadObjectReferenceException {
      JSONArray JSONRoleTypes = new JSONArray();
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.
        String query = String.format("select $ASSOC, $COLUMN from instance-of($COLUMN,subordinate-role-type),scope($n,$COLUMN), topic-name($ASSOC,$n) ORDER BY $COLUMN?\n");
        System.out.println(query);
        ParsedStatementIF statement = null;
        QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
        QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

        ArrayList<NdlaTopic> roleList = new ArrayList<>();
        ArrayList<NdlaTopic> assocList = new ArrayList<>();

        QueryResultIF tologResult = null;
        try {
          statement = queryProcessor.parse(query);
          if (null != statement) {
            tologResult = ((ParsedQueryIF) statement).execute();
            while (tologResult.next()) {
              roleList.add( new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), null));
              assocList.add( new NdlaTopic((TopicIF) tologResult.getValue("ASSOC"), null));
            }
          }

        } catch (BadObjectReferenceException e) {
          throw new BadObjectReferenceException(e.getMessage());
        } catch (InvalidQueryException e) {
          throw new NdlaServiceException(e.getMessage());
        } finally {
          if (null != tologResult) {
            tologResult.close();
          }
        }


        String assocIdentifier = "";
        String roleIdentifier = "";
        int counter = 0;
        for(NdlaTopic role : roleList){
          System.out.println("role: "+role  );
          JSONObject roleObject = new JSONObject();
          NdlaTopic assocTopic = assocList.get(counter);
          roleIdentifier = role.getIdentifier();
          roleObject.put("roleIdentifier",roleIdentifier);
          assocIdentifier = assocTopic.getIdentifier();
          ArrayList<NdlaTopic> roleScopeList = new ArrayList<>();
          String roleQuery = String.format("SELECT $COLUMN from scope($N,$COLUMN),topic-name(%s,$N)?\n",assocIdentifier);
          System.out.println(roleQuery);
          ParsedStatementIF roleStatement = null;
          QueryResultIF roleTologResult = null;
          try {
            roleStatement = queryProcessor.parse(roleQuery);
            if (null != roleStatement) {
              roleTologResult = ((ParsedQueryIF) roleStatement).execute();
              while (roleTologResult.next()) {
                roleScopeList.add( new NdlaTopic((TopicIF) roleTologResult.getValue("COLUMN"), null));
              }
            }

          } catch (BadObjectReferenceException e) {
            throw new BadObjectReferenceException(e.getMessage());
          } catch (InvalidQueryException e) {
            throw new NdlaServiceException(e.getMessage());
          } finally {
            if (null != roleTologResult) {
              roleTologResult.close();
            }
          }


          roleObject.put("assocIdentifier",assocIdentifier);
          ArrayList<String> roleIds = new ArrayList<>();
          for(NdlaTopic roleTopic : roleScopeList) {
            if(!roleTopic.getPsi().contains("iso") && !roleTopic.getPsi().contains("language-neutral") && !roleTopic.getIdentifier().equals(roleIdentifier)){
              roleIds.add(roleTopic.getIdentifier());
            }
          }
          System.out.println("assocIdentifier: "+assocIdentifier);
          System.out.println("roleIdentifier: "+roleIdentifier);
          int countObject = 0;
          if(roleIds.size() > 0){
            String assocCountQuery = String.format("select count($COLUMN) from %s($COLUMN : %s, $TOPIC2 : %s)?",assocIdentifier,roleIdentifier,roleIds.get(0));
            //String assocCountQuery = String.format("select count($COLUMN), count($TOPIC2) from %s($COLUMN : %s, $TOPIC2 : %s)?","delingnode-keyword","deling-node","keyword");
            System.out.println(assocCountQuery);
            Object queryObject = executeObjectQuery(assocCountQuery);
            if(queryObject != null){
              countObject = (int) queryObject;
            }
          }
          roleObject.put("relationCount",countObject);
          HashMap<String,String> roleNames = role.getNames();
          JSONArray roleNameArray = new JSONArray();
          Iterator roleNameIterator = roleNames.keySet().iterator();
          while(roleNameIterator.hasNext()) {
            JSONObject roleNameObject = new JSONObject();
            String roleNameLanguage = (String) roleNameIterator.next();
            String roleNameString = roleNames.get(roleNameLanguage);
            roleNameObject.put("language",roleNameLanguage);
            roleNameObject.put("name",roleNameString);
            roleNameArray.put(roleNameObject);
          }
          roleObject.put("roleNames",roleNameArray);

          TreeMap<String,TreeMap<String,String>> assocNames = assocTopic.getRelationshipNames();
          JSONArray assocNameArray = new JSONArray();
          Iterator assocNameIterator = assocNames.keySet().iterator();
          while(assocNameIterator.hasNext()) {
            String assocNameIdentifier = (String) assocNameIterator.next();
            TreeMap<String,String> assocNameMap = assocNames.get(assocNameIdentifier);
            Iterator assocNameMapIterator = assocNameMap.keySet().iterator();
            while(assocNameMapIterator.hasNext()){
              JSONObject assocNameObject = new JSONObject();
              String assocNameLanguage = (String) assocNameMapIterator.next();
              String assocNameString = assocNameMap.get(assocNameLanguage);
              assocNameObject.put("language",assocNameLanguage);
              assocNameObject.put("name",assocNameString);
              assocNameArray.put(assocNameObject);
            }

          }
          roleObject.put("assocNames",assocNameArray);

          HashMap<String,HashMap<String,String>> roleTypes = role.getTypes();
          Iterator roleTypeIterator = roleTypes.keySet().iterator();
          JSONArray roleTypeArray = new JSONArray();
          while(roleTypeIterator.hasNext()){
            JSONObject roleTypeObject = new JSONObject();
            String roleTypeIdentifier = (String) roleTypeIterator.next();
            roleTypeObject.put("roleTypeIdentifier",roleTypeIdentifier);
            HashMap<String,String> roleTypeNames = roleTypes.get(roleTypeIdentifier);
            Iterator roleTypeNameIterator = roleTypeNames.keySet().iterator();
            while(roleTypeNameIterator.hasNext()){
              String roleTypeNameLanguage = (String) roleTypeNameIterator.next();
              String roleTypeNameString = roleTypeNames.get(roleTypeNameLanguage);
              roleTypeObject.put("language",roleTypeNameLanguage);
              roleTypeObject.put("name",roleTypeNameString);
              roleTypeArray.put(roleTypeObject);
            }
          }

          roleObject.put("roleTypes",roleTypeArray);
          JSONRoleTypes.put(roleObject);
          counter++;
        }

      }catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          throw new NdlaServiceException(e.getMessage());
        }
      }
      finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }
      return JSONRoleTypes;
    }

    public NdlaTopic getRoleType(String identifier) throws NdlaServiceException, BadObjectReferenceException {
      NdlaTopic result = null;
      String query = String.format("select $COLUMN from {subject-identifier($COLUMN, \"http://psi.topic.ndla.no/topics/#%s\") |subject-identifier($COLUMN, \"http://psi.topic.ndla.no/#%s\") | subject-identifier($COLUMN, \"http://psi.udir.no/ontologi/%s\") | subject-identifier($COLUMN,\"http://psi.udir.no/ontologi/lkt/%s\") | subject-identifier($COLUMN,\"http://psi.udir.no/ontologi/lkt/#%s\")}?", identifier, identifier, identifier, identifier, identifier);

      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

        ParsedStatementIF statement = null;
        QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
        QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

        QueryResultIF tologResult = null;
        try {
          statement = queryProcessor.parse(query);
          if (null != statement) {
            tologResult = ((ParsedQueryIF) statement).execute();
            if (tologResult.next()) {
              result = new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), queryProcessor);
            }
          }
        } catch (BadObjectReferenceException e) {
          throw new BadObjectReferenceException(e.getMessage());
        } catch (InvalidQueryException e) {
          throw new NdlaServiceException(e.getMessage(),e);
        } finally {
          if (null != tologResult) {
            tologResult.close();
          }
        }
      } catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          if (e instanceof BadObjectReferenceException) {
            throw new BadObjectReferenceException(e.getMessage());
          } else {
            throw new NdlaServiceException(e.getMessage(),e);
          }
        }
      } finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }
      return result;
    }

    /* SuperordinateRoleTypes */

    public JSONArray getSuperordinateRoleTypes() throws NdlaServiceException, BadObjectReferenceException{
      JSONArray JSONRoleTypes = new JSONArray();
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.
        String query = String.format("select $ASSOC, $COLUMN from instance-of($COLUMN,superordinate-role-type),scope($n,$COLUMN), topic-name($ASSOC,$n) ORDER BY $COLUMN?\n");
        ParsedStatementIF statement = null;
        QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
        QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

        ArrayList<NdlaTopic> roleList = new ArrayList<>();
        ArrayList<NdlaTopic> assocList = new ArrayList<>();

        QueryResultIF tologResult = null;
        try {
          statement = queryProcessor.parse(query);
          if (null != statement) {
            tologResult = ((ParsedQueryIF) statement).execute();
            while (tologResult.next()) {
              roleList.add( new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), null));
              assocList.add( new NdlaTopic((TopicIF) tologResult.getValue("ASSOC"), null));
            }
          }

        } catch (BadObjectReferenceException e) {
          throw new BadObjectReferenceException(e.getMessage());
        } catch (InvalidQueryException e) {
          throw new NdlaServiceException(e.getMessage());
        } finally {
          if (null != tologResult) {
            tologResult.close();
          }
        }



        int counter = 0;
        String assocIdentifier = "";
        for(NdlaTopic role : roleList){
          JSONObject roleObject = new JSONObject();
          NdlaTopic assocTopic = assocList.get(counter);
          String roleIdentifier = role.getIdentifier();
          roleObject.put("roleIdentifier", roleIdentifier);
          assocIdentifier = assocTopic.getIdentifier();
          roleObject.put("assocIdentifier",assocIdentifier);
          ArrayList<NdlaTopic> roleScopeList = new ArrayList<>();
          String roleQuery = String.format("SELECT $COLUMN from scope($N,$COLUMN),topic-name(%s,$N)?\n",assocIdentifier);
          ParsedStatementIF roleStatement = null;
          QueryResultIF roleTologResult = null;
          try {
            roleStatement = queryProcessor.parse(roleQuery);
            if (null != roleStatement) {
              roleTologResult = ((ParsedQueryIF) roleStatement).execute();
              while (roleTologResult.next()) {
                roleScopeList.add( new NdlaTopic((TopicIF) roleTologResult.getValue("COLUMN"), null));
              }
            }

          } catch (BadObjectReferenceException e) {
            throw new BadObjectReferenceException(e.getMessage());
          } catch (InvalidQueryException e) {
            throw new NdlaServiceException(e.getMessage());
          } finally {
            if (null != roleTologResult) {
              roleTologResult.close();
            }
          }


          roleObject.put("assocIdentifier",assocIdentifier);
          ArrayList<String> roleIds = new ArrayList<>();
          for(NdlaTopic roleTopic : roleScopeList) {
            if(!roleTopic.getPsi().contains("iso") && !roleTopic.getPsi().contains("language-neutral") && !roleTopic.getIdentifier().equals(roleIdentifier)){
              roleIds.add(roleTopic.getIdentifier());
            }
          }

          int countObject = 0;
          if(roleIds.size() > 0){
            String assocCountQuery = String.format("select count($COLUMN) from %s($COLUMN : %s, $TOPIC2 : %s)?",assocIdentifier,roleIdentifier,roleIds.get(0));
            //String assocCountQuery = String.format("select count($COLUMN), count($TOPIC2) from %s($COLUMN : %s, $TOPIC2 : %s)?","delingnode-keyword","deling-node","keyword");
            Object queryObject = executeObjectQuery(assocCountQuery);
            if(queryObject != null){
              countObject = (int) queryObject;
            }
          }
          roleObject.put("relationCount",countObject);
          HashMap<String,String> roleNames = role.getNames();
          JSONArray roleNameArray = new JSONArray();
          Iterator roleNameIterator = roleNames.keySet().iterator();
          while(roleNameIterator.hasNext()) {
            JSONObject roleNameObject = new JSONObject();
            String roleNameLanguage = (String) roleNameIterator.next();
            String roleNameString = roleNames.get(roleNameLanguage);
            roleNameObject.put("language",roleNameLanguage);
            roleNameObject.put("name",roleNameString);
            roleNameArray.put(roleNameObject);
          }
          roleObject.put("roleNames",roleNameArray);

          TreeMap<String,TreeMap<String,String>> assocNames = assocTopic.getRelationshipNames();
          JSONArray assocNameArray = new JSONArray();
          Iterator assocNameIterator = assocNames.keySet().iterator();
          while(assocNameIterator.hasNext()) {
            String assocNameIdentifier = (String) assocNameIterator.next();
            TreeMap<String,String> assocNameMap = assocNames.get(assocNameIdentifier);
            Iterator assocNameMapIterator = assocNameMap.keySet().iterator();
            while(assocNameMapIterator.hasNext()){
              JSONObject assocNameObject = new JSONObject();
              String assocNameLanguage = (String) assocNameMapIterator.next();
              String assocNameString = assocNameMap.get(assocNameLanguage);
              assocNameObject.put("language",assocNameLanguage);
              assocNameObject.put("name",assocNameString);
              assocNameArray.put(assocNameObject);
            }

          }
          roleObject.put("assocNames",assocNameArray);

          HashMap<String,HashMap<String,String>> roleTypes = role.getTypes();
          Iterator roleTypeIterator = roleTypes.keySet().iterator();
          JSONArray roleTypeArray = new JSONArray();
          while(roleTypeIterator.hasNext()){
            JSONObject roleTypeObject = new JSONObject();
            String roleTypeIdentifier = (String) roleTypeIterator.next();
            roleTypeObject.put("roleTypeIdentifier",roleTypeIdentifier);
            HashMap<String,String> roleTypeNames = roleTypes.get(roleTypeIdentifier);
            Iterator roleTypeNameIterator = roleTypeNames.keySet().iterator();
            while(roleTypeNameIterator.hasNext()){
              String roleTypeNameLanguage = (String) roleTypeNameIterator.next();
              String roleTypeNameString = roleTypeNames.get(roleTypeNameLanguage);
              roleTypeObject.put("language",roleTypeNameLanguage);
              roleTypeObject.put("name",roleTypeNameString);
              roleTypeArray.put(roleTypeObject);
            }
          }

          roleObject.put("roleTypes",roleTypeArray);
          JSONRoleTypes.put(roleObject);
          counter++;
        }

      }catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          throw new NdlaServiceException(e.getMessage());
        }
      }
      finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }
      return JSONRoleTypes;
    }


    /* HierarchicalAssociationTypes */
    public ArrayList<NdlaTopic> getHierarchicalAssociationTypes() throws NdlaServiceException, BadObjectReferenceException {
        String query = String.format("instance-of($COLUMN, hierarchical-relation-type) order by $COLUMN?");
        return executeCollectionQuery(query);
    }

    public NdlaTopic getHierarchicalAssociationType(String identifier) throws NdlaServiceException, BadObjectReferenceException {
        NdlaTopic result = null;
        String query = String.format("select $COLUMN from {subject-identifier($COLUMN, \"http://psi.topic.ndla.no/#%s\") | subject-identifier($COLUMN, \"http://psi.udir.no/ontologi/%s\") | subject-identifier($COLUMN,\"http://psi.udir.no/ontologi/lkt/%s\") | subject-identifier($COLUMN,\"http://psi.udir.no/ontologi/lkt/#%s\")}?", identifier, identifier, identifier, identifier);

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            ParsedStatementIF statement = null;
            QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
            QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

            QueryResultIF tologResult = null;
            try {
                statement = queryProcessor.parse(query);
                if (null != statement) {
                    tologResult = ((ParsedQueryIF) statement).execute();
                    if (tologResult.next()) {
                        result = new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), queryProcessor);
                    }
                }
            } catch (BadObjectReferenceException e) {
                throw new BadObjectReferenceException(e.getMessage());
            } catch (InvalidQueryException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            } finally {
                if (null != tologResult) {
                    tologResult.close();
                }
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                if (e instanceof BadObjectReferenceException) {
                    throw new BadObjectReferenceException(e.getMessage());
                } else {
                    throw new NdlaServiceException(e.getMessage(),e);
                }
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    /* HorizontalAssociationTypes */

    public ArrayList<NdlaTopic> getHorizontalAssociationTypes() throws NdlaServiceException, BadObjectReferenceException {
        String query = String.format("instance-of($COLUMN,horisontal-relation-type) ORDER BY $COLUMN?");
        return executeCollectionQuery(query);
    }

    synchronized public NdlaTopic getHorizontalAssociationType(String identifier) throws NdlaServiceException, BadObjectReferenceException {
      NdlaTopic result = null;
      String query = String.format("select $COLUMN from {subject-identifier($COLUMN, \"http://psi.topic.ndla.no/#%s\") | subject-identifier($COLUMN, \"http://psi.udir.no/ontologi/%s\")| subject-identifier($COLUMN,\"http://psi.udir.no/ontologi/lkt/%s\") | subject-identifier($COLUMN,\"http://psi.udir.no/ontologi/lkt/#%s\")}?", identifier, identifier, identifier, identifier);

      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try {
          store = reference.createStore(false);
          TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

          ParsedStatementIF statement = null;
          QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
          QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

          QueryResultIF tologResult = null;
          try {
              statement = queryProcessor.parse(query);
              if (null != statement) {
                  tologResult = ((ParsedQueryIF) statement).execute();
                  if (tologResult.next()) {
                      result = new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), queryProcessor);
                  }
              }
          } catch (BadObjectReferenceException e) {
              throw new BadObjectReferenceException(e.getMessage());
          } catch (InvalidQueryException e) {
              throw new NdlaServiceException(e.getMessage());
          } finally {
              if (null != tologResult) {
                  tologResult.close();
              }
          }
      } catch (Exception e) {
          if (store != null) {
              store.abort(); // Roll-back changes and deactivate (top-level) transaction.
              if (e instanceof BadObjectReferenceException) {
                  throw new BadObjectReferenceException(e.getMessage());
              } else {
                  throw new NdlaServiceException(e.getMessage());
              }
          }
      } finally { // Release all topic map engine resources.
          if (store != null) {
              store.close();
          }
          if (repository != null) {
              repository.close();
          }
      }
      return result;
    }


    /* ASSOCTYPES */

  /**
   *
   * @param data JSON Payload
   * @return HashMap <id, topic>
   * @throws NdlaServiceException
   * @throws BadObjectReferenceException
   */
    public HashMap<String,TopicIF> createAssociationTypes(String data)throws NdlaServiceException, BadObjectReferenceException{
      HashMap<String,TopicIF> result = new HashMap<>();
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

        TopicMapBuilderIF builder = topicMap.getBuilder();
        LocatorIF baseAddress = store.getBaseAddress();

        JSONObject payload = new JSONObject(data);
        JSONArray relationTypes = payload.getJSONArray("relationTypes");

        for(int i = 0; i < relationTypes.length(); i ++) {
          JSONObject relationType = relationTypes.getJSONObject(i);
          String dataName = relationType.getString("data-name");
          String relationTypeType = relationType.getString("relationType-type");
          JSONArray names = relationType.getJSONArray("names");


          TopicIF newRelationTypeTopic = builder.makeTopic();

          newRelationTypeTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + dataName));
          newRelationTypeTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + dataName));
          TopicIF relationshipType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#"+relationTypeType));
          newRelationTypeTopic.addType(relationshipType);

          for(int j = 0; j < names.length(); j++) {
            JSONObject nameObject = names.getJSONObject(j);
            String language = nameObject.getString("language");
            String nameString = nameObject.getString("name");
            JSONArray roleScopes = nameObject.getJSONArray("role-topics");
            ArrayList<TopicIF> roleTopics = new ArrayList<TopicIF>();

            for(int k = 0; k < roleScopes.length(); k++){
              JSONObject roleObject = roleScopes.getJSONObject(k);
              String roleTopicPSI = roleObject.getString("roleTopicPsi");

              JSONArray roleNames = roleObject.getJSONArray("names");
              if(roleNames.length() > 0) {
                //new role topic
                TopicIF newRoleTopic = builder.makeTopic();
                newRoleTopic.addSubjectIdentifier(new URILocator(roleTopicPSI));
                String roleTopicId = roleTopicPSI.substring(roleTopicPSI.lastIndexOf("#")+1);
                newRoleTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + roleTopicId));

                String roleTopicType = roleObject.getString("roleTopicType");
                TopicIF roleTopicTypeTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(roleTopicType));
                newRoleTopic.addType(roleTopicTypeTopicIF);

                for(int l = 0; l < roleNames.length(); l++) {
                  JSONObject roleNameObject = roleNames.getJSONObject(l);
                  String roleNameLanguage = roleNameObject.getString("language");
                  String roleNameString = roleNameObject.getString("name");
                  Utils.createBasename(newRoleTopic,roleNameString,roleNameLanguage,topicMap);

                }
                roleTopics.add(newRoleTopic);
                Utils.createBasename(newRelationTypeTopic,nameString,language,roleTopics,topicMap);
              }
              else{

                TopicIF roleTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(roleTopicPSI));
                roleTopics.add(roleTopicIF);
                Utils.createBasename(newRelationTypeTopic,nameString,language,roleTopics,topicMap);
              }
            }//end for roles
          }//end for names
          result.put(dataName,newRelationTypeTopic);
        }//end for relationTypes
        store.commit();
      }
      catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          throw new NdlaServiceException(e.getMessage());
        }
      }
      finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }
      return result;
    }


    public HashMap<String,TopicIF> updateAssociationTypeByAssocTypeId(String data)throws NdlaServiceException, BadObjectReferenceException{
      HashMap<String,TopicIF> result = new HashMap<>();
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

        TopicMapBuilderIF builder = topicMap.getBuilder();
        LocatorIF baseAddress = store.getBaseAddress();


        JSONObject payload = new JSONObject(data);
        String topicId = payload.getString("topicId");
        LocatorIF psi = new URILocator(topicId);

        Utils.deleteBasenames(psi, topicMap);
        Utils.deleteAllTypes(psi, topicMap);

        TopicIF associationTypeTopic = topicMap.getTopicBySubjectIdentifier(psi);

        JSONArray relationTypeIds = payload.getJSONArray("relationTypeIds");
        for (int j = 0; j < relationTypeIds.length(); j++) {
          String relationTypeId = relationTypeIds.getString(j);
          LocatorIF relationTypePsi = new URILocator(relationTypeId);
          TopicIF relationTypeTopic = topicMap.getTopicBySubjectIdentifier(relationTypePsi);
          associationTypeTopic.addType(relationTypeTopic);
        }

        JSONArray names = payload.getJSONArray("names");

        for(int i = 0; i < names.length(); i++) {
          JSONObject nameObject = names.getJSONObject(i);
          String language = nameObject.getString("language");
          String nameString = nameObject.getString("name");
          String roleTopicPsi = nameObject.getString("roleTopicPsi");
          ArrayList<TopicIF> roleTopics = new ArrayList<TopicIF>();
          TopicIF roleTopicIF = null;
          if(!roleTopicPsi.contains("http") || roleTopicPsi.contains("/topic/")){
            roleTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#"+roleTopicPsi));
          }
          else{
            roleTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(roleTopicPsi));
          }

          roleTopics.add(roleTopicIF);
          Utils.createBasename(associationTypeTopic,nameString,language,roleTopics,topicMap);
        }

        store.commit();
      }
      catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          throw new NdlaServiceException(e.getMessage());
        }
      }
      finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }
      return result;
    }



  public ArrayList<String> deleteAssociationTypeByTopicId(String identifiers) throws NdlaServiceException, BadObjectReferenceException {
    ArrayList<String> deletedTopics = new ArrayList<>();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.
      JSONArray payload = new JSONArray(identifiers);

      for (int i = 0; i < payload.length(); i++) {
        String identifier = payload.getString(i);

        TopicIF topicToBeDeleted = null;
        try {
          topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator(identifier));
          if (null != topicToBeDeleted) {
            String psi = topicToBeDeleted.getSubjectIdentifiers().iterator().next().getAddress();
            deletedTopics.add(psi);
            topicToBeDeleted.remove();
          }
        } catch (MalformedURLException e) {
          throw new NdlaServiceException(e.getMessage());
        }
        store.commit(); // Persist object graph.
      }
      //delete from index

    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage());
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }

    return deletedTopics;
  }


    /* WordClasses */

    public ArrayList<NdlaTopic> getWordClasses() throws NdlaServiceException, BadObjectReferenceException {
        String query = String.format("instance-of($COLUMN,word-class) ORDER BY $COLUMN?");
        return executeCollectionQuery(query);
    }

    synchronized public NdlaTopic getLanguage(String id) throws NdlaServiceException, BadObjectReferenceException {
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      NdlaTopic language = null;
      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

        ParsedStatementIF statement = null;
        QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
        QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);
        String psi = "http://psi.oasis-open.org/iso/639/#"+id;
        NdlaLanguage ndlaLanguage = new NdlaLanguage(queryProcessor);
        language = ndlaLanguage.getLanguage(psi);
      } catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          if (e instanceof BadObjectReferenceException) {
            throw new BadObjectReferenceException(e.getMessage());
          } else {
            throw new NdlaServiceException(e.getMessage(),e);
          }
        }
      } finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }
      return language;
    }

    synchronized public ArrayList<NdlaTopic> getLanguages() throws NdlaServiceException, BadObjectReferenceException {
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      ArrayList<NdlaTopic> languages = null;
      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

        ParsedStatementIF statement = null;
        QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
        QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

        NdlaLanguage ndlaLanguage = new NdlaLanguage(queryProcessor);
        languages = ndlaLanguage.getLanguages();
      } catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          if (e instanceof BadObjectReferenceException) {
            throw new BadObjectReferenceException(e.getMessage());
          } else {
            throw new NdlaServiceException(e.getMessage(),e);
          }
        }
      } finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }
      return languages;
    }

  public TopicIF createLanguage(String data)throws NdlaServiceException, BadObjectReferenceException{
    TopicIF languageTopic = null;

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = store.getBaseAddress();


      JSONObject payload = new JSONObject(data);
      TopicIF checkDuplicateLanguage = null;
      String languageId = payload.getString("languageId");
      checkDuplicateLanguage = Utils.isDuplicateTopic(languageId,topicMap);

      if(null == checkDuplicateLanguage) {
        String topicId = languageId.substring(languageId.lastIndexOf("#")+1);
        languageTopic = builder.makeTopic();

        TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#language"));
        languageTopic.addType(type);

        languageTopic.addSubjectIdentifier(new URILocator(languageId));
        languageTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));

        JSONArray names = payload.getJSONArray("names");
        for (int i = 0; i < names.length(); i++) {
          JSONObject langName = names.getJSONObject(i);

          String langNameLang = langName.getString("language-id");
          String langNameString = langName.getString("name");
          // Names and scopes
          Utils.createBasename(languageTopic, langNameString, langNameLang, topicMap);
        }
      }
      store.commit();
      } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return languageTopic;
  }

  public TopicIF updateLanguage(String data)throws NdlaServiceException, BadObjectReferenceException {
    TopicIF languageTopic = null;

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = store.getBaseAddress();


      JSONObject payload = new JSONObject(data);
      String languageId = payload.getString("languageId");
      LocatorIF psi = new URILocator(languageId);
      languageTopic = topicMap.getTopicBySubjectIdentifier(psi);


      if (null != languageTopic) {
        Utils.deleteBasenames(psi, topicMap);
        JSONArray names = payload.getJSONArray("names");
        for (int i = 0; i < names.length(); i++) {
          JSONObject langName = names.getJSONObject(i);

          String langNameLang = langName.getString("language-id");
          String langNameString = langName.getString("name");
          // Names and scopes
          Utils.createBasename(languageTopic, langNameString, langNameLang, topicMap);
        }
      }
      store.commit();
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return languageTopic;
  }


  /**
   * 0 = «Not processed - Ikke behandlet»
   * 1 = «Pending - Under behandling»
   * 2 = «Processed - Ferdig behandlet»
   * @param psi
   * @param state
   * @param index
   * @return JSONObject message
   * @throws NdlaServiceException
   * @throws BadObjectReferenceException
   */

  public JSONObject setProcessState(String psi, String state, String index) throws NdlaServiceException, BadObjectReferenceException{
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    boolean indexed = false;
    JSONObject jsonMessage = new JSONObject();
    try{
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      TopicMapBuilderIF builder = topicMap.getBuilder();
      TopicIF topic = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
      LocatorIF processLocator = new URILocator("http://psi.topic.ndla.no/#process-state");
      Utils.deleteOccurrencesByType(new URILocator(psi),processLocator,topicMap);


      TopicIF processOcctype = topicMap.getTopicBySubjectIdentifier(processLocator);
      builder.makeOccurrence(topic, processOcctype, state);


      store.commit(); // Persist object graph.

      JSONArray indexSet = null;
      if(index.equals("keywords")) {
        indexSet = Utils.buildIndexJSONArray(psi, topicMap);
      }
      else if(index.equals("topics")){
        indexSet = Utils.buildTopicIndexJSONArray(psi,"",topicMap);
      }

      if(indexSet.length() > 0) {
        JSONObject temporaryPayload = (JSONObject) indexSet.get(0);
        temporaryPayload.put("processState",state);
        indexSet.put(0,temporaryPayload);
      }

      JSONObject indexPayloadObject = new JSONObject();
      String topicId = psi.substring(psi.lastIndexOf("#")+1);
      indexPayloadObject.put(topicId,indexSet);

      JSONArray indexPayloadArray = null;
      if(index.equals("keywords")) {
        indexPayloadArray = Utils.indexPayload(indexPayloadObject, "", topicMap);
      }
      else if(index.equals("topics")) {
        indexPayloadArray = indexSet;
      }

      if (indexPayloadArray.length() > 0) {
        Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
        indexed = indexer.postToIndex(indexPayloadArray.toString(), false,index);
      }

      if(indexed) {
        jsonMessage.put("success", "The topic processing state was set to: "+state);
      }
      else{
        jsonMessage.put("error", "The topic processing state could not be set");
      }
    }
    catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return jsonMessage;
  }


  synchronized public JSONObject approveTopic(String psi, java.util.Date date, String index) throws NdlaServiceException, BadObjectReferenceException{
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    JSONObject jsonMessage = new JSONObject();
    boolean indexed = false;

      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap();
        TopicMapBuilderIF builder = topicMap.getBuilder();

        TopicIF topic = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
        disApproveTopic(psi,topicMap);

        TopicIF approvedOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
        builder.makeOccurrence(topic, approvedOcctype, "true");

        Timestamp currentTimestamp= new Timestamp(date.getTime());
        TopicIF approvalDateOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));
        builder.makeOccurrence(topic, approvalDateOcctype, currentTimestamp.toString());

        store.commit(); // Persist object graph.

        setProcessState(psi,"2",index);

        JSONArray indexSet = null;
        if(index.equals("keywords")) {
          indexSet = Utils.buildIndexJSONArray(psi, topicMap);
        }
        else if(index.equals("topics")){
          indexSet = Utils.buildTopicIndexJSONArray(psi,"",topicMap);
        }

        if(indexSet.length() > 0) {
          JSONObject temporaryPayload = (JSONObject) indexSet.get(0);
          temporaryPayload.put("approved","true");
          temporaryPayload.put("approval_date",currentTimestamp.toString());
          indexSet.put(0,temporaryPayload);
        }

        JSONObject indexPayloadObject = new JSONObject();
        String topicId = psi.substring(psi.lastIndexOf("#")+1);
        indexPayloadObject.put(topicId,indexSet);

        JSONArray indexPayloadArray = null;
        if(index.equals("keywords")) {
          indexPayloadArray = Utils.indexPayload(indexPayloadObject, "", topicMap);
        }
        else if(index.equals("topics")) {
          indexPayloadArray = indexSet;
        }

        if (indexPayloadArray.length() > 0) {
          Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
          indexed = indexer.postToIndex(indexPayloadArray.toString(), false,index);
        }

        if(indexed) {
          jsonMessage.put("success", "The topic approval state was set to TRUE");
        }
        else{
          jsonMessage.put("error", "The topic approval state could not be set");
        }
      }catch (Exception e) {
          if (store != null) {
            store.abort(); // Roll-back changes and deactivate (top-level) transaction.
            if (e instanceof BadObjectReferenceException) {
              throw new BadObjectReferenceException(e.getMessage());
            } else {
              throw new NdlaServiceException(e.getMessage(),e);
            }
          }
        } finally { // Release all topic map engine resources.
          if (store != null) {
            store.close();
          }
          if (repository != null) {
            repository.close();
          }
        }
      return jsonMessage;
    }

  synchronized public JSONObject disApproveTopic(String psi, String index) throws NdlaServiceException, BadObjectReferenceException{
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    JSONObject jsonMessage = new JSONObject();
    ArrayList<OccurrenceIF> forRemoval = new ArrayList<>();
    boolean indexed = false;

    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      TopicMapBuilderIF builder = topicMap.getBuilder();

      TopicIF topic = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));

      Collection<OccurrenceIF> occurrences = topic.getOccurrences();
      for (OccurrenceIF occurrence : occurrences) {
        String occType = occurrence.getType().toString();
        if(occType.contains("approved")) {
          forRemoval.add(occurrence);
        }

        if(occType.contains("approval-date")) {
          forRemoval.add(occurrence);
        }
      }//end for

      if(forRemoval.size() > 0) {
        for(OccurrenceIF occToRemove : forRemoval) {
          occToRemove.remove();
        }
      }

      TopicIF approvedOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
      builder.makeOccurrence(topic, approvedOcctype, "false");

      store.commit(); // Persist object graph.
      setProcessState(psi,"1",index);

      JSONArray indexSet = null;
      if(index.equals("keywords")) {
        indexSet = Utils.buildIndexJSONArray(psi, topicMap);
      }
      else if(index.equals("topics")){
        indexSet = Utils.buildTopicIndexJSONArray(psi,"",topicMap);
      }


      if(indexSet.length() > 0) {
        JSONObject temporaryPayload = (JSONObject) indexSet.get(0);
        temporaryPayload.put("approved","false");
        indexSet.put(0,temporaryPayload);
      }

      JSONObject indexPayloadObject = new JSONObject();
      String topicId = psi.substring(psi.lastIndexOf("#")+1);
      indexPayloadObject.put(topicId,indexSet);

      JSONArray indexPayloadArray = null;
      if(index.equals("keywords")) {
        indexPayloadArray = Utils.indexPayload(indexPayloadObject, "", topicMap);
      }
      else if(index.equals("topics")) {
        indexPayloadArray = indexSet;
      }

      if (indexPayloadArray.length() > 0) {
        Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
        indexed = indexer.postToIndex(indexPayloadArray.toString(), false, index);
      }

      if(indexed) {
        jsonMessage.put("success","The topic approval state was set to FALSE");
      }
      else{
        jsonMessage.put("error", "The topic approval state could not be set");
      }

    }catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return jsonMessage;
  }

  synchronized public void disApproveTopic(String psi, TopicMapIF _topicMap) throws NdlaServiceException, BadObjectReferenceException{

    TopicMapStoreIF store = null;
    ArrayList<OccurrenceIF> forRemoval = new ArrayList<>();

    try {
      store = _topicMap.getStore();

      TopicIF topic = _topicMap.getTopicBySubjectIdentifier(new URILocator(psi));

      Collection<OccurrenceIF> occurrences = topic.getOccurrences();
      for (OccurrenceIF occurrence : occurrences) {
        String occType = occurrence.getType().toString();
        if(occType.contains("approved")) {
          forRemoval.add(occurrence);
        }

        if(occType.contains("approval-date")) {
          forRemoval.add(occurrence);
        }
      }//end for

      if(forRemoval.size() > 0) {
        for(OccurrenceIF occToRemove : forRemoval) {
          occToRemove.remove();
        }
      }

      store.commit(); // Persist object graph.
    }catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
    }
  }

  synchronized public TopicIF deleteLanguageByLanguageId(String data) throws NdlaServiceException, BadObjectReferenceException {
    TopicIF deletedTopic = null;
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    TopicIF topicToBeDeleted = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      try {
        JSONArray jData = new JSONArray(data);
        for(int i = 0; i < jData.length(); i++){
          String psi = jData.getString(i);
          topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
          if (null != topicToBeDeleted) {
            topicToBeDeleted.remove();
            deletedTopic = topicToBeDeleted;
          }
        }
      } catch (MalformedURLException e) {
        throw new NdlaServiceException(e.getMessage(),e);
      }
      store.commit(); // Persist object graph.
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(),e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return deletedTopic;
  }

    synchronized public NdlaTopic getWordClass(String identifier) throws NdlaServiceException, BadObjectReferenceException {
        NdlaTopic result = null;
        String query = String.format("select $COLUMN from subject-identifier($COLUMN, \"http://psi.topic.ndla.no/#%s\")?", identifier);

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            ParsedStatementIF statement = null;
            QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
            QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

            QueryResultIF tologResult = null;
            try {
                statement = queryProcessor.parse(query);
                if (null != statement) {
                    tologResult = ((ParsedQueryIF) statement).execute();
                    if (tologResult.next()) {
                        result = new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), null);
                    }
                }
            } catch (BadObjectReferenceException e) {
                throw new BadObjectReferenceException(e.getMessage());
            } catch (InvalidQueryException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            } finally {
                if (null != tologResult) {
                    tologResult.close();
                }
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                if (e instanceof BadObjectReferenceException) {
                    throw new BadObjectReferenceException(e.getMessage());
                } else {
                    throw new NdlaServiceException(e.getMessage(),e);
                }
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    //**** Admin methods *****

    synchronized public HashMap<String, TopicIF> removeAssociation(String data, NdlaSite site) throws NdlaServiceException {
        HashMap<String, TopicIF> result = new HashMap<>();

        String siteUrl = "";
        switch (site) {
            case NDLA:
                siteUrl = NDLA_SITE_URL;
                break;
            case DELING:
                siteUrl = DELING_SITE_URL;
                break;
            case NYGIV:
                siteUrl = NYGIV_SITE_URL;
                break;
        }

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            HashMap<String, TopicIF> newKeyMap = new HashMap<String, TopicIF>(); // TODO: Flagged for removal (BK).
            TopicMapBuilderIF builder = topicMap.getBuilder(); // TODO: Flagged for removal (BK).
            LocatorIF baseAddress = store.getBaseAddress(); // TODO: Flagged for removal (BK).

            JSONObject payload = new JSONObject(data);
            String topicId = payload.getString("topicId");
            String topicType = payload.getString("topicType");

            String topicIdPsi = "";
            TopicIF role1 = null;
            String nodeId = "";
            switch (topicType) {
                case "ndla-node":
                    nodeId = topicId.substring(topicId.lastIndexOf("_") + 1);
                    topicIdPsi = siteUrl + "node/" + nodeId;
                    break;
                case "nygiv-node":
                    nodeId = topicId.substring(topicId.lastIndexOf("_") + 1);
                    topicIdPsi = siteUrl + "node/" + nodeId;
                    break;
                case "deling-node":
                    nodeId = topicId.substring(topicId.lastIndexOf("_") + 1);
                    topicIdPsi = siteUrl + "node/" + nodeId;
                    break;
                case "keyword":
                    topicIdPsi = "http://psi.topic.ndla.no/keywords/#" + topicId;
                    break;
                case "topic":
                    topicIdPsi = "http://psi.topic.ndla.no/topics/#" + topicId;
                    break;
                case "kompetansemaal":
                    topicIdPsi = "http://psi.udir.no/kl06/"+topicId;
                  break;
                case "domainTopics":
                  topicIdPsi = "http://psi.topic.ndla.no/domainTopics/#"+topicId;
                  break;
                case "person":
                  topicIdPsi = " http://psi.topic.ndla.no/persons/#"+topicId;
                  break;
            }

            role1 = topicMap.getTopicBySubjectIdentifier(new URILocator(topicIdPsi));

            if (null != role1) {
                String relationType = "http://psi.topic.ndla.no/#" + payload.getString("relationType");

                JSONArray relatedIds = payload.getJSONArray("related_topicIds");

                for (int i = 0; i < relatedIds.length(); i++) {
                    JSONObject relatedObject = relatedIds.getJSONObject(i);
                    String relatedTopicId = relatedObject.getString("relatedTopicId");
                    String relatedTopicType = relatedObject.getString("relatedTopicType");
                    TopicIF role2 = null;
                    String topicIdPsi2 = "";
                    String nodeId2 = "";
                    switch (relatedTopicType) {
                        case "ndla-node":
                            nodeId2 = topicId.substring(topicId.lastIndexOf("_") + 1);
                            topicIdPsi2 = siteUrl + "node/" + nodeId2;
                            break;
                        case "nygiv-node":
                            nodeId2 = topicId.substring(topicId.lastIndexOf("_") + 1);
                            topicIdPsi2 = siteUrl + "node/" + nodeId2;
                            break;
                        case "deling-node":
                            nodeId2 = topicId.substring(topicId.lastIndexOf("_") + 1);
                            topicIdPsi2 = siteUrl + "node/" + nodeId2;
                            break;
                        case "keyword":
                            topicIdPsi2 = "http://psi.topic.ndla.no/keywords/#" + relatedTopicId;
                            break;
                        case "topic":
                            topicIdPsi2 = "http://psi.topic.ndla.no/topics/#" + relatedTopicId;
                            break;
                    }
                    role2 = topicMap.getTopicBySubjectIdentifier(new URILocator(topicIdPsi2));
                      if (null != role2) {
                        Utils.deleteAssocsByRole(role1, role2, relationType, topicMap);
                        result.put(relatedTopicId, role2);
                    }
                }
            }// end if topic not null
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }


  public ArrayList<String> removeAssociationByReifier(String data, NdlaSite site) throws NdlaServiceException {
    ArrayList<String> result = new ArrayList();

    String siteUrl = "";
    switch (site) {
      case NDLA:
        siteUrl = NDLA_SITE_URL;
        break;
      case DELING:
        siteUrl = DELING_SITE_URL;
        break;
      case NYGIV:
        siteUrl = NYGIV_SITE_URL;
        break;
    }

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      HashMap<String, TopicIF> newKeyMap = new HashMap<String, TopicIF>(); // TODO: Flagged for removal (BK).
      TopicMapBuilderIF builder = topicMap.getBuilder(); // TODO: Flagged for removal (BK).
      LocatorIF baseAddress = store.getBaseAddress(); // TODO: Flagged for removal (BK).

      JSONObject payload = new JSONObject(data);
      String topicId = payload.getString("topicId");
      String topicType = payload.getString("topicType");
      String reifierId = payload.getString("reifierId");

      String topicIdPsi = "";
      TopicIF role1 = null;
      String nodeId = "";
      switch (topicType) {
        case "ndla-node":
          nodeId = topicId.substring(topicId.lastIndexOf("_") + 1);
          topicIdPsi = siteUrl + "node/" + nodeId;
          break;
        case "nygiv-node":
          nodeId = topicId.substring(topicId.lastIndexOf("_") + 1);
          topicIdPsi = siteUrl + "node/" + nodeId;
          break;
        case "deling-node":
          nodeId = topicId.substring(topicId.lastIndexOf("_") + 1);
          topicIdPsi = siteUrl + "node/" + nodeId;
          break;
        case "keyword":
          topicIdPsi = "http://psi.topic.ndla.no/keywords/#" + topicId;
          break;
        case "topic":
          topicIdPsi = "http://psi.topic.ndla.no/topics/#" + topicId;
          break;
        case "kompetansemaal":
          topicIdPsi = "http://psi.udir.no/kl06/"+topicId;
          break;
        case "domainTopics":
          topicIdPsi = "http://psi.topic.ndla.no/domainTopics/#"+topicId;
          break;
        case "persons":
          topicIdPsi = " http://psi.topic.ndla.no/persons/#"+topicId;
          break;
      }

      role1 = topicMap.getTopicBySubjectIdentifier(new URILocator(topicIdPsi));

      if (null != role1) {
        String relationType = "http://psi.topic.ndla.no/#" + payload.getString("relationType");
        TopicIF relationTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(relationType));
        if(null == relationTopicIF) {
          relationType = "http://psi.udir.no/ontologi/lkt/"+ payload.getString("relationType");
          relationTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(relationType));
        }

        if(null == relationTopicIF) {
          relationType = "http://www.topicmaps.org/xtm/1.0/core.xtm#"+ payload.getString("relationType");
          relationTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(relationType));
        }


        if(null == relationTopicIF) {
          relationType = "http://psi.udir.no/ontologi/"+ payload.getString("relationType");
          relationTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(relationType));
        }

        JSONArray relatedIds = payload.getJSONArray("related_topicIds");

        for (int i = 0; i < relatedIds.length(); i++) {
          JSONObject relatedObject = relatedIds.getJSONObject(i);
          String relatedTopicId = relatedObject.getString("relatedTopicId");
          String relatedTopicType = relatedObject.getString("relatedTopicType");
          TopicIF role2 = null;
          String topicIdPsi2 = "";
          String nodeId2 = "";
          switch (relatedTopicType) {
            case "ndla-node":
              nodeId2 = topicId.substring(topicId.lastIndexOf("_") + 1);
              topicIdPsi2 = siteUrl + "node/" + nodeId2;
              break;
            case "nygiv-node":
              nodeId2 = topicId.substring(topicId.lastIndexOf("_") + 1);
              topicIdPsi2 = siteUrl + "node/" + nodeId2;
              break;
            case "deling-node":
              nodeId2 = topicId.substring(topicId.lastIndexOf("_") + 1);
              topicIdPsi2 = siteUrl + "node/" + nodeId2;
              break;
            case "keyword":
              topicIdPsi2 = "http://psi.topic.ndla.no/keywords/#" + relatedTopicId;
              break;
            case "topic":
              topicIdPsi2 = "http://psi.topic.ndla.no/topics/#" + relatedTopicId;
              break;
          }
          role2 = topicMap.getTopicBySubjectIdentifier(new URILocator(topicIdPsi2));
          if (null != role2) {
            String reifid = Utils.deleteAssocsByReifier(topicIdPsi, topicIdPsi2, relationType, reifierId, topicMap);
            if(reifid == reifierId) {
              TopicIF reifierTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(reifierId));
              if(null != reifierTopic){
                reifierTopic.remove();
              }
              result.add(reifid);

            }

          }
        }
      }// end if topic not null
      store.commit(); // Persist object graph.
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(),e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return result;
  }


    synchronized public HashMap<String, TopicIF> addAssociation(String data, NdlaSite site) throws NdlaServiceException {
        HashMap<String, TopicIF> result = new HashMap<>();

        String siteUrl = "";
        switch (site) {
            case NDLA:
                siteUrl = NDLA_SITE_URL;
                break;
            case DELING:
                siteUrl = DELING_SITE_URL;
                break;
            case NYGIV:
                siteUrl = NYGIV_SITE_URL;
                break;
        }

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            HashMap<String, TopicIF> newKeyMap = new HashMap<String, TopicIF>(); // TODO: Flagged for removal.
            TopicMapBuilderIF builder = topicMap.getBuilder(); // TODO: Flagged for removal (BK).
            LocatorIF baseAddress = store.getBaseAddress(); // TODO: Flagged for removal (BK).

            JSONObject payload = new JSONObject(data);
            String topicId = payload.getString("topicId");
            String topicType = payload.getString("topicType");

            String topicIdPsi = "";
            TopicIF role1 = null;
            String nodeId = "";
            switch (topicType) {
                case "ndla-node":
                    nodeId = topicId.substring(topicId.lastIndexOf("_") + 1);
                    topicIdPsi = siteUrl + "node/" + nodeId;
                    break;
                case "nygiv-node":
                    nodeId = topicId.substring(topicId.lastIndexOf("_") + 1);
                    topicIdPsi = siteUrl + "node/" + nodeId;
                    break;
                case "deling-node":
                    nodeId = topicId.substring(topicId.lastIndexOf("_") + 1);
                    topicIdPsi = siteUrl + "node/" + nodeId;
                    break;
                case "keyword":
                    topicIdPsi = "http://psi.topic.ndla.no/keywords/#" + topicId;
                    break;
                case "topic":
                    topicIdPsi = "http://psi.topic.ndla.no/topics/#" + topicId;
                    break;
            }

            role1 = topicMap.getTopicBySubjectIdentifier(new URILocator(topicIdPsi));

            if (null != role1) {
                String relationType = "http://psi.topic.ndla.no/#" + payload.getString("relationType");

                JSONArray relatedIds = payload.getJSONArray("related_topicIds");

                for (int i = 0; i < relatedIds.length(); i++) {
                    JSONObject relatedObject = relatedIds.getJSONObject(i);
                    String relatedTopicId = relatedObject.getString("relatedTopicId");
                    String relatedTopicType = relatedObject.getString("relatedTopicType");
                    TopicIF role2 = null;
                    String topicIdPsi2 = "";
                    String nodeId2 = "";
                    String typeName = ""; // TODO: Flagged for removal (BK).

                    switch (relatedTopicType) {
                        case "ndla-node":
                            nodeId2 = topicId.substring(topicId.lastIndexOf("_") + 1);
                            topicIdPsi2 = siteUrl + "node/" + nodeId2;
                            break;
                        case "nygiv-node":
                            nodeId2 = topicId.substring(topicId.lastIndexOf("_") + 1);
                            topicIdPsi2 = siteUrl + "node/" + nodeId2;
                            break;
                        case "deling-node":
                            nodeId2 = topicId.substring(topicId.lastIndexOf("_") + 1);
                            topicIdPsi2 = siteUrl + "node/" + nodeId2;
                            break;
                        case "keyword":
                            topicIdPsi2 = "http://psi.topic.ndla.no/keywords/#" + relatedTopicId;
                            break;
                        case "topic":
                            topicIdPsi2 = "http://psi.topic.ndla.no/topics/#" + relatedTopicId;
                            break;
                    }
                    role2 = topicMap.getTopicBySubjectIdentifier(new URILocator(topicIdPsi2));

                    String player1Type = "http://psi.topic.ndla.no/#" + topicType;
                    String player2Type = "http://psi.topic.ndla.no/#" + relatedTopicType;

                    Utils.createAssociation(role1, role2, site, relationType, player1Type, player2Type, topicMap);

                    result.put(relatedTopicId, role2);
                }//end for
            }// end if topic not null
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    /**
     * Start of 'identifier' getters.
     */

    synchronized public ArrayList<String> getItemIdentifiers(String nodeId, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase();
        ArrayList<String> itemIdentifiers = new ArrayList<>(); // TODO: Flagged for removal (BK).

        String query = String.format("select $COLUMN from item-identifier(%snode_%s,$COLUMN)?", siteIdentifier, nodeId);
        ArrayList<String> result = executeStringCollectionQuery(query);

        return result;
    }

    synchronized public ArrayList<String> getSubjectIdentifiers(String nodeId, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        ArrayList<String> subjectIdentifiers = new ArrayList<>();
        String siteIdentifier = site.toString().toLowerCase();

        String query = String.format("select $COLUMN from subject-identifier(%snode_%s,$COLUMN)?", siteIdentifier, nodeId);
        ArrayList<String> result = executeStringCollectionQuery(query);

        return subjectIdentifiers;
    }

    synchronized public ArrayList<String> getKeywordSubjectIdentifiers(String nodeId, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        ArrayList<String> subjectIdentifiers = new ArrayList<>();
        String siteIdentifier = site.toString().toLowerCase();

        ArrayList<NdlaTopic> keywords = getKeywordsByNodeId(20, 0, siteIdentifier + "node_" + nodeId, site);

        for (NdlaTopic keyword : keywords) {
          String keywordId = keyword.getIdentifier();
          if(keywordId == "") {
            ArrayList<String> psis = keyword.getPsis();
            if(psis.size() > 0){
              if(psis.size() > 1){
                for(String tempPSI : psis){
                  if(tempPSI.contains("keyword")){
                    keywordId = tempPSI.substring(tempPSI.lastIndexOf("#")+1);
                    break;
                  }
                }
              }
              else{
                String tempPSI = psis.get(0);
                if(tempPSI.contains("keyword")){
                  keywordId = tempPSI.substring(tempPSI.lastIndexOf("#")+1);
                }
              }
            }

          }
          String query = String.format("select $COLUMN from subject-identifier(%s,$COLUMN)?", keywordId);
          ArrayList<String> intermediateResult = executeStringCollectionQuery(query);
          for(String intermediatePSI : intermediateResult){
            if(intermediatePSI.contains("keyword")){
              subjectIdentifiers.add(intermediatePSI);
            }
          }

        }
        return subjectIdentifiers;
    }

    synchronized public ArrayList<String> getKeywordSubjectIdentifiers(TopicIF topic) throws NdlaServiceException, BadObjectReferenceException {
      ArrayList<String> subjectIdentifiers = new ArrayList<>();

      return subjectIdentifiers;
    }

    synchronized public JSONObject getWordData(String nodeId, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        JSONObject result = new JSONObject();
        JSONArray wordClasses = new JSONArray();
        JSONArray wordVisibilities = new JSONArray();
        String siteIdentifier = site.toString().toLowerCase();
        ArrayList<NdlaTopic> keywords = getKeywordsByNodeId(20, 0, siteIdentifier + "node_" + nodeId, site);
        try {
            for (NdlaTopic topic : keywords) {
                String visibility = topic.getVisibility();
                wordVisibilities.put(visibility);
                ArrayList<NdlaWord> wordNames = topic.getWordNames();

                JSONObject wc = new JSONObject();
                for (NdlaWord wordName : wordNames) {
                    wc.put(topic.getIdentifier(), wordName.getWordClass());
                    wordClasses.put(wc);
                }
            }
            result.put("wordclasses", wordClasses);
            result.put("visibilities", wordVisibilities);
        } catch (JSONException e) {
            throw new NdlaServiceException(e.getMessage(),e);
        }
        return result;
    }

    /*
        Removed after having determined that methods are not currently used within the project (BK).
     */

    /*
    synchronized public Collection<TopicNameIF> getTopicNames(TopicIF topic) throws NdlaServiceException {
        TopicMapStoreIF store = null;
        Collection<TopicNameIF> topicNames = null;
        try {
            store = _reference.createStore(false);
            if (!store.isOpen()) {
                store.open();
            }

            topicNames = topic.getTopicNames();

        } catch (IOException e) {
            throw new NdlaServiceException(e.getMessage(),e);
        } finally { // Release all topic map engine resources.
            if ((store != null) && (store.isOpen())) {
                store.close();
            }
        }
        return topicNames;
    }

    synchronized public Collection<TopicIF> getScopes(TopicNameIF topic) throws NdlaServiceException {
        TopicMapStoreIF store = null;
        Collection<TopicIF> topicIFs = null;
        try {
            store = _reference.createStore(false);
            if (!store.isOpen()) {
                store.open();
            }

            topicIFs = topic.getScope();

        } catch (IOException e) {
            throw new NdlaServiceException(e.getMessage(),e);
        } finally { // Release all topic map engine resources.
            if ((store != null) && (store.isOpen())) {
                store.close();
            }
        }
        return topicIFs;
    }

    synchronized public Collection<OccurrenceIF> getOccurrences(TopicIF topic) throws NdlaServiceException {
        TopicMapStoreIF store = null;
        Collection<OccurrenceIF> topicOccs = null;
        try {
            store = _reference.createStore(false);
            if (!store.isOpen()) {
                store.open();
            }

            topicOccs = topic.getOccurrences();

        } catch (IOException e) {
            throw new NdlaServiceException(e.getMessage(),e);
        } finally { // Release all topic map engine resources.
            if ((store != null) && (store.isOpen())) {
                store.close();
            }
        }
        return topicOccs;
    }
    */

    /**
     * End of 'identifier' getters.
     */

    // ***** Topics *****
    public ArrayList<NdlaTopic> getSiteTopics(int limit, int offset, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        // instance-of($topic, ndla-topic) order by $topic limit 10 offset 10?
        String siteIdentifier = site.toString().toLowerCase();
        String query = String.format("instance-of($COLUMN, %s-topic) order by $COLUMN limit %s offset %s?", siteIdentifier, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }

    synchronized public NdlaTopic getSiteTopicByTopicId(String identifier) throws NdlaServiceException, BadObjectReferenceException {
        NdlaTopic result = null;
        String query = String.format("select $COLUMN from subject-identifier($COLUMN, \"http://psi.topic.ndla.no/topics/#%s\")?", identifier);

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            ParsedStatementIF statement = null;
            QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
            QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

            QueryResultIF tologResult = null;
            try {
                statement = queryProcessor.parse(query);
                if (null != statement) {
                    tologResult = ((ParsedQueryIF) statement).execute();
                    if (tologResult.next()) {
                        result = new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), null);
                    }
                }
            } catch (BadObjectReferenceException e) {
                throw new BadObjectReferenceException(e.getMessage());
            } catch (InvalidQueryException e) {
                throw new NdlaServiceException(e.getMessage());
            } finally {
                if (null != tologResult) {
                    tologResult.close();
                }
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                if (e instanceof BadObjectReferenceException) {
                    throw new BadObjectReferenceException(e.getMessage());
                } else {
                    throw new NdlaServiceException(e.getMessage());
                }
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    public ArrayList<NdlaTopic> getSiteTopicsByNodeId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase();
        String query = String.format("select $COLUMN from %snode-topic(%s: %s-node, $COLUMN : %s-topic) order by $COLUMN limit %s offset %s?", siteIdentifier, identifier, siteIdentifier, siteIdentifier, limit, offset);

        return executeCollectionQuery(query);
    }

    public JSONObject getTopicsBySubjectMatter(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
      JSONObject topics = getOntologyTopics(identifier, site);
      JSONObject result = new JSONObject();
      JSONArray resultRelations = new JSONArray();
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try{
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap();
        JSONArray relations = topics.getJSONArray("relations");
        for(int i = 0; i < relations.length(); i++){
          JSONObject relation = relations.getJSONObject(i);
          JSONArray relatedTopics = relation.getJSONArray("relatedTopics");
          JSONArray resultRelatedTopics = new JSONArray();
          if(relatedTopics.length() > offset){
            JSONObject resultRelation = new JSONObject();
            resultRelation.put("associationId",relation.getString("associationId"));
            resultRelation.put("associationTypeNames",relation.getJSONArray("associationTypeNames"));
            for(int j = offset; j < relatedTopics.length(); j++) {
              JSONObject relatedTopic = (JSONObject) relatedTopics.get(j);
              String relatedTopicId = relatedTopic.getString("topicId");
              TopicIF relatedTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(relatedTopicId));
              TopicIF approvedOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
              TopicIF approvedDateOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));
              Collection<OccurrenceIF> approvedOccurrenceIFs = relatedTopicIF.getOccurrencesByType(approvedOccurrenceType);
              String approvedString = "false";
              for(OccurrenceIF approvedOccurrenceIF : approvedOccurrenceIFs){
                if(approvedOccurrenceIF.getValue().equals("true")) {
                  approvedString = "true";
                  break;
                }
              }
              relatedTopic.put("approved",approvedString);
              if(approvedString.equals("true")){
                String approvalDate = "";
                Collection<OccurrenceIF> approvedDateOccurrencesIFs = relatedTopicIF.getOccurrencesByType(approvedDateOccurrenceType);
                for(OccurrenceIF approvedDateOccurrencesIF : approvedDateOccurrencesIFs){
                  if(approvedDateOccurrencesIF.getValue() != "") {
                    approvalDate = approvedDateOccurrencesIF.getValue();
                    break;
                  }
                }

                if(approvalDate != ""){
                  relatedTopic.put("approval_date",approvalDate);
                }
              }
              resultRelatedTopics.put(relatedTopic);
            }
            resultRelation.put("relatedTopics",resultRelatedTopics);
            resultRelations.put(resultRelation);
          }//end if relatedTopics within limit

        }
        result.put("relations",resultRelations);
      } catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          throw new NdlaServiceException(e.getMessage());
        }
      }
      finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }
      return result;
    }

    public ArrayList<NdlaTopic> getSiteTopicsBySubjectMatter(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
    String siteIdentifier = site.toString().toLowerCase();
    String query = String.format("select $COLUMN from subject-topic(%s: subjectmatter, $COLUMN : %s-topic) order by $COLUMN limit %s offset %s?", identifier, siteIdentifier, Integer.toString(limit), Integer.toString(offset));

    return executeCollectionQuery(query);
    }


  /**
   *
   * createTopic
   *
   * @param  data
   * @return HashMap<String,TopicIF>
   * @throws NdlaServiceException
   * @throws BadObjectReferenceException
   */

  public HashMap<String,TopicIF> createTopic(String data) throws NdlaServiceException, BadObjectReferenceException {
    HashMap<String,TopicIF> topics = new HashMap<>();
    TopicIF newTopic = null;
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    boolean isExistingKeyword = true;
    Indexer indexer = null;

    String topicTypeString = "topic";

    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();

      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = store.getBaseAddress();

      JSONObject payload = new JSONObject(data);
      String fromKeyword = payload.getString("fromKeyword");

      if(fromKeyword.equals("newTopic")){
        isExistingKeyword = false;
      }

      JSONObject indexTopicsPayLoad = new JSONObject();
      JSONObject indexKeywordsPayLoad = new JSONObject();

      if(!isExistingKeyword){

        String topicId = "topic-" + Utils.makeRandomId(topicMap);
        newTopic = builder.makeTopic();

        newTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicId));
        newTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));


        indexTopicsPayLoad.put("topicId",topicId);

        JSONArray topicNames = payload.getJSONArray("names");

        boolean isExistingTopic = Utils.isExistingTopicByNames(topicNames,_configFile,_topicmapReferenceKey);

        if(!isExistingTopic){
          JSONArray topicTypes = payload.getJSONArray("types");
          JSONArray topicDescriptions = payload.getJSONArray("descriptions");
          JSONArray topicImages = payload.getJSONArray("images");
          String wordClass = payload.getString("wordClass");
          indexTopicsPayLoad.put("wordclass",wordClass);

          TopicIF approvedOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
          builder.makeOccurrence(newTopic,approvedOccurrenceType,"false");
          indexTopicsPayLoad.put("approved","false");

          TopicIF processOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#process-state"));
          builder.makeOccurrence(newTopic, processOcctype, "0");
          indexTopicsPayLoad.put("processState","0");


          // Visibility occurrence
          TopicIF visibilityOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#keyword-visibility"));
          builder.makeOccurrence(newTopic, visibilityOcctype, "1");
          indexTopicsPayLoad.put("visibility","1");

          if(topicNames.length() > 0){
            JSONArray indexTopicNames = new JSONArray();
            for (int i = 0; i < topicNames.length(); i++) {
              JSONObject nameObject = topicNames.getJSONObject(i);
              indexTopicNames.put(nameObject);

              String topicNameString = nameObject.getString("name");
              String topicNameLang = nameObject.getString("language");

              Utils.createBasename(newTopic, topicNameString, topicNameLang, wordClass, topicMap);
            }
            indexTopicsPayLoad.put("names",indexTopicNames);
          }


          TopicIF defaultType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeString));
          newTopic.addType(defaultType);

          if(topicTypes.length() > 0) {
            for (int j = 0; j < topicTypes.length(); j++) {
              JSONObject topicTypeObject = topicTypes.getJSONObject(j);
              String topicTypeId = topicTypeObject.getString("typeId");
              TopicIF type = null;
              if(topicTypeId.contains("#")){
                type = topicMap.getTopicBySubjectIdentifier(new URILocator(topicTypeId));
              }
              else{
                type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicTypeId));
              }

              newTopic.addType(type);
            }
          }
          JSONArray indexTypes = Utils.indexParseTypes(topicTypes,topicMap);
          indexTopicsPayLoad.put("types",indexTypes);

          JSONArray indexDescriptions = new JSONArray();
          if(topicDescriptions.length() > 0) {
            for (int k = 0; k < topicDescriptions.length(); k++) {
              JSONObject topicDescriptionObject = topicDescriptions.getJSONObject(k);
              indexDescriptions.put(topicDescriptionObject);

              String descriptionLanguage = topicDescriptionObject.getString("language");
              String descriptionText = topicDescriptionObject.getString("content");

              TopicIF descriptionOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#description"));
              OccurrenceIF descriptionOccurrence = builder.makeOccurrence(newTopic, descriptionOccurrenceType, descriptionText);

              descriptionOccurrence.addTheme(Utils.getLanguageTopic(descriptionLanguage, topicMap));
            }
          }
          indexTopicsPayLoad.put("descriptions",indexDescriptions);

          JSONArray indexImages = new JSONArray();
          if(topicImages.length() > 0) {
            for (int l = 0; l < topicImages.length(); l++) {
              JSONObject topicImageObject = topicImages.getJSONObject(l);
              indexImages.put(topicImageObject);
              String imageLanguage = topicImageObject.getString("language");
              String topicImageUrl = topicImageObject.getString("url");

              TopicIF imageOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#image"));
              OccurrenceIF imageOccurrence = builder.makeOccurrence(newTopic,imageOccurrenceType,new URILocator(topicImageUrl));
              imageOccurrence.addTheme(Utils.getLanguageTopic(imageLanguage, topicMap));

            }
          }
          indexTopicsPayLoad.put("images",indexImages);

          topics.put(topicId,newTopic);

          store.commit();

          if (indexTopicsPayLoad.length() > 0) {
            JSONArray indexPayloadArray = new JSONArray();
            indexPayloadArray.put(indexTopicsPayLoad);
              indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
            boolean indexed = indexer.postToIndex(indexPayloadArray.toString(), false,"topics");
          }


          JSONArray indexPayloadArray = Utils.indexPayload(indexKeywordsPayLoad, "", topicMap);

          if (indexPayloadArray.length() > 0) {
            if(null == indexer){
              indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
            }

            boolean indexed = indexer.postToIndex(indexPayloadArray.toString(), false,"keywords");
          }

        }//end if !ExistingTopic


      }else{
        String keywordPsi = "http://psi.topic.ndla.no/keywords/#"+fromKeyword;
        String topicTypePsi = "http://psi.topic.ndla.no/#" + topicTypeString;
        TopicIF keywordTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(keywordPsi));

        TopicIF topicType = topicMap.getTopicBySubjectIdentifier(new URILocator(topicTypePsi));
        keywordTopic.addType(topicType);

        String topicId = "topic-" + Utils.makeRandomId(topicMap);
        String topicPsi = "http://psi.topic.ndla.no/topics/#" + topicId;
        keywordTopic.addSubjectIdentifier(new URILocator(topicPsi));
        keywordTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));


        //approval state
        TopicIF approvedOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
        TopicIF approvalDateOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));

        String approved = "";
        String approval_date = "";

        HashMap<String,String> approvedState = Utils.getApprovedState(keywordPsi,topicMap);
        approved = approvedState.get("approved");
        approval_date = approvedState.get("approval_date");

        TopicIF processOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#process-state"));
        builder.makeOccurrence(keywordTopic, processOcctype, "0");


        JSONArray indexSetKeywords = Utils.buildIndexJSONArray(keywordPsi, topicMap);
        JSONArray indexSetTopics = Utils.buildTopicIndexJSONArray(keywordPsi,topicPsi,topicMap);

        JSONArray indexKeywordPayloadArray = null;
        JSONObject indexPayloadObject = new JSONObject();



        if(indexSetKeywords.length() > 0) {
          JSONObject temporaryKeywordPayload = (JSONObject) indexSetKeywords.get(0);
          JSONObject temporaryTopicPayload = (JSONObject) indexSetTopics.get(0);

          if(null != approved && approved.length() > 0){
            if(approved.equals("true") && approval_date.length() > 0) {
              temporaryKeywordPayload.put("approved","true");
              temporaryKeywordPayload.put("approval_date",approval_date);
              temporaryTopicPayload.put("approved","true");
              temporaryTopicPayload.put("approval_date",approval_date);

            }
            else{
              temporaryKeywordPayload.put("approved","false");
              temporaryTopicPayload.put("approved","false");

            }
          }
          else{
            temporaryKeywordPayload.put("approved","false");
            temporaryTopicPayload.put("approved","false");
          }

          temporaryKeywordPayload.put("processState","0");
          temporaryTopicPayload.put("processState","0");


          indexSetKeywords.put(0,temporaryKeywordPayload);
          indexSetTopics.put(0,temporaryTopicPayload);
        }



        indexPayloadObject.put(fromKeyword,indexSetKeywords);

        indexKeywordPayloadArray = Utils.indexPayload(indexPayloadObject, "", topicMap);

        boolean indexedKeywords = false;
        boolean indexedTopics = false;


        if (indexKeywordPayloadArray.length() > 0) {
          indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
          indexedKeywords = indexer.postToIndex(indexKeywordPayloadArray.toString(), false,"keywords");
        }

        if (indexSetTopics.length() > 0) {
          if(null == indexer){
            indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
          }

          indexedTopics = indexer.postToIndex(indexSetTopics.toString(), false,"topics");
        }

        //newTopic = keywordTopic;
        topics.put(topicId,keywordTopic);

        store.commit();
      }//end if not existing keyword


    }
    catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage());
      }
    }
    finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }

    return topics;
  }

    public HashMap<String,TopicIF> createTopic_old(String data) throws NdlaServiceException, BadObjectReferenceException {
      HashMap<String,TopicIF> topics = new HashMap<>();
      TopicIF newTopic = null;
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap();

        TopicMapBuilderIF builder = topicMap.getBuilder();
        LocatorIF baseAddress = store.getBaseAddress();

        String topicTypeString = "topic";

        String topicId = topicTypeString + "-" + Utils.makeRandomId(topicMap);
        newTopic = builder.makeTopic();

        newTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicId));
        newTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));

        JSONObject payload = new JSONObject(data);

        JSONObject indexPayLoad = new JSONObject();
        indexPayLoad.put("topicId",topicId);

        JSONArray topicNames = payload.getJSONArray("names");

        boolean isExistingTopic = Utils.isExistingTopicByNames(topicNames,_configFile,_topicmapReferenceKey);
        if(!isExistingTopic){
          JSONArray topicTypes = payload.getJSONArray("types");
          JSONArray topicDescriptions = payload.getJSONArray("descriptions");
          JSONArray topicImages = payload.getJSONArray("images");

          TopicIF approvedOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
          builder.makeOccurrence(newTopic,approvedOccurrenceType,"false");
          indexPayLoad.put("approved","false");


          if(topicNames.length() > 0){
            JSONArray indexTopicNames = new JSONArray();
            for (int i = 0; i < topicNames.length(); i++) {
              JSONObject nameObject = topicNames.getJSONObject(i);
              indexTopicNames.put(nameObject);

              String topicNameString = nameObject.getString("name");
              String topicNameLang = nameObject.getString("language");

              Utils.createBasename(newTopic, topicNameString, topicNameLang, topicMap);
            }
            indexPayLoad.put("names",indexTopicNames);
          }


          // add default type of topic
          TopicIF defaultType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeString));
          newTopic.addType(defaultType);

          if(topicTypes.length() > 0) {
            for (int j = 0; j < topicTypes.length(); j++) {
              JSONObject topicTypeObject = topicTypes.getJSONObject(j);
              String topicTypeId = topicTypeObject.getString("typeId");
              TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicTypeId));
              newTopic.addType(type);
            }
            JSONArray indexTypes = Utils.indexParseTypes(topicTypes,topicMap);
            indexPayLoad.put("types",indexTypes);
          }

          if(topicDescriptions.length() > 0) {
            JSONArray indexDescriptions = new JSONArray();
            for (int k = 0; k < topicDescriptions.length(); k++) {
              JSONObject topicDescriptionObject = topicDescriptions.getJSONObject(k);
              indexDescriptions.put(topicDescriptionObject);

              String descriptionLanguage = topicDescriptionObject.getString("language");
              String descriptionText = topicDescriptionObject.getString("content");

              TopicIF descriptionOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#description"));
              OccurrenceIF descriptionOccurrence = builder.makeOccurrence(newTopic, descriptionOccurrenceType, descriptionText);

              descriptionOccurrence.addTheme(Utils.getLanguageTopic(descriptionLanguage, topicMap));
            }
            indexPayLoad.put("descriptions",indexDescriptions);
          }

          if(topicImages.length() > 0) {
            JSONArray indexImages = new JSONArray();
            for (int l = 0; l < topicImages.length(); l++) {
              JSONObject topicImageObject = topicImages.getJSONObject(l);
              indexImages.put(topicImageObject);
              String imageLanguage = topicImageObject.getString("language");
              String topicImageUrl = topicImageObject.getString("url");

              TopicIF imageOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#image"));
              OccurrenceIF imageOccurrence = builder.makeOccurrence(newTopic,imageOccurrenceType,new URILocator(topicImageUrl));
              imageOccurrence.addTheme(Utils.getLanguageTopic(imageLanguage, topicMap));

            }
            indexPayLoad.put("images",indexImages);
          }
          store.commit();

          if (indexPayLoad.length() > 0) {
            JSONArray indexPayloadArray = new JSONArray();
            indexPayloadArray.put(indexPayLoad);
            Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
            boolean indexed = indexer.postToIndex(indexPayloadArray.toString(), false,"topics");
          }
          topics.put(topicId,newTopic);
        }
      }
      catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          throw new NdlaServiceException(e.getMessage());
        }
      }
      finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }

      return topics;
    }


  /**
   *
   * deleteTopicByTopicId
   *
   * @param identifiers
   * @return ArrayList<String>
   * @throws NdlaServiceException
   * @throws BadObjectReferenceException
   */
  public ArrayList<String> deleteTopicByTopicId(String identifiers) throws NdlaServiceException, BadObjectReferenceException {
    ArrayList<String> deletedTopics = new ArrayList<>();
    ArrayList<String> deleteFromIndex = new ArrayList<>();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.
      JSONArray payload = new JSONArray(identifiers);

      for (int i = 0; i < payload.length(); i++) {
        String identifier = payload.getString(i);

        deleteFromIndex.add(identifier);
        TopicIF topicToBeDeleted = null;
        try {
          topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + identifier));
          if (null != topicToBeDeleted) {
            String psi = topicToBeDeleted.getSubjectIdentifiers().iterator().next().getAddress();
            deletedTopics.add(psi);
            topicToBeDeleted.remove();
          }
        } catch (MalformedURLException e) {
          throw new NdlaServiceException(e.getMessage());
        }
        store.commit(); // Persist object graph.
      }
      //delete from index
      if (deleteFromIndex.size() > 0) {
        Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
        indexer.deleteFromIndex(deleteFromIndex,"topics");
      }
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage());
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }

    return deletedTopics;
  }

  /**
   *
   * getTopics
   *
   * @param limit
   * @param offset
   * @return ArrayList<NdlaTopic>
   * @throws NdlaServiceException
   * @throws BadObjectReferenceException
   */
  public ArrayList<NdlaTopic> getTopics(int limit, int offset) throws NdlaServiceException, BadObjectReferenceException {
    String query = "";

    if(limit == 0 && offset == 0){
      query = String.format("select $COLUMN from instance-of($COLUMN, topic) order by $COLUMN?");
    }
    else{
      query = String.format("select $COLUMN from instance-of($COLUMN, topic) order by $COLUMN limit %s offset %s?", Integer.toString(limit), Integer.toString(offset));
    }

    return executeCollectionQuery(query);
  }



  /**
   *
   * getTopicByTopicId
   *
   * @param identifier
   * @return NdlaTopic
   * @throws NdlaServiceException
   * @throws BadObjectReferenceException
   */
  public NdlaTopic getTopicByTopicId(String identifier) throws NdlaServiceException, BadObjectReferenceException {
    NdlaTopic result = null;
    String query = String.format("select $COLUMN from {subject-identifier($COLUMN, \"http://psi.topic.ndla.no/topics/#%s\") | subject-identifier($COLUMN, \"http://psi.topic.ndla.no/#%s\")}?", identifier, identifier);

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      ParsedStatementIF statement = null;
      QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
      QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

      QueryResultIF tologResult = null;
      try {
        statement = queryProcessor.parse(query);
        if (null != statement) {
          tologResult = ((ParsedQueryIF) statement).execute();
          if (tologResult.next()) {
            result = new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), null);
          }
        }
      } catch (BadObjectReferenceException e) {
        throw new BadObjectReferenceException(e.getMessage());
      } catch (InvalidQueryException e) {
        throw new NdlaServiceException(e.getMessage());
      } finally {
        if (null != tologResult) {
          tologResult.close();
        }
      }
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage());
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return result;
  }

  /**
   *
   * getRelationTopicByTopicId
   *
   * @param identifier
   * @return NdlaTopic
   * @throws NdlaServiceException
   * @throws BadObjectReferenceException
   */
  public NdlaTopic getRelationTopicByTopicId(String identifier) throws NdlaServiceException, BadObjectReferenceException {
    NdlaTopic result = null;
    String query = String.format("select $COLUMN from subject-identifier($COLUMN, \"%s\")?", identifier);

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      ParsedStatementIF statement = null;
      QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
      QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

      QueryResultIF tologResult = null;
      try {
        statement = queryProcessor.parse(query);
        if (null != statement) {
          tologResult = ((ParsedQueryIF) statement).execute();
          if (tologResult.next()) {
            result = new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), null);
          }
        }
      } catch (BadObjectReferenceException e) {
        throw new BadObjectReferenceException(e.getMessage());
      } catch (InvalidQueryException e) {
        throw new NdlaServiceException(e.getMessage());
      } finally {
        if (null != tologResult) {
          tologResult.close();
        }
      }
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage());
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return result;
  }

  public HashMap<String,ArrayList<NdlaTopic>> getTopicPlayersByRelationTypeId(String identifier, String roleTopic1, String roleTopic2) throws NdlaServiceException, BadObjectReferenceException  {
    HashMap<String,ArrayList<NdlaTopic>> result = new HashMap<String,ArrayList<NdlaTopic>>();
    String query = String.format("select $topic1,$topic2 from i\"%s\"($topic1 : %s, $topic2 : %s)?", identifier,roleTopic1,roleTopic2);

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      ParsedStatementIF statement = null;
      QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
      QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);


      ArrayList<NdlaTopic> role1List = new ArrayList<>();
      ArrayList<NdlaTopic> role2List = new ArrayList<>();

      QueryResultIF tologResult = null;
      try {
        statement = queryProcessor.parse(query);
        if (null != statement) {
          tologResult = ((ParsedQueryIF) statement).execute();
          while (tologResult.next()) {
            role1List.add( new NdlaTopic((TopicIF) tologResult.getValue("topic1"), null));
            role2List.add( new NdlaTopic((TopicIF) tologResult.getValue("topic2"), null));
          }
        }
        result.put(roleTopic1,role1List);
        result.put(roleTopic2,role2List);
      } catch (BadObjectReferenceException e) {
        throw new BadObjectReferenceException(e.getMessage());
      } catch (InvalidQueryException e) {
        throw new NdlaServiceException(e.getMessage());
      } finally {
        if (null != tologResult) {
          tologResult.close();
        }
      }
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage());
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return result;
  }

  public HashMap<String,TopicIF> updateTopicsByTopicId(String data) throws NdlaServiceException, BadObjectReferenceException {
    HashMap<String, TopicIF> keyMap = new HashMap<String, TopicIF>();

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;

    try{
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = store.getBaseAddress();

      JSONObject payload = new JSONObject(data);
      JSONObject indexPayLoad = new JSONObject();

      JSONArray topics = payload.getJSONArray("topics");

      ArrayList<String> foundKeys = new ArrayList<>();
      String topicId = "";
      for (int i = 0; i < topics.length(); i++) {
        JSONObject jsonTopicSet = topics.getJSONObject(i);
        JSONArray indexTopicSet = new JSONArray();

        Iterator topicIdKeyIter = jsonTopicSet.keys();
        while (topicIdKeyIter.hasNext()) {
          topicId = (String) topicIdKeyIter.next();
          indexPayLoad.put("topicId",topicId);
          URILocator topicLocator = new URILocator("http://psi.topic.ndla.no/topics/#" + topicId);
          TopicIF existingTopic = null;
          existingTopic = topicMap.getTopicBySubjectIdentifier(topicLocator);
          if(null == existingTopic){
            topicLocator = new URILocator("http://psi.topic.ndla.no/#" + topicId);
            existingTopic = topicMap.getTopicBySubjectIdentifier(topicLocator);
          }
          String approved = "";
          String approval_date = "";

          if (null != existingTopic) {

            HashMap<String,String> approvedState = Utils.getApprovedState(topicLocator.getAddress(),topicMap);
            approved = approvedState.get("approved");
            approval_date = approvedState.get("approval_date");

            HashMap<String,String> processState = Utils.getProcessState(topicLocator.getAddress(),topicMap);
            String processStateValue = "";
            if(processState.size() > 0){
              processStateValue = processState.get("processState");
            }
            else{
              processStateValue = "0";
            }

            if(null != approved && approved.length() > 0) {
              indexPayLoad.put("approved",approved);
              if(approved.equals("true")) {
                indexPayLoad.put("approval_date",approval_date);
              }
            }
            else{
              indexPayLoad.put("approved",false);
            }

            Utils.deleteBasenames(topicLocator, topicMap);
            Utils.deleteOccurrences(topicLocator, topicMap);
            ArrayList<LocatorIF> excludedTypes = new ArrayList<>();
            excludedTypes.add(new URILocator("http://psi.topic.ndla.no/#keyword"));
            excludedTypes.add(new URILocator("http://psi.topic.ndla.no/#topic"));
            Utils.deleteTypes(topicLocator,excludedTypes,topicMap);

            JSONArray topicSets = jsonTopicSet.getJSONArray(topicId);
            Utils.deleteItemIdentifiers(topicLocator,topicMap);
            Utils.deletePsis(topicLocator,topicMap);

            for (int j = 0; j < topicSets.length(); j++) {
              JSONObject topicData = topicSets.getJSONObject(j);
              JSONObject indexTopic = new JSONObject();

              JSONArray topicNames = topicData.getJSONArray("names");
              JSONArray topicTypes = topicData.getJSONArray("types");
              String wordClass = topicData.getString("wordClass");


              TopicIF processOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#process-state"));
              builder.makeOccurrence(existingTopic, processOcctype, processStateValue);
              indexPayLoad.put("processState",processStateValue);
              indexPayLoad.put("wordclass",wordClass);

              String visibility = "";
              if(topicData.has("visibility")){
               visibility = topicData.getString("visibility");
              }
              else{
                visibility = "1";
              }

              // Visibility occurrence
              TopicIF visbilityOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#keyword-visibility"));
              builder.makeOccurrence(existingTopic, visbilityOcctype, visibility);
              indexPayLoad.put("visibility",visibility);

              JSONArray psis = topicData.getJSONArray("psis");
              for(int n = 0; n < psis.length(); n++){
                String psiString = psis.getString(n);
                existingTopic.addSubjectIdentifier(new URILocator(psiString));
                String topicFragment = psiString.substring(psiString.lastIndexOf("#")+1);
                existingTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicFragment));
              }

              JSONArray topicDescriptions = topicData.getJSONArray("descriptions");
              JSONArray topicImages = topicData.getJSONArray("images");

              if(topicNames.length() > 0){
                JSONArray indexTopicNames = new JSONArray();
                for (int k = 0; k < topicNames.length(); k++) {
                  JSONObject nameObject = topicNames.getJSONObject(k);
                  indexTopicNames.put(nameObject);

                  String topicNameString = nameObject.getString("name");
                  String topicNameLang = nameObject.getString("language");

                  Utils.createBasename(existingTopic, topicNameString, topicNameLang,wordClass, topicMap);
                }
                indexPayLoad.put("names",indexTopicNames);
              }//end topicNames


              if(topicTypes.length() > 0) {
                for (int l = 0; l < topicTypes.length(); l++) {
                  JSONObject topicTypeObject = topicTypes.getJSONObject(j);
                  String topicTypeId = topicTypeObject.getString("typeId");
                  TopicIF type = null;
                  type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicTypeId));
                  if(null == type){
                    type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeId));
                  }
                  existingTopic.addType(type);

                }
                JSONArray indexTypes = Utils.indexParseTypes(topicTypes,topicMap);
                indexPayLoad.put("types",indexTypes);
              }//end if topicTypes


              if(topicDescriptions.length() > 0) {
                JSONArray indexDescriptions = new JSONArray();
                for (int k = 0; k < topicDescriptions.length(); k++) {
                  JSONObject topicDescriptionObject = topicDescriptions.getJSONObject(k);
                  indexDescriptions.put(topicDescriptionObject);

                  String descriptionLanguage = topicDescriptionObject.getString("language");
                  String descriptionText = topicDescriptionObject.getString("content");

                  TopicIF descriptionOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#description"));
                  OccurrenceIF descriptionOccurrence = builder.makeOccurrence(existingTopic, descriptionOccurrenceType, descriptionText);

                  descriptionOccurrence.addTheme(Utils.getLanguageTopic(descriptionLanguage, topicMap));
                }
                indexPayLoad.put("descriptions",indexDescriptions);
              }//end topicDescriptions


              if(topicImages.length() > 0) {
                JSONArray indexImages = new JSONArray();
                for (int m = 0; m < topicImages.length(); m++) {
                  JSONObject topicImageObject = topicImages.getJSONObject(m);
                  indexImages.put(topicImageObject);
                  String imageLanguage = topicImageObject.getString("language");
                  String topicImageUrl = topicImageObject.getString("url");

                  TopicIF imageOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#image"));
                  OccurrenceIF imageOccurrence = builder.makeOccurrence(existingTopic,imageOccurrenceType,new URILocator(topicImageUrl));
                  imageOccurrence.addTheme(Utils.getLanguageTopic(imageLanguage, topicMap));

                }
                indexPayLoad.put("images",indexImages);
              }//end if topicImages


              TopicIF approvedOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
              TopicIF approvalDateOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));
              if(null != approved && approved.length() > 0){
                if(approved.equals("true") && approval_date.length() > 0) {
                  builder.makeOccurrence(existingTopic,approvedOcctype,approved);
                  builder.makeOccurrence(existingTopic,approvalDateOcctype,approval_date);
                }
                else{

                  builder.makeOccurrence(existingTopic,approvedOcctype,"false");
                }
              }
              else{
                builder.makeOccurrence(existingTopic,approvedOcctype,"false");
              }



              keyMap.put(topicId, existingTopic);
            }//end for topicSets
          }//end if existing
        }//end while
      }//end for topics

      store.commit(); // Persist object graph.
      if (indexPayLoad.length() > 0) {
        JSONArray indexPayloadArray = new JSONArray();
        indexPayloadArray.put(indexPayLoad);
        Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
        boolean indexed = indexer.postToIndex(indexPayloadArray.toString(), false,"topics");
      }
    }
    catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage());
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
    }
    return keyMap;
  }

    /*
    *  @TODO: Implement test function when data is imported into topic map
    */
    public ArrayList<NdlaTopic> getTopicsByAimId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase(); // TODO: Flagged for removal (BK).
        String query = String.format("select $COLUMN from aim_has_topic(%s: kompetansemaal, $COLUMN : topic) order by $COLUMN limit %s offset %s?", identifier, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }


  public JSONObject getAimTopicsBySubjectId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
    JSONObject result = new JSONObject();
    JSONArray associations = new JSONArray();
    HashMap<String,ArrayList<HashMap<String,String>>> associationData = new HashMap<>();
    HashMap<String,ArrayList<HashMap<String,String>>> playerNameMap = new HashMap<>();
    ArrayList<String> aimRoles = new ArrayList<>();
    HashMap<String,String> rolePlayerMap = new HashMap<String,String>();

    TreeMap<String,ArrayList<String>> aimMap = new TreeMap<>();

    String siteIdentifier = site.toString().toLowerCase(); // TODO: Flagged for removal (BK).
    String query = String.format("association($COLUMN),reifies($TOPIC, $COLUMN), {udir-subject($TOPIC,\"http://psi.topic.ndla.no/#%s\")|subjectmatter($TOPIC,\"http://psi.topic.ndla.no/#%s\")} limit %s offset %s?" ,identifier,identifier, Integer.toString(limit), Integer.toString(offset));
    try{
    ArrayList<Object> assocs = executeObjectCollectionQuery(query);
      for( Object assoc : assocs) {
        JSONObject association = new JSONObject();
        AssociationIF associationIF = (AssociationIF) assoc;

        TopicIF associationType = associationIF.getType();
        Collection<LocatorIF> associationTypeLocators = associationType.getSubjectIdentifiers();
        String associationTypePSI = "";
        if(associationTypeLocators.size() == 1) {
          associationTypePSI = associationTypeLocators.iterator().next().getAddress();
        }
        else {
          int associationTypeCounter = 0;
          for(LocatorIF associationTypeLocator : associationTypeLocators) {
            if(associationTypeCounter == 0) {
              associationTypePSI = associationTypeLocator.getAddress();
              break;
            }
          }
        }

        ArrayList<String> foundAssocNames = new ArrayList<>();
        if(associationTypePSI.contains("aim_has_topic")){
          association.put("associationType",associationTypePSI);
          Collection<TopicNameIF> assocTypeNames = associationType.getTopicNames();
          JSONArray assocTypeNamesJSON = new JSONArray();

          if (assocTypeNames.size() > 0) {
            for (TopicNameIF assocTypeName : assocTypeNames) {
              JSONObject assocTypeNameJSON = new JSONObject();

              Collection<TopicIF> assocTypeScopes = assocTypeName.getScope();
              for (TopicIF assocTypeScope : assocTypeScopes) {
                Collection<LocatorIF> assocTypeScopeLocators = assocTypeScope.getSubjectIdentifiers();
                for (LocatorIF assocTypeScopeLocator : assocTypeScopeLocators) {
                  if(assocTypeScopeLocator.getAddress().contains("iso/639")
                      && !foundAssocNames.contains(assocTypeScopeLocator.getAddress()+"_"+assocTypeName.getValue())){
                    assocTypeNameJSON.put("language",assocTypeScopeLocator.getAddress());
                    assocTypeNameJSON.put("name",assocTypeName.getValue());
                    assocTypeNamesJSON.put(assocTypeNameJSON);
                    foundAssocNames.add(assocTypeScopeLocator.getAddress()+"_"+assocTypeName.getValue());
                  }

                }//end for locators
              } //end for scopes

            }//end for names
            association.put("associationTypeNames",assocTypeNamesJSON);
          }//end if typenames


          JSONArray rolePlayers = new JSONArray();
          if(!associationTypePSI.equals("http://psi.udir.no/ontologi/aim_has_topic")){
            Collection<AssociationRoleIF> roles =  associationIF.getRoles();
            for (AssociationRoleIF role : roles) {
              JSONObject rolePlayerJSON = new JSONObject();
              TopicIF roleType = role.getType();
              Collection<LocatorIF> roleTypeLocators = roleType.getSubjectIdentifiers();
              String rolePSI = "";
              if(roleTypeLocators.size() == 1) {
                rolePSI = roleTypeLocators.iterator().next().getAddress();
              }
              else {
                int roleTypeCounter = 0;
                for(LocatorIF roleTypeLocator : roleTypeLocators) {
                  if(roleTypeCounter == 0) {
                    rolePSI = roleTypeLocator.getAddress();
                    break;
                  }
                }
              }
              rolePlayerJSON.put("roleType",rolePSI);


              TopicIF rolePlayer = role.getPlayer();
              Collection<LocatorIF> rolePlayerLocators = rolePlayer.getSubjectIdentifiers();
              String rolePlayerPSI = "";
              int rolePlayerCounter = 0;
              for(LocatorIF rolePlayerLocator : rolePlayerLocators) {
                if(rolePlayerLocator.getAddress().contains("psi.udir.no")) {
                  if(rolePlayerCounter == 0){
                    rolePlayerPSI = rolePlayerLocator.getAddress();
                    break;
                  }
                }
                else{
                  if(rolePlayerLocator.getAddress().contains("#topic-")){
                    rolePlayerPSI = rolePlayerLocator.getAddress();
                    break;
                  }
                }
              }

              rolePlayerJSON.put("rolePlayer",rolePlayerPSI);
              ArrayList<String> foundRoleNames = new ArrayList<>();
              if(rolePlayerPSI.toString().contains("#topic-")){
                Collection<TopicNameIF> rolePlayerNames = rolePlayer.getTopicNames();
                JSONArray rolePlayerNamesJSON = new JSONArray();

                if (rolePlayerNames.size() > 0) {
                  for (TopicNameIF rolePlayerName : rolePlayerNames) {
                    JSONObject rolePlayerNameJSON = new JSONObject();

                    Collection<TopicIF> rolePlayerNameScopes = rolePlayerName.getScope();
                    for (TopicIF rolePlayerNameScope : rolePlayerNameScopes) {
                      Collection<LocatorIF> rolePlayerNameScopeLocators = rolePlayerNameScope.getSubjectIdentifiers();
                      for (LocatorIF rolePlayerNameScopeLocator : rolePlayerNameScopeLocators) {
                        if(rolePlayerNameScopeLocator.getAddress().contains("iso/639")
                            && !foundRoleNames.contains(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue())){
                          rolePlayerNameJSON.put("language",rolePlayerNameScopeLocator.getAddress());
                          rolePlayerNameJSON.put("name",rolePlayerName.getValue());
                          rolePlayerNamesJSON.put(rolePlayerNameJSON);
                          foundRoleNames.add(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue());
                        }
                      }//end for locators
                    } //end for scopes

                  }//end for names
                }//end if typenames
                rolePlayerJSON.put("rolePlayerNames",rolePlayerNamesJSON);
              }

              rolePlayers.put(rolePlayerJSON);

            }


            association.put("rolePlayers",rolePlayers);
            associations.put(association);
          }

        }
        result.put("associations",associations);
        }//end if aimtopicassocs


    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage());
    }

    return result;
  }

    public JSONObject getTopicsAssociationsBySubjectId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
      JSONObject result = new JSONObject();
      JSONArray associations = new JSONArray();
      HashMap<String,ArrayList<HashMap<String,String>>> associationData = new HashMap<>();
      HashMap<String,ArrayList<HashMap<String,String>>> playerNameMap = new HashMap<>();
      ArrayList<String> aimRoles = new ArrayList<>();
      HashMap<String,String> rolePlayerMap = new HashMap<String,String>();

      TreeMap<String,ArrayList<String>> aimMap = new TreeMap<>();

      String siteIdentifier = site.toString().toLowerCase(); // TODO: Flagged for removal (BK).
      String query = String.format("association($COLUMN),reifies($TOPIC, $COLUMN), {udir-subject($TOPIC,\"http://psi.topic.ndla.no/#%s\")|subjectmatter($TOPIC,\"http://psi.topic.ndla.no/#%s\")} limit %s offset %s?", identifier, identifier, Integer.toString(limit), Integer.toString(offset));

      ArrayList<Object> assocs = executeObjectCollectionQuery(query);
      String lastAim = "";
      try{

        for( Object assoc : assocs) {
          HashMap<String, ArrayList<String>> currentTopicPlayers = new HashMap<>();
          ArrayList<String> currentTopics = new ArrayList<>();

          ArrayList<HashMap<String,ArrayList<String>>> roleArray = new ArrayList<>();
          AssociationIF associationIF = (AssociationIF) assoc;

          TopicIF associationType = associationIF.getType();
          Collection<LocatorIF> associationTypeLocators = associationType.getSubjectIdentifiers();
          String associationTypePSI = "";
          if(associationTypeLocators.size() == 1) {
            associationTypePSI = associationTypeLocators.iterator().next().getAddress();
          }
          else {
            int associationTypeCounter = 0;
            for(LocatorIF associationTypeLocator : associationTypeLocators) {
              if(associationTypeCounter == 0) {
                associationTypePSI = associationTypeLocator.getAddress();
                break;
              }
            }
          }


          Collection<TopicNameIF> assocTypeNames = associationType.getTopicNames();
          ArrayList<HashMap<String,String>> currentNameList = new ArrayList<>();

          if (assocTypeNames.size() > 0) {
            for (TopicNameIF assocTypeName : assocTypeNames) {
              HashMap<String,String> currentMap = new HashMap<>();
              Collection<TopicIF> assocTypeScopes = assocTypeName.getScope();
              String combined = "";
              for (TopicIF assocTypeScope : assocTypeScopes) {

                Collection<LocatorIF> assocTypeScopeLocators = assocTypeScope.getSubjectIdentifiers();
                for (LocatorIF assocTypeScopeLocator : assocTypeScopeLocators) {

                  if (assocTypeScopeLocator.getAddress().contains("iso")) {
                    currentMap.put(assocTypeScopeLocator.getAddress()+"_"+assocTypeName.getObjectId(),assocTypeName.getValue());
                  }

                  if(!assocTypeScopeLocator.getAddress().contains("iso") && !assocTypeScopeLocator.getAddress().contains("language-neutral"))
                  rolePlayerMap.put(assocTypeName.getObjectId(),assocTypeScopeLocator.getAddress());
                }//end for locators

              } //end for scopes

              currentNameList.add(currentMap);
            }//end for names

            associationData.put(associationTypePSI,currentNameList);
          }//end if typenames

          Collection<AssociationRoleIF> roles =  associationIF.getRoles();
          String rolePlayerPSI = "";
          String accumulate = "";
          for (AssociationRoleIF role : roles) {
            TopicIF roleType = role.getType();
            Collection<LocatorIF> roleTypeLocators = roleType.getSubjectIdentifiers();
            String rolePSI = "";

            if(roleTypeLocators.size() == 1) {
              rolePSI = roleTypeLocators.iterator().next().getAddress();
            }
            else {
              int roleTypeCounter = 0;
              for(LocatorIF roleTypeLocator : roleTypeLocators) {
                if(roleTypeCounter == 0) {
                  rolePSI = roleTypeLocator.getAddress();
                  break;
                }
              }
            }

            TopicIF rolePlayer = role.getPlayer();
            Collection<LocatorIF> rolePlayerLocators = rolePlayer.getSubjectIdentifiers();

            if(rolePlayerLocators.size() == 1) {
              rolePlayerPSI = rolePlayerLocators.iterator().next().getAddress();
            }
            else {
              int rolePlayerCounter = 0;
              for(LocatorIF rolePlayerLocator : rolePlayerLocators) {
                if(rolePlayerLocator.getAddress().contains("psi.udir.no")) {
                  if(rolePlayerCounter == 0){
                    rolePlayerPSI = rolePlayerLocator.getAddress();
                    break;
                  }
                }
                else{
                    if(rolePlayerLocator.getAddress().contains("#topic-")){
                      rolePlayerPSI = rolePlayerLocator.getAddress();
                      break;
                    }
                }
              }
            }
            accumulate += rolePlayerPSI+"_"+associationTypePSI+"_"+rolePSI+"§";

          Collection<TopicNameIF> rolePlayerNames = rolePlayer.getTopicNames();
          ArrayList<HashMap<String,String>> currentRoleNameList = new ArrayList<>();

          if (rolePlayerNames.size() > 0) {
            for (TopicNameIF rolePlayerName : rolePlayerNames) {
              HashMap<String,String> currentNameMapper = new HashMap<>();
              Collection<TopicIF> rolePlayerNameScopes = rolePlayerName.getScope();
              for (TopicIF rolePlayerNameScope : rolePlayerNameScopes) {
                Collection<LocatorIF> rolePlayerNameScopeLocators = rolePlayerNameScope.getSubjectIdentifiers();

                for (LocatorIF rolePlayerNameScopeLocator : rolePlayerNameScopeLocators) {
                  if(rolePlayerNameScopeLocator.getAddress().length() > 0){
                    //currentNameMapper.put("language",rolePlayerNameScopeLocator.getAddress());
                    currentNameMapper.put(rolePlayerNameScopeLocator.getAddress(),rolePlayerName.getValue());
                  }
                }//end for locators
              } //end for scopes
              if(currentNameMapper.size() > 1){
                Iterator cMapIt = currentNameMapper.keySet().iterator();
                while(cMapIt.hasNext()){
                  HashMap<String,String> newCurrentNameMapper = new HashMap<>();
                  String cMapLang = (String) cMapIt.next();
                  newCurrentNameMapper.put(cMapLang,currentNameMapper.get(cMapLang));
                  currentRoleNameList.add(newCurrentNameMapper);
                }
              }
              else{
                currentRoleNameList.add(currentNameMapper);
              }

            }//end for names
            playerNameMap.put(rolePlayerPSI,currentRoleNameList);
          }//end if typenames



          }//end for roles
          aimRoles.add(accumulate);
        }

        Collections.sort(aimRoles);
        Collections.reverse(aimRoles);

        for(String aimRole : aimRoles) {
          String[] roleArray = aimRole.split("§");
          String role1 = roleArray[0];
          String role2 = roleArray[1];

          String[] httpArray1 = role1.split("_http");
          String[] httpArray2 = role2.split("_http");

          String topicId1 = httpArray1[0];
          String assocId1 = httpArray1[1];
          String roleId1 = httpArray1[2];

          String topicId2 = httpArray2[0];
          String assocId2 = httpArray2[1];
          String roleId2 = httpArray2[2];

          if(assocId1.equals(assocId2)) {
            //JSONObject assocObject = new JSONObject();
            String aimId = "";
            String topicId = "";
            String assocId = assocId1;
            String thisRoleId = "";
            String thatRoleId = "";

            if(topicId1.contains("kl06")){
              aimId = topicId1;
              topicId = topicId2;
              thisRoleId = roleId1;
              thatRoleId = roleId2;
            }
            else if(topicId2.contains("kl06")){
              aimId = topicId2;
              topicId = topicId1;
              thisRoleId = roleId2;
              thatRoleId = roleId1;
            }
            else if(topicId2.contains("#topic-")){
              topicId = topicId2;
              aimId = topicId1;
              thisRoleId = roleId2;
              thatRoleId = roleId1;
            }
            else if(topicId1.contains("#topic-")){
              aimId = topicId2;
              topicId = topicId1;
              thisRoleId = roleId1;
              thatRoleId = roleId2;
            }

            if(!aimMap.containsKey(aimId)){
              aimMap.put(aimId,new ArrayList<String>());
            }
            aimMap.get(aimId).add("http"+thisRoleId+"_"+"http"+assocId+"_"+topicId+"_"+"http"+thatRoleId);

          }

        }

        Iterator<String> it = aimMap.keySet().iterator();
        while(it.hasNext()) {
          JSONObject aimJSON = new JSONObject();
          String aimId = (String) it.next();
          if(aimId != ""){
            aimJSON.put("aimId",aimId);
            JSONArray aimNames = new JSONArray();
            ArrayList<HashMap<String,String>> aimNameMapping = playerNameMap.get(aimId);

            for(HashMap<String,String> aimLangNames : aimNameMapping) {
              Iterator<String> aimLangNamesIterator = aimLangNames.keySet().iterator();
              JSONObject aimNameObject = new JSONObject();
              while (aimLangNamesIterator.hasNext()){
                String aimNameLanguage = (String) aimLangNamesIterator.next();
                aimNameObject.put(aimNameLanguage,aimLangNames.get(aimNameLanguage));
              }

              if(aimNameObject.length() > 0) {
                aimNames.put(aimNameObject);
              }
            }

            aimJSON.put("aimNames",aimNames);


            ArrayList<String> data = aimMap.get(aimId);
            JSONArray assocJSON = new JSONArray();
            for(String assocData : data) {
              String[] assocArray = assocData.split("_http");
              JSONObject assocObject = new JSONObject();
              String thisRoleId = assocArray[0];

              String assocId = "http"+assocArray[1];
              assocObject.put("associationType",assocId);
              JSONArray assocTypeNames = new JSONArray();

              ArrayList<HashMap<String,String>> typeNameMap = associationData.get(assocId);
              for(HashMap<String,String> typeLangNames : typeNameMap){
                Iterator<String> typeLangNamesIterator = typeLangNames.keySet().iterator();
                JSONObject typeNameObject = new JSONObject();
                while (typeLangNamesIterator.hasNext()){
                  String typeNameLanguage = (String) typeLangNamesIterator.next();
                  String[] typeNameLanguageArray = typeNameLanguage.split("_");
                  typeNameObject.put("language",typeNameLanguageArray[0]);
                  typeNameObject.put("role",rolePlayerMap.get(typeNameLanguageArray[1]));
                  typeNameObject.put("name",typeLangNames.get(typeNameLanguage));
                }
                assocTypeNames.put(typeNameObject);
              }
              assocObject.put("associationTypeNames",assocTypeNames);

              assocObject.put("rolePlayedByAim",thisRoleId);
              String topicId = "http"+assocArray[2];
              assocObject.put("topicId",topicId);


              JSONArray topicNames = new JSONArray();
              ArrayList<HashMap<String,String>> topicNameMapping = playerNameMap.get(topicId);
              ArrayList<String> foundTopicNames = new ArrayList<>();

              for(HashMap<String,String> topicLangNames : topicNameMapping) {
                Iterator<String> topicLangNamesIterator = topicLangNames.keySet().iterator();
                JSONObject topicNameObject = new JSONObject();
                while (topicLangNamesIterator.hasNext()){
                  String topicNameLanguage = (String) topicLangNamesIterator.next();
                  if(!foundTopicNames.contains(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage))) {
                    topicNameObject.put(topicNameLanguage,topicLangNames.get(topicNameLanguage));
                    foundTopicNames.add(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage));
                  }
                }

                if(topicNameObject.length() > 0) {
                  topicNames.put(topicNameObject);
                }
              }
              HashMap<String,String> existingApprovedState = Utils.getApprovedState(topicId,_configFile,_topicmapReferenceKey);
              String existingApproved = existingApprovedState.get("approved");
              String existingApprovalDate = existingApprovedState.get("approval_date");

              if(null != existingApproved && existingApproved.length() > 0) {
                assocObject.put("approved",existingApproved);
                if(null != existingApprovalDate) {
                  assocObject.put("approval_date",existingApprovalDate);
                }
              }
              else{
                assocObject.put("approved","false");
              }

              HashMap<String,String> existingProcessState = Utils.getProcessState(topicId,_configFile,_topicmapReferenceKey);
              if(existingProcessState.size() > 0){
                assocObject.put("processState",existingProcessState.get("processState"));
              }
              else{
                assocObject.put("processState","0");
              }

              assocObject.put("topicNames",topicNames);



              int assocArrayLength = (assocArray.length - 1);
              String thatRoleId = "http"+assocArray[assocArrayLength];
              assocObject.put("rolePlayedByTopic",thatRoleId);
              assocJSON.put(assocObject);
            }
            aimJSON.put("relations",assocJSON);
            associations.put(aimJSON);
          }//end if aimId not empty

        }//end while it.hasNext()


        result.put("associations",associations);
      } catch (JSONException e) {
        throw new NdlaServiceException(e.getMessage());
      }

      return result;
    }



    public JSONObject getAimTopicAssociations(String aimId, String lang)  throws NdlaServiceException, BadObjectReferenceException{
      JSONObject result = new JSONObject();
      JSONArray associations = new JSONArray();
      try {

        String query = String.format("select $COLUMN from role-player($ROLE1, %s),association-role($COLUMN, $ROLE1),association-role($COLUMN, $ROLE2),role-player($ROLE2, $topic2)?",aimId);
        ArrayList<Object> assocs = executeObjectCollectionQuery(query);
        for( Object assoc : assocs) {
          JSONObject association = new JSONObject();
          AssociationIF associationIF = (AssociationIF) assoc;

          TopicIF associationType = associationIF.getType();
          Collection<LocatorIF> associationTypeLocators = associationType.getSubjectIdentifiers();
          String associationTypePSI = "";
          if(associationTypeLocators.size() == 1) {
            associationTypePSI = associationTypeLocators.iterator().next().getAddress();
          }
          else {
            int associationTypeCounter = 0;
            for(LocatorIF associationTypeLocator : associationTypeLocators) {
              if(associationTypeCounter == 0) {
                associationTypePSI = associationTypeLocator.getAddress();
                break;
              }
            }
          }
          association.put("associationType",associationTypePSI);

          Collection<TopicNameIF> assocTypeNames = associationType.getTopicNames();
          JSONArray assocTypeNamesJSON = new JSONArray();

          if (assocTypeNames.size() > 0) {
            HashMap<String,String> assocNameMap = new HashMap<>();
            JSONObject assocTypeNameJSON = new JSONObject();
            for (TopicNameIF assocTypeName : assocTypeNames) {


              Collection<TopicIF> assocTypeScopes = assocTypeName.getScope();
              for (TopicIF assocTypeScope : assocTypeScopes) {
                Collection<LocatorIF> assocTypeScopeLocators = assocTypeScope.getSubjectIdentifiers();
                for (LocatorIF assocTypeScopeLocator : assocTypeScopeLocators) {
                  assocNameMap.put(assocTypeScopeLocator.getAddress(),assocTypeName.getValue());
                }//end for locators
              } //end for scopes
            }//end for names

            String assocLanguageString = "";
            String assocTypeNameString = "";


            if(assocNameMap.containsKey(lang)){
              assocLanguageString = lang;
              assocTypeNameString = assocNameMap.get(lang);
            }
            else{
              if(assocNameMap.containsKey("http://psi.oasis-open.org/iso/639/#nob") && assocLanguageString == "" && assocTypeNameString == ""){
                assocLanguageString = "http://psi.oasis-open.org/iso/639/#nob";
                assocTypeNameString = assocNameMap.get("http://psi.oasis-open.org/iso/639/#nob");
              }
              else if(assocNameMap.containsKey("http://psi.oasis-open.org/iso/639/#nno") && assocLanguageString == "" && assocTypeNameString == ""){
                assocLanguageString = "http://psi.oasis-open.org/iso/639/#nno";
                assocTypeNameString = assocNameMap.get("http://psi.oasis-open.org/iso/639/#nno");
              }
              else if(assocNameMap.containsKey("http://psi.oasis-open.org/iso/639/#eng") && assocLanguageString == "" && assocTypeNameString == ""){
                assocLanguageString = "http://psi.oasis-open.org/iso/639/#eng";
                assocTypeNameString = assocNameMap.get("http://psi.oasis-open.org/iso/639/#eng");
              }
              else if(assocNameMap.containsKey("http://psi.topic.ndla.no/#language-neutral") && assocLanguageString == "" && assocTypeNameString == ""){
                assocLanguageString = "http://psi.topic.ndla.no/#language-neutral";
                assocTypeNameString = assocNameMap.get("http://psi.topic.ndla.no/#language-neutral");
              }
            }
            if(assocLanguageString != "" && assocTypeNameString != ""){
              assocTypeNameJSON.put("language",assocLanguageString);
              assocTypeNameJSON.put("name",assocTypeNameString);
              assocTypeNamesJSON.put(assocTypeNameJSON);
            }
            association.put("associationTypeNames",assocTypeNamesJSON);
          }//end if typenames


          JSONArray rolePlayers = new JSONArray();
          if(!associationTypePSI.equals("http://psi.udir.no/ontologi/aim_has_topic")){
            Collection<AssociationRoleIF> roles =  associationIF.getRoles();
            for (AssociationRoleIF role : roles) {
              JSONObject rolePlayerJSON = new JSONObject();
              TopicIF roleType = role.getType();
              Collection<LocatorIF> roleTypeLocators = roleType.getSubjectIdentifiers();
              String rolePSI = "";
              if(roleTypeLocators.size() == 1) {
                rolePSI = roleTypeLocators.iterator().next().getAddress();
              }
              else {
                int roleTypeCounter = 0;
                for(LocatorIF roleTypeLocator : roleTypeLocators) {
                  if(roleTypeCounter == 0) {
                    rolePSI = roleTypeLocator.getAddress();
                    break;
                  }
                }
              }
              rolePlayerJSON.put("roleType",rolePSI);


              TopicIF rolePlayer = role.getPlayer();
              Collection<LocatorIF> rolePlayerLocators = rolePlayer.getSubjectIdentifiers();
              String rolePlayerPSI = "";
              int rolePlayerCounter = 0;
              for(LocatorIF rolePlayerLocator : rolePlayerLocators) {
                if(rolePlayerLocator.getAddress().contains("psi.udir.no")) {
                  if(rolePlayerCounter == 0){
                    rolePlayerPSI = rolePlayerLocator.getAddress();
                    break;
                  }
                }
                else{
                  if(rolePlayerLocator.getAddress().contains("#topic-")){
                    rolePlayerPSI = rolePlayerLocator.getAddress();
                    break;
                  }
                }
              }

              rolePlayerJSON.put("rolePlayer",rolePlayerPSI);

              if(rolePlayerPSI.toString().contains("#topic-")){
                Collection<TopicNameIF> rolePlayerNames = rolePlayer.getTopicNames();
                JSONArray rolePlayerNamesJSON = new JSONArray();

                if (rolePlayerNames.size() > 0) {
                  HashMap<String,String> nameMap = new HashMap<>();
                  JSONObject rolePlayerNameJSON = new JSONObject();
                  for (TopicNameIF rolePlayerName : rolePlayerNames) {

                    Collection<TopicIF> rolePlayerNameScopes = rolePlayerName.getScope();
                    for (TopicIF rolePlayerNameScope : rolePlayerNameScopes) {
                      Collection<LocatorIF> rolePlayerNameScopeLocators = rolePlayerNameScope.getSubjectIdentifiers();
                      for (LocatorIF rolePlayerNameScopeLocator : rolePlayerNameScopeLocators) {
                        nameMap.put(rolePlayerNameScopeLocator.getAddress(),rolePlayerName.getValue());
                      }//end for locators
                    } //end for scopes
                  }//end for names
                  String rolePlayerLanguageString = "";
                  String rolePlayerNameString = "";
                  if(nameMap.containsKey(lang)){
                    rolePlayerLanguageString = lang;
                    rolePlayerNameString = nameMap.get(lang);
                  }
                  else{
                    if(nameMap.containsKey("http://psi.oasis-open.org/iso/639/#nob") && rolePlayerLanguageString == "" && rolePlayerNameString == ""){
                      rolePlayerLanguageString = "http://psi.oasis-open.org/iso/639/#nob";
                      rolePlayerNameString = nameMap.get("http://psi.oasis-open.org/iso/639/#nob");
                    }
                    else if(nameMap.containsKey("http://psi.oasis-open.org/iso/639/#nno") && rolePlayerLanguageString == "" && rolePlayerNameString == ""){
                      rolePlayerLanguageString = "http://psi.oasis-open.org/iso/639/#nno";
                      rolePlayerNameString = nameMap.get("http://psi.oasis-open.org/iso/639/#nno");
                    }
                    else if(nameMap.containsKey("http://psi.oasis-open.org/iso/639/#eng") && rolePlayerLanguageString == "" && rolePlayerNameString == ""){
                      rolePlayerLanguageString = "http://psi.oasis-open.org/iso/639/#eng";
                      rolePlayerNameString = nameMap.get("http://psi.oasis-open.org/iso/639/#eng");
                    }
                    else if(nameMap.containsKey("http://psi.topic.ndla.no/#language-neutral") && rolePlayerLanguageString == "" && rolePlayerNameString == ""){
                      rolePlayerLanguageString = "http://psi.topic.ndla.no/#language-neutral";
                      rolePlayerNameString = nameMap.get("http://psi.topic.ndla.no/#language-neutral");
                    }
                  }
                  if(rolePlayerLanguageString != "" && rolePlayerNameString != ""){
                    rolePlayerNameJSON.put("language",rolePlayerLanguageString);
                    rolePlayerNameJSON.put("name",rolePlayerNameString);
                    rolePlayerNamesJSON.put(rolePlayerNameJSON);
                  }
                }//end if typenames
                rolePlayerJSON.put("rolePlayerNames",rolePlayerNamesJSON);
              }

              rolePlayers.put(rolePlayerJSON);

            }


            association.put("rolePlayers",rolePlayers);
            associations.put(association);
          }

        }
        result.put("associations",associations);
      } catch (JSONException e) {
        throw new NdlaServiceException(e.getMessage());
      }

      return result;
    }

  public JSONArray mergeDuplicateTopics(String data)throws NdlaServiceException, BadObjectReferenceException{
    JSONArray result = new JSONArray();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;

    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      TopicMapBuilderIF builder = topicMap.getBuilder();

      JSONArray payload = new JSONArray(data);
      for(int i = 0; i < payload.length();i++){
        JSONObject duplicateData = payload.getJSONObject(i);
        JSONObject processedData = new JSONObject();
        String toTopicId = duplicateData.getString("toTopic");

        String type = duplicateData.getString("type");
        TopicIF toTopicIF = null;
        TopicIF assocType = null;

        TopicIF player1RoleTopic = null;
        TopicIF player2RoleTopic = null;

        String player1Psi = "";
        if(type.equals("keyword")){
          player1Psi =  "http://psi.topic.ndla.no/keywords/#"+toTopicId;
          toTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(player1Psi));
          player1RoleTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#keyword"));
        }
        else{
          player1Psi =  "http://psi.topic.ndla.no/topics/#"+toTopicId;
          toTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(player1Psi));
          player1RoleTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#topic"));
        }

        if(null != toTopicIF){
          processedData.put("toTopic",toTopicId);
          JSONArray processedTopics = new JSONArray();

          JSONArray fromTopics = duplicateData.getJSONArray("fromTopics");
          for(int j = 0; j < fromTopics.length();j++){
            JSONObject fromTopic = fromTopics.getJSONObject(j);
            String fromTopicId = fromTopic.getString("topicId");

            if(fromTopicId.contains("fyr")){
              assocType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#nygivnode-keyword"));
            }
            else{
              assocType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#ndlanode-keyword"));
            }

            TopicIF fromTopicIF = null;
            if(type.equals("keyword")){
              fromTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#"+fromTopicId));
            }
            else{
              fromTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#"+fromTopicId));
            }

            if(null != fromTopicIF){
              JSONObject processedFromTopic = new JSONObject();
              processedFromTopic.put("fromTopicId",fromTopicId);

              Collection<AssociationIF> fromAssociations = fromTopicIF.getAssociationsByType(assocType);
              JSONArray processedFromAssociations = new JSONArray();

              for(AssociationIF associationIF : fromAssociations) {
                TopicIF associationType = associationIF.getType();
                AssociationIF association = builder.makeAssociation(associationType);

                String associationTypePSI = "";
                Collection<LocatorIF> associationLocators = associationType.getSubjectIdentifiers();
                int k = 0;
                for(LocatorIF associationLocator : associationLocators){
                  if(k == 0){
                    associationTypePSI = associationLocator.getAddress();
                  }
                  k++;
                }//end for locators

                JSONObject processedFromAssociation = new JSONObject();
                String player2Psi = "";
                if(associationTypePSI.contains("#delingnode-keyword")){
                  player2Psi = "http://psi.topic.ndla.no/#deling-node";
                }
                else if(associationTypePSI.contains("#nygivnode-keyword")){
                  player2Psi = "http://psi.topic.ndla.no/#nygiv-node";
                }
                else if(associationTypePSI.contains("#ndlanode-keyword")){
                  player2Psi = "http://psi.topic.ndla.no/#ndla-node";
                }
                player2RoleTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(player2Psi));
                processedFromAssociation.put("assocationType",associationTypePSI);
                processedFromAssociation.put("role1",player1Psi);
                processedFromAssociation.put("role2",player2Psi);

                Collection<AssociationRoleIF> associationRoleIFs = associationIF.getRolesByType(player2RoleTopic);
                for(AssociationRoleIF associationRoleIF : associationRoleIFs){
                  TopicIF rolePlayer = associationRoleIF.getPlayer();
                  String rolePlayer2Psi = "";
                  Collection<LocatorIF> playerPsis = rolePlayer.getSubjectIdentifiers();
                  rolePlayer2Psi = playerPsis.iterator().next().getAddress();
                  processedFromAssociation.put("role2Player",rolePlayer2Psi);
                  /* Make the player2 related to the player1 with the role of roleplayer2 */
                  builder.makeAssociationRole(association, player2RoleTopic, rolePlayer);

                  /* Make the player1 related to the player2 with the role of roleplayer1 */
                  builder.makeAssociationRole(association, player1RoleTopic, toTopicIF);
                }//end for associationRoleIFs

                //now delete the fromTopics which will delete all associations to it as well

                processedFromAssociations.put(processedFromAssociation);
              }//end for associations
              //fromTopicIF.getAssociationsByType()
              processedFromTopic.put("processedFromAssocations",processedFromAssociations);
              processedTopics.put(processedFromTopic);
              DeletionUtils.removeDependencies(fromTopicIF);
              fromTopicIF.remove();
            }//end if fromTopic not null
          }
          processedData.put("processedData",processedTopics);
        }//end if totopic exists
        result.put(processedData);
      }
      store.commit();
    }
    catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage());
      }
    }
    finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return result;
  }

  public JSONArray getDuplicateTopics(int limit, int offset, String typePsi)throws NdlaServiceException, BadObjectReferenceException{
    JSONArray result = new JSONArray();
    JSONArray limitedResult = new JSONArray();
    HashMap<String,ArrayList<NdlaTopic>> duplicates = new HashMap<>();
    ArrayList<String> foundPsis = new ArrayList<>();

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    HashMap<String,HashMap<String,NdlaTopic>> foundNames = new HashMap<>();

    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      TopicIF typeTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(typePsi));

      Collection topics = topicMap.getTopics();
      Iterator it = topics.iterator();
      //check all the topics.

      while (it.hasNext()) {
        TopicIF topic = (TopicIF)it.next();
        Collection<TopicIF> types = topic.getTypes();
        if(types.contains(typeTopicIF)){
          QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
          QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);
          NdlaTopic currentNdlaTopic = new NdlaTopic(topic,queryProcessor);

          //HashMap<String,String> names = currentNdlaTopic.getNames();
          HashMap<String,String> names = null;
          String wordClass = "";
          //if(null == names || names.size() == 0){
          ArrayList<NdlaWord> ndlaWords = currentNdlaTopic.getWordNames();
          if(ndlaWords != null){
            for(NdlaWord ndlaWord : ndlaWords) {
              names = ndlaWord.getNames();
              wordClass = ndlaWord.getWordClass();
            }
          }
          else{
            names = currentNdlaTopic.getNames();
          }

          if(names != null) {
            Iterator nameIterator = names.keySet().iterator();

            while(nameIterator.hasNext()){
              String languageString = (String) nameIterator.next();
              String nameString = names.get(languageString);
              String key = languageString+"§"+wordClass;

              if(foundNames.containsKey(nameString)){
                  HashMap<String,NdlaTopic> foundData = foundNames.get(nameString);

                  if(foundData.containsKey(key)){
                    NdlaTopic foundNdlaTopic = foundData.get(key);
                    if(duplicates.containsKey(nameString)){
                      ArrayList<NdlaTopic> tempList = duplicates.get(nameString);
                      tempList.add(foundNdlaTopic);
                      tempList.add(currentNdlaTopic);
                      duplicates.put(nameString,tempList);
                    }
                    else{
                      ArrayList<NdlaTopic> tempList = new ArrayList<>();
                      tempList.add(foundNdlaTopic);
                      tempList.add(currentNdlaTopic);
                      duplicates.put(nameString,tempList);
                    }
                  }



              }
              else{

                HashMap<String,NdlaTopic> tempMap = new HashMap<>();
                tempMap.put(key, currentNdlaTopic);
                foundNames.put(nameString,tempMap);

              }

            }
          }
        }//end if correct type
      }//end while topics



      Iterator duplicateIterator = duplicates.keySet().iterator();
      while(duplicateIterator.hasNext()){

          String duplicateString = (String) duplicateIterator.next();
          ArrayList<NdlaTopic> duplicateList = duplicates.get(duplicateString);
          JSONObject currentDuplicateObject = new JSONObject();
          currentDuplicateObject.put("duplicateString",duplicateString);
          JSONArray duplicateListJSON = new JSONArray();
          for(NdlaTopic duplicate : duplicateList){
            boolean alreadyExists = false;
            JSONObject currentNdlaTopicJSON = new JSONObject();
            ArrayList<String> psis = duplicate.getPsis();
            String approvedState = duplicate.getApproved();
            String approvalDate = duplicate.getApprovalDate();
            String processState = duplicate.getProcessState();
            currentNdlaTopicJSON.put("approvedState",approvedState);
            currentNdlaTopicJSON.put("approvalDate",approvalDate);
            currentNdlaTopicJSON.put("processState",processState);

            JSONArray JSONPsis = new JSONArray();
            for(String psi : psis){
              if(foundPsis.contains(psi)){
                alreadyExists = true;
              }
              JSONPsis.put(psi);
              foundPsis.add(psi);
            }
            currentNdlaTopicJSON.put("psis",JSONPsis);
            String wordClass = "";
            ArrayList<NdlaWord> ndlaWords = duplicate.getWordNames();
            JSONArray nameJSONArray = new JSONArray();
            if(ndlaWords != null){
              for(NdlaWord ndlaWord : ndlaWords) {
                JSONObject currentName = new JSONObject();
                HashMap<String,String> names = ndlaWord.getNames();
                Iterator nameIterator = names.keySet().iterator();
                while (nameIterator.hasNext()){
                  String langString = (String) nameIterator.next();
                  String nameString = names.get(langString);
                  currentName.put("language",langString);
                  currentName.put("name",nameString);
                }
                nameJSONArray.put(currentName);
                wordClass = ndlaWord.getWordClass();
              }
            }
            else {
              System.out.println("NONDLAWORDS");
            }
            currentNdlaTopicJSON.put("names",nameJSONArray);
            currentNdlaTopicJSON.put("wordClass",wordClass);


            if(!alreadyExists){
              duplicateListJSON.put(currentNdlaTopicJSON);
            }
          }
          currentDuplicateObject.put("duplicateObjects",duplicateListJSON);
          result.put(currentDuplicateObject);


      }


      for(int i = offset; i < (offset+limit); i++){
        JSONObject tempObject = (JSONObject) result.get(i);
        limitedResult.put(tempObject);
      }
    }
    catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage());
      }
    }
    finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }


    return limitedResult;
  }

  public JSONObject lookUpTopicsBySearchTerm(String search, String type, String searchType, String language, int limit, int offset)   throws NdlaServiceException, BadObjectReferenceException {
    JSONObject result = new JSONObject();

    String typeString = "";
    if(searchType.equals("any")){
      typeString = "str:contains($VAL, \""+search+"\")";
    }
    else if(searchType.equals("start")){
      typeString = "str:starts-with($VAL, \""+search+"\")";
    }
    else if(searchType.equals("end")){
      typeString = "str:ends-with($VAL, \""+search+"\")";
    }

    String query = String.format("import \"http://psi.ontopia.net/tolog/string/\" as str select $COLUMN from " +
        "topic-name($COLUMN,$NAME)," +
        "scope($NAME,i\"%s\")," +
        "value($NAME,$VAL)," +
        "instance-of($COLUMN,%s), %s" +
        " ORDER BY $COLUMN limit %s offset %s?",language,type,typeString,limit,offset);
    ArrayList<NdlaTopic> topics = executeCollectionQuery(query);


    JSONArray resultTopics = new JSONArray();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try{
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();

      for(int i = 0; i < topics.size(); i++){
        NdlaTopic topic = topics.get(i);
        JSONObject resultTopic = new JSONObject();
        HashMap<String,String> topicNames = topic.getNames();
        HashMap<String,String> topicNames2 = topic.getNames();

        if(null == topicNames){
          topicNames = new HashMap<>();
          topicNames2 = new HashMap<>();
          ArrayList<NdlaWord> topicWordNames = topic.getWordNames();
          for(NdlaWord topicWordName : topicWordNames) {
            HashMap<String,String> wordNameMap = topicWordName.getNames();
            Iterator wit = wordNameMap.keySet().iterator();
            while(wit.hasNext()){
              String wnLang = (String) wit.next();
              String wnName = (String) wordNameMap.get(wnLang);
              topicNames.put(wnLang,wnName);
              topicNames2.put(wnLang,wnName);
            }
          }
        }
        boolean hasName = false;
        Iterator<String> nameIterator = topicNames.keySet().iterator();
        while(nameIterator.hasNext()){
          String langKey = (String) nameIterator.next();
          String nameString = topicNames.get(langKey);
          if(nameString.trim().toLowerCase().contains(search.trim().toLowerCase())){
            hasName = true;
            break;
          }
        }

        if(hasName) {
          Iterator<String> nameIterator2 = topicNames2.keySet().iterator();
          JSONArray JSONtopicNames = new JSONArray();
          while(nameIterator2.hasNext()) {
            String langKey = (String) nameIterator2.next();
            String nameString = topicNames2.get(langKey);
            JSONObject JSONtopicName = new JSONObject();
            JSONtopicName.put("language", langKey);
            JSONtopicName.put("name", nameString);
            JSONtopicNames.put(JSONtopicName);
          }
          resultTopic.put("names",JSONtopicNames);

          String topicId = "";
          String topicPSI = "";
          ArrayList<String> psis = topic.getPsis();
          for(String psi : psis) {
            if(type.equals("topic")){
              if(psi.contains("#topic-")){
                topicPSI = psi;
                topicId = psi.substring(psi.lastIndexOf("#")+1);
                break;
              }
            }
            else if(type.equals("keyword")){
              if(psi.contains("#keyword-") || psi.contains("#fyr-keyword-")){
                topicPSI = psi;
                topicId = psi.substring(psi.lastIndexOf("#")+1);
                break;
              }
            }

          }
          if(topicId != ""){
            resultTopic.put("topicId",topicId);
          }

          if(topicPSI != ""){
            TopicIF topicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(topicPSI));

            TopicIF approvedOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
            TopicIF approvedDateOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));
            Collection<OccurrenceIF> approvedOccurrenceIFs = topicIF.getOccurrencesByType(approvedOccurrenceType);
            String approvedString = "false";
            for(OccurrenceIF approvedOccurrenceIF : approvedOccurrenceIFs){
              if(approvedOccurrenceIF.getValue().equals("true")) {
                approvedString = "true";
                break;
              }
            }

            resultTopic.put("approved",approvedString);
            if(approvedString.equals("true")){
              String approvalDate = "";
              Collection<OccurrenceIF> approvedDateOccurrencesIFs = topicIF.getOccurrencesByType(approvedDateOccurrenceType);
              for(OccurrenceIF approvedDateOccurrencesIF : approvedDateOccurrencesIFs){
                if(approvedDateOccurrencesIF.getValue() != "") {
                  approvalDate = approvedDateOccurrencesIF.getValue();
                  break;
                }
              }

              if(approvalDate != ""){
                resultTopic.put("approval_date",approvalDate);
              }
            }

            TopicIF processOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#process-state"));
            Collection<OccurrenceIF> processOccurrenceIFs = topicIF.getOccurrencesByType(processOcctype);
            String processState = "";
            for(OccurrenceIF processOccurrenceIF : processOccurrenceIFs){
              if(processOccurrenceIF.getValue() != "") {
                processState = processOccurrenceIF.getValue();
                break;
              }
            }

            if(processOccurrenceIFs.size() > 0){
              resultTopic.put("processState",processState);
            }
            else{
              resultTopic.put("processState","0");
            }
          }


          resultTopics.put(resultTopic);
        }
      }

      result.put("topics", resultTopics);
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage());
      }
    }
    finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }



    return result;
  }

    public JSONObject lookUpTopicsBySearchTerm(String search, String type)   throws NdlaServiceException, BadObjectReferenceException {
      JSONObject result = new JSONObject();
      ArrayList<NdlaTopic> topics = null;


      if(type.equals("topic")) {
        topics = getTopics(0, 0);
      }
      else if(type.equals("keyword")){
        topics = getKeywords(0,0);
      }


      JSONArray resultTopics = new JSONArray();
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try{
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap();

        for(int i = 0; i < topics.size(); i++){
          NdlaTopic topic = topics.get(i);
          JSONObject resultTopic = new JSONObject();
          HashMap<String,String> topicNames = topic.getNames();
          HashMap<String,String> topicNames2 = topic.getNames();

          if(null == topicNames){
            topicNames = new HashMap<>();
            topicNames2 = new HashMap<>();
            ArrayList<NdlaWord> topicWordNames = topic.getWordNames();
            for(NdlaWord topicWordName : topicWordNames) {
              HashMap<String,String> wordNameMap = topicWordName.getNames();
              Iterator wit = wordNameMap.keySet().iterator();
              while(wit.hasNext()){
                String wnLang = (String) wit.next();
                String wnName = (String) wordNameMap.get(wnLang);
                topicNames.put(wnLang,wnName);
                topicNames2.put(wnLang,wnName);
              }
            }
          }
          boolean hasName = false;
          Iterator<String> nameIterator = topicNames.keySet().iterator();
          while(nameIterator.hasNext()){
            String langKey = (String) nameIterator.next();
            String nameString = topicNames.get(langKey);
            if(nameString.trim().toLowerCase().contains(search.trim().toLowerCase())){
              hasName = true;
              break;
            }
          }

          if(hasName) {
            Iterator<String> nameIterator2 = topicNames2.keySet().iterator();
            JSONArray JSONtopicNames = new JSONArray();
            while(nameIterator2.hasNext()) {
              String langKey = (String) nameIterator2.next();
              String nameString = topicNames2.get(langKey);
              JSONObject JSONtopicName = new JSONObject();
              JSONtopicName.put("language", langKey);
              JSONtopicName.put("name", nameString);
              JSONtopicNames.put(JSONtopicName);
            }
            resultTopic.put("names",JSONtopicNames);

            String topicId = "";
            String topicPSI = "";
            ArrayList<String> psis = topic.getPsis();
            for(String psi : psis) {
              if(type.equals("topic")){
                if(psi.contains("#topic-")){
                  topicPSI = psi;
                  topicId = psi.substring(psi.lastIndexOf("#")+1);
                  break;
                }
              }
              else if(type.equals("keyword")){
                if(psi.contains("#keyword-")){
                  topicPSI = psi;
                  topicId = psi.substring(psi.lastIndexOf("#")+1);
                  break;
                }
              }

            }
            if(topicId != ""){
              resultTopic.put("topicId",topicId);
            }

            if(topicPSI != ""){
              TopicIF topicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(topicPSI));

              TopicIF approvedOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
              TopicIF approvedDateOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));
              Collection<OccurrenceIF> approvedOccurrenceIFs = topicIF.getOccurrencesByType(approvedOccurrenceType);
              String approvedString = "false";
              for(OccurrenceIF approvedOccurrenceIF : approvedOccurrenceIFs){
                if(approvedOccurrenceIF.getValue().equals("true")) {
                  approvedString = "true";
                  break;
                }
              }

              resultTopic.put("approved",approvedString);
              if(approvedString.equals("true")){
                String approvalDate = "";
                Collection<OccurrenceIF> approvedDateOccurrencesIFs = topicIF.getOccurrencesByType(approvedDateOccurrenceType);
                for(OccurrenceIF approvedDateOccurrencesIF : approvedDateOccurrencesIFs){
                  if(approvedDateOccurrencesIF.getValue() != "") {
                    approvalDate = approvedDateOccurrencesIF.getValue();
                    break;
                  }
                }

                if(approvalDate != ""){
                  resultTopic.put("approval_date",approvalDate);
                }
              }

              TopicIF processOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#process-state"));
              Collection<OccurrenceIF> processOccurrenceIFs = topicIF.getOccurrencesByType(processOcctype);
              String processState = "";
              for(OccurrenceIF processOccurrenceIF : processOccurrenceIFs){
                if(processOccurrenceIF.getValue() != "") {
                  processState = processOccurrenceIF.getValue();
                  break;
                }
              }

              if(processOccurrenceIFs.size() > 0){
                resultTopic.put("processState",processState);
              }
              else{
                resultTopic.put("processState","0");
              }
            }


            resultTopics.put(resultTopic);
          }
        }

        result.put("topics", resultTopics);
      } catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          throw new NdlaServiceException(e.getMessage());
        }
      }
      finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }

      return result;
    }


  public ArrayList<String> saveTopicsByNodeId(String nodeId, String topics, NdlaSite site) throws NdlaServiceException,BadObjectReferenceException {
    ArrayList<String> result = new ArrayList<String>();

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      String relationType = "http://psi.topic.ndla.no/#";
      String player1Type = "http://psi.topic.ndla.no/#";
      String siteUrl = "";
      switch (site) {
        case NDLA:
          siteUrl = NDLA_SITE_URL;
          relationType += "ndlanode-topic-generic";
          player1Type += "ndla-node";
          break;
        case DELING:
          siteUrl = DELING_SITE_URL;
          relationType += "delingnode-topic-generic";
          player1Type += "deling-node";
          break;
        case NYGIV:
          siteUrl = NYGIV_SITE_URL;
          relationType += "nygivnode-topic-generic";
          player1Type += "nygiv-node";
          break;
      }

      TopicIF nodeTopic = null;


      try {
        nodeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(siteUrl + "node/" + nodeId));
      } catch (MalformedURLException urlEX) {
        throw new NdlaServiceException(urlEX.getMessage());
      }

      JSONArray payload = new JSONArray(topics);

      if (null != nodeTopic) {

        Utils.deleteAssocs(nodeTopic, relationType, topicMap);

        for(int i = 0; i < payload.length(); i++){
          String topicId = payload.getString(i);

          TopicIF currentTopic = null;
          try {
            currentTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicId));
            if (null != currentTopic) {
              String player2Type = "http://psi.topic.ndla.no/#topic";

              Utils.createAssociation(nodeTopic, currentTopic, site, relationType, player1Type, player2Type, topicMap);
              //Utils.createReifiedAssociation(nodeTopic, currentTopic, relationType, player1Type, player2Type, reifierData, topicMap);
            }
          } catch (MalformedURLException urlEX) {
            throw new NdlaServiceException(urlEX.getMessage());
          }
        }//end for

      }//end if null subjectTopic
      store.commit(); // Persist object graph.
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage());
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return result;
  }

  public ArrayList<String> saveTopicsByNodeIdReified(String nodeId, String topics, NdlaSite site) throws NdlaServiceException,BadObjectReferenceException {
    ArrayList<String> result = new ArrayList<String>();

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      String relationType = "http://psi.topic.ndla.no/#";
      String player1Type = "http://psi.topic.ndla.no/#";
      String siteUrl = "";
      switch (site) {
        case NDLA:
          siteUrl = NDLA_SITE_URL;
          relationType += "ndlanode-topic-generic";
          player1Type += "ndla-node";
          break;
        case DELING:
          siteUrl = DELING_SITE_URL;
          relationType += "delingnode-topic-generic";
          player1Type += "deling-node";
          break;
        case NYGIV:
          siteUrl = NYGIV_SITE_URL;
          relationType += "nygivnode-topic-generic";
          player1Type += "nygiv-node";
          break;
      }

      TopicIF nodeTopic = null;


      try {
        nodeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(siteUrl + "node/" + nodeId));
      } catch (MalformedURLException urlEX) {
        throw new NdlaServiceException(urlEX.getMessage());
      }

      JSONArray payload = new JSONArray(topics);

      if (null != nodeTopic) {

        //Utils.deleteAssocs(nodeTopic, relationType, topicMap);

        for(int i = 0; i < payload.length(); i++){
          JSONObject topicObject = payload.getJSONObject(i);
          HashMap<String,String> reifierData = new HashMap<>();

          String topicId = topicObject.getString("topicId");
          JSONArray reifiers = topicObject.getJSONArray("reifiers");
          for(int j = 0; j < reifiers.length(); j++){
            JSONObject reifierObject = reifiers.getJSONObject(j);
            reifierData.put(reifierObject.getString("reifierType"),reifierObject.getString("reifierValue"));
          }


          TopicIF currentTopic = null;
          try {
            currentTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicId));
            if (null != currentTopic) {
              String player2Type = "http://psi.topic.ndla.no/#topic";
              Utils.createReifiedAssociationOntologyTopics(nodeTopic, currentTopic, relationType, player1Type, player2Type, reifierData, topicMap);
            }
          } catch (MalformedURLException urlEX) {
            throw new NdlaServiceException(urlEX.getMessage());
          }
        }//end for

      }//end if null subjectTopic
      store.commit(); // Persist object graph.
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage());
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return result;
  }

  public ArrayList<String> deleteTopicsByNodeId(String nodeId, String data, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException{
    ArrayList<String> deletedTopics = new ArrayList<>();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try{
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();

      String relationType = "http://psi.topic.ndla.no/#";
      String player1Type = "http://psi.topic.ndla.no/#";
      String siteUrl = "";
      switch (site) {
        case NDLA:
          siteUrl = NDLA_SITE_URL;
          relationType += "ndlanode-topic-generic";
          break;
        case DELING:
          siteUrl = DELING_SITE_URL;
          relationType += "delingnode-topic-generic";
          break;
        case NYGIV:
          siteUrl = NYGIV_SITE_URL;
          relationType += "nygivnode-topic-generic";
          break;
      }

      TopicIF nodeTopic = null;
      try {
        nodeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(siteUrl + "node/" + nodeId));
      } catch (MalformedURLException urlEX) {
        throw new NdlaServiceException(urlEX.getMessage());
      }

      if(null != nodeTopic ) {

        JSONArray topicIds = new JSONArray(data);
        for(int i = 0; i < topicIds.length(); i++) {
          String topicId = topicIds.getString(i);
          TopicIF topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#"+topicId));

          Utils.deleteAssocsByRole(nodeTopic, topicToBeDeleted, relationType, topicMap);
          deletedTopics.add(topicId);
        }
      }

      store.commit();
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage());
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return deletedTopics;
  }

  public ArrayList<NdlaTopic> getTopicsByNodeId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
    String siteIdentifier = site.toString().toLowerCase(); // TODO: Flagged for removal (BK).
    String query = String.format("select $COLUMN from %snode-topic-generic(%s: %s-node, $COLUMN : topic) order by $COLUMN limit %s offset %s?", siteIdentifier, identifier, siteIdentifier, Integer.toString(limit), Integer.toString(offset));

    return executeCollectionQuery(query);
  }

    synchronized public HashMap<String, TopicIF> saveSiteTopicsByNodeId(String identifier, HashMap<String, HashMap<String, HashMap<String, String>>> data, NdlaSite site) throws NdlaServiceException {
        HashMap<String, TopicIF> topics = new HashMap<String, TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            TopicMapBuilderIF builder = topicMap.getBuilder();
            LocatorIF baseAddress = store.getBaseAddress();

            String siteUrl = "";
            switch (site) {
                case NDLA:
                    siteUrl = NDLA_SITE_URL;
                    break;
                case DELING:
                    siteUrl = DELING_SITE_URL;
                    break;
                case NYGIV:
                    siteUrl = NYGIV_SITE_URL;
                    break;
            }
            TopicIF nodeTopic = null;
            try {

                String nodeId = identifier.substring(identifier.lastIndexOf('_') + 1);
                nodeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(siteUrl + "node/" + nodeId));
            } catch (MalformedURLException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            }

            /*
            This typestring expects to be one of the specific topic types
            created for each site type until the transition to generic "topic" type is enforced
            @TODO: Create or refactor method for fetching of generic "topic" types when appropriate
            */
            String topicTypeString = site.toString().toLowerCase() + "-topic";
            String assocTypeString = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "node-topic";

            /*
            checks for duplicates, creates new ones, if the keyword does not exist,
            creates only assoc to node for existing ones
            */

            HashMap<String, TopicIF> duplicateData = Utils.lookForExistingTopics(data, topicTypeString, topicMap);

            Iterator duplicatesIterator = duplicateData.keySet().iterator();

            while (duplicatesIterator.hasNext()) {
                Object object = duplicateData.get(duplicatesIterator.next());
                if (null != object && object instanceof TopicIF) {
                    TopicIF duplicateTopic = (TopicIF) object;
                    LocatorIF psi = duplicateTopic.getSubjectIdentifiers().iterator().next();

                    Utils.deleteBasenames(psi, topicMap);
                    Utils.deleteOccurrences(psi, topicMap);
                    Utils.deleteAssocsByRole(duplicateTopic, nodeTopic, assocTypeString, topicMap);
                }
            }

            Iterator dataIterator = data.keySet().iterator();

            try {
                while (dataIterator.hasNext()) {
                    boolean topicExists = false; // TODO: Flagged for removal (BK).
                    String dataSetKey = (String) dataIterator.next();
                    HashMap<String, HashMap<String, String>> keysets = data.get(dataSetKey);
                    Iterator setIterator = keysets.keySet().iterator();
                    TopicIF currentTopic = null;

                    if (null != duplicateData.get(dataSetKey)) {
                        topicExists = true;
                        currentTopic = duplicateData.get(dataSetKey);
                    } else {
                        String id = site.toString().toLowerCase() + "-topic-" + Utils.makeRandomId(topicMap);
                        currentTopic = builder.makeTopic();
                        TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeString));
                        currentTopic.addType(type);

                        currentTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + id));
                        currentTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + id));
                    }

                    while (setIterator.hasNext()) {
                        String setKey = (String) setIterator.next();
                        HashMap<String, String> topicSetMap = keysets.get(setKey);
                        TopicIF existingSiteTopic = null; // TODO: Flagged for removal (BK).
                        Iterator topicIterator = topicSetMap.keySet().iterator();

                        TopicIF newTopic = null; // TODO: Flagged for removal (BK).

                        switch (setKey) {
                            case "names":
                                while (topicIterator.hasNext()) {
                                    String topicStringLang = (String) topicIterator.next();
                                    String topicString = topicSetMap.get(topicStringLang);

                                    if (!Utils.isEmpty(topicString) && !Utils.isEmpty(topicStringLang)) {
                                        // Names and scopes
                                        Utils.createBasename(currentTopic, topicString, topicStringLang, topicMap);
                                    }// endif not empty name and scope
                                }//end while topicIterator
                                break;
                            case "occurrences":
                                while (topicIterator.hasNext()) {
                                    String occurrenceString = (String) topicIterator.next();
                                    String occurrenceType = "";
                                    String occurrenceLang = "";

                                    if (occurrenceString.contains("#")) {
                                        String[] occArray = occurrenceString.split("#");
                                        occurrenceLang = occArray[0];
                                        occurrenceType = occArray[1];
                                    } else {
                                        occurrenceType = occurrenceString;
                                        occurrenceLang = "language-neutral";
                                    }

                                    String occurrenceData = topicSetMap.get(occurrenceString);
                                    TopicIF occType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + occurrenceType));
                                    switch (occurrenceType) {
                                        case "description":
                                            OccurrenceIF descriptionOcc = builder.makeOccurrence(currentTopic, occType, occurrenceData);
                                            if (!Utils.isEmpty(occurrenceLang)) {
                                                descriptionOcc.addTheme(Utils.getLanguageTopic(occurrenceLang, topicMap));
                                            }
                                            break;
                                        case "image":
                                            builder.makeOccurrence(currentTopic, occType, new URILocator(occurrenceData));
                                            break;
                                    }
                                }//end while topicIterator
                                break;
                        }//end switch setKey
                    }//end while setiterator

                    TopicIF siteOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#originating-site"));
                    builder.makeOccurrence(currentTopic, siteOccType, new URILocator(siteUrl));

                    if (null != currentTopic && null != nodeTopic) {
                        String relationType = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "node-topic";
                        String player1Type = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "-node";
                        String player2Type = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "-topic";

                        Utils.createAssociation(nodeTopic, currentTopic, site, relationType, player1Type, player2Type, topicMap);
                    }
                    topics.put(dataSetKey, currentTopic);
                }//end while dataIterator
            } catch (MalformedURLException | UniquenessViolationException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            }
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return topics;
    }

    synchronized public HashMap<String, TopicIF> createSiteTopics(HashMap<String, HashMap<String, HashMap<String, String>>> data, NdlaSite site) throws NdlaServiceException {
        HashMap<String, TopicIF> topics = new HashMap<String, TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            TopicMapBuilderIF builder = topicMap.getBuilder();
            LocatorIF baseAddress = store.getBaseAddress();

            String siteUrl = "";
            switch (site) {
                case NDLA:
                    siteUrl = NDLA_SITE_URL;
                    break;
                case DELING:
                    siteUrl = DELING_SITE_URL;
                    break;
                case NYGIV:
                    siteUrl = NYGIV_SITE_URL;
                    break;
            }

            /*
            This typestring expects to be one of the specific topic types
            created for each site type until the transition to generic "topic" type is enforced
            @TODO: Create or refactor method for fetching of generic "topic" types when appropriate
            */
            String topicTypeString = site.toString().toLowerCase() + "-topic";

            /*
            checks for duplicates, creates new ones, if the keyword does not exist,
            creates only assoc to node for existing ones
            */

            HashMap<String, TopicIF> duplicateData = Utils.lookForExistingTopics(data, topicTypeString, topicMap);

            Iterator dataIterator = data.keySet().iterator();
            try {
                while (dataIterator.hasNext()) {
                    boolean topicExists = false; // TODO: Flagged for removal (BK).
                    String dataSetKey = (String) dataIterator.next();
                    HashMap<String, HashMap<String, String>> keysets = data.get(dataSetKey);
                    Iterator setIterator = keysets.keySet().iterator();
                    TopicIF currentTopic = null;

                    if (null == duplicateData.get(dataSetKey)) {
                        String id = site.toString().toLowerCase() + "-topic-" + Utils.makeRandomId(topicMap);
                        currentTopic = builder.makeTopic();
                        TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeString));
                        currentTopic.addType(type);

                        currentTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + id));
                        currentTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + id));

                        while (setIterator.hasNext()) {
                            String setKey = (String) setIterator.next();
                            HashMap<String, String> topicSetMap = keysets.get(setKey);
                            TopicIF existingSiteTopic = null; // TODO: Flagged for removal (BK).
                            Iterator topicIterator = topicSetMap.keySet().iterator();

                            TopicIF newTopic = null; // TODO: Flagged for removal (BK).

                            switch (setKey) {
                                case "names":
                                    while (topicIterator.hasNext()) {
                                        String topicStringLang = (String) topicIterator.next();
                                        String topicString = topicSetMap.get(topicStringLang);

                                        if (!Utils.isEmpty(topicString) && !Utils.isEmpty(topicStringLang)) {
                                            // Names and scopes
                                            Utils.createBasename(currentTopic, topicString, topicStringLang, topicMap);
                                        }
                                        // endif not empty name and scope
                                    }//end while topicIterator
                                    break;
                                case "occurrences":
                                    while (topicIterator.hasNext()) {
                                        String occurrenceString = (String) topicIterator.next();
                                        String occurrenceType = "";
                                        String occurrenceLang = "";
                                        if (occurrenceString.contains("#")) {
                                            String[] occArray = occurrenceString.split("#");
                                            occurrenceLang = occArray[0];
                                            occurrenceType = occArray[1];
                                        } else if (occurrenceString.contains("_")) {
                                            String[] occArray = occurrenceString.split("_");
                                            occurrenceLang = occArray[1];
                                            occurrenceType = occArray[0];
                                        } else {
                                            occurrenceType = occurrenceString;
                                            occurrenceLang = "language-neutral";
                                        }

                                        String occurrenceData = topicSetMap.get(occurrenceString);
                                        TopicIF occtype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + occurrenceType));
                                        switch (occurrenceType) {
                                            case "description":
                                                OccurrenceIF descriptionOcc = builder.makeOccurrence(currentTopic, occtype, occurrenceData);
                                                if (!Utils.isEmpty(occurrenceLang)) {
                                                    descriptionOcc.addTheme(Utils.getLanguageTopic(occurrenceLang, topicMap));
                                                }
                                                break;
                                            case "node-author":
                                                OccurrenceIF authorOcc = builder.makeOccurrence(currentTopic, occtype, occurrenceData);
                                                break;
                                            case "image":
                                                builder.makeOccurrence(currentTopic, occtype, new URILocator(occurrenceData));
                                                break;

                                        }
                                    }//end while topicIterator
                                    break;
                            }//end switch setKey
                        }//end while setiterator
                        TopicIF siteOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#originating-site"));
                        builder.makeOccurrence(currentTopic, siteOcctype, new URILocator(siteUrl));
                        topics.put(dataSetKey, currentTopic);
                    }//end if topic does not exist
                }//end while dataIterator
            } catch (MalformedURLException | UniquenessViolationException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            }
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return topics;
    }

    synchronized public ArrayList<String> saveTopicsBySubjectMatter(String subjectMatterIdentifier, String topics, NdlaSite site)  throws NdlaServiceException, BadObjectReferenceException {
        ArrayList<String> result = new ArrayList<String>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            TopicIF subjectTopic = null;

            String siteUrl = "";
            switch (site) {
                case NDLA:
                    siteUrl = NDLA_SITE_URL;
                    break;
                case DELING:
                    siteUrl = DELING_SITE_URL;
                    break;
                case NYGIV:
                    siteUrl = NYGIV_SITE_URL;
                    break;
            }

            try {
                String subjectUrl = "http://psi.topic.ndla.no/#" + subjectMatterIdentifier;
                subjectTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(subjectUrl));
            } catch (MalformedURLException urlEX) {
                throw new NdlaServiceException(urlEX.getMessage());
            }

          JSONArray payload = new JSONArray(topics);

            if (null != subjectTopic) {
              for(int i = 0; i < payload.length(); i++){
                String topicId = payload.getString(i);

                TopicIF siteTopic = null;
                try {
                  siteTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicId));
                  if (null != siteTopic) {
                    String relationType = "http://psi.topic.ndla.no/#subject_ontology_has_topic";
                    String player1Type = "http://psi.topic.ndla.no/#subjectmatter";
                    String player2Type = "http://psi.topic.ndla.no/#topic";

                    Utils.createAssociation(subjectTopic, siteTopic, site, relationType, player1Type, player2Type, topicMap);

                    result.add("http://psi.topic.ndla.no/topics/#" + topicId);
                  }
                } catch (MalformedURLException urlEX) {
                  throw new NdlaServiceException(urlEX.getMessage());
                }



              }//end for

            }//end if null subjectTopic
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    synchronized public ArrayList<TopicIF> relateTopicsByNodeId(String nodeId, List<String> topics, NdlaSite site) throws NdlaServiceException {
        ArrayList<TopicIF> result = new ArrayList<TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            TopicIF nodeTopic = null;

            String siteUrl = "";
            switch (site) {
                case NDLA:
                    siteUrl = NDLA_SITE_URL;
                    break;
                case DELING:
                    siteUrl = DELING_SITE_URL;
                    break;
                case NYGIV:
                    siteUrl = NYGIV_SITE_URL;
                    break;
            }

            try {
                String tempNodeId = nodeId.substring(nodeId.lastIndexOf("_") + 1);
                String nodeUrl = siteUrl + "node/" + tempNodeId;
                nodeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(nodeUrl));

            } catch (MalformedURLException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            }

            if (null != nodeTopic) {
                for (String topicId : topics) {
                    TopicIF siteTopic = null;
                    try {
                        siteTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicId));
                        if (null != siteTopic) {
                            result.add(siteTopic);
                            String relationType = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "node-topic";
                            String player1Type = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "-node";
                            String player2Type = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "-topic";

                            Utils.createAssociation(nodeTopic, siteTopic, site, relationType, player1Type, player2Type, topicMap);
                            result.add(siteTopic);
                        }
                    } catch (MalformedURLException urlEX) {
                        throw new NdlaServiceException(urlEX.getMessage());
                    }
                }//end for in keywords
            }//end if null subjectTopic
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    synchronized public ArrayList<TopicIF> updateSiteTopicRelationsByNodeId(String nodeId, ArrayList<String> topics, NdlaSite site) throws NdlaServiceException {
        ArrayList<TopicIF> result = new ArrayList<TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            TopicIF nodeTopic = null;

            String siteUrl = "";
            switch (site) {
                case NDLA:
                    siteUrl = NDLA_SITE_URL;
                    break;
                case DELING:
                    siteUrl = DELING_SITE_URL;
                    break;
                case NYGIV:
                    siteUrl = NYGIV_SITE_URL;
                    break;
            }

            String siteIdentifier = site.toString().toLowerCase();
            String relationType = "http://psi.topic.ndla.no/#" + siteIdentifier + "node-topic";

            try {
                String tempNodeId = nodeId.substring(nodeId.lastIndexOf("_") + 1);
                String nodeUrl = siteUrl + "node/" + tempNodeId;
                nodeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(nodeUrl));

                if (null != nodeTopic) {
                    //delete exsiting relations

                    String nodeIdentifier = siteIdentifier + "node_" + nodeId;
                    ArrayList<NdlaTopic> existingTopics = getSiteTopicsByNodeId(10, 0, nodeIdentifier, site);
                    if (existingTopics.size() > 0) {
                        for (NdlaTopic tmp : existingTopics) {
                            String subjectIdentifier = tmp.getPsi();
                            TopicIF existingTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(subjectIdentifier));
                            Utils.deleteAssocsByRole(existingTopic, nodeTopic, relationType, topicMap);
                        }
                    }

                    for (String topicId : topics) {
                        TopicIF siteTopic = null;
                        try {
                            siteTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicId));
                            if (null != siteTopic) {
                                result.add(siteTopic);
                                String player1type = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "-node";
                                String player2type = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "-topic";

                                Utils.createAssociation(nodeTopic, siteTopic, site, relationType, player1type, player2type, topicMap);
                                result.add(siteTopic);
                            }
                        } catch (MalformedURLException e) {
                            throw new NdlaServiceException(e.getMessage(),e);
                        }
                    }//end for in keywords
                }//end if not null nodeTopic

            } catch (MalformedURLException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            }
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    synchronized public HashMap<String, TopicIF> updateTopicsByTopicId(String topicId, HashMap<String, HashMap<String, HashMap<String, String>>> data, NdlaSite site) throws NdlaServiceException {
        HashMap<String, TopicIF> keyMap = new HashMap<String, TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            TopicMapBuilderIF builder = topicMap.getBuilder();

            String siteUrl = "";
            switch (site) {
                case NDLA:
                    siteUrl = NDLA_SITE_URL;
                    break;
                case DELING:
                    siteUrl = DELING_SITE_URL;
                    break;
                case NYGIV:
                    siteUrl = NYGIV_SITE_URL;
                    break;
            }

            try {
                Iterator dataIterator = data.keySet().iterator();
                while (dataIterator.hasNext()) {
                    String dataKey = (String) dataIterator.next();

                    TopicIF existingSiteTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicId));
                    LocatorIF psi = existingSiteTopic.getSubjectIdentifiers().iterator().next();
                    Utils.deleteBasenames(psi, topicMap);
                    Utils.deleteOccurrences(psi, topicMap);

                    HashMap<String, HashMap<String, String>> dataSets = data.get(dataKey);
                    Iterator setIterator = dataSets.keySet().iterator();

                    while (setIterator.hasNext()) {
                        String setType = (String) setIterator.next();
                        switch (setType) {
                            case "occurrences":
                                HashMap<String, String> occurrenceData = dataSets.get(setType);
                                Iterator occIterator = occurrenceData.keySet().iterator();
                                while (occIterator.hasNext()) {
                                    String occurrenceString = (String) occIterator.next();
                                    String occurrenceTypeString = "";
                                    String occurrenceLangString = "";

                                    if (occurrenceString.contains("#")) {
                                        String[] occArray = occurrenceString.split("#");
                                        occurrenceLangString = occArray[0];
                                        occurrenceTypeString = occArray[1];
                                    } else {
                                        occurrenceTypeString = occurrenceString;
                                        occurrenceLangString = "language-neutral";
                                    }

                                    String occurrenceValue = occurrenceData.get(occurrenceString);

                                    TopicIF occType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + occurrenceTypeString));
                                    switch (occurrenceTypeString) {
                                        case "description":
                                            OccurrenceIF descriptionOcc = builder.makeOccurrence(existingSiteTopic, occType, occurrenceValue);
                                            if (!Utils.isEmpty(occurrenceLangString)) {
                                                descriptionOcc.addTheme(Utils.getLanguageTopic(occurrenceLangString, topicMap));
                                            }
                                            break;
                                        case "image":
                                            builder.makeOccurrence(existingSiteTopic, occType, new URILocator(occurrenceValue));
                                            break;
                                    }
                                }//end while occIterator
                                break;
                            case "names":
                                HashMap<String, String> newTopicNames = dataSets.get(setType);
                                Iterator nameIterator = newTopicNames.keySet().iterator();

                                while (nameIterator.hasNext()) {
                                    String keywordLang = (String) nameIterator.next();
                                    String keywordString = newTopicNames.get(keywordLang);
                                    Utils.createBasename(existingSiteTopic, keywordString, keywordLang, topicMap);
                                }
                                break;
                        } //end switch setType
                    }//end while SetIterator
                    TopicIF siteOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#originating-site"));
                    builder.makeOccurrence(existingSiteTopic, siteOcctype, new URILocator(siteUrl));
                    keyMap.put(topicId, existingSiteTopic);
                }//end while data
            } catch (MalformedURLException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            }
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return keyMap;
    }

    public ArrayList<String> deleteTopicsFromSubjectOntology(String data, String subjectId, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException{
      ArrayList<String> deletedTopics = new ArrayList<>();
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try{
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap();


        NdlaTopic subjectTopic = getSubjectBySubjectId(subjectId,site);

        HashMap<String,String> siteMapping = new HashMap<>();
        siteMapping.put("NDLA","http://ndla.no/");
        siteMapping.put("DELING","http://deling.ndla.no/");
        siteMapping.put("NYGIV","http://fyr.ndla.no/");

        ArrayList<String> originatingSites = subjectTopic.getOriginatingSites();

        if(null != subjectTopic && originatingSites.contains(siteMapping.get(site.toString()))) {
          String relationType = "http://psi.topic.ndla.no/#subject_ontology_has_topic";
          TopicIF subjectTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#"+subjectId));

          JSONArray topicIds = new JSONArray(data);
          for(int i = 0; i < topicIds.length(); i++) {
            String topicId = topicIds.getString(i);
            TopicIF topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#"+topicId));

            Utils.deleteAssocsByRole(subjectTopicIF, topicToBeDeleted, relationType, topicMap);
            deletedTopics.add(topicId);
          }
        }

        store.commit();
      } catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          if (e instanceof BadObjectReferenceException) {
            throw new BadObjectReferenceException(e.getMessage());
          } else {
            throw new NdlaServiceException(e.getMessage());
          }
        }
      } finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }
      return deletedTopics;
    }

    synchronized public ArrayList<TopicIF> deleteSiteTopicsByTopicId(List<String> identifiers, NdlaSite site) throws NdlaServiceException {
        ArrayList<TopicIF> result = new ArrayList<TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            for (String identifier : identifiers) {
                TopicIF topicToBeDeleted = null;
                try {
                    topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + identifier));
                    if (null != topicToBeDeleted) {
                        result.add(topicToBeDeleted);
                        topicToBeDeleted.remove();
                    }
                } catch (MalformedURLException e) {
                    throw new NdlaServiceException(e.getMessage(),e);
                }
            }
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    // ***** Keywords *****

    public ArrayList<NdlaTopic> getKeywords(int limit, int offset, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteUrl = "";
        switch (site) {
            case NDLA:
                siteUrl = NDLA_SITE_URL;
                break;
            case DELING:
                siteUrl = DELING_SITE_URL;
                break;
            case NYGIV:
                siteUrl = NYGIV_SITE_URL;
                break;
        }
        String query = String.format("select $COLUMN from topic-name($COLUMN,$N),instance-of($COLUMN, keyword), originating-site($COLUMN, \"%s\") order by $COLUMN limit %s offset %s?", siteUrl, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }


    public ArrayList<NdlaTopic> getKeywords(int limit, int offset) throws NdlaServiceException, BadObjectReferenceException {
      String query = "";

      if(limit == 0 && offset == 0){
        query = String.format("select $COLUMN from instance-of($COLUMN, keyword) order by $COLUMN?");
      }
      else{
        query = String.format("select $COLUMN from instance-of($COLUMN, keyword) order by $COLUMN limit %s offset %s?", Integer.toString(limit), Integer.toString(offset));
      }


      return executeCollectionQuery(query);
    }

    public ArrayList<NdlaTopic> getKeywordsByNodeId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase();
        String query = String.format("select $COLUMN from %snode-keyword(%s : %s-node, $COLUMN : keyword) order by $COLUMN limit %s offset %s?", siteIdentifier, identifier, siteIdentifier, limit, offset);

        return executeCollectionQuery(query);
    }

    public ArrayList<NdlaTopic> getKeywordsBySubjectMatter(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase(); // TODO: Flagged for removal (BK).
        String query = String.format("select $COLUMN from subject-keyword(%s: subjectmatter, $COLUMN : keyword) order by $COLUMN limit %s offset %s?", identifier, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }

    synchronized public NdlaTopic getKeywordByKeywordId(String identifier) throws NdlaServiceException, BadObjectReferenceException {
        NdlaTopic result = null;
        String query = String.format("select $COLUMN from subject-identifier($COLUMN, \"http://psi.topic.ndla.no/keywords/#%s\")?", identifier);

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            ParsedStatementIF statement = null;
            QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
            QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

            QueryResultIF tologResult = null;
            try {
                statement = queryProcessor.parse(query);
                if (null != statement) {
                    tologResult = ((ParsedQueryIF) statement).execute();
                    if (tologResult.next()) {
                        result = new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), null);
                    }
                }
            } catch (BadObjectReferenceException e) {
                throw new BadObjectReferenceException(e.getMessage());
            } catch (InvalidQueryException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            } finally {
                if (null != tologResult) {
                    tologResult.close();
                }
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                if (e instanceof BadObjectReferenceException) {
                    throw new BadObjectReferenceException(e.getMessage());
                } else {
                    throw new NdlaServiceException(e.getMessage(),e);
                }
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    public ArrayList<NdlaTopic> lookUpKeywordsBySearchTerm(String term, NdlaSite site) {
        throw new UnsupportedOperationException("Method not implemented");
    }

    synchronized public HashMap<String, TopicIF> createKeywordsByNodeId(String nodeId, String data, NdlaSite site) throws NdlaServiceException {
        HashMap<String, TopicIF> keyMap = new HashMap<String, TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            HashMap<String, TopicIF> newKeyMap = new HashMap<String, TopicIF>();
            HashMap<String, TopicIF> existingKeyMap = new HashMap<String, TopicIF>();
            HashMap<String,JSONArray> existingKeySets = new HashMap<>();

            TopicMapBuilderIF builder = topicMap.getBuilder();
            LocatorIF baseAddress = store.getBaseAddress();

            String siteUrl = "";
            switch (site) {
                case NDLA:
                    siteUrl = NDLA_SITE_URL;
                    break;
                case DELING:
                    siteUrl = DELING_SITE_URL;
                    break;
                case NYGIV:
                    siteUrl = NYGIV_SITE_URL;
                    break;
            }

            String topicTypeString = "keyword";
            JSONObject payload = new JSONObject(data);
            JSONObject indexPayLoad = new JSONObject();

            JSONArray keywords = payload.getJSONArray("keywords");
            for (int i = 0; i < keywords.length(); i++) {
              JSONObject jsonKeywordSet = keywords.getJSONObject(i);
              JSONArray keywordSet = jsonKeywordSet.getJSONArray("keywordset");
              String setKey = "set_" + i;
              String approved = null;
              String approval_date = null;

                // Check if topic already exsits
              TopicIF keywordExists = Utils.isExistingTopic(keywordSet, topicMap);

              String topicId = "";
              if (null != keywordExists) {
                Collection<LocatorIF> existingIds = keywordExists.getSubjectIdentifiers();
                for(LocatorIF existingId : existingIds) {
                  String loc = existingId.getAddress();
                  if(loc.contains("#")) {
                    topicId = loc.substring(loc.lastIndexOf("#") + 1);
                  }
                }

                // Put keyword in KeyMap for use later.
                existingKeyMap.put(setKey, keywordExists);
                existingKeySets.put(setKey, keywordSet);
              } else {

                for (int j = 0; j < keywordSet.length(); j++) {
                    JSONObject keywordData = keywordSet.getJSONObject(j);
                    TopicIF keyTopic = null;

                    JSONArray keywordNames = keywordData.getJSONArray("names");
                    topicId = keywordData.getString("topicId");

                    for (int k = 0; k < keywordNames.length(); k++) {

                      // Names and scopes
                      JSONObject keywordName = keywordNames.getJSONObject(k);

                      String keywordNameLang = keywordName.getString("language");
                      String keywordNameString = keywordName.getString("keyword");

                      String wordClass = keywordData.getString("wordclass");
                      String visibility = keywordData.getString("visibility");


                      if (!newKeyMap.containsKey(setKey)) {
                        topicId = topicTypeString + "-" + Utils.makeRandomId(topicMap);
                        keyTopic = builder.makeTopic();

                        TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeString));
                        // Add default keyword type.
                        keyTopic.addType(type);
                        // Add additional keyword type from payload.
                        keyTopic = addAdditionalTypes(keyTopic, keywordSet, topicMap);


                        keyTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + topicId));
                        keyTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));

                        // Site occurrence
                        TopicIF siteOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#originating-site"));
                        builder.makeOccurrence(keyTopic, siteOcctype, new URILocator(siteUrl));

                        // Visibility occurrence
                        TopicIF visbilityOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeString + "-visibility"));
                        builder.makeOccurrence(keyTopic, visbilityOcctype, visibility);

                        // Approved Occtype
                        TopicIF approvedOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
                        builder.makeOccurrence(keyTopic,approvedOcctype,"false");

                        // Names and scopes
                        Utils.createBasename(keyTopic, keywordNameString, keywordNameLang, wordClass, topicMap);

                        newKeyMap.put(setKey, keyTopic);

                      } else if (newKeyMap.containsKey(setKey)) {
                        keyTopic = newKeyMap.get(setKey);
                        Utils.createBasename(keyTopic, keywordNameString, keywordNameLang, wordClass, topicMap);
                      }//end if keyword exists
                    }//end for names
                }//end for keywordsets
              }//end if duplicate
                if(null != keywordExists){
                  keywordSet = appendDisapproval(keywordSet,topicTypeString,topicMap,keywordExists);
                  //Add missing approval for existing topic
                  LocatorIF psi = keywordExists.getSubjectIdentifiers().iterator().next();
                  HashMap<String,String> approvedState = Utils.getApprovedState(psi.getAddress(),topicMap);
                  approved = approvedState.get("approved");
                  approval_date = approvedState.get("approval_date");

                  if(null == approved){
                    TopicIF approvedOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
                    builder.makeOccurrence(keywordExists,approvedOcctype,"false");
                  }


                  keywordSet = appendProcessStatus(keywordSet,topicTypeString,topicMap,keywordExists);

                  HashMap<String,String> processState = Utils.getProcessState("http://psi.topic.ndla.no/#" + topicId,topicMap);
                  if(processState.size() == 0){
                    TopicIF processOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#process-state"));
                    builder.makeOccurrence(keywordExists, processOcctype, "0");
                  }

                }
                else {
                  keywordSet = appendDisapproval(keywordSet,topicTypeString,topicMap,null);
                }

                indexPayLoad.put(topicId, keywordSet);
            }//end for keywords

            JSONArray indexPayloadArray = Utils.indexPayload(indexPayLoad, siteUrl, topicMap);
            TopicIF nodeTopic = null;

            String tempNodeId = nodeId.substring(nodeId.lastIndexOf('_') + 1);
            nodeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(siteUrl + "node/" + tempNodeId));
            String relationType = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "node-" + topicTypeString;
            String player1Type = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "-node";
            String player2Type = "http://psi.topic.ndla.no/#" + topicTypeString;
            // Check if we have an existing KeyMap.
            if (existingKeyMap.size() > 0) {
                Iterator eIter = existingKeyMap.keySet().iterator();
                while (eIter.hasNext()) {
                    String eSetKey = (String) eIter.next();
                    TopicIF keywordTopic = existingKeyMap.get(eSetKey);
                    // Add additional topics to existing keyword
                    keywordTopic = addAdditionalTypes(keywordTopic, existingKeySets.get(eSetKey), topicMap);

                    if (null != keywordTopic && null != nodeTopic) {
                          Utils.createAssociation(nodeTopic, keywordTopic, site, relationType, player1Type, player2Type, topicMap);
                          keyMap.put(eSetKey, keywordTopic);
                    }
                }
            }

            if (newKeyMap.size() > 0) {
                Iterator nIter = newKeyMap.keySet().iterator();
                while (nIter.hasNext()) {
                    String nSetKey = (String) nIter.next();
                    TopicIF nKeywordTopic = newKeyMap.get(nSetKey);
                    if (null != nKeywordTopic && null != nodeTopic) {
                        Utils.createAssociation(nodeTopic, nKeywordTopic, site, relationType, player1Type, player2Type, topicMap);
                        keyMap.put(nSetKey, nKeywordTopic);
                    }
                }
            }
            store.commit(); // Persist object graph.
            if (indexPayloadArray.length() > 0) {
                Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
                boolean indexed = indexer.postToIndex(indexPayloadArray.toString(), false,"keywords");
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        }
    finally { // Release all topic map engine resources.
              if (store != null) {
                  store.close();
              }
              if (repository != null) {
                  repository.close();
              }
          }
          return keyMap;
      }

  synchronized public HashMap<String, TopicIF> createKeywordsByNodeIdUpdated(String nodeId, String data, NdlaSite site) throws NdlaServiceException {
    HashMap<String, TopicIF> keyMap = new HashMap<String, TopicIF>();

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;

    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();

      HashMap<String, TopicIF> newKeyMap = new HashMap<String, TopicIF>();
      HashMap<String, TopicIF> existingKeyMap = new HashMap<String, TopicIF>();
      HashMap<String,JSONArray> existingKeySets = new HashMap<>();

      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = topicMap.getStore().getBaseAddress();

      String siteUrl = "";
      switch (site) {
        case NDLA:
          siteUrl = NDLA_SITE_URL;
          break;
        case DELING:
          siteUrl = DELING_SITE_URL;
          break;
        case NYGIV:
          siteUrl = NYGIV_SITE_URL;
          break;
      }

      String topicTypeString = "keyword";
      JSONObject payload = new JSONObject(data);
      JSONObject indexPayLoad = new JSONObject();

      JSONArray keywords = payload.getJSONArray("keywords");
      for (int i = 0; i < keywords.length(); i++) {
        JSONObject jsonKeywordSet = keywords.getJSONObject(i);
        JSONArray keywordSet = jsonKeywordSet.getJSONArray("keywordset");
        String setKey = "set_" + i;
                /*
                check for duplicates, creates new ones, if the keyword does not exist,
                create only assoc to node for existing ones
                */

        TopicIF keywordExists = Utils.isDuplicateUpdateTopic(keywordSet, topicMap);

        if (null != keywordExists) {
          existingKeyMap.put(setKey, keywordExists);
          existingKeySets.put(setKey, keywordSet);
        } else {
          String topicId = "";
            for (int j = 0; j < keywordSet.length(); j++) {
              JSONObject keywordData = keywordSet.getJSONObject(j);
              TopicIF keyTopic = null;

            JSONArray keywordNames = keywordData.getJSONArray("names");

            for (int k = 0; k < keywordNames.length(); k++) {

              // Names and scopes
              JSONObject keywordName = keywordNames.getJSONObject(k);

              String keywordNameLang = keywordName.getString("language");
              String keywordNameString = keywordName.getString("keyword");

              String wordClass = keywordData.getString("wordclass");

              String visibility = keywordData.getString("visibility");

              if (!newKeyMap.containsKey(setKey)) {
                topicId = topicTypeString + "-" + Utils.makeRandomId(topicMap);
                keyTopic = builder.makeTopic();

                //Add default type
                TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeString));
                keyTopic.addType(type);
                keyTopic = addAdditionalTypes(keyTopic, keywordSet, topicMap);

                keyTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + topicId));
                keyTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));

                // Site occurrence
                TopicIF siteOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#originating-site"));
                builder.makeOccurrence(keyTopic, siteOcctype, new URILocator(siteUrl));

                // Visibility occurrence
                TopicIF visbilityOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeString + "-visibility"));
                builder.makeOccurrence(keyTopic, visbilityOcctype, visibility);

                // Approved Occtype
                TopicIF approvedOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
                builder.makeOccurrence(keyTopic,approvedOcctype,"false");

                // Names and scopes
                Utils.createBasename(keyTopic, keywordNameString, keywordNameLang, wordClass, topicMap);

                newKeyMap.put(setKey, keyTopic);

              } else if (newKeyMap.containsKey(setKey)) {
                keyTopic = newKeyMap.get(setKey);
                Utils.createBasename(keyTopic, keywordNameString, keywordNameLang, wordClass, topicMap);
              }//end if keyword exists
            }//end for names
          }//end for keywordsets
          keywordSet = appendDefaultType(keywordSet,topicTypeString,topicMap);
          keywordSet = appendDisapproval(keywordSet,topicTypeString,topicMap,null);
          indexPayLoad.put(topicId, keywordSet);
        }//end if duplicate
      }//end for keywords

      JSONArray indexPayloadArray = Utils.indexPayload(indexPayLoad, siteUrl, topicMap);
      TopicIF nodeTopic = null;

      String tempNodeId = nodeId.substring(nodeId.lastIndexOf('_') + 1);
      nodeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(siteUrl + "node/" + tempNodeId));
      String relationType = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "node-" + topicTypeString;
      String player1Type = "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "-node";
      String player2Type = "http://psi.topic.ndla.no/#" + topicTypeString;

      if (existingKeyMap.size() > 0) {
        Iterator eIter = existingKeyMap.keySet().iterator();
        while (eIter.hasNext()) {
          String eSetKey = (String) eIter.next();
          TopicIF keywordTopic = existingKeyMap.get(eSetKey);
          keywordTopic = addAdditionalTypes(keywordTopic, existingKeySets.get(eSetKey), topicMap);

          if (null != keywordTopic && null != nodeTopic) {
            Utils.createAssociation(nodeTopic, keywordTopic, site, relationType, player1Type, player2Type, topicMap);
            keyMap.put(eSetKey, keywordTopic);
          }
        }
      }

      if (newKeyMap.size() > 0) {
        Iterator nIter = newKeyMap.keySet().iterator();
        while (nIter.hasNext()) {
          String nSetKey = (String) nIter.next();
          TopicIF nKeywordTopic = newKeyMap.get(nSetKey);
          if (null != nKeywordTopic && null != nodeTopic) {
            Utils.createAssociation(nodeTopic, nKeywordTopic, site, relationType, player1Type, player2Type, topicMap);
            keyMap.put(nSetKey, nKeywordTopic);
          }
        }
      }
      topicMap.getStore().commit(); // Persist object graph.

      if (indexPayloadArray.length() > 0) {
        Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
        boolean indexed = indexer.postToIndex(indexPayloadArray.toString(), false, "keywords");
      }

    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(), e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return keyMap;
  }

    synchronized public HashMap<String, TopicIF> createKeyword(String data) throws NdlaServiceException, BadObjectReferenceException {
        HashMap<String, TopicIF> keyMap = new HashMap<String, TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            HashMap<String, TopicIF> newKeyMap = new HashMap<String, TopicIF>();
            TopicMapBuilderIF builder = topicMap.getBuilder();
            LocatorIF baseAddress = store.getBaseAddress();

            String topicTypeString = "keyword";

            JSONObject payload = new JSONObject(data);
            JSONObject indexPayLoad = new JSONObject();

             /*
                check for duplicates, creates new ones, if the keyword does not exist,
                create only assoc to node for existing ones
            */

            JSONArray keywords = payload.getJSONArray("keywords");
            for (int i = 0; i < keywords.length(); i++) {
                JSONObject jsonKeywordSet = keywords.getJSONObject(i);
                JSONArray keywordSets = jsonKeywordSet.getJSONArray("keywordset");

                String setKey = "set_" + i;
                String topicId = "";
                for (int j = 0; j < keywordSets.length(); j++) {
                    JSONObject keywordData = keywordSets.getJSONObject(j);
                    JSONArray keywordNames = keywordData.getJSONArray("names");

                    String wordClass = keywordData.getString("wordclass");
                    String visibility = keywordData.getString("visibility");
                    TopicIF existingKeywordTopic = null;
                    boolean keywordExists = false;
                    TopicIF keyTopic = null;

                    for (int k = 0; k < keywordNames.length(); k++) {
                        JSONObject keywordName = keywordNames.getJSONObject(k);

                        String keywordNameLang = keywordName.getString("language");
                        String keywordNameString = keywordName.getString("keyword");
                        if (!Utils.isEmpty(keywordNameString) && !Utils.isEmpty(keywordNameLang)) {
                            if (!newKeyMap.containsKey(setKey)) {
                                String query = String.format("select $COLUMN from value($N,\"%s\"),scope($N,i\"%s\"),topic-name($COLUMN,$N),instance-of($COLUMN,%s)?", keywordNameString, keywordNameLang, topicTypeString);

                                //TopicIF result = (TopicIF) executeObjectQuery(query); // Replaced with inline query (BK).
                                Object queryResult = null;
                                ParsedStatementIF statement = null;
                                QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
                                QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

                                QueryResultIF tologResult = null;
                                try {
                                    statement = queryProcessor.parse(query);
                                    if (null != statement) {
                                        tologResult = ((ParsedQueryIF) statement).execute();
                                        if (tologResult.next()) {
                                            queryResult = tologResult.getValue("COLUMN");
                                        }
                                    }
                                } catch (BadObjectReferenceException e) {
                                    throw new BadObjectReferenceException(e.getMessage());
                                } catch (InvalidQueryException e) {
                                    throw new NdlaServiceException(e.getMessage(),e);
                                } finally {
                                    if (null != tologResult) {
                                        tologResult.close();
                                    }
                                }
                                TopicIF result = (TopicIF) queryResult; // TODO: Possible bug (BK).

                                String topicNameByLang = "";

                                if (result != null) {
                                    HashMap<String, String> topicNames = Utils.getTopicNames(result, wordClass);
                                    topicNameByLang = topicNames.get(keywordNameLang);
                                }
                                if (result != null && result instanceof TopicIF && (topicNameByLang != null && topicNameByLang.equals(keywordNameString))) {
                                    keywordExists = true;
                                    existingKeywordTopic = result;
                                }
                            }//end if set key is not set

                            if (!keywordExists && !newKeyMap.containsKey(setKey)) {
                                topicId = topicTypeString + "-" + Utils.makeRandomId(topicMap);
                                keyTopic = builder.makeTopic();

                                TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeString));
                                keyTopic.addType(type);

                                keyTopic = addAdditionalTypes(keyTopic, keywordSets, topicMap);

                                keyTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + topicId));
                                keyTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));

                                // Names and scopes
                                Utils.createBasename(keyTopic, keywordNameString, keywordNameLang, wordClass, topicMap);

                                // Visibility occurrence
                                TopicIF visbilityOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeString + "-visibility"));
                                builder.makeOccurrence(keyTopic, visbilityOcctype, visibility);

                                //processState occurrence
                                TopicIF processOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#process-state"));
                                builder.makeOccurrence(keyTopic, processOcctype, "0");

                                newKeyMap.put(setKey, keyTopic);
                            } else if (!keywordExists && newKeyMap.containsKey(setKey)) {
                                keyTopic = newKeyMap.get(setKey);
                                Utils.createBasename(keyTopic, keywordNameString, keywordNameLang, wordClass, topicMap);
                            }//end if keyword exists
                        }//end if strings not empty
                    }//end foreach keywordNames

                    if (keywordExists) {
                        keyTopic = existingKeywordTopic;
                    } else {
                        if (newKeyMap.get(setKey) instanceof TopicIF) {
                            keyTopic = newKeyMap.get(setKey);
                        }
                      keyMap.put(topicId, keyTopic);
                      indexPayLoad.put(topicId, keywordSets);
                    }//end if keyword exists


                }//end for keywordsets
            }//end for keywords
            JSONArray indexPayloadArray = Utils.indexPayload(indexPayLoad, "", topicMap);

              store.commit(); // Persist object graph.
            if (indexPayloadArray.length() > 0) {
                Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
                boolean indexed = indexer.postToIndex(indexPayloadArray.toString(), false, "keywords");
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                if (e instanceof BadObjectReferenceException) {
                    throw new BadObjectReferenceException(e.getMessage());
                } else {
                    throw new NdlaServiceException(e.getMessage(),e);
                }
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return keyMap;
    }

    synchronized public HashMap<String, TopicIF> updateKeywordsByNodeId(String nodeId, String data, NdlaSite site) throws NdlaServiceException {
      HashMap<String, TopicIF> updatedKeys = new HashMap<String, TopicIF>();
      HashMap<String, TopicIF> createdKeys = new HashMap<String, TopicIF>();

      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      String nodeTopicObjectId = null;
      // check that nodetopic exists
      TopicIF nodeTopic = null;
      String tempNodeId = "";
      TopicMapIF topicMap = null;
      try {
        store = reference.createStore(false);
        topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

        String siteUrl = "";
        switch (site) {
          case NDLA:
            siteUrl = NDLA_SITE_URL;
            break;
          case DELING:
            siteUrl = DELING_SITE_URL;
            break;
          case NYGIV:
            siteUrl = NYGIV_SITE_URL;
            break;
        }


        try {
          tempNodeId = nodeId.substring(nodeId.lastIndexOf("_") + 1);
          String nodeUrl = siteUrl + "node/" + tempNodeId;
          nodeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(nodeUrl));
          if (nodeTopic != null) {
            nodeTopicObjectId = topicMap.getTopicBySubjectIdentifier(new URILocator(nodeUrl)).getObjectId();
          }
        } catch (MalformedURLException e) {
          throw new NdlaServiceException(e.getMessage(), e);
        }
      } catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          throw new NdlaServiceException(e.getMessage(), e);
        }
      } finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }

      try {
        if (null != nodeTopicObjectId) {
          //System.out.println("updateKeywordsByNodeId: DATA ? "+data+" SITE: "+site+" NodeID: "+nodeId);
          //System.out.println("updateKeywordsByNodeId: NODETOPIC NOT NULL ? "+nodeTopic.toString());
          //delete existing assoc associations
          //Utils.deleteAssocs(nodeTopicObjectId, "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "node-keyword", _configFile, _topicmapReferenceKey);
          Utils.deleteAssocs(nodeTopicObjectId, "http://psi.topic.ndla.no/#" + site.toString().toLowerCase() + "node-keyword",  _configFile,_topicmapReferenceKey);
          //build nyew JSON for the UpdateByKeywordId function
          JSONObject updateData = Utils.lookForExistingTopics(data, _configFile,_topicmapReferenceKey);
          //System.out.println("updateKeywordsByNodeId: lookForExistingTopics ? "+updateData.toString());

          //update old keywords in cas of names in other languages having been added!!
          //updatedKeys = updateKeywordsByKeywordId(updateData.toString(), site, topicMap); // TODO: Testing required. Existing topicMap object not being used in updateKeywordsByKeywordId method (BK).
          updatedKeys = updateKeywordsByKeywordId(updateData.toString(), site);
          //System.out.println("updateKeywordsByNodeId: updateKeywordsByKeywordId ? "+updatedKeys.toString());

          //create new assocs
          createdKeys = createKeywordsByNodeIdUpdated(nodeId, data, site); // TODO: Testing required. Existing topicMap object not being used in createKeywordsByNodeId method (BK).
          //createdKeys = createKeywordsByNodeId(nodeId,data,site);
          //System.out.println("updateKeywordsByNodeId: createKeywordsByNodeId ? "+createdKeys.toString());
        } else {
          throw new NdlaServiceException("The node topic with node id " + tempNodeId + " does not exist");
        } //end if not null nodeTopic
        //store.commit(); // Persist object graph.
        return createdKeys;
      }catch (BadObjectReferenceException e) {
        throw new NdlaServiceException("Reference exception: "+e.getMessage(),e);
      }
    }

    synchronized public HashMap<String, TopicIF> updateKeywordsByKeywordId(String data, NdlaSite site) throws NdlaServiceException {
      HashMap<String, TopicIF> keyMap = new HashMap<String, TopicIF>();

      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;

        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            TopicMapBuilderIF builder = topicMap.getBuilder();

            String siteUrl = "";
            switch (site) {
                case NDLA:
                    siteUrl = NDLA_SITE_URL;
                    break;
                case DELING:
                    siteUrl = DELING_SITE_URL;
                    break;
                case NYGIV:
                    siteUrl = NYGIV_SITE_URL;
                    break;
            }

            JSONObject payload = new JSONObject(data);
            JSONObject indexPayLoad = new JSONObject();

            JSONArray keywords = payload.getJSONArray("keywords");

            ArrayList<String> foundKeys = new ArrayList<>();
            String topicId = "";
            for (int i = 0; i < keywords.length(); i++) {
                JSONObject jsonKeywordSet = keywords.getJSONObject(i);
                JSONArray indexKeywordSet = new JSONArray();

                Iterator topicIdKeyIter = jsonKeywordSet.keys();

                while (topicIdKeyIter.hasNext()) {
                  topicId = (String) topicIdKeyIter.next();
                  TopicIF existingKeywordTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + topicId));
                  String approved = "";
                  String approval_date = "";

                  if (null != existingKeywordTopic  && !foundKeys.contains(topicId)) {
                    LocatorIF psi = existingKeywordTopic.getSubjectIdentifiers().iterator().next();
                    HashMap<String,String> approvedState = Utils.getApprovedState(psi.getAddress(),topicMap);
                    approved = approvedState.get("approved");
                    approval_date = approvedState.get("approval_date");

                    Utils.deleteBasenames(psi, topicMap);
                    Utils.deleteOccurrences(psi, topicMap);
                    ArrayList<LocatorIF> excludedTypes = new ArrayList<>();
                    excludedTypes.add(new URILocator("http://psi.topic.ndla.no/#keyword"));
                    excludedTypes.add(new URILocator("http://psi.topic.ndla.no/#topic"));
                    Utils.deleteTypes(psi,excludedTypes,topicMap);

                  }

                    JSONArray keywordsets = jsonKeywordSet.getJSONArray(topicId);

                    for (int j = 0; j < keywordsets.length(); j++) {
                      JSONObject keywordData = keywordsets.getJSONObject(j);
                      JSONObject indexKeyword = new JSONObject();

                      JSONArray keywordNames = keywordData.getJSONArray("names");
                      JSONArray indexKeywordNames = new JSONArray();

                    JSONArray keywordTypes = keywordData.getJSONArray("types");
                    JSONArray indexKeywordTypes = new JSONArray();

                      String wordClass = keywordData.getString("wordclass");
                      indexKeyword.put("wordclass", wordClass);
                      String visibility = keywordData.getString("visibility");
                      indexKeyword.put("visibility", visibility);

                      String processState = keywordData.getString("processState");
                      indexKeyword.put("processState",processState);
                      TopicIF processOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#process-state"));
                      builder.makeOccurrence(existingKeywordTopic, processOcctype, processState);

                      for (int k = 0; k < keywordNames.length(); k++) {
                          JSONObject keywordName = keywordNames.getJSONObject(k);
                          JSONObject indexKeywordName = new JSONObject();
                          String keywordNameLang = keywordName.getString("language");
                          String keywordNameString = keywordName.getString("keyword");
                          if (null != existingKeywordTopic  && !foundKeys.contains(topicId)) {
                              Utils.createBasename(existingKeywordTopic, keywordNameString, keywordNameLang, wordClass, topicMap);
                              indexKeywordName.put("language", keywordNameLang);
                              indexKeywordName.put("keyword", keywordNameString);
                              indexKeywordNames.put(indexKeywordName);
                          }
                      }

                      for(int l = 0; l < keywordTypes.length(); l++) {
                        JSONObject keywordType = keywordTypes.getJSONObject(l);
                        JSONObject indexKeywordType = new JSONObject();

                        String keywordTypeId = keywordType.getString("typeId");
                        indexKeywordType.put("typeId",keywordTypeId);
                        indexKeywordTypes.put(indexKeywordType);
                        if (keywordTypeId.length() > 0) {
                          TopicIF additionalType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + keywordTypeId));
                          if (null != additionalType) {
                            existingKeywordTopic.addType(additionalType);
                          }
                        } //end if
                      }//end for types

                      if (null != existingKeywordTopic && !foundKeys.contains(topicId)) {
                        indexKeyword.put("names", indexKeywordNames);
                        indexKeyword.put("types", indexKeywordTypes);
                        if (!foundKeys.contains(topicId)) {
                          TopicIF occType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#originating-site"));
                          builder.makeOccurrence(existingKeywordTopic, occType, new URILocator(siteUrl));

                          // Visibility occurrence
                          TopicIF visbilityOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#keyword-visibility"));
                          builder.makeOccurrence(existingKeywordTopic, visbilityOcctype, visibility);
                          TopicIF approvedOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
                          TopicIF approvalDateOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));

                          if(null != approved && approved.length() > 0){
                            if(approved.equals("true") && approval_date.length() > 0) {
                              indexKeyword.put("approved","true");
                              indexKeyword.put("approval_date",approval_date);
                              builder.makeOccurrence(existingKeywordTopic,approvedOcctype,approved);
                              builder.makeOccurrence(existingKeywordTopic,approvalDateOcctype,approval_date);
                            }
                            else{
                              indexKeyword.put("approved","false");
                              builder.makeOccurrence(existingKeywordTopic,approvedOcctype,"false");
                            }
                          }
                          else{
                            indexKeyword.put("approved","false");
                            builder.makeOccurrence(existingKeywordTopic,approvedOcctype,"false");
                          }



                          foundKeys.add(topicId);
                        }
                        keyMap.put(topicId, existingKeywordTopic);
                        indexKeywordSet.put(indexKeyword);
                      }//end if existing
                    }//end for keywordSets

                }//end while topicIDKey
                indexPayLoad.put(topicId, indexKeywordSet);
            }//end for keywords

            JSONArray indexPayloadArray = Utils.indexPayload(indexPayLoad, siteUrl, topicMap);

            store.commit(); // Persist object graph.
            if (indexPayloadArray.length() > 0) {
                Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
                boolean indexed = indexer.postToIndex(indexPayloadArray.toString(), false, "keywords");
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return keyMap;
    }

  synchronized public HashMap<String, TopicIF> updateKeywordsByKeywordId(String data) throws NdlaServiceException {
    HashMap<String, TopicIF> keyMap = new HashMap<String, TopicIF>();

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;

    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      TopicMapBuilderIF builder = topicMap.getBuilder();

      JSONObject payload = new JSONObject(data);
      JSONObject indexPayLoad = new JSONObject();

      JSONArray keywords = payload.getJSONArray("keywords");

      ArrayList<String> foundKeys = new ArrayList<>();
      String topicId = "";
      for (int i = 0; i < keywords.length(); i++) {
        JSONObject jsonKeywordSet = keywords.getJSONObject(i);
        JSONArray indexKeywordSet = new JSONArray();

        Iterator topicIdKeyIter = jsonKeywordSet.keys();

        while (topicIdKeyIter.hasNext()) {
          topicId = (String) topicIdKeyIter.next();
          TopicIF existingKeywordTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + topicId));
          String approved = "";
          String approval_date = "";
          HashMap<String,String> processState = null;
          if (null != existingKeywordTopic) {
            LocatorIF psi = existingKeywordTopic.getSubjectIdentifiers().iterator().next();
            HashMap<String,String> approvedState = Utils.getApprovedState(psi.getAddress(),topicMap);
            approved = approvedState.get("approved");
            approval_date = approvedState.get("approval_date");
            processState = Utils.getProcessState(psi.getAddress(),topicMap);

            Utils.deleteBasenames(psi, topicMap);
            Utils.deleteOccurrences(psi, topicMap);
            ArrayList<LocatorIF> excludedTypes = new ArrayList<>();
            excludedTypes.add(new URILocator("http://psi.topic.ndla.no/#keyword"));
            excludedTypes.add(new URILocator("http://psi.topic.ndla.no/#topic"));
            Utils.deleteTypes(psi,excludedTypes,topicMap);

          }

          JSONArray keywordsets = jsonKeywordSet.getJSONArray(topicId);

          for (int j = 0; j < keywordsets.length(); j++) {
            JSONObject keywordData = keywordsets.getJSONObject(j);
            JSONObject indexKeyword = new JSONObject();

            JSONArray keywordNames = keywordData.getJSONArray("names");
            JSONArray indexKeywordNames = new JSONArray();

            JSONArray keywordTypes = keywordData.getJSONArray("types");
            JSONArray indexKeywordTypes = new JSONArray();

            String wordClass = keywordData.getString("wordclass");
            indexKeyword.put("wordclass", wordClass);
            String visibility = keywordData.getString("visibility");
            indexKeyword.put("visibility", visibility);

            for (int k = 0; k < keywordNames.length(); k++) {
              JSONObject keywordName = keywordNames.getJSONObject(k);
              JSONObject indexKeywordName = new JSONObject();
              String keywordNameLang = keywordName.getString("language");
              String keywordNameString = keywordName.getString("keyword");
              if (null != existingKeywordTopic) {
                Utils.createBasename(existingKeywordTopic, keywordNameString, keywordNameLang, wordClass, topicMap);
                indexKeywordName.put("language", keywordNameLang);
                indexKeywordName.put("keyword", keywordNameString);
                indexKeywordNames.put(indexKeywordName);
              }
            }//end for keywordnames

            for(int l = 0; l < keywordTypes.length(); l++) {
              JSONObject keywordType = keywordTypes.getJSONObject(l);
              JSONObject indexKeywordType = new JSONObject();
              String keywordTypeId = keywordType.getString("typeId");
              indexKeywordType.put("typeId",keywordTypeId);
              indexKeywordTypes.put(indexKeywordType);

              if (keywordTypeId.length() > 0) {
                TopicIF additionalType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + keywordTypeId));
                if (null != additionalType) {
                  existingKeywordTopic.addType(additionalType);
                }
              } //end if
            }//end for types

            if (null != existingKeywordTopic) {
              indexKeyword.put("names", indexKeywordNames);
              indexKeyword.put("types", indexKeywordTypes);

              if (!foundKeys.contains(topicId)) {
                // Visibility occurrence
                TopicIF visbilityOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#keyword-visibility"));
                builder.makeOccurrence(existingKeywordTopic, visbilityOcctype, visibility);

                TopicIF siteOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#originating-site"));
                builder.makeOccurrence(existingKeywordTopic, siteOccType, new URILocator("http://ndla.no"));

                TopicIF approvedOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
                TopicIF approvalDateOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));


                if(null != approved && approved.length() > 0){
                  if(approved.equals("true") && approval_date.length() > 0) {
                    indexKeyword.put("approved","true");
                    indexKeyword.put("approval_date",approval_date);
                    builder.makeOccurrence(existingKeywordTopic,approvedOcctype,approved);
                    builder.makeOccurrence(existingKeywordTopic,approvalDateOcctype,approval_date);
                  }
                  else{
                    indexKeyword.put("approved","false");
                    builder.makeOccurrence(existingKeywordTopic,approvedOcctype,"false");
                  }
                }
                else{
                  indexKeyword.put("approved","false");
                  builder.makeOccurrence(existingKeywordTopic,approvedOcctype,"false");
                }

                if(null != processState && processState.size() > 0){
                  TopicIF processOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#process-state"));
                  builder.makeOccurrence(existingKeywordTopic, processOcctype, processState.get("processState"));
                  indexKeyword.put("processState",processState.get("processState"));
                }
                else{
                  TopicIF processOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#process-state"));
                  builder.makeOccurrence(existingKeywordTopic, processOcctype, "0");
                  indexKeyword.put("processState","0");
                }


                foundKeys.add(topicId);
              }
              keyMap.put(topicId, existingKeywordTopic);
              indexKeywordSet.put(indexKeyword);
            }//end if existing


          }//end for keywordsets

        }//end while
        indexPayLoad.put(topicId, indexKeywordSet);
      }//end for

      JSONArray indexPayloadArray = Utils.indexPayload(indexPayLoad, "", topicMap);

      store.commit(); // Persist object graph.
      if (indexPayloadArray.length() > 0) {
        Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
        boolean indexed = indexer.postToIndex(indexPayloadArray.toString(), false, "keywords");
      }
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(),e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return keyMap;
  }

  public HashMap<String, TopicIF> updateKeywordsByKeywordId(String data, NdlaSite site, final TopicMapIF topicMap)  throws NdlaServiceException, BadObjectReferenceException  {
    HashMap<String, TopicIF> keyMap = new HashMap<String, TopicIF>();

    //TopicMapStoreIF store = null;

    try {
      //store = topicMap.getStore();
      TopicMapBuilderIF builder = topicMap.getBuilder();

      String siteUrl = "";
      switch (site) {
        case NDLA:
          siteUrl = NDLA_SITE_URL;
          break;
        case DELING:
          siteUrl = DELING_SITE_URL;
          break;
        case NYGIV:
          siteUrl = NYGIV_SITE_URL;
          break;
      }

      JSONObject payload = new JSONObject(data);
      JSONObject indexPayLoad = new JSONObject();

      JSONArray keywords = payload.getJSONArray("keywords");

      ArrayList<String> foundKeys = new ArrayList<>();
      String topicId = "";
      for (int i = 0; i < keywords.length(); i++) {
        JSONObject jsonKeywordSet = keywords.getJSONObject(i);
        JSONArray indexKeywordSet = new JSONArray();

        Iterator topicIdKeyIter = jsonKeywordSet.keys();

        while (topicIdKeyIter.hasNext()) {
          topicId = (String) topicIdKeyIter.next();
          String approved = "";
          String approval_date = "";

          TopicIF existingKeywordTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + topicId));
          if (null != existingKeywordTopic) {
            LocatorIF psi = existingKeywordTopic.getSubjectIdentifiers().iterator().next();
            HashMap<String,String> approvedState = Utils.getApprovedState(psi.getAddress(),topicMap);
            approved = approvedState.get("approved");
            approval_date = approvedState.get("approval_date");

            Utils.deleteBasenames(psi, topicMap);
            Utils.deleteOccurrences(psi, topicMap);
            ArrayList<LocatorIF> excludedTypes = new ArrayList<>();
            excludedTypes.add(new URILocator("http://psi.topic.ndla.no/#keyword"));
            excludedTypes.add(new URILocator("http://psi.topic.ndla.no/#topic"));
            Utils.deleteTypes(psi,excludedTypes,topicMap);

            HashMap<String,String> processState = Utils.getProcessState(psi.getAddress(),topicMap);
            if(null == processState){
              setProcessState(psi.getAddress(),"0","keywords");
            }
          }

          JSONArray keywordsets = jsonKeywordSet.getJSONArray(topicId);

          for (int j = 0; j < keywordsets.length(); j++) {
            JSONObject keywordData = keywordsets.getJSONObject(j);
            JSONObject indexKeyword = new JSONObject();

            JSONArray keywordNames = keywordData.getJSONArray("names");
            JSONArray indexKeywordNames = new JSONArray();

            JSONArray keywordTypes = keywordData.getJSONArray("types");
            JSONArray indexKeywordTypes = new JSONArray();

            String wordClass = keywordData.getString("wordclass");
            indexKeyword.put("wordclass", wordClass);
            String visibility = keywordData.getString("visibility");
            indexKeyword.put("visibility", visibility);

            for (int k = 0; k < keywordNames.length(); k++) {
              JSONObject keywordName = keywordNames.getJSONObject(k);
              JSONObject indexKeywordName = new JSONObject();
              String keywordNameLang = keywordName.getString("language");
              String keywordNameString = keywordName.getString("keyword");
              if (null != existingKeywordTopic) {
                Utils.createBasename(existingKeywordTopic, keywordNameString, keywordNameLang, wordClass, topicMap);
                indexKeywordName.put("language", keywordNameLang);
                indexKeywordName.put("keyword", keywordNameString);
                indexKeywordNames.put(indexKeywordName);
              }
            }

            for(int l = 0; l < keywordTypes.length(); l++) {
              JSONObject keywordType = keywordTypes.getJSONObject(l);
              JSONObject indexKeywordType = new JSONObject();

              String keywordTypeId = keywordType.getString("typeId");
              indexKeywordType.put("typeId",keywordTypeId);
              indexKeywordTypes.put(indexKeywordType);
              if (keywordTypeId.length() > 0) {
                TopicIF additionalType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + keywordTypeId));
                if (null != additionalType) {
                  existingKeywordTopic.addType(additionalType);
                }
              } //end if
            }

            if (null != existingKeywordTopic) {
              indexKeyword.put("names", indexKeywordNames);
              indexKeyword.put("types", indexKeywordTypes);
              if (!foundKeys.contains(topicId)) {
                TopicIF occType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#originating-site"));
                builder.makeOccurrence(existingKeywordTopic, occType, new URILocator(siteUrl));

                // Visibility occurrence
                TopicIF visbilityOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#keyword-visibility"));
                builder.makeOccurrence(existingKeywordTopic, visbilityOcctype, visibility);

                TopicIF approvedOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
                TopicIF approvalDateOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));
                if(null != approved && approved.length() > 0){
                  if(approved.equals("true") && approval_date.length() > 0) {
                    indexKeyword.put("approved","true");
                    indexKeyword.put("approval_date",approval_date);
                    builder.makeOccurrence(existingKeywordTopic,approvedOcctype,approved);
                    builder.makeOccurrence(existingKeywordTopic,approvalDateOcctype,approval_date);
                  }
                  else{
                    indexKeyword.put("approved","false");
                    builder.makeOccurrence(existingKeywordTopic,approvedOcctype,"false");
                  }
                }
                else{
                  indexKeyword.put("approved","false");
                  builder.makeOccurrence(existingKeywordTopic,approvedOcctype,"false");
                }

                foundKeys.add(topicId);
              }
              keyMap.put(topicId, existingKeywordTopic);
              indexKeywordSet.put(indexKeyword);
            }//end if existing
          }//end for keywordSets
        }//end while topicIDKey
        indexPayLoad.put(topicId, indexKeywordSet);
      }//end for keywords

      JSONArray indexPayloadArray = Utils.indexPayload(indexPayLoad, siteUrl, topicMap);
      topicMap.getStore().commit(); // Persist object graph.
      if (indexPayloadArray.length() > 0) {
        Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
        boolean indexed = indexer.postToIndex(indexPayloadArray.toString(), false, "keywords");
      }
    } catch (Exception e) {
      if (e instanceof BadObjectReferenceException) {
        throw new BadObjectReferenceException(e.getMessage());
      } else {
        throw new NdlaServiceException(e.getMessage(),e);
      }
      /*
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
      */
    } finally { // Release all topic map engine resources.
      /*
      if (store != null) {
        store.close();
      }
      */

    }
    return keyMap;
  }

    synchronized public ArrayList<TopicIF> saveKeywordsBySubjectMatter(String subjectMatterIdentifier, List<String> keywords, NdlaSite site) throws NdlaServiceException {
        ArrayList<TopicIF> result = new ArrayList<TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            TopicIF subjectTopic = null;

            String siteUrl = "";
            switch (site) {
                case NDLA:
                    siteUrl = NDLA_SITE_URL;
                    break;
                case DELING:
                    siteUrl = DELING_SITE_URL;
                    break;
                case NYGIV:
                    siteUrl = NYGIV_SITE_URL;
                    break;
            }

            try {
                String subjectId = subjectMatterIdentifier.substring(subjectMatterIdentifier.lastIndexOf("_") + 1);
                String subjectUrl = siteUrl + "node/" + subjectId;
                subjectTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(subjectUrl));
            } catch (MalformedURLException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            }

            if (null != subjectTopic) {
                result.add(subjectTopic);
                for (String keywordId : keywords) {
                    TopicIF kewordTopic = null;
                    try {
                        kewordTopic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + keywordId));
                        if (null != kewordTopic) {
                            result.add(kewordTopic);
                            String relationType = "http://psi.topic.ndla.no/#subject-keyword";
                            String player1Type = "http://psi.topic.ndla.no/#subjectmatter";
                            String player2Type = "http://psi.topic.ndla.no/#keyword";

                            Utils.createAssociation(subjectTopic, kewordTopic, site, relationType, player1Type, player2Type, topicMap);
                        }
                    } catch (MalformedURLException urlEX) {
                        throw new NdlaServiceException(urlEX.getMessage());
                    }
                }//end for in keywords
            }
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    synchronized public ArrayList<String> deleteKeywordsByKeywordId(List<String> identifiers, NdlaSite site) throws NdlaServiceException {
        ArrayList<String> result = new ArrayList<String>();
        ArrayList<String> deleteFromIndex = new ArrayList<>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            for (String identifier : identifiers) {
                deleteFromIndex.add(identifier);
                TopicIF topicToBeDeleted = null;
                try {
                    topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + identifier));
                    if (null != topicToBeDeleted) {
                        String psi = topicToBeDeleted.getSubjectIdentifiers().iterator().next().getAddress();
                        result.add(psi);
                        topicToBeDeleted.remove();
                    }
                } catch (MalformedURLException e) {
                    throw new NdlaServiceException(e.getMessage(),e);
                }
                store.commit(); // Persist object graph.
            }
            //delete from index
            if (deleteFromIndex.size() > 0) {
                Indexer indexer = new Indexer(_indexHostUrl, _indexSOLRHostUrl, _indexHostUrlUser, _indexHostUrlPass);
                boolean deleted = indexer.deleteFromIndex(deleteFromIndex,"keywords");
                boolean commitSuccess = indexer.commitIndex("keywords");
                if (!deleted || !commitSuccess) {
                  throw new NdlaServiceException("Could not delete the keyword from the search index");
                }
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    // ***** Nodes *****

    public ArrayList<NdlaTopic> getNodes(int limit, int offset, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase();
        String query = String.format("select $COLUMN from instance-of($COLUMN, %s-node) order by $COLUMN limit %s offset %s?", siteIdentifier, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }

    public ArrayList<NdlaTopic> getNodesByTopicId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase();
        String query = String.format("select $COLUMN from %snode-topic-generic(%s: topic, $COLUMN : %s-node) order by $COLUMN limit %s offset %s?", siteIdentifier, identifier, siteIdentifier, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }

  public ArrayList<NdlaTopic> getNodesBySiteTopicId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
    String siteIdentifier = site.toString().toLowerCase();
    String query = String.format("select $COLUMN from %snode-topic(%s: %s-topic, $COLUMN : %s-node) order by $COLUMN limit %s offset %s?", siteIdentifier, identifier, siteIdentifier, siteIdentifier, Integer.toString(limit), Integer.toString(offset));

    return executeCollectionQuery(query);
  }

    public ArrayList<NdlaTopic> getNodesByKeywordId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase();
        String query = String.format("select $COLUMN from %snode-keyword(%s: keyword, $COLUMN : %s-node) order by $COLUMN limit %s offset %s?", siteIdentifier, identifier, siteIdentifier, Integer.toString(limit), Integer.toString(offset));
        return executeCollectionQuery(query);
    }

    synchronized public NdlaTopic getNodeByNodeId(String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        NdlaTopic result = null;
        String siteUrl = "";
        switch (site) {
            case NDLA:
                siteUrl = NDLA_SITE_URL;
                break;
            case DELING:
                siteUrl = DELING_SITE_URL;
                break;
            case NYGIV:
                siteUrl = NYGIV_SITE_URL;
                break;
        }

        String[] split;
        split = identifier.split("_");
        String nodeUrl = siteUrl + "node/" + split[1];
        String query = String.format("select $COLUMN from subject-identifier($COLUMN, \"%s\")?", nodeUrl);

        //return new NdlaTopic((TopicIF) executeObjectQuery(query), null); // FIXED: Inlined query to ensure persistence-layer is available for the creation of the NdlaTopic (BK).

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            ParsedStatementIF statement = null;
            QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
            QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

            QueryResultIF tologResult = null;
            try {
                statement = queryProcessor.parse(query);
                if (null != statement) {
                    tologResult = ((ParsedQueryIF) statement).execute();
                    if (tologResult.next()) {
                        result = new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), queryProcessor);
                    }
                }
            } catch (BadObjectReferenceException e) {
                throw new BadObjectReferenceException(e.getMessage());
            } catch (InvalidQueryException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            } finally {
                if (null != tologResult) {
                    tologResult.close();
                }
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                if (e instanceof BadObjectReferenceException) {
                    throw new BadObjectReferenceException(e.getMessage());
                } else {
                    throw new NdlaServiceException(e.getMessage(),e);
                }
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    // ***** Subjects *****


  public TopicIF createSubject(String data)throws NdlaServiceException, BadObjectReferenceException{
    TopicIF subjectTopic = null;

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = store.getBaseAddress();


      JSONObject payload = new JSONObject(data);
      TopicIF checkDuplicateSubject = null;
      String subjectTopicId = payload.getString("subjectId");
      String subjectId = "http://psi.topic.ndla.no/#subject_"+subjectTopicId;
      checkDuplicateSubject = Utils.isDuplicateTopic(subjectId,topicMap);

      if(null == checkDuplicateSubject) {
        String topicId = "subject_"+subjectTopicId;
        subjectTopic = builder.makeTopic();

        TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#subjectmatter"));
        subjectTopic.addType(type);

        subjectTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));

        JSONArray names = payload.getJSONArray("names");
        for (int i = 0; i < names.length(); i++) {
          JSONObject langName = names.getJSONObject(i);

          String langNameLang = langName.getString("language-id");
          String langNameString = langName.getString("name");
          // Names and scopes
          Utils.createBasename(subjectTopic, langNameString, langNameLang, topicMap);
        }
        JSONArray sites = payload.getJSONArray("originating-sites");
        for(int j = 0; j < sites.length(); j++) {
          String site = sites.getString(j);
          TopicIF siteOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#originating-site"));
          builder.makeOccurrence(subjectTopic, siteOcctype, new URILocator(site));
        }

        String udir_psi = payload.getString("udir-psi");
        TopicIF udirOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#udir-subject"));
        builder.makeOccurrence(subjectTopic, udirOcctype, new URILocator(udir_psi));

        subjectTopic.addSubjectIdentifier(new URILocator(subjectId));

        JSONArray psis = payload.getJSONArray("psis");
        for(int k = 0; k < psis.length(); k++) {
          String psi = psis.getString(k);
          subjectTopic.addSubjectIdentifier(new URILocator(psi));
        }
      }
      store.commit();
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return subjectTopic;
  }

  /**
   *
   * @param data
   * @return OntologyDomain topic
   * @throws NdlaServiceException
   * @throws BadObjectReferenceException
   */
  public JSONObject createOntologyDomain(String data)throws NdlaServiceException, BadObjectReferenceException{
    TopicIF domainTopic = null;
    JSONObject resultData = new JSONObject();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = store.getBaseAddress();


      JSONObject payload = new JSONObject(data);
      TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#ontology-domain"));
      String topicId = "ontologyDomain-" + Utils.makeRandomId(topicMap);
      domainTopic = builder.makeTopic();
      domainTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/domainTopics/#" + topicId));
      domainTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));
      domainTopic.addType(type);


      JSONArray names = payload.getJSONArray("names");
      for (int i = 0; i < names.length(); i++) {
        JSONObject langName = names.getJSONObject(i);

        String langNameLang = langName.getString("languageId");
        String langNameString = langName.getString("name");
        // Names and scopes
        Utils.createBasename(domainTopic, langNameString, langNameLang, topicMap);
      }

      store.commit();
      resultData.put("topicId", topicId);
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return resultData;
  }


  public JSONObject updateOntologyDomain(String data)throws NdlaServiceException, BadObjectReferenceException{
    TopicIF domainTopic = null;
    JSONObject resultData = new JSONObject();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = store.getBaseAddress();

      JSONObject payload = new JSONObject(data);

      String domainId = payload.getString("domainId");
      LocatorIF domainPsi = new URILocator(" http://psi.topic.ndla.no/domainTopics/#"+domainId);
      domainTopic = topicMap.getTopicBySubjectIdentifier(domainPsi);

      if(null != domainTopic) {
        Utils.deleteBasenames(domainPsi, topicMap);

        JSONArray names = payload.getJSONArray("names");
        for (int i = 0; i < names.length(); i++) {
          JSONObject langName = names.getJSONObject(i);

          String langNameLang = langName.getString("languageId");
          String langNameString = langName.getString("name");
          // Names and scopes
          Utils.createBasename(domainTopic, langNameString, langNameLang, topicMap);
        }

      }

      store.commit();
      resultData.put("topicId",domainId);
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return resultData;
  }

  public JSONObject deleteOntologyDomains(String data) throws NdlaServiceException, BadObjectReferenceException {
   JSONObject deletedTopic = new JSONObject();

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      try {
        JSONArray ontologyIds = new JSONArray(data);
        for(int i = 0; i < ontologyIds.length(); i++) {
          String ontologyId = ontologyIds.getString(i);
          String psi = "http://psi.topic.ndla.no/domainTopics/#"+ontologyId;
          TopicIF topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
          DeletionUtils.removeDependencies(topicToBeDeleted);
          if (null != topicToBeDeleted) {
            topicToBeDeleted.remove();
            deletedTopic.put("topicId",ontologyId);
          }

        }
      } catch (MalformedURLException e) {
        throw new NdlaServiceException(e.getMessage(),e);
      }
      store.commit(); // Persist object graph.
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(),e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return deletedTopic;
  }

  public JSONObject getOntologyDomains(int limit, int offset) throws NdlaServiceException, BadObjectReferenceException {
    JSONObject payload = new JSONObject();
    JSONArray ontologyDomains = new JSONArray();

    String query = String.format("select $COLUMN from direct-instance-of($COLUMN,ontology-domain), not($COLUMN = person) order by $COLUMN?");
    ArrayList<NdlaTopic> topics =  executeCollectionQuery(query);

    try {
      for(NdlaTopic topic : topics){
        JSONObject ontologyDomain = new JSONObject();
        ArrayList<String> psis = topic.getPsis();
        JSONArray JSONPsis = new JSONArray();
        for(String psi : psis){
          JSONPsis.put(psi);
        }
        ontologyDomain.put("psis",JSONPsis);

        HashMap<String,String> names = topic.getNames();
        Iterator<String> nameIt = names.keySet().iterator();
        JSONArray JSONNames = new JSONArray();
        while(nameIt.hasNext()){
          JSONObject name = new JSONObject();
          String language = (String) nameIt.next();
          String nameString = names.get(language);
          name.put("language",language);
          name.put("name",nameString);
          JSONNames.put(name);
        }//end while
        ontologyDomain.put("names",JSONNames);
        ontologyDomains.put(ontologyDomain);
        payload.put("domains",ontologyDomains);
      }//end NDLATopics
    } catch (Exception e) {
      if (e instanceof BadObjectReferenceException) {
        throw new BadObjectReferenceException(e.getMessage());
      } else {
        throw new NdlaServiceException(e.getMessage(),e);
      }
    }

    return payload;
  }

  /**
   *
   * @param data
   * @return Person topic
   * @throws NdlaServiceException
   * @throws BadObjectReferenceException
   */
  public JSONObject createPerson(String data)throws NdlaServiceException, BadObjectReferenceException{
    TopicIF personTopic = null;
    JSONObject resultData = new JSONObject();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = store.getBaseAddress();


      JSONObject payload = new JSONObject(data);
      TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#person"));
      String topicId = "person-" + Utils.makeRandomId(topicMap);
      personTopic = builder.makeTopic();
      personTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/persons/#" + topicId));
      personTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));
      personTopic.addType(type);


      JSONArray names = payload.getJSONArray("names");
      for (int i = 0; i < names.length(); i++) {
        JSONObject langName = names.getJSONObject(i);

        String langNameLang = langName.getString("languageId");
        String langNameString = langName.getString("name");
        // Names and scopes
        Utils.createBasename(personTopic, langNameString, langNameLang, topicMap);
      }

      String emailAddress = payload.getString("emailAddress");
      // Visibility occurrence
      TopicIF emailOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#email"));
      builder.makeOccurrence(personTopic, emailOccType, emailAddress);

      store.commit();
      resultData.put("topicId",topicId);
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return resultData;
  }


  public JSONObject updatePerson(String data)throws NdlaServiceException, BadObjectReferenceException{
    TopicIF personTopic = null;
    JSONObject resultData = new JSONObject();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = store.getBaseAddress();

      JSONObject payload = new JSONObject(data);

      String personId = payload.getString("personId");
      LocatorIF personPsi = new URILocator("http://psi.topic.ndla.no/persons/#"+personId);
      personTopic = topicMap.getTopicBySubjectIdentifier(personPsi);

      if(null != personTopic) {
        Utils.deleteBasenames(personPsi, topicMap);
        Utils.deleteOccurrences(personPsi, topicMap);

        JSONArray names = payload.getJSONArray("names");
        for (int i = 0; i < names.length(); i++) {
          JSONObject langName = names.getJSONObject(i);

          String langNameLang = langName.getString("languageId");
          String langNameString = langName.getString("name");
          // Names and scopes
          Utils.createBasename(personTopic, langNameString, langNameLang, topicMap);
        }

        String emailAddress = payload.getString("emailAddress");
        // Visibility occurrence
        TopicIF emailOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#email"));
        builder.makeOccurrence(personTopic, emailOccType, emailAddress);
      }

      store.commit();
      resultData.put("topicId",personId);
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return resultData;
  }

  public JSONArray deletePersons(String data) throws NdlaServiceException, BadObjectReferenceException {
    JSONArray deletedTopics = new JSONArray();

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      try {
        JSONArray personIds = new JSONArray(data);
        for(int i = 0; i < personIds.length(); i++) {
          String personId = personIds.getString(i);
          String psi = "http://psi.topic.ndla.no/persons/#"+personId;
          TopicIF topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
          DeletionUtils.removeDependencies(topicToBeDeleted);
          if (null != topicToBeDeleted) {
            topicToBeDeleted.remove();
            deletedTopics.put(personId);
          }

        }

      } catch (MalformedURLException e) {
        throw new NdlaServiceException(e.getMessage(),e);
      }
      store.commit(); // Persist object graph.
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(),e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return deletedTopics;
  }

  public JSONObject getPersons(int limit, int offset) throws NdlaServiceException, BadObjectReferenceException {
    JSONObject payload = new JSONObject();
    JSONArray persons = new JSONArray();

    String query = String.format("select $COLUMN from direct-instance-of($COLUMN,person) order by $COLUMN?");
    ArrayList<NdlaTopic> topics =  executeCollectionQuery(query);

    try {
      for(NdlaTopic topic : topics){
        JSONObject person = new JSONObject();
        ArrayList<String> psis = topic.getPsis();
        JSONArray JSONPsis = new JSONArray();
        for(String psi : psis){
          JSONPsis.put(psi);
        }
        person.put("psis",JSONPsis);

        HashMap<String,String> names = topic.getNames();
        Iterator<String> nameIt = names.keySet().iterator();
        JSONArray JSONNames = new JSONArray();
        while(nameIt.hasNext()){
          JSONObject name = new JSONObject();
          String language = (String) nameIt.next();
          String nameString = names.get(language);
          name.put("language",language);
          name.put("name",nameString);
          JSONNames.put(name);
        }//end while
        person.put("names",JSONNames);

        String email = topic.getEmail();
        person.put("emailAddress",email);

        persons.put(person);
        payload.put("persons",persons);
      }//end NDLATopics
    } catch (Exception e) {
      if (e instanceof BadObjectReferenceException) {
        throw new BadObjectReferenceException(e.getMessage());
      } else {
        throw new NdlaServiceException(e.getMessage(),e);
      }
    }

    return payload;
  }




  public JSONArray saveTopicsByDomain(String domainIdentifier, String domainType, String domainPsi, String topics)  throws NdlaServiceException, BadObjectReferenceException {
    JSONArray result = new JSONArray();

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      LocatorIF baseAddress = store.getBaseAddress();
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      TopicIF domainTopic = null;

      HashMap<String,String> reifierData = new HashMap<>();
      if(domainType.length() > 0){
        reifierData.put("http://psi.topic.ndla.no/#ontology_topic_domain_type",domainPsi);
      }

      try {
        String domainUrl = "http://psi.topic.ndla.no/"+domainType+"/#" + domainIdentifier;
        domainTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(domainUrl));
      } catch (MalformedURLException urlEX) {
        throw new NdlaServiceException(urlEX.getMessage());
      }

      JSONArray payload = new JSONArray(topics);

      if (null != domainTopic) {
        for (int i = 0; i < payload.length(); i++) {
          String topicId = payload.getString(i);
          String relationType = "http://psi.topic.ndla.no/#domain_ontology_has_topic";

          String player1Type = "http://psi.topic.ndla.no/#ontology-domain";
          String player2Type = "http://psi.topic.ndla.no/#topic";

          if(!domainHasTopic(domainIdentifier, domainType,"http://psi.topic.ndla.no/topics/#"+topicId, relationType)){
            TopicIF otherTopicObject = (TopicIF) topicMap.getObjectByItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));

            Utils.createReifiedAssociation(domainTopic, otherTopicObject, relationType, player1Type, player2Type, reifierData, topicMap);
            result.put("http://psi.topic.ndla.no/topics/#" + topicId);
          }
        }
      }//end if domainTopic


      store.commit(); // Persist object graph.
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(),e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return result;
  }


  public JSONArray saveDomainOntologyTopicAssociations(String domainIdentifier,String domainType,  String data)  throws NdlaServiceException, BadObjectReferenceException {
    JSONArray result = new JSONArray();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try{
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      LocatorIF baseAddress = store.getBaseAddress();
      TopicIF domainTopic = null;

      try {
        String domainPsi = "http://psi.topic.ndla.no/#";
        if(domainType.equals("domainTopics")){
          domainPsi += "ontology-domain";
        }
        else if(domainType.equals("persons")){
          domainPsi += "person";
        }
        String domainUrl = "http://psi.topic.ndla.no/"+domainType+"/#" + domainIdentifier;
        domainTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(domainUrl));



        if(null != domainTopic) {
          HashMap<String,String> reifierData = new HashMap<>();
          if(domainType.length() > 0){
            reifierData.put("http://psi.topic.ndla.no/#ontology_topic_domain_type",domainUrl);
          }
          JSONObject payload = new JSONObject(data);
          JSONArray associations = payload.getJSONArray("associations");
          for (int i = 0; i < associations.length(); i++) {
            JSONObject association = associations.getJSONObject(i);
            String relationTypeId = association.getString("relationId");
            JSONObject assocObject = new JSONObject();

            JSONArray relatedTopics = association.getJSONArray("topics");
            for (int j = 0; j < relatedTopics.length(); j++) {
              ArrayList<TopicIF> tempTopics = new ArrayList<>();
              JSONObject relatedTopic = relatedTopics.getJSONObject(j);


              JSONArray rolePlayers = relatedTopic.getJSONArray("roles");
              JSONObject roleObject1 = rolePlayers.getJSONObject(0);
              JSONObject roleObject2 = rolePlayers.getJSONObject(1);

              TopicIF topicObject1 = (TopicIF) topicMap.getObjectByItemIdentifier(baseAddress.resolveAbsolute("#" + roleObject1.getString("topicId")));
              if(!domainHasTopic(domainIdentifier, domainType, "http://psi.topic.ndla.no/topics/#"+roleObject1.getString("topicId"), "http://psi.topic.ndla.no/#domain_ontology_has_topic")){
                JSONArray savedTopic = saveTopicsByDomain(domainIdentifier, domainType, domainPsi, "[\""+roleObject1.getString("topicId")+"\"]");
              }

              if(!domainHasTopic(domainIdentifier, domainType, "http://psi.topic.ndla.no/topics/#"+roleObject2.getString("topicId"), "http://psi.topic.ndla.no/#domain_ontology_has_topic")){
                JSONArray savedTopic = saveTopicsByDomain(domainIdentifier, domainType, domainPsi, "[\""+roleObject2.getString("topicId")+"\"]");
              }

              TopicIF topicObject2 = (TopicIF) topicMap.getObjectByItemIdentifier(baseAddress.resolveAbsolute("#" + roleObject2.getString("topicId")));
              tempTopics.add(topicObject1);
              tempTopics.add(topicObject2);

              String roleTypeId1 = roleObject1.getString("roleId");
              String roleTypeId2 = roleObject2.getString("roleId");

              if (relationTypeId.length() > 0 && roleTypeId1.length() > 0 && roleTypeId2.length() > 0) {
                Utils.createReifiedAssociation(topicObject1, topicObject2, relationTypeId, roleTypeId1, roleTypeId2, reifierData, topicMap);
                assocObject.put(relationTypeId, tempTopics);
                result.put(assocObject);
              }
            }//end relatedTopics
          }//end for assocs
        }//end if

      } catch (MalformedURLException urlEX) {
        throw new NdlaServiceException(urlEX.getMessage());
      }
      store.commit(); // Persist object graph.
    }
    catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage());
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return result;
  }


  public JSONObject getDomainOntologyTopicRelations(String domainIdentifier, String domainType) throws NdlaServiceException, BadObjectReferenceException {
    JSONObject result = new JSONObject();
    JSONArray associations = new JSONArray();
    HashMap<String,ArrayList<HashMap<String,String>>> associationData = new HashMap<>();
    HashMap<String,String> rolePlayerMap = new HashMap<String,String>();
    HashMap<String,ArrayList<HashMap<String,String>>> playerNameMap = new HashMap<>();
    HashMap<String,HashMap<String,String>> roleNameMap = new HashMap<>();
    ArrayList<String> otherTopicRoles = new ArrayList<>();
    TreeMap<String,ArrayList<String>> otherTopicMap = new TreeMap<>();

    ArrayList<String> blackList = new ArrayList<>();
    blackList.add("http://psi.topic.ndla.no/#domain_ontology_has_topic");
    String domainTypeString = "";
    if(domainType.equals("domainTopics")){
      domainTypeString += "ontology-domain";
    }
    else if(domainType.equals("persons")){
      domainTypeString += "person";
    }

    String query = String.format("association($COLUMN),reifies($TOPIC, $COLUMN), ontology_topic_domain_type($TOPIC,\"http://psi.topic.ndla.no/#%s\")?" ,domainTypeString);
    ArrayList<Object> assocs = executeObjectCollectionQuery(query);
    for( Object assoc : assocs) {
      boolean hasOntologyRelationType = false;
      String currentAssociationTypePSI = "";
      String currentTypeTypePSI = "";
      String currentOntologyRelationType = "";
      AssociationIF associationIF = (AssociationIF) assoc;
      TopicIF reifier = associationIF.getReifier();

      Collection<OccurrenceIF> reifierOccurrences = reifier.getOccurrences();
      for(OccurrenceIF reifierOccurrence : reifierOccurrences) {
        String occIdentifier = reifierOccurrence.getType().getSubjectIdentifiers().iterator().next().getAddress();
        if(occIdentifier.contains("ontology_topic_domain_type") && reifierOccurrence.getValue().equals("http://psi.topic.ndla.no/#"+domainTypeString)){
          hasOntologyRelationType = true;
          currentOntologyRelationType = reifierOccurrence.getValue();
          break;
        }
      }

      if(hasOntologyRelationType){
        String reifierPSI = reifier.getSubjectIdentifiers().iterator().next().getAddress();
        TopicIF associationType = associationIF.getType();
        Collection<TopicIF> typeTypes = associationType.getTypes();
        String typeTypePSI = "";
        for(TopicIF typeType : typeTypes) {
          Collection<LocatorIF> typeTypePsis = typeType.getSubjectIdentifiers();
          if(typeTypePsis.size() == 1){
            typeTypePSI = typeTypePsis.iterator().next().getAddress();
          }
          else{
            int typeTypeCounter = 0;
            for(LocatorIF typeTypeLocator : typeTypePsis) {
              if(typeTypeCounter == 0) {
                typeTypePSI = typeTypeLocator.getAddress();
                break;
              }
            }
          }
        }

        Collection<LocatorIF> associationTypeLocators = associationType.getSubjectIdentifiers();
        String associationTypePSI = "";
        if(associationTypeLocators.size() == 1) {
          associationTypePSI = associationTypeLocators.iterator().next().getAddress();
        }
        else {
          int associationTypeCounter = 0;
          for(LocatorIF associationTypeLocator : associationTypeLocators) {
            if(associationTypeCounter == 0) {
              associationTypePSI = associationTypeLocator.getAddress();
              break;
            }
          }
        }//end if associationTypeLocators

        Collection<TopicNameIF> assocTypeNames = associationType.getTopicNames();
        ArrayList<HashMap<String,String>> currentNameList = new ArrayList<>();

        if (assocTypeNames.size() > 0) {
          for (TopicNameIF assocTypeName : assocTypeNames) {
            HashMap<String,String> currentMap = new HashMap<>();
            Collection<TopicIF> assocTypeScopes = assocTypeName.getScope();
            String combined = "";
            for (TopicIF assocTypeScope : assocTypeScopes) {

              Collection<LocatorIF> assocTypeScopeLocators = assocTypeScope.getSubjectIdentifiers();
              for (LocatorIF assocTypeScopeLocator : assocTypeScopeLocators) {

                if (assocTypeScopeLocator.getAddress().contains("iso")) {
                  currentMap.put(assocTypeScopeLocator.getAddress()+"_"+assocTypeName.getObjectId(),assocTypeName.getValue());
                }

                if(!assocTypeScopeLocator.getAddress().contains("iso") && !assocTypeScopeLocator.getAddress().contains("language-neutral"))
                  rolePlayerMap.put(assocTypeName.getObjectId(),assocTypeScopeLocator.getAddress());
              }//end for locators

            } //end for scopes

            currentNameList.add(currentMap);
          }//end for names
          if(!blackList.contains(associationTypePSI)){
            associationData.put(associationTypePSI,currentNameList);
            currentAssociationTypePSI = associationTypePSI;
            currentTypeTypePSI = typeTypePSI;
          }
        }//end if typenames

        if(currentAssociationTypePSI.length() > 0) {
          Collection<AssociationRoleIF> roles =  associationIF.getRoles();
          String rolePlayerPSI = "";
          String accumulate = "";
          for (AssociationRoleIF role : roles) {
            TopicIF roleType = role.getType();
            Collection<LocatorIF> roleTypeLocators = roleType.getSubjectIdentifiers();
            String rolePSI = "";

            if(roleTypeLocators.size() == 1) {
              rolePSI = roleTypeLocators.iterator().next().getAddress();
            }
            else {
              int roleTypeCounter = 0;
              for(LocatorIF roleTypeLocator : roleTypeLocators) {
                if(roleTypeCounter == 0) {
                  rolePSI = roleTypeLocator.getAddress();
                  break;
                }
              }
            }

            if(!roleNameMap.containsKey(rolePSI)){
              HashMap<String,String> tempRoleNames = Utils.getTopicNames(roleType);
              roleNameMap.put(rolePSI,tempRoleNames);
            }

            TopicIF rolePlayer = role.getPlayer();
            Collection<LocatorIF> rolePlayerLocators = rolePlayer.getSubjectIdentifiers();

            if(rolePlayerLocators.size() == 1) {
              String locatorString = rolePlayerLocators.iterator().next().getAddress();
              if(locatorString.contains("#topic")) {
                rolePlayerPSI = locatorString;
              }
            }
            else {
              int rolePlayerCounter = 0;
              for(LocatorIF rolePlayerLocator : rolePlayerLocators) {
                if(rolePlayerLocator.getAddress().contains("#topic")) {
                  rolePlayerPSI = rolePlayerLocator.getAddress();
                  break;
                }
              }
            }
            accumulate += rolePlayerPSI+"_"+associationTypePSI+"_"+typeTypePSI+"_"+rolePSI+"_"+reifierPSI+"_"+currentOntologyRelationType+"§";

            Collection<TopicNameIF> rolePlayerNames = rolePlayer.getTopicNames();
            ArrayList<HashMap<String,String>> currentRoleNameList = new ArrayList<>();
            ArrayList<String> foundNameMaps = new ArrayList<>();
            if (rolePlayerNames.size() > 0) {
              for (TopicNameIF rolePlayerName : rolePlayerNames) {
                HashMap<String,String> currentNameMapper = new HashMap<>();
                Collection<TopicIF> rolePlayerNameScopes = rolePlayerName.getScope();
                for (TopicIF rolePlayerNameScope : rolePlayerNameScopes) {
                  Collection<LocatorIF> rolePlayerNameScopeLocators = rolePlayerNameScope.getSubjectIdentifiers();

                  for (LocatorIF rolePlayerNameScopeLocator : rolePlayerNameScopeLocators) {
                    if(rolePlayerNameScopeLocator.getAddress().length() > 0){
                      //currentNameMapper.put("language",rolePlayerNameScopeLocator.getAddress());
                      if(!foundNameMaps.contains(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue())){
                        currentNameMapper.put(rolePlayerNameScopeLocator.getAddress(),rolePlayerName.getValue());
                        foundNameMaps.add(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue());
                      }

                    }
                  }//end for locators
                } //end for scopes

                if(currentNameMapper.size() > 1){
                  Iterator cMapIt = currentNameMapper.keySet().iterator();
                  ArrayList<String> currentNameMaps = new ArrayList<>();
                  while(cMapIt.hasNext()){
                    HashMap<String,String> newCurrentNameMapper = new HashMap<>();
                    String cMapLang = (String) cMapIt.next();
                    if(!currentNameMaps.contains(cMapLang+"_"+currentNameMapper.get(cMapLang))){
                      newCurrentNameMapper.put(cMapLang,currentNameMapper.get(cMapLang));
                      currentRoleNameList.add(newCurrentNameMapper);
                      currentNameMaps.add(cMapLang+"_"+currentNameMapper.get(cMapLang));
                    }

                  }
                }
                else{
                  currentRoleNameList.add(currentNameMapper);
                }

              }//end for names
              playerNameMap.put(rolePlayerPSI,currentRoleNameList);
            }//end if typenames
          }//end for roles
          otherTopicRoles.add(accumulate);
        }//end if currentAssocType
      }//end if hasCorrectRelationType))


    }//end for assocs

    Collections.sort(otherTopicRoles);
    Collections.reverse(otherTopicRoles);

    for(String otherTopicRole : otherTopicRoles) {
      String[] roleArray = otherTopicRole.split("§");
      String role1 = roleArray[0];
      String role2 = roleArray[1];

      String[] httpArray1 = role1.split("_http");
      String[] httpArray2 = role2.split("_http");

      String topicId1 = httpArray1[0];
      String assocId1 = httpArray1[1];
      String assocType1 = httpArray1[2];
      String roleId1 = httpArray1[3];
      String reifierId1 = httpArray1[4];
      String ontologyType1 = "";
      if(httpArray1.length > 5){
        ontologyType1 = httpArray1[5];
      }
      String topicId2 = httpArray2[0];
      String assocId2 = httpArray2[1];
      String assocType2 = httpArray2[2];
      String roleId2 = httpArray2[3];
      String reifierId2 = httpArray2[4];
      String ontologyType2 = "";
      if(httpArray2.length > 5){
        ontologyType2 = httpArray2[5];
      }

      if(assocId1.equals(assocId2)) {
        //JSONObject assocObject = new JSONObject();
        String otherTopicId = "";
        String topicId = "";
        String assocId = assocId1;
        String assocType = assocType1;
        String thisRoleId = "";
        String thatRoleId = "";
        String thisReifierId = reifierId1;
        String thisOntologyType = ontologyType1;

        if(topicId1.contains("kl06")){
          otherTopicId = topicId1;
          topicId = topicId2;
          thisRoleId = roleId1;
          thatRoleId = roleId2;
        }
        else if(topicId2.contains("kl06")){
          otherTopicId = topicId2;
          topicId = topicId1;
          thisRoleId = roleId2;
          thatRoleId = roleId1;
        }
        else if(topicId2.contains("#topic-")){
          topicId = topicId2;
          otherTopicId = topicId1;
          thisRoleId = roleId2;
          thatRoleId = roleId1;
        }
        else if(topicId1.contains("#topic-")){
          otherTopicId = topicId2;
          topicId = topicId1;
          thisRoleId = roleId1;
          thatRoleId = roleId2;
        }

        if(!otherTopicMap.containsKey(otherTopicId)){
          otherTopicMap.put(otherTopicId,new ArrayList<String>());
        }
        otherTopicMap.get(otherTopicId).add("http"+thisRoleId+"_"+"http"+assocId+"_"+topicId+"_"+"http"+assocType+"_"+topicId+"_"+"http"+thatRoleId+"_http"+thisReifierId+"_http"+thisOntologyType);

      }//end if associd matches
    }//end for

    try{
      Iterator<String> it = otherTopicMap.keySet().iterator();
      while(it.hasNext()) {
        JSONObject otherTopicJSON = new JSONObject();
        String otherTopicId = (String) it.next();
        otherTopicJSON.put("topicId",otherTopicId);
        JSONArray otherTopicNames = new JSONArray();
        ArrayList<HashMap<String,String>> otherTopicNameMapping = playerNameMap.get(otherTopicId);

        for(HashMap<String,String> otherTopicLangNames : otherTopicNameMapping) {
          Iterator<String> otherTopicLangNamesIterator = otherTopicLangNames.keySet().iterator();
          JSONObject otherTopicNameObject = new JSONObject();
          ArrayList<String> foundOtherTopicNames = new ArrayList<>();
          while (otherTopicLangNamesIterator.hasNext()){
            String otherTopicNameLanguage = (String) otherTopicLangNamesIterator.next();
            if(!foundOtherTopicNames.contains(otherTopicNameLanguage+"_"+otherTopicLangNames.get(otherTopicNameLanguage))) {
              otherTopicNameObject.put(otherTopicNameLanguage,otherTopicLangNames.get(otherTopicNameLanguage));
              foundOtherTopicNames.add(otherTopicNameLanguage+"_"+otherTopicLangNames.get(otherTopicNameLanguage));
            }
          }

          if(otherTopicNameObject.length() > 0) {
            otherTopicNames.put(otherTopicNameObject);
          }
        }

        otherTopicJSON.put("topicNames",otherTopicNames);


        ArrayList<String> data = otherTopicMap.get(otherTopicId);
        JSONArray assocJSON = new JSONArray();
        for(String assocData : data) {
          String[] assocArray = assocData.split("_http");
          JSONObject assocObject = new JSONObject();
          String thisRoleId = assocArray[0];
          String assocId = "http"+assocArray[1];
          String assocType = "http"+assocArray[3];
          String reifierId = "http"+assocArray[6];
          String ontologyType = "";
          if(assocArray.length > 7){
            ontologyType = "http"+assocArray[7];
          }

          assocObject.put("associationId",assocId);
          assocObject.put("associationType",assocType);
          assocObject.put("reifierId",reifierId);
          assocObject.put("ontologyType",ontologyType);
          JSONArray assocTypeNames = new JSONArray();

          ArrayList<HashMap<String,String>> typeNameMap = associationData.get(assocId);
          for(HashMap<String,String> typeLangNames : typeNameMap){
            Iterator<String> typeLangNamesIterator = typeLangNames.keySet().iterator();
            JSONObject typeNameObject = new JSONObject();
            while (typeLangNamesIterator.hasNext()){
              String typeNameLanguage = (String) typeLangNamesIterator.next();
              String[] typeNameLanguageArray = typeNameLanguage.split("_");
              typeNameObject.put("language",typeNameLanguageArray[0]);
              typeNameObject.put("role",rolePlayerMap.get(typeNameLanguageArray[1]));
              typeNameObject.put("name",typeLangNames.get(typeNameLanguage));
            }
            assocTypeNames.put(typeNameObject);
          }
          assocObject.put("associationTypeNames",assocTypeNames);

          assocObject.put("rolePlayedByOtherTopic",thisRoleId);
          String topicId = "http"+assocArray[4];
          assocObject.put("topicId",topicId);


          JSONArray topicNames = new JSONArray();
          ArrayList<HashMap<String,String>> topicNameMapping = playerNameMap.get(topicId);
          ArrayList<String> foundTopicNames = new ArrayList<>();

          for(HashMap<String,String> topicLangNames : topicNameMapping) {
            Iterator<String> topicLangNamesIterator = topicLangNames.keySet().iterator();
            JSONObject topicNameObject = new JSONObject();
            while (topicLangNamesIterator.hasNext()){
              String topicNameLanguage = (String) topicLangNamesIterator.next();

              if(!foundTopicNames.contains(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage))) {
                topicNameObject.put(topicNameLanguage,topicLangNames.get(topicNameLanguage));
                foundTopicNames.add(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage));
              }
            }

            if(topicNameObject.length() > 0) {
              topicNames.put(topicNameObject);
            }
          }

          assocObject.put("topicNames",topicNames);


          String thatRoleId = "http"+assocArray[5];
          assocObject.put("rolePlayedByTopic",thatRoleId);
          assocJSON.put(assocObject);
        }

        otherTopicJSON.put("relations",assocJSON);
        associations.put(otherTopicJSON);
      }

      JSONArray roleNameMapping = new JSONArray();
      Iterator roleNameMapIt = roleNameMap.keySet().iterator();
      while (roleNameMapIt.hasNext()){
        JSONObject roleJSONObject = new JSONObject();
        JSONArray roleJSONNameArray = new JSONArray();

        String rolePsi = (String) roleNameMapIt.next();
        roleJSONObject.put("psi",rolePsi);
        HashMap<String,String> roleNames = roleNameMap.get(rolePsi);
        Iterator roleNameIt = roleNames.keySet().iterator();
        while(roleNameIt.hasNext()){
          JSONObject roleNameJSON = new JSONObject();
          String roleNameLanguage = (String) roleNameIt.next();
          roleNameJSON.put("language",roleNameLanguage);
          String roleNameString = (String) roleNames.get(roleNameLanguage);
          roleNameJSON.put("name",roleNameString);
          roleJSONNameArray.put(roleNameJSON);
        }
        roleJSONObject.put("roleNames",roleJSONNameArray);
        roleNameMapping.put(roleJSONObject);
      }

      result.put("associations",associations);
      result.put("roleNameMapping",roleNameMapping);
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage());
    }
    return result;
  }

  public JSONArray deleteTopicRelationsFromDomain(String domainIdentifier, String domainType, String payload) throws NdlaServiceException, BadObjectReferenceException{
    JSONArray deletedTopics = new JSONArray();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try{
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();

      TopicIF domainTopic = null;

      HashMap<String,String> reifierData = new HashMap<>();

      try {
        String domainUrl = "http://psi.topic.ndla.no/"+domainType+"/#" + domainIdentifier;
        domainTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(domainUrl));
      } catch (MalformedURLException urlEX) {
        throw new NdlaServiceException(urlEX.getMessage());
      }

      if(null != domainTopic) {


      }

      store.commit();
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage());
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return deletedTopics;
  }

  public JSONArray deleteTopicsFromDomain(String domainIdentifier, String domainType, String domainPsi, String topics) throws NdlaServiceException, BadObjectReferenceException{
    JSONArray deletedTopics = new JSONArray();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try{
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();

      TopicIF domainTopic = null;

      HashMap<String,String> reifierData = new HashMap<>();
      if(domainType.length() > 0){
        reifierData.put("http://psi.topic.ndla.no/#ontology_topic_domain_type",domainPsi);
      }

      try {
        String domainUrl = "http://psi.topic.ndla.no/"+domainType+"/#" + domainIdentifier;
        domainTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(domainUrl));
      } catch (MalformedURLException urlEX) {
        throw new NdlaServiceException(urlEX.getMessage());
      }

      if(null != domainTopic) {
        String relationType = "http://psi.topic.ndla.no/#domain_ontology_has_topic";

        JSONArray topicIds = new JSONArray(topics);
        for(int i = 0; i < topicIds.length(); i++) {
          String topicId = topicIds.getString(i);
          TopicIF topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#"+topicId));

          Utils.deleteAssocsByRole(domainTopic, topicToBeDeleted, relationType, topicMap);
          deletedTopics.put(topicId);
        }

      }

      store.commit();
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage());
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return deletedTopics;
  }


  public boolean domainHasTopic(String domainId, String domainType,String topicId, String associationId)throws NdlaServiceException, BadObjectReferenceException{
      boolean hasTopic = false;

      try{
        JSONObject domainTopics  = getDomainOntologyTopics(domainId,domainType);
        JSONArray relations = domainTopics.getJSONArray("relations");
        for(int i = 0 ; i < relations.length();i++){
          JSONObject relation = relations.getJSONObject(i);
          String assocId = relation.getString("associationId");
          if(assocId.equals(associationId)){
            JSONArray relatedTopics = relation.getJSONArray("relatedTopics");
            for(int j = 0 ; j < relatedTopics.length();j++){
              JSONObject relatedTopic = relatedTopics.getJSONObject(j);
              if(relatedTopic.getString("topicId").equals(topicId)){
                hasTopic = true;
                break;
              }
            }
          }
        }//end for domainTopics
      }
      catch (Exception e){
      if (e instanceof BadObjectReferenceException) {
        throw new BadObjectReferenceException(e.getMessage());
      } else {
        throw new NdlaServiceException(e.getMessage());
      }
    }



    return hasTopic;
  }

  public JSONObject getDomainOntologyTopics(String domainIdentifier, String domainType) throws NdlaServiceException, BadObjectReferenceException {
    JSONObject result = new JSONObject();
    JSONArray associations = new JSONArray();


    HashMap<String,ArrayList<HashMap<String,String>>> associationData = new HashMap<>();
    HashMap<String,String> rolePlayerMap = new HashMap<String,String>();

    TreeMap<String,HashMap<String,ArrayList<HashMap<String,String>>>> assocRoleMapper = new TreeMap<>();
    HashMap<String,ArrayList<HashMap<String,String>>> playerNameMap = new HashMap<>();
    HashMap<String,String> currentMap = new HashMap<>();
    HashMap<String,HashMap<String,String>> reifierMap = new HashMap<>();

    TopicIF newTopic = null;
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;



    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      LocatorIF baseAddress = store.getBaseAddress();
      String domainUrl = "http://psi.topic.ndla.no/"+domainType+"/#" + domainIdentifier;
      TopicIF domain = topicMap.getTopicBySubjectIdentifier(new URILocator(domainUrl));
      if(null != domain){
        Collection<AssociationIF> associationIFs = domain.getAssociations();
        for(AssociationIF associationIF : associationIFs){
          String currentAssociationTypePSI = "";
          String currentTypeTypePSI = "";

          TopicIF associationType = associationIF.getType();
          Collection<TopicIF> typeTypes = associationType.getTypes();
          String typeTypePSI = "";
          for(TopicIF typeType : typeTypes) {
            Collection<LocatorIF> typeTypePsis = typeType.getSubjectIdentifiers();
            if(typeTypePsis.size() == 1){
              typeTypePSI = typeTypePsis.iterator().next().getAddress();
            }
            else{
              int typeTypeCounter = 0;
              for(LocatorIF typeTypeLocator : typeTypePsis) {
                if(typeTypeCounter == 0) {
                  typeTypePSI = typeTypeLocator.getAddress();
                  break;
                }
              }
            }
          }

          Collection<LocatorIF> associationTypeLocators = associationType.getSubjectIdentifiers();
          String associationTypePSI = "";
          if(associationTypeLocators.size() == 1) {
            associationTypePSI = associationTypeLocators.iterator().next().getAddress();
          }
          else {
            int associationTypeCounter = 0;
            for(LocatorIF associationTypeLocator : associationTypeLocators) {
              if(associationTypeCounter == 0) {
                associationTypePSI = associationTypeLocator.getAddress();
                break;
              }
            }
          }//end if associationTypeLocators

          Collection<TopicNameIF> assocTypeNames = associationType.getTopicNames();
          ArrayList<HashMap<String,String>> currentNameList = new ArrayList<>();

          if (assocTypeNames.size() > 0) {
            for (TopicNameIF assocTypeName : assocTypeNames) {

              Collection<TopicIF> assocTypeScopes = assocTypeName.getScope();
              String combined = "";
              for (TopicIF assocTypeScope : assocTypeScopes) {

                Collection<LocatorIF> assocTypeScopeLocators = assocTypeScope.getSubjectIdentifiers();
                for (LocatorIF assocTypeScopeLocator : assocTypeScopeLocators) {

                  if (assocTypeScopeLocator.getAddress().contains("iso")) {
                    currentMap.put(assocTypeScopeLocator.getAddress()+"_"+assocTypeName.getObjectId(),assocTypeName.getValue());
                  }

                  if(!assocTypeScopeLocator.getAddress().contains("iso") && !assocTypeScopeLocator.getAddress().contains("language-neutral"))
                    rolePlayerMap.put(assocTypeName.getObjectId(),assocTypeScopeLocator.getAddress());
                }//end for locators

              } //end for scopes
              if(!associationData.containsValue(currentNameList)){
                currentNameList.add(currentMap);
                associationData.put(associationTypePSI,currentNameList);
              }


            }//end for names
          }//end if typenames


          currentAssociationTypePSI = associationTypePSI;
          currentTypeTypePSI = typeTypePSI;

          TopicIF assocReifier = associationIF.getReifier();
          String reifierPsi = assocReifier.getSubjectIdentifiers().iterator().next().getAddress();


          if(currentAssociationTypePSI.length() > 0) {
            Collection<AssociationRoleIF> roles = associationIF.getRoles();

            for (AssociationRoleIF role : roles) {
              String rolePlayerPSI = "";
              TopicIF roleType = role.getType();

              TopicIF rolePlayer = role.getPlayer();
              Collection<LocatorIF> rolePlayerLocators = rolePlayer.getSubjectIdentifiers();

              if(rolePlayerLocators.size() == 1) {
                if(rolePlayerLocators.iterator().next().getAddress().contains("topics")) {
                  rolePlayerPSI = rolePlayerLocators.iterator().next().getAddress();
                }
              }
              else {
                int rolePlayerCounter = 0;
                for(LocatorIF rolePlayerLocator : rolePlayerLocators) {
                  if(rolePlayerLocator.getAddress().contains("topics")) {
                    rolePlayerPSI = rolePlayerLocator.getAddress();
                    break;
                  }
                }
              }

              HashMap<String,String> tempReifierMap = new HashMap<>();
              tempReifierMap.put("reifierId",reifierPsi);
              Collection<OccurrenceIF> domainTypeOccCollection = assocReifier.getOccurrencesByType(topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#ontology_topic_domain_type")));

              OccurrenceIF domainTypeOcc = domainTypeOccCollection.iterator().next();
              String domainTypeId = domainTypeOcc.getValue();
              tempReifierMap.put("domainTypeId",domainTypeId);
              reifierMap.put(rolePlayerPSI,tempReifierMap);


              if(rolePlayerPSI.contains("topics")){
                Collection<TopicNameIF> rolePlayerNames = rolePlayer.getTopicNames();
                ArrayList<HashMap<String,String>> currentRoleNameList = new ArrayList<>();
                ArrayList<String> foundNameMaps = new ArrayList<>();
                if (rolePlayerNames.size() > 0) {
                  for (TopicNameIF rolePlayerName : rolePlayerNames) {
                    HashMap<String,String> currentNameMapper = new HashMap<>();
                    Collection<TopicIF> rolePlayerNameScopes = rolePlayerName.getScope();
                    for (TopicIF rolePlayerNameScope : rolePlayerNameScopes) {
                      Collection<LocatorIF> rolePlayerNameScopeLocators = rolePlayerNameScope.getSubjectIdentifiers();

                      for (LocatorIF rolePlayerNameScopeLocator : rolePlayerNameScopeLocators) {
                        if(rolePlayerNameScopeLocator.getAddress().length() > 0){
                          if(!foundNameMaps.contains(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue())){
                            currentNameMapper.put(rolePlayerNameScopeLocator.getAddress(),rolePlayerName.getValue());
                            foundNameMaps.add(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue());
                          }

                        }
                      }//end for locators
                    } //end for scopes

                    if(currentNameMapper.size() > 1){
                      Iterator cMapIt = currentNameMapper.keySet().iterator();
                      ArrayList<String> currentNameMaps = new ArrayList<>();
                      while(cMapIt.hasNext()){
                        HashMap<String,String> newCurrentNameMapper = new HashMap<>();
                        String cMapLang = (String) cMapIt.next();
                        if(!currentNameMaps.contains(cMapLang+"_"+currentNameMapper.get(cMapLang))){
                          newCurrentNameMapper.put(cMapLang,currentNameMapper.get(cMapLang));
                          currentRoleNameList.add(newCurrentNameMapper);
                          currentNameMaps.add(cMapLang+"_"+currentNameMapper.get(cMapLang));
                        }

                      }
                    }
                    else{
                      currentRoleNameList.add(currentNameMapper);
                    }

                  }//end for names
                  playerNameMap.put(rolePlayerPSI,currentRoleNameList);

                }//end if typenames
              }



            }//end for AssociationRoleIf
            assocRoleMapper.put(currentAssociationTypePSI,playerNameMap);
          }

        }//end assocs


        JSONArray associationJSONArray = new JSONArray();
        Iterator it = associationData.keySet().iterator();
        while(it.hasNext()){
          JSONObject assocJSONObject = new JSONObject();
          String assocId = (String) it.next();
          assocJSONObject.put("associationId",assocId);
          JSONArray assocNameJSONArray = new JSONArray();
          ArrayList<HashMap<String,String>> assocNameData = associationData.get(assocId);
          for(HashMap<String,String> nameMap : assocNameData){
            Iterator nit = nameMap.keySet().iterator();
            while(nit.hasNext()){
              JSONObject assocNameJSONObject = new JSONObject();
              String compoundNameId = (String) nit.next();
              String[] nameIdFragments = compoundNameId.split("_");
              String nameLangString = nameIdFragments[0];
              String rolePsiString = rolePlayerMap.get(nameIdFragments[1]);
              String nameString = nameMap.get(compoundNameId);
              assocNameJSONObject.put("language",nameLangString);
              assocNameJSONObject.put("role",rolePsiString);
              assocNameJSONObject.put("name",nameString);
              assocNameJSONArray.put(assocNameJSONObject);
            }//end while nameMap
          }//end for assocNameData
          assocJSONObject.put("associationTypeNames",assocNameJSONArray);
          JSONArray relatedTopicsJSONArray = new JSONArray();
          HashMap<String,ArrayList<HashMap<String,String>>> topicDataMap = assocRoleMapper.get(assocId);
          Iterator tnIt = topicDataMap.keySet().iterator();
          while(tnIt.hasNext()) {
            String topicDataId  = (String) tnIt.next();
            JSONObject topicJSONObject = new JSONObject();
            topicJSONObject.put("topicId",topicDataId);
            HashMap<String,String> reifierData = reifierMap.get(topicDataId);
            topicJSONObject.put("reifierId",reifierData.get("reifierId"));
            topicJSONObject.put("domainTypeId",reifierData.get("domainTypeId"));
            JSONArray topicNameJSONArray = new JSONArray();
            ArrayList<HashMap<String,String>> topicList = topicDataMap.get(topicDataId);
            for(HashMap<String,String> topicNameMapData : topicList) {
              Iterator tnMIt = topicNameMapData.keySet().iterator();
              JSONObject topicNameJSONObject = new JSONObject();

              while(tnMIt.hasNext()){
                String topicNameLang = (String) tnMIt.next();
                String topicNameString = topicNameMapData.get(topicNameLang);
                topicNameJSONObject.put(topicNameLang,topicNameString);
              }//enf while topicNAmeMap
              topicNameJSONArray.put(topicNameJSONObject);
            }//end for list of names
            topicJSONObject.put("topicNames",topicNameJSONArray);


            HashMap<String,String> existingProcessState = Utils.getProcessState(topicDataId,topicMap);
            if(existingProcessState.size() > 0){
              topicJSONObject.put("processState",existingProcessState.get("processState"));
            }
            else{
              topicJSONObject.put("processState","0");
            }

            HashMap<String,String> existingApprovedState = Utils.getApprovedState(topicDataId,topicMap);
            String existingApproved = existingApprovedState.get("approved");
            String existingApprovalDate = existingApprovedState.get("approval_date");

            if(null != existingApproved && existingApproved.length() > 0) {
              topicJSONObject.put("approved",existingApproved);
              if(null != existingApprovalDate) {
                topicJSONObject.put("approval_date",existingApprovalDate);
              }
            }
            else{
              topicJSONObject.put("approved","false");
            }

            relatedTopicsJSONArray.put(topicJSONObject);
          }

          assocJSONObject.put("relatedTopics",relatedTopicsJSONArray);
          associationJSONArray.put(assocJSONObject);
        }//end while assocs
        result.put("relations",associationJSONArray);
      }
      else{
        throw new BadObjectReferenceException("Topic with id "+domainIdentifier+" does not exist");
      }//end if topic exists

    }
    catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    }
    finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }

    return result;
  }


  public TopicIF updateSubject(String data)throws NdlaServiceException, BadObjectReferenceException{
    TopicIF subjectTopic = null;

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = store.getBaseAddress();


      JSONObject payload = new JSONObject(data);

      String subjectTopicId = payload.getString("subjectId");
      LocatorIF topicPsi = new URILocator("http://psi.topic.ndla.no/#subject_"+subjectTopicId);
      subjectTopic = topicMap.getTopicBySubjectIdentifier(topicPsi);

      if(null != subjectTopic) {
        Utils.deleteBasenames(topicPsi, topicMap);
        Utils.deleteOccurrences(topicPsi, topicMap);
        Utils.deletePsis(topicPsi,topicMap);

        JSONArray names = payload.getJSONArray("names");
        for (int i = 0; i < names.length(); i++) {
          JSONObject langName = names.getJSONObject(i);

          String langNameLang = langName.getString("language-id");
          String langNameString = langName.getString("name");
          // Names and scopes
          Utils.createBasename(subjectTopic, langNameString, langNameLang, topicMap);
        }
        JSONArray sites = payload.getJSONArray("originating-sites");
        for(int j = 0; j < sites.length(); j++) {
          String site = sites.getString(j);
          TopicIF siteOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#originating-site"));
          builder.makeOccurrence(subjectTopic, siteOcctype, new URILocator(site));
        }

        String udir_psi = payload.getString("udir-psi");
        TopicIF udirOcctype = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#udir-subject"));
        builder.makeOccurrence(subjectTopic, udirOcctype, new URILocator(udir_psi));

        subjectTopic.addSubjectIdentifier(topicPsi);

        JSONArray psis = payload.getJSONArray("psis");
        for(int k = 0; k < psis.length(); k++) {
          String psi = psis.getString(k);
          subjectTopic.addSubjectIdentifier(new URILocator(psi));
        }
      }
      store.commit();
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        if (e instanceof BadObjectReferenceException) {
          throw new BadObjectReferenceException(e.getMessage());
        } else {
          throw new NdlaServiceException(e.getMessage(),e);
        }
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return subjectTopic;
  }

  synchronized public ArrayList<TopicIF> deleteSubjectBySubjectId(String data) throws NdlaServiceException, BadObjectReferenceException {
    ArrayList<TopicIF> deletedTopics = new ArrayList<>();

    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

      try {
        JSONArray jData = new JSONArray(data);
        for(int i = 0; i< jData.length(); i++){
          String psi = jData.getString(i);
          TopicIF topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
          if (null != topicToBeDeleted) {
            topicToBeDeleted.remove();
            deletedTopics.add(topicToBeDeleted);
          }
        }
      } catch (MalformedURLException e) {
        throw new NdlaServiceException(e.getMessage(),e);
      }
      store.commit(); // Persist object graph.
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(),e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return deletedTopics;
  }


  public ArrayList<NdlaTopic> getSubjects(int limit, int offset, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
    String siteUrl = "";
    ArrayList<NdlaTopic> subjects = new ArrayList<>();
    switch (site) {
      case NDLA:
        siteUrl = NDLA_SITE_URL;
        break;
      case DELING:
        siteUrl = DELING_SITE_URL;
        break;
      case NYGIV:
        siteUrl = FYR_SITE_URL;
        break;
    }

      String query = String.format("select $COLUMN from direct-instance-of($COLUMN,subjectmatter) order by $COLUMN?");
      ArrayList<NdlaTopic> topics =  executeCollectionQuery(query);



      for(NdlaTopic topic : topics){
        ArrayList<String> originatingSites = topic.getOriginatingSites();
        if(originatingSites.contains(siteUrl)){
          subjects.add(topic);
        }
      }

    return subjects;
  }

    public ArrayList<NdlaTopic> getSubjectsByTopicId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase();
        String query = String.format("select $COLUMN from subject_ontology_has_topic($COLUMN : subjectmatter, %s : topic) order by $COLUMN limit %s offset %s?", identifier, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }

  public ArrayList<NdlaTopic> getSubjectsBySiteTopicId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
    String siteIdentifier = site.toString().toLowerCase();
    String query = String.format("select $COLUMN from subject-topic($COLUMN : subjectmatter, %s : %s-topic) order by $COLUMN limit %s offset %s?", identifier, siteIdentifier, Integer.toString(limit), Integer.toString(offset));

    return executeCollectionQuery(query);
  }

    public ArrayList<NdlaTopic> getSubjectsByKeywordId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase(); // TODO: Flagged for removal (BK).
        String query = String.format("select $COLUMN from subject-keyword($COLUMN : subjectmatter, %s : keyword) order by $COLUMN limit %s offset %s?", identifier, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }

    synchronized public NdlaTopic getSubjectBySubjectId(String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
      String siteUrl = "";
      NdlaTopic result = null;
      switch (site) {
          case NDLA:
              siteUrl = NDLA_SITE_URL;
              break;
          case DELING:
              siteUrl = DELING_SITE_URL;
              break;
          case NYGIV:
              siteUrl = FYR_SITE_URL;
              break;
      }

      String subject_id = "http://psi.topic.ndla.no/#"+identifier;
      String query = String.format("select $COLUMN from subject-identifier($COLUMN, \"%s\"),originating-site($COLUMN,\"%s\")?", subject_id, siteUrl);
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

        ParsedStatementIF statement = null;
        QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
        QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

        QueryResultIF tologResult = null;
        try {
          statement = queryProcessor.parse(query);
          if (null != statement) {
            tologResult = ((ParsedQueryIF) statement).execute();
            if (tologResult.next()) {
              result = new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), null);
            }
          }
        } catch (BadObjectReferenceException e) {
          throw new BadObjectReferenceException(e.getMessage());
        } catch (InvalidQueryException e) {
          throw new NdlaServiceException(e.getMessage(),e);
        } finally {
          if (null != tologResult) {
            tologResult.close();
          }
        }
      } catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          if (e instanceof BadObjectReferenceException) {
            throw new BadObjectReferenceException(e.getMessage());
          } else {
            throw new NdlaServiceException(e.getMessage(),e);
          }
        }
      } finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }
      return result;
    }

    // ***** MetadataSet *****

    public ArrayList<NdlaTopic> getMetadataSets(int limit, int offset, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase(); // TODO: Flagged for removal (BK).
        String query = String.format("select $COLUMN from instance-of($COLUMN, metadataset) order by $COLUMN limit %s offset %s?", Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }

    public ArrayList<NdlaTopic> getMetadataSetsByNodeId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase();
        String query = String.format("select $COLUMN from %snode-metadataset(%s : %s-node, $COLUMN : metadataset) order by $COLUMN limit %s offset %s?", siteIdentifier, identifier, siteIdentifier, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }

    public ArrayList<NdlaTopic> lookupMetadataSetBySearchTerm(String term, NdlaSite site) throws NdlaServiceException {
        throw new UnsupportedOperationException("Method not implemented");
    }

    public void saveMetadataSetBySetId() throws NdlaServiceException {
        throw new UnsupportedOperationException("Method not implemented");
    }

    // ***** MetadataTerm *****

    public ArrayList<NdlaTopic> getMetadataTermsByNodeId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase();
        String query = String.format("select $COLUMN from %snode-metadataterm(%s : %s-node, $COLUMN : metadataterm) order by $COLUMN limit %s offset %s?", siteIdentifier, identifier, siteIdentifier, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }

    public ArrayList<NdlaTopic> getMetadataTermsBySubjectMatterId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase(); // TODO: Flagged for removal (BK).
        String query = String.format("select $COLUMN from subjectmatter-metadataterm(%s : subjectmatter, $COLUMN : metadataterm) order by $COLUMN limit %s offset %s?", identifier, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }

    public void lookupMetadataTermBySearchTerm(String term, NdlaSite site) throws NdlaServiceException {
        throw new UnsupportedOperationException("Method not implemented");
    }

    public void saveMetadataTermsByNodeId(String identifier, NdlaSite site) throws NdlaServiceException {
        throw new UnsupportedOperationException("Method not implemented");
    }

    public void saveMetadataTermsBySubjectMatterId(String identifier, NdlaSite site) throws NdlaServiceException {
        throw new UnsupportedOperationException("Method not implemented");
    }

    public void deleteMetadataSetBySetId(String identifier, NdlaSite site) throws NdlaServiceException {
        throw new UnsupportedOperationException("Method not implemented");
    }

    public void saveMetadataTermByMetaDataTermId(String identifier, NdlaSite site) throws NdlaServiceException {
        throw new UnsupportedOperationException("Method not implemented");
    }

    public void deleteMetadataTermByMetaDataTermId(String identifier, NdlaSite site) throws NdlaServiceException {
        throw new UnsupportedOperationException("Method not implemented");
    }

    // ***** Competence aims *****

    /*
    *  @TODO: Implement test function when data is imported into topicmap
    */
    public ArrayList<NdlaTopic> lookUpAimsBySearchTerm() throws NdlaServiceException {
        throw new UnsupportedOperationException("Method not implemented");
    }

    synchronized public HashMap<String, TopicIF> createAimTopics(HashMap<String, HashMap<String, HashMap<String, String>>> data) throws NdlaServiceException {
        HashMap<String, TopicIF> topics = new HashMap<String, TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            TopicMapBuilderIF builder = topicMap.getBuilder();
            LocatorIF baseAddress = store.getBaseAddress();

            /*
            This typestring expects to be one of the specific topic types
            created for each site type until the transition to generic "topic" type is enforced
            @TODO: Create or refactor method for fetching of generic "topic" types when appropriate
            */
            String topicTypeString = "topic";

            /*
            checks for duplicates, creates new ones, if the keyword does not exist,
            creates only assoc to node for existing ones
            */

            HashMap<String, TopicIF> duplicateData = Utils.lookForExistingTopics(data, topicTypeString, topicMap);

            Iterator dataIterator = data.keySet().iterator();

            try {
                while (dataIterator.hasNext()) {
                    boolean topicExists = false; // TODO: Flagged for removal (BK).
                    String dataSetKey = (String) dataIterator.next();
                    HashMap<String, HashMap<String, String>> keySets = data.get(dataSetKey);
                    Iterator setIterator = keySets.keySet().iterator();
                    TopicIF currentTopic = null;

                    if (null == duplicateData.get(dataSetKey)) {
                        String id = "topic-" + Utils.makeRandomId(topicMap);
                        currentTopic = builder.makeTopic();
                        TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + topicTypeString));
                        currentTopic.addType(type);

                        currentTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + id));
                        currentTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + id));

                        while (setIterator.hasNext()) {
                            String setkey = (String) setIterator.next();
                            HashMap<String, String> topicSetMap = keySets.get(setkey);
                            TopicIF existingSiteTopic = null; // TODO: Flagged for removal (BK).
                            Iterator topicIterator = topicSetMap.keySet().iterator();

                            TopicIF newTopic = null; // TODO: Flagged for removal (BK).

                            switch (setkey) {
                                case "names":
                                    while (topicIterator.hasNext()) {
                                        String topicStringLang = (String) topicIterator.next();
                                        String topicString = topicSetMap.get(topicStringLang);

                                        if (!Utils.isEmpty(topicString) && !Utils.isEmpty(topicStringLang)) {
                                            // Names and scopes
                                            Utils.createBasename(currentTopic, topicString, topicStringLang, topicMap);
                                        }// endif not empty name and scope
                                    }//end while topicIterator
                                    break;
                                case "occurrences":
                                    while (topicIterator.hasNext()) {
                                        String occurrenceString = (String) topicIterator.next();
                                        String occurrenceType = "";
                                        String occurrenceLang = "";
                                        if (occurrenceString.contains("#")) {
                                            String[] occArray = occurrenceString.split("#");
                                            occurrenceLang = occArray[0];
                                            occurrenceType = occArray[1];
                                        } else {
                                            occurrenceType = occurrenceString;
                                            occurrenceLang = "language-neutral";
                                        }

                                        String occurrenceData = topicSetMap.get(occurrenceString);
                                        TopicIF occType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + occurrenceType));
                                        switch (occurrenceType) {
                                            case "description":
                                                OccurrenceIF descriptionOcc = builder.makeOccurrence(currentTopic, occType, occurrenceData);
                                                if (!Utils.isEmpty(occurrenceLang)) {
                                                    descriptionOcc.addTheme(Utils.getLanguageTopic(occurrenceLang, topicMap));
                                                }
                                                break;
                                            case "image":
                                                builder.makeOccurrence(currentTopic, occType, new URILocator(occurrenceData));
                                                break;
                                        }
                                    }//end while topicIterator
                                    break;
                            }//end switch setKey
                        }//end while setiterator
                        topics.put(dataSetKey, currentTopic);
                    }//end if topic does not exist
                }//end while dataIterator
            } catch (MalformedURLException | UniquenessViolationException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            }
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return topics;
    }



  public JSONObject getOntologyTopicsByNodeId(String nodeIdentifier, NdlaSite site)  throws NdlaServiceException, BadObjectReferenceException {
    JSONObject result = new JSONObject();
    JSONArray associations = new JSONArray();
    JSONObject filteredResult = new JSONObject();

    HashMap<String,ArrayList<HashMap<String,String>>> associationData = new HashMap<>();
    HashMap<String,String> rolePlayerMap = new HashMap<String,String>();
    HashMap<String,ArrayList<HashMap<String,String>>> playerNameMap = new HashMap<>();
    ArrayList<String> otherTopicRoles = new ArrayList<>();
    TreeMap<String,ArrayList<String>> otherTopicMap = new TreeMap<>();

    ArrayList<String> blackList = new ArrayList<>();
    blackList.add("http://psi.topic.ndla.no/#aim_has_topic");
    blackList.add("http://psi.topic.ndla.no/aim_has_topic");
    blackList.add("http://psi.udir.no/ontologi/aim_has_topic");

    String query = String.format("reifies($TOPIC, $COLUMN),\n" +
        "    role-player($ROLE1, ndlanode_800031),\n" +
        "    association-role($COLUMN, $ROLE1),\n" +
        "    association-role($COLUMN, $ROLE2),\n" +
        "    role-player($ROLE2, $TOPIC1), type($COLUMN,ndlanode-topic-generic)?");
    ArrayList<Object> assocs = executeObjectCollectionQuery(query);
    ArrayList<String> foundReifiers = new ArrayList<>();

    for( Object assoc : assocs) {
      boolean containsTopicId = false;

      String currentAssociationTypePSI = "";
      String currentTypeTypePSI = "";
      AssociationIF associationIF = (AssociationIF) assoc;
      TopicIF reifierTopic = associationIF.getReifier();

      Collection<LocatorIF> reifierPSIS = reifierTopic.getSubjectIdentifiers();
      String reifierPSIString = reifierPSIS.iterator().next().getAddress();


      TopicIF associationType = associationIF.getType();

      Collection<TopicIF> typeTypes = associationType.getTypes();
      String typeTypePSI = "";
      for(TopicIF typeType : typeTypes) {
        Collection<LocatorIF> typeTypePsis = typeType.getSubjectIdentifiers();
        if(typeTypePsis.size() == 1){
          typeTypePSI = typeTypePsis.iterator().next().getAddress();
        }
        else{
          int typeTypeCounter = 0;
          for(LocatorIF typeTypeLocator : typeTypePsis) {
            if(typeTypeCounter == 0) {
              typeTypePSI = typeTypeLocator.getAddress();
              break;
            }
          }
        }
      }



      Collection<LocatorIF> associationTypeLocators = associationType.getSubjectIdentifiers();
      String associationTypePSI = "";
      if(associationTypeLocators.size() == 1) {
        associationTypePSI = associationTypeLocators.iterator().next().getAddress();
      }
      else {
        int associationTypeCounter = 0;
        for(LocatorIF associationTypeLocator : associationTypeLocators) {
          if(associationTypeCounter == 0) {
            associationTypePSI = associationTypeLocator.getAddress();
            break;
          }
        }
      }//end if associationTypeLocators

      Collection<TopicNameIF> assocTypeNames = associationType.getTopicNames();
      ArrayList<HashMap<String,String>> currentNameList = new ArrayList<>();

      if (assocTypeNames.size() > 0) {
        for (TopicNameIF assocTypeName : assocTypeNames) {
          HashMap<String,String> currentMap = new HashMap<>();
          Collection<TopicIF> assocTypeScopes = assocTypeName.getScope();
          String combined = "";
          for (TopicIF assocTypeScope : assocTypeScopes) {

            Collection<LocatorIF> assocTypeScopeLocators = assocTypeScope.getSubjectIdentifiers();
            for (LocatorIF assocTypeScopeLocator : assocTypeScopeLocators) {

              if (assocTypeScopeLocator.getAddress().contains("iso")) {
                currentMap.put(assocTypeScopeLocator.getAddress()+"_"+assocTypeName.getObjectId(),assocTypeName.getValue());
              }

              if(!assocTypeScopeLocator.getAddress().contains("iso") && !assocTypeScopeLocator.getAddress().contains("language-neutral"))
                rolePlayerMap.put(assocTypeName.getObjectId(),assocTypeScopeLocator.getAddress());
            }//end for locators

          } //end for scopes

          currentNameList.add(currentMap);
        }//end for names
        if(!blackList.contains(associationTypePSI)){
          associationData.put(associationTypePSI,currentNameList);
          currentAssociationTypePSI = associationTypePSI;
          currentTypeTypePSI = typeTypePSI;
        }

      }//end if typenames

      if(currentAssociationTypePSI.length() > 0) {
        Collection<AssociationRoleIF> roles =  associationIF.getRoles();
        String rolePlayerPSI = "";
        String accumulate = "";
        for (AssociationRoleIF role : roles) {
          TopicIF roleType = role.getType();
          Collection<LocatorIF> roleTypeLocators = roleType.getSubjectIdentifiers();
          String rolePSI = "";

          if(roleTypeLocators.size() == 1) {
            rolePSI = roleTypeLocators.iterator().next().getAddress();
          }
          else {
            int roleTypeCounter = 0;
            for(LocatorIF roleTypeLocator : roleTypeLocators) {
              if(roleTypeCounter == 0) {
                rolePSI = roleTypeLocator.getAddress();
                break;
              }
            }
          }

          TopicIF rolePlayer = role.getPlayer();
          Collection<LocatorIF> rolePlayerLocators = rolePlayer.getSubjectIdentifiers();

          if(rolePlayerLocators.size() == 1) {
            rolePlayerPSI = rolePlayerLocators.iterator().next().getAddress();
          }
          else {
            int rolePlayerCounter = 0;
            for(LocatorIF rolePlayerLocator : rolePlayerLocators) {
              if(rolePlayerCounter == 0) {
                rolePlayerPSI = rolePlayerLocator.getAddress();
                break;
              }
            }
          }

          String roleId = rolePlayerPSI.substring(rolePlayerPSI.lastIndexOf("#")+1);
          if(roleId.equals(nodeIdentifier)){
            containsTopicId = true;
          }

          String reifierPSI = "";
          if(!foundReifiers.contains(reifierPSIString)){
            reifierPSI = reifierPSIString;
          }
          foundReifiers.add(reifierPSIString);


          accumulate += rolePlayerPSI+"_"+associationTypePSI+"_"+typeTypePSI+"_"+rolePSI+"_"+reifierPSI+"§";

          Collection<TopicNameIF> rolePlayerNames = rolePlayer.getTopicNames();
          ArrayList<HashMap<String,String>> currentRoleNameList = new ArrayList<>();

          if (rolePlayerNames.size() > 0) {
            for (TopicNameIF rolePlayerName : rolePlayerNames) {
              HashMap<String,String> currentNameMapper = new HashMap<>();
              Collection<TopicIF> rolePlayerNameScopes = rolePlayerName.getScope();
              for (TopicIF rolePlayerNameScope : rolePlayerNameScopes) {
                Collection<LocatorIF> rolePlayerNameScopeLocators = rolePlayerNameScope.getSubjectIdentifiers();

                for (LocatorIF rolePlayerNameScopeLocator : rolePlayerNameScopeLocators) {
                  if(rolePlayerNameScopeLocator.getAddress().length() > 0){
                    //currentNameMapper.put("language",rolePlayerNameScopeLocator.getAddress());
                    currentNameMapper.put(rolePlayerNameScopeLocator.getAddress(),rolePlayerName.getValue());
                  }
                }//end for locators
              } //end for scopes
              if(currentNameMapper.size() > 1){
                Iterator cMapIt = currentNameMapper.keySet().iterator();
                while(cMapIt.hasNext()){
                  HashMap<String,String> newCurrentNameMapper = new HashMap<>();
                  String cMapLang = (String) cMapIt.next();
                  newCurrentNameMapper.put(cMapLang,currentNameMapper.get(cMapLang));
                  currentRoleNameList.add(newCurrentNameMapper);
                }
              }
              else{
                currentRoleNameList.add(currentNameMapper);
              }

            }//end for names
            playerNameMap.put(rolePlayerPSI,currentRoleNameList);
          }//end if typenames

        }//end for roles
        if(containsTopicId) {
          otherTopicRoles.add(accumulate);
        }
      }//end if currentAssocType
    }//end for assocs

    Collections.sort(otherTopicRoles);
    Collections.reverse(otherTopicRoles);
    HashMap<String,String> reifierMap = new HashMap<>();

    for(String otherTopicRole : otherTopicRoles) {
      String[] roleArray = otherTopicRole.split("§");
      String role1 = roleArray[0];
      String role2 = roleArray[1];

      String[] httpArray1 = role1.split("_http");
      String[] httpArray2 = role2.split("_http");

      String topicId1 = httpArray1[0];
      String assocId1 = httpArray1[1];
      String assocType1 = httpArray1[2];
      String roleId1 = httpArray1[3];
      String assocReifier = "";
      if(httpArray1.length >= 5){
        assocReifier = httpArray1[4];
      }


      String topicId2 = httpArray2[0];
      String assocId2 = httpArray2[1];
      String assocType2 = httpArray2[2];
      String roleId2 = httpArray2[3];


      if(assocId1.equals(assocId2)) {
        //JSONObject assocObject = new JSONObject();
        String otherTopicId = "";
        String topicId = "";
        String assocId = assocId1;
        String assocType = assocType1;
        String thisRoleId = "";
        String thatRoleId = "";

        if(topicId1.contains("kl06")){
          otherTopicId = topicId1;
          topicId = topicId2;
          thisRoleId = roleId1;
          thatRoleId = roleId2;
        }
        else if(topicId2.contains("kl06")){
          otherTopicId = topicId2;
          topicId = topicId1;
          thisRoleId = roleId2;
          thatRoleId = roleId1;
        }
        else if(topicId2.contains("#topic-")){
          topicId = topicId2;
          otherTopicId = topicId1;
          thisRoleId = roleId2;
          thatRoleId = roleId1;
        }
        else if(topicId1.contains("#topic-")){
          otherTopicId = topicId2;
          topicId = topicId1;
          thisRoleId = roleId1;
          thatRoleId = roleId2;
        }

        if(!otherTopicMap.containsKey(otherTopicId)){
          otherTopicMap.put(otherTopicId,new ArrayList<String>());
        }
        otherTopicMap.get(otherTopicId).add("http"+thisRoleId+"_"+"http"+assocId+"_"+topicId+"_"+"http"+assocType+"_"+topicId+"_"+"http"+thatRoleId);
        reifierMap.put(topicId, "http"+assocReifier);
      }//end if associd matches
    }//end for

    try{
      Iterator<String> it = otherTopicMap.keySet().iterator();
      while(it.hasNext()) {
        JSONObject otherTopicJSON = new JSONObject();
        String otherTopicId = (String) it.next();
        otherTopicJSON.put("topicId",otherTopicId);
        JSONArray otherTopicNames = new JSONArray();
        ArrayList<HashMap<String,String>> otherTopicNameMapping = playerNameMap.get(otherTopicId);

        for(HashMap<String,String> otherTopicLangNames : otherTopicNameMapping) {
          Iterator<String> otherTopicLangNamesIterator = otherTopicLangNames.keySet().iterator();
          JSONObject otherTopicNameObject = new JSONObject();
          while (otherTopicLangNamesIterator.hasNext()){
            String otherTopicNameLanguage = (String) otherTopicLangNamesIterator.next();
            otherTopicNameObject.put(otherTopicNameLanguage,otherTopicLangNames.get(otherTopicNameLanguage));
          }

          if(otherTopicNameObject.length() > 0) {
            otherTopicNames.put(otherTopicNameObject);
          }
        }

        otherTopicJSON.put("topicNames", otherTopicNames);

        ArrayList<String> data = otherTopicMap.get(otherTopicId);
        JSONArray assocJSON = new JSONArray();
        for(String assocData : data) {
          String[] assocArray = assocData.split("_http");

          JSONObject assocObject = new JSONObject();
          String thisRoleId = assocArray[0].replaceAll("_","");;

          String assocId = "http"+assocArray[1];
          String assocType = assocArray[3];



          assocObject.put("associationId", assocId);
          assocObject.put("associationType",assocType);
          JSONArray assocTypeNames = new JSONArray();

          ArrayList<HashMap<String,String>> typeNameMap = associationData.get(assocId);
          for(HashMap<String,String> typeLangNames : typeNameMap){
            Iterator<String> typeLangNamesIterator = typeLangNames.keySet().iterator();
            JSONObject typeNameObject = new JSONObject();
            while (typeLangNamesIterator.hasNext()){
              String typeNameLanguage = (String) typeLangNamesIterator.next();
              String[] typeNameLanguageArray = typeNameLanguage.split("_");
              typeNameObject.put("language",typeNameLanguageArray[0]);
              typeNameObject.put("role",rolePlayerMap.get(typeNameLanguageArray[1]));
              typeNameObject.put("name",typeLangNames.get(typeNameLanguage));
            }
            assocTypeNames.put(typeNameObject);
          }
          assocObject.put("associationTypeNames",assocTypeNames);

          assocObject.put("rolePlayedByOtherTopic", thisRoleId);
          String topicId = "http"+assocArray[4];
          assocObject.put("topicId",topicId);

          JSONArray topicNames = new JSONArray();
          ArrayList<HashMap<String,String>> topicNameMapping = playerNameMap.get(topicId);
          ArrayList<String> foundTopicNames = new ArrayList<>();

          for(HashMap<String,String> topicLangNames : topicNameMapping) {
            Iterator<String> topicLangNamesIterator = topicLangNames.keySet().iterator();
            JSONObject topicNameObject = new JSONObject();
            while (topicLangNamesIterator.hasNext()){
              String topicNameLanguage = (String) topicLangNamesIterator.next();
              if(!foundTopicNames.contains(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage))) {
                topicNameObject.put(topicNameLanguage,topicLangNames.get(topicNameLanguage));
                foundTopicNames.add(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage));
              }
            }

            if(topicNameObject.length() > 0) {
              topicNames.put(topicNameObject);
            }
          }

          assocObject.put("topicNames", topicNames);


          String thatRoleId = "http"+assocArray[5].replaceAll("_","");
          assocObject.put("rolePlayedByTopic", thatRoleId);


          assocJSON.put(assocObject);
        }
        otherTopicJSON.put("relations",assocJSON);
        associations.put(otherTopicJSON);
      }//end while

      //filter out duplicates

      result.put("associations",associations);

      HashMap<String,JSONArray> jsonMap = new HashMap<>();
      ArrayList<String> foundIds = new ArrayList<>();
      JSONArray filteredAssociations = new JSONArray();

      JSONArray allAssociations = result.getJSONArray("associations");
      for(int i = 0; i < allAssociations.length(); i++) {
        JSONObject currentAssociation = allAssociations.getJSONObject(i);

        JSONArray relations = currentAssociation.getJSONArray("relations");

        for(int j = 0; j < relations.length(); j++) {
          boolean foundIt = true;
          JSONObject relation = (JSONObject) relations.get(j);
          String topicId = relation.getString("topicId");

          if(!foundIds.contains(topicId)){
            JSONObject newRelation = new JSONObject();
            newRelation.put("rolePlayedByOtherTopic", relation.getString("rolePlayedByOtherTopic"));
            newRelation.put("topicId", topicId);


            HashMap<String,String> topicAttributes = Utils.getTopicAttributes(_configFile, _topicmapReferenceKey,topicId);
            if(topicAttributes.containsKey("visibility")) {
              newRelation.put("visibility", topicAttributes.get("visibility"));
            }

            if(topicAttributes.containsKey("approved")) {
              newRelation.put("approved", topicAttributes.get("approved"));
            }

            if(topicAttributes.containsKey("approval_date")){
              newRelation.put("approval_date",topicAttributes.get("approval_date"));
            }

            if(topicAttributes.containsKey("processState")) {
              newRelation.put("processState", topicAttributes.get("processState"));
            }

            newRelation.put("topicNames", relation.getJSONArray("topicNames"));

            HashMap<String,HashMap<String,String>> typeMap = Utils.getTopicTypeAttributes(_configFile, _topicmapReferenceKey,topicId);
            Iterator<String> typeIt = typeMap.keySet().iterator();
            JSONArray types = new JSONArray();
            while(typeIt.hasNext()){
              String typeId = (String) typeIt.next();
              JSONObject type = new JSONObject();
              type.put("typeId",typeId);
              JSONArray typeNames = new JSONArray();
              HashMap<String,String> typeScopedNames = typeMap.get(typeId);
              Iterator nit = typeScopedNames.keySet().iterator();
              while(nit.hasNext()){
                JSONObject nameObject = new JSONObject();
                String langId = (String) nit.next();
                String nameString = typeScopedNames.get(langId);
                nameObject.put(langId,nameString);
                typeNames.put(nameObject);
              }//end while typeNames
              type.put("names",typeNames);
              types.put(type);
            }//end while
            newRelation.put("types", types);
            newRelation.put("rolePlayedByTopic", relation.getString("rolePlayedByTopic"));
            String reifierId = reifierMap.get(topicId);

            newRelation.put("reifierId", reifierId);
            HashMap<String,String> reifierOccurrences = Utils.getReifierOccurrences(_configFile, _topicmapReferenceKey, reifierId);
            JSONArray reifierOccurrenceJSONArray = new JSONArray();
            Iterator<String> reit = reifierOccurrences.keySet().iterator();
            while(reit.hasNext()){
              String key = (String) reit.next();
              String value = reifierOccurrences.get(key);
              JSONObject reifierOccJSON = new JSONObject();
              reifierOccJSON.put(key,value);
              reifierOccurrenceJSONArray.put(reifierOccJSON);
            }
            newRelation.put("reifierOccurrences", reifierOccurrenceJSONArray);
            filteredAssociations.put(newRelation);

          }
          foundIds.add(topicId);

          jsonMap.put(relation.getString("associationId"), filteredAssociations);
        }

      }
      filteredResult.put("associations", filteredAssociations);

    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage());
    }

    return filteredResult;
  }


  public JSONObject getOntologyTopicsByTopicId(String topicIdentifier, NdlaSite site)  throws NdlaServiceException, BadObjectReferenceException {
    JSONObject result = new JSONObject();
    JSONArray associations = new JSONArray();

    HashMap<String,ArrayList<HashMap<String,String>>> associationData = new HashMap<>();
    HashMap<String,String> rolePlayerMap = new HashMap<String,String>();
    HashMap<String,ArrayList<HashMap<String,String>>> playerNameMap = new HashMap<>();
    ArrayList<String> otherTopicRoles = new ArrayList<>();
    TreeMap<String,ArrayList<String>> otherTopicMap = new TreeMap<>();

    ArrayList<String> blackList = new ArrayList<>();
    blackList.add("http://psi.topic.ndla.no/#ndlanode-topic-generic");
    blackList.add("http://psi.topic.ndla.no/#aim_has_topic");
    blackList.add("http://psi.topic.ndla.no/aim_has_topic");
    blackList.add("http://psi.udir.no/ontologi/aim_has_topic");

    String query = String.format("association($COLUMN),reifies($TOPIC, $COLUMN)?");
    ArrayList<Object> assocs = executeObjectCollectionQuery(query);
    for( Object assoc : assocs) {
      boolean containsTopicId = false;

      String currentAssociationTypePSI = "";
      String currentTypeTypePSI = "";
      AssociationIF associationIF = (AssociationIF) assoc;

      TopicIF associationType = associationIF.getType();

      Collection<TopicIF> typeTypes = associationType.getTypes();
      String typeTypePSI = "";
      for(TopicIF typeType : typeTypes) {
        Collection<LocatorIF> typeTypePsis = typeType.getSubjectIdentifiers();
        if(typeTypePsis.size() == 1){
          typeTypePSI = typeTypePsis.iterator().next().getAddress();
        }
        else{
          int typeTypeCounter = 0;
          for(LocatorIF typeTypeLocator : typeTypePsis) {
            if(typeTypeCounter == 0) {
              typeTypePSI = typeTypeLocator.getAddress();
              break;
            }
          }
        }
      }



      Collection<LocatorIF> associationTypeLocators = associationType.getSubjectIdentifiers();
      String associationTypePSI = "";
      if(associationTypeLocators.size() == 1) {
        associationTypePSI = associationTypeLocators.iterator().next().getAddress();
      }
      else {
        int associationTypeCounter = 0;
        for(LocatorIF associationTypeLocator : associationTypeLocators) {
          if(associationTypeCounter == 0) {
            associationTypePSI = associationTypeLocator.getAddress();
            break;
          }
        }
      }//end if associationTypeLocators

      Collection<TopicNameIF> assocTypeNames = associationType.getTopicNames();
      ArrayList<HashMap<String,String>> currentNameList = new ArrayList<>();

      if (assocTypeNames.size() > 0) {
        for (TopicNameIF assocTypeName : assocTypeNames) {
          HashMap<String,String> currentMap = new HashMap<>();
          Collection<TopicIF> assocTypeScopes = assocTypeName.getScope();
          String combined = "";
          for (TopicIF assocTypeScope : assocTypeScopes) {

            Collection<LocatorIF> assocTypeScopeLocators = assocTypeScope.getSubjectIdentifiers();
            for (LocatorIF assocTypeScopeLocator : assocTypeScopeLocators) {

              if (assocTypeScopeLocator.getAddress().contains("iso")) {
                currentMap.put(assocTypeScopeLocator.getAddress()+"_"+assocTypeName.getObjectId(),assocTypeName.getValue());
              }

              if(!assocTypeScopeLocator.getAddress().contains("iso") && !assocTypeScopeLocator.getAddress().contains("language-neutral"))
                rolePlayerMap.put(assocTypeName.getObjectId(),assocTypeScopeLocator.getAddress());
            }//end for locators

          } //end for scopes

          currentNameList.add(currentMap);
        }//end for names
        if(!blackList.contains(associationTypePSI)){
          associationData.put(associationTypePSI,currentNameList);
          currentAssociationTypePSI = associationTypePSI;
          currentTypeTypePSI = typeTypePSI;
        }

      }//end if typenames

      if(currentAssociationTypePSI.length() > 0) {
        Collection<AssociationRoleIF> roles =  associationIF.getRoles();
        String rolePlayerPSI = "";
        String accumulate = "";
        for (AssociationRoleIF role : roles) {
          TopicIF roleType = role.getType();
          Collection<LocatorIF> roleTypeLocators = roleType.getSubjectIdentifiers();
          String rolePSI = "";

          if(roleTypeLocators.size() == 1) {
            rolePSI = roleTypeLocators.iterator().next().getAddress();
          }
          else {
            int roleTypeCounter = 0;
            for(LocatorIF roleTypeLocator : roleTypeLocators) {
              if(roleTypeCounter == 0) {
                rolePSI = roleTypeLocator.getAddress();
                break;
              }
            }
          }

          TopicIF rolePlayer = role.getPlayer();
          Collection<LocatorIF> rolePlayerLocators = rolePlayer.getSubjectIdentifiers();

          if(rolePlayerLocators.size() == 1) {
            rolePlayerPSI = rolePlayerLocators.iterator().next().getAddress();
          }
          else {
            int rolePlayerCounter = 0;
            for(LocatorIF rolePlayerLocator : rolePlayerLocators) {
              if(rolePlayerCounter == 0) {
                rolePlayerPSI = rolePlayerLocator.getAddress();
                break;
              }
            }
          }

          String roleId = rolePlayerPSI.substring(rolePlayerPSI.lastIndexOf("#")+1);

          if(roleId.equals(topicIdentifier)){
            containsTopicId = true;
          }


          accumulate += rolePlayerPSI+"_"+associationTypePSI+"_"+typeTypePSI+"_"+rolePSI+"§";

            Collection<TopicNameIF> rolePlayerNames = rolePlayer.getTopicNames();
            ArrayList<HashMap<String,String>> currentRoleNameList = new ArrayList<>();

            if (rolePlayerNames.size() > 0) {
              for (TopicNameIF rolePlayerName : rolePlayerNames) {
                HashMap<String,String> currentNameMapper = new HashMap<>();
                Collection<TopicIF> rolePlayerNameScopes = rolePlayerName.getScope();
                for (TopicIF rolePlayerNameScope : rolePlayerNameScopes) {
                  Collection<LocatorIF> rolePlayerNameScopeLocators = rolePlayerNameScope.getSubjectIdentifiers();

                  for (LocatorIF rolePlayerNameScopeLocator : rolePlayerNameScopeLocators) {
                    if(rolePlayerNameScopeLocator.getAddress().length() > 0){
                      //currentNameMapper.put("language",rolePlayerNameScopeLocator.getAddress());
                      currentNameMapper.put(rolePlayerNameScopeLocator.getAddress(),rolePlayerName.getValue());
                    }
                  }//end for locators
                } //end for scopes
                if(currentNameMapper.size() > 1){
                  Iterator cMapIt = currentNameMapper.keySet().iterator();
                  while(cMapIt.hasNext()){
                    HashMap<String,String> newCurrentNameMapper = new HashMap<>();
                    String cMapLang = (String) cMapIt.next();
                    newCurrentNameMapper.put(cMapLang,currentNameMapper.get(cMapLang));
                    currentRoleNameList.add(newCurrentNameMapper);
                  }
                }
                else{
                  currentRoleNameList.add(currentNameMapper);
                }

              }//end for names
              playerNameMap.put(rolePlayerPSI,currentRoleNameList);
            }//end if typenames

        }//end for roles
        if(containsTopicId) {
          otherTopicRoles.add(accumulate);
        }
      }//end if currentAssocType
    }//end for assocs

    Collections.sort(otherTopicRoles);
    Collections.reverse(otherTopicRoles);

    for(String otherTopicRole : otherTopicRoles) {
      String[] roleArray = otherTopicRole.split("§");
      String role1 = roleArray[0];
      String role2 = roleArray[1];

      String[] httpArray1 = role1.split("_http");
      String[] httpArray2 = role2.split("_http");

      String topicId1 = httpArray1[0];
      String assocId1 = httpArray1[1];
      String assocType1 = httpArray1[2];
      String roleId1 = httpArray1[3];

      String topicId2 = httpArray2[0];
      String assocId2 = httpArray2[1];
      String assocType2 = httpArray2[2];
      String roleId2 = httpArray2[3];

      if(assocId1.equals(assocId2)) {
        //JSONObject assocObject = new JSONObject();
        String otherTopicId = "";
        String topicId = "";
        String assocId = assocId1;
        String assocType = assocType1;
        String thisRoleId = "";
        String thatRoleId = "";

        if(topicId1.contains("kl06")){
          otherTopicId = topicId1;
          topicId = topicId2;
          thisRoleId = roleId1;
          thatRoleId = roleId2;
        }
        else if(topicId2.contains("kl06")){
          otherTopicId = topicId2;
          topicId = topicId1;
          thisRoleId = roleId2;
          thatRoleId = roleId1;
        }
        else if(topicId2.contains("#topic-")){
          topicId = topicId2;
          otherTopicId = topicId1;
          thisRoleId = roleId2;
          thatRoleId = roleId1;
        }
        else if(topicId1.contains("#topic-")){
          otherTopicId = topicId2;
          topicId = topicId1;
          thisRoleId = roleId1;
          thatRoleId = roleId2;
        }

        if(!otherTopicMap.containsKey(otherTopicId)){
          otherTopicMap.put(otherTopicId,new ArrayList<String>());
        }
        otherTopicMap.get(otherTopicId).add("http"+thisRoleId+"_"+"http"+assocId+"_"+topicId+"_"+"http"+assocType+"_"+topicId+"_"+"http"+thatRoleId);

      }//end if associd matches
    }//end for

    try{
      Iterator<String> it = otherTopicMap.keySet().iterator();
      while(it.hasNext()) {
        JSONObject otherTopicJSON = new JSONObject();
        String otherTopicId = (String) it.next();
        otherTopicJSON.put("topicId",otherTopicId);
        JSONArray otherTopicNames = new JSONArray();
        ArrayList<HashMap<String,String>> otherTopicNameMapping = playerNameMap.get(otherTopicId);

        for(HashMap<String,String> otherTopicLangNames : otherTopicNameMapping) {
          Iterator<String> otherTopicLangNamesIterator = otherTopicLangNames.keySet().iterator();
          JSONObject otherTopicNameObject = new JSONObject();
          while (otherTopicLangNamesIterator.hasNext()){
            String otherTopicNameLanguage = (String) otherTopicLangNamesIterator.next();
            otherTopicNameObject.put(otherTopicNameLanguage,otherTopicLangNames.get(otherTopicNameLanguage));
          }

          if(otherTopicNameObject.length() > 0) {
            otherTopicNames.put(otherTopicNameObject);
          }
        }

        otherTopicJSON.put("topicNames",otherTopicNames);


        ArrayList<String> data = otherTopicMap.get(otherTopicId);
        JSONArray assocJSON = new JSONArray();
        for(String assocData : data) {
          String[] assocArray = assocData.split("_http");
          JSONObject assocObject = new JSONObject();
          String thisRoleId = assocArray[0];

          String assocId = "http"+assocArray[1];
          String assocType = assocArray[3];
          assocObject.put("associationId",assocId);
          assocObject.put("associationType",assocType);
          JSONArray assocTypeNames = new JSONArray();

          ArrayList<HashMap<String,String>> typeNameMap = associationData.get(assocId);
          for(HashMap<String,String> typeLangNames : typeNameMap){
            Iterator<String> typeLangNamesIterator = typeLangNames.keySet().iterator();
            JSONObject typeNameObject = new JSONObject();
            while (typeLangNamesIterator.hasNext()){
              String typeNameLanguage = (String) typeLangNamesIterator.next();
              String[] typeNameLanguageArray = typeNameLanguage.split("_");
              typeNameObject.put("language",typeNameLanguageArray[0]);
              typeNameObject.put("role",rolePlayerMap.get(typeNameLanguageArray[1]));
              typeNameObject.put("name",typeLangNames.get(typeNameLanguage));
            }
            assocTypeNames.put(typeNameObject);
          }
          assocObject.put("associationTypeNames",assocTypeNames);

          assocObject.put("rolePlayedByOtherTopic",thisRoleId);
          String topicId = "http"+assocArray[4];
          assocObject.put("topicId",topicId);


          JSONArray topicNames = new JSONArray();
          ArrayList<HashMap<String,String>> topicNameMapping = playerNameMap.get(topicId);
          ArrayList<String> foundTopicNames = new ArrayList<>();

          for(HashMap<String,String> topicLangNames : topicNameMapping) {
            Iterator<String> topicLangNamesIterator = topicLangNames.keySet().iterator();
            JSONObject topicNameObject = new JSONObject();
            while (topicLangNamesIterator.hasNext()){
              String topicNameLanguage = (String) topicLangNamesIterator.next();
              if(!foundTopicNames.contains(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage))) {
                topicNameObject.put(topicNameLanguage,topicLangNames.get(topicNameLanguage));
                foundTopicNames.add(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage));
              }
            }

            if(topicNameObject.length() > 0) {
              topicNames.put(topicNameObject);
            }
          }

          assocObject.put("topicNames",topicNames);


          String thatRoleId = "http"+assocArray[5];
          assocObject.put("rolePlayedByTopic",thatRoleId);
          assocJSON.put(assocObject);
        }
        otherTopicJSON.put("relations",assocJSON);
        associations.put(otherTopicJSON);
      }


      result.put("associations",associations);
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage());
    }


    JSONObject filteredResult = new JSONObject();
    try{

      JSONArray filteredAssociations = new JSONArray();


      JSONArray allAssociations = result.getJSONArray("associations");
      for(int i = 0; i < allAssociations.length(); i++) {
        JSONObject currentAssociation = allAssociations.getJSONObject(i);
        String topicId1 = currentAssociation.getString("topicId");
        topicId1 = topicId1.substring(topicId1.lastIndexOf("#")+1);
        JSONArray relatedHits = Utils.hasTopicId(currentAssociation.getJSONArray("relations"),topicIdentifier);

        if(topicId1.equals(topicIdentifier)) {
          filteredAssociations.put(currentAssociation);
        }
        else if(relatedHits.length() > 0){
          for(int j = 0; j < relatedHits.length(); j++) {
            JSONObject relatedHit = relatedHits.getJSONObject(j);
            JSONObject revertedAssocObject = new JSONObject();

            revertedAssocObject.put("topicId",topicId1);
            revertedAssocObject.put("topicNames",currentAssociation.getJSONArray("topicNames"));

            JSONArray revertedRelatedArray = new JSONArray();
            JSONObject revertedRelationObject = new JSONObject();
            revertedRelationObject.put("associationType",relatedHit.getString("associationType"));
            revertedRelationObject.put("associationTypeNames",relatedHit.getString("associationTypeNames"));
            revertedRelationObject.put("rolePlayedByOtherTopic",relatedHit.getString("rolePlayedByTopic"));
            revertedRelationObject.put("rolePlayedByTopic",relatedHit.getString("rolePlayedByOtherTopic"));
            revertedRelatedArray.put(revertedRelationObject);
            revertedAssocObject.put("related",revertedRelatedArray);

            filteredAssociations.put(revertedAssocObject);
          }
        }

      }

      filteredResult.put("associations",filteredAssociations);
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage());
    }

    return filteredResult;
  }

  public JSONObject getSubjectOntologyTopicsByTopicId(String subjectMatterIdentifier,String topicId, NdlaSite site)  throws NdlaServiceException, BadObjectReferenceException {
    JSONObject result = new JSONObject();
    JSONObject topics = getOntologyTopics(subjectMatterIdentifier,site);


    JSONArray resultRelations = new JSONArray();
    TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
    TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
    TopicMapStoreIF store = null;
    try{
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      JSONArray relations = topics.getJSONArray("relations");
      for(int i = 0; i < relations.length(); i++){
        JSONObject relation = relations.getJSONObject(i);
        JSONArray relatedTopics = relation.getJSONArray("relatedTopics");
        JSONArray resultRelatedTopics = new JSONArray();

        JSONObject resultRelation = new JSONObject();
        resultRelation.put("associationId",relation.getString("associationId"));
        resultRelation.put("associationTypeNames",relation.getJSONArray("associationTypeNames"));
        for(int j = 0; j < relatedTopics.length(); j++) {
          JSONObject relatedTopic = (JSONObject) relatedTopics.get(j);
          String relatedTopicId = relatedTopic.getString("topicId");
          JSONArray relatedTopicNames = relatedTopic.getJSONArray("topicNames");
          boolean hasName = false;
          for(int k = 0; k < relatedTopicNames.length(); k++){
            JSONObject relatedTopicName = relatedTopicNames.getJSONObject(k);

          }

            TopicIF relatedTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(relatedTopicId));
            TopicIF approvedOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
            TopicIF approvedDateOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));
            Collection<OccurrenceIF> approvedOccurrenceIFs = relatedTopicIF.getOccurrencesByType(approvedOccurrenceType);
            String approvedString = "false";
            for(OccurrenceIF approvedOccurrenceIF : approvedOccurrenceIFs){
              if(approvedOccurrenceIF.getValue().equals("true")) {
                approvedString = "true";
                break;
              }
            }
            relatedTopic.put("approved",approvedString);
            if(approvedString.equals("true")){
              String approvalDate = "";
              Collection<OccurrenceIF> approvedDateOccurrencesIFs = relatedTopicIF.getOccurrencesByType(approvedDateOccurrenceType);
              for(OccurrenceIF approvedDateOccurrencesIF : approvedDateOccurrencesIFs){
                if(approvedDateOccurrencesIF.getValue() != "") {
                  approvalDate = approvedDateOccurrencesIF.getValue();
                  break;
                }
              }

              if(approvalDate != ""){
                relatedTopic.put("approval_date",approvalDate);
              }
            }
            HashMap<String,String> existingProcessState = Utils.getProcessState(relatedTopicId,topicMap);
            if(existingProcessState.size() > 0){
              relatedTopic.put("processState",existingProcessState.get("processState"));
            }
            else{
              relatedTopic.put("processState","0");
            }

            resultRelatedTopics.put(relatedTopic);

        }
        resultRelation.put("relatedTopics",resultRelatedTopics);
        resultRelations.put(resultRelation);


      }
      result.put("relations",resultRelations);
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage());
      }
    }
    finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }

    return result;
  }



    public JSONObject getTopicsBySubjectMatterAndNameString(String search, String subjectMatterIdentifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
      JSONObject result = new JSONObject();
      JSONObject topics = getOntologyTopics(subjectMatterIdentifier,site);


      JSONArray resultRelations = new JSONArray();
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      try{
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap();
        JSONArray relations = topics.getJSONArray("relations");
        for(int i = 0; i < relations.length(); i++){
          JSONObject relation = relations.getJSONObject(i);
          JSONArray relatedTopics = relation.getJSONArray("relatedTopics");
          JSONArray resultRelatedTopics = new JSONArray();

          JSONObject resultRelation = new JSONObject();
          resultRelation.put("associationId",relation.getString("associationId"));
          resultRelation.put("associationTypeNames",relation.getJSONArray("associationTypeNames"));
          for(int j = 0; j < relatedTopics.length(); j++) {
            JSONObject relatedTopic = (JSONObject) relatedTopics.get(j);
            String relatedTopicId = relatedTopic.getString("topicId");
            JSONArray relatedTopicNames = relatedTopic.getJSONArray("topicNames");
            boolean hasName = false;
            for(int k = 0; k < relatedTopicNames.length(); k++){
              JSONObject relatedTopicName = relatedTopicNames.getJSONObject(k);
              Iterator<String> keys = relatedTopicName.keys();
              while(keys.hasNext()){
                String key = (String)keys.next();
                String nameString = (String) relatedTopicName.get(key);
                if(nameString.trim().toLowerCase().contains(search.trim().toLowerCase())){
                  hasName = true;
                  break;
                }
              }
            }
            if(hasName) {
              TopicIF relatedTopicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(relatedTopicId));
              TopicIF approvedOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
              TopicIF approvedDateOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));
              Collection<OccurrenceIF> approvedOccurrenceIFs = relatedTopicIF.getOccurrencesByType(approvedOccurrenceType);
              String approvedString = "false";
              for(OccurrenceIF approvedOccurrenceIF : approvedOccurrenceIFs){
                if(approvedOccurrenceIF.getValue().equals("true")) {
                  approvedString = "true";
                  break;
                }
              }
              relatedTopic.put("approved",approvedString);
              if(approvedString.equals("true")){
                String approvalDate = "";
                Collection<OccurrenceIF> approvedDateOccurrencesIFs = relatedTopicIF.getOccurrencesByType(approvedDateOccurrenceType);
                for(OccurrenceIF approvedDateOccurrencesIF : approvedDateOccurrencesIFs){
                  if(approvedDateOccurrencesIF.getValue() != "") {
                    approvalDate = approvedDateOccurrencesIF.getValue();
                    break;
                  }
                }

                if(approvalDate != ""){
                  relatedTopic.put("approval_date",approvalDate);
                }
              }

              HashMap<String,String> existingProcessState = Utils.getProcessState(relatedTopicId,topicMap);
              if(existingProcessState.size() > 0){
                relatedTopic.put("processState",existingProcessState.get("processState"));
              }
              else{
                relatedTopic.put("processState","0");
              }

              resultRelatedTopics.put(relatedTopic);
            }
          }
          resultRelation.put("relatedTopics",resultRelatedTopics);
          resultRelations.put(resultRelation);


        }
        result.put("relations", resultRelations);
      } catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          throw new NdlaServiceException(e.getMessage());
        }
      }
      finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }

      return result;
    }

    public JSONObject getOntologyTopics(String subjectMatterIdentifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
      JSONObject result = new JSONObject();
      JSONArray associations = new JSONArray();


      HashMap<String,ArrayList<HashMap<String,String>>> associationData = new HashMap<>();
      HashMap<String,String> rolePlayerMap = new HashMap<String,String>();

      TreeMap<String,HashMap<String,ArrayList<HashMap<String,String>>>> assocRoleMapper = new TreeMap<>();
      HashMap<String,ArrayList<HashMap<String,String>>> playerNameMap = new HashMap<>();
      HashMap<String,String> currentMap = new HashMap<>();

      TopicIF newTopic = null;
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;

      String siteUrl = "";

      switch (site) {
        case NDLA:
          siteUrl = NDLA_SITE_URL;
          break;
        case DELING:
          siteUrl = DELING_SITE_URL;
          break;
        case NYGIV:
          siteUrl = FYR_SITE_URL;
          break;
      }

      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap();
        LocatorIF baseAddress = store.getBaseAddress();
        TopicIF subjectMatter = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#"+subjectMatterIdentifier));
        Collection<AssociationIF> associationIFs = subjectMatter.getAssociations();
        for(AssociationIF associationIF : associationIFs){
          String currentAssociationTypePSI = "";
          String currentTypeTypePSI = "";
          ArrayList<String> blackList = new ArrayList<>();
          blackList.add("http://psi.topic.ndla.no/#aim_has_topic");
          blackList.add("http://psi.topic.ndla.no/aim_has_topic");
          blackList.add("http://psi.udir.no/ontologi/aim_has_topic");

          TopicIF associationType = associationIF.getType();
          Collection<TopicIF> typeTypes = associationType.getTypes();
          String typeTypePSI = "";
          for(TopicIF typeType : typeTypes) {
            Collection<LocatorIF> typeTypePsis = typeType.getSubjectIdentifiers();
            if(typeTypePsis.size() == 1){
              typeTypePSI = typeTypePsis.iterator().next().getAddress();
            }
            else{
              int typeTypeCounter = 0;
              for(LocatorIF typeTypeLocator : typeTypePsis) {
                if(typeTypeCounter == 0) {
                  typeTypePSI = typeTypeLocator.getAddress();
                  break;
                }
              }
            }
          }

          Collection<LocatorIF> associationTypeLocators = associationType.getSubjectIdentifiers();
          String associationTypePSI = "";
          if(associationTypeLocators.size() == 1) {
            associationTypePSI = associationTypeLocators.iterator().next().getAddress();
          }
          else {
            int associationTypeCounter = 0;
            for(LocatorIF associationTypeLocator : associationTypeLocators) {
              if(associationTypeCounter == 0) {
                associationTypePSI = associationTypeLocator.getAddress();
                break;
              }
            }
          }//end if associationTypeLocators

          Collection<TopicNameIF> assocTypeNames = associationType.getTopicNames();
          ArrayList<HashMap<String,String>> currentNameList = new ArrayList<>();

          if (assocTypeNames.size() > 0) {
            for (TopicNameIF assocTypeName : assocTypeNames) {

              Collection<TopicIF> assocTypeScopes = assocTypeName.getScope();
              String combined = "";
              for (TopicIF assocTypeScope : assocTypeScopes) {

                Collection<LocatorIF> assocTypeScopeLocators = assocTypeScope.getSubjectIdentifiers();
                for (LocatorIF assocTypeScopeLocator : assocTypeScopeLocators) {

                  if (assocTypeScopeLocator.getAddress().contains("iso")) {
                    currentMap.put(assocTypeScopeLocator.getAddress()+"_"+assocTypeName.getObjectId(),assocTypeName.getValue());
                  }

                  if(!assocTypeScopeLocator.getAddress().contains("iso") && !assocTypeScopeLocator.getAddress().contains("language-neutral"))
                    rolePlayerMap.put(assocTypeName.getObjectId(),assocTypeScopeLocator.getAddress());
                }//end for locators

              } //end for scopes
              if(!associationData.containsValue(currentNameList)){
                currentNameList.add(currentMap);
                associationData.put(associationTypePSI,currentNameList);
              }


            }//end for names
          }//end if typenames

          //System.out.println("currentNameList: "+currentNameList);
          if(!blackList.contains(associationTypePSI)){

            currentAssociationTypePSI = associationTypePSI;
            currentTypeTypePSI = typeTypePSI;
          }

          if(currentAssociationTypePSI.length() > 0) {
            Collection<AssociationRoleIF> roles = associationIF.getRoles();

            for (AssociationRoleIF role : roles) {
              String rolePlayerPSI = "";
              TopicIF roleType = role.getType();

              TopicIF rolePlayer = role.getPlayer();
              Collection<LocatorIF> rolePlayerLocators = rolePlayer.getSubjectIdentifiers();

              if(rolePlayerLocators.size() == 1) {
                if(rolePlayerLocators.iterator().next().getAddress().contains("topics")) {
                  rolePlayerPSI = rolePlayerLocators.iterator().next().getAddress();
                }
              }
              else {
                int rolePlayerCounter = 0;
                for(LocatorIF rolePlayerLocator : rolePlayerLocators) {
                  if(rolePlayerLocator.getAddress().contains("topics")) {
                    rolePlayerPSI = rolePlayerLocator.getAddress();
                    break;
                  }
                }
              }

              if(rolePlayerPSI.contains("topics")){
                Collection<TopicNameIF> rolePlayerNames = rolePlayer.getTopicNames();
                ArrayList<HashMap<String,String>> currentRoleNameList = new ArrayList<>();
                ArrayList<String> foundNameMaps = new ArrayList<>();
                if (rolePlayerNames.size() > 0) {
                  for (TopicNameIF rolePlayerName : rolePlayerNames) {
                    HashMap<String,String> currentNameMapper = new HashMap<>();
                    Collection<TopicIF> rolePlayerNameScopes = rolePlayerName.getScope();
                    for (TopicIF rolePlayerNameScope : rolePlayerNameScopes) {
                      Collection<LocatorIF> rolePlayerNameScopeLocators = rolePlayerNameScope.getSubjectIdentifiers();

                      for (LocatorIF rolePlayerNameScopeLocator : rolePlayerNameScopeLocators) {
                        if(rolePlayerNameScopeLocator.getAddress().length() > 0){
                          if(!foundNameMaps.contains(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue())){
                            currentNameMapper.put(rolePlayerNameScopeLocator.getAddress(),rolePlayerName.getValue());
                            foundNameMaps.add(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue());
                          }

                        }
                      }//end for locators
                    } //end for scopes

                    if(currentNameMapper.size() > 1){
                      Iterator cMapIt = currentNameMapper.keySet().iterator();
                      ArrayList<String> currentNameMaps = new ArrayList<>();
                      while(cMapIt.hasNext()){
                        HashMap<String,String> newCurrentNameMapper = new HashMap<>();
                        String cMapLang = (String) cMapIt.next();
                        if(!currentNameMaps.contains(cMapLang+"_"+currentNameMapper.get(cMapLang))){
                          newCurrentNameMapper.put(cMapLang,currentNameMapper.get(cMapLang));
                          currentRoleNameList.add(newCurrentNameMapper);
                          currentNameMaps.add(cMapLang+"_"+currentNameMapper.get(cMapLang));
                        }

                      }
                    }
                    else{
                      currentRoleNameList.add(currentNameMapper);
                    }

                  }//end for names
                  playerNameMap.put(rolePlayerPSI,currentRoleNameList);

                }//end if typenames
              }



            }//end for AssociationRoleIf
            assocRoleMapper.put(currentAssociationTypePSI,playerNameMap);
          }

        }//end assocs


        JSONArray associationJSONArray = new JSONArray();
        Iterator it = associationData.keySet().iterator();
        while(it.hasNext()){
          JSONObject assocJSONObject = new JSONObject();
          String assocId = (String) it.next();
          assocJSONObject.put("associationId",assocId);
          JSONArray assocNameJSONArray = new JSONArray();
          ArrayList<HashMap<String,String>> assocNameData = associationData.get(assocId);
          for(HashMap<String,String> nameMap : assocNameData){
            Iterator nit = nameMap.keySet().iterator();
            while(nit.hasNext()){
              JSONObject assocNameJSONObject = new JSONObject();
              String compoundNameId = (String) nit.next();
              String[] nameIdFragments = compoundNameId.split("_");
              String nameLangString = nameIdFragments[0];
              String rolePsiString = rolePlayerMap.get(nameIdFragments[1]);
              String nameString = nameMap.get(compoundNameId);
              assocNameJSONObject.put("language",nameLangString);
              assocNameJSONObject.put("role",rolePsiString);
              assocNameJSONObject.put("name",nameString);
              assocNameJSONArray.put(assocNameJSONObject);
            }//end while nameMap
          }//end for assocNameData
          assocJSONObject.put("associationTypeNames",assocNameJSONArray);
          JSONArray relatedTopicsJSONArray = new JSONArray();
          HashMap<String,ArrayList<HashMap<String,String>>> topicDataMap = assocRoleMapper.get(assocId);
          Iterator tnIt = topicDataMap.keySet().iterator();
          while(tnIt.hasNext()) {
            String topicDataId  = (String) tnIt.next();
            JSONObject topicJSONObject = new JSONObject();
            topicJSONObject.put("topicId",topicDataId);
            JSONArray topicNameJSONArray = new JSONArray();
            ArrayList<HashMap<String,String>> topicList = topicDataMap.get(topicDataId);
            for(HashMap<String,String> topicNameMapData : topicList) {
              Iterator tnMIt = topicNameMapData.keySet().iterator();
              JSONObject topicNameJSONObject = new JSONObject();

              while(tnMIt.hasNext()){
                String topicNameLang = (String) tnMIt.next();
                String topicNameString = topicNameMapData.get(topicNameLang);
                topicNameJSONObject.put(topicNameLang,topicNameString);
              }//enf while topicNAmeMap
              topicNameJSONArray.put(topicNameJSONObject);
            }//end for list of names
            topicJSONObject.put("topicNames",topicNameJSONArray);


            HashMap<String,String> existingProcessState = Utils.getProcessState(topicDataId,topicMap);
            if(existingProcessState.size() > 0){
              topicJSONObject.put("processState",existingProcessState.get("processState"));
            }
            else{
              topicJSONObject.put("processState","0");
            }

            HashMap<String,String> existingApprovedState = Utils.getApprovedState(topicDataId,topicMap);
            String existingApproved = existingApprovedState.get("approved");
            String existingApprovalDate = existingApprovedState.get("approval_date");

            if(null != existingApproved && existingApproved.length() > 0) {
              topicJSONObject.put("approved",existingApproved);
              if(null != existingApprovalDate) {
                topicJSONObject.put("approval_date",existingApprovalDate);
              }
            }
            else{
              topicJSONObject.put("approved","false");
            }

            relatedTopicsJSONArray.put(topicJSONObject);
          }

          assocJSONObject.put("relatedTopics",relatedTopicsJSONArray);
          associationJSONArray.put(assocJSONObject);
        }//end while assocs
        result.put("relations",associationJSONArray);
      }
      catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          throw new NdlaServiceException(e.getMessage());
        }
      }
      finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }

      return result;
    }


  public JSONObject getSubjectOntologyTopicRelations(String subjectMatterIdentifier, NdlaSite site)  throws NdlaServiceException, BadObjectReferenceException {
    JSONObject result = new JSONObject();
    JSONArray associations = new JSONArray();

    HashMap<String,ArrayList<HashMap<String,String>>> associationData = new HashMap<>();
    HashMap<String,String> rolePlayerMap = new HashMap<String,String>();
    HashMap<String,ArrayList<HashMap<String,String>>> playerNameMap = new HashMap<>();
    HashMap<String,HashMap<String,String>> roleNameMap = new HashMap<>();
    ArrayList<String> otherTopicRoles = new ArrayList<>();
    TreeMap<String,ArrayList<String>> otherTopicMap = new TreeMap<>();


    ArrayList<String> blackList = new ArrayList<>();
    blackList.add("http://psi.topic.ndla.no/#ndlanode-topic-generic");
    blackList.add("http://psi.topic.ndla.no/#aim_has_topic");
    blackList.add("http://psi.topic.ndla.no/aim_has_topic");
    blackList.add("http://psi.udir.no/ontologi/aim_has_topic");

    String query = String.format("association($COLUMN),reifies($TOPIC, $COLUMN), {udir-subject($TOPIC,\"http://psi.topic.ndla.no/#%s\")|subjectmatter($TOPIC,\"http://psi.topic.ndla.no/#%s\")}?" ,subjectMatterIdentifier,subjectMatterIdentifier);
    ArrayList<Object> assocs = executeObjectCollectionQuery(query);
      for( Object assoc : assocs) {
        boolean hasOntologyRelationType = false;
        String currentAssociationTypePSI = "";
        String currentTypeTypePSI = "";
        String currentOntologyRelationType = "";
        AssociationIF associationIF = (AssociationIF) assoc;
        TopicIF reifier = associationIF.getReifier();

        Collection<OccurrenceIF> reifierOccurrences = reifier.getOccurrences();
        for(OccurrenceIF reifierOccurrence : reifierOccurrences) {
          String occIdentifier = reifierOccurrence.getType().getSubjectIdentifiers().iterator().next().getAddress();
          if(occIdentifier.contains("ontology_topic_relationship_type") && reifierOccurrence.getValue().equals("http://psi.topic.ndla.no/#subjectmatter")){
            hasOntologyRelationType = true;
            currentOntologyRelationType = reifierOccurrence.getValue();
            break;
          }
        }

      if(hasOntologyRelationType){
        String reifierPSI = reifier.getSubjectIdentifiers().iterator().next().getAddress();
        TopicIF associationType = associationIF.getType();
        Collection<TopicIF> typeTypes = associationType.getTypes();
        String typeTypePSI = "";
        for(TopicIF typeType : typeTypes) {
          Collection<LocatorIF> typeTypePsis = typeType.getSubjectIdentifiers();
          if(typeTypePsis.size() == 1){
            typeTypePSI = typeTypePsis.iterator().next().getAddress();
          }
          else{
            int typeTypeCounter = 0;
            for(LocatorIF typeTypeLocator : typeTypePsis) {
              if(typeTypeCounter == 0) {
                typeTypePSI = typeTypeLocator.getAddress();
                break;
              }
            }
          }
        }

        Collection<LocatorIF> associationTypeLocators = associationType.getSubjectIdentifiers();
        String associationTypePSI = "";
        if(associationTypeLocators.size() == 1) {
          associationTypePSI = associationTypeLocators.iterator().next().getAddress();
        }
        else {
          int associationTypeCounter = 0;
          for(LocatorIF associationTypeLocator : associationTypeLocators) {
            if(associationTypeCounter == 0) {
              associationTypePSI = associationTypeLocator.getAddress();
              break;
            }
          }
        }//end if associationTypeLocators

        Collection<TopicNameIF> assocTypeNames = associationType.getTopicNames();
        ArrayList<HashMap<String,String>> currentNameList = new ArrayList<>();

        if (assocTypeNames.size() > 0) {
          for (TopicNameIF assocTypeName : assocTypeNames) {
            HashMap<String,String> currentMap = new HashMap<>();
            Collection<TopicIF> assocTypeScopes = assocTypeName.getScope();
            String combined = "";
            for (TopicIF assocTypeScope : assocTypeScopes) {

              Collection<LocatorIF> assocTypeScopeLocators = assocTypeScope.getSubjectIdentifiers();
              for (LocatorIF assocTypeScopeLocator : assocTypeScopeLocators) {

                if (assocTypeScopeLocator.getAddress().contains("iso")) {
                  currentMap.put(assocTypeScopeLocator.getAddress()+"_"+assocTypeName.getObjectId(),assocTypeName.getValue());
                }

                if(!assocTypeScopeLocator.getAddress().contains("iso") && !assocTypeScopeLocator.getAddress().contains("language-neutral"))
                  rolePlayerMap.put(assocTypeName.getObjectId(),assocTypeScopeLocator.getAddress());
              }//end for locators

            } //end for scopes

            currentNameList.add(currentMap);
          }//end for names
          if(!blackList.contains(associationTypePSI)){
            associationData.put(associationTypePSI,currentNameList);
            currentAssociationTypePSI = associationTypePSI;
            currentTypeTypePSI = typeTypePSI;
          }
        }//end if typenames

        if(currentAssociationTypePSI.length() > 0) {
          Collection<AssociationRoleIF> roles =  associationIF.getRoles();
          String rolePlayerPSI = "";
          String accumulate = "";
          for (AssociationRoleIF role : roles) {
            TopicIF roleType = role.getType();
            Collection<LocatorIF> roleTypeLocators = roleType.getSubjectIdentifiers();
            String rolePSI = "";

            if(roleTypeLocators.size() == 1) {
              rolePSI = roleTypeLocators.iterator().next().getAddress();
            }
            else {
              int roleTypeCounter = 0;
              for(LocatorIF roleTypeLocator : roleTypeLocators) {
                if(roleTypeCounter == 0) {
                  rolePSI = roleTypeLocator.getAddress();
                  break;
                }
              }
            }

            if(!roleNameMap.containsKey(rolePSI)){
              HashMap<String,String> tempRoleNames = Utils.getTopicNames(roleType);
              roleNameMap.put(rolePSI,tempRoleNames);
            }

            TopicIF rolePlayer = role.getPlayer();
            Collection<LocatorIF> rolePlayerLocators = rolePlayer.getSubjectIdentifiers();

            if(rolePlayerLocators.size() == 1) {
              String locatorString = rolePlayerLocators.iterator().next().getAddress();
              if(locatorString.contains("#topic")) {
                rolePlayerPSI = locatorString;
              }
            }
            else {
              int rolePlayerCounter = 0;
              for(LocatorIF rolePlayerLocator : rolePlayerLocators) {
                if(rolePlayerLocator.getAddress().contains("#topic")) {
                  rolePlayerPSI = rolePlayerLocator.getAddress();
                  break;
                }
              }
            }
            accumulate += rolePlayerPSI+"_"+associationTypePSI+"_"+typeTypePSI+"_"+rolePSI+"_"+reifierPSI+"_"+currentOntologyRelationType+"§";

            Collection<TopicNameIF> rolePlayerNames = rolePlayer.getTopicNames();
            ArrayList<HashMap<String,String>> currentRoleNameList = new ArrayList<>();
            ArrayList<String> foundNameMaps = new ArrayList<>();
            if (rolePlayerNames.size() > 0) {
              for (TopicNameIF rolePlayerName : rolePlayerNames) {
                HashMap<String,String> currentNameMapper = new HashMap<>();
                Collection<TopicIF> rolePlayerNameScopes = rolePlayerName.getScope();
                for (TopicIF rolePlayerNameScope : rolePlayerNameScopes) {
                  Collection<LocatorIF> rolePlayerNameScopeLocators = rolePlayerNameScope.getSubjectIdentifiers();

                  for (LocatorIF rolePlayerNameScopeLocator : rolePlayerNameScopeLocators) {
                    if(rolePlayerNameScopeLocator.getAddress().length() > 0){
                      //currentNameMapper.put("language",rolePlayerNameScopeLocator.getAddress());
                      if(!foundNameMaps.contains(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue())){
                        currentNameMapper.put(rolePlayerNameScopeLocator.getAddress(),rolePlayerName.getValue());
                        foundNameMaps.add(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue());
                      }

                    }
                  }//end for locators
                } //end for scopes

                if(currentNameMapper.size() > 1){
                  Iterator cMapIt = currentNameMapper.keySet().iterator();
                  ArrayList<String> currentNameMaps = new ArrayList<>();
                  while(cMapIt.hasNext()){
                    HashMap<String,String> newCurrentNameMapper = new HashMap<>();
                    String cMapLang = (String) cMapIt.next();
                    if(!currentNameMaps.contains(cMapLang+"_"+currentNameMapper.get(cMapLang))){
                      newCurrentNameMapper.put(cMapLang,currentNameMapper.get(cMapLang));
                      currentRoleNameList.add(newCurrentNameMapper);
                      currentNameMaps.add(cMapLang+"_"+currentNameMapper.get(cMapLang));
                    }

                  }
                }
                else{
                  currentRoleNameList.add(currentNameMapper);
                }

              }//end for names
              playerNameMap.put(rolePlayerPSI,currentRoleNameList);
            }//end if typenames
          }//end for roles
          otherTopicRoles.add(accumulate);
        }//end if currentAssocType
      }//end if hasCorrectRelationType))


    }//end for assocs

    Collections.sort(otherTopicRoles);
    Collections.reverse(otherTopicRoles);

    for(String otherTopicRole : otherTopicRoles) {
      String[] roleArray = otherTopicRole.split("§");
      String role1 = roleArray[0];
      String role2 = roleArray[1];

      String[] httpArray1 = role1.split("_http");
      String[] httpArray2 = role2.split("_http");

      String topicId1 = httpArray1[0];
      String assocId1 = httpArray1[1];
      String assocType1 = httpArray1[2];
      String roleId1 = httpArray1[3];
      String reifierId1 = httpArray1[4];
      String ontologyType1 = "";
      if(httpArray1.length > 5){
        ontologyType1 = httpArray1[5];
      }
      String topicId2 = httpArray2[0];
      String assocId2 = httpArray2[1];
      String assocType2 = httpArray2[2];
      String roleId2 = httpArray2[3];
      String reifierId2 = httpArray2[4];
      String ontologyType2 = "";
      if(httpArray2.length > 5){
        ontologyType2 = httpArray2[5];
      }

      if(assocId1.equals(assocId2)) {
        //JSONObject assocObject = new JSONObject();
        String otherTopicId = "";
        String topicId = "";
        String assocId = assocId1;
        String assocType = assocType1;
        String thisRoleId = "";
        String thatRoleId = "";
        String thisReifierId = reifierId1;
        String thisOntologyType = ontologyType1;

        if(topicId1.contains("kl06")){
          otherTopicId = topicId1;
          topicId = topicId2;
          thisRoleId = roleId1;
          thatRoleId = roleId2;
        }
        else if(topicId2.contains("kl06")){
          otherTopicId = topicId2;
          topicId = topicId1;
          thisRoleId = roleId2;
          thatRoleId = roleId1;
        }
        else if(topicId2.contains("#topic-")){
          topicId = topicId2;
          otherTopicId = topicId1;
          thisRoleId = roleId2;
          thatRoleId = roleId1;
        }
        else if(topicId1.contains("#topic-")){
          otherTopicId = topicId2;
          topicId = topicId1;
          thisRoleId = roleId1;
          thatRoleId = roleId2;
        }

        if(!otherTopicMap.containsKey(otherTopicId)){
          otherTopicMap.put(otherTopicId,new ArrayList<String>());
        }
        otherTopicMap.get(otherTopicId).add("http"+thisRoleId+"_"+"http"+assocId+"_"+topicId+"_"+"http"+assocType+"_"+topicId+"_"+"http"+thatRoleId+"_http"+thisReifierId+"_http"+thisOntologyType);

      }//end if associd matches
    }//end for

    try{
      Iterator<String> it = otherTopicMap.keySet().iterator();
      while(it.hasNext()) {
        JSONObject otherTopicJSON = new JSONObject();
        String otherTopicId = (String) it.next();
        otherTopicJSON.put("topicId",otherTopicId);
        JSONArray otherTopicNames = new JSONArray();
        ArrayList<HashMap<String,String>> otherTopicNameMapping = playerNameMap.get(otherTopicId);

        for(HashMap<String,String> otherTopicLangNames : otherTopicNameMapping) {
          Iterator<String> otherTopicLangNamesIterator = otherTopicLangNames.keySet().iterator();
          JSONObject otherTopicNameObject = new JSONObject();
          ArrayList<String> foundOtherTopicNames = new ArrayList<>();
          while (otherTopicLangNamesIterator.hasNext()){
            String otherTopicNameLanguage = (String) otherTopicLangNamesIterator.next();
            if(!foundOtherTopicNames.contains(otherTopicNameLanguage+"_"+otherTopicLangNames.get(otherTopicNameLanguage))) {
              otherTopicNameObject.put(otherTopicNameLanguage,otherTopicLangNames.get(otherTopicNameLanguage));
              foundOtherTopicNames.add(otherTopicNameLanguage+"_"+otherTopicLangNames.get(otherTopicNameLanguage));
            }
          }

          if(otherTopicNameObject.length() > 0) {
            otherTopicNames.put(otherTopicNameObject);
          }
        }

        otherTopicJSON.put("topicNames",otherTopicNames);


        ArrayList<String> data = otherTopicMap.get(otherTopicId);
        JSONArray assocJSON = new JSONArray();
        for(String assocData : data) {
          String[] assocArray = assocData.split("_http");
          JSONObject assocObject = new JSONObject();
          String thisRoleId = assocArray[0];
          String assocId = "http"+assocArray[1];
          String assocType = "http"+assocArray[3];
          String reifierId = "http"+assocArray[6];
          String ontologyType = "";
          if(assocArray.length > 7){
            ontologyType = "http"+assocArray[7];
          }

          assocObject.put("associationId",assocId);
          assocObject.put("associationType",assocType);
          assocObject.put("reifierId",reifierId);
          assocObject.put("ontologyType",ontologyType);
          JSONArray assocTypeNames = new JSONArray();

          ArrayList<HashMap<String,String>> typeNameMap = associationData.get(assocId);
          for(HashMap<String,String> typeLangNames : typeNameMap){
            Iterator<String> typeLangNamesIterator = typeLangNames.keySet().iterator();
            JSONObject typeNameObject = new JSONObject();
            while (typeLangNamesIterator.hasNext()){
              String typeNameLanguage = (String) typeLangNamesIterator.next();
              String[] typeNameLanguageArray = typeNameLanguage.split("_");
              typeNameObject.put("language",typeNameLanguageArray[0]);
              typeNameObject.put("role",rolePlayerMap.get(typeNameLanguageArray[1]));
              typeNameObject.put("name",typeLangNames.get(typeNameLanguage));
            }
            assocTypeNames.put(typeNameObject);
          }
          assocObject.put("associationTypeNames",assocTypeNames);

          assocObject.put("rolePlayedByOtherTopic",thisRoleId);
          String topicId = "http"+assocArray[4];
          assocObject.put("topicId",topicId);


          JSONArray topicNames = new JSONArray();
          ArrayList<HashMap<String,String>> topicNameMapping = playerNameMap.get(topicId);
          ArrayList<String> foundTopicNames = new ArrayList<>();

          for(HashMap<String,String> topicLangNames : topicNameMapping) {
            Iterator<String> topicLangNamesIterator = topicLangNames.keySet().iterator();
            JSONObject topicNameObject = new JSONObject();
            while (topicLangNamesIterator.hasNext()){
              String topicNameLanguage = (String) topicLangNamesIterator.next();

              if(!foundTopicNames.contains(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage))) {
                topicNameObject.put(topicNameLanguage,topicLangNames.get(topicNameLanguage));
                foundTopicNames.add(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage));
              }
            }

            if(topicNameObject.length() > 0) {
              topicNames.put(topicNameObject);
            }
          }

          assocObject.put("topicNames",topicNames);


          String thatRoleId = "http"+assocArray[5];
          assocObject.put("rolePlayedByTopic",thatRoleId);
          assocJSON.put(assocObject);
        }

        otherTopicJSON.put("relations",assocJSON);
        associations.put(otherTopicJSON);
      }

      JSONArray roleNameMapping = new JSONArray();
      Iterator roleNameMapIt = roleNameMap.keySet().iterator();
      while (roleNameMapIt.hasNext()){
        JSONObject roleJSONObject = new JSONObject();
        JSONArray roleJSONNameArray = new JSONArray();

        String rolePsi = (String) roleNameMapIt.next();
        roleJSONObject.put("psi",rolePsi);
        HashMap<String,String> roleNames = roleNameMap.get(rolePsi);
        Iterator roleNameIt = roleNames.keySet().iterator();
        while(roleNameIt.hasNext()){
          JSONObject roleNameJSON = new JSONObject();
          String roleNameLanguage = (String) roleNameIt.next();
          roleNameJSON.put("language",roleNameLanguage);
          String roleNameString = (String) roleNames.get(roleNameLanguage);
          roleNameJSON.put("name",roleNameString);
          roleJSONNameArray.put(roleNameJSON);
        }
        roleJSONObject.put("roleNames",roleJSONNameArray);
        roleNameMapping.put(roleJSONObject);
      }

      result.put("associations",associations);
      result.put("roleNameMapping",roleNameMapping);
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage());
    }

    return result;
  }


    public JSONObject getCurriculaOntologyTopics(String subjectMatterIdentifier, NdlaSite site)  throws NdlaServiceException, BadObjectReferenceException {
      JSONObject result = new JSONObject();
      JSONArray associations = new JSONArray();

      HashMap<String,ArrayList<HashMap<String,String>>> associationData = new HashMap<>();
      HashMap<String,String> rolePlayerMap = new HashMap<String,String>();
      HashMap<String,ArrayList<HashMap<String,String>>> playerNameMap = new HashMap<>();
      HashMap<String,HashMap<String,String>> roleNameMap = new HashMap<>();
      ArrayList<String> otherTopicRoles = new ArrayList<>();
      TreeMap<String,ArrayList<String>> otherTopicMap = new TreeMap<>();

      ArrayList<String> blackList = new ArrayList<>();
      blackList.add("http://psi.topic.ndla.no/#ndlanode-topic-generic");
      blackList.add("http://psi.topic.ndla.no/#aim_has_topic");
      blackList.add("http://psi.topic.ndla.no/aim_has_topic");
      blackList.add("http://psi.udir.no/ontologi/aim_has_topic");

      String query = String.format("association($COLUMN),reifies($TOPIC, $COLUMN), {udir-subject($TOPIC,\"http://psi.topic.ndla.no/#%s\")|subjectmatter($TOPIC,\"http://psi.topic.ndla.no/#%s\")}?" ,subjectMatterIdentifier,subjectMatterIdentifier);
      ArrayList<Object> assocs = executeObjectCollectionQuery(query);
      for( Object assoc : assocs) {
        boolean hasCurriculaOntologyRelationType = false;
        String currentAssociationTypePSI = "";
        String currentTypeTypePSI = "";
        String currentCurriculaOntologyRelationType = "";
        AssociationIF associationIF = (AssociationIF) assoc;
        TopicIF reifier = associationIF.getReifier();

        Collection<OccurrenceIF> reifierOccurrences = reifier.getOccurrences();
        for(OccurrenceIF reifierOccurrence : reifierOccurrences) {
          String occIdentifier = reifierOccurrence.getType().getSubjectIdentifiers().iterator().next().getAddress();

          if(occIdentifier.contains("ontology_topic_relationship_type") && reifierOccurrence.getValue().equals("http://psi.udir.no/ontologi/lkt/#laereplan")){
            hasCurriculaOntologyRelationType = true;
            currentCurriculaOntologyRelationType = reifierOccurrence.getValue();
            break;
          }
        }

        if(hasCurriculaOntologyRelationType){
          String reifierPSI = reifier.getSubjectIdentifiers().iterator().next().getAddress();
          TopicIF associationType = associationIF.getType();
          Collection<TopicIF> typeTypes = associationType.getTypes();
          String typeTypePSI = "";
          for(TopicIF typeType : typeTypes) {
            Collection<LocatorIF> typeTypePsis = typeType.getSubjectIdentifiers();
            if(typeTypePsis.size() == 1){
              typeTypePSI = typeTypePsis.iterator().next().getAddress();
            }
            else{
              int typeTypeCounter = 0;
              for(LocatorIF typeTypeLocator : typeTypePsis) {
                if(typeTypeCounter == 0) {
                  typeTypePSI = typeTypeLocator.getAddress();
                  break;
                }
              }
            }
          }

          Collection<LocatorIF> associationTypeLocators = associationType.getSubjectIdentifiers();
          String associationTypePSI = "";
          if(associationTypeLocators.size() == 1) {
            associationTypePSI = associationTypeLocators.iterator().next().getAddress();
          }
          else {
            int associationTypeCounter = 0;
            for(LocatorIF associationTypeLocator : associationTypeLocators) {
              if(associationTypeCounter == 0) {
                associationTypePSI = associationTypeLocator.getAddress();
                break;
              }
            }
          }//end if associationTypeLocators

          Collection<TopicNameIF> assocTypeNames = associationType.getTopicNames();
          ArrayList<HashMap<String,String>> currentNameList = new ArrayList<>();

          if (assocTypeNames.size() > 0) {
            for (TopicNameIF assocTypeName : assocTypeNames) {
              HashMap<String,String> currentMap = new HashMap<>();
              Collection<TopicIF> assocTypeScopes = assocTypeName.getScope();
              String combined = "";
              for (TopicIF assocTypeScope : assocTypeScopes) {

                Collection<LocatorIF> assocTypeScopeLocators = assocTypeScope.getSubjectIdentifiers();
                for (LocatorIF assocTypeScopeLocator : assocTypeScopeLocators) {

                  if (assocTypeScopeLocator.getAddress().contains("iso")) {
                    currentMap.put(assocTypeScopeLocator.getAddress()+"_"+assocTypeName.getObjectId(),assocTypeName.getValue());
                  }

                  if(!assocTypeScopeLocator.getAddress().contains("iso") && !assocTypeScopeLocator.getAddress().contains("language-neutral"))
                    rolePlayerMap.put(assocTypeName.getObjectId(),assocTypeScopeLocator.getAddress());
                }//end for locators

              } //end for scopes

              currentNameList.add(currentMap);
            }//end for names
            if(!blackList.contains(associationTypePSI)){
              associationData.put(associationTypePSI,currentNameList);
              currentAssociationTypePSI = associationTypePSI;
              currentTypeTypePSI = typeTypePSI;
            }
          }//end if typenames

          if(currentAssociationTypePSI.length() > 0) {
            Collection<AssociationRoleIF> roles =  associationIF.getRoles();
            String rolePlayerPSI = "";
            String accumulate = "";
            for (AssociationRoleIF role : roles) {
              TopicIF roleType = role.getType();
              Collection<LocatorIF> roleTypeLocators = roleType.getSubjectIdentifiers();
              String rolePSI = "";

              if(roleTypeLocators.size() == 1) {
                rolePSI = roleTypeLocators.iterator().next().getAddress();
              }
              else {
                int roleTypeCounter = 0;
                for(LocatorIF roleTypeLocator : roleTypeLocators) {
                  if(roleTypeCounter == 0) {
                    rolePSI = roleTypeLocator.getAddress();
                    break;
                  }
                }
              }

              if(!roleNameMap.containsKey(rolePSI)){
                HashMap<String,String> tempRoleNames = Utils.getTopicNames(roleType);
                roleNameMap.put(rolePSI,tempRoleNames);
              }

              TopicIF rolePlayer = role.getPlayer();
              Collection<LocatorIF> rolePlayerLocators = rolePlayer.getSubjectIdentifiers();

              if(rolePlayerLocators.size() == 1) {
                String locatorString = rolePlayerLocators.iterator().next().getAddress();
                if(locatorString.contains("#topic")) {
                  rolePlayerPSI = locatorString;
                }
              }
              else {
                int rolePlayerCounter = 0;
                for(LocatorIF rolePlayerLocator : rolePlayerLocators) {
                  if(rolePlayerLocator.getAddress().contains("#topic")) {
                    rolePlayerPSI = rolePlayerLocator.getAddress();
                    break;
                  }
                }
              }
              accumulate += rolePlayerPSI+"_"+associationTypePSI+"_"+typeTypePSI+"_"+rolePSI+"_"+reifierPSI+"_"+currentCurriculaOntologyRelationType+"§";

              Collection<TopicNameIF> rolePlayerNames = rolePlayer.getTopicNames();
              ArrayList<HashMap<String,String>> currentRoleNameList = new ArrayList<>();
              ArrayList<String> foundNameMaps = new ArrayList<>();
              if (rolePlayerNames.size() > 0) {
                for (TopicNameIF rolePlayerName : rolePlayerNames) {
                  HashMap<String,String> currentNameMapper = new HashMap<>();
                  Collection<TopicIF> rolePlayerNameScopes = rolePlayerName.getScope();
                  for (TopicIF rolePlayerNameScope : rolePlayerNameScopes) {
                    Collection<LocatorIF> rolePlayerNameScopeLocators = rolePlayerNameScope.getSubjectIdentifiers();

                    for (LocatorIF rolePlayerNameScopeLocator : rolePlayerNameScopeLocators) {
                      if(rolePlayerNameScopeLocator.getAddress().length() > 0){
                        //currentNameMapper.put("language",rolePlayerNameScopeLocator.getAddress());
                        if(!foundNameMaps.contains(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue())){
                          currentNameMapper.put(rolePlayerNameScopeLocator.getAddress(),rolePlayerName.getValue());
                          foundNameMaps.add(rolePlayerNameScopeLocator.getAddress()+"_"+rolePlayerName.getValue());
                        }

                      }
                    }//end for locators
                  } //end for scopes

                  if(currentNameMapper.size() > 1){
                    Iterator cMapIt = currentNameMapper.keySet().iterator();
                    ArrayList<String> currentNameMaps = new ArrayList<>();
                    while(cMapIt.hasNext()){
                      HashMap<String,String> newCurrentNameMapper = new HashMap<>();
                      String cMapLang = (String) cMapIt.next();
                      if(!currentNameMaps.contains(cMapLang+"_"+currentNameMapper.get(cMapLang))){
                        newCurrentNameMapper.put(cMapLang,currentNameMapper.get(cMapLang));
                        currentRoleNameList.add(newCurrentNameMapper);
                        currentNameMaps.add(cMapLang+"_"+currentNameMapper.get(cMapLang));
                      }

                    }
                  }
                  else{
                    currentRoleNameList.add(currentNameMapper);
                  }

                }//end for names
                playerNameMap.put(rolePlayerPSI,currentRoleNameList);
              }//end if typenames
            }//end for roles
            otherTopicRoles.add(accumulate);
          }//end if currentAssocType
        }//end has laereplan as curriculaOntologyRelationType
      }//end for assocs

      Collections.sort(otherTopicRoles);
      Collections.reverse(otherTopicRoles);

      for(String otherTopicRole : otherTopicRoles) {
        String[] roleArray = otherTopicRole.split("§");
        String role1 = roleArray[0];
        String role2 = roleArray[1];

        String[] httpArray1 = role1.split("_http");
        String[] httpArray2 = role2.split("_http");

        String topicId1 = httpArray1[0];
        String assocId1 = httpArray1[1];
        String assocType1 = httpArray1[2];
        String roleId1 = httpArray1[3];
        String reifierId1 = httpArray1[4];
        String ontologyType1 = "";
        if(httpArray1.length > 5){
          ontologyType1 = httpArray1[5];
        }


        String topicId2 = httpArray2[0];
        String assocId2 = httpArray2[1];
        String assocType2 = httpArray2[2];
        String roleId2 = httpArray2[3];
        String reifierId2 = httpArray2[4];
        String ontologyType2 = "";
        if(httpArray2.length > 5){
          ontologyType2 = httpArray2[5];
        }


        if(assocId1.equals(assocId2)) {
          //JSONObject assocObject = new JSONObject();
          String otherTopicId = "";
          String topicId = "";
          String assocId = assocId1;
          String assocType = assocType1;
          String thisRoleId = "";
          String thatRoleId = "";
          String thisReifierId = reifierId1;
          String thisOntologyType = ontologyType1;

          if(topicId1.contains("kl06")){
            otherTopicId = topicId1;
            topicId = topicId2;
            thisRoleId = roleId1;
            thatRoleId = roleId2;
          }
          else if(topicId2.contains("kl06")){
            otherTopicId = topicId2;
            topicId = topicId1;
            thisRoleId = roleId2;
            thatRoleId = roleId1;
          }
          else if(topicId2.contains("#topic-")){
            topicId = topicId2;
            otherTopicId = topicId1;
            thisRoleId = roleId2;
            thatRoleId = roleId1;
          }
          else if(topicId1.contains("#topic-")){
            otherTopicId = topicId2;
            topicId = topicId1;
            thisRoleId = roleId1;
            thatRoleId = roleId2;
          }

          if(!otherTopicMap.containsKey(otherTopicId)){
            otherTopicMap.put(otherTopicId,new ArrayList<String>());
          }
          otherTopicMap.get(otherTopicId).add("http"+thisRoleId+"_"+"http"+assocId+"_"+topicId+"_"+"http"+assocType+"_"+topicId+"_"+"http"+thatRoleId+"_http"+thisReifierId+"_http"+thisOntologyType);

        }//end if associd matches
      }//end for

      try{
        Iterator<String> it = otherTopicMap.keySet().iterator();
        while(it.hasNext()) {
          JSONObject otherTopicJSON = new JSONObject();
          String otherTopicId = (String) it.next();
          otherTopicJSON.put("topicId",otherTopicId);
          JSONArray otherTopicNames = new JSONArray();
          ArrayList<HashMap<String,String>> otherTopicNameMapping = playerNameMap.get(otherTopicId);

          for(HashMap<String,String> otherTopicLangNames : otherTopicNameMapping) {
            Iterator<String> otherTopicLangNamesIterator = otherTopicLangNames.keySet().iterator();
            JSONObject otherTopicNameObject = new JSONObject();
            ArrayList<String> foundOtherTopicNames = new ArrayList<>();
            while (otherTopicLangNamesIterator.hasNext()){
              String otherTopicNameLanguage = (String) otherTopicLangNamesIterator.next();
              if(!foundOtherTopicNames.contains(otherTopicNameLanguage+"_"+otherTopicLangNames.get(otherTopicNameLanguage))) {
                otherTopicNameObject.put(otherTopicNameLanguage,otherTopicLangNames.get(otherTopicNameLanguage));
                foundOtherTopicNames.add(otherTopicNameLanguage+"_"+otherTopicLangNames.get(otherTopicNameLanguage));
              }
            }

            if(otherTopicNameObject.length() > 0) {
              otherTopicNames.put(otherTopicNameObject);
            }
          }

          otherTopicJSON.put("topicNames",otherTopicNames);


          ArrayList<String> data = otherTopicMap.get(otherTopicId);
          JSONArray assocJSON = new JSONArray();
          for(String assocData : data) {
            String[] assocArray = assocData.split("_http");
            JSONObject assocObject = new JSONObject();
            String thisRoleId = assocArray[0];
            String assocId = "http"+assocArray[1];
            String assocType = "http"+assocArray[3];
            String reifierId = "http"+assocArray[6];
            String ontologyType = "";
            if(assocArray.length > 7){
              ontologyType = "http"+assocArray[7];
            }
            assocObject.put("associationId",assocId);
            assocObject.put("associationType",assocType);
            assocObject.put("reifierId",reifierId);
            assocObject.put("ontologyType",ontologyType);
            JSONArray assocTypeNames = new JSONArray();

            ArrayList<HashMap<String,String>> typeNameMap = associationData.get(assocId);
            for(HashMap<String,String> typeLangNames : typeNameMap){
              Iterator<String> typeLangNamesIterator = typeLangNames.keySet().iterator();
              JSONObject typeNameObject = new JSONObject();
              while (typeLangNamesIterator.hasNext()){
                String typeNameLanguage = (String) typeLangNamesIterator.next();
                String[] typeNameLanguageArray = typeNameLanguage.split("_");
                typeNameObject.put("language",typeNameLanguageArray[0]);
                typeNameObject.put("role",rolePlayerMap.get(typeNameLanguageArray[1]));
                typeNameObject.put("name",typeLangNames.get(typeNameLanguage));
              }
              assocTypeNames.put(typeNameObject);
            }
            assocObject.put("associationTypeNames",assocTypeNames);

            assocObject.put("rolePlayedByOtherTopic",thisRoleId);
            String topicId = "http"+assocArray[4];
            assocObject.put("topicId",topicId);


            JSONArray topicNames = new JSONArray();
            ArrayList<HashMap<String,String>> topicNameMapping = playerNameMap.get(topicId);
            ArrayList<String> foundTopicNames = new ArrayList<>();

            for(HashMap<String,String> topicLangNames : topicNameMapping) {
              Iterator<String> topicLangNamesIterator = topicLangNames.keySet().iterator();
              JSONObject topicNameObject = new JSONObject();
              while (topicLangNamesIterator.hasNext()){
                String topicNameLanguage = (String) topicLangNamesIterator.next();

                if(!foundTopicNames.contains(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage))) {
                  topicNameObject.put(topicNameLanguage,topicLangNames.get(topicNameLanguage));
                  foundTopicNames.add(topicNameLanguage+"_"+topicLangNames.get(topicNameLanguage));
                }
              }

              if(topicNameObject.length() > 0) {
                topicNames.put(topicNameObject);
              }
            }

            assocObject.put("topicNames",topicNames);


            String thatRoleId = "http"+assocArray[5];
            assocObject.put("rolePlayedByTopic",thatRoleId);
            assocJSON.put(assocObject);
          }
          otherTopicJSON.put("relations",assocJSON);
          associations.put(otherTopicJSON);
        }

        result.put("associations",associations);
        JSONArray roleNameMapping = new JSONArray();
        Iterator roleNameMapIt = roleNameMap.keySet().iterator();
        while (roleNameMapIt.hasNext()){
          JSONObject roleJSONObject = new JSONObject();
          JSONArray roleJSONNameArray = new JSONArray();

          String rolePsi = (String) roleNameMapIt.next();
          roleJSONObject.put("psi",rolePsi);
          HashMap<String,String> roleNames = roleNameMap.get(rolePsi);
          Iterator roleNameIt = roleNames.keySet().iterator();
          while(roleNameIt.hasNext()){
            JSONObject roleNameJSON = new JSONObject();
            String roleNameLanguage = (String) roleNameIt.next();
            roleNameJSON.put("language",roleNameLanguage);
            String roleNameString = (String) roleNames.get(roleNameLanguage);
            roleNameJSON.put("name",roleNameString);
            roleJSONNameArray.put(roleNameJSON);
          }
          roleJSONObject.put("roleNames",roleJSONNameArray);
          roleNameMapping.put(roleJSONObject);
        }
        result.put("roleNameMapping",roleNameMapping);
      } catch (JSONException e) {
        throw new NdlaServiceException(e.getMessage());
      }

      return result;
    }

    public TreeMap<String,ArrayList<TopicIF>> saveOntologyTopicAssociations(String subjectMatterIdentifier,String reifierMarker, String data, NdlaSite site)  throws NdlaServiceException, BadObjectReferenceException {
      TreeMap<String,ArrayList<TopicIF>> result = new TreeMap<>();
      TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
      TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
      TopicMapStoreIF store = null;
      boolean deleted = false;

      try {
        store = reference.createStore(false);
        TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

        LocatorIF baseAddress = store.getBaseAddress();
        TopicIF subjectTopic = null;

        try {
          String subjectUrl = "http://psi.topic.ndla.no/#" + subjectMatterIdentifier;
          subjectTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(subjectUrl));
        } catch (MalformedURLException urlEX) {
          throw new NdlaServiceException(urlEX.getMessage());
        }

        if(null != subjectTopic){

          HashMap<String,String> reifierData = new HashMap<>();
          Collection <LocatorIF> subjectPsis = subjectTopic.getSubjectIdentifiers();
          for(LocatorIF subjectPsi : subjectPsis) {
            String psi = subjectPsi.getAddress();
            if(psi.contains(subjectMatterIdentifier)){
              reifierData.put("http://psi.topic.ndla.no/#udir-subject",psi);
            }
            else{
              reifierData.put("http://psi.topic.ndla.no/#subjectmatter",psi);
            }
          }

          if(reifierMarker.length() > 0){
            reifierData.put("http://psi.topic.ndla.no/#ontology_topic_relationship_type",reifierMarker);
          }




          JSONObject payload = new JSONObject(data);
          JSONArray associations = payload.getJSONArray("associations");
          for(int i = 0; i < associations.length(); i++){
            JSONObject association = associations.getJSONObject(i);
            String relationTypeId = association.getString("relationId");

            JSONArray relatedTopics = association.getJSONArray("topics");

            for(int j = 0; j < relatedTopics.length(); j++){
              ArrayList<TopicIF> tempTopics = new ArrayList<>();
              JSONObject relatedTopic = relatedTopics.getJSONObject(j);
              String[] topicIds = relatedTopic.getString("ids").split("_");
              TopicIF topicObject1 = (TopicIF) topicMap.getObjectByItemIdentifier(baseAddress.resolveAbsolute("#" + topicIds[0]));
              TopicIF topicObject2 = (TopicIF) topicMap.getObjectByItemIdentifier(baseAddress.resolveAbsolute("#" + topicIds[1]));
              tempTopics.add(topicObject1);
              tempTopics.add(topicObject2);

              String[] roleIds = new String[2];
              JSONArray rolePlayers = relatedTopic.getJSONArray("roles");
              JSONObject roleObject1 = rolePlayers.getJSONObject(0);
              JSONObject roleObject2 = rolePlayers.getJSONObject(1);
              String roleTypeId1 = roleObject1.getString("roleName");
              String roleTypeId2 = roleObject2.getString("roleName");

              if(relationTypeId.length() > 0 && roleTypeId1.length() > 0 && roleTypeId2.length() > 0) {
                boolean assocExists = Utils.assocExists(topicIds[0], topicIds[1], relationTypeId, roleTypeId1, roleTypeId2, reifierData, topicMap);
                Utils.deleteAssocsByAssoctypeAndSubjectId(topicIds[0],topicIds[1],relationTypeId,subjectMatterIdentifier,topicMap);
                Utils.createReifiedAssociation(topicObject1, topicObject2, relationTypeId, roleTypeId1, roleTypeId2, reifierData, false, topicMap);
                result.put(relationTypeId,tempTopics);
              }
            }
          }
        }


        store.commit(); // Persist object graph.
      } catch (Exception e) {
        if (store != null) {
          store.abort(); // Roll-back changes and deactivate (top-level) transaction.
          if (e instanceof BadObjectReferenceException) {
            throw new BadObjectReferenceException(e.getMessage());
          } else {
            throw new NdlaServiceException(e.getMessage());
          }
        }
      } finally { // Release all topic map engine resources.
        if (store != null) {
          store.close();
        }
        if (repository != null) {
          repository.close();
        }
      }
      return result;
    }

    public ArrayList<TopicIF> saveAimHasTopic(String subjectMatterIdentifier, String data, NdlaSite site)  throws NdlaServiceException, BadObjectReferenceException {
        ArrayList<TopicIF> result = new ArrayList<TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        boolean deleted = false;

        try {
          store = reference.createStore(false);
          TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

          LocatorIF baseAddress = store.getBaseAddress();
          TopicIF subjectTopic = null;

          String siteUrl = "";
          switch (site) {
            case NDLA:
              siteUrl = NDLA_SITE_URL;
              break;
            case DELING:
              siteUrl = DELING_SITE_URL;
              break;
            case NYGIV:
              siteUrl = NYGIV_SITE_URL;
              break;
          }

          try {
            String subjectUrl = "http://psi.topic.ndla.no/#" + subjectMatterIdentifier;
            subjectTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(subjectUrl));
          } catch (MalformedURLException urlEX) {
            throw new NdlaServiceException(urlEX.getMessage());
          }

          JSONObject payload = new JSONObject(data);

          if (null != subjectTopic) {

            HashMap<String,String> reifierData = new HashMap<>();
            Collection <LocatorIF> subjectPsis = subjectTopic.getSubjectIdentifiers();
            for(LocatorIF subjectPsi : subjectPsis) {
              String psi = subjectPsi.getAddress();
              if(psi.contains(subjectMatterIdentifier)){
                reifierData.put("http://psi.topic.ndla.no/#udir-subject",psi);
              }
              else{
                reifierData.put("http://psi.topic.ndla.no/#subjectmatter",psi);
              }
            }


            Utils.deleteAllAimAssocsBySubjectId(subjectMatterIdentifier, topicMap);

            JSONArray aimTopics = payload.getJSONArray("aimtopics");
            for(int i = 0; i < aimTopics.length(); i++) {
              JSONObject aimTopicData = aimTopics.getJSONObject(i);
              String aimId = aimTopicData.getString("aimId");
              TopicIF aimTopic = (TopicIF) topicMap.getObjectByItemIdentifier(baseAddress.resolveAbsolute("#" + aimId));



              String aimTopicRelation = "http://psi.topic.ndla.no/#aim_has_topic";
              String primaryTopicRelation = "http://psi.topic.ndla.no/#primary-topic";
              String aimRoleTypeTopic1 = "http://psi.udir.no/ontologi/lkt/#kompetansemaal";
              String aimRoleTypeTopic2 = "http://psi.topic.ndla.no/#topic";




              JSONArray topics = aimTopicData.getJSONArray("topics");
              for(int j = 0; j < topics.length(); j++) {
                JSONObject topicRelationData = topics.getJSONObject(j);
                String topicId = topicRelationData.getString("topicId");
                TopicIF topicObject = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicId));
                //Utils.createAssociation(aimTopic, topicObject, site, aimTopicRelation, aimRoleTypeTopic1, aimRoleTypeTopic2, topicMap);
                Utils.createReifiedAssociation(aimTopic,topicObject, aimTopicRelation, aimRoleTypeTopic1, aimRoleTypeTopic2, reifierData, topicMap);
                Utils.createReifiedAssociation(aimTopic,topicObject, primaryTopicRelation, aimRoleTypeTopic1, aimRoleTypeTopic2, reifierData, topicMap);

                String relationTypeId = topicRelationData.getString("relationTypeId");

                String roleTypeId1 = topicRelationData.getString("roleTypeId1");
                String roleTypeId2 = topicRelationData.getString("roleTypeId2");
                //Utils.createAssociation(topicObject,aimTopic, site, relationTypeId, roleTypeId1, roleTypeId2, topicMap);
                if(relationTypeId.length() > 0 && roleTypeId1.length() > 0 && roleTypeId2.length() > 0) {
                  if(!relationTypeId.contains("aim_has_topic")){
                    Utils.createReifiedAssociation(topicObject, aimTopic, relationTypeId, roleTypeId1, roleTypeId2, reifierData, topicMap);
                  }
                }
                result.add(topicObject);
              }
            }
          }
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
              if (e instanceof BadObjectReferenceException) {
                throw new BadObjectReferenceException(e.getMessage());
              } else {
                throw new NdlaServiceException(e.getMessage());
              }
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
          return result;
    }

    synchronized public ArrayList<TopicIF> deleteTopicFromAim(String aimId, String topicId, NdlaSite site) throws NdlaServiceException {
        ArrayList<TopicIF> result = new ArrayList<TopicIF>();

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            LocatorIF baseAddress = store.getBaseAddress();

            TopicIF topic = null;
            TopicIF aim = null;

            try {
                topic = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + topicId));
                aim = (TopicIF) topicMap.getObjectByItemIdentifier(baseAddress.resolveAbsolute("#" + aimId));

                if (null != topic) {
                    String relationType = "http://psi.udir.no/ontologi/aim_has_topic";

                    Utils.deleteAssocsByRole(topic, aim, relationType, topicMap);
                    result.add(topic);
                }
            } catch (MalformedURLException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            }
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return result;
    }

    public ArrayList<NdlaTopic> getAimsByTopicId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException, BadObjectReferenceException {
        String siteIdentifier = site.toString().toLowerCase(); // TODO: Flagged for removal (BK).
        String query = String.format("select $COLUMN from topic-name($COLUMN,$N),aim_has_topic(%s : topic, $COLUMN : kompetansemaal) order by $COLUMN limit %s offset %s?", identifier, Integer.toString(limit), Integer.toString(offset));

        return executeCollectionQuery(query);
    }

    // ***** NODES *****

    synchronized public TopicIF createNewNode(String nodeId, String data, NdlaSite site) throws NdlaServiceException {
        TopicIF nodeTopic = null;

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            TopicMapBuilderIF builder = topicMap.getBuilder();
            LocatorIF baseAddress = store.getBaseAddress();

            String siteUrl = "";
            switch (site) {
                case NDLA:
                    siteUrl = NDLA_SITE_URL;
                    break;
                case DELING:
                    siteUrl = DELING_SITE_URL;
                    break;
                case NYGIV:
                    siteUrl = NYGIV_SITE_URL;
                    break;
            }

            JSONObject payload = new JSONObject(data);
            TopicIF checkDuplicateTopic = null;

            /*
                checks for duplicates, creates new ones, if the node does not exist,
            */

            TMObjectIF duplicateObject = null;
            String checkPsi = siteUrl + "node/" + nodeId;
            String basePsi = baseAddress.getAddress() + "#" + site.toString().toLowerCase() + "node_" + nodeId;

            try {
                checkDuplicateTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(checkPsi));
                if (null == checkDuplicateTopic) {
                    duplicateObject = topicMap.getObjectByItemIdentifier(new URILocator(basePsi));
                }
            } catch (MalformedURLException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            }
            //System.out.println("DUPLICATE ? "+checkDuplicateTopic.toString()+" OR "+duplicateObject.toString());

            if (null == checkDuplicateTopic && null == duplicateObject) {
                nodeTopic = builder.makeTopic();
                nodeTopic.addSubjectIdentifier(new URILocator(siteUrl + "node/" + nodeId));
                nodeTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + site.toString().toLowerCase() + "node_" + nodeId));
                TopicIF type = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + site.toString().toLowerCase()  + "-node"));
                nodeTopic.addType(type);

                JSONObject jsonData = payload.getJSONObject("data");

                JSONArray jsonNames = payload.getJSONArray("names");
                for (int n = 0; n < jsonNames.length(); n++) {
                    JSONObject currName = jsonNames.getJSONObject(n);

                    String titleLang = (String) currName.keys().next();
                    String titleString = currName.getString(titleLang);

                    Utils.createBasename(nodeTopic, titleString, titleLang, topicMap);
                }//end for names

                Iterator dataIter = jsonData.keys();
                while (dataIter.hasNext()) {
                    String dataKey = (String) dataIter.next();
                    switch (dataKey) {
                        case "author":
                            JSONArray authorData = jsonData.getJSONArray(dataKey);
                            for (int j = 0; j < authorData.length(); j++) {
                                JSONObject author = authorData.getJSONObject(j);
                                Iterator auIter = author.keys();
                                while (auIter.hasNext()) {
                                    String nodeAuthorScope = (String) auIter.next();
                                    String nodeAuthor = author.getString(nodeAuthorScope);
                                    TopicIF authorOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#node-author"));
                                    OccurrenceIF authorOcc = builder.makeOccurrence(nodeTopic, authorOccType, nodeAuthor);
                                }
                            }
                            break;
                        case "author_id":
                            JSONArray authorIdData = jsonData.getJSONArray(dataKey);
                            for (int k = 0; k < authorIdData.length(); k++) {
                                JSONObject authorId = authorIdData.getJSONObject(k);
                                Iterator auIdIter = authorId.keys();
                                while (auIdIter.hasNext()) {
                                    String authorScope = (String) auIdIter.next();
                                    String author_id = authorId.getString(authorScope);
                                    // Site occurrence author_url
                                    TopicIF authorUrlOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#node-author-url"));
                                    OccurrenceIF authorUrlOcc = builder.makeOccurrence(nodeTopic, authorUrlOccType, new URILocator(siteUrl + "user/" + author_id));
                                }
                            }
                            break;
                        case "node_type":
                            JSONArray nodeTypeData = jsonData.getJSONArray(dataKey);
                            for (int kn = 0; kn < nodeTypeData.length(); kn++) {
                                JSONObject nodeType = nodeTypeData.getJSONObject(kn);
                                Iterator nodeTypeIter = nodeType.keys();
                                while (nodeTypeIter.hasNext()) {
                                    String nodeTypeScope = (String) nodeTypeIter.next();
                                    String nodeTypeString = nodeType.getString(nodeTypeScope);
                                    // Site occurrence author_url
                                    TopicIF nodeTypeOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#node-type"));
                                    OccurrenceIF nodeTypeOcc = builder.makeOccurrence(nodeTopic, nodeTypeOccType, nodeTypeString);
                                }
                            }
                            break;
                        case "license":
                            JSONArray licenseData = jsonData.getJSONArray(dataKey);
                            for (int l = 0; l < licenseData.length(); l++) {
                                JSONObject license = licenseData.getJSONObject(l);
                                Iterator licenseIter = license.keys();
                                while (licenseIter.hasNext()) {
                                    String nodeLicenseScopeEn = (String) licenseIter.next();
                                    String nodeLicense = license.getString(nodeLicenseScopeEn);

                                    if (!Utils.isEmpty(nodeLicenseScopeEn) && !Utils.isEmpty(nodeLicense)) {
                                        String nodeLicenseScopeNb = "http://psi.oasis-open.org/iso/639/#nob";
                                        TopicIF licenseOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#node-usufruct"));
                                        if (nodeLicense.contains("by")) {
                                            OccurrenceIF licenseOccEn = builder.makeOccurrence(nodeTopic, licenseOccType, new URILocator("http://creativecommons.org/licenses/" + nodeLicense + "/3.0"));
                                            licenseOccEn.addTheme(Utils.getLanguageTopic(nodeLicenseScopeEn, topicMap));
                                            OccurrenceIF licenseOccNb = builder.makeOccurrence(nodeTopic, licenseOccType, new URILocator("http://creativecommons.org/licenses/" + nodeLicense + "/3.0/no"));
                                            licenseOccNb.addTheme(Utils.getLanguageTopic(nodeLicenseScopeNb, topicMap));
                                        } else {
                                            HashMap<String, String> licenseHash = Utils.resolveLicense(nodeLicense);
                                            OccurrenceIF licenseOccEn = builder.makeOccurrence(nodeTopic, licenseOccType, new URILocator(licenseHash.get("en")));
                                            licenseOccEn.addTheme(Utils.getLanguageTopic(nodeLicenseScopeEn, topicMap));
                                            OccurrenceIF licenseOccNb = builder.makeOccurrence(nodeTopic, licenseOccType, new URILocator(licenseHash.get("nb")));
                                            licenseOccNb.addTheme(Utils.getLanguageTopic(nodeLicenseScopeNb, topicMap));
                                        }
                                    }
                                }
                            }
                            break;
                        case "ingress":
                            JSONArray ingressData = jsonData.getJSONArray(dataKey);
                            for (int m = 0; m < ingressData.length(); m++) {
                                JSONObject ingress = ingressData.getJSONObject(m);
                                Iterator ingressIter = ingress.keys();
                                while (ingressIter.hasNext()) {
                                    String nodeIngressScope = (String) ingressIter.next();
                                    String nodeIngress = ingress.getString(nodeIngressScope);

                                    TopicIF ingressOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#node-ingress"));
                                    OccurrenceIF ingressOcc = builder.makeOccurrence(nodeTopic, ingressOccType, nodeIngress);
                                    ingressOcc.addTheme(Utils.getLanguageTopic(nodeIngressScope, topicMap));
                                }
                            }
                            break;
                    }
                }
                store.commit(); // Persist object graph.

            } else {
                throw new NdlaServiceException("The node topic already exists");
            }
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
              System.out.println("ERROR ? "+e.getMessage());
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return nodeTopic;
    }

    synchronized public TopicIF deleteNodeByNodeId(String nodeId, NdlaSite site) throws NdlaServiceException {
        String siteUrl = "";
        switch (site) {
            case NDLA:
                siteUrl = NDLA_SITE_URL;
                break;
            case DELING:
                siteUrl = DELING_SITE_URL;
                break;
            case NYGIV:
                siteUrl = NYGIV_SITE_URL;
                break;
        }

        TopicIF topicToBeDeleted = null;

        TopicMapRepositoryIF repository = XMLConfigSource.getRepository(_configFile);
        TopicMapReferenceIF reference = repository.getReferenceByKey(_topicmapReferenceKey);
        TopicMapStoreIF store = null;
        try {
            store = reference.createStore(false);
            TopicMapIF topicMap = store.getTopicMap(); // Open the topic store (implicitly) and creation of concomitant transaction.

            LocatorIF baseAddress = store.getBaseAddress(); // TODO: Flagged for removal (BK).
            try {
                topicToBeDeleted = topicMap.getTopicBySubjectIdentifier(new URILocator(siteUrl + "node/" + nodeId));
                if (null != topicToBeDeleted) {
                    topicToBeDeleted.remove();
                }
            } catch (MalformedURLException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            }
            store.commit(); // Persist object graph.
        } catch (Exception e) {
            if (store != null) {
                store.abort(); // Roll-back changes and deactivate (top-level) transaction.
                throw new NdlaServiceException(e.getMessage(),e);
            }
        } finally { // Release all topic map engine resources.
            if (store != null) {
                store.close();
            }
            if (repository != null) {
                repository.close();
            }
        }
        return topicToBeDeleted;
    }
  /*
  Handles approved state for existing topics
   */
  synchronized public TopicIF handleApprovedStatus(TopicIF keyTopic, JSONArray data, TopicMapIF _topicMap) throws NdlaServiceException, BadObjectReferenceException {
    try {
      TopicMapBuilderIF builder = _topicMap.getBuilder();

      for (int j = 0; j < data.length(); j++) {
        JSONObject keywordData = data.getJSONObject(j);
        String approved = keywordData.getString("approved");
        String approvedDate = keywordData.getString("approvalDate");
        boolean hasApproved = false;
        String approvedValue = "";

        boolean hasApprovalDate = false;

        Collection<OccurrenceIF> occurrences = keyTopic.getOccurrences();
        for (OccurrenceIF occurrence : occurrences) {
          String occType = occurrence.getType().toString();
          if(occType.contains("approved")) {
            hasApproved = true;
            approvedValue = occurrence.getValue();
          }

          if(occType.contains("approvalDate")) {
            hasApprovalDate = true;
          }
        }//end for

        if(!hasApproved) {
          TopicIF approvedOcctype = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
          builder.makeOccurrence(keyTopic, approvedOcctype, approved);
          if(approved.equals("true")){
            if(approvedDate.length() > 0) {
              TopicIF approvalDateOcctype = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));
              builder.makeOccurrence(keyTopic, approvalDateOcctype, approvedDate);
            }
          }//end if approvedvalue
        } else {
          // the keyword already has an approved stamp
          if(approvedValue.equals("false")){
            if(approved.equals("true")){
              if(!hasApprovalDate){
                if(approvedDate.length() > 0) {
                  java.util.Date date= new java.util.Date();
                  Timestamp currentTimestamp= new Timestamp(date.getTime());
                  TopicIF approvalDateOcctype = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));
                  builder.makeOccurrence(keyTopic, approvalDateOcctype, currentTimestamp.toString());
                }
              }
              else{
                //figure out how to parse a java sql timestamp string
              }
            }
          }
        }//end hasApproved

      }
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage(),e);
    }
    catch (MalformedURLException ue) {
      throw new NdlaServiceException(ue.getMessage());
    }
    return keyTopic;
  }


  /*
    Adds Default type to topics if the default type lacks in the payload when updating
   */

  synchronized  public JSONArray appendDefaultType(JSONArray keywordSet, String type,TopicMapIF _topicMap) throws NdlaServiceException, BadObjectReferenceException{
    try {
      boolean hasDefaultType = false;
      JSONObject defaultType = new JSONObject();
      defaultType.put("typeId",type);

      for (int j = 0; j < keywordSet.length(); j++) {
        JSONObject keywordData = keywordSet.getJSONObject(j);

        JSONArray typeIds = keywordData.getJSONArray("types");

        if (typeIds.length() > 0) {
          for (int l = 0; l < typeIds.length(); l++) {
            JSONObject typeId = typeIds.getJSONObject(l);
            String typeIdString = typeId.getString("typeId");
            if (typeIdString.length() > 0 && typeIdString.equals(type)) {
              hasDefaultType = true;
              break;
            } //end if
          } // end for typeids
        }//end if typeids
        if(!hasDefaultType) {
          typeIds.put(defaultType);
        }
      }//end for

    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage(),e);
    }

    return keywordSet;
  }



  public JSONArray appendProcessStatus(JSONArray keywordSet, String type,TopicMapIF _topicMap, TopicIF existingTopic) throws NdlaServiceException, BadObjectReferenceException{
    try {
      for (int j = 0; j < keywordSet.length(); j++) {
        JSONObject keywordData = keywordSet.getJSONObject(j);
        String topicId = keywordData.getString("topicId");

        if (null != existingTopic) {
          String existingPsi = existingTopic.getSubjectIdentifiers().iterator().next().getAddress();
          HashMap<String,String> existingProcessState = Utils.getProcessState(existingPsi,_topicMap);
          if(existingProcessState.size() > 0){
            keywordData.put("processState",existingProcessState.get("processState"));
          }
          else{
            keywordData.put("processState","0");
          }
        } else{
          if(topicId.length() > 0){
            String psi = "http://psi.topic.ndla.no/keywords/#"+topicId;
            HashMap<String,String> existingProcessState = Utils.getProcessState(psi,_topicMap);
            if(existingProcessState.size() > 0){
              keywordData.put("processState",existingProcessState.get("processState"));
            }
            else{
              keywordData.put("processState","0");
            }
          }
          else{
            if(j == 0) {
              keywordData.put("approved","false");
            }
          }
        }//end if != exisiting
      }//end for
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage(),e);
    }

    return keywordSet;
  }

  /*
    Adds approved data = false for the Indexer for new topics
   */

  synchronized  public JSONArray appendDisapproval(JSONArray keywordSet, String type,TopicMapIF _topicMap, TopicIF existingTopic) throws NdlaServiceException, BadObjectReferenceException{
    String approved = null;
    String existingApproved = null;
    String existingApprovalDate = null;
    try {

      for (int j = 0; j < keywordSet.length(); j++) {
        JSONObject keywordData = keywordSet.getJSONObject(j);
        String topicId = keywordData.getString("topicId");


        if(null != existingTopic){
          String existingPsi = existingTopic.getSubjectIdentifiers().iterator().next().getAddress();
          HashMap<String,String> existingApprovedState = Utils.getApprovedState(existingPsi,_topicMap);
          existingApproved = existingApprovedState.get("approved");
          existingApprovalDate = existingApprovedState.get("approval_date");

          if(null != existingApproved && existingApproved.length() > 0) {
            keywordData.put("approved",existingApproved);
            if(null != existingApprovalDate) {
              keywordData.put("approval_date",existingApprovalDate);
            }
          }
          else{
            keywordData.put("approved","false");
          }
        }
        else{

          if(topicId.length() > 0){
            String psi = "http://psi.topic.ndla.no/keywords/#"+topicId;
            HashMap<String,String> approvedState = Utils.getApprovedState(psi,_topicMap);
            approved = approvedState.get("approved");
            if(null == approved || approved.length() == 0) {
              keywordData.put("approved","false");
            }
            else {
              keywordData.put("approved",approved);
            }
          }
          else{
            if(j == 0) {
              keywordData.put("approved","false");
            }
          }

        }




      }//end for

    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage(),e);
    }

    return keywordSet;
  }

  /*
  Adds additional types to keywords in addition to the default type of keyword.
   */
  synchronized public TopicIF addAdditionalTypes(TopicIF keyTopic, JSONArray data, TopicMapIF _topicMap) throws NdlaServiceException, BadObjectReferenceException {
    try {
      for (int j = 0; j < data.length(); j++) {
        JSONObject keywordData = data.getJSONObject(j);

        JSONArray typeIds = keywordData.getJSONArray("types");

        if (typeIds.length() > 0) {
          for (int l = 0; l < typeIds.length(); l++) {
            JSONObject typeId = typeIds.getJSONObject(l);
            String typeIdString = typeId.getString("typeId");
            if (typeIdString.length() > 0) {
              TopicIF additionalType = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + typeIdString));
              if (null != additionalType) {
                keyTopic.addType(additionalType);
              }

            } //end for
          } // end if typeids
        }
      }
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage(),e);
    }
    catch (MalformedURLException ue) {
      throw new NdlaServiceException(ue.getMessage());
    }
    return keyTopic;
  }

}
