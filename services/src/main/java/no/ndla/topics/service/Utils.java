package no.ndla.topics.service;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.infoset.impl.basic.URILocator;
import net.ontopia.topicmaps.core.*;
import net.ontopia.topicmaps.entry.TopicMapReferenceIF;
import net.ontopia.topicmaps.entry.TopicMapRepositoryIF;
import net.ontopia.topicmaps.entry.XMLConfigSource;
import net.ontopia.topicmaps.query.core.*;
import net.ontopia.topicmaps.query.utils.QueryUtils;
import net.ontopia.utils.StringUtils;
import no.ndla.topics.service.model.NdlaTopic;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.xml.sax.Locator;

import java.net.MalformedURLException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    private static List<String> wcs = Arrays.asList(
            "http://psi.topic.ndla.no/#adjective",
            "http://psi.topic.ndla.no/#verb",
            "http://psi.topic.ndla.no/#noun",
            "http://psi.topic.ndla.no/#pronoun",
            "http://psi.topic.ndla.no/#adverb",
            "http://psi.topic.ndla.no/#determiner",
            "http://psi.topic.ndla.no/#neutral",
            "http://psi.topic.ndla.no/#preposition",
            "http://psi.topic.ndla.no/#conjunction");

    synchronized protected static ArrayList<TopicIF> executeCollectionQuery(String query, final TopicMapIF topicMap) throws NdlaServiceException, BadObjectReferenceException {
        ArrayList<TopicIF> result = new ArrayList<TopicIF>();

        ParsedStatementIF statement;
        QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
        QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

        QueryResultIF tologResult = null;
        try {
            statement = queryProcessor.parse(query);
            if (null != statement) {
                tologResult = ((ParsedQueryIF) statement).execute();
                while (tologResult.next()) {
                    result.add((TopicIF) tologResult.getValue("COLUMN"));
                }
            }
        } catch (BadObjectReferenceException e) {
            throw new BadObjectReferenceException(e.getMessage());
        } catch (InvalidQueryException e) {
            throw new NdlaServiceException(e.getMessage(),e);
        } finally {
            if (null != tologResult) {
                tologResult.close();
            }
        }
        return result;
    }

    synchronized protected static ArrayList<Object> executeObjectCollectionQuery(String query, final TopicMapIF topicMap) throws NdlaServiceException, BadObjectReferenceException {
    ArrayList<Object> result = new ArrayList<Object>();

      ParsedStatementIF statement;
      QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
      QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);
      QueryResultIF tologResult = null;
      try {
        statement = queryProcessor.parse(query);
        if (null != statement) {
          tologResult = ((ParsedQueryIF) statement).execute();
          while (tologResult.next()) {
            result.add(tologResult.getValue("COLUMN"));
          }
        }
      } catch (BadObjectReferenceException e) {
        throw new BadObjectReferenceException(e.getMessage());
      } catch (InvalidQueryException e) {
        throw new NdlaServiceException(e.getMessage(),e);
      } finally {
        if (null != tologResult) {
          tologResult.close();
        }
      }
    return result;
  }

    synchronized protected static TopicIF executeObjectQuery(String query, final TopicMapIF topicMap) throws NdlaServiceException, BadObjectReferenceException {
        TopicIF result = null;

        ParsedStatementIF statement;
        QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
        QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

        QueryResultIF tologResult = null;
        try {
            statement = queryProcessor.parse(query);
            if (null != statement) {
                tologResult = ((ParsedQueryIF) statement).execute();
                while (tologResult.next()) {
                    result = (TopicIF) tologResult.getValue("COLUMN");
                }
            }
        } catch (BadObjectReferenceException e) {
            throw new BadObjectReferenceException(e.getMessage());
        } catch (InvalidQueryException e) {
            throw new NdlaServiceException(e.getMessage(),e);
        } finally {
            if (null != tologResult) {
                tologResult.close();
            }
        }
        return result;
    }

  synchronized protected static ArrayList<String> executeStringCollectionQuery(String query, final TopicMapIF topicMap) throws NdlaServiceException, BadObjectReferenceException {
    ArrayList<String> result = new ArrayList<String>();

    ParsedStatementIF statement;
    QueryProcessorFactoryIF queryProcessorFactory = QueryUtils.getQueryProcessorFactory("tolog");
    QueryProcessorIF queryProcessor = queryProcessorFactory.createQueryProcessor(topicMap, null, null);

    QueryResultIF tologResult = null;
      try {
        statement = queryProcessor.parse(query);
        if (null != statement) {
          tologResult = ((ParsedQueryIF) statement).execute();
          while (tologResult.next()) {
            result.add((String) tologResult.getValue("COLUMN"));
          }
        }
      } catch (BadObjectReferenceException e) {
        throw new BadObjectReferenceException(e.getMessage());
      } catch (InvalidQueryException e) {
        throw new NdlaServiceException(e.getMessage(),e);
      } finally {
        if (null != tologResult) {
          tologResult.close();
        }
      }

    return result;
  }

    public static String makeRandomId(final TopicMapIF topicMap) {
        String id;
        TMObjectIF tmObject;
        LocatorIF base = topicMap.getStore().getBaseAddress();
        long unixTime = System.currentTimeMillis() / 1000L;
        do {
            id = unixTime + StringUtils.makeRandomId(10);
            tmObject = topicMap.getObjectByItemIdentifier(base.resolveAbsolute("#" + id));
        } while (tmObject != null);

        return id;
    }

    public static void createBasename(TopicIF topic, String title, String language, final TopicMapIF topicMap) throws NdlaServiceException {
      try {
        TopicNameIF topicName = topicMap.getBuilder().makeTopicName(topic, title);
        if(language.equals("http://psi.oasis-open.org/iso/639/#eng")) {
          TopicIF en = topicMap.getTopicBySubjectIdentifier(new URILocator(language));
          TopicIF neutral = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#language-neutral"));
          topicName.addTheme(en);
          topicName.addTheme(neutral);
        }
        else{
          TopicIF topiclang = topicMap.getTopicBySubjectIdentifier(new URILocator(language));
          topicName.addTheme(topiclang);
        }
      } catch (MalformedURLException e) {
        throw new NdlaServiceException(e.getMessage(),e);
      }
    }

    public static void createBasename(TopicIF topic, String title, String language, ArrayList<TopicIF> scopes, final TopicMapIF topicMap) throws NdlaServiceException {
      try {
        TopicNameIF topicName = topicMap.getBuilder().makeTopicName(topic, title);
        if(language.equals("http://psi.oasis-open.org/iso/639/#eng")) {
          TopicIF en = topicMap.getTopicBySubjectIdentifier(new URILocator(language));
          TopicIF neutral = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#language-neutral"));
          topicName.addTheme(en);
          topicName.addTheme(neutral);
        }
        else{
          TopicIF topiclang = topicMap.getTopicBySubjectIdentifier(new URILocator(language));
          topicName.addTheme(topiclang);
        }

        for(TopicIF scope : scopes) {
          topicName.addTheme(scope);
        }
      } catch (MalformedURLException e) {
        throw new NdlaServiceException(e.getMessage());
      }
    }

    public static void createBasename(TopicIF topic, String title, String language, String wordClass, final TopicMapIF topicMap) throws NdlaServiceException {
      try {
        TopicNameIF topicName = topicMap.getBuilder().makeTopicName(topic, title);
        if(language.equals("http://psi.oasis-open.org/iso/639/#eng")) {
          TopicIF en = topicMap.getTopicBySubjectIdentifier(new URILocator(language));
          TopicIF neutral = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#language-neutral"));
          TopicIF enWc = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + wordClass));
          topicName.addTheme(en);
          topicName.addTheme(neutral);
          topicName.addTheme(enWc);
        }
        else{
          TopicIF topiclang = topicMap.getTopicBySubjectIdentifier(new URILocator(language));
          TopicIF nbWc = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#" + wordClass));
          topicName.addTheme(topiclang);
          topicName.addTheme(nbWc);
        }
      } catch (MalformedURLException e) {
          throw new NdlaServiceException(e.getMessage(),e);
      }
    }

    public static void createAssociation(TopicIF player1Topic, TopicIF player2Topic, NdlaSite site, String relationType, String player1Type, String player2Type, final TopicMapIF topicMap
    ) throws NdlaServiceException, BadObjectReferenceException { // TODO: The site parameter is superfluous. Flagged for removal.
        try {
            TopicMapBuilderIF builder = topicMap.getBuilder();

            String player1TopicItemId = player1Topic.getItemIdentifiers().iterator().next().getAddress();
            String player1TopicId = player1TopicItemId.substring(player1TopicItemId.lastIndexOf('#') + 1);

            String player2TopicItemId = player2Topic.getItemIdentifiers().iterator().next().getAddress();
            String player2TopicId = player2TopicItemId.substring(player2TopicItemId.lastIndexOf('#') + 1);

            QueryProcessorIF queryProcessor = QueryUtils.getQueryProcessor(topicMap);
            String query = String.format("i\"%s\"(%s : i\"%s\", %s : i\"%s\")?", relationType, player1TopicId, player1Type, player2TopicId, player2Type);

            boolean assocExists = false;
            QueryResultIF tologResult = null;
            try {
                tologResult = queryProcessor.execute(query);
                if (tologResult.next()) {
                    assocExists = true;
                }
            } catch (BadObjectReferenceException e) {
                throw new BadObjectReferenceException(e.getMessage());
            } catch (InvalidQueryException e) {
                throw new NdlaServiceException(e.getMessage(),e);
            } finally {
                if (null != tologResult) {
                    tologResult.close();
                }
            }

            if (!assocExists) {
                TopicIF player1RoleTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(player1Type));
                TopicIF player2RoleTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(player2Type));
                TopicIF assocTypeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(relationType));
                AssociationIF association = builder.makeAssociation(assocTypeTopic);

                /* Make the player2 related to the player1 with the role of roleplayer2 */
                builder.makeAssociationRole(association, player2RoleTopic, player2Topic);

                /* Make the player1 related to the player2 with the role of roleplayer1 */
                builder.makeAssociationRole(association, player1RoleTopic, player1Topic);
            }//end if assocExists
        } catch (MalformedURLException | UniquenessViolationException e) {
            throw new NdlaServiceException(e.getMessage(),e);
        }
    }

  public static void createReifiedAssociation(TopicIF player1Topic, TopicIF player2Topic, String relationType, String player1Type, String player2Type, HashMap<String, String> reifiers, final TopicMapIF topicMap
  ) throws NdlaServiceException, BadObjectReferenceException {
    try {
      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = topicMap.getStore().getBaseAddress();

      String player1TopicItemId = player1Topic.getItemIdentifiers().iterator().next().getAddress();
      String player1TopicId = player1TopicItemId.substring(player1TopicItemId.lastIndexOf('#') + 1);

      String player2TopicItemId = player2Topic.getItemIdentifiers().iterator().next().getAddress();
      String player2TopicId = player2TopicItemId.substring(player2TopicItemId.lastIndexOf('#') + 1);

      QueryProcessorIF queryProcessor = QueryUtils.getQueryProcessor(topicMap);

      String reifierValue = "";

      if(null != reifiers.get("http://psi.topic.ndla.no/#udir-subject")){
        reifierValue = reifiers.get("http://psi.topic.ndla.no/#udir-subject");
      }
      else if(null != reifiers.get("http://psi.topic.ndla.no/#ontology_topic_domain_type")){
        reifierValue = reifiers.get("http://psi.topic.ndla.no/#ontology_topic_domain_type");
      }
      String query = String.format("select $COLUMN from udir-subject($REIFIER,\"%s\"),reifies($REIFIER,$COLUMN),\n" +
          "  role-player($ROLE1, %s),\n" +
          "  association-role($COLUMN, $ROLE1),\n" +
          "  association-role($COLUMN, $ROLE2),\n" +
          "  role-player($ROLE2, %s), type($COLUMN,i\"%s\")?", reifierValue, player1TopicId, player2TopicId, relationType);
      boolean assocExists = false;
      QueryResultIF tologResult = null;
      try {
        tologResult = queryProcessor.execute(query);

        if (tologResult.next()){
          AssociationIF assoc = (AssociationIF) tologResult.getValue("COLUMN");
          TreeMap<String,String> gotRoles = new TreeMap<>();
          int length = tologResult.getValues().length;
          if(length > 0){

            Collection<AssociationRoleIF> roles = assoc.getRoles();
            for(AssociationRoleIF role : roles){
              TopicIF roleType = role.getType();
              Collection<LocatorIF> roleTypePSIs = roleType.getSubjectIdentifiers();
              String roleTypePSIString = "";
              if(roleTypePSIs.size() > 1){
                int rolePsiCounter = 0;
                for(LocatorIF rPSI : roleTypePSIs){
                  if(rolePsiCounter == 0){
                    roleTypePSIString = rPSI.getAddress();
                  }
                }
              }
              else{
                roleTypePSIString = roleTypePSIs.iterator().next().getAddress();
              }

              TopicIF rolePlayer = role.getPlayer();
              Collection<LocatorIF> playerPSIs = rolePlayer.getSubjectIdentifiers();
              String rolePlayerPSIString = "";
              if(playerPSIs.size() > 1){
                int rolePsiCounter = 0;
                for(LocatorIF pPSI : playerPSIs){
                  if(rolePsiCounter == 0){
                    rolePlayerPSIString = pPSI.getAddress();
                  }
                }
              }
              else{
                rolePlayerPSIString = playerPSIs.iterator().next().getAddress();
              }
              String rolePlayerString = "";
              if(rolePlayerPSIString.contains("#")){
                rolePlayerString = rolePlayerPSIString.substring(rolePlayerPSIString.lastIndexOf("#")+1);
              }
              else{
                rolePlayerString = rolePlayerPSIString.substring(rolePlayerPSIString.lastIndexOf("/")+1);
              }
              gotRoles.put(roleTypePSIString,rolePlayerString);
            }

            if(gotRoles.get(player1Type).equals(player1TopicId) && gotRoles.get(player2Type).equals(player2TopicId)){
              assocExists = true;
            }


          }
        }

      } catch (BadObjectReferenceException e) {
        throw new BadObjectReferenceException(e.getMessage());
      } catch (InvalidQueryException e) {
        throw new NdlaServiceException(e.getMessage());
      } finally {
        if (null != tologResult) {
          tologResult.close();
        }
      }

      if (!assocExists) {

        TopicIF player1RoleTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(player1Type));
        TopicIF player2RoleTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(player2Type));
        TopicIF assocTypeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(relationType));
        AssociationIF association = builder.makeAssociation(assocTypeTopic);

                /* Make the player2 related to the player1 with the role of roleplayer2 */
        builder.makeAssociationRole(association, player2RoleTopic, player2Topic);

                /* Make the player1 related to the player2 with the role of roleplayer1 */
        builder.makeAssociationRole(association, player1RoleTopic, player1Topic);
        TopicIF reifyingTopic = builder.makeTopic();
        String topicId = "reifiedAssociation-"+makeRandomId(topicMap);
        reifyingTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/reifiers/#" + topicId));
        reifyingTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));
        createBasename(reifyingTopic, "Reifier for "+topicId, "http://psi.oasis-open.org/iso/639/#eng", topicMap);
        createBasename(reifyingTopic, "Reifikator for "+topicId, "http://psi.oasis-open.org/iso/639/#nob", topicMap);
        createBasename(reifyingTopic, "Reifikator for "+topicId, "http://psi.oasis-open.org/iso/639/#nno", topicMap);

        Iterator<String> it = reifiers.keySet().iterator();
        while(it.hasNext()) {
          String typePsi = (String) it.next();
          if(typePsi.contains("udir-subject")) {
            TopicIF udirOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#udir-subject"));
            builder.makeOccurrence(reifyingTopic, udirOccType, new URILocator(reifiers.get(typePsi)));
          }
          else if(typePsi.contains("subjectmatter")){
            TopicIF subjectOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#subjectmatter"));
            builder.makeOccurrence(reifyingTopic, subjectOccType, new URILocator(reifiers.get(typePsi)));
          }
          else if(typePsi.contains("ontology_topic_relationship_type")){
            TopicIF subjectTopicOccType = topicMap.getTopicBySubjectIdentifier(new URILocator(typePsi));
            builder.makeOccurrence(reifyingTopic, subjectTopicOccType, new URILocator(reifiers.get(typePsi)));
          }
          else if(typePsi.contains("ontology_topic_domain_type")){
            TopicIF subjectTopicOccType = topicMap.getTopicBySubjectIdentifier(new URILocator(typePsi));
            builder.makeOccurrence(reifyingTopic, subjectTopicOccType, new URILocator(reifiers.get(typePsi)));
          }

        }//end while

        association.setReifier(reifyingTopic);
      }//end if assocExists
    } catch (MalformedURLException | UniquenessViolationException e) {
      throw new NdlaServiceException(e.getMessage());
    }
  }

  public static void createReifiedAssociation(TopicIF player1Topic, TopicIF player2Topic, String relationType, String player1Type, String player2Type, HashMap<String, String> reifiers,  boolean assocExists, final TopicMapIF topicMap
  ) throws NdlaServiceException, BadObjectReferenceException {
    try {
      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = topicMap.getStore().getBaseAddress();

      String player1TopicItemId = player1Topic.getItemIdentifiers().iterator().next().getAddress();
      String player1TopicId = player1TopicItemId.substring(player1TopicItemId.lastIndexOf('#') + 1);

      String player2TopicItemId = player2Topic.getItemIdentifiers().iterator().next().getAddress();
      String player2TopicId = player2TopicItemId.substring(player2TopicItemId.lastIndexOf('#') + 1);



      if (!assocExists) {
        TopicIF player1RoleTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(player1Type));
        TopicIF player2RoleTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(player2Type));
        TopicIF assocTypeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(relationType));
        AssociationIF association = builder.makeAssociation(assocTypeTopic);

                /* Make the player2 related to the player1 with the role of roleplayer2 */
        builder.makeAssociationRole(association, player2RoleTopic, player2Topic);

                /* Make the player1 related to the player2 with the role of roleplayer1 */
        builder.makeAssociationRole(association, player1RoleTopic, player1Topic);
        TopicIF reifyingTopic = builder.makeTopic();
        String topicId = "reifiedAssociation-"+makeRandomId(topicMap);
        reifyingTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/reifiers/#" + topicId));
        reifyingTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));
        createBasename(reifyingTopic, "Reifier for "+topicId, "http://psi.oasis-open.org/iso/639/#eng", topicMap);
        createBasename(reifyingTopic, "Reifikator for "+topicId, "http://psi.oasis-open.org/iso/639/#nob", topicMap);
        createBasename(reifyingTopic, "Reifikator for "+topicId, "http://psi.oasis-open.org/iso/639/#nno", topicMap);

        Iterator<String> it = reifiers.keySet().iterator();
        while(it.hasNext()) {
          String typePsi = (String) it.next();
          if(typePsi.contains("udir-subject")) {
            TopicIF udirOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#udir-subject"));
            builder.makeOccurrence(reifyingTopic, udirOccType, new URILocator(reifiers.get(typePsi)));
          }
          else if(typePsi.contains("subjectmatter")){
            TopicIF subjectOccType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#subjectmatter"));
            builder.makeOccurrence(reifyingTopic, subjectOccType, new URILocator(reifiers.get(typePsi)));
          }
          else if(typePsi.contains("ontology_topic_relationship_type")){
            TopicIF subjectTopicOccType = topicMap.getTopicBySubjectIdentifier(new URILocator(typePsi));
            builder.makeOccurrence(reifyingTopic, subjectTopicOccType, new URILocator(reifiers.get(typePsi)));
          }
          else if(typePsi.contains("ontology_topic_domain_type")){
            TopicIF subjectTopicOccType = topicMap.getTopicBySubjectIdentifier(new URILocator(typePsi));
            builder.makeOccurrence(reifyingTopic, subjectTopicOccType, new URILocator(reifiers.get(typePsi)));
          }

        }//end while

        association.setReifier(reifyingTopic);
      }//end if assocExists
    } catch (MalformedURLException | UniquenessViolationException e) {
      throw new NdlaServiceException(e.getMessage());
    }
  }


  public static void createReifiedAssociationOntologyTopics(TopicIF player1Topic, TopicIF player2Topic, String relationType, String player1Type, String player2Type, HashMap<String, String> reifiers, final TopicMapIF topicMap
  ) throws NdlaServiceException, BadObjectReferenceException {
    try {
      TopicMapBuilderIF builder = topicMap.getBuilder();
      LocatorIF baseAddress = topicMap.getStore().getBaseAddress();

      String player1TopicItemId = player1Topic.getItemIdentifiers().iterator().next().getAddress();
      String player1TopicId = player1TopicItemId.substring(player1TopicItemId.lastIndexOf('#') + 1);

      String player2TopicItemId = player2Topic.getItemIdentifiers().iterator().next().getAddress();
      String player2TopicId = player2TopicItemId.substring(player2TopicItemId.lastIndexOf('#') + 1);

      QueryProcessorIF queryProcessor = QueryUtils.getQueryProcessor(topicMap);

      Iterator rit = reifiers.keySet().iterator();
      String query = "select $COLUMN from";
      boolean assocExists = false;
      while (rit.hasNext()){
        String reifierKey = (String) rit.next();
        String occurrenceType = reifierKey.substring(reifierKey.lastIndexOf("#") + 1);
        String reifierValue = reifiers.get(reifierKey);
        query += " "+occurrenceType+"($REIFIER,\""+reifierValue+"\"),reifies($REIFIER,$COLUMN),";
      }
      query += "  role-player($ROLE1, "+player1TopicId+"),\n" +
          "  association-role($COLUMN, $ROLE1),\n" +
          "  association-role($COLUMN, $ROLE2),\n" +
          "  role-player($ROLE2, "+player2TopicId+"), type($COLUMN,i\""+relationType+"\")?";

      QueryResultIF tologResult = null;
      try {
        tologResult = queryProcessor.execute(query);
        if (tologResult.next()){
          AssociationIF assoc = (AssociationIF) tologResult.getValue("COLUMN");
          TreeMap<String,String> gotRoles = new TreeMap<>();
          int length = tologResult.getValues().length;
          if(length > 0){
            Collection<AssociationRoleIF> roles = assoc.getRoles();
            for(AssociationRoleIF role : roles){
              TopicIF roleType = role.getType();
              Collection<LocatorIF> roleTypePSIs = roleType.getSubjectIdentifiers();
              String roleTypePSIString = "";
              if(roleTypePSIs.size() > 1){
                int rolePsiCounter = 0;
                for(LocatorIF rPSI : roleTypePSIs){
                  if(rolePsiCounter == 0){
                    roleTypePSIString = rPSI.getAddress();
                  }
                }
              }
              else{
                roleTypePSIString = roleTypePSIs.iterator().next().getAddress();
              }

              TopicIF rolePlayer = role.getPlayer();
              Collection<LocatorIF> playerPSIs = rolePlayer.getSubjectIdentifiers();
              String rolePlayerPSIString = "";
              if(playerPSIs.size() > 1){
                int rolePsiCounter = 0;
                for(LocatorIF pPSI : playerPSIs){
                  if(rolePsiCounter == 0){
                    rolePlayerPSIString = pPSI.getAddress();
                  }
                }
              }
              else{
                rolePlayerPSIString = playerPSIs.iterator().next().getAddress();
              }
              String rolePlayerString = "";
              if(rolePlayerPSIString.contains("#")){
                rolePlayerString = rolePlayerPSIString.substring(rolePlayerPSIString.lastIndexOf("#")+1);
              }
              else{
                rolePlayerString = rolePlayerPSIString.substring(rolePlayerPSIString.lastIndexOf("/")+1);
              }
              gotRoles.put(roleTypePSIString,rolePlayerString);
            }

            if(gotRoles.get(player1Type).equals(player1TopicId) && gotRoles.get(player2Type).equals(player2TopicId)){
              assocExists = true;
            }
          }//end if length > 0?
        }//end if tologResultHasnext

      } catch (BadObjectReferenceException e) {
        throw new BadObjectReferenceException(e.getMessage());
      } catch (InvalidQueryException e) {
        throw new NdlaServiceException(e.getMessage());
      } finally {
        if (null != tologResult) {
          tologResult.close();
        }
      }

      if (!assocExists) {
        TopicIF player1RoleTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(player1Type));
        TopicIF player2RoleTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(player2Type));
        TopicIF assocTypeTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(relationType));
        AssociationIF association = builder.makeAssociation(assocTypeTopic);

                /* Make the player2 related to the player1 with the role of roleplayer2 */
        builder.makeAssociationRole(association, player2RoleTopic, player2Topic);

                /* Make the player1 related to the player2 with the role of roleplayer1 */
        builder.makeAssociationRole(association, player1RoleTopic, player1Topic);
        TopicIF reifyingTopic = builder.makeTopic();
        String topicId = "reifiedAssociation-"+makeRandomId(topicMap);
        reifyingTopic.addSubjectIdentifier(new URILocator("http://psi.topic.ndla.no/reifiers/#" + topicId));
        reifyingTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + topicId));
        createBasename(reifyingTopic, "Reifier for "+topicId, "http://psi.oasis-open.org/iso/639/#eng", topicMap);
        createBasename(reifyingTopic, "Reifikator for "+topicId, "http://psi.oasis-open.org/iso/639/#nob", topicMap);
        createBasename(reifyingTopic, "Reifikator for "+topicId, "http://psi.oasis-open.org/iso/639/#nno", topicMap);


        Iterator<String> it = reifiers.keySet().iterator();
        while(it.hasNext()) {
          String typePsi = (String) it.next();
          TopicIF subjectTopicOccType = topicMap.getTopicBySubjectIdentifier(new URILocator(typePsi));
          builder.makeOccurrence(reifyingTopic, subjectTopicOccType, new URILocator(reifiers.get(typePsi)));
        }//end while

        association.setReifier(reifyingTopic);
      }//end if assocExists
    } catch (MalformedURLException | UniquenessViolationException e) {
      throw new NdlaServiceException(e.getMessage());
    }
  }

    public static JSONArray hasTopicId(JSONArray associations, String topicId)throws NdlaServiceException, BadObjectReferenceException{
      JSONArray relatedTopics = new JSONArray();
      try {
        for (int i = 0; i < associations.length(); i++) {
          JSONObject associationObject = associations.getJSONObject(i);
          String associationRolePlayerId = associationObject.getString("topicId").toString().trim();
          associationRolePlayerId = associationRolePlayerId.substring(associationRolePlayerId.lastIndexOf("#")+1);
          if(associationRolePlayerId.equals(topicId)) {
            relatedTopics.put(associationObject);
          }
        }
      } catch (JSONException e) {
        throw new NdlaServiceException(e.getMessage());
      }

      return relatedTopics;
    }

    public static boolean isEmpty(String value) {
        boolean isEmpty = true;
        if (value.length() > 0) {
            isEmpty = false;
        }

        if (false) {
            String regEx = "\\s*"; // A whitespace character: [ \t\n\x0B\f\r]
            Pattern pattern;
            pattern = Pattern.compile(regEx);
            Matcher matcher = pattern.matcher(value);
            if (matcher.matches()) {
                isEmpty = true;
            }
        }
        return isEmpty;
    }

    public static String determineLanguage(String value) {
        String result = "";

        /*
        Pattern neutralPattern = Pattern.compile(NEUTRAL_PATTERN);
        Pattern englishPattern = Pattern.compile(ENGLISH_PATTERN);
        Pattern bokmalPattern = Pattern.compile(BOKMAL_PATTERN);
        Pattern nynorskPattern = Pattern.compile(NYNORSK_PATTERN);

        Matcher matcher;
        */
        if (value.equals("http://psi.oasis-open.org/iso/639/#eng") || value.equals("en")) {
            result = "en";
        } else if (value.equals("http://psi.oasis-open.org/iso/639/#nob") || value.equals("nb")) {
            result = "nb";
        } else if (value.equals("http://psi.oasis-open.org/iso/639/#nno") || value.equals("nn")) {
            result = "nn";
        }
        return result;
    }


  public static HashMap<String,String> getProcessState(String psi, TopicMapIF _topicMap) throws NdlaServiceException, BadObjectReferenceException{
    HashMap<String,String> processMap = new HashMap<>();

    try {
      TopicIF topic = _topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
      if(null != topic) {
        String topicId = psi.substring(psi.lastIndexOf("#")+1);

        Collection<OccurrenceIF> occurrences = topic.getOccurrences();
        for (OccurrenceIF occurrence : occurrences) {
          String occType = occurrence.getType().toString();
          if(occType.contains("process-state")){
            processMap.put("processState",occurrence.getValue());
            break;
          }
        }
      }//end if topic exists


    } catch (Exception e) {
      throw new NdlaServiceException(e.getMessage(),e);
    }

    return processMap;
  }

  public static HashMap<String,String> getProcessState(String psi, String _configFile, String _topicmapReferenceKey) throws NdlaServiceException, BadObjectReferenceException{

    HashMap<String,String> processMap = new HashMap<>();

    TopicMapStoreIF store = null;
    TopicMapRepositoryIF repository = null;
    TopicMapReferenceIF reference = null;

    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      TopicIF topic = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
      if(null != topic) {
        String topicId = psi.substring(psi.lastIndexOf("#")+1);

        Collection<OccurrenceIF> occurrences = topic.getOccurrences();
        for (OccurrenceIF occurrence : occurrences) {
          String occType = occurrence.getType().toString();
          if(occType.contains("process-state")){
            processMap.put("processState",occurrence.getValue());
            break;
          }
        }
      }//end if topic exists
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(), e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return processMap;
  }

  public static HashMap<String,String> getApprovedState(String psi, String _configFile, String _topicmapReferenceKey) throws NdlaServiceException, BadObjectReferenceException{
    HashMap<String,String> approvedMap = new HashMap<>();
    TopicMapStoreIF store = null;
    TopicMapRepositoryIF repository = null;
    TopicMapReferenceIF reference = null;

    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      TopicIF topic = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
      if(null != topic) {
        String topicId = psi.substring(psi.lastIndexOf("#")+1);

        Collection<OccurrenceIF> occurrences = topic.getOccurrences();
        for (OccurrenceIF occurrence : occurrences) {
          String occType = occurrence.getType().toString();
          if(occType.contains("approved")){
            approvedMap.put("approved",occurrence.getValue());
          }
          if(occType.contains("approval-date")){
            approvedMap.put("approval_date",occurrence.getValue());
          }
        }
      }//end if topic exists
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(), e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }

    return approvedMap;
  }

  public static HashMap<String,String> getApprovedState(String psi, TopicMapIF _topicMap) throws NdlaServiceException, BadObjectReferenceException{
    HashMap<String,String> approvedMap = new HashMap<>();

    try {
      TopicIF topic = _topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
      if(null != topic) {
        String topicId = psi.substring(psi.lastIndexOf("#")+1);

        Collection<OccurrenceIF> occurrences = topic.getOccurrences();
        for (OccurrenceIF occurrence : occurrences) {
          String occType = occurrence.getType().toString();
          if(occType.contains("approved")){
            approvedMap.put("approved",occurrence.getValue());
          }
          if(occType.contains("approval-date")){
            approvedMap.put("approval_date",occurrence.getValue());
          }
        }
      }//end if topic exists


    } catch (Exception e) {
      throw new NdlaServiceException(e.getMessage(),e);
    }

    return approvedMap;
  }

  public static JSONArray buildTopicIndexJSONArray(String psi, String additionalPsi, TopicMapIF _topicMap) throws NdlaServiceException, BadObjectReferenceException {
    JSONArray topicSet = new JSONArray();
    JSONObject jsonTopic = new JSONObject();
    boolean getVisibility = false;
    boolean getWordClass = false;

    try{
      TopicIF topic = _topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
      if(null != topic) {
        String topicId = psi.substring(psi.lastIndexOf("#")+1);
        String visibility = "";
        if(topicId.contains("keyword-")){
          getVisibility = true;
          getWordClass = true;
          topicId = additionalPsi.substring(additionalPsi.lastIndexOf("#")+1);
        }
        jsonTopic.put("topicId",topicId);
        if(additionalPsi != ""){
          jsonTopic.put("psi",additionalPsi);
        }
        else{
          jsonTopic.put("psi",psi);
          getWordClass = true;
        }

        Collection<OccurrenceIF> occurrences = topic.getOccurrences();
        JSONArray jsonDescriptions = new JSONArray();
        JSONArray jsonImages = new JSONArray();
        JSONArray jsonTypes = new JSONArray();

        for (OccurrenceIF occurrence : occurrences) {
          String occType = occurrence.getType().toString();

          if(getVisibility && occType.contains("keyword-visibility")){
            visibility = occurrence.getValue();
            jsonTopic.put("visibility",visibility);
          }


          if(occType.contains("description")){
            Collection<TopicIF> descriptionScopes = occurrence.getScope();
            for (TopicIF descriptionScope : descriptionScopes) {
              Collection<LocatorIF> descriptionScopeSubjectIdentifiers = descriptionScope.getSubjectIdentifiers();
              for (LocatorIF descriptionScopeSubjectIdentifier : descriptionScopeSubjectIdentifiers) {
                if (descriptionScopeSubjectIdentifier.getAddress().contains("#")) {
                  JSONObject tempDesc = new JSONObject();
                  tempDesc.put("language",descriptionScopeSubjectIdentifier.getAddress());
                  tempDesc.put("content",occurrence.getValue());
                  jsonDescriptions.put(tempDesc);
                }
              }
            }
          }

          if(occType.contains("image")) {
            Collection<TopicIF> imageScopes = occurrence.getScope();
            if(imageScopes.size() > 0){
              for (TopicIF imageScope : imageScopes) {
                Collection<LocatorIF> imageScopeSubjectIdentifiers = imageScope.getSubjectIdentifiers();
                for (LocatorIF imageScopeSubjectIdentifier : imageScopeSubjectIdentifiers) {
                  if (imageScopeSubjectIdentifier.getAddress().contains("#")) {
                    JSONObject tempImg = new JSONObject();
                    tempImg.put("language",imageScopeSubjectIdentifier.getAddress());
                    tempImg.put("url",occurrence.getValue());
                    jsonImages.put(tempImg);
                  }
                }
              }
            }
          }

        }//end for occurrences

        Collection<TopicIF> topicTypes = topic.getTypes();
        String wordClass = "";

        for (TopicIF topicType : topicTypes) {
          Collection<LocatorIF> topicTypeIds = topicType.getSubjectIdentifiers();
          String topicTypeId = "";
          for (LocatorIF topicTypeLocator : topicTypeIds) {
            String loc = topicTypeLocator.getAddress();
            if (loc.contains("#") && !loc.equals("http://psi.topic.ndla.no/#topic") && !loc.equals("http://psi.topic.ndla.no/#keyword")) {
              topicTypeId = loc.substring(loc.lastIndexOf("#") + 1);
              JSONObject jsonType = new JSONObject();
              jsonType.put("typeId",topicTypeId);
              TopicIF typeTopic = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#"+topicTypeId));
              if(null != typeTopic && !topicTypeId.equals("keyword") && !topicTypeId.equals("topic")){
                Collection<TopicNameIF> typeNames = typeTopic.getTopicNames();
                JSONArray typeNameArray = new JSONArray();

                if (typeNames.size() > 0) {
                  for (TopicNameIF typeName : typeNames) {
                    JSONObject typeNameObject = new JSONObject();
                    Collection<TopicIF> typeScopes = typeName.getScope();
                    for (TopicIF typeScope : typeScopes) {
                      Collection<LocatorIF> typeScopeLocators = typeScope.getSubjectIdentifiers();
                      for (LocatorIF typeScopeLocator : typeScopeLocators) {
                        if (typeScopeLocator.getAddress().contains("#") && typeScopeLocator.getAddress().contains("iso") && !typeScopeLocator.getAddress().contains("language-neutral")) {
                          typeNameObject.put(typeScopeLocator.getAddress(),typeName.getValue());
                        }//end if hash so we don't run into the few subjectIdentifiers with no hash
                      }//end for locators
                    } //end for scopes
                    typeNameArray.put(typeNameObject);
                  }//end for names
                }//end if typenames
                jsonType.put("names",typeNameArray);
              }
              jsonTypes.put(jsonType);
            }
          } //end locator forloop
        }//end topictypes


        JSONArray jsonNames = new JSONArray();
        Collection<TopicNameIF> topicNames = topic.getTopicNames();

        for (TopicNameIF topicName : topicNames) {
          JSONObject jsonName = new JSONObject();
          Collection<TopicIF> scopes = topicName.getScope();
          for (TopicIF scope : scopes) {
            Collection<LocatorIF> locators = scope.getSubjectIdentifiers();
            for (LocatorIF locator : locators) {
              String locatorAddress = locator.getAddress();
              if (locatorAddress.contains("#") && !locatorAddress.contains("language-neutral")) {
                if(locatorAddress.contains("iso")) {
                  jsonName.put("name", topicName.getValue());
                  jsonName.put("language", locatorAddress);
                  jsonNames.put(jsonName);
                }

                if(wcs.contains(locatorAddress)){
                  wordClass = locatorAddress.substring(locatorAddress.lastIndexOf("#")+1);
                }
              }
            }
          }
        }//end for topicnames

        jsonTopic.put("wordclass",wordClass);
        jsonTopic.put("names",jsonNames);
        jsonTopic.put("descriptions",jsonDescriptions);
        jsonTopic.put("images",jsonImages);
        jsonTopic.put("types",jsonTypes);



        HashMap<String,String> processState = Utils.getProcessState(psi,_topicMap);
        String processStateValue = "";
        if(processState.size() > 0){
          processStateValue = processState.get("processState");
        }
        else{
          processStateValue = "0";
        }
        jsonTopic.put("processState",processStateValue);


        topicSet.put(jsonTopic);
      }
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage());
    } catch (MalformedURLException | UniquenessViolationException e) {
      throw new NdlaServiceException(e.getMessage());
    }
    return topicSet;
  }

  public static JSONArray buildIndexJSONArray(String psi, TopicMapIF _topicMap) throws NdlaServiceException, BadObjectReferenceException{

    JSONArray keywordSet = new JSONArray();
    JSONObject jsonTopic = new JSONObject();

    ArrayList<String> wcs = new ArrayList<>();
    wcs.add("http://psi.topic.ndla.no/#verb");
    wcs.add("http://psi.topic.ndla.no/#noun");
    wcs.add("http://psi.topic.ndla.no/#adverb");
    wcs.add("http://psi.topic.ndla.no/#adjective");
    wcs.add("http://psi.topic.ndla.no/#preposition");
    wcs.add("http://psi.topic.ndla.no/#pronoun");
    wcs.add("http://psi.topic.ndla.no/#conjunction");
    wcs.add("http://psi.topic.ndla.no/#determiner");
    wcs.add("http://psi.topic.ndla.no/#neutral");

    try {
      TopicIF topic = _topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
      if(null != topic) {
        String topicId = psi.substring(psi.lastIndexOf("#")+1);
        jsonTopic.put("topicId",topicId);
        Collection<OccurrenceIF> occurrences = topic.getOccurrences();
        for (OccurrenceIF occurrence : occurrences) {
          String occType = occurrence.getType().toString();
          if(occType.contains("keyword-visibility")){
            jsonTopic.put("visibility",occurrence.getValue());
            break;
          }
        }

        JSONArray jsonNames = new JSONArray();
        Collection<TopicNameIF> topicNames = topic.getTopicNames();
        String wordClass = "";
        for (TopicNameIF topicName : topicNames) {
          JSONObject jsonName = new JSONObject();
          Collection<TopicIF> scopes = topicName.getScope();
          for (TopicIF scope : scopes) {
            Collection<LocatorIF> locators = scope.getSubjectIdentifiers();
            for (LocatorIF locator : locators) {
              String locatorAddress = locator.getAddress();
              if (locatorAddress.contains("#") && !locatorAddress.contains("language-neutral")) {
                if(locatorAddress.contains("iso")) {
                  jsonName.put("keyword", topicName.getValue());
                  jsonName.put("language", locatorAddress);
                  jsonNames.put(jsonName);
                }
                if(wcs.contains(locatorAddress)){
                  wordClass = locatorAddress.substring(locatorAddress.lastIndexOf("#")+1);
                }
              }
            }
          }
        }//end for topicnames

        jsonTopic.put("wordclass",wordClass);
        jsonTopic.put("names",jsonNames);

        Collection<TopicIF> topicTypes = topic.getTypes();
        JSONArray jsonTopicTypes = new JSONArray();
        for (TopicIF topicType : topicTypes) {
          Collection<LocatorIF> topicTypeIds = topicType.getSubjectIdentifiers();
          String topicTypeId = "";
          for (LocatorIF topicTypeLocator : topicTypeIds) {
            String loc = topicTypeLocator.getAddress();
            if (loc.contains("#") && !loc.equals("http://psi.topic.ndla.no/#keyword") && !loc.equals("http://psi.topic.ndla.no/#topic")) {
              topicTypeId = loc.substring(loc.lastIndexOf("#") + 1);
              JSONObject jsonType = new JSONObject();
              jsonType.put("typeId",topicTypeId);
              jsonTopicTypes.put(jsonType);
            }
          } //end locator forloop
        }//end topictypes

        jsonTopic.put("types",jsonTopicTypes);
        keywordSet.put(jsonTopic);
      }//end if topic exists
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage(),e);
    } catch (MalformedURLException | UniquenessViolationException e) {
      throw new NdlaServiceException(e.getMessage(),e);
    }
    return keywordSet;
  }

    public static JSONArray indexPayload(JSONObject payload, String siteUrl, TopicMapIF _topicMap) throws NdlaServiceException {
        JSONArray output = new JSONArray();
        Iterator it = payload.keys();

        try {
            while (it.hasNext()) {
                String topicId = (String) it.next();

                JSONObject indexObject = new JSONObject();
                indexObject.put("topicId", topicId);
                indexObject.put("psi", "http://psi.topic.ndla.no/keywords/#" + topicId);
                if (!siteUrl.equals("")) {
                    indexObject.put("originatingSite", siteUrl);
                }
                JSONArray indexObjectNames = new JSONArray();
                JSONArray indexTypeData = new JSONArray();

                JSONArray keywordset = payload.getJSONArray(topicId);
                for (int i = 0; i < keywordset.length(); i++) {
                    JSONObject keyWordObject = keywordset.getJSONObject(i);

                    if (i == 0) {
                        indexObject.put("visibility", keyWordObject.get("visibility"));
                        if(keyWordObject.has("approved")){
                          String approved = keyWordObject.getString("approved");
                          if(approved.length() > 0){
                            if(approved.equals("true")) {
                              indexObject.put("approved", approved);
                              String approval_date = keyWordObject.getString("approval_date");
                              if( approval_date.length() > 0){
                                indexObject.put("approval_date",approval_date);
                              }

                            }
                            else{
                              indexObject.put("approved", "false");
                            }
                          }
                          else{
                            indexObject.put("approved", "false");
                          }
                        }
                        else{
                          HashMap<String,String> approvedState = Utils.getApprovedState("http://psi.topic.ndla.no/keywords/#" + topicId,_topicMap);
                          String approved = approvedState.get("approved");
                          String approval_date = approvedState.get("approval_date");
                          if(approved.equals("true")){
                            indexObject.put("approved", approved);
                            if( approval_date.length() > 0){
                              indexObject.put("approval_date",approval_date);
                            }
                          }
                          else{
                            indexObject.put("approved", "false");
                          }
                        }

                      if(keyWordObject.has("processState")){
                        String processState = keyWordObject.getString("processState");
                        indexObject.put("processState",processState);
                      }
                      else{
                        HashMap<String,String> processState = Utils.getProcessState("http://psi.topic.ndla.no/keywords/#" + topicId,_topicMap);
                        String processStateValue = "";
                        if(processState.size() > 0){
                          processStateValue = processState.get("processState");
                        }
                        else{
                          processStateValue = "0";
                        }
                        indexObject.put("processState",processStateValue);
                      }

                    }

                    JSONObject indexObjectData = new JSONObject();
                    indexObjectData.put("wordclass", keyWordObject.get("wordclass"));
                    JSONArray indexNameData = new JSONArray();

                    JSONArray keyWordNames = keyWordObject.getJSONArray("names");
                    for (int j = 0; j < keyWordNames.length(); j++) {
                        JSONObject keyWordName = keyWordNames.getJSONObject(j);
                        JSONObject indexName = new JSONObject();
                        indexName.put(keyWordName.getString("language"), keyWordName.getString("keyword"));
                        indexNameData.put(indexName);
                    }
                    JSONArray keyWordTypes = keyWordObject.getJSONArray("types");
                    for (int k = 0; k < keyWordTypes.length(); k++) {
                      JSONObject typeObject = keyWordTypes.getJSONObject(k);
                      JSONObject indexTypeObject = new JSONObject();
                      String typeId = typeObject.getString("typeId");
                      indexTypeObject.put("typeId", typeId);
                      TopicIF typeTopic = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#"+typeId));

                      if(null != typeTopic && !typeId.equals("keyword") && !typeId.equals("topic")){
                        Collection<TopicNameIF> typeNames = typeTopic.getTopicNames();
                        JSONArray typeNameArray = new JSONArray();

                        if (typeNames.size() > 0) {
                          for (TopicNameIF typeName : typeNames) {
                            JSONObject typeNameObject = new JSONObject();
                            Collection<TopicIF> typeScopes = typeName.getScope();
                            for (TopicIF typeScope : typeScopes) {
                              Collection<LocatorIF> typeScopeLocators = typeScope.getSubjectIdentifiers();
                              for (LocatorIF typeScopeLocator : typeScopeLocators) {
                                if (typeScopeLocator.getAddress().contains("#") && typeScopeLocator.getAddress().contains("iso") && !typeScopeLocator.getAddress().contains("language-neutral")) {
                                  typeNameObject.put(typeScopeLocator.getAddress(),typeName.getValue());
                                }//end if hash so we don't run into the few subjectIdentifiers with no hash
                              }//end for locators
                            } //end for scopes
                            typeNameArray.put(typeNameObject);
                          }//end for names
                        }//end if typenames

                        indexTypeObject.put("names",typeNameArray);
                      }

                      indexTypeData.put(indexTypeObject);
                    }

                  indexObjectData.put("data", indexNameData);
                    indexObjectNames.put(indexObjectData);
                }//end for sets
                indexObject.put("names", indexObjectNames);
                indexObject.put("types", indexTypeData);
                output.put(indexObject);
            }//end while
        } catch (JSONException e) {
          throw new NdlaServiceException(e.getMessage(), e);
        }
        catch (MalformedURLException | UniquenessViolationException e) {
          throw new NdlaServiceException(e.getMessage(),e);
        } catch (Exception ex) {
          throw new NdlaServiceException(ex.getMessage(),ex);
        }
        return output;
    }

  public static JSONArray indexTopicPayload(JSONObject payload, TopicMapIF _topicMap) throws NdlaServiceException {
    JSONArray output = new JSONArray();
    Iterator it = payload.keys();

    try {
      while (it.hasNext()) {
        String topicId = (String) it.next();

        JSONObject indexObject = new JSONObject();
        indexObject.put("topicId", topicId);
        indexObject.put("psi", "http://psi.topic.ndla.no/topics/#" + topicId);

        JSONArray indexObjectNames = new JSONArray();
        JSONArray indexTypeData = new JSONArray();
        JSONArray indexDescriptionData = new JSONArray();
        JSONArray indexImageData = new JSONArray();

        JSONArray topicSet = payload.getJSONArray(topicId);
        for (int i = 0; i < topicSet.length(); i++) {
          JSONObject keyWordObject = topicSet.getJSONObject(i);
          indexImageData = keyWordObject.getJSONArray("images");
          indexDescriptionData = keyWordObject.getJSONArray("descriptions");

          if (i == 0) {
            String approved = keyWordObject.getString("approved");
            if(approved.length() > 0){
              if(approved.equals("true")) {
                indexObject.put("approved", approved);
                String approval_date = keyWordObject.getString("approval_date");
                if( approval_date.length() > 0){
                  indexObject.put("approval_date",approval_date);
                }

              }
              else{
                indexObject.put("approved", "false");
              }
            }
            else{
              indexObject.put("approved", "false");
            }
          }

          JSONObject indexObjectData = new JSONObject();
          JSONArray indexNameData = new JSONArray();

          JSONArray keyWordNames = keyWordObject.getJSONArray("names");
          for (int j = 0; j < keyWordNames.length(); j++) {
            JSONObject keyWordName = keyWordNames.getJSONObject(j);
            JSONObject indexName = new JSONObject();
            indexName.put(keyWordName.getString("language"), keyWordName.getString("name"));
            indexNameData.put(indexName);
          }
          JSONArray keyWordTypes = keyWordObject.getJSONArray("types");
          for (int k = 0; k < keyWordTypes.length(); k++) {
            JSONObject typeObject = keyWordTypes.getJSONObject(k);
            JSONObject indexTypeObject = new JSONObject();
            String typeId = typeObject.getString("typeId");
            indexTypeObject.put("typeId", typeId);
            TopicIF typeTopic = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#"+typeId));

            if(null != typeTopic && !typeId.equals("keyword") && !typeId.equals("topic")){
              Collection<TopicNameIF> typeNames = typeTopic.getTopicNames();
              JSONArray typeNameArray = new JSONArray();

              if (typeNames.size() > 0) {
                for (TopicNameIF typeName : typeNames) {
                  JSONObject typeNameObject = new JSONObject();
                  Collection<TopicIF> typeScopes = typeName.getScope();
                  for (TopicIF typeScope : typeScopes) {
                    Collection<LocatorIF> typeScopeLocators = typeScope.getSubjectIdentifiers();
                    for (LocatorIF typeScopeLocator : typeScopeLocators) {
                      if (typeScopeLocator.getAddress().contains("#") && typeScopeLocator.getAddress().contains("iso") && !typeScopeLocator.getAddress().contains("language-neutral")) {
                        typeNameObject.put(typeScopeLocator.getAddress(),typeName.getValue());
                      }//end if hash so we don't run into the few subejctIdentifiers with no hash
                    }//end for locators
                  } //end for scopes
                  typeNameArray.put(typeNameObject);
                }//end for names
              }//end if typenames

              indexTypeObject.put("names",typeNameArray);
            }

            indexTypeData.put(indexTypeObject);
          }

          indexObjectData.put("data", indexNameData);
          indexObjectNames.put(indexObjectData);
        }//end for sets
        indexObject.put("names", indexObjectNames);
        indexObject.put("types", indexTypeData);
        indexObject.put("descriptions", indexDescriptionData);
        indexObject.put("images",indexImageData);

        output.put(indexObject);
      }//end while
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage());
    } catch (MalformedURLException | UniquenessViolationException e) {
      throw new NdlaServiceException(e.getMessage());
    }
    return output;
  }

    public static JSONArray indexParseTypes(JSONArray keyWordTypes, TopicMapIF _topicMap) throws NdlaServiceException {
      JSONArray indexTypeData = new JSONArray();
      try {
        for (int k = 0; k < keyWordTypes.length(); k++) {
          JSONObject typeObject = keyWordTypes.getJSONObject(k);
          JSONObject indexTypeObject = new JSONObject();
          String typeId = typeObject.getString("typeId");
          indexTypeObject.put("typeId", typeId);
          TopicIF typeTopic = null;
          if(!typeId.contains("#")){
            if(typeId.contains("keyword")) {
              typeTopic = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + typeId));
            }
            else if(typeId.contains("topic")){
              typeTopic = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/topics/#" + typeId));
            }
          }


          if (null != typeTopic && !typeId.equals("keyword") && !typeId.equals("topic")) {
            Collection<TopicNameIF> typeNames = typeTopic.getTopicNames();
            JSONArray typeNameArray = new JSONArray();

            if (typeNames.size() > 0) {
              for (TopicNameIF typeName : typeNames) {
                JSONObject typeNameObject = new JSONObject();
                Collection<TopicIF> typeScopes = typeName.getScope();
                for (TopicIF typeScope : typeScopes) {
                  Collection<LocatorIF> typeScopeLocators = typeScope.getSubjectIdentifiers();
                  for (LocatorIF typeScopeLocator : typeScopeLocators) {
                    if (typeScopeLocator.getAddress().contains("#") && typeScopeLocator.getAddress().contains("iso") && !typeScopeLocator.getAddress().contains("language-neutral")) {
                      typeNameObject.put(typeScopeLocator.getAddress(), typeName.getValue());
                    }//end if hash so we don't run into the few subejctIdentifiers with no hash
                  }//end for locators
                } //end for scopes
                typeNameArray.put(typeNameObject);
              }//end for names
            }//end if typenames

            indexTypeObject.put("names", typeNameArray);
          }

          indexTypeData.put(indexTypeObject);
        }
      }
      catch (JSONException e) {
        throw new NdlaServiceException(e.getMessage());
      } catch (MalformedURLException | UniquenessViolationException e) {
        throw new NdlaServiceException(e.getMessage());
      }
      return indexTypeData;
    }

    public static TopicIF getLanguageTopic(String value, final TopicMapIF topicMap) throws NdlaServiceException {
        TopicIF languageTopic = null;

        try {
          languageTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(value));
        } catch (MalformedURLException | UniquenessViolationException e) {
            throw new NdlaServiceException(e.getMessage(),e);
        }
        return languageTopic;
    }

    public static void deleteBasenames(LocatorIF loc, final TopicMapIF topicMap) {
        TopicIF topic = topicMap.getTopicBySubjectIdentifier(loc);
        Collection<TopicNameIF> topicNames = topic.getTopicNames();
        ArrayList<TopicNameIF> namesForRemoval = new ArrayList<TopicNameIF>();

        for (TopicNameIF topicName : topicNames) {
            namesForRemoval.add(topicName);
        }

        for (TopicNameIF nameForRemoval : namesForRemoval) {
            nameForRemoval.remove();
        }
    }

    public static void deletePsis(LocatorIF loc, final TopicMapIF topicMap) {
      TopicIF topic = topicMap.getTopicBySubjectIdentifier(loc);
      Collection<LocatorIF> psis = topic.getSubjectIdentifiers();
      ArrayList<LocatorIF> psisForRemoval = new ArrayList<LocatorIF>();
      for(LocatorIF psi : psis) {
        psisForRemoval.add(psi);
      }

      for (LocatorIF rPsi : psisForRemoval) {
        topic.removeSubjectIdentifier(rPsi);
      }
    }

  public static void deleteItemIdentifiers(LocatorIF loc, final TopicMapIF topicMap) {
    TopicIF topic = topicMap.getTopicBySubjectIdentifier(loc);
    Collection<LocatorIF> itemLocatorIFs = topic.getItemIdentifiers();
    ArrayList<LocatorIF> itemsForRemoval = new ArrayList<LocatorIF>();
    for(LocatorIF itemLocator : itemLocatorIFs) {
      itemsForRemoval.add(itemLocator);
    }

    for (LocatorIF rItem : itemsForRemoval) {
      topic.removeItemIdentifier(rItem);
    }
  }


  public static void deleteTypes(LocatorIF topicLocator, ArrayList<LocatorIF> typeLocators, final TopicMapIF topicMap) {
      TopicIF topic = topicMap.getTopicBySubjectIdentifier(topicLocator);
      ArrayList<TopicIF> excludedTypeTopics = new ArrayList<>();
      for(LocatorIF locatorIF : typeLocators){
        TopicIF excludedTypeTopic = topicMap.getTopicBySubjectIdentifier(locatorIF);
        excludedTypeTopics.add(excludedTypeTopic);
      }

      Collection<TopicIF> types = topic.getTypes();
      ArrayList<TopicIF> typesForRemoval = new ArrayList<>();
      for(TopicIF type : types) {
        if(!excludedTypeTopics.contains(type)) {
          typesForRemoval.add(type);
        }
      }

      if(typesForRemoval.size() > 0) {
        for(TopicIF removedType : typesForRemoval) {
          topic.removeType(removedType);
        }
      }
    }

    public static void deleteAllTypes(LocatorIF topicLocator, final TopicMapIF topicMap) {
      TopicIF topic = topicMap.getTopicBySubjectIdentifier(topicLocator);

      Collection<TopicIF> types = topic.getTypes();
      ArrayList<TopicIF> typesForRemoval = new ArrayList<>();
      for(TopicIF type : types) {
          typesForRemoval.add(type);
      }

      if(typesForRemoval.size() > 0) {
        for(TopicIF removedType : typesForRemoval) {
          topic.removeType(removedType);
        }
      }
    }


    public static HashMap<String, TopicIF> lookForExistingTopics(HashMap<String, HashMap<String, HashMap<String, String>>> data, String typeString, final TopicMapIF topicMap) throws NdlaServiceException, BadObjectReferenceException {
        HashMap<String, TopicIF> result = new HashMap<String, TopicIF>();
        ArrayList<String> locators = new ArrayList<String>();
        Iterator dataIterator = data.keySet().iterator();

        while (dataIterator.hasNext()) {
            String dataSetKey = (String) dataIterator.next();
            HashMap<String, HashMap<String, String>> keySets = data.get(dataSetKey);
            Iterator setIterator = keySets.keySet().iterator();

            while (setIterator.hasNext()) {
                String setKey = (String) setIterator.next();
                HashMap<String, String> topicSetMap = keySets.get(setKey);
                Iterator topicIterator = topicSetMap.keySet().iterator();
                String loc = "";
                switch (setKey) {
                    case "names":
                        while (topicIterator.hasNext()) {
                            String topicStringLang = (String) topicIterator.next();
                            String topicString = topicSetMap.get(topicStringLang);
                            if (!isEmpty(topicString) && !isEmpty(topicStringLang)) {
                                // check for duplicates
                                String query = String.format("select $COLUMN from value($N,\"%s\"),scope($N,%s),topic-name($COLUMN,$N),instance-of($COLUMN,%s)?", topicString, topicStringLang, typeString);

                                TopicIF resultData = executeObjectQuery(query, topicMap);

                                String resultTopicString = getTopicNames(resultData).get(topicStringLang);

                                if (resultData != null && resultData instanceof TopicIF && (resultTopicString != null && resultTopicString.equalsIgnoreCase(topicString))) {
                                    loc = resultData.getSubjectIdentifiers().iterator().next().getAddress();
                                    if (!locators.contains(loc)) {
                                        locators.add(loc);
                                        result.put(dataSetKey, resultData);
                                    }
                                } else {
                                    result.put(dataSetKey, null);
                                }
                            }// endif not empty name and scope
                        }//end while topicIterator
                        break;
                }//end switch setKey
            }//end while setiterator
        }//end while dataIterator
        return result;
    }

  public ArrayList<NdlaTopic> getAssociationCount(JSONArray keywordNames, final TopicMapIF _topicMap)  throws NdlaServiceException, BadObjectReferenceException {
    ArrayList<NdlaTopic> result = new ArrayList<>();
    return result;
  }

  public static boolean assocExists(String player1TopicId, String player2TopicId, String relationType, String player1Type, String player2Type, HashMap<String, String> reifiers, final TopicMapIF topicMap)  throws NdlaServiceException, BadObjectReferenceException {
    boolean assocExists = false;
    QueryProcessorIF queryProcessor = QueryUtils.getQueryProcessor(topicMap);

    String reifierValue = "";

    if(null != reifiers.get("http://psi.topic.ndla.no/#udir-subject")){
      reifierValue = reifiers.get("http://psi.topic.ndla.no/#udir-subject");
    }
    else if(null != reifiers.get("http://psi.topic.ndla.no/#ontology_topic_domain_type")){
      reifierValue = reifiers.get("http://psi.topic.ndla.no/#ontology_topic_domain_type");
    }
    String query = String.format("select $COLUMN from udir-subject($REIFIER,\"%s\"),reifies($REIFIER,$COLUMN),\n" +
        "  role-player($ROLE1, %s),\n" +
        "  association-role($COLUMN, $ROLE1),\n" +
        "  association-role($COLUMN, $ROLE2),\n" +
        "  role-player($ROLE2, %s), type($COLUMN,i\"%s\")?",reifierValue,player1TopicId,player2TopicId,relationType);

    QueryResultIF tologResult = null;
    try {
      tologResult = queryProcessor.execute(query);

      if (tologResult.next()){
        AssociationIF assoc = (AssociationIF) tologResult.getValue("COLUMN");
        //TreeMap<String,String> gotRoles = new TreeMap<>();
        ArrayList<String> gotRoles = new ArrayList<>();
        int length = tologResult.getValues().length;
        if(length > 0){

          Collection<AssociationRoleIF> roles = assoc.getRoles();
          for(AssociationRoleIF role : roles){
            TopicIF roleType = role.getType();
            Collection<LocatorIF> roleTypePSIs = roleType.getSubjectIdentifiers();
            String roleTypePSIString = "";
            if(roleTypePSIs.size() > 1){
              int rolePsiCounter = 0;
              for(LocatorIF rPSI : roleTypePSIs){
                if(rolePsiCounter == 0){
                  roleTypePSIString = rPSI.getAddress();
                }
              }
            }
            else{
              roleTypePSIString = roleTypePSIs.iterator().next().getAddress();
            }

            TopicIF rolePlayer = role.getPlayer();
            Collection<LocatorIF> playerPSIs = rolePlayer.getSubjectIdentifiers();
            String rolePlayerPSIString = "";
            if(playerPSIs.size() > 1){
              int rolePsiCounter = 0;
              for(LocatorIF pPSI : playerPSIs){
                String psiUri =  pPSI.getAddress();
                if(psiUri.contains("#topic")){
                  rolePlayerPSIString = psiUri;
                }
              }
            }
            else{
              rolePlayerPSIString = playerPSIs.iterator().next().getAddress();
            }
            String rolePlayerString = "";
            if(rolePlayerPSIString.contains("#")){
              rolePlayerString = rolePlayerPSIString.substring(rolePlayerPSIString.lastIndexOf("#")+1);
            }
            else{
              rolePlayerString = rolePlayerPSIString.substring(rolePlayerPSIString.lastIndexOf("/")+1);
            }
            //gotRoles.put(roleTypePSIString,rolePlayerString);
            gotRoles.add(rolePlayerString);
          }

          /*
          if(gotRoles.get(player1Type).equals(player1TopicId) && gotRoles.get(player2Type).equals(player2TopicId)){
            assocExists = true;
          }
          */
          if(gotRoles.contains(player1TopicId) && gotRoles.contains(player2TopicId)){
            assocExists = true;
          }


        }
      }

    } catch (BadObjectReferenceException e) {
      throw new BadObjectReferenceException(e.getMessage());
    } catch (InvalidQueryException e) {
      throw new NdlaServiceException(e.getMessage());
    } finally {
      if (null != tologResult) {
        tologResult.close();
      }
    }
    return assocExists;
  }

  public static boolean lookForExistingTopicsByNameSet(JSONArray keywordNames, final TopicMapIF _topicMap)  throws NdlaServiceException, BadObjectReferenceException {
    boolean exists = false;
    String typeString = "keyword";
    try {
      for (int k = 0; k < keywordNames.length(); k++) {
        JSONObject keywordName = keywordNames.getJSONObject(k);
        String keywordNameLang = keywordName.getString("language");
        String keywordNameString = keywordName.getString("keyword").toLowerCase();
        if (!isEmpty(keywordNameString) && !isEmpty(keywordNameLang)) {
          String query = String.format("select $COLUMN from value($N,\"%s\"),scope($N,%s),topic-name($COLUMN,$N),instance-of($COLUMN,%s)?", keywordNameString, keywordNameLang, typeString);

          TopicIF resultData = executeObjectQuery(query, _topicMap);

          String resultTopicString = getTopicNames(resultData).get(keywordNameLang);

          if (resultData != null && resultData instanceof TopicIF && (resultTopicString != null && resultTopicString.equals(keywordNameString))) {
           exists = true;
            break;
          }
        }
      }
    }catch (JSONException e) {
        throw new NdlaServiceException(e.getMessage(),e);
      }
    return exists;
  }

  public static JSONObject lookForExistingTopics(String data, String _configFile, String _topicmapReferenceKey) throws NdlaServiceException, BadObjectReferenceException {
    JSONObject result = new JSONObject();

    TopicMapStoreIF store = null;
    TopicMapRepositoryIF repository = null;
    TopicMapReferenceIF reference = null;

    try {
      JSONObject payload = new JSONObject(data);
      JSONArray keywords = payload.getJSONArray("keywords");
      JSONArray newKeywords = new JSONArray();
      repository = XMLConfigSource.getRepository(_configFile);
      reference = repository.getReferenceByKey(_topicmapReferenceKey);

      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();

      ArrayList<String> foundNames = new ArrayList<>();

      for (int i = 0; i < keywords.length(); i++) {
        JSONObject jsonKeywordSet = keywords.getJSONObject(i);
        JSONObject newJsonKeywordSet = new JSONObject();

        JSONArray keywordSets = jsonKeywordSet.getJSONArray("keywordset");
        JSONArray newKeywordSets = new JSONArray();

        String setKey = "";
        String foundString = "";
        for (int j = 0; j < keywordSets.length(); j++) {
          JSONObject keywordData = keywordSets.getJSONObject(j);
          JSONObject newKeywordData = new JSONObject();

          JSONArray keywordNames = keywordData.getJSONArray("names");
          JSONArray newKeywordNames = new JSONArray();
          JSONArray keywordTypes = keywordData.getJSONArray("types");


          String topicId = keywordData.getString("topicId").trim();
          newKeywordData.put("topicId", topicId);

          String wordClass = keywordData.getString("wordclass");
          newKeywordData.put("wordclass", wordClass);

          String visibility = keywordData.getString("visibility");
          newKeywordData.put("visibility", visibility);

          for (int k = 0; k < keywordNames.length(); k++) {
            JSONObject keywordName = keywordNames.getJSONObject(k);
            String keywordNameLang = keywordName.getString("language");
            String keywordNameString = keywordName.getString("keyword").toLowerCase();
            if (!isEmpty(topicId) && !isEmpty(keywordNameString) && !isEmpty(keywordNameLang)) {
              //subject-identifier(keyword-1387108369OJWPZNLOVR,$COLUMN)
              String query = String.format("select $COLUMN from subject-identifier(%s,$COLUMN)?",topicId);

              ArrayList<String> subjids = executeStringCollectionQuery(query, topicMap);
              String psi = "";
              if(subjids.size() > 0) {
                if(subjids.size() > 1){
                  for(String tempPSI : subjids){
                    if(tempPSI.contains("keyword")){
                      psi = tempPSI;
                      break;
                    }
                  }
                }
                else{
                  String tempPSI = subjids.get(0);
                  if(tempPSI.contains("keyword")){
                    psi = tempPSI;
                  }
                }

                HashMap<String,String> processState = getProcessState(psi,topicMap);
                if(processState.size() > 0){
                  newKeywordData.put("processState",processState.get("processState"));
                }
                else{
                  newKeywordData.put("processState","0");
                }

                setKey = psi.substring(psi.lastIndexOf("#") + 1);
                if(setKey.equals(topicId)){
                  foundString = setKey + "_" + wordClass;
                  newKeywordNames.put(keywordName);
                }
              }
              else{
                newKeywordNames.put(keywordName);
              }//end if topic exists
            }//end if not empty
            if (newKeywordNames.length() > 0) {
              newKeywordData.put("names", newKeywordNames);
            }

              newKeywordData.put("types", keywordTypes);

            if (!foundNames.contains(foundString)) {
              newKeywordSets.put(newKeywordData);
              foundNames.add(foundString);
            }
            if (!setKey.equals("")) {
              newJsonKeywordSet.put(setKey, newKeywordSets);
            }
          }//end for keywordnames k
        }//end for keywordsets j
        if (newJsonKeywordSet.length() > 0) {
          newKeywords.put(newJsonKeywordSet);
        }
      }//end for keywords i
      result.put("keywords", newKeywords);
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(), e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }

    return result;
  }

  public static boolean isExistingTopicByNames(JSONArray topicNames, String _configFile, String _topicmapReferenceKey) throws NdlaServiceException, BadObjectReferenceException {
    boolean isExistingTopic = false;

    TopicMapStoreIF store = null;
    TopicMapRepositoryIF repository = null;
    TopicMapReferenceIF reference = null;
    TreeMap<String,String> duplicateMap= new TreeMap<>();
    try {

      repository = XMLConfigSource.getRepository(_configFile);
      reference = repository.getReferenceByKey(_topicmapReferenceKey);

      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();

      if(topicNames.length() > 0){
        for (int i = 0; i < topicNames.length(); i++) {
          JSONObject nameObject = topicNames.getJSONObject(i);

          String topicNameString = nameObject.getString("name");
          String topicNameLang = nameObject.getString("language");

          String query = String.format("select $COLUMN from value($N,\"%s\"),scope($N,i\"%s\"),topic-name($COLUMN,$N),instance-of($COLUMN,%s)?", topicNameString, topicNameLang, "topic");

          TopicIF resultData = executeObjectQuery(query, topicMap);
          String resultTopicString = getTopicNames(resultData).get(topicNameLang);

          if (resultData != null && resultData instanceof TopicIF && (resultTopicString != null && resultTopicString.equalsIgnoreCase(topicNameString))) {
            duplicateMap.put(topicNameLang,"duplicate");
          }
          else{
            duplicateMap.put(topicNameLang,"new");
          }
        }//end for names

        if(duplicateMap.get("http://psi.oasis-open.org/iso/639/#eng") == "duplicate" && duplicateMap.get("http://psi.oasis-open.org/iso/639/#nob") == "duplicate") {
          isExistingTopic = true;
        }
      }


    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(), e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return isExistingTopic;
  }

  public static JSONObject lookForExistingTopics(String data, TopicMapRepositoryIF repository, TopicMapReferenceIF reference) throws NdlaServiceException, BadObjectReferenceException {
    JSONObject result = new JSONObject();
    TopicMapStoreIF store = null;

    try {
      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();

      JSONObject payload = new JSONObject(data);
      JSONArray keywords = payload.getJSONArray("keywords");
      JSONArray newKeywords = new JSONArray();

      ArrayList<String> foundNames = new ArrayList<>();

      for (int i = 0; i < keywords.length(); i++) {
        JSONObject jsonKeywordSet = keywords.getJSONObject(i);
        JSONObject newJsonKeywordSet = new JSONObject();

        JSONArray keywordSets = jsonKeywordSet.getJSONArray("keywordset");
        JSONArray newKeywordSets = new JSONArray();

        String setKey = "";
        String foundString = "";
        for (int j = 0; j < keywordSets.length(); j++) {
          JSONObject keywordData = keywordSets.getJSONObject(j);
          JSONObject newKeywordData = new JSONObject();

          JSONArray keywordNames = keywordData.getJSONArray("names");
          JSONArray newKeywordNames = new JSONArray();
          JSONArray keywordTypes = keywordData.getJSONArray("types");


          String topicId = keywordData.getString("topicId").trim();
          newKeywordData.put("topicId", topicId);

          String wordClass = keywordData.getString("wordclass");
          newKeywordData.put("wordclass", wordClass);

          String visibility = keywordData.getString("visibility");
          newKeywordData.put("visibility", visibility);

          for (int k = 0; k < keywordNames.length(); k++) {
            JSONObject keywordName = keywordNames.getJSONObject(k);
            String keywordNameLang = keywordName.getString("language");
            String keywordNameString = keywordName.getString("keyword").toLowerCase();
            if (!isEmpty(topicId) && !isEmpty(keywordNameString) && !isEmpty(keywordNameLang)) {
              //subject-identifier(keyword-1387108369OJWPZNLOVR,$COLUMN)
              String query = String.format("select $COLUMN from subject-identifier(%s,$COLUMN)?",topicId);

              ArrayList<String> subjids = executeStringCollectionQuery(query, topicMap);
              String psi = "";
              if(subjids.size() > 0) {
                psi = subjids.get(0);
                setKey = psi.substring(psi.lastIndexOf("#") + 1);
                if(setKey.equals(topicId)){
                  foundString = setKey + "_" + wordClass;
                  newKeywordNames.put(keywordName);
                }
              }
              else{
                newKeywordNames.put(keywordName);
              }//end if topic exists
            }//end if not empty
            if (newKeywordNames.length() > 0) {
              newKeywordData.put("names", newKeywordNames);
            }

            newKeywordData.put("types", keywordTypes);

            if (!foundNames.contains(foundString)) {
              newKeywordSets.put(newKeywordData);
              foundNames.add(foundString);
            }
            if (!setKey.equals("")) {
              newJsonKeywordSet.put(setKey, newKeywordSets);
            }
          }//end for keywordnames k
        }//end for keywordsets j
        if (newJsonKeywordSet.length() > 0) {
          newKeywords.put(newJsonKeywordSet);
        }
      }//end for keywords i
      result.put("keywords", newKeywords);
    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(), e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return result;
  }


    /*
      Check if topic already exists and return it.
      @return TopicIF existingTopic
     */
    public static TopicIF isDuplicateTopic(String psi, final TopicMapIF topicMap) throws NdlaServiceException, BadObjectReferenceException {
      TopicIF duplicate = null;
      try {
        TopicIF topicExists = topicMap.getTopicBySubjectIdentifier(new URILocator(psi));
        if (null != topicExists) {
          duplicate = topicExists;
        }
      } catch (MalformedURLException e) {
        throw new NdlaServiceException(e.getMessage(),e);
      }
      return duplicate;
    }

  public static TopicIF isExistingTopic(JSONArray keywordset, final TopicMapIF topicMap) throws NdlaServiceException, BadObjectReferenceException {
    TopicIF existingTopic = null;
    HashMap<String, HashMap<String, String>> setMap = new HashMap<>(); // TODO: Flagged for removal (BK).

    try {
      for (int j = 0; j < keywordset.length(); j++) {
        String setid = "set_" + j;

        JSONObject keywordData = keywordset.getJSONObject(j);
        String topicId = keywordData.getString("topicId");

        TopicIF topicExists = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#" + topicId));
        if (null != topicExists) {
          existingTopic = topicExists;
        }
      }//end for set
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage(),e);
    }
    catch (MalformedURLException ue) {
      throw new NdlaServiceException(ue.getMessage());
    }
    return existingTopic;
  }

  public static TopicIF isDuplicateUpdateTopic(JSONArray keywordset, final TopicMapIF topicMap) throws NdlaServiceException, BadObjectReferenceException {
    TopicIF duplicate = null;

    try {
      for (int j = 0; j < keywordset.length(); j++) {
        JSONObject keywordData = keywordset.getJSONObject(j);
        String topicId = keywordData.getString("topicId");
        if(topicId.length() > 0){
          duplicate = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/keywords/#"+topicId));
          if(null != duplicate){
            break;
          }

        }
      }//end for set
    } catch (JSONException e) {
      throw new NdlaServiceException(e.getMessage(),e);
    }
    catch (MalformedURLException e) {
      throw new NdlaServiceException(e.getMessage(),e);
    }
    return duplicate;
  }
    protected static TopicIF compareTopicNames(String lang, String word, String wordClass, ArrayList<TopicIF> resultData) {
        TopicIF theTopic = null;
        for (TopicIF topic : resultData) {
            Collection<TopicNameIF> topicNames = topic.getTopicNames();
            for (TopicNameIF topicName : topicNames) {
                String topicNameString = topicName.getValue();
                HashMap<String, String> currentNameScopes = resolveKeywordNameScopes(topicName);
                String currentLang = currentNameScopes.get("lang");
                String currentWordclass = currentNameScopes.get("wordclass");
                // [Bug] currentWordclass is intermittently null, causing a Null Pointer Exception: currentWordclass.equals(word_class)
                // Need to fix resolveKeywordNameScopes method.
                if (currentLang.equals(lang) && (!isEmpty(topicNameString) && topicNameString.equals(word)) && currentWordclass.equals(wordClass)) {
                    theTopic = topic;
                    break;
                }
            }
        }
        return theTopic;
    }
    protected static HashMap<String, String> resolveKeywordNameScopes(TopicNameIF topicName) {
        HashMap<String, String> resolvedScopes = new HashMap<>();
        Collection<TopicIF> scopes = topicName.getScope();
        for (TopicIF scope : scopes) {
            Collection<LocatorIF> locators = scope.getSubjectIdentifiers();
            for (LocatorIF locator : locators) {
                if (locator.getAddress().contains("#")) {
                    if (wcs.contains(locator.getAddress())) {
                        String tmp = locator.getAddress();
                        resolvedScopes.put("wordclass", tmp.substring(tmp.lastIndexOf("#") + 1));
                    }

                    if (!locator.getAddress().contains("language-neutral") && !wcs.contains(locator.getAddress())) {
                        resolvedScopes.put("lang", Utils.determineLanguage(locator.getAddress()));
                    }
                }
            }
        }
        return resolvedScopes;
    }

    public static HashMap<String, String> resolveLicense(String nodeLicense) {
        HashMap<String, HashMap<String, String>> licenseMap = new HashMap<String, HashMap<String, String>>();

        HashMap<String, String> gnuMap = new HashMap<String, String>();
        gnuMap.put("en", "http://en.wikipedia.org/wiki/GNU_General_Public_License");
        gnuMap.put("nb", "http://no.wikipedia.org/wiki/GNU_General_Public_License");
        licenseMap.put("gnu", gnuMap);

        HashMap<String, String> publicDomainMap = new HashMap<String, String>();
        publicDomainMap.put("en", "http://en.wikipedia.org/wiki/Public_domain");
        publicDomainMap.put("nb", "http://no.wikipedia.org/wiki/Offentlig_eiendom");
        licenseMap.put("publicdomain", publicDomainMap);

        HashMap<String, String> copyrightedMap = new HashMap<String, String>();
        copyrightedMap.put("en", "http://en.wikipedia.org/wiki/Copyright");
        copyrightedMap.put("nb", "http://no.wikipedia.org/wiki/Opphavsrett");
        licenseMap.put("copyrighted", copyrightedMap);

        HashMap<String, String> noLawMap = new HashMap<String, String>();
        noLawMap.put("en", "http://creativecommons.org/publicdomain/zero/1.0/");
        noLawMap.put("nb", "http://creativecommons.org/publicdomain/zero/1.0/deed.no");
        licenseMap.put("nolaw", noLawMap);

        HashMap<String, String> noCMap = new HashMap<String, String>();
        noCMap.put("en", "http://creativecommons.org/publicdomain/mark/1.0/deed");
        noCMap.put("nb", "http://creativecommons.org/publicdomain/mark/1.0/deed.no");
        licenseMap.put("noc", noCMap);

        HashMap<String, String> nlodCMap = new HashMap<String, String>();
        nlodCMap.put("en", "http://data.norge.no/nlod/no/1.0");
        nlodCMap.put("nb", "http://data.norge.no/nlod/no/1.0");
        licenseMap.put("nlod", nlodCMap);

      return licenseMap.get(nodeLicense);
    }

    public static void deleteOccurrences(LocatorIF loc, final TopicMapIF topicMap) {
        TopicIF topic = topicMap.getTopicBySubjectIdentifier(loc);
        Collection<OccurrenceIF> occurrences = topic.getOccurrences();
        ArrayList<OccurrenceIF> occurrencesForRemoval = new ArrayList<OccurrenceIF>();

        for (OccurrenceIF occurrence : occurrences) {
            occurrencesForRemoval.add(occurrence);
        }
        for (OccurrenceIF occurrenceForRemoval : occurrencesForRemoval) {
            occurrenceForRemoval.remove();
        }
    }

    public static void deleteOccurrencesByType(LocatorIF loc, LocatorIF occurrenceType, final TopicMapIF topicMap) {
      TopicIF topic = topicMap.getTopicBySubjectIdentifier(loc);
      Collection<OccurrenceIF> occurrences = topic.getOccurrences();
      ArrayList<OccurrenceIF> occurrencesForRemoval = new ArrayList<OccurrenceIF>();

      for (OccurrenceIF occurrence : occurrences) {
        TopicIF occType = occurrence.getType();
        Collection<LocatorIF> occTypePsis = occType.getSubjectIdentifiers();
        if(occTypePsis.contains(occurrenceType)){
          occurrencesForRemoval.add(occurrence);
        }

      }
      for (OccurrenceIF occurrenceForRemoval : occurrencesForRemoval) {
        occurrenceForRemoval.remove();
      }
    }


    public static void deleteAllAssocs(TopicIF topic, final TopicMapIF _topicMap)  throws NdlaServiceException, BadObjectReferenceException  {
      Collection<AssociationRoleIF> topicRoles = topic.getRoles();
      if (topicRoles.size() > 0) {
        ArrayList<AssociationRoleIF> rolesForRemoval = new ArrayList<AssociationRoleIF>();
        Iterator roleIt = topicRoles.iterator();
        while (roleIt.hasNext()) {
          AssociationRoleIF topicRole = (AssociationRoleIF) roleIt.next();
          AssociationIF assoc = topicRole.getAssociation();

          Iterator iter = assoc.getRoles().iterator();
          AssociationRoleIF otherRole = null;
          while (iter.hasNext()) {
            AssociationRoleIF role = (AssociationRoleIF) iter.next();
            if (!role.equals(topicRole)) {
              otherRole = role;
              rolesForRemoval.add(otherRole);
            }
          }//end while roles
        }//end while noderoles

        for (AssociationRoleIF forRemoval : rolesForRemoval) {
          forRemoval.getAssociation().remove();
        }
      }// end if noderoles to delete
    }

  public static void deleteAssocsByAssoctypeAndSubjectId(String topicId1, String topicId2, String assocType ,String subjectMatterIdentifier, final TopicMapIF _topicMap)  throws NdlaServiceException, BadObjectReferenceException,MalformedURLException  {
    boolean hasSubjectMatterIdentifier = false;
    ArrayList<AssociationIF> assocForDeletion = new ArrayList<AssociationIF>();
    ArrayList<TopicIF> reifiersForDeletion = new ArrayList<TopicIF>();

    //String query = String.format("select $COLUMN from value($N,\"%s\"),scope($N,%s),topic-name($COLUMN,$N),instance-of($COLUMN,%s)?", topicString, topicStringLang, typeString);
    String query = String.format("select $COLUMN from role-player($ROLE1, %s),association-role($COLUMN, $ROLE1),association-role($COLUMN, $ROLE2),role-player($ROLE2, %s), type($COLUMN, i\"%s\")?",topicId1,topicId2,assocType);
    ArrayList<Object> assocs =  executeObjectCollectionQuery(query, _topicMap);
    for( Object assoc : assocs) {
      AssociationIF association = (AssociationIF) assoc;
      TopicIF reifier = association.getReifier();

      TopicIF udirOccTopic = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#udir-subject"));
      TopicIF subjectMatterTopic = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#subjectmatter"));
      Collection<OccurrenceIF> udirOccurrences = null;
      Collection<OccurrenceIF> subjectMatterOccurrences = null;
      if(null != reifier) {
        udirOccurrences = reifier.getOccurrencesByType(udirOccTopic);
        subjectMatterOccurrences = reifier.getOccurrencesByType(subjectMatterTopic);
        reifiersForDeletion.add(reifier);
      }

      if(udirOccurrences.size() > 0 && !hasSubjectMatterIdentifier) {
        for(OccurrenceIF udirOccurrence : udirOccurrences) {
          if(udirOccurrence.getValue().equals("http://psi.topic.ndla.no/#"+subjectMatterIdentifier)){
            hasSubjectMatterIdentifier = true;
          }
        }
      }
      if(subjectMatterOccurrences.size() > 0 && !hasSubjectMatterIdentifier) {
        for(OccurrenceIF subjectMatterOccurrence : subjectMatterOccurrences) {
          if(subjectMatterOccurrence.getValue().equals("http://psi.topic.ndla.no/#"+subjectMatterIdentifier)){
            hasSubjectMatterIdentifier = true;
          }
        }
      }

      if(hasSubjectMatterIdentifier) {
        assocForDeletion.add(association);

      }
    }//end for
    if(hasSubjectMatterIdentifier) {
      for (AssociationIF forRemoval : assocForDeletion) {
        forRemoval.remove();
      }

      for(TopicIF reifierForRemoval: reifiersForDeletion) {
        reifierForRemoval.remove();
      }
    }
  }

  public static void deleteAllAimAssocsBySubjectId(String subjectMatterIdentifier, final TopicMapIF _topicMap)  throws NdlaServiceException, BadObjectReferenceException,MalformedURLException  {
    ArrayList<AssociationIF> assocsForRemoval = new ArrayList<AssociationIF>();
    try {


      QueryProcessorIF queryProcessor = QueryUtils.getQueryProcessor(_topicMap);


      String query = String.format("select $COLUMN from udir-subject($REIFIER,\"http://psi.topic.ndla.no/#%s\"),reifies($REIFIER,$COLUMN),\n" +
          "  role-player($ROLE1, $PLAYER1),\n" +
          "  association-role($COLUMN, $ROLE1),\n" +
          "  association-role($COLUMN, $ROLE2),\n" +
          "  role-player($ROLE2, $PLAYER2), {type($ROLE1,i\"http://psi.udir.no/ontologi/lkt/#kompetansemaal\") | type($ROLE2,i\"http://psi.udir.no/ontologi/lkt/#kompetansemaal\")}?",subjectMatterIdentifier);

      QueryResultIF tologResult = null;
      try {
        tologResult = queryProcessor.execute(query);

        while (tologResult.next()) {
          AssociationIF assoc = (AssociationIF) tologResult.getValue("COLUMN");
          assocsForRemoval.add(assoc);
        }

        for(AssociationIF associationIF : assocsForRemoval){
          associationIF.remove();
        }
      } catch (BadObjectReferenceException e) {
        throw new BadObjectReferenceException(e.getMessage());
      } catch (InvalidQueryException e) {
        throw new NdlaServiceException(e.getMessage());
      } finally {
        if (null != tologResult) {
          tologResult.close();
        }
      }
    } catch (Exception e) {
      throw new NdlaServiceException(e.getMessage());
    }
   }

    public static void deleteAllAssocsBySubjectId(TopicIF topic, String subjectMatterIdentifier, final TopicMapIF _topicMap)  throws NdlaServiceException, BadObjectReferenceException,MalformedURLException  {
      Collection<AssociationRoleIF> topicRoles = topic.getRoles();
      boolean hasSubjectMatterIdentifier = false;
      if (topicRoles.size() > 0) {
        ArrayList<AssociationRoleIF> rolesForRemoval = new ArrayList<AssociationRoleIF>();
        Iterator roleIt = topicRoles.iterator();
        while (roleIt.hasNext()) {
          AssociationRoleIF topicRole = (AssociationRoleIF) roleIt.next();
          AssociationIF assoc = topicRole.getAssociation();

          TopicIF reifier = assoc.getReifier();
          TopicIF udirOccTopic = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#udir-subject"));
          TopicIF subjectMatterTopic = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#subjectmatter"));
          Collection<OccurrenceIF> udirOccurrences = reifier.getOccurrencesByType(udirOccTopic);
          Collection<OccurrenceIF> subjectMatterOccurrences = reifier.getOccurrencesByType(subjectMatterTopic);

          if(udirOccurrences.size() > 0 && !hasSubjectMatterIdentifier) {
            for(OccurrenceIF udirOccurrence : udirOccurrences) {
              if(udirOccurrence.getValue().equals("http://psi.topic.ndla.no/#"+subjectMatterIdentifier)){
                hasSubjectMatterIdentifier = true;
              }
            }
          }
          if(subjectMatterOccurrences.size() > 0 && !hasSubjectMatterIdentifier) {
            for(OccurrenceIF subjectMatterOccurrence : subjectMatterOccurrences) {
              if(subjectMatterOccurrence.getValue().equals("http://psi.topic.ndla.no/#"+subjectMatterIdentifier)){
                hasSubjectMatterIdentifier = true;
              }
            }
          }

          if(hasSubjectMatterIdentifier) {
            Iterator iter = assoc.getRoles().iterator();
            AssociationRoleIF otherRole = null;
            while (iter.hasNext()) {
              AssociationRoleIF role = (AssociationRoleIF) iter.next();
              if (!role.equals(topicRole)) {
                otherRole = role;
                rolesForRemoval.add(otherRole);
              }
            }//end while roles
          }

        }//end while noderoles

        if(hasSubjectMatterIdentifier) {
          for (AssociationRoleIF forRemoval : rolesForRemoval) {
            forRemoval.getAssociation().remove();
          }
        }
      }// end if noderoles to delete
    }

    public static void deleteAssocs(TopicIF topic, String assocTypeString, final TopicMapIF topicMap) { // TODO: The topicMap parameter is superfluous. Flagged for removal.
        Collection<AssociationRoleIF> nodeRoles = topic.getRoles();
        if (nodeRoles.size() > 0) {
            ArrayList<AssociationRoleIF> rolesForRemoval = new ArrayList<AssociationRoleIF>();
            Iterator roleIt = nodeRoles.iterator();
            while (roleIt.hasNext()) {
              AssociationRoleIF nodeRole = (AssociationRoleIF) roleIt.next();
              AssociationIF assoc = nodeRole.getAssociation();
              TopicIF assocType = assoc.getType();
              Collection<LocatorIF> typeStrings = assocType.getSubjectIdentifiers(); //getItemIdentifiers().iterator().next().getAddress();
              Iterator it = typeStrings.iterator();

              while(it.hasNext()){
                LocatorIF typeStringLocator = (LocatorIF) it.next();
                String typestring = typeStringLocator.getAddress();

                if (typestring.equals(assocTypeString)) {
                  Iterator iter = assoc.getRoles().iterator();
                  AssociationRoleIF otherRole = null;
                  while (iter.hasNext()) {
                    AssociationRoleIF role = (AssociationRoleIF) iter.next();
                    if (!role.equals(nodeRole)) {
                      otherRole = role;
                      rolesForRemoval.add(otherRole);
                    }
                  }//end while roles
                }//end if correct assoctype
              }//end while iterator

            }//end while noderoles

            for (AssociationRoleIF forRemoval : rolesForRemoval) {
                forRemoval.getAssociation().remove();
            }
          topicMap.getStore().commit();
        }// end if noderoles to delete
    }

  public static void deleteAssocs(String topicObjectId, String assocTypeString, String _configFile, String _topicmapReferenceKey) throws NdlaServiceException, BadObjectReferenceException { // TODO: The topicMap parameter is superfluous. Flagged for removal.
    TopicMapStoreIF store = null;
    TopicMapRepositoryIF repository = null;
    TopicMapReferenceIF reference = null;

    try {

      repository = XMLConfigSource.getRepository(_configFile);
      reference = repository.getReferenceByKey(_topicmapReferenceKey);

      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      TopicIF topic = (TopicIF) topicMap.getObjectById(topicObjectId);

      Collection<AssociationRoleIF> nodeRoles = topic.getRoles();
      if (nodeRoles.size() > 0) {
        ArrayList<AssociationRoleIF> rolesForRemoval = new ArrayList<AssociationRoleIF>();
        Iterator roleIt = nodeRoles.iterator();
        while (roleIt.hasNext()) {
          AssociationRoleIF nodeRole = (AssociationRoleIF) roleIt.next();
          AssociationIF assoc = nodeRole.getAssociation();
          TopicIF assocType = assoc.getType();
          Collection<LocatorIF> typeStrings = assocType.getSubjectIdentifiers(); //getItemIdentifiers().iterator().next().getAddress();
          Iterator it = typeStrings.iterator();

          while(it.hasNext()){
            LocatorIF typeStringLocator = (LocatorIF) it.next();
            String typestring = typeStringLocator.getAddress();

            if (typestring.equals(assocTypeString)) {
              Iterator iter = assoc.getRoles().iterator();
              AssociationRoleIF otherRole = null;
              while (iter.hasNext()) {
                AssociationRoleIF role = (AssociationRoleIF) iter.next();
                if (!role.equals(nodeRole)) {
                  otherRole = role;
                  rolesForRemoval.add(otherRole);
                }
              }//end while roles
            }//end if correct assoctype
          }//end while iterator

        }//end while noderoles

        for (AssociationRoleIF forRemoval : rolesForRemoval) {
          forRemoval.getAssociation().remove();
        }
        topicMap.getStore().commit();
      }// end if noderoles to delete

    } catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(), e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }


  }

  public static String deleteAssocsByReifier(String player1, String player2, String assocTypeString, String reifierPsi, final TopicMapIF topicMap) throws NdlaServiceException { // TODO: The topicMap parameter is superfluous. Flagged for removal.
    String result = "";
    try{
    TopicIF player1Topic = topicMap.getTopicBySubjectIdentifier(new URILocator(player1));
    Collection<AssociationRoleIF> nodeRoles = player1Topic.getRoles();
    if (nodeRoles.size() > 0) {
        ArrayList<AssociationIF> assocsForRemoval = new ArrayList<AssociationIF>();
        Iterator roleIt = nodeRoles.iterator();
        while (roleIt.hasNext()) {
          AssociationRoleIF nodeRole = (AssociationRoleIF) roleIt.next();
          AssociationIF assoc = nodeRole.getAssociation();
          TopicIF reifier = assoc.getReifier();
          if(null != reifier){
            Collection reifierPsis = reifier.getSubjectIdentifiers();
            TopicIF assocType = assoc.getType();
            Collection typeStrings = assocType.getSubjectIdentifiers();


            if (typeStrings.contains(new URILocator(assocTypeString)) && reifierPsis.contains(new URILocator(reifierPsi))) {
              assocsForRemoval.add(assoc);
              break;
            }//end if correct assoctype
          }
        }//end while noderoles

        for (AssociationIF assocForRemoval : assocsForRemoval) {
          assocForRemoval.remove();
          TopicIF removedReifier = assocForRemoval.getReifier();
          if(null == removedReifier){
            result = reifierPsi;
          }
        }

      }// end if noderoles to delete

    }catch (MalformedURLException e) {
        throw new NdlaServiceException(e.getMessage());
    }
    return result;
  }

    public static void deleteAssocsByRole(TopicIF player1, TopicIF player2, String assocTypeString, final TopicMapIF topicMap) throws NdlaServiceException { // TODO: The topicMap parameter is superfluous. Flagged for removal.
        Collection<AssociationRoleIF> nodeRoles = player1.getRoles();
        String player2PSI = player2.getSubjectIdentifiers().iterator().next().getAddress();
        if (nodeRoles.size() > 0) {
          try {
            ArrayList<AssociationRoleIF> rolesForRemoval = new ArrayList<AssociationRoleIF>();
            Iterator roleIt = nodeRoles.iterator();
            while (roleIt.hasNext()) {
              AssociationRoleIF nodeRole = (AssociationRoleIF) roleIt.next();
              AssociationIF assoc = nodeRole.getAssociation();
              TopicIF assocType = assoc.getType();

              Collection typeStrings = assocType.getSubjectIdentifiers();
              //String typestring = assocType.getSubjectIdentifiers().iterator().next().getAddress();

              if (typeStrings.contains(new URILocator(assocTypeString))) {
                Iterator iter = assoc.getRoles().iterator();
                AssociationRoleIF otherRole = null;
                while (iter.hasNext()) {
                  AssociationRoleIF role = (AssociationRoleIF) iter.next();
                  if (!role.equals(nodeRole)) {
                    otherRole = role;
                    TopicIF playerTopic = otherRole.getPlayer();
                    String playerTopicPSI = playerTopic.getSubjectIdentifiers().iterator().next().getAddress();
                    if (playerTopicPSI.equals(player2PSI)) {
                      rolesForRemoval.add(otherRole);
                    }
                  }
                }//end while roles
              }//end if correct assoctype
            }//end while noderoles

            for (AssociationRoleIF forRemoval : rolesForRemoval) {
              forRemoval.getAssociation().remove();
            }
          }
          catch (MalformedURLException e) {
            throw new NdlaServiceException(e.getMessage());
          }
        }// end if noderoles to delete
    }

    public static HashMap<String, String> getTopicNames(TopicIF topic) {
        HashMap<String, String> result = new HashMap<String, String>();
        if (null != topic) {
            Collection<TopicNameIF> topicNames = topic.getTopicNames();
            for (TopicNameIF topicName : topicNames) {
                Collection<TopicIF> scopes = topicName.getScope();
                for (TopicIF scope : scopes) {
                    Collection<LocatorIF> locators = scope.getSubjectIdentifiers();
                    for (LocatorIF locator : locators) {
                        if (locator.getAddress().contains("#")) {
                            String langString = "";
                            if (!locator.getAddress().contains("language-neutral") && !wcs.contains(locator.getAddress())) {
                              langString = locator.getAddress();
                            }

                            if (langString != "" && !isEmpty(topicName.getValue())) {
                                result.put(langString, topicName.getValue());
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    public static HashMap<String, String> getTopicNames(TopicIF topicIf, String wordClass) {
        HashMap<String, String> result = new HashMap<String, String>();
        Collection<TopicNameIF> topicNames = topicIf.getTopicNames();

        for (TopicNameIF topicName : topicNames) {
            Collection<TopicIF> scopes = topicName.getScope();
            for (TopicIF scope : scopes) {
                Collection<LocatorIF> locators = scope.getSubjectIdentifiers();
                for (LocatorIF locator : locators) {
                    if (locator.getAddress().contains("#")) {
                        String langString = "";
                        if (!locator.getAddress().contains("language-neutral") && !wcs.contains(locator.getAddress())) {
                            //langString = Utils.determineLanguage(locator.getAddress());
                          langString = locator.getAddress();
                        }

                        if (langString != "" && !isEmpty(topicName.getValue()) && hasCorrectWordClass(topicName, wordClass)) {
                            result.put(langString, topicName.getValue());
                        }
                    }
                }
            }
        }
        return result;
    }

    public static boolean hasCorrectWordClass(TopicNameIF topicName, String wordClass) {
        boolean hasWordClass = false;
        Collection<TopicIF> scopes = topicName.getScope();
        for (TopicIF scope : scopes) {
            Collection<LocatorIF> locators = scope.getSubjectIdentifiers();
            boolean correctWordClass = false; // TODO: Flagged for removal (BK).
            for (LocatorIF locator : locators) {
                if (locator.getAddress().contains("#")) {
                    if (locator.getAddress().contains(wordClass)) {
                        hasWordClass = true;
                        break;
                    }
                }
            }
        }
        return hasWordClass;
    }

  public static HashMap<String,String> getReifierOccurrences(String _configFile, String _topicmapReferenceKey, String reifierPSI) throws NdlaServiceException, BadObjectReferenceException {
    HashMap<String,String> reifierOccurrences = new HashMap<>();
    TopicMapStoreIF store = null;
    TopicMapRepositoryIF repository = null;
    TopicMapReferenceIF reference = null;

    try {

      repository = XMLConfigSource.getRepository(_configFile);
      reference = repository.getReferenceByKey(_topicmapReferenceKey);

      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      TopicIF reifierTopic = topicMap.getTopicBySubjectIdentifier(new URILocator(reifierPSI));
      Collection<OccurrenceIF> reifierOccurrenceList = reifierTopic.getOccurrences();
      for(OccurrenceIF reifierOccurrence : reifierOccurrenceList) {
        TopicIF occType = reifierOccurrence.getType();
        String occurrenceType = occType.getSubjectIdentifiers().iterator().next().getAddress();
        String occurrenceValue = reifierOccurrence.getValue();
        reifierOccurrences.put(occurrenceType,occurrenceValue);
      }
    }
    catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(), e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return reifierOccurrences;
  }

  public static HashMap<String,String> getTopicAttributes(String _configFile, String _topicmapReferenceKey, String topicPSI) throws NdlaServiceException, BadObjectReferenceException {
    HashMap<String,String> topicAttributes = new HashMap<>();
    TopicMapStoreIF store = null;
    TopicMapRepositoryIF repository = null;
    TopicMapReferenceIF reference = null;

    try {

      repository = XMLConfigSource.getRepository(_configFile);
      reference = repository.getReferenceByKey(_topicmapReferenceKey);

      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      TopicIF topicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(topicPSI));

      TopicIF approvedOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approved"));
      TopicIF approvedDateOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#approval-date"));
      Collection<OccurrenceIF> approvedOccurrenceIFs = topicIF.getOccurrencesByType(approvedOccurrenceType);
      String approvedString = "false";
      for(OccurrenceIF approvedOccurrenceIF : approvedOccurrenceIFs){
        if(approvedOccurrenceIF.getValue().equals("true")) {
          approvedString = "true";
          break;
        }
      }
      topicAttributes.put("approved",approvedString);
      if(approvedString.equals("true")){
        String approvalDate = "";
        Collection<OccurrenceIF> approvedDateOccurrencesIFs = topicIF.getOccurrencesByType(approvedDateOccurrenceType);
        for(OccurrenceIF approvedDateOccurrencesIF : approvedDateOccurrencesIFs){
          if(approvedDateOccurrencesIF.getValue() != "") {
            approvalDate = approvedDateOccurrencesIF.getValue();
            break;
          }
        }

        if(approvalDate != ""){
          topicAttributes.put("approval_date",approvalDate);
        }
      }
      HashMap<String,String> existingProcessState = Utils.getProcessState(topicPSI, topicMap);
      if(existingProcessState.size() > 0){
        topicAttributes.put("processState",existingProcessState.get("processState"));
      }
      else{
        topicAttributes.put("processState","0");
      }

      TopicIF visibilityOccurrenceType = topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#keyword-visibility"));
      Collection<OccurrenceIF> visibilityOccurrencesIFs = topicIF.getOccurrencesByType(visibilityOccurrenceType);
      for(OccurrenceIF visibilityOccurrenceIF : visibilityOccurrencesIFs){
        if(visibilityOccurrenceIF.getValue() != "") {
          topicAttributes.put("visibility",visibilityOccurrenceIF.getValue());
          break;
        }
      }//end for
    }
    catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(), e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return topicAttributes;
  }

  public static HashMap<String,HashMap<String,String>> getTopicTypeAttributes(String _configFile, String _topicmapReferenceKey, String topicPSI) throws NdlaServiceException, BadObjectReferenceException {
    TopicMapStoreIF store = null;
    TopicMapRepositoryIF repository = null;
    TopicMapReferenceIF reference = null;
    HashMap<String,HashMap<String,String>> typeMap = new HashMap<>();

    try {

      repository = XMLConfigSource.getRepository(_configFile);
      reference = repository.getReferenceByKey(_topicmapReferenceKey);

      store = reference.createStore(false);
      TopicMapIF topicMap = store.getTopicMap();
      TopicIF topicIF = topicMap.getTopicBySubjectIdentifier(new URILocator(topicPSI));

      Collection<TopicIF> topicTypes = topicIF.getTypes();

      for (TopicIF topicType : topicTypes) {
        Collection<LocatorIF> topicTypeIds = topicType.getSubjectIdentifiers();
        String topicTypeId = "";

        for (LocatorIF topicTypeLocator : topicTypeIds) {
          String loc = topicTypeLocator.getAddress();
          if (loc.contains("#")) {
            topicTypeId = loc.substring(loc.lastIndexOf("#")+1);
          }
        } //end locator forloop

        Collection<TopicNameIF> typeNames = topicType.getTopicNames();
        HashMap<String,String> typeScopedNames = new HashMap<>();


        if (typeNames.size() > 0) {
          for (TopicNameIF typeName : typeNames) {
            Collection<TopicIF> typeScopes = typeName.getScope();
            for (TopicIF typeScope : typeScopes) {
              Collection<LocatorIF> typeScopeLocators = typeScope.getSubjectIdentifiers();
              for (LocatorIF typeScopeLocator : typeScopeLocators) {
                if (typeScopeLocator.getAddress().contains("#") && (typeScopeLocator.getAddress().contains("iso") || typeScopeLocator.getAddress().contains("language-neutral"))) {
                  typeScopedNames.put(typeScopeLocator.getAddress(),typeName.getValue());
                }//end if hash so we don't run itno the few subejctIdentifiers with no hash
              }//end for locators
            } //end for scopes
          }//end for names
        }//end if typenames

        if (topicTypeId.length() > 0) {
          typeMap.put(topicTypeId,typeScopedNames);
        }
      }//end for types
    }
    catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage(), e);
      }
    } finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
      if (repository != null) {
        repository.close();
      }
    }
    return typeMap;
  }
}
