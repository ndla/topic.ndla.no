package no.ndla.topics.service.model;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.infoset.impl.basic.URILocator;
import net.ontopia.topicmaps.core.*;
import net.ontopia.topicmaps.query.core.*;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.Utils;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by rolfguescini on 26.03.14.
 */
public class NdlaLanguage {
  ArrayList<NdlaTopic> languages;

  public NdlaLanguage(QueryProcessorIF queryProcessor) throws NdlaServiceException, BadObjectReferenceException  {
    this.languages = getCurrentLanguages(queryProcessor);
  }

  public ArrayList<NdlaTopic> getLanguages(){
    return this.languages;
  }

  public NdlaTopic getLanguage(String id){
    NdlaTopic result = null;
    for (NdlaTopic language : this.languages) {
      if (language.getPsi().equals(id)){
        result = language;
        break;
      }
    }
    return result;
  }

 synchronized public void addLanguages(String data, TopicMapIF _topicMap) throws NdlaServiceException, BadObjectReferenceException {
    TopicMapBuilderIF builder = _topicMap.getBuilder();
    TopicMapStoreIF store = _topicMap.getStore();
    LocatorIF baseAddress = store.getBaseAddress();

    try {
      JSONArray languages = new JSONArray(data);
      for (int i = 0; i < languages.length(); i++) {
        TopicIF langTopic = builder.makeTopic();
        TopicIF type = _topicMap.getTopicBySubjectIdentifier(new URILocator("http://psi.topic.ndla.no/#language"));
        langTopic.addType(type);

        JSONObject language = languages.getJSONObject(i);
        String langId = language.getString("languageId");
        langTopic.addSubjectIdentifier(new URILocator(langId));
        String id = langId.substring(langId.lastIndexOf("#")+1);
        langTopic.addItemIdentifier(baseAddress.resolveAbsolute("#" + id));

        JSONArray langNames = language.getJSONArray("names");
        for(int j = 0; j < langNames.length(); j++) {
          JSONObject langName = langNames.getJSONObject(j);

          String langNameLang = langName.getString("language");
          String langNameString = langName.getString("languageName").toLowerCase();
          Utils.createBasename(langTopic,langNameString,langNameLang,_topicMap);

        }//end for names
      }//end for languages
      store.commit(); // Persist object graph.
    }
    catch (Exception e) {
      if (store != null) {
        store.abort(); // Roll-back changes and deactivate (top-level) transaction.
        throw new NdlaServiceException(e.getMessage());
      }
      throw new NdlaServiceException(e.getMessage());
    }
    finally { // Release all topic map engine resources.
      if (store != null) {
        store.close();
      }
    }
  }

  private ArrayList<NdlaTopic> getCurrentLanguages(QueryProcessorIF queryProcessor) throws NdlaServiceException, BadObjectReferenceException {
    ArrayList<NdlaTopic> result = new ArrayList<>();
    if (null != queryProcessor) {
      ParsedStatementIF statement = null;
      String query = String.format("select $COLUMN from instance-of($COLUMN,i\"http://psi.topic.ndla.no/#language\")?");
      QueryResultIF tologResult = null;
      try {
        statement = queryProcessor.parse(query);
        if (null != statement) {
          tologResult = ((ParsedQueryIF) statement).execute();
          while (tologResult.next()) {
            result.add(new NdlaTopic((TopicIF) tologResult.getValue("COLUMN"), queryProcessor));
          }
        }
      } catch (BadObjectReferenceException e) {
        throw new BadObjectReferenceException(e.getMessage());
      } catch (InvalidQueryException e) {
        throw new NdlaServiceException(e.getMessage());
      } finally {
        if (null != tologResult) {
          tologResult.close();
        }
      }
    }//end if queryProcessor

    return result;
  }
}
