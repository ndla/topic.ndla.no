/*  Implemented by Brett Kromkamp (brett@seria.no)
    Date: April 03, 2013
 */

package no.ndla.topics.service.model;

import net.ontopia.infoset.core.LocatorIF;
import net.ontopia.topicmaps.core.OccurrenceIF;
import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.core.TopicMapIF;
import net.ontopia.topicmaps.core.TopicNameIF;
import net.ontopia.topicmaps.query.core.*;
import no.ndla.topics.service.NdlaServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NdlaTopic {

    // Constants.
    private static final String DESCRIPTION_PATTERN = "/#description}]$";
    private static final String INGRESS_PATTERN = "/#node-ingress}]$";
    private static final String LICENSE_PATTERN = "/#node-usufruct}]$";
    private static final String IMAGE_PATTERN = "/#image}]$";
    private static final String AUTHOR_PATTERN = "/#node-author}]$";
    private static final String NODETYPE_PATTERN = "/#node-type}]$";
    private static final String ORIGINATING_SITE_PATTERN = "/#originating-site}]$";
    private static final String VISIBILITY_PATTERN = "/#keyword-visibility}]$";
    private static final String UDIR_PATTERN = "/#udir-subject}]$";
    private static final String APPROVED_PATTERN = "/#approved}]$";
    private static final String PROCESS_STATE_PATTERN = "/#process-state}]$";
    private static final String APPROVAL_DATE_PATTERN = "/#approval-date}]$";
    private static final String EMAIL_PATTERN = "/#email}]$";

    private static final String DEFAULT_ORIGINATING_SITE = "http://ndla.no/";

    private static final int FRAGMENT_IDX = 1;

    private static Pattern _pattern;

    // Fields.
    private String _identifier;
    private String _psi;
    private String _udir_psi;
    private String _originatingSite;
    private String _visibility;
    private String _nodeType;
    private String _image;
    private TopicIF _topicIf;
    private String _itemIdentifier;
    private String _approved;
    private String _email;


    private String _processState;
    private String _approval_date;
    private HashMap<String, String> _names;
    private TreeMap<String, TreeMap<String, String>> _RelationNames;
    private TreeMap<String, TreeMap<String, String>> _RelationRoleNames;
    private ArrayList<NdlaWord> _wordNames;
    private ArrayList<String> _psis;


    private ArrayList<String> _identifiers = new ArrayList<>();
    private ArrayList<String> _itemIdentifiers = new ArrayList<>();
    private HashMap<String, String> _images = new HashMap<String, String>();;
    private ArrayList<String> _originating_sites = new ArrayList<>();
    private HashMap<String, String> _descriptions = new HashMap<String, String>();
    private HashMap<String, String> _ingresses = new HashMap<String, String>();
    private HashMap<String, String> _licenses = new HashMap<String, String>();
    private ArrayList<String> _authors = new ArrayList<String>();
    private HashMap<String,HashMap<String,String>> _types = new HashMap<>();

    Logger _logger = LoggerFactory.getLogger(NdlaTopic.class);

    // Constructor(s).
    public NdlaTopic(TopicIF topicIf, QueryProcessorIF queryProcessor) throws NdlaServiceException, BadObjectReferenceException {
        this._topicIf = topicIf;

      Collection<LocatorIF> subjItms = topicIf.getItemIdentifiers();
      Iterator itmIt = subjItms.iterator();
      while (itmIt.hasNext()) {
        LocatorIF itmLoc = (LocatorIF) itmIt.next();
        this._itemIdentifier = itmLoc.getAddress();
        this._itemIdentifiers.add(itmLoc.getAddress());
      }

        Collection<LocatorIF> subjIds = topicIf.getSubjectIdentifiers();
        Iterator subjIt = subjIds.iterator();
        if(subjIds.size() > 1){
          ArrayList<String> psiList = new ArrayList<>();

          while (subjIt.hasNext()) {
            LocatorIF subjLoc = (LocatorIF) subjIt.next();
            psiList.add(subjLoc.getAddress());
          }

          this._psis = psiList;
          this._psi = "";
        }
        else{
          this._psis = new ArrayList<>();
          while (subjIt.hasNext()) {
            LocatorIF subjLoc = (LocatorIF) subjIt.next();
            this._psi = subjLoc.getAddress();
            this._psis.add(this._psi);
          }
        }




        String[] split = null;
        if (this._psi != null && this._psi.length() > 0) {
            split = this._psi.split("#");
        }

        if (split != null && split.length == 2) {
            this._identifier = split[FRAGMENT_IDX];
        } else {
          if(this._psis.size() > 1){
            this._identifier = "";
            for(String psiString : this._psis){
              String[] fragmentedPsi = psiString.split("#");
              if(fragmentedPsi.length == 2){
                this._identifiers.add(fragmentedPsi[FRAGMENT_IDX]);
              }

            }
          }
          else{
            if (null != queryProcessor) {
              ParsedStatementIF statement = null;
              String query = String.format("item-identifier(i\"%s\", $COLUMN)?", this._psi);
              String result = "";
              QueryResultIF tologResult = null;
              try {
                statement = queryProcessor.parse(query);
                if (null != statement) {
                  tologResult = ((ParsedQueryIF) statement).execute();
                  if (tologResult.next()) {
                    result = tologResult.getValue("COLUMN").toString(); // file:/home/rolf/ontopia/topicmaps/topicndlano-ontologi.ltm#ndlanode_18891
                  }
                }
              } catch (BadObjectReferenceException e) {
                throw new BadObjectReferenceException(e.getMessage());
              } catch (InvalidQueryException e) {
                throw new NdlaServiceException(e.getMessage());
              } finally {
                if (null != tologResult) {
                  tologResult.close();
                }
              }
              split = result.split("#");
              if (split.length == 2) {
                this._identifier = split[FRAGMENT_IDX];
              }
            }
          }
        }

        Matcher matcher;
        Collection<OccurrenceIF> occurrences = topicIf.getOccurrences();
        for (OccurrenceIF occurrence : occurrences) {

            // Get topic's descriptions.
            _pattern = Pattern.compile(DESCRIPTION_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
                Collection<TopicIF> descriptionScopes = occurrence.getScope();
                for (TopicIF descriptionScope : descriptionScopes) {
                    Collection<LocatorIF> descriptionScopeSubjectIdentifiers = descriptionScope.getSubjectIdentifiers();
                    for (LocatorIF descriptionScopeSubjectIdentifier : descriptionScopeSubjectIdentifiers) {
                        if (descriptionScopeSubjectIdentifier.getAddress().contains("#")) {
                            this._descriptions.put(descriptionScopeSubjectIdentifier.getAddress(), occurrence.getValue());
                        }
                    }
                }
            }

            // Get topic's ingresses.
            _pattern = Pattern.compile(INGRESS_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
                Collection<TopicIF> ingressScopes = occurrence.getScope();
                for (TopicIF ingressScope : ingressScopes) {
                    Collection<LocatorIF> ingressScopeSubjectIdentifiers = ingressScope.getSubjectIdentifiers();
                    for (LocatorIF ingressScopeSubjectIdentifier : ingressScopeSubjectIdentifiers) {
                        if (ingressScopeSubjectIdentifier.getAddress().contains("#")) {
                            this._ingresses.put(ingressScopeSubjectIdentifier.getAddress(), occurrence.getValue());
                        }
                    }
                }
            }

            // Get topic's ingresses.
            _pattern = Pattern.compile(LICENSE_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
                Collection<TopicIF> licenseScopes = occurrence.getScope();
                for (TopicIF licenseScope : licenseScopes) {
                    Collection<LocatorIF> licenseScopeSubjectIdentifiers = licenseScope.getSubjectIdentifiers();
                    for (LocatorIF licenseScopeSubjectIdentifier : licenseScopeSubjectIdentifiers) {
                        if (licenseScopeSubjectIdentifier.getAddress().contains("#")) {
                            this._licenses.put(licenseScopeSubjectIdentifier.getAddress(), occurrence.getValue());
                        }
                    }
                }
            }

            // Get topic's authors.
            _pattern = Pattern.compile(AUTHOR_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
                this._authors.add(occurrence.getValue());
            }

            // Get topic's image.
            _pattern = Pattern.compile(IMAGE_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
              Collection<TopicIF> imageScopes = occurrence.getScope();
              if(imageScopes.size() > 0){
                for (TopicIF imageScope : imageScopes) {
                  Collection<LocatorIF> imageScopeSubjectIdentifiers = imageScope.getSubjectIdentifiers();
                  for (LocatorIF imageScopeSubjectIdentifier : imageScopeSubjectIdentifiers) {
                    if (imageScopeSubjectIdentifier.getAddress().contains("#")) {
                      this._images.put(imageScopeSubjectIdentifier.getAddress(), occurrence.getValue());
                    }
                  }
                }
              }
              else{
                this._image = occurrence.getValue();
              }


            }

            //get topics PROCESS_STATE_PATTERN
            _pattern = Pattern.compile(PROCESS_STATE_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
              this._processState = occurrence.getValue();
            }

            // Get topic's originating site.
            _pattern = Pattern.compile(ORIGINATING_SITE_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
                this._originatingSite = occurrence.getValue();
                this._originating_sites.add(occurrence.getValue());
            }

            // Get topic's originating site.
            _pattern = Pattern.compile(VISIBILITY_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
                this._visibility = occurrence.getValue();
            }

            // Get topic's originating site.
            _pattern = Pattern.compile(NODETYPE_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
                this._nodeType = occurrence.getValue();
            }

            // Get topic's udir subject
            _pattern = Pattern.compile(UDIR_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
              this._udir_psi = occurrence.getValue();
            }

            // Get topic's approved status
            _pattern = Pattern.compile(APPROVED_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
              this._approved = occurrence.getValue();
            }

            // Get topic's approved status
            _pattern = Pattern.compile(APPROVAL_DATE_PATTERN);
            matcher = _pattern.matcher(occurrence.getType().toString());
            if (matcher.find()) {
              this._approval_date = occurrence.getValue();
            }

          // Get person topic's email address.
          _pattern = Pattern.compile(EMAIL_PATTERN);
          matcher = _pattern.matcher(occurrence.getType().toString());
          if (matcher.find()) {
            this._email = occurrence.getValue();
          }
        }

        boolean isWord = false;
        if (topicIf.getTypes().iterator().hasNext()) {
            TopicIF topicType = topicIf.getTypes().iterator().next();
            String psi = topicType.getSubjectIdentifiers().iterator().next().getAddress();
            if (psi.contains("#") && (psi.contains("#keyword") || psi.contains("#topic"))) {
                isWord = true;
            }
        }

        if (isWord) {
            HashMap<String, ArrayList<TopicNameIF>> nameMap = new HashMap<>();
            nameMap.put("verb", new ArrayList<TopicNameIF>());
            nameMap.put("noun", new ArrayList<TopicNameIF>());
            nameMap.put("adverb", new ArrayList<TopicNameIF>());
            nameMap.put("adjective", new ArrayList<TopicNameIF>());
            nameMap.put("preposition", new ArrayList<TopicNameIF>());
            nameMap.put("pronoun", new ArrayList<TopicNameIF>());
            nameMap.put("conjunction", new ArrayList<TopicNameIF>());
            nameMap.put("determiner", new ArrayList<TopicNameIF>());
            nameMap.put("neutral", new ArrayList<TopicNameIF>());

            boolean hasWc = false;

            Collection<TopicNameIF> topicNames = topicIf.getTopicNames();
            Collection<TopicIF> topicTypes = topicIf.getTypes();
            for (TopicIF topicType : topicTypes) {
              Collection<LocatorIF> topicTypeIds = topicType.getSubjectIdentifiers();
              String topicTypeId = "";

              for (LocatorIF topicTypeLocator : topicTypeIds) {
                String loc = topicTypeLocator.getAddress();
                if (loc.contains("#")) {
                  topicTypeId = loc.substring(loc.lastIndexOf("#")+1);
                }
              } //end locator forloop

              Collection<TopicNameIF> typeNames = topicType.getTopicNames();
              HashMap<String,String> typeScopedNames = new HashMap<>();


              if (typeNames.size() > 0) {
                for (TopicNameIF typeName : typeNames) {
                  Collection<TopicIF> typeScopes = typeName.getScope();
                  for (TopicIF typeScope : typeScopes) {
                    Collection<LocatorIF> typeScopeLocators = typeScope.getSubjectIdentifiers();
                    for (LocatorIF typeScopeLocator : typeScopeLocators) {
                      if (typeScopeLocator.getAddress().contains("#") && (typeScopeLocator.getAddress().contains("iso") || typeScopeLocator.getAddress().contains("language-neutral"))) {
                        typeScopedNames.put(typeScopeLocator.getAddress(),typeName.getValue());
                      }//end if hash so we don't run itno the few subejctIdentifiers with no hash
                    }//end for locators
                  } //end for scopes
                }//end for names
              }//end if typenames

              if (topicTypeId.length() > 0) {
                _types.put(topicTypeId,typeScopedNames);
              }

            }


          for (TopicNameIF topicName : topicNames) {
                Collection<TopicIF> scopes = topicName.getScope();
                HashMap<String, String> scopeMap = new HashMap<>(); // TODO: Flagged for removal (BK).
                for (TopicIF scope : scopes) {
                    Collection<LocatorIF> locators = scope.getSubjectIdentifiers();
                    for (LocatorIF locator : locators) {
                        if (locator.getAddress().contains("#")) {
                            String loc = locator.getAddress().substring(locator.getAddress().lastIndexOf("#") + 1);
                            if (nameMap.containsKey(loc)) {
                                nameMap.get(loc).add(topicName);
                                hasWc = true;
                            }
                        }//end if hash so we don't run itno the few subejctIdentifiers with no hash
                    }//end for locators
                } //end for scopes
            }//end for names

            if (hasWc) {
                this._wordNames = new ArrayList<NdlaWord>();
                Iterator nit = nameMap.keySet().iterator();
                while (nit.hasNext()) {
                    String wc = (String) nit.next();
                    ArrayList<TopicNameIF> names = nameMap.get(wc);
                    if (names.size() > 0) {
                        for (TopicNameIF topicName : names) {
                            Collection<TopicIF> scopes = topicName.getScope();
                            HashMap<String, String> currscopednames = new HashMap<>();

                            for (TopicIF scope : scopes) {
                                Collection<LocatorIF> locators = scope.getSubjectIdentifiers();
                                for (LocatorIF locator : locators) {
                                    if (locator.getAddress().contains("#") && (locator.getAddress().contains("iso") || locator.getAddress().contains("language-neutral"))) {
                                        currscopednames.put(locator.getAddress(), topicName.getValue());
                                    }
                                }
                            }//end for scopes
                            NdlaWord currword = new NdlaWord(wc, currscopednames);
                            this._wordNames.add(currword);
                        }
                    }
                }//end while iterator nit
            } else {
                this._names = new HashMap<String, String>();
                for (TopicNameIF topicName : topicNames) {
                    Collection<TopicIF> scopes = topicName.getScope();
                    for (TopicIF scope : scopes) {
                        Collection<LocatorIF> locators = scope.getSubjectIdentifiers();
                        for (LocatorIF locator : locators) {
                            if (locator.getAddress().contains("#")) {
                                this._names.put(locator.getAddress(), topicName.getValue());
                            }
                        }
                    }
                }
            }
        } else {
          String topicTypeId = "";
          Collection<TopicIF> topicTypes = topicIf.getTypes();
          boolean isRelationshipTypeType = false;
          for(TopicIF topicType : topicTypes) {
            Collection<LocatorIF> typeLocators = topicType.getSubjectIdentifiers();
            for (LocatorIF typeLocator : typeLocators) {
              topicTypeId = typeLocator.getAddress().substring(typeLocator.getAddress().lastIndexOf("#")+1);
              if(typeLocator.getAddress().contains("hierarchical-relation-type") || typeLocator.getAddress().contains("horisontal-relation-type")){
                isRelationshipTypeType = true;
                break;
              }
              else{
                Collection<TopicNameIF> typeNames = topicType.getTopicNames();
                HashMap<String,String> typeScopedNames = new HashMap<>();
                if (typeNames.size() > 0) {
                  for (TopicNameIF typeName : typeNames) {
                    Collection<TopicIF> typeScopes = typeName.getScope();
                    for (TopicIF typeScope : typeScopes) {
                      Collection<LocatorIF> typeScopeLocators = typeScope.getSubjectIdentifiers();
                      for (LocatorIF typeScopeLocator : typeScopeLocators) {
                        if (typeScopeLocator.getAddress().contains("#") && (typeScopeLocator.getAddress().contains("iso") || typeScopeLocator.getAddress().contains("language-neutral"))) {
                          typeScopedNames.put(typeScopeLocator.getAddress(),typeName.getValue());
                        }//end if hash so we don't run itno the few subejctIdentifiers with no hash
                      }//end for locators
                    } //end for scopes
                  }//end for names
                }//end if typenames

                if (topicTypeId.length() > 0) {
                  _types.put(topicTypeId,typeScopedNames);
                }
              }
            }
          }

          if(isRelationshipTypeType) {
            this._RelationNames = new TreeMap<>();
            this._RelationRoleNames = new TreeMap<>();
            Collection<TopicNameIF> topicNames = topicIf.getTopicNames();
            for (TopicNameIF topicName : topicNames) {
              String objectId = topicName.getObjectId();
              Collection<TopicIF> scopes = topicName.getScope();
              TreeMap<String, String> nameData = new TreeMap<>();
              TreeMap<String, String> roleData = new TreeMap<>();
              for (TopicIF scope : scopes) {
                Collection<LocatorIF> locators = scope.getSubjectIdentifiers();
                for (LocatorIF locator : locators) {
                  if(!locator.getAddress().contains("language-neutral")) {
                    nameData.put(locator.getAddress(), topicName.getValue());
                  }

                  if(!locator.getAddress().contains("iso")) {
                    TopicMapIF _topicMap = topicIf.getTopicMap();
                    TopicIF roleTopicIF = _topicMap.getTopicBySubjectIdentifier(locator);
                    Collection<TopicNameIF> roleTopicNames = roleTopicIF.getTopicNames();
                    for (TopicNameIF roleTopicName : roleTopicNames) {
                      Collection<TopicIF> roleTopicNameScopes = roleTopicName.getScope();
                      for (TopicIF roleTopicNameScope : roleTopicNameScopes) {
                        Collection<LocatorIF> roleTopicNameLocators = roleTopicNameScope.getSubjectIdentifiers();
                        for (LocatorIF roleTopicNameLocator : roleTopicNameLocators) {
                          if (!locator.getAddress().contains("language-neutral")) {
                            roleData.put(locator.getAddress()+"_"+roleTopicNameLocator.getAddress(), roleTopicName.getValue());
                          }//end if not lang-neutral
                        }//end for roleTopicNameLocators
                      }//end for roleTopicNames
                    }//end for roleTopicNames
                  }//end if not iso
                }//end for locators
              }
              this._RelationNames.put(objectId,nameData);
              this._RelationRoleNames.put(objectId,roleData);
            }
          }
          else{
            this._names = new HashMap<String, String>();
            Collection<TopicNameIF> topicNames = topicIf.getTopicNames();
            for (TopicNameIF topicName : topicNames) {
              Collection<TopicIF> scopes = topicName.getScope();
              for (TopicIF scope : scopes) {
                Collection<LocatorIF> locators = scope.getSubjectIdentifiers();
                for (LocatorIF locator : locators) {
                  if (locator.getAddress().contains("#")) {
                    this._names.put(locator.getAddress(), topicName.getValue());
                  }
                }
              }
            }//end for topicNames

          }//end if relationshipTypeType
        }
    }

    public String getIdentifier() {
        return _identifier;
    }

    public ArrayList<String> getIdentifiers() {
      return _identifiers;
    }

    public String getItemIdentifier() {
        return _itemIdentifier;
    }

    public ArrayList<String> getItemIdentifiers() {
      return _itemIdentifiers;
    }


  public String getPsi() {
        return _psi;
    }
    public ArrayList<String> getPsis() {
      return _psis;
    }

    public HashMap<String,String> getImages(){
      return _images;
    }


    public String getOriginatingSite() {
        return _originatingSite;
    }

   public ArrayList<String> getOriginatingSites() {
    return _originating_sites;
  }

    public String getVisibility() {
        return _visibility;
    }

    public String getNodeType() {
        return _nodeType;
    }

    public HashMap<String, String> getDescriptions() {
        return _descriptions;
    }

    public ArrayList<String> getAuthors() {
        return _authors;
    }

    public HashMap<String, String> getIngresses() {
        return _ingresses;
    }

    public HashMap<String, String> getLicenses() {
        return _licenses;
    }

    public String getImage() {
        return _image;
    }

    public String getUdirPsi() {return _udir_psi; }

    public TopicIF getTopicIf() {
        return _topicIf;
    }

    public HashMap<String, String> getNames() {
        return _names;
    }

    public TreeMap<String, TreeMap<String, String>> getRelationshipNames() {
      return _RelationNames;
    }
    public TreeMap<String, TreeMap<String, String>> getRelationshipRoleNames() {
    return _RelationRoleNames;
  }

    public ArrayList<NdlaWord> getWordNames() {
        return _wordNames;
    }

    public HashMap<String, HashMap<String,String>> getTypes() {
      return _types;
    }

    public  String getApproved() {
      return this._approved;
    }

    public  String getApprovalDate() {
      return this._approval_date;
    }

    public String getProcessState() {
      String processState = "0";
      if(null != this._processState){
        processState = this._processState;
      }
      return processState;
    }

    public String getEmail() {
      return _email;
    }

}
