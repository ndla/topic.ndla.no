/*  Implemented by Brett Kromkamp (brett@seria.no)
    Date: April 03, 2013
 */

package no.ndla.topics.service.model;

import java.util.HashMap;

public final class NdlaWord {
    private String _wordClass;
    private HashMap<String,String> _names;

    public NdlaWord (String wordClass, HashMap<String,String> names) {
        this._wordClass = wordClass;
        this._names = names;
    }

    public String getWordClass() {
        return _wordClass;
    }

    public HashMap<String,String> getNames() {
        return _names;
    }
}
