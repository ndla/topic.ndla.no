/*  Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
    Date: April 9, 2013
 */
package no.ndla.service.test;

import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

public class TestAim {

  private static final String CONFIG_FILE_PATH = "/Users/rolfguescini/adminconfig/tm-sources.xml";
  private static final String TOPICMAP_REFERENCE_KEY = "NdlaTopicsService";


    public void getAimsByTopicId() {
        Collection<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getAimsByTopicId(10, 0, "topic-1", NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }


  public void getTopicsByAimId() {
    Collection<NdlaTopic> topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      topics = topicService.getTopicsByAimId(30,0,"K14884",NdlaSite.NDLA);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  @Test
  public void getAimTopicsBySubjectId() {
    JSONObject topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      topics = topicService.getAimTopicsBySubjectId(100,0,"subject_137413",NdlaSite.NDLA);
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
      }
  }


  public void getAimTopicAssociations() {
    JSONObject topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      topics = topicService.getAimTopicAssociations("K9858","http://psi.oasis-open.org/iso/639/#eng");
      System.out.println("TÅPIKKS: "+topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }




  public void saveAimHasTopic(){
    ArrayList<TopicIF> savedRelations = new ArrayList<>();
    String payload = "{\"aimtopics\": [\n" +
        "    {\n" +
        "        \"aimId\": \"K9858\",\n" +
        "        \"topics\": [\n" +
        "            {\n" +
        "                \"topicId\": \"topic-1432800776OPFLQPDZCI\",\n" +
        "                \"relationTypeId\": \"http://psi.topic.ndla.no/aim_has_topic\",\n" +
        "                \"roleTypeId1\": \"http://psi.topic.ndla.no/#topic\",\n" +
        "                \"roleTypeId2\": \"http://psi.udir.no/ontologi/lkt/#kompetansemaal\"\n" +
        "            },\n" +
        "            {\n" +
        "                \"topicId\": \"topic-1432727098MIRZABRNUD\",\n" +
        "                \"relationTypeId\": \"http://psi.topic.ndla.no/aim_has_topic\",\n" +
        "                \"roleTypeId1\": \"http://psi.topic.ndla.no/#topic\",\n" +
        "                \"roleTypeId2\": \"http://psi.udir.no/ontologi/lkt/#kompetansemaal\"\n" +
        "            }\n" +
        "        ]\n" +
        "    },\n" +
        "    {\n" +
        "        \"aimId\": \"K9855\",\n" +
        "        \"topics\": [{\n" +
        "            \"topicId\": \"topic-1432733578FKDVQZHYPT\",\n" +
        "            \"relationTypeId\": \"http://psi.topic.ndla.no/aim_has_topic\",\n" +
        "            \"roleTypeId1\": \"http://psi.topic.ndla.no/#topic\",\n" +
        "            \"roleTypeId2\": \"http://psi.udir.no/ontologi/lkt/#kompetansemaal\"\n" +
        "        }]\n" +
        "    }\n" +
        "]}";

    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      savedRelations = topicService.saveAimHasTopic("subject_BRH3-01",payload,NdlaSite.NDLA);
      System.out.println(savedRelations);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }

  }

    public void createAimTopics() {
        HashMap<String, TopicIF> topics = null;

        HashMap<String, HashMap<String, HashMap<String, String>>> args = new HashMap<String, HashMap<String, HashMap<String, String>>>();
        HashMap<String, HashMap<String, String>> data1 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> data2 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> data3 = new HashMap<String, HashMap<String, String>>();

        HashMap<String, HashMap<String, String>> namedata1 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> namedata2 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> namedata3 = new HashMap<String, HashMap<String, String>>();

        HashMap<String, String> names1 = new HashMap<String, String>();
        HashMap<String, String> names2 = new HashMap<String, String>();
        HashMap<String, String> names3 = new HashMap<String, String>();
        HashMap<String, String> data_elm1 = new HashMap<String, String>();
        HashMap<String, String> data_elm2 = new HashMap<String, String>();
        HashMap<String, String> data_elm3 = new HashMap<String, String>();

        names1.put("nb", "Tester kompetanesmålsemne");
        names1.put("nn", "Testar kompetansmaalsemne");
        names1.put("en", "Testing competenceaim topic");
        data1.put("names", names1);

        data_elm1.put("en#description", "These topics are intended to be connected to komeptence aims as well as subjects to form subject ontologies.");
        data_elm1.put("nb#description", "Disse emnene er ment å skulle relateres til kompetansemål og til fag for å danne fagontologier.");
        data_elm1.put("nb#description", "Desse emnene er meint å skulle relateres til kompetansemål og til fag for å danne fagontologiar.");
        data_elm1.put("image", "http://upload.wikimedia.org/wikipedia/commons/9/90/Thai_Seafood_Curry.jpg");
        data1.put("occurrences", data_elm1);

        names2.put("en", "showing");
        data2.put("names", names2);
        data2.put("occurrences", data_elm2);

        args.put("set1", data1);
        args.put("set2", data2);

        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.createAimTopics(args);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }


    public void deleteTopicFromAim() {
        Collection<TopicIF> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.deleteTopicFromAim("kompetansemaal_ENG4-01_H03_K11417", "topic-KCXKTKTXWT", NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }
}
