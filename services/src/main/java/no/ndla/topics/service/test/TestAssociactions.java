package no.ndla.service.test;

/**
 * Created with IntelliJ IDEA.
 * User: rolf
 * Date: 27.06.13
 * Time: 16:14
 * To change this template use File | Settings | File Templates.
 */

import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import no.ndla.topics.service.model.NdlaWord;
import org.codehaus.jettison.json.JSONArray;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

public class TestAssociactions {
  private static final String CONFIG_FILE_PATH = "/Users/rolfguescini/adminconfig/tm-sources.xml";
  private static final String TOPICMAP_REFERENCE_KEY = "NdlaTopicsService";


  public void removeAssociationByReifier() {
    ArrayList<String> topics = null;
    String payload = "\n" +
        "{\n" +
        "    \"topicId\": \"topic-1440152125FMWTCIKKMN\",\n" +
        "    \"topicType\": \"topic\",\n" +
        "    \"related_topicIds\": [\n" +
        "        {\n" +
        "            \"relatedTopicId\": \"topic-1440152136JMPOIBVCYK\",\n" +
        "            \"relatedTopicType\": \"topic\"\n" +
        "        }\n" +
        "    ],\n" +
        "    \"relationType\": \"hovedomraader\",\n" +
        "    \"reifierId\": \"http://psi.topic.ndla.no/reifiers/#reifiedAssociation-1440574666TFDZMUKMPS\"\n" +
        "}";

    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      topics = topicService.removeAssociationByReifier(payload, NdlaSite.NDLA);
      System.out.println(topics);
    } catch (NdlaServiceException e) {
      System.out.println(e.getMessage());
    }

  }

  @Test
    public void removeAssociation() {
        HashMap<String, TopicIF> topics = null;
        String payload = "\n" +
                "{\n" +
            "    \"topicId\": \"K794\",\n" +
            "    \"topicType\": \"kompetansemaal\",\n" +
            "    \"related_topicIds\": [\n" +
            /*
            "        {\n" +
            "            \"relatedTopicId\": \"topic-1443599812HKCGBZNUWB\",\n" +
            "            \"relatedTopicType\": \"topic\"\n" +
            "        },\n" +
            "        {\n" +
            "            \"relatedTopicId\": \"topic-1439560323IRBGWCPOIF\",\n" +
            "            \"relatedTopicType\": \"topic\"\n" +
            "        },\n" +
            */
            "        {\n" +
            "            \"relatedTopicId\": \"topic-1443599798EZKLLOGBEY\",\n" +
            "            \"relatedTopicType\": \"topic\"\n" +
            "        }\n" +

            "    ],\n" +
            "    \"relationType\": \"primary-topic\"\n" +
            "}";

        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.removeAssociation(payload, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }

    }

    public void addAssociation() {
        HashMap<String, TopicIF> topics = null;
      /*
        String payload = "\n" +
                "{\n" +
                "\"topicId\": \"ndlanode_125930\",\n" +
                "\"topicType\": \"ndla-node\",\n" +
                "\"related_topicIds\": [\n" +
                "     {\n" +
                "        \"relatedTopicId\": \"keyword-114406\",\n" +
                "        \"relatedTopicType\": \"keyword\"\n" +
                "     },\n" +
                "     {\n" +
                "        \"relatedTopicId\": \"keyword-51152\",\n" +
                "        \"relatedTopicType\": \"keyword\"\n" +
                "     },\n" +
                "     {\n" +
                "        \"relatedTopicId\": \"keyword-93077\",\n" +
                "        \"relatedTopicType\": \"keyword\"\n" +
                "     },\n" +
                "     {\n" +
                "        \"relatedTopicId\": \"keyword-DJUREIKEFU\",\n" +
                "        \"relatedTopicType\": \"keyword\"\n" +
                "     },\n" +
                "     {\n" +
                "        \"relatedTopicId\": \"keyword-IBQRIFQLMJ\",\n" +
                "        \"relatedTopicType\": \"keyword\"\n" +
                "     }\n" +

                "],\n" +
                "\"relationType\": \"ndlanode-keyword\"\n" +
                "}";
                */
      String payload = "{\n" +
          "    \"topicId\": \"ndlanode_1556\",\n" +
          "    \"topicType\": \"ndla-node\",\n" +
          "    \"related_topicIds\": [\n" +
          "        {\n" +
          "            \"relatedTopicId\": \"keyword-1387109691ISQPPQPYNI\",\n" +
          "            \"relatedTopicType\": \"keyword\"\n" +
          "        }\n" +
          "    ],\n" +
          "    \"relationType\": \"ndlanode-keyword\"\n" +
          "}";

        try {
          TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
            topics = topicService.addAssociation(payload, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics.size() > 0) {
            Iterator it = topics.keySet().iterator();
            while (it.hasNext()) {
                TopicIF tmp = (TopicIF) topics.get(it.next());
                System.out.println(tmp.getSubjectIdentifiers().toString());
            }
        }
    }




  public void createRelationType() {
    HashMap<String, TopicIF> topics = null;

    String payload_existing_roles = "{\n" +
        "    \"relationTypes\": [\n" +
        "        {\n" +
        "            \"data-name\": \"precedes-topic\",\n" +
        "            \"relationType-type\": \"horisontal-relation-type\",\n" +
        "            \"names\": [\n" +
        "                {\n" +
        "                    \"language\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "                    \"name\": \"Følger\",\n" +
        "                    \"role-topics\": [\n" +
        "                        {\n" +
        "                            \"roleTopicPsi\": \"http://psi.topic.ndla.no/#follower\",\n" +
        "                            \"roleTopicType\": \"http://psi.topic.ndla.no/#subordinate-role-type\",\n" +
        "                            \"names\": []\n" +
        "                        }\n" +
        "                    ]\n" +
        "                },\n" +
        "                {\n" +
        "                    \"language\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "                    \"name\": \"Forut for\",\n" +
        "                    \"role-topics\": [\n" +
        "                        {\n" +
        "                            \"roleTopicPsi\": \"http://psi.topic.ndla.no/#preceder\",\n" +
        "                            \"roleTopicType\": \"http://psi.topic.ndla.no/#superordinate-role-type\",\n" +
        "                            \"names\": []\n" +
        "                        }\n" +
        "                    ]\n" +
        "                },\n" +
        "                {\n" +
        "                    \"language\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "                    \"name\": \"Follows\",\n" +
        "                    \"role-topics\": [\n" +
        "                        {\n" +
        "                            \"roleTopicPsi\": \"http://psi.topic.ndla.no/topics/#follower\",\n" +
        "                            \"roleTopicType\": \"http://psi.topic.ndla.no/#subordinate-role-type\",\n" +
        "                            \"names\": []\n" +
        "                        }\n" +
        "                    ]\n" +
        "                },\n" +
        "                {\n" +
        "                    \"language\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "                    \"name\": \"Precedes\",\n" +
        "                    \"role-topics\": [\n" +
        "                        {\n" +
        "                            \"roleTopicPsi\": \"http://psi.topic.ndla.no/topics/#preceder\",\n" +
        "                            \"roleTopicType\": \"http://psi.topic.ndla.no/#superordinate-role-type\",\n" +
        "                            \"names\": []\n" +
        "                        }\n" +
        "                    ]\n" +
        "                }\n" +
        "            ]\n" +
        "        }\n" +
        "    ]\n" +
        "}";
/*
    String payload_new_roles = "{\n" +
        "    \"relationTypes\": [\n" +
        "        {\n" +
        "            \"data-name\": \"rel-boo-boo2\",\n" +
        "            \"names\": [\n" +
        "                {\n" +
        "                    \"language\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "                    \"name\": \"Har bobo2\",\n" +
        "                    \"role-topics\": [\n" +
        "                        {\n" +
        "                            \"names\": [\n" +
        "                                {\n" +
        "                                    \"language\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "                                    \"name\": \"HarBoboRolle2\"\n" +
        "                                }\n" +
        "                            ],\n" +
        "                            \"roleTopicPsi\": \"http://psi.topic.ndla.no/#harBoboRolle2\",\n" +
        "                            \"roleTopicType\": \"http://psi.topic.ndla.no/#superordinate-role-type\"\n" +
        "                        }\n" +
        "                    ]\n" +
        "                },\n" +
        "                {\n" +
        "                    \"language\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "                    \"name\": \"Bobo har2\",\n" +
        "                    \"role-topics\": [\n" +
        "                        {\n" +
        "                            \"names\": [\n" +
        "                                {\n" +
        "                                    \"language\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "                                    \"name\": \"BoboHarRolle2\"\n" +
        "                                }\n" +
        "                            ],\n" +
        "                            \"roleTopicPsi\": \"http://psi.topic.ndla.no/#BoboHarRolle2\",\n" +
        "                            \"roleTopicType\": \"http://psi.topic.ndla.no/#subordinate-role-type\"\n" +
        "                        }\n" +
        "                    ]\n" +
        "                }\n" +
        "            ],\n" +
        "            \"relationType-type\": \"horisontal-relation-type\"\n" +
        "        }\n" +
        "    ]\n" +
        "}\n";
        */
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      topics = topicService.createAssociationTypes(payload_existing_roles);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void updateRelationType() {
    HashMap<String, TopicIF> topics = null;
    String payload = "{\n" +
        "    \"topicId\": \"http://psi.topic.ndla.no/#connection-strength\",\n" +
        "    \"relationTypeIds\": [\n" +
        "        \"http://psi.topic.ndla.no/#horisontal-relation-type\"\n" +
        "    ],\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "            \"name\": \"Incoming connection strength\",\n" +
        "            \"roleTopicPsi\": \"http://psi.topic.ndla.no/#incoming\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "            \"name\": \"Relasjonsstyrke innkommende\",\n" +
        "            \"roleTopicPsi\": \"http://psi.topic.ndla.no/#incoming\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "            \"name\": \"Outgoing connection strength\",\n" +
        "            \"roleTopicPsi\": \"http://psi.topic.ndla.no/#outgoing\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "            \"name\": \"Relasjonsstyrke utgående\",\n" +
        "            \"roleTopicPsi\": \"http://psi.topic.ndla.no/#outgoing\"\n" +
        "        }\n" +
        "    ]\n" +
        "}";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search2", "topics-user", "index_My_Topics_13");
      topics = topicService.updateAssociationTypeByAssocTypeId(payload);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void getTopicPlayersByRelationTypeId(){
    HashMap<String,ArrayList<NdlaTopic>> topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search2", "topics-user", "index_My_Topics_13");
      topics = topicService.getTopicPlayersByRelationTypeId("http://psi.topic.ndla.no/#subject_ontology_has_topic","subjectmatter","topic");
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void deleteAssociationTypeByTopicId(){
    ArrayList<String> deletedTopics = new ArrayList<>();
    String payload = "[\"http://psi.topic.ndla.no/#so-tester-vi-igjen\"]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      deletedTopics = topicService.deleteAssociationTypeByTopicId(payload);
      System.out.println(deletedTopics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void getHorizontalAssociationType(){
    NdlaTopic assocType = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      assocType = topicService.getHorizontalAssociationType("precedes-topic");
      System.out.println(assocType.toString());
      TreeMap<String, TreeMap<String, String>> names = assocType.getRelationshipNames();
      System.out.println("NAMES: "+names);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getHierarchicalAssociationType(){
    NdlaTopic assocType = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      assocType = topicService.getHierarchicalAssociationType("hovedomraader");
      System.out.println(assocType.toString());
      TreeMap<String, TreeMap<String, String>> names = assocType.getRelationshipNames();
      System.out.println("NAMES: "+names);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getHierarchicalAssociationTypes(){
    ArrayList<NdlaTopic> assocTypes = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      assocTypes = topicService.getHierarchicalAssociationTypes();
      for(NdlaTopic hierarchicalType : assocTypes) {
        TreeMap<String, TreeMap<String, String>> names = hierarchicalType.getRelationshipNames();
        System.out.println("NAMES: "+names);
      }

    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getSubordinateRoleTypes(){
    JSONArray assocTypes = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      assocTypes = topicService.getSubordinateRoleTypes();

        System.out.println(assocTypes);


    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }

  public void getSuperordinateRoleTypes(){
    JSONArray assocTypes = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      assocTypes = topicService.getSuperordinateRoleTypes();
      System.out.println(assocTypes);

    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getRoleType(){
    NdlaTopic assocType = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      assocType = topicService.getRoleType("topic-1426155334AVUNDECNGM");
      System.out.println(assocType.toString());
      HashMap<String, String> names = assocType.getNames();

      System.out.println("NAMES: "+names);
      ArrayList<NdlaWord> wnames = assocType.getWordNames();
      System.out.println("wnames: "+wnames);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



}
