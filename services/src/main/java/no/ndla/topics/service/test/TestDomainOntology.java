/*  Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
    Date: April 8, 2013
 */
package no.ndla.service.test;

import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.TreeMap;

public class TestDomainOntology {

  private static final String CONFIG_FILE_PATH = "/Users/rolfguescini/adminconfig/tm-sources.xml";
  private static final String TOPICMAP_REFERENCE_KEY = "NdlaTopicsService";


  public void createOntologyDomain() {
    JSONObject createdOntologyDomain = null;
    String payload = "{\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"languageId\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "            \"name\": \"Year Wheel\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"languageId\": \"http://psi.oasis-open.org/iso/639/#ara\",\n" +
        "            \"name\": \"برنامج تعليمي للهندسة جيدالعربية\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"languageId\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "            \"name\": \"Årshjul\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"languageId\": \"http://psi.oasis-open.org/iso/639/#nno\",\n" +
        "            \"name\": \"Årshjul\"\n" +
        "        }\n" +
        "    ]\n" +
        "}";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      createdOntologyDomain = topicService.createOntologyDomain(payload);
      if(null != createdOntologyDomain){
        System.out.println(createdOntologyDomain.toString());
      }


    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }



  public void updateOntologyDomain() {
    JSONObject updatedOntologyDomain = null;
    String payload = "{\n" +
        "    \"domainId\": \"ontologyDomain-1437033328NSHTUTWZMJ\",\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"languageId\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "            \"name\": \"Year Wheel\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"languageId\": \"http://psi.oasis-open.org/iso/639/#deu\",\n" +
        "            \"name\": \"JahresRad\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"languageId\": \"http://psi.oasis-open.org/iso/639/#ara\",\n" +
        "            \"name\": \"برنامج تعليمي للهندسة جيدالعربية\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"languageId\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "            \"name\": \"Årshjul\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"languageId\": \"http://psi.oasis-open.org/iso/639/#nno\",\n" +
        "            \"name\": \"Årshjul\"\n" +
        "        }\n" +
        "    ]\n" +
        "}";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      updatedOntologyDomain = topicService.updateOntologyDomain(payload);
      if(null != updatedOntologyDomain){
        System.out.println(updatedOntologyDomain.toString());
      }


    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }


  public void deleteOntologyDomains() {
    JSONObject deletedTopic = null;
    String payload = "[\"ontologyDomain-1436523987YBNUNGLJCI\"]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      deletedTopic = topicService.deleteOntologyDomains(payload);
      if(deletedTopic.length() > 0){
        System.out.println(deletedTopic.toString());
      }


    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }



  public void createPerson() {
    JSONObject createdPerson = null;
    String payload = "{\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"languageId\": \"http://psi.topic.ndla.no/#language-neutral\",\n" +
        "            \"name\": \"Phillip petrat\"\n" +
        "        },\n" +
        "    ],\n" +
        "    \"emailAddress\": \"l-s-d@alice-dsl.de\",\n" +
        "}";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      createdPerson = topicService.createPerson(payload);
      if(null != createdPerson){
        System.out.println(createdPerson.toString());
      }


    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }



  public void updatePerson() {
    JSONObject updatedPerson = null;
    String payload = "{\n" +
        "    \"personId\": \"person-1437033371MGLBATRMDC\",\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"languageId\": \"http://psi.topic.ndla.no/#language-neutral\",\n" +
        "            \"name\": \"Phillip Dieter Petrat\"\n" +
        "        },\n" +
        "    ],\n" +
        "    \"emailAddress\": \"l-s-d@alice-dsl.de\",\n" +
        "}";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      updatedPerson = topicService.updatePerson(payload);
      if(null != updatedPerson){
        System.out.println(updatedPerson.toString());
      }


    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }


  public void deletePersons() {
    JSONArray deletedTopic = null;
    String payload = "[\"person-1436526623YCHGQYCWVY\"]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      deletedTopic = topicService.deletePersons(payload);
      if(deletedTopic.length() > 0){
        System.out.println(deletedTopic.toString());
      }


    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }


  public void getPersons() {
    JSONObject topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      topics = topicService.getPersons(50, 0);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
    // Temporary code
    if (topics != null) {
      System.out.println(topics.toString());
    }
  }


  public void getOntologyDomains() {
      JSONObject topics = null;
      try {
          TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
          topics = topicService.getOntologyDomains(50, 0);
      } catch (NdlaServiceException | BadObjectReferenceException e) {
          System.out.println(e.getMessage());
      }
      // Temporary code
      if (topics != null) {
        System.out.println(topics.toString());
      }
  }


  public void saveTopicsByDomain(){
    JSONArray topics = null;
    try {
      String payload = "[\"topic-1432730298SXAKPQBRRS\",\"topic-1426078607WCVNBBVWPB\"]";
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      topics = topicService.saveTopicsByDomain("ontologyDomain-1436969865DSITXUBAVG", "domainTopics", "http://psi.topic.ndla.no/#ontology-domain", payload);
      if (topics != null) {
        System.out.println(topics.toString());
      }
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }

  @Test
  public void saveOntologyTopicAssociations(){
    JSONArray topics = null;
    try {

      String payload = "{\n" +
          "    \"associations\": [\n" +
          "        {\n" +
          "            \"relationId\": \"http://psi.topic.ndla.no/#part-of\",\n" +
          "            \"topics\": [\n" +
          "                {\n" +
          "                    \"roles\": [\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1432730298SXAKPQBRRS\",\n" +
          "                            \"roleId\": \"http://psi.topic.ndla.no/#container\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1432728961GKBRMNLQEB\",\n" +
          "                            \"roleId\": \"http://psi.topic.ndla.no/#containee\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        },\n" +
          "        {\n" +
          "            \"relationId\": \"http://psi.topic.ndla.no/#has-connection\",\n" +
          "            \"topics\": [\n" +
          "                {\n" +
          "                    \"roles\": [\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1432898975SRWXOZIBWQ\",\n" +
          "                            \"roleId\": \"http://psi.topic.ndla.no/#incoming\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1426155334AVUNDECNGM\",\n" +
          "                            \"roleId\": \"http://psi.topic.ndla.no/#outgoing\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        },\n" +
          "        {\n" +
          "            \"relationId\": \"http://psi.topic.ndla.no/#narrower-than\",\n" +
          "            \"topics\": [\n" +
          "                {\n" +
          "                    \"roles\": [\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1431347132SAZIHNVDEH\",\n" +
          "                            \"roleId\": \"http://psi.topic.ndla.no/#broader\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1432804217TBEONAYXCM\",\n" +
          "                            \"roleId\": \"http://psi.topic.ndla.no/#narrower\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        }\n" +
          "    ]\n" +
          "}";
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      topics = topicService.saveDomainOntologyTopicAssociations("person-1436449708TJPURLMALL", "persons", payload);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void deleteTopicsFromDomain(){
    JSONArray topics = null;
    try {
      String payload = "[\"topic-1426078607WCVNBBVWPB\"]";
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      topics = topicService.deleteTopicsFromDomain("person-1436449708TJPURLMALL", "persons", "http://psi.topic.ndla.no/#person", payload);
      if (topics != null) {
        System.out.println(topics.toString());
      }
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }

  public void deleteTopicRelationsFromDomain(){
    JSONArray topics = null;
    try {
      String payload = "";
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      topics = topicService.deleteTopicsFromDomain("person-1436449708TJPURLMALL", "persons", "http://psi.topic.ndla.no/#person", payload);
      if (topics != null) {
        System.out.println(topics.toString());
      }
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getOntologyTopics(){
    JSONObject topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      topics = topicService.getDomainOntologyTopics("ontologyDomain-1436969865DSITXUBAVGi","domainTopics");
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getDomainOntologyTopicRelations(){
    JSONObject topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      topics = topicService.getDomainOntologyTopicRelations("person-1436449708TJPURLMALL", "persons");
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }

}
