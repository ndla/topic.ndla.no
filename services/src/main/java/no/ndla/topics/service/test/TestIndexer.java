package no.ndla.service.test;

import no.ndla.topics.service.Indexer;
import no.ndla.topics.service.NdlaServiceException;
import org.junit.Test;

public class TestIndexer {


    public void testLogin() {
        boolean indexed = false;

        Indexer indexer = new Indexer("http://search.ndla.no", "topics-user", "index_My_Topics_13");
        try {

            String payload = "[{\n" +
                    "            \"topicId\": \"keyword-baknekrafoo3\",\n" +
                    "            \"psi\": \"http://psi.topic.ndla.no/keywords/#keyword-baknekrafoo3\",\n" +
                    "            \"originatingSite\": \"http://ndla.no/\",\n" +
                    "            \"visibility\": \"1\",\n" +
                    "            \"names\": [\n" +
                    "                {\n" +
                    "                    \"wordclass\": \"noun\",\n" +
                    "                    \"data\": [\n" +
                    "                        {\n" +
                    "                            \"http://psi.oasis-open.org/iso/639/#nob\": \"Poopsidaisy\"\n" +
                    "                        },\n" +
                    "                        {\n" +
                    "                            \"http://psi.topic.ndla.no/#language-neutral\": \"PrompsiPrestekrage\"\n" +
                    "                        },\n" +
                    "                        {\n" +
                    "                            \"http://psi.oasis-open.org/iso/639/#eng\": \"PrompsiPrestekragar\"\n" +
                    "                        }\n" +
                    "                    ]\n" +
                    "                }\n" +
                    "            ]\n" +
                    "        }]";
            indexed = indexer.postToIndex(payload, false,"keywords");
            if (indexed) {
                System.out.println("The keyword was successfully created");
            }

        } catch (NdlaServiceException ne) {
            System.out.println("TESTLOGIN ERROR: " + ne.getMessage());
        }
    }



    public void testDelete() {
        Indexer indexer = new Indexer("http://search", "topics-user", "index_My_Topics_13");
        boolean deleted = false;
        try {
            deleted = indexer.deleteFromIndex("keyword-UNERGXFEZQ","keywords");
            if (deleted) {
                System.out.println("keyword-UNERGXFEZQ" + " was successfully deleted");

            }

        } catch (NdlaServiceException e) {
            System.out.println("TESTDELETE ERROR: " + e.getMessage());
        }
    }


  public void testDeleteAll() {
    Indexer indexer = new Indexer("http://search", "topics-user", "index_My_Topics_13");
    boolean deleted = false;
    try {
      deleted = indexer.deleteAllFromIndex("topics");
      if (deleted) {
        System.out.println("it all was successfully deleted");

      }

    } catch (NdlaServiceException e) {
      System.out.println("TESTDELETE ERROR: " + e.getMessage());
    }
  }



}
