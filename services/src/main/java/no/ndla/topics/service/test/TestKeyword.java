/*  Implemented by Brett Kromkamp (brett@seria.no)
    Date: February 11, 2013
 */

package no.ndla.service.test;

import edu.emory.mathcs.backport.java.util.Arrays;
import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import org.junit.Test;

import java.util.*;

public class TestKeyword {
    private static final String CONFIG_FILE_PATH = "/Users/rolfguescini/adminconfig/tm-sources.xml";
    private static final String TOPICMAP_REFERENCE_KEY = "NdlaTopicsService";

    public void getKeywords() {
        Collection<NdlaTopic> keywords1 = null;
        Collection<NdlaTopic> keywords2 = null;
        Collection<NdlaTopic> keywords3 = null;
        Collection<NdlaTopic> keywords4 = null;
        Collection<NdlaTopic> keywords5 = null;
        Collection<NdlaTopic> keywords6 = null;
        Collection<NdlaTopic> keywords7 = null;
        Collection<NdlaTopic> keywords8 = null;
        Collection<NdlaTopic> keywords9 = null;
        Collection<NdlaTopic> keywords10 = null;
        long millis = 0;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search.test.ndla.no", "topics-user", "index_My_Topics_13");

            millis = System.currentTimeMillis();
            keywords1 = topicService.getKeywords(10, 300, NdlaSite.NDLA);
            millis = System.currentTimeMillis() - millis;
            System.out.println(millis);

            millis = System.currentTimeMillis();
            keywords2 = topicService.getKeywords(10, 300, NdlaSite.NDLA);
            millis = System.currentTimeMillis() - millis;
            System.out.println(millis);

            millis = System.currentTimeMillis();
            keywords3 = topicService.getKeywords(10, 300, NdlaSite.NDLA);
            millis = System.currentTimeMillis() - millis;
            System.out.println(millis);

            millis = System.currentTimeMillis();
            keywords4 = topicService.getKeywords(10, 300, NdlaSite.NDLA);
            millis = System.currentTimeMillis() - millis;
            System.out.println(millis);

            millis = System.currentTimeMillis();
            keywords5 = topicService.getKeywords(10, 300, NdlaSite.NDLA);
            millis = System.currentTimeMillis() - millis;
            System.out.println(millis);

            millis = System.currentTimeMillis();
            keywords6 = topicService.getKeywords(10, 300, NdlaSite.NDLA);
            millis = System.currentTimeMillis() - millis;
            System.out.println(millis);

            millis = System.currentTimeMillis();
            keywords7 = topicService.getKeywords(10, 300, NdlaSite.NDLA);
            millis = System.currentTimeMillis() - millis;
            System.out.println(millis);

            millis = System.currentTimeMillis();
            keywords8 = topicService.getKeywords(10, 300, NdlaSite.NDLA);
            millis = System.currentTimeMillis() - millis;
            System.out.println(millis);

            millis = System.currentTimeMillis();
            keywords9 = topicService.getKeywords(10, 300, NdlaSite.NDLA);
            millis = System.currentTimeMillis() - millis;
            System.out.println(millis);

            millis = System.currentTimeMillis();
            keywords10 = topicService.getKeywords(10, 300, NdlaSite.NDLA);
            millis = System.currentTimeMillis() - millis;
            System.out.println(millis);

        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }


    public void getKeywordsByNodeId() {
        Collection<NdlaTopic> keywords = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            keywords = topicService.getKeywordsByNodeId(10, 0, "ndlanode_700030", NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (keywords != null) {
            for (NdlaTopic topic : keywords) {
                System.out.println(topic.getIdentifier());
            }
        }
    }

    public void massiveGetKeywordsByNodeId() {
        Collection<NdlaTopic> keywords = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            for (int i = 0; i < 1000; i++) {
                keywords = topicService.getKeywordsByNodeId(10, 0, "ndlanode_15091", NdlaSite.NDLA);
                if (keywords != null) {
                    for (NdlaTopic topic : keywords) {
                        System.out.println(topic.getIdentifier());
                    }
                }
                System.out.println("==========");
            }
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }


    public void createKeyword() {
        HashMap<String, TopicIF> topics = null;

        String payload = "{\n" +
            "    \"keywords\": [\n" +
            "        {\n" +
            "            \"keywordset\": [\n" +
            "                {\n" +
            "                    \"names\": [\n" +
            "                        {\n" +
            "                            \"keyword\": \"pakurium\",\n" +
            "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"keyword\": \"pakuriNOB\",\n" +
            "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"keyword\": \"pakuriar\",\n" +
            "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nno\"\n" +
            "                        }\n" +
            "                    ],\n" +
            "                    \"types\": [\n" +
            "                        {\n" +
            "                            \"typeId\": \"keyword-1387108541KFMDUVWFTL\"\n" +
            "                        }\n" +
            "                    ],\n" +
            "                    \"visibility\": \"1\",\n" +
            "                    \"wordclass\": \"verb\"\n" +
            "                }\n" +
            "            ]\n" +
            "        }\n" +
            "    ]\n" +
            "}\n";

        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search.test.ndla.no", "topics-user", "index_My_Topics_13");
            topics = topicService.createKeyword(payload);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics.size() > 0) {
            Iterator it = topics.keySet().iterator();
            while (it.hasNext()) {
                TopicIF tmp = (TopicIF) topics.get(it.next());
                System.out.println(tmp.getSubjectIdentifiers().toString());
            }
        }
    }




    public void   createKeywordsByNodeId() {
        HashMap<String, TopicIF> topics = null;

    /*
      String payload = "{\n" +
          "    \"keywords\": [\n" +
          "        {\n" +
          "            \"keywordset\": [\n" +
          "                {\n" +
          "                    \"topicId\": \"keyword-1387108473WHCCMNLIZC\",\n" +
          "                    \"visibility\": \"1\",\n" +
          "                    \"wordclass\": \"noun\",\n" +
          "                    \"types\": [],\n" +
          "                    \"names\": [\n" +
          "                        {\n" +
          "                            \"keyword\": \"fargebilde\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#sma\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"fargebilde\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"color image\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        },\n" +
          "        {\n" +
          "            \"keywordset\": [\n" +
          "                {\n" +
          "                    \"topicId\": \"keyword-1387108452ZKMZLNOPTB\",\n" +
          "                    \"visibility\": \"1\",\n" +
          "                    \"wordclass\": \"noun\",\n" +
          "                    \"types\": [],\n" +
          "                    \"names\": [\n" +
          "                        {\n" +
          "                            \"keyword\": \"eplebrandy\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#sma\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"eplebrandy\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"apple brandy\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        },\n" +
          "        {\n" +
          "            \"keywordset\": [\n" +
          "                {\n" +
          "                    \"topicId\": \"keyword-1387110104LPAGGQPSZZ\",\n" +
          "                    \"visibility\": \"1\",\n" +
          "                    \"wordclass\": \"noun\",\n" +
          "                    \"types\": [],\n" +
          "                    \"names\": [\n" +
          "                        {\n" +
          "                            \"keyword\": \"skreifiske\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#sma\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"skreifiske\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"cod fishing\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        },\n" +
          "        {\n" +
          "            \"keywordset\": [\n" +
          "                {\n" +
          "                    \"topicId\": \"\",\n" +
          "                    \"visibility\": \"1\",\n" +
          "                    \"wordclass\": \"noun\",\n" +
          "                    \"types\": [],\n" +
          "                    \"names\": [\n" +
          "                        {\n" +
          "                            \"keyword\": \"SKRÆVrævdundresma\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#sma\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"SKRÆVrævdundrenob\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"SKRÆVrævdundreng\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        }\n" +
          "    ]\n" +
          "}";
*/
    String payload = "{\n" +
        "    \"keywords\": [\n" +
        "        {\n" +
        "            \"keywordset\": [\n" +
        "                {\n" +
        "                    \"topicId\": \"fyr-keyword-1387111310MULOBOQMAP\",\n" +
        "                    \"visibility\": \"1\",\n" +
        "                    \"wordclass\": \"noun\",\n" +
        "                    \"types\": [],\n" +
        "                    \"names\": [\n" +
        "                        {\n" +
        "                            \"keyword\": \"vocational quiz\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
        "                        },\n" +
        "                        {\n" +
        "                            \"keyword\": \"yrkes quiz\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
        "                        }\n" +
        "                    ]\n" +
        "                }\n" +
        "            ]\n" +
        "        },\n" +
        "        {\n" +
        "            \"keywordset\": [\n" +
        "                {\n" +
        "                    \"topicId\": \"fyr-keyword-1387111305FUAONMNRFI\",\n" +
        "                    \"visibility\": \"1\",\n" +
        "                    \"wordclass\": \"noun\",\n" +
        "                    \"types\": [],\n" +
        "                    \"names\": [\n" +
        "                        {\n" +
        "                            \"keyword\": \"tesol\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
        "                        },\n" +
        "                        {\n" +
        "                            \"keyword\": \"tesol\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
        "                        }\n" +
        "                    ]\n" +
        "                }\n" +
        "            ]\n" +
        "        },\n" +
        "        {\n" +
        "            \"keywordset\": [\n" +
        "                {\n" +
        "                    \"topicId\": \"fyr-keyword-1387111309PEBSOYFUYW\",\n" +
        "                    \"visibility\": \"1\",\n" +
        "                    \"wordclass\": \"noun\",\n" +
        "                    \"types\": [],\n" +
        "                    \"names\": [\n" +
        "                        {\n" +
        "                            \"keyword\": \"vurderingskriterium\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nno\"\n" +
        "                        },\n" +
        "                        {\n" +
        "                            \"keyword\": \"assessment criterion\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
        "                        },\n" +
        "                        {\n" +
        "                            \"keyword\": \"vurderingskriterium\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
        "                        }\n" +
        "                    ]\n" +
        "                }\n" +
        "            ]\n" +
        "        }\n" +
        "    ]\n" +
        "}";
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
            topics = topicService.createKeywordsByNodeId("500006", payload, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics.size() > 0) {
            Iterator it = topics.keySet().iterator();
            while (it.hasNext()) {
                TopicIF tmp = (TopicIF) topics.get(it.next());
                System.out.println(tmp.getSubjectIdentifiers().toString());
            }
        }
    }




    public void testSeveralUpdates() {
      for(int i = 0; i < 100; i++) {
        updateKeywordsByNodeId();
      }
    }



    public void updateKeywordsByNodeId() {
        HashMap<String, TopicIF> topics = null;

      String payload = "{\n" +
          "    \"keywords\": [\n" +
          "        {\n" +
          "            \"keywordset\": [\n" +
          "                {\n" +
          "                    \"topicId\": \"keyword-1387108380YPWATQSLAX\",\n" +
          "                    \"visibility\": \"1\",\n" +
          "                    \"wordclass\": \"noun\",\n" +
          "                    \"types\": [],\n" +
          "                    \"names\": [\n" +
          "                        {\n" +
          "                            \"keyword\": \"bleaching\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"bleking\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"bleiking\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nno\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        },\n" +
          "        {\n" +
          "            \"keywordset\": [\n" +
          "                {\n" +
          "                    \"topicId\": \"keyword-1387109019NBSBZKBJBL\",\n" +
          "                    \"visibility\": \"1\",\n" +
          "                    \"wordclass\": \"noun\",\n" +
          "                    \"types\": [],\n" +
          "                    \"names\": [\n" +
          "                        {\n" +
          "                            \"keyword\": \"listerine\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"listerine\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        },\n" +
          "        {\n" +
          "            \"keywordset\": [\n" +
          "                {\n" +
          "                    \"topicId\": \"keyword-1387108640WSZYPSKJBY\",\n" +
          "                    \"visibility\": \"1\",\n" +
          "                    \"wordclass\": \"noun\",\n" +
          "                    \"types\": [],\n" +
          "                    \"names\": [\n" +
          "                        {\n" +
          "                            \"keyword\": \"gulliver\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nno\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"gulliver\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"gulliver\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        },\n" +
          "        {\n" +
          "            \"keywordset\": [\n" +
          "                {\n" +
          "                    \"topicId\": \"keyword-1387108369OMVNAFRURU\",\n" +
          "                    \"visibility\": \"1\",\n" +
          "                    \"wordclass\": \"noun\",\n" +
          "                    \"types\": [],\n" +
          "                    \"names\": [\n" +
          "                        {\n" +
          "                            \"keyword\": \"appell\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nno\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"appeal\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"keyword\": \"appell\",\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        }\n" +
          "    ]\n" +
          "}";
    try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
            topics = topicService.updateKeywordsByNodeId("500006", payload, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }


    public void updateKeywordsByKeywordId() {
      HashMap<String, TopicIF> topics = null;
      String payload = "{\n" +
          "    \"keywords\": [\n" +
          "        {\n" +
          "            \"keyword-1387108452EBNFNXJVSU\": [\n" +
          "                {\n" +
          "                    \"visibility\": \"1\",\n" +
          "                    \"wordclass\": \"noun\",\n" +
          "                    \"names\": [\n" +
          "                        {\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
          "                            \"keyword\": \"epledessert\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#deu\",\n" +
          "                            \"keyword\": \"Apfeldessert\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nno\",\n" +
          "                            \"keyword\": \"epledessert\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
          "                            \"keyword\": \"apple dessert\"\n" +
          "                        }\n" +
          "                    ],\n" +
          "                    \"types\": []\n" +
          "                }\n" +
          "            ]\n" +
          "        }\n" +
          "    ]\n" +
          "}";

        try {
          TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
            topics = topicService.updateKeywordsByKeywordId(payload);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }

    public void getKeywordsBySubjectMatter() {
        Collection<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getKeywordsBySubjectMatter(10, 200, "subject_7", NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics != null) {
            for (NdlaTopic topic : topics) {
                System.out.println(topic.getIdentifier());
            }
        }
    }


    public void getKeywordByKeywordId() {
        NdlaTopic topic = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topic = topicService.getKeywordByKeywordId("keyword-1387108369OJWPZNLOVR");
          System.out.println("PROCESS: "+topic.getProcessState()+" APPROVED: "+topic.getApproved());
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topic != null) {
            System.out.println(topic.getIdentifier());
        }
    }

    public void saveKeywordsBySubjectMatter() {
        ArrayList<TopicIF> topics = null;
        List<String> args = Arrays.asList(new String[]{"keyword-YDZWDFQAJM", "keyword-AUOHMKJNGZ"});
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.saveKeywordsBySubjectMatter("subject_18910", args, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }

    @Test
    public void deleteKeywordsByKeywordId() {
        ArrayList<String> topics = null;
        List<String> args = Arrays.asList(new String[]{"keyword-1387108836SVJLUJMYWA"});
        try {
            //TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search.test.ndla.no", "topics-user", "index_My_Topics_13");
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
            topics = topicService.deleteKeywordsByKeywordId(args, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }
}
