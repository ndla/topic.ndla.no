/*  Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
    Date: April 9, 2013
 */
package no.ndla.service.test;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;

import java.util.Collection;

public class TestMetadataSet {
    // Topic map engine connection constants. Should be configurable.
    private static final String DRIVER_CLASS = "org.postgresql.Driver";
    private static final String CONNECTION_STRING = "jdbc:postgresql://localhost/rolf";
    private static final String DATABASE = "postgresql";
    private static final String USERNAME = "rolf";
    private static final String PASSWORD = "08isNzWVbwMI";
    private static final String CONNECTION_POOL = "false";

    private static final String NDLA_TOPIC_IDENTIFIER = "ndla-topic-15843"; //"ndla-topic-26368";
    private static final String NDLA_NODE_IDENTIFIER = "ndlanode_28546";
    private static final String SUBJECT_MATTER = "subject_7";
    private static final String KEYWORD_IDENTIFIER = "keyword-49347";

    private static final long TOPICMAP_IDENTIFIER = 26773001; //11443601    postgresql-24767601

    /*
    private static final String PROPERTY_FILE_PATH = "/home/brettk/ndla.no/topic/services/topicmap/api/src/no/ndla/service/db.postgresql.props";
    private static final String TOPICMAP_REFERENCE = "RDBMS-26773001";
    */

    private static final String CONFIG_FILE_PATH = "/home/brettk/ndla.no/topic/services/topicmap/api/src/no/ndla/service/tm-sources.xml";
    private static final String TOPICMAP_REFERENCE_KEY = "NdlaTopicsService";

    public void getMetadataSets() {
        Collection<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getMetadataSets(10, 0, NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics != null) {
            for (NdlaTopic topic : topics) {
                System.out.println(topic.getIdentifier());
            }
        }
    }

    public void getMetadataSetsByNodeId() {
        Collection<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getMetadataSetsByNodeId(10, 0, "ndlanode_15091", NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics != null) {
            for (NdlaTopic topic : topics) {
                System.out.println(topic.getIdentifier());
            }
        }
    }
}
