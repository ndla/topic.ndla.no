package no.ndla.service.test;

import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.Indexer;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.TreeMap;

public class TestMiscelaneous {
  private static final String CONFIG_FILE_PATH = "/Users/rolfguescini/adminconfig/tm-sources.xml";
  private static final String TOPICMAP_REFERENCE_KEY = "NdlaTopicsService";

    public void getHierarchicalAssociationTypes() {
        ArrayList<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getHierarchicalAssociationTypes();
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }


    public void getHierarchicalAssociationType() {
        NdlaTopic topic = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            String type = "part-of";
            topic = topicService.getHierarchicalAssociationType(type);
            TreeMap<String, TreeMap<String, String>> names = topic.getRelationshipNames();
            System.out.println("NAMES: "+names);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }


    public void getHorizontalAssociationTypes() {
        ArrayList<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getHorizontalAssociationTypes();
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }


    public void getHorizontalAssociationType() {
        NdlaTopic topic = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            String type = "subjectmatter-metadataterm";
            topic = topicService.getHorizontalAssociationType(type);
          System.out.println(topic);
            TreeMap<String, TreeMap<String, String>> names = topic.getRelationshipNames();

            System.out.println("NAMES: "+names);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }


    public void getWordClasses() {
        ArrayList<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getWordClasses();
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }

    public void getWordClass() {
        NdlaTopic topic = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            String type = "verb";
            topic = topicService.getWordClass(type);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }

  public void getLanguages() {
    ArrayList<NdlaTopic> languages = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search.test.ndla.no", "topics-user", "index_My_Topics_13");
      languages = topicService.getLanguages();

      for(NdlaTopic language : languages) {
        HashMap<String,String> langnames = language.getNames();
        System.out.println(langnames);
      }
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getLanguage() {
    NdlaTopic language = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search.test.ndla.no", "topics-user", "index_My_Topics_13");
      language = topicService.getLanguage("ara");
      System.out.println(language.getIdentifier());
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void createLanguage() {
    TopicIF createdLanguage = null;
    String payload = "{\n" +
        "    \"languageId\": \"http://psi.oasis-open.org/iso/639/#ara\",\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "            \"name\": \"Arabic\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#ara\",\n" +
        "            \"name\": \"العربية\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "            \"name\": \"Arabisk\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#nno\",\n" +
        "            \"name\": \"Arabisk\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#sma\",\n" +
        "            \"name\": \"Arábalašgiella\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#sme\",\n" +
        "            \"name\": \"Arábalašgiella\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#smj\",\n" +
        "            \"name\": \"Arábalašgiella\"\n" +
        "        }\n" +
        "    ]\n" +
        "}";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      TopicIF topic1 = topicService.createLanguage(payload);
      if(null != topic1){
        System.out.println(topic1.toString());
      }


    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }


  public void updateLanguage() {
    TopicIF updatedLanguage = null;
    String payload = "{\n" +
        "    \"languageId\": \"http://psi.oasis-open.org/iso/639/#ara\",\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "            \"name\": \"Arabic\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#ara\",\n" +
        "            \"name\": \"العربية\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "            \"name\": \"Arabisk\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#nno\",\n" +
        "            \"name\": \"Arabisk\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#sma\",\n" +
        "            \"name\": \"Arábalašgiella\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#sme\",\n" +
        "            \"name\": \"Arábalašgiella\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#smj\",\n" +
        "            \"name\": \"Arábalašgiella\"\n" +
        "        }\n" +
        "    ]\n" +
        "}";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      TopicIF topic1 = topicService.updateLanguage(payload);
      if(null != topic1){
        System.out.println(topic1.toString());
      }


    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }



  public void deleteLanguage() {
    TopicIF createdLanguage = null;
    String payload = "[\n" +
        "    \"http://psi.oasis-open.org/iso/639/#aze\"\n" +
        "]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      TopicIF topic = topicService.deleteLanguageByLanguageId(payload);
      if(null != topic){
        System.out.println(topic.toString());
      }
    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }

  public void itemIdentifier() {
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search.test.ndla.no", "topics-user", "index_My_Topics_13");
            String id = "15091";
            ArrayList<String> gotid = topicService.getItemIdentifiers(id, NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }

    public void getWordData() {
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search.test.ndla.no", "topics-user", "index_My_Topics_13");
            String id = "15091";
            JSONObject foo = topicService.getWordData(id, NdlaSite.NDLA);
            System.out.println(foo);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }


    public void getKeywordSubjectIdentifiers() {
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search.test.ndla.no", "topics-user", "index_My_Topics_13");
            String id = "700030";
            ArrayList<String> foo = topicService.getKeywordSubjectIdentifiers(id, NdlaSite.NDLA);
            System.out.println(foo);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }


    public void indexAll() {
        String message = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search.test.ndla.no", "topics-user", "index_My_Topics_13");
            message = topicService.reIndexAllKeywords("DELING");
            System.out.println(message);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }



    public void deleteAll() {
        String message = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search.ndla.no", "topics-user", "index_My_Topics_13");
            message = topicService.deleteAllKeywordsFromAutoCompleteIndex("keywords");
            System.out.println(message);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }



  public void approveTopic(){
    JSONObject message = null;
    java.util.Date date= new java.util.Date();
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      message = topicService.approveTopic("http://psi.topic.ndla.no/keywords/#keyword-1387108369OJWPZNLOVR", date, "keywords");
      System.out.println(message);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void disApproveTopic(){
    JSONObject message = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      message = topicService.disApproveTopic("http://psi.topic.ndla.no/keywords/#keyword-1387108369OJWPZNLOVR", "keywords");
      System.out.println(message);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }




  public void setProcessState(){
    JSONObject message = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      //@todo: BUG!
      message = topicService.setProcessState("http://psi.topic.ndla.no/topics/#topic-1430926447ELBZAGYXNB", "1", "topics");
      System.out.println(message);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  @Test
  public void testIndexAllTopics() {
    TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
    String message = "";
    try {
      message = topicService.reIndexAllTopics();
      System.out.println("reindexing of topics: "+message);



    } catch (Exception e) {
      System.out.println("TESTDELETE ERROR: " + e.getMessage());
    }
  }


  public void testIndexAllKeywords() {
    TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");

    String message = "";
    try {
      message = topicService.reIndexAllKeywords("NDLA");
      System.out.println("reindexing of topics: "+message);



    } catch (Exception e) {
      System.out.println("TESTDELETE ERROR: " + e.getMessage());
    }
  }



  public void deleteAllKeywordsFromAutoCompleteIndex() {
    TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");

    String message = "";
    try {
      message = topicService.deleteAllKeywordsFromAutoCompleteIndex("topics");
      System.out.println("reindexing of topics: "+message);



    } catch (Exception e) {
      System.out.println("TESTDELETE ERROR: " + e.getMessage());
    }
  }

}
