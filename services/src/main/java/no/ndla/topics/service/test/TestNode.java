/*  Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
    Date: April 8, 2013
 */
package no.ndla.service.test;

import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import org.junit.Test;

import java.util.Collection;

public class TestNode {

    // Topic map engine connection constants. Should be configurable.
    private static final String CONFIG_FILE_PATH = "/Users/rolfguescini/adminconfig/tm-sources.xml";
    private static final String TOPICMAP_REFERENCE_KEY = "NdlaTopicsService";



    public void getNodes() {
        Collection<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getNodes(10, 10, NdlaSite.DELING);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics != null) {
            for (NdlaTopic topic : topics) {
                System.out.println(topic.getIdentifier());
            }
        }
    }


    public void getNodesByKeywordId() {
        Collection<NdlaTopic> topics = null;
        try {
            //TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search2", "topics-user", "index_My_Topics_13");
            topics = topicService.getNodesByKeywordId(200, 0, "fyr-keyword-1387111304NWFYHOYBPW", NdlaSite.NYGIV);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics != null) {
            for (NdlaTopic topic : topics) {
                System.out.println(topic.getIdentifier());
            }
        }
    }

    public void getNodesByTopicId() {
        Collection<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getNodesByTopicId(10, 0, "ndla-topic-53479", NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics != null) {
            for (NdlaTopic topic : topics) {
                System.out.println(topic.getIdentifier());
            }
        }
    }


    public void getNodeByNodeId() {
        NdlaTopic topic = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topic = topicService.getNodeByNodeId("ndlanode_28546", NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topic != null) {
            System.out.println(topic.getIdentifier());
        }
    }

  @Test
    public void createNewNode() {
    /*
        String payload = "{\n" +
            "    \"names\": [\n" +
            "        {\n" +
            "            \"http://psi.oasis-open.org/iso/639/#nob\": \"Deunode\"\n" +
            "        }\n" +
            "    ],\n" +
            "    \"data\": {\n" +
            "        \"author\": [\n" +
            "            {\n" +
            "                \"http://psi.topic.ndla.no/#language-neutral\": \"Rolf Guescini\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"author_id\": [\n" +
            "            {\n" +
            "                \"http://psi.topic.ndla.no/#language-neutral\": \"407\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"node_type\": [\n" +
            "            {\n" +
            "                \"http://psi.topic.ndla.no/#language-neutral\": \"fagstoff\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"license\": [\n" +
            "            {\n" +
            "                \"http://psi.oasis-open.org/iso/639/#eng\": \"by-sa\"\n" +
            "            }\n" +
            "        ],\n" +
            "        \"ingress\": [\n" +
            "            {\n" +
            "                \"http://psi.oasis-open.org/iso/639/#nob\": \"ingrezz\"\n" +
            "            },\n" +
            "            {\n" +
            "                \"http://psi.oasis-open.org/iso/639/#deu\": \"ingrezzenDEU\"\n" +
            "            }\n" +
            "        ]\n" +
            "    }\n" +
            "}";
            */
    String payload = "{\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"http://psi.oasis-open.org/iso/639/#eng\": \"Fopptik\"\n" +
        "        }\n" +
        "    ],\n" +
        "    \"data\": {\n" +
        "        \"author\": [\n" +
        "            {\n" +
        "                \"http://psi.topic.ndla.no/#language-neutral\": \"Rolf  Vennenbernd \"\n" +
        "            }\n" +
        "        ],\n" +
        "        \"author_id\": [\n" +
        "            {\n" +
        "                \"http://psi.topic.ndla.no/#language-neutral\": \"407\"\n" +
        "            }\n" +
        "        ],\n" +
        "        \"node_type\": [\n" +
        "            {\n" +
        "                \"http://psi.topic.ndla.no/#language-neutral\": \"fagstoff\"\n" +
        "            }\n" +
        "        ],\n" +
        "        \"license\": [\n" +
        "            {\n" +
        "                \"http://psi.oasis-open.org/iso/639/#eng\": \"nlod\"\n" +
        "            }\n" +
        "        ],\n" +
        "        \"ingress\": [\n" +
        "            {\n" +
        "                \"http://psi.oasis-open.org/iso/639/#eng\": \"dette er en ingress\"\n" +
        "            }\n" +
        "        ]\n" +
        "    }\n" +
        "}";
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            TopicIF topic1 = topicService.createNewNode("800018", payload, NdlaSite.NDLA);

            //TopicIF topic2 = topicService.createNewNode("brettkromkamp_140124_005_node", payload, NdlaSite.NDLA); // Should fail!

        } catch (NdlaServiceException e) {
            System.err.println(e.getMessage());
        }
    }

    public void deleteNodeByNodeId() {
        TopicIF topic = null;

        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topic = topicService.deleteNodeByNodeId("123897", NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }
}
