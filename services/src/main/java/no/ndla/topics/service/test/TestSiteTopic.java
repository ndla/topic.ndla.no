/*  Implemented by Brett Kromkamp (brett@seria.no)
    Date: February 11, 2013
 */

package no.ndla.service.test;

import edu.emory.mathcs.backport.java.util.Arrays;
import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

public class TestSiteTopic {

    // Topic map engine connection constants. Should be configurable.
    private static final String DRIVER_CLASS = "org.postgresql.Driver";
    private static final String CONNECTION_STRING = "jdbc:postgresql://localhost/rolf";
    private static final String DATABASE = "postgresql";
    private static final String USERNAME = "rolf";
    private static final String PASSWORD = "08isNzWVbwMI";
    private static final String CONNECTION_POOL = "false";

    private static final String NDLA_TOPIC_IDENTIFIER = "ndla-topic-15843"; //"ndla-topic-26368";
    private static final String NDLA_NODE_IDENTIFIER = "ndlanode_28546";
    private static final String SUBJECT_MATTER = "subject_7";
    private static final String KEYWORD_IDENTIFIER = "keyword-49347";

    private static final long TOPICMAP_IDENTIFIER = 26773001; //24767601; //11443601    postgresql-24767601

    /*
    private static final String PROPERTY_FILE_PATH = "/home/brettk/ndla.no/topic/services/topicmap/api/src/no/ndla/service/db.postgresql.props";
    private static final String TOPICMAP_REFERENCE = "RDBMS-26773001";
    */

    private static final String CONFIG_FILE_PATH = "/home/brettk/ndla.no/topic/services/topicmap/api/src/no/ndla/service/tm-sources.xml";
    //private static final String CONFIG_FILE_PATH = "/usr/local/tomcat-7/webapps/no.ndla.topics/WEB-INF/config/tm-sources.xml";
    private static final String TOPICMAP_REFERENCE_KEY = "NdlaTopicsService";



    public void getSiteTopics() {
        Collection<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);

            topics = topicService.getSiteTopics(10, 10, NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics != null) {
            for (NdlaTopic topic : topics) {
                System.out.println(topic.getIdentifier());
            }
        }
    }


    public void getSiteTopicByTopicId() {
        NdlaTopic topic1 = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topic1 = topicService.getSiteTopicByTopicId("ndla-topic-99929");
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }

    public void getTopicsBySubjectMatter() {
      JSONObject topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getTopicsBySubjectMatter(10, 10, "subject_7", NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

    }

    public void saveSiteTopicsByNodeId() {
        HashMap<String, TopicIF> topics = null;

        HashMap<String, HashMap<String, HashMap<String, String>>> args = new HashMap<String, HashMap<String, HashMap<String, String>>>();
        HashMap<String, HashMap<String, String>> data1 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> data2 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> data3 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> data4 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> data5 = new HashMap<String, HashMap<String, String>>();

        HashMap<String, HashMap<String, String>> namedata1 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> namedata2 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> namedata3 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> namedata4 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> namedata5 = new HashMap<String, HashMap<String, String>>();

        HashMap<String, String> names1 = new HashMap<String, String>();
        HashMap<String, String> names2 = new HashMap<String, String>();
        HashMap<String, String> names3 = new HashMap<String, String>();
        HashMap<String, String> names4 = new HashMap<String, String>();
        HashMap<String, String> names5 = new HashMap<String, String>();

        HashMap<String, String> data_elm1 = new HashMap<String, String>();
        HashMap<String, String> data_elm2 = new HashMap<String, String>();
        HashMap<String, String> data_elm3 = new HashMap<String, String>();
        HashMap<String, String> data_elm4 = new HashMap<String, String>();
        HashMap<String, String> data_elm5 = new HashMap<String, String>();

        names1.put("nb", "nytt ndlaemne");
        names1.put("nn", "nytt ndlaemne");
        names1.put("en", "new ndla topic");
        data1.put("names", names1);

        data_elm1.put("description", "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?");
        data_elm1.put("image", "http://images.com/image.jpg");
        data1.put("occurrences", data_elm1);

        names2.put("nb", "Biebertrofaste");
        names2.put("nn", "Biebertrufaste");
        names2.put("en", "Beliebers");
        data2.put("names", names2);

        data_elm2.put("en#description", "It'll be a few miles to go. You have to have good shoes. It is indeed important, says Osmundsvaag explaining that guards will keep a watch with a rope to the concert participants in the pools the last way out to the hall");
        data_elm2.put("nb#description", "Det blir vel noen kilometer å gå. Man må ha gode sko. Det er faktisk viktig, sier Osmundsvaag og forklarer at vakter vil gå manngard med tau for å føre konsertdeltakerne i puljer den siste veien bort til hallen.");
        data_elm2.put("nn#description", "Det blir vel nokre kilometer å gå. Man må ha gode sko. Det er faktisk viktig, sier Osmundsvaag og forklarar at vaktar kjem til å gå manngard med tau for å føre konsertdeltakerne i puljer den siste veien bort til hallen.");
        data_elm2.put("image", "http://images.com/image.jpg");
        data2.put("occurrences", data_elm2);

        names3.put("nb", "Innsiden av hjertekamrene");
        names3.put("en", "The inside of the heart chambers");
        names3.put("nn", "Innsia av hjartekamrane");
        data3.put("names", names3);

        data_elm3.put("nb#description", "Innsiden av hjertekamrene har pølseformede muskelframspring som kalles\"papillemuske\". Disse er utstyrt med sterke, hvitaktige senetråder som er festet til seilklaffene mellom for- og hjertekamrene. Fotograf: Alf Jacob Nilsen");
        data_elm3.put("en#description", "The inside of the heart chambers has sausage shaped muscle protrusion called \"papillemuske\". These are equipped with strong whitish tendon threads that are attached to the sail flaps between the front and the lower chambers. Photographer: Alf Jacob Nilsen");
        data_elm3.put("image", "http://ndla.no/sites/default/files/imagecache/Hovedspalte/images/_BIQ6040-tekst.jpg");
        data3.put("occurrences", data_elm3);

        names4.put("nb", "Gyroskop");
        names4.put("nn", "Gyroskop");
        names4.put("en", "Gyroscope");
        data4.put("names", names4);

        data_elm4.put("nb#description", "Gyroskop.Opphavsmann: Public domainEt gyroskop (kortform: en gyro) er en innretning som demonstrerer prinsippet om bevaring av bevegelsesmengde innenfor fysikken. Det består av et balansert hjul på en aksel som er opphengt slik at den fritt kan røre seg i alle retninger. Når hjulet roterer, forsøker akselen å beholde sin retning og gjør motstand mot retningsendringer.");
        data_elm4.put("image", "http://ndla.no/sites/default/files/imagecache/Hoyrespalte/images/Gyroscope_precession.gif");
        data4.put("occurrences", data_elm4);

        names5.put("nb", "Lærer");
        names5.put("nn", "Lærar");
        names5.put("en", "Teacher");
        data5.put("names", names5);

        data_elm5.put("en#description", "A teacher or schoolteacher is a person who provides education for pupils (children) and students (adults).");
        data_elm5.put("nb#description", "I Norge brukes begrepet lærer alene historisk mest om personer som underviser på grunnskolenivå (og grunnskolens forløpere).");
        data_elm5.put("nn#description", "Ein lærar er ein kunnskapsformidlar. Ein person som er utdanna til å undervise barn og ungdom blir òg kalla ein pedagog.");
        data_elm5.put("image", "http://upload.wikimedia.org/wikipedia/commons/d/db/Classroom_at_a_seconday_school_in_Pendembu_Sierra_Leone.jpg");
        data5.put("occurrences", data_elm5);

        args.put("set1", data1);
        args.put("set2", data2);
        args.put("set3", data3);
        args.put("set4", data4);
        args.put("set5", data5);

        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.saveSiteTopicsByNodeId("ndlanode_772727277712", args, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }

    public void createSiteTopic() {
        HashMap<String, TopicIF> topics = null;

        HashMap<String, HashMap<String, HashMap<String, String>>> args = new HashMap<String, HashMap<String, HashMap<String, String>>>();
        HashMap<String, HashMap<String, String>> data1 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> data2 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> data3 = new HashMap<String, HashMap<String, String>>();

        HashMap<String, HashMap<String, String>> namedata1 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> namedata2 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> namedata3 = new HashMap<String, HashMap<String, String>>();

        HashMap<String, String> names1 = new HashMap<String, String>();
        HashMap<String, String> names2 = new HashMap<String, String>();
        HashMap<String, String> names3 = new HashMap<String, String>();
        HashMap<String, String> data_elm1 = new HashMap<String, String>();
        HashMap<String, String> data_elm2 = new HashMap<String, String>();
        HashMap<String, String> data_elm3 = new HashMap<String, String>();

        names1.put("nb", "Thaimat1");
        names1.put("nn", "Thaimatar1");
        names1.put("en", "Thai food1");
        names1.put("language-neutral", "Putreputre");
        data1.put("names", names1);

        data_elm1.put("en#description", "Thai cooking places emphasis on lightly prepared dishes with strong aromatic components. The spiciness of Thai cuisine is well known. As with other Asian cuisines, balance, detail and variety are of great significance to Thai chefs. Thai food is known for its balance of three to four fundamental taste senses in each dish or the overall meal: sour, sweet, salty, and bitter.");
        data_elm1.put("nb#description", "Thaimat legger vekt på lett tilberedte retter med sterke aromatiske komponenter. Kryddertyrken i thailandsk mat er godt kjent. Som med andre asiatiske retter, balanse, detaljer og variasjon er av stor betydning for Thai kokker. Thai mat er kjent for sin balanse av tre til fire fundamentale smak sanser i hver tallerken eller det generelle måltidet: surt, søtt, salt og bittert.");
        data_elm1.put("image_1", "http://upload.wikimedia.org/wikipedia/commons/9/90/Thai_Seafood_Curry.jpg");
        data_elm1.put("image_2", "http://upload.wikimedia.org/wikipedia/commons/9/90/Thai_Seafood_Curry.jpg");
        data_elm1.put("node-author_1", "Ruttan Tuttan");
        data_elm1.put("node-author_2", "Putrid dong");

        data1.put("occurrences", data_elm1);
        args.put("set1", data1);

        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.createSiteTopics(args, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateSiteTopicsByTopicId() {
        HashMap<String, TopicIF> topics = null;

        HashMap<String, HashMap<String, HashMap<String, String>>> args = new HashMap<String, HashMap<String, HashMap<String, String>>>();
        HashMap<String, HashMap<String, String>> data1 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> data2 = new HashMap<String, HashMap<String, String>>();


        HashMap<String, HashMap<String, String>> namedata1 = new HashMap<String, HashMap<String, String>>();
        HashMap<String, HashMap<String, String>> namedata2 = new HashMap<String, HashMap<String, String>>();

        HashMap<String, String> names1 = new HashMap<String, String>();
        HashMap<String, String> names2 = new HashMap<String, String>();
        HashMap<String, String> data_elm1 = new HashMap<String, String>();
        HashMap<String, String> data_elm2 = new HashMap<String, String>();


        names1.put("nb", "Innsiden av hjertekamrene er fine");
        names1.put("en", "The inside of the heart chambers are nice");
        names1.put("nn", "Innsia av hjartekamrane er herlege");
        data1.put("names", names1);

        data_elm1.put("nb#description", "Innsiden av hjertekamrene har pølseformede muskelframspring som kalles\"papillemuske\". Disse er utstyrt med sterke, hvitaktige senetråder som er festet til seilklaffene mellom for- og hjertekamrene. Fotograf: Alf Jacob Nilsen");
        data_elm1.put("en#description", "The inside of the heart chambers has sausage shaped muscle protrusion called \"papillemuske\". These are equipped with strong whitish tendon threads that are attached to the sail flaps between the front and the lower chambers. Photographer: Alf Jacob Nilsen");
        data_elm1.put("image", "http://ndla.no/sites/default/files/imagecache/Hovedspalte/images/_BIQ6040-tekst.jpg");
        data1.put("occurrences", data_elm1);

        args.put("data", data1);

        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.updateTopicsByTopicId("ndla-topic-CVJVDAWWBR", args, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }



    public void relateSiteTopicsByNodeId() {
        ArrayList<TopicIF> topics = null;
        List<String> args = Arrays.asList(new String[]{"ndla-topic-1322"});
        try {
            //TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search.test.ndla.no", "topics-user", "index_My_Topics_13");
            topics = topicService.relateTopicsByNodeId("ndlanode_15091", args, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }

    public void updateTopicRelationsByNodeId() {
        ArrayList<TopicIF> topics = null;
        ArrayList<String> args = new ArrayList<String>();
        //args.add("ndla-topic-HHLNGZZNPB");
        args.add("ndla-topic-CMFCKLJZND");

        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.updateSiteTopicRelationsByNodeId("ndlanode_118597", args, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }

    public void deleteTopicsByTopicId() {
        ArrayList<TopicIF> topics = null;
        List<String> args = Arrays.asList(new String[]{"ndla-topic-LKSBQFIGUM"});
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.deleteSiteTopicsByTopicId(args, NdlaSite.NDLA);
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage());
        }
    }
}
