/*  Implemented by Rolf Guescini (rolf.guescini@cerpus.com)
    Date: April 8, 2013
 */
package no.ndla.service.test;

import edu.emory.mathcs.backport.java.util.Arrays;
import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;


import java.util.ArrayList;
import java.util.Collection;

public class TestSubject {

  private static final String CONFIG_FILE_PATH = "/Users/rolfguescini/adminconfig/tm-sources.xml";
  private static final String TOPICMAP_REFERENCE_KEY = "NdlaTopicsService";

  public void createSubject() {
    TopicIF createdSubject = null;
    String payload = "{\n" +
        "    \"subjectId\": \"BRT2\",\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "            \"name\": \"Educational programme for Well engineering\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#ara\",\n" +
        "            \"name\": \"برنامج تعليمي للهندسة جيدالعربية\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "            \"name\": \"Programområde for brønnteknikk\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#nno\",\n" +
        "            \"name\": \"Programområde for brønnteknikk\"\n" +
        "        }\n" +
        "    ],\n" +
        "    \"udir-psi\": \"http://psi.topic.ndla.no/#subject_BRT2\",\n" +
        "    \"psis\": [\n" +
        "        \"http://psi.topic.ndla.no/#subject_666699\"\n" +
        "    ],\n" +
        "    \"originating-sites\": [\n" +
        "        \"http://ndla.no\",\n" +
        "        \"http://fyr.ndla.no\"\n" +
        "    ]\n" +
        "}";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      createdSubject = topicService.createSubject(payload);
      if(null != createdSubject){
        System.out.println(createdSubject.toString());
      }


    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }



  public void updateSubject() {
    TopicIF updatedSubject = null;
    /*
    String payload = "{\n" +
        "    \"subjectId\": \"KRO1\",\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "            \"name\": \"Physical Education Common Subject\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "            \"name\": \"Kroppsøving Fellesfag\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#nno\",\n" +
        "            \"name\": \"Kroppsøving Fellesfag\"\n" +
        "        }\n" +
        "    ],\n" +
        "    \"udir-psi\": \"http://psi.topic.ndla.no/#subject_KRO1\",\n" +
        "    \"psis\": [\n" +
        "        \"http://psi.topic.ndla.no/#subject_46\"\n" +
        "    ],\n" +
        "    \"originating-sites\": [\n" +
        "        \"http://ndla.no\",\n" +
        "    ]\n" +
        "}";
        */
    String payload = "{\n" +
        "    \"udir-psi\": \"http://psi.topic.ndla.no/#subject_SAF1\",\n" +
        "    \"subjectId\": \"\",\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "            \"name\": \"Samfunnsfag Fellesfag\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#nno\",\n" +
        "            \"name\": \"Samfunnsfag Fellesfag\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language-id\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "            \"name\": \"Social Sciences Common Subject\"\n" +
        "        }\n" +
        "    ],\n" +
        "    \"psis\": [\n" +
        "        \"http://psi.topic.ndla.no/#subject_SAF1\",\n" +
        "        \"http://psi.topic.ndla.no/#subject_36\"\n" +
        "    ],\n" +
        "    \"originating-sites\": [\n" +
        "        \"http://fyr.ndla.no/\",\n" +
        "        \"http://ndla.no/\"\n" +
        "    ]\n" +
        "}";

    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search", "topics-user", "index_My_Topics_13");
      updatedSubject = topicService.updateSubject(payload);
      if(null != updatedSubject){
        System.out.println(updatedSubject.toString());
      }


    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }


  public void deleteSubjects() {
    String payload = "[\n" +
        "    \"http://psi.topic.ndla.no/#subject_TEST\"\n" +
        "]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
      ArrayList<TopicIF> topics = topicService.deleteSubjectBySubjectId(payload);
      if(topics.size() > 0){
        for(TopicIF topic : topics) {
          System.out.println(topic.toString());
        }
      }


    }catch (NdlaServiceException | BadObjectReferenceException be){
      System.err.println(be.getMessage());
    }
  }

  @Test
    public void getSubjects() {
        Collection<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getSubjects(50, 0, NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics != null) {
            for (NdlaTopic topic : topics) {
                System.out.println(topic.toString());
            }
        }
    }



    public void getSubjectsByTopicId() {
        Collection<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getSubjectsByTopicId(10, 0, "topic-1417176849XTEGIQBYIW", NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics != null) {
            for (NdlaTopic topic : topics) {
                System.out.println(topic.getIdentifier());
            }
        }
    }

    public void getSubjectsByKeywordId() {
        Collection<NdlaTopic> topics = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topics = topicService.getSubjectsByKeywordId(10, 0, "keyword-75030", NdlaSite.NDLA);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topics != null) {
            for (NdlaTopic topic : topics) {
                System.out.println(topic.getIdentifier());
            }
        }
    }



    public void getSubjectBySubjectId() {
        NdlaTopic topic = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);
            topic = topicService.getSubjectBySubjectId("subject_ENG1", NdlaSite.NYGIV);
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }

        // Temporary code
        if (topic != null) {

                System.out.println(topic.getIdentifier());
                System.out.println(topic.getOriginatingSites());


        }
    }
}
