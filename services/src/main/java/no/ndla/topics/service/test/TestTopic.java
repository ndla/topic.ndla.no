package no.ndla.service.test;

import edu.emory.mathcs.backport.java.util.Arrays;
import net.ontopia.topicmaps.core.TopicIF;
import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.junit.Test;

import java.util.*;

/**
 * Created by rolfguescini on 04.06.14.
 */
public class TestTopic {
  private static final String CONFIG_FILE_PATH = "/Users/rolfguescini/adminconfig/tm-sources.xml";
  private static final String TOPICMAP_REFERENCE_KEY = "NdlaTopicsService";



  public void createTopic(){

    HashMap<String,TopicIF> topics = null;

    String payload = "{\n" +
        "    \"names\": [\n" +
        "        {\n" +
        "            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\",\n" +
        "            \"name\": \"parliamentary\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "            \"name\": \"storting\"\n" +
        "        }\n" +
        "    ],\n" +
        "    \"fromKeyword\": \"keyword-1387110277NUKHEPXDDV\",\n" +
        "    \"wordClass\": \"noun\",\n" +
        "    \"descriptions\": [\n" +
        "        {\n" +
        "            \"language\": \"http://psi.oasis-open.org/iso/639/#deu\",\n" +
        "            \"content\": \"deu_DESCsdsd\"\n" +
        "        },\n" +
        "        {\n" +
        "            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\",\n" +
        "            \"content\": \"NOB_DESCsdsd\"\n" +
        "        }\n" +
        "    ],\n" +
        "    \"images\": [],\n" +
        "    \"types\": [\n" +
        /*
        "        {\n" +
        "            \"typeId\": \"http://psi.topic.ndla.no/#subordinate-role-type\"\n" +
        "        }\n" +
        */
        "    ]\n" +
        "}";

    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.createTopic(payload);

      // Temporary code
      if (topics.size() > 0) {
        Iterator<String> it = topics.keySet().iterator();
        while(it.hasNext()) {
          String topicId = (String) it.next();
          System.out.println(topicId);
        }

      }
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }


  }


  public void updateTopicsByTopicId() {
    HashMap<String,TopicIF> keyMap = null;
    /*
    String payload = "{\n" +
        "    \"topics\": [\n" +
        "        {\n" +
        "            \"topic-1423838620LSDKZBAKAU\": [\n" +
        "                {\n" +
        "                    \"approved\": \"true\",\n" +
        "                    \"wordClass\": \"noun\",\n" +
        "                    \"names\": [\n" +
        "                        {\n" +
        "                            \"name\": \"furaner\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
        "                        },\n" +
        "                        {\n" +
        "                            \"name\": \"furaner\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nno \"\n" +
        "                        },\n" +
        "                        {\n" +
        "                            \"name\": \"furans\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
        "                        }\n" +
        "                    ],\n" +
        "                    \"descriptions\": [\n" +
        "                        {\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#zho\",\n" +
        "                            \"content\": \"dinggzhi\"\n" +
        "                        }\n" +
        "                    ],\n" +
        "                    \"images\": [],\n" +
        "                    \"types\": [],\n" +
        "                    \"psis\": [\"http://psi.topic.ndla.no/topics/#topic-1423838864FTOBHZYRSD\",\"http://psi.topic.ndla.no/keywords/#keyword-1387108570MHZPUUEBIQ\"]\n" +
        "                }\n" +
        "            ]\n" +
        "        }\n" +
        "    ]\n" +
        "}\n";
        */
    String payload = "{\n" +
        "    \"topics\": [\n" +
        "        {\n" +
        "            \"topic-1432733578FKDVQZHYPT\": [\n" +
        "                {\n" +
        "                    \"approved\": \"false\",\n" +
        "                    \"visibility\": \"1\",\n" +
        "                    \"wordClass\": \"noun\",\n" +
        "                    \"names\": [\n" +
        "                        {\n" +
        "                            \"name\": \"Pergamentrull\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nob\"\n" +
        "                        },\n" +
        "                        {\n" +
        "                            \"name\": \"Pergamentrull\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#nno\"\n" +
        "                        },\n" +
        "                        {\n" +
        "                            \"name\": \"Pergamentrolle\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#deu\"\n" +
        "                        },\n" +
        "                        {\n" +
        "                            \"name\": \"Parchment roll\",\n" +
        "                            \"language\": \"http://psi.oasis-open.org/iso/639/#eng\"\n" +
        "                        }\n" +
        "                    ],\n" +
        "                    \"descriptions\": [],\n" +
        "                    \"images\": [],\n" +
        "                    \"types\": [],\n" +
        "                    \"psis\": [\n" +
        "                        \"http://psi.topic.ndla.no/topics/#topic-1432733578FKDVQZHYPT\"\n" +
        "                    ]\n" +
        "                }\n" +
        "            ]\n" +
        "        }\n" +
        "    ]\n" +
        "}";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      keyMap = topicService.updateTopicsByTopicId(payload);

    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }


    if (keyMap.size() > 0) {
      Iterator it = keyMap.keySet().iterator();
      while (it.hasNext()) {
        TopicIF tmp = (TopicIF) keyMap.get(it.next());
        System.out.println(tmp.toString());
      }
    }
  }



  public void getTopics(){
    ArrayList<NdlaTopic> topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.getTopics(100, 0);

    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void getTopicByTopicId(){
    NdlaTopic topic = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topic = topicService.getTopicByTopicId("topic-1430926447ELBZAGYXNB");
      System.out.println(topic.getPsis());
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }

  public void getRelationTopicByTopicId(){
    NdlaTopic topic = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topic = topicService.getRelationTopicByTopicId("http://www.topicmaps.org/xtm/1.0/core.xtm#subclass");
      System.out.println(topic.getImages());
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void deleteTopicByTopicId(){
    ArrayList<String> deletedIds = new ArrayList<>();
    String payload = "[\"topic-1416926911HINNJIGIOP\"]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      deletedIds = topicService.deleteTopicByTopicId(payload);

    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void saveTopicsBySubjectMatter() {
    ArrayList<String> savedIds = new ArrayList<>();
    String payload = "[\"topic-1406549667SYGWJLFMBS\",\"topic-1406539645OWZEQWLIHU\"]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      savedIds = topicService.saveTopicsBySubjectMatter("subject_ENG1", payload, NdlaSite.NDLA);

    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getTopicsBySubjectMatter(){
    JSONObject topics = null;

    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.getTopicsBySubjectMatter(100,0,"subject_BRT2-01",NdlaSite.NDLA);
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getTopicsBySubjectMatterAndNameString(){
    JSONObject topics = null;

    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.getTopicsBySubjectMatterAndNameString("er", "subject_ENG1", NdlaSite.NDLA);
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void lookUpTopicsBySearchTerm(){
    JSONObject topics = null;

    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.lookUpTopicsBySearchTerm("mak", "keyword");
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void lookUpTopicsBySearchTerm2(){
    JSONObject topics = null;

    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.lookUpTopicsBySearchTerm("mak","keyword","start","http://psi.oasis-open.org/iso/639/#nob",10,0);
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void deleteTopicsFromSubjectOntology(){
    ArrayList<String> deletedTopics = new ArrayList<>();
    String payload = "[\"topic-1406623539XYTTWYXBFE\"]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      deletedTopics = topicService.deleteTopicsFromSubjectOntology(payload,"subject_ENG1",NdlaSite.NDLA);

    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void saveTopicsByNodeId(){
    ArrayList<String> savedIds = new ArrayList<>();
    String payload = "[\"topic-1429882500UVZRCXCTGM\",\"topic-1426078607WCVNBBVWPB\",\"topic-1430121432PCKFGPNXYQ\"]";
    //String payload = "[\"topic-1408449802MXXCYEUBFH\",\"topic-1408447156CDVRNWDYEO\"]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      savedIds = topicService.saveTopicsByNodeId("15091", payload, NdlaSite.NDLA);

    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }




  public void saveTopicsByNodeIdReified(){
    ArrayList<String> savedIds = new ArrayList<>();

    String payload = "[\n" +
        "    {\n" +
        "        \"topicId\": \"topic-1432800776OPFLQPDZCI\",\n" +
        "        \"reifiers\": [\n" +
        "            {\n" +
        "                \"reifierType\": \"http://psi.topic.ndla.no/#originating-site\",\n" +
        "                \"reifierValue\": \"http://ndla.no\"\n" +
        "            },\n" +
        "            {\n" +
        "                \"reifierType\": \"http://psi.topic.ndla.no/#ontology-domain\",\n" +
        "                \"reifierValue\": \"http://psi.topic.ndla.no/#subject_BRT2-01\"\n" +
        "            }\n" +
        "        ]\n" +
        "    }\n" +
        "]";
    //String payload = "[\"topic-1408449802MXXCYEUBFH\",\"topic-1408447156CDVRNWDYEO\"]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      savedIds = topicService.saveTopicsByNodeIdReified("800031", payload, NdlaSite.NDLA);

    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getDuplicateTopics(){
    JSONArray topics  = null;

    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.getDuplicateTopics(10,0,"http://psi.topic.ndla.no/#keyword");
      System.out.println("result: " + topics.length()+" DATA: "+topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void mergeDuplicateTopics(){
    JSONArray topics  = null;
    String payload = "[\n" +
        "    {\n" +
        "        \"toTopic\": \"keyword-1387109860MSPPGUKTAQ\",\n" +
        "        \"type\": \"keyword\",\n" +
        "        \"fromTopics\": [\n" +
        "            {\n" +
        "                \"topicId\": \"keyword-1387109848ZVMQCBEUSA\"\n" +
        "            }\n" +
        "        ]\n" +
        "    }\n" +
        "]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.mergeDuplicateTopics(payload);
      System.out.println("result: " + topics.length()+" DATA: "+topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void deleteTopicsByNodeId(){
    ArrayList<String> deletedTopics = new ArrayList<>();
    String payload = "[\"topic-1408449802MXXCYEUBFH\"]";
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      deletedTopics = topicService.deleteTopicsByNodeId("15091", payload, NdlaSite.NDLA);

    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getTopicsByNodeId() {
    Collection<NdlaTopic> topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.getTopicsByNodeId(10, 0, "ndlanode_800031", NdlaSite.NDLA);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }

    // Temporary code
    if (topics != null) {
      for (NdlaTopic topic : topics) {
        System.out.println(topic.getIdentifier());
      }
    }
  }


  @Test
  public void saveOntologyTopicAssociations(){
    TreeMap<String,ArrayList<TopicIF>> topics = null;
    try {

      String payload = "{\n" +
          "    \"associations\": [\n" +
          "        {\n" +
          "            \"relationId\": \"http://psi.topic.ndla.no/#secondary-topic\",\n" +
          "            \"relationName\": \"Has secondary topic/Secondary topic for\",\n" +
          "            \"topics\": [\n" +
          "                {\n" +
          "                    \"ids\": \"topic-1443599798EZKLLOGBEY_topic-1443599811UVJYLLDCJV\",\n" +
          "                    \"assocType\": \"http://psi.topic.ndla.no/#hierarchical-relation-type\",\n" +
          "                    \"reifierId\": \"http://psi.topic.ndla.no/reifiers/#reifiedAssociation-1443618810WFXHTHJDMA\",\n" +
          "                    \"names\": \"produksjonsbrønn/produksjonsrør\",\n" +
          "                    \"roles\": [\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1443599798EZKLLOGBEY\",\n" +
          "                            \"roleName\": \"http://psi.topic.ndla.no/#topic\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1443599811UVJYLLDCJV\",\n" +
          "                            \"roleName\": \"http://psi.topic.ndla.no/#topic\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                },\n" +
          "                {\n" +
          "                    \"ids\": \"topic-1443599798EZKLLOGBEY_topic-1443599810RBEXEVNETJ\",\n" +
          "                    \"assocType\": \"http://psi.topic.ndla.no/#hierarchical-relation-type\",\n" +
          "                    \"reifierId\": \"http://psi.topic.ndla.no/reifiers/#reifiedAssociation-1443618809CSTSFHHMDR\",\n" +
          "                    \"names\": \"produksjonsbrønn/hengersystem\",\n" +
          "                    \"roles\": [\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1443599798EZKLLOGBEY\",\n" +
          "                            \"roleName\": \"http://psi.topic.ndla.no/#topic\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1443599810RBEXEVNETJ\",\n" +
          "                            \"roleName\": \"http://psi.topic.ndla.no/#topic\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                },\n" +
          "                {\n" +
          "                    \"ids\": \"topic-1443599798EZKLLOGBEY_topic-1443599811IDXAJFPJRE\",\n" +
          "                    \"names\": \"produksjonsbrønn/valgfritt utstyr\",\n" +
          "                    \"roles\": [\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1443599798EZKLLOGBEY\",\n" +
          "                            \"roleName\": \"http://psi.topic.ndla.no/#topic\"\n" +
          "                        },\n" +
          "                        {\n" +
          "                            \"topicId\": \"topic-1443599811IDXAJFPJRE\",\n" +
          "                            \"roleName\": \"http://psi.topic.ndla.no/#topic\"\n" +
          "                        }\n" +
          "                    ]\n" +
          "                }\n" +
          "            ]\n" +
          "        }\n" +
          "    ]\n" +
          "}";
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.saveOntologyTopicAssociations("subject_BRT2-01", "http://psi.udir.no/ontologi/lkt/#laereplan", payload, NdlaSite.NDLA);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void getOntologyTopics(){
    JSONObject topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.getOntologyTopics("subject_NAT1",NdlaSite.NDLA);
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getSubjectOntologyTopicRelations(){
    JSONObject topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.getSubjectOntologyTopicRelations("subject_NAT1", NdlaSite.NDLA);
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }


  public void getCurriculaOntologyTopics(){
    JSONObject topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.getCurriculaOntologyTopics("subject_BRT2-01", NdlaSite.NDLA);
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void getSubjectOntologyTopicsByTopicId(){
    JSONObject topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.getSubjectOntologyTopicsByTopicId("subject_NAT1", "topic-1429882498RXPNKTZCUK", NdlaSite.NDLA);
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }



  public void getOntologyTopicsByTopicId(){
    JSONObject topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.getOntologyTopicsByTopicId("topic-1417176849XTEGIQBYIW",NdlaSite.NDLA);

    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }




  public void getOntologyTopicsByNodeId(){
    JSONObject topics = null;
    try {
      TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY, "http://search","http://localhost:8983", "topics-user", "index_My_Topics_13");
      topics = topicService.getOntologyTopicsByNodeId("http://ndla.no/node/800031", NdlaSite.NDLA);
      System.out.println(topics);
    } catch (NdlaServiceException | BadObjectReferenceException e) {
      System.out.println(e.getMessage());
    }
  }

}
