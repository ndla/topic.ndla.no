/*  Implemented by Brett Kromkamp (brett@seria.no)
    Date: April 30, 2013
 */

package no.ndla.service.test;

import net.ontopia.topicmaps.query.core.BadObjectReferenceException;
import no.ndla.topics.service.NdlaServiceException;
import no.ndla.topics.service.NdlaSite;
import no.ndla.topics.service.TopicService;
import no.ndla.topics.service.model.NdlaTopic;
import org.junit.Test;

import java.util.Collection;

public class TestTopicService {

    // Topic map engine connection constants. Should be configurable.
    private static final String DRIVER_CLASS = "org.postgresql.Driver";
    private static final String CONNECTION_STRING = "jdbc:postgresql://localhost/rolf";
    private static final String DATABASE = "postgresql";
    private static final String USERNAME = "rolf";
    private static final String PASSWORD = "08isNzWVbwMI";
    private static final String CONNECTION_POOL = "false";

    /*
    private static final String PROPERTY_FILE_PATH = "/home/brettk/ndla.no/topic/services/topicmap/api/src/no/ndla/service/db.postgresql.props";
    private static final String TOPICMAP_REFERENCE = "RDBMS-26773001";
    */

    private static final String CONFIG_FILE_PATH = "/home/brettk/ndla.no/topic/services/topicmap/api/src/no/ndla/service/tm-sources.xml";
    private static final String TOPICMAP_REFERENCE_KEY = "NdlaTopicsService";

    @Test
    public void testOpen() {
        Collection<NdlaTopic> keywords = null;
        try {
            TopicService topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY);

            for (int i = 0; i < 1000; i++) {
                keywords = topicService.getKeywordsByNodeId(10, 0, "ndlanode_15091", NdlaSite.NDLA);
                if (keywords != null) {
                    for (NdlaTopic topic : keywords) {
                        System.out.println(topic.getIdentifier());
                    }
                }
                System.out.println("==========");
            }
        } catch (NdlaServiceException | BadObjectReferenceException e) {
            System.out.println(e.getMessage());
        }
    }
}