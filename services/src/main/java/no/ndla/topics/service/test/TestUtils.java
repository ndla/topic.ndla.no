package no.ndla.service.test;

import no.ndla.topics.service.Utils;
import org.junit.Assert;
import org.junit.Test;

public class TestUtils {

    @Test
    public void testIsEmpty() {
        Utils utils = new Utils();
        Assert.assertEquals(utils.isEmpty(""), true);
        Assert.assertEquals(utils.isEmpty(" "), true);
        Assert.assertEquals(utils.isEmpty("\t"), true);
        Assert.assertEquals(utils.isEmpty("\n"), true);
        Assert.assertEquals(utils.isEmpty("\r"), true);
        Assert.assertEquals(utils.isEmpty("1"), false);
        Assert.assertEquals(utils.isEmpty("a"), false);
        Assert.assertEquals(utils.isEmpty("-"), false);
        Assert.assertEquals(utils.isEmpty("\t\n\na"), false);
    }
}
