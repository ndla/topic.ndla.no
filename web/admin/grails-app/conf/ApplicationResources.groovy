modules = {
    application {
        resource url:'js/application.js'
    }

    moment {
        resource url: 'moment/moment-with-langs.min.js'
    }
}