import no.ndla.topics.admin.Role
import no.ndla.topics.admin.User
import no.ndla.topics.admin.UserRole

class BootStrap {

    def init = { servletContext ->
    def adminRole = new Role(authority: 'ROLE_ADMIN').save(flush: true)
    def userRole = new Role(authority: 'ROLE_USER').save(flush: true)
    def keyWordRole = new Role(authority: 'ROLE_KEYWORD_USER').save(flush: true)
    def ontologyRole = new Role(authority: 'ROLE_ONTOLOGY_USER').save(flush: true)
    def curriculaRole = new Role(authority: 'ROLE_CURRICULA_USER').save(flush: true)
    def curriculaOntologyRole = new Role(authority: 'ROLE_CURRICULAONTOLOGY_USER').save(flush: true)

		def testUser1 = new User(username: 'brett@seria.no', enabled: true, password: 'password')
		testUser1.save(flush: true)

		def testUser2 = new User(username: 'rolf.guescini@cerpus.com', enabled: true, password: 'password')
		testUser2.save(flush: true)

        def testUser19 = new User(username: 'tor-martin@cerpus.com', enabled: true, password: 'testmeg')
        testUser19.save(flush: true)
        def testUser20 = new User(username: 'monica.nordeng@cerpus.com', enabled: true, password: 'testmeg')
        testUser20.save(flush: true)
		
		//keywords
		def testUser3 = new User(username: 'testuser1', enabled: true, password: '123456')
		testUser3.save(flush: true)
		def testUser4 = new User(username: 'testuser2', enabled: true, password: '123456')
		testUser4.save(flush: true)
		def testUser5 = new User(username: 'testuser3', enabled: true, password: '123456')
		testUser5.save(flush: true)
		def testUser6 = new User(username: 'testuser4', enabled: true, password: '123456')
        testUser6.save(flush: true)
        //ontologies

		def testUser7 = new User(username: 'testuser5', enabled: true, password: '123456')
        testUser7.save(flush: true)
        def testUser8 = new User(username: 'testuser6', enabled: true, password: '123456')
        testUser8.save(flush: true)
        def testUser9 = new User(username: 'testuser7', enabled: true, password: '123456')
        testUser9.save(flush: true)
        def testUser10 = new User(username: 'testuser8', enabled: true, password: '123456')
        testUser10.save(flush: true)


        def testUser11 = new User(username: 'testuser9', enabled: true, password: '123456')
        testUser11.save(flush: true)
        def testUser12 = new User(username: 'testuser10', enabled: true, password: '123456')
        testUser12.save(flush: true)
        def testUser13 = new User(username: 'testuser11', enabled: true, password: '123456')
        testUser13.save(flush: true)
        def testUser14 = new User(username: 'testuser12', enabled: true, password: '123456')
        testUser14.save(flush: true)


        def testUser15 = new User(username: 'testuser13', enabled: true, password: '123456')
        testUser15.save(flush: true)
        def testUser16 = new User(username: 'testuser14', enabled: true, password: '123456')
        testUser16.save(flush: true)
        def testUser17 = new User(username: 'testuser15', enabled: true, password: '123456')
        testUser17.save(flush: true)
        def testUser18 = new User(username: 'testuser16', enabled: true, password: '123456')
        testUser18.save(flush: true)



		UserRole.create testUser1, adminRole, true
		UserRole.create testUser2, adminRole, true

        UserRole.create testUser19, adminRole, true
        UserRole.create testUser20, adminRole, true

		UserRole.create testUser3, keyWordRole, true
		UserRole.create testUser4, keyWordRole, true
		UserRole.create testUser5, keyWordRole, true
		UserRole.create testUser6, keyWordRole, true

		UserRole.create testUser7, ontologyRole, true
		UserRole.create testUser8, ontologyRole, true
        UserRole.create testUser9, ontologyRole, true
		UserRole.create testUser10, ontologyRole, true


		UserRole.create testUser11, curriculaRole, true
        UserRole.create testUser12, curriculaRole, true
        UserRole.create testUser13, curriculaRole, true
        UserRole.create testUser14, curriculaRole, true

        UserRole.create testUser15, curriculaOntologyRole, true
        UserRole.create testUser16, curriculaOntologyRole, true
        UserRole.create testUser17, curriculaOntologyRole, true
        UserRole.create testUser18, curriculaOntologyRole, true

       // assert User.count() == 12
        //assert Role.count() == 2
        //assert UserRole.count() == 12

    }

    def destroy = {
    }


}
