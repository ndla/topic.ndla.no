modules = {
    core {
        resource url: 'bootstrap/css/bootstrap.min.css'
        //resource url: 'http://code.jquery.com/jquery.js'
        resource url: 'bootstrap/js/bootstrap.min.js'
        resource url: 'css/main.css'
    }

    fontawesome {
        resource url: 'font-awesome/css/font-awesome.css'
    }

    moment {
        resource url: 'moment/moment-with-langs.min.js'
    }

    jquery {
        resource url: 'js/jqueryDownoaded.js'
    }
}