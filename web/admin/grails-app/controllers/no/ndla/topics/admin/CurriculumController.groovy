package no.ndla.topics.admin

import grails.plugins.springsecurity.Secured
import groovy.json.JsonSlurper
import no.ndla.topics.service.NdlaServiceException
import no.ndla.topics.service.NdlaSite
import no.ndla.topics.service.model.NdlaTopic
import org.codehaus.jettison.json.JSONArray
import org.codehaus.jettison.json.JSONObject

import javax.annotation.PostConstruct


@Secured(['ROLE_ADMIN','ROLE_CURRICULA_USER','ROLE_CURRICULAONTOLOGY_USER'])
class CurriculumController {
    def adminTopicService // Dependency Injection (DI) by the Spring container.
    def serviceController

    @PostConstruct
    void init() {
        if(null == adminTopicService.getTopicService()){
            adminTopicService.setTopicService();
        }
    }

    def index() {
    	flash.warning = "The curricula-related functionality has not been implemented, yet."
        redirect(controller: 'keyword', action: 'lookupKeyword')
    }

    def getSubjects() {
        def subjects = [:]

        if(!null == params.sites){
            ArrayList<NdlaTopic> subjectArray = adminTopicService.getSubjects(50,0,NdlaSite.NDLA);
            subjectArray.each{ subject ->
                subjects.put(subject.getIdentifier(),subject.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }
        }

        [subjects: subjects,  sites: params.sites,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
    }

    def doSetSubject() {
        def subjectId = ""
        //withForm {
            try {
                subjectId = params.subject
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to index the keywords."
            }

            redirect(controller: 'curriculum', action: 'getCurriculum', params: [subjectId: subjectId, sites: params.sites])


        //}.invalidToken {
            // Bad request.
        //}
    }

    def doSetSubjectForAddHierarchicalRelation(){
        def relationSubject = ""
        def relationSite = ""
        //withForm {
            try {
                relationSubject = params.relationSubjects
                relationSite = params.relationSites


            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to index the keywords."
            }

            redirect(controller: 'curriculum', action: 'addHierarchicalRelation', params: [relationSubject: relationSubject, relationSite: relationSite])


        //}.invalidToken {
            // Bad request.
        //}
    }

    def doSetSubjectForAddHorizontalRelation(){
        def relationSubject = ""
        def relationSite = ""
        //withForm {
            try {
                relationSubject = params.relationSubjects
                relationSite = params.relationSites


            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to index the keywords."
            }

            redirect(controller: 'curriculum', action: 'addHorizontalRelation', params: [relationSubject: relationSubject, relationSite: relationSite])


        //}.invalidToken {
            // Bad request.
        //}
    }

    def getCurriculum() {
        def subjectId = params.subjectId
        def sites = params.sites
        NdlaTopic subjectTopic = null
        def subjectName = ""
        def siteSubjectId = ""
        def curriculum = null

        if(null != params.sites){
            switch (params.sites) {
                case "NDLA":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NDLA)
                    break;
                case "DELING":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.DELING)
                    break;
                case "FYR":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NYGIV)
                    break;
            }
        }


        if(null != subjectTopic && null != params.sites) {
            def names = subjectTopic.getNames()
            subjectName = names.get("http://psi.oasis-open.org/iso/639/#eng")
            def subjectIds = subjectTopic.getPsis();
            def subjectUdirId = subjectTopic.getUdirPsi();
            def idBit = ""
            subjectIds.each{tempId ->
                if(tempId != subjectUdirId) {
                    //ta høyde for fyr-ids
                    def idArray = tempId.toString().split("_");

                    if(idArray.length > 2){
                        if(idArray[1] == "fyr" || idArray[1] == "deling"){
                            idBit = idArray[2];
                            siteSubjectId = tempId.toString().substring(tempId.toString().lastIndexOf("#")+1);
                        }
                    }
                    else{
                        idBit = idArray[1];
                        if(idBit.matches("-?\\d+")){
                            siteSubjectId = tempId.toString().substring(tempId.toString().lastIndexOf("#")+1);
                        }
                    }
                }
            }


            def udirId = ""
            def nameMap = [:]
            def curriculumRenderArray = [:]
            def curriculumLevels = [:]
            def foundLevels = []
            def foundMaps = [:]
            def langMap = [:]
            def setMapping = []
            def structureMap = [:]
            JSONObject langJSON = new JSONObject()
            //def curriculum = adminTopicService.getCurriculumData()
            //System.out.println(" siteSubjectId: "+siteSubjectId)
            if(siteSubjectId.length() > 0) {
                def courses = adminTopicService.getCourses(siteSubjectId)
                if(courses.courseStructures.size() > 0) {
                    def currentCurriculumName = ""
                    courses.courseStructures.each{ courseStructureElement ->
                        if("subject_"+courseStructureElement.courseStructure.id.toString().contains(subjectId)
                                || courseStructureElement.courseStructure.id.toString().contains(idBit)) {
                            udirId = courseStructureElement.courseStructure.id
                            //System.out.println(" udirId: "+udirId)
                            curriculum = adminTopicService.getCurriculumData(udirId)
                            def udirIdBit = udirId.toString().substring(0,udirId.toString().lastIndexOf("-"))
                           // System.out.println(" udirIdBit: "+udirIdBit)

                            if(udirId.length() > 0 && (subjectId.contains(udirId) || subjectId.contains(udirIdBit) || subjectId.contains(idBit))) {

                                curriculumRenderArray['id'] = curriculum.id
                                setMapping = getCurriculaSetMapping(curriculum.id)

                                //get the name of the curriculum
                                curriculum.names.each{ curriculumName ->
                                    if(curriculumName.scopes.contains("http://psi.oasis-open.org/iso/639/#nob")){
                                        currentCurriculumName =  curriculumName.name
                                        true// break
                                    }
                                    else if(currentCurriculumName == "" && curriculumName.scopes.contains("http://psi.oasis-open.org/iso/639/#nno")){
                                        currentCurriculumName = curriculumName.name
                                        true// break
                                    }
                                }//end if names

                                if(currentCurriculumName == ""){
                                    curriculum.names.each{ curriculumName ->
                                        if(currentCurriculumName == "" && curriculumName.scopes.contains("http://psi.oasis-open.org/iso/639/#eng")){
                                            currentCurriculumName =  curriculumName.name
                                            true// break
                                        }
                                        else if(currentCurriculumName == "" && curriculumName.scopes.contains("http://psi.mycurriculum.org/#universal-type")){
                                            currentCurriculumName =  curriculumName.name
                                            true// break
                                        }
                                    }//end if names

                                }//end if currentCurriculumName is empty
                                curriculumRenderArray['curriculumName'] = currentCurriculumName

                            }// if we have udirid
                        }//end if contains subjectID
                    }//end each courseStruture
                }//end if courses

                def levelSetMapping = [:]

                curriculum.competenceAimSets.each { competenceAimSet ->
                    if (setMapping.contains(competenceAimSet.id)) {
                        def currentCompetenceAimSets = [:]
                        def competenceSetLevel = competenceAimSet.levels[0].id;
                        curriculumLevels[competenceSetLevel] = [:]

                        if(!structureMap.containsKey(competenceSetLevel)){
                            structureMap[competenceSetLevel] = [:]
                        }

                        //System.out.println(" competenceAimSet: " + competenceAimSet.id + " SETMAPPING: " + setMapping)
                        competenceAimSet.competenceAimSets.each{ levelSet ->
                            def setIdArray = levelSet.id.toString().split("_")
                            def currentSetId = setIdArray[0]+"_"+setIdArray[1]
                            def levelSetId = competenceSetLevel+"_"+setIdArray[0]+"_"+setIdArray[1]
                            levelSetMapping[levelSetId] = currentSetId;
                            structureMap[competenceSetLevel][levelSet.id] = []
                            def currentSetName = ""
                            //System.out.println(" currentSetId: " + currentSetId + " levelSetId: " + levelSetId)

                            levelSet.names.each{ levelSetName ->
                                if(levelSetName.scopes.contains("http://psi.oasis-open.org/iso/639/#nob")){
                                    nameMap[levelSet.id] = levelSetName.name
                                    currentSetName  = levelSetName.name
                                    true //break
                                }
                                else if(currentSetName == "" && levelSetName.scopes.contains("http://psi.oasis-open.org/iso/639/#nno")){
                                    nameMap[levelSet.id] = levelSetName.name
                                    currentSetName  = levelSetName.name
                                    true //break
                                }
                            }//end each levelset.Names

                            if(currentSetName == ""){
                                levelSet.names.each{ levelSetName ->

                                    if(currentSetName == "" && levelSetName.scopes.contains("http://psi.oasis-open.org/iso/639/#eng")){
                                        nameMap[levelSet.id] = levelSetName.name
                                        currentSetName  = levelSetName.name
                                        true //break
                                    }
                                    else if(currentSetName == "" && levelSetName.scopes.contains("http://psi.mycurriculum.org/#universal-type")){
                                        nameMap[levelSet.id] = levelSetName.name
                                        currentSetName  = levelSetName.name
                                        true //break
                                    }
                                }//end each levelset.Names
                            }//end if name still empty

                            def currentCompetenceAims = [:]
                            levelSet.competenceAims.each{ competenceAim ->
                                def tempCompetenceAimName = "";
                                competenceAim.names.each{ competenceAimName ->
                                    if(competenceAimName.scopes.contains("http://psi.oasis-open.org/iso/639/#nob")){
                                        tempCompetenceAimName = competenceAimName.name
                                        true //break
                                    }
                                    else if(tempCompetenceAimName == "" && competenceAimName.scopes.contains("http://psi.oasis-open.org/iso/639/#nno")){
                                        tempCompetenceAimName = competenceAimName.name
                                        true // break
                                    }
                                }//end each competenceAim.names

                                if(tempCompetenceAimName == ""){
                                    competenceAim.names.each{ competenceAimName ->
                                        if(tempCompetenceAimName == "" && competenceAimName.scopes.contains("\"http://psi.oasis-open.org/iso/639/#eng\"")){
                                            tempCompetenceAimName = competenceAimName.name
                                            return
                                        }
                                        else if(tempCompetenceAimName == "" && competenceAimName.scopes.contains("\"http://psi.mycurriculum.org/#universal-type\"")){
                                            tempCompetenceAimName = competenceAimName.name
                                            return
                                        }
                                    }//end each competenceAim.names
                                }//end if name still empty
                                currentCompetenceAims[competenceAim.id] =  tempCompetenceAimName
                                structureMap[competenceSetLevel][levelSet.id] = currentCompetenceAims
                            }//end each levelSet.competenceAims

                            /*
                            if(setMapping.contains(currentSetId)){
                                if(!foundMaps.containsKey(currentSetName)){
                                    currentCompetenceAimSets[levelSet.id] = currentCompetenceAims
                                    def tempMap = [:]
                                    tempMap.put(levelSet.id,currentCompetenceAims)
                                    foundMaps.put(currentSetName,tempMap)
                                }
                                else{
                                    def tempData = foundMaps.get(currentSetName)
                                    String[] keys = tempData.keySet() as String[]
                                    def levelSetKey = keys[0]
                                    def tempCompetenceAims = tempData[levelSetKey]

                                    currentCompetenceAimSets[levelSetKey] = tempCompetenceAims + currentCompetenceAims
                                }
                            }
                            else{
                                currentCompetenceAimSets[levelSet.id] = currentCompetenceAims
                            }
                            */

                            /*

                            if(levelSetMapping[levelSetId] == currentSetId){
                                //System.out.println(levelSetId+" for "+levelSetMapping[levelSetId]+" ER LIK "+currentSetId)
                                if(!foundMaps.containsKey(competenceSetLevel+"_"+levelSet.id)){
                                    System.out.println("FANT IKKE: "+competenceSetLevel+"_"+levelSet.id)
                                    currentCompetenceAimSets[levelSet.id] = currentCompetenceAims
                                    //System.out.println("SETTER HOVEDOMRÅDE: "+levelSet.id)
                                    def tempMap = [:]
                                    tempMap.put(levelSet.id,currentCompetenceAims)
                                    foundMaps.put(competenceSetLevel+"_"+levelSet.id,tempMap);
                                    curriculumLevels[competenceSetLevel] = currentCompetenceAimSets;
                                }
                                else{
                                    def tempData = foundMaps.get(competenceAimSet.levels[0].id+"_"+levelSet.id)
                                    String[] keys = tempData.keySet() as String[]
                                    def levelSetKey = keys[0]
                                    System.out.println("Fetching levelsetkey: "+levelSetKey)
                                    def tempCompetenceAims = tempData[levelSetKey]
                                    def combinedAims = tempCompetenceAims + currentCompetenceAims
                                    currentCompetenceAimSets[levelSetKey] = combinedAims
                                    curriculumLevels[competenceSetLevel] = currentCompetenceAimSets;

                                }
                               // System.out.println("Adding: "+competenceSetLevel+" => "+levelSet.id)


                            }
                            */
                            nameMap[competenceSetLevel] = competenceAimSet.levels[0].names[0].name

                        }//end competenceAimSet.competenceAimSets.each
                    }//end if setMapping contains set
                }//end competenceAimSets.each



                structureMap.each{ level, mainareas ->
                    def foundMainAreas = [:]
                    def areaIdMap = [:]
                    mainareas.each{ mainAreaId, aims ->
                        def idMap = mainAreaId.split("_")
                        def areaCounterId = idMap[2]
                        def name = nameMap[mainAreaId];

                        if(foundMainAreas.containsKey(areaCounterId) && foundMainAreas[areaCounterId] == name){
                            //System.out.println("foundMainAreas has "+areaCounterId+"_"+name)
                            def merged = mergeMainAreas(mainAreaId, name, nameMap, mainareas)
                            //System.out.println("THE MERGE : "+merged)
                            structureMap[level][areaIdMap.get(areaCounterId)] = null;
                            structureMap[level][mainAreaId] = merged
                        }
                        areaIdMap.put(areaCounterId,mainAreaId)
                        foundMainAreas.put(areaCounterId,name)
                    }
                }

                curriculumRenderArray["levels"] = structureMap//curriculumLevels
                //System.out.println("structureMap "+structureMap)
                //System.out.println("nameMap "+nameMap)

                ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
                languageArray.each{ languageItem ->
                    langJSON.put(languageItem.getIdentifier(),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
                }
            }



            //System.out.println(" curriculumRenderArray: "+curriculumRenderArray)

            [curriculumRenderArray: curriculumRenderArray,nameMap: nameMap, subjectName: subjectName, subjectId: subjectId,siteSubjectId:siteSubjectId, site: sites, langMap: langJSON,searchDomain: grailsApplication.config.INDEX_SERVICE_URL, pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
        else{
            flash.error = "An error occurred while trying to fetch the subject for the site "+params.sites+". It was probably not configured for this site"
            redirect(controller: 'curriculum', action: 'getSubjects')
        }
    }

    def mergeMainAreas(mainId, mainName, nameMap, mainAreas){
        def map = []
        def foundMainAreas = [:]
        def aimsMap = [:]
        mainAreas.each{mainAreaId, aims ->
            def idMap = mainAreaId.split("_")
            def areaCounterId = idMap[2]
            def name = nameMap[mainAreaId];

            if(foundMainAreas.containsKey(areaCounterId) && foundMainAreas[areaCounterId] == name && name == mainName){
                map = aimsMap[areaCounterId] + aims
                return;
            }
            aimsMap[areaCounterId] = aims;
            foundMainAreas.put(areaCounterId,name)
        }

        return map;
    }

    def getCurriculaSetMapping(String curriculumId) {
        def map = []
        File myFile = grailsApplication.mainContext.getResource("aimSetMappings.json").file
        String fileContents = myFile.getText('UTF-8')
        JSONObject jData = new JSONObject(fileContents);
        JSONArray mapping = jData.getJSONArray(curriculumId)
        for(int i = 0; i < mapping.length(); i++){
            map.add(mapping.getString(i));
        }
        return map;
    }

    def doSaveAimTopics(){
        def chosenTopics = params.list("chosenTopics")
        def chosenTopicRelations = params.list("chosenTopicRelations")
        def chosenTopicRelationRoles = params.list("chosenTopicRelationRoles")
        def chosenTopicOtherRelationRoles = params.list("chosenTopicOtherRelationRoles")

        def existingTopics = params.list("existingTopics")
        def existingTopicRelations = params.list("existingTopicRelations")
        def existingTopicRelationRoles = params.list("existingTopicRelationRoles")
        def existingTopicOtherRelationRoles = params.list("existingTopicOtherRelationRoles")
        //System.out.println("PARAMS: "+params);
        def subjectId = params.subjectId
        def site = params.site
/*
        System.out.println("chosenTopics: "+chosenTopics);
        System.out.println("chosenTopicRelations: "+chosenTopicRelations);
        System.out.println("chosenTopicRelationRoles: "+chosenTopicRelationRoles);
        System.out.println("chosenTopicOtherRelationRoles: "+chosenTopicOtherRelationRoles);


        System.out.println("existingTopics: "+existingTopics);
        System.out.println("existingTopicRelations: "+existingTopicRelations);
        System.out.println("existingTopicRelationRoles: "+existingTopicRelationRoles);
        System.out.println("existingTopicOtherRelationRoles: "+existingTopicOtherRelationRoles);
*/

        def topics = [:]
        def existingTopicsArray = [:]
        def movedTopics = []

        def lastAimId = ""
        def foundTopics = []
        def lastExistingAimId = ""
        def foundExistingTopics = []

        def relations = [:]
        def lastRelation = ""
        def foundRelations = []

        def existingRelations = [:]
        def existingLastRelation = ""
        def existingFoundRelations = []

        def roles = [:]
        def lastRelationRole = ""
        def foundRelationRoles = []

        def existingRoles = [:]
        def existingLastRelationRole = ""
        def existingFoundRelationRoles = []


        def otherRoles = [:]
        def otherLastRelationRole = ""
        def otherFoundRelationRoles = []

        def existingOtherRoles = [:]
        def existingOtherLastRelationRole = ""
        def existingOtherFoundRelationRoles = []


        if(chosenTopics.size() > 0) {
            chosenTopics.each { chosenTopic ->
                def topicArray = chosenTopic.toString().split("_")
                def aimId = topicArray[2]
                def topicId = topicArray[4]

                if (existingTopics.contains(chosenTopic)) {
                    movedTopics.add(chosenTopics)

                }

                if (lastAimId != "" && lastAimId != aimId) {
                    foundTopics = []
                }
                lastAimId = aimId
                topics[lastAimId] = foundTopics
                foundTopics.add(topicId)

            }
        }

        if(existingTopics.size() > 0) {
            existingTopics.each { existingTopic ->
                def existingTopicArray = existingTopic.toString().split("_")
                def existingAimId = existingTopicArray[2]
                def existingTopicId = existingTopicArray[4]

                if (lastExistingAimId != "" && lastExistingAimId != existingAimId) {
                    foundExistingTopics = []
                }
                lastExistingAimId = existingAimId
                existingTopicsArray[lastExistingAimId] = foundExistingTopics
                foundExistingTopics.add(existingTopicId)

            }
        }

        if(chosenTopicRelations.size() > 0){
            chosenTopicRelations.each{ chosenTopicRelation ->
                def httpArray = chosenTopicRelation.toString().split("_http")
                def topicRelationArray = httpArray[0].toString().split("_")

                def relationAimId = topicRelationArray[2]
                def relationId = "http"+httpArray[1].toString().trim()
                if(lastRelation != "" && lastRelation != relationAimId){
                    foundRelations = []

                }
                lastRelation = relationAimId
                relations[lastRelation] = foundRelations
                foundRelations.add(relationId)

            }
        }

        if(existingTopicRelations.size() > 0) {
            existingTopicRelations.each { chosenExistingTopicRelation ->
                def httpArray = chosenExistingTopicRelation.toString().split("_http")
                def existingTopicRelationArray = httpArray[0].toString().split("_")

                def existingRelationAimId = existingTopicRelationArray[2]
                def existingRelationId = "http" + httpArray[1].toString().trim()
                if (existingLastRelation != "" && existingLastRelation != existingRelationAimId) {
                    existingFoundRelations = []

                }
                existingLastRelation = existingRelationAimId
                existingRelations[existingLastRelation] = existingFoundRelations
                existingFoundRelations.add(existingRelationId)

            }
        }

        if(chosenTopicRelationRoles.size() > 0) {
            chosenTopicRelationRoles.each { chosenTopicRelationRole ->
                def httpArray = chosenTopicRelationRole.toString().split("_http")
                def topicRelationRoleArray = httpArray[0].toString().split("_")

                def relationRoleAimId = topicRelationRoleArray[2]
                def relationRoleId = "http" + httpArray[1].toString().trim()
                if (lastRelationRole != "" && lastRelationRole != relationRoleAimId) {
                    foundRelationRoles = []

                }
                lastRelationRole = relationRoleAimId
                roles[lastRelationRole] = foundRelationRoles
                foundRelationRoles.add(relationRoleId)
            }
        }

        if(existingTopicRelationRoles.size() > 0) {
            existingTopicRelationRoles.each { existingTopicRelationRole ->
                def httpArray = existingTopicRelationRole.toString().split("_http")
                def existingTopicRelationRoleArray = httpArray[0].toString().split("_")

                def existingRelationRoleAimId = existingTopicRelationRoleArray[2]
                def existingRelationRoleId = "http" + httpArray[1].toString().trim()
                if (existingLastRelationRole != "" && existingLastRelationRole != existingRelationRoleAimId) {
                    existingFoundRelationRoles = []

                }
                existingLastRelationRole = existingRelationRoleAimId
                existingRoles[existingLastRelationRole] = existingFoundRelationRoles
                existingFoundRelationRoles.add(existingRelationRoleId)
            }
        }

        if(chosenTopicOtherRelationRoles.size() > 0) {
            chosenTopicOtherRelationRoles.each { chosenTopicOtherRelationRole ->
                def httpOtherArray = chosenTopicOtherRelationRole.toString().split("_http")
                def topicOtherRelationRoleArray = httpOtherArray[0].toString().split("_")

                def relationOtherRoleAimId = topicOtherRelationRoleArray[2]
                def relationOtherRoleId = "http" + httpOtherArray[1].toString().trim()
                if (otherLastRelationRole != "" && otherLastRelationRole != relationOtherRoleAimId) {
                    otherFoundRelationRoles = []

                }
                otherLastRelationRole = relationOtherRoleAimId
                otherRoles[otherLastRelationRole] = otherFoundRelationRoles
                otherFoundRelationRoles.add(relationOtherRoleId)
            }
        }

        if(existingTopicOtherRelationRoles.size() > 0) {
            existingTopicOtherRelationRoles.each { existingTopicOtherRelationRole ->
                def httpOtherArray = existingTopicOtherRelationRole.toString().split("_http")
                def existingTopicOtherRelationRoleArray = httpOtherArray[0].toString().split("_")

                def existingRelationOtherRoleAimId = existingTopicOtherRelationRoleArray[2]
                def existingRelationOtherRoleId = "http" + httpOtherArray[1].toString().trim()
                if (existingOtherLastRelationRole != "" && existingOtherLastRelationRole != existingRelationOtherRoleAimId) {
                    existingOtherFoundRelationRoles = []

                }
                existingOtherLastRelationRole = existingRelationOtherRoleAimId
                existingOtherRoles[existingOtherLastRelationRole] = existingOtherFoundRelationRoles
                existingOtherFoundRelationRoles.add(existingRelationOtherRoleId)
            }
        }


        JSONObject payload = new JSONObject()
        JSONArray aimTopics = new JSONArray()
        topics.each {aimIdentifier, aimData ->
            JSONObject aimObject = new JSONObject()
            aimObject.put("aimId",aimIdentifier)
            JSONArray topicsArray = new JSONArray()
            def topicCounter = 0
            aimData.each{ topic ->
                JSONObject topicObject = new JSONObject()
                topicObject.put("topicId",topic)
                def relationArr = [:]
                if(relations.size() > 0){
                    relationArr = relations.get(aimIdentifier);
                }

                def roleArr = [:]
                if(roles.size() > 0) {
                    roleArr = roles.get(aimIdentifier);
                }

                def otherRoleArr = [:]
                if(otherRoles.size() > 0) {
                    otherRoleArr = otherRoles.get(aimIdentifier)
                }

                if(relationArr.size() > 0) {
                    topicObject.put("relationTypeId",relationArr[topicCounter])
                }
                else{
                    topicObject.put("relationTypeId","")
                }

                if(roleArr.size() > 0){
                    topicObject.put("roleTypeId1",roleArr[topicCounter])
                }
                else{
                    topicObject.put("roleTypeId1","")
                }

                if(otherRoleArr.size() > 0) {
                    topicObject.put("roleTypeId2",otherRoleArr[topicCounter])
                }
                else{
                    topicObject.put("roleTypeId2","")
                }

                topicsArray.put(topicObject)
                topicCounter++
            }
            aimObject.put("topics",topicsArray)
            aimTopics.put(aimObject)
        }


        existingTopicsArray.each {existingAimIdentifier, existingAimData ->
            JSONObject existingAimObject = new JSONObject()
            existingAimObject.put("aimId",existingAimIdentifier)
            JSONArray existingTopicsJSONArray = new JSONArray()
            def existingTopicCounter = 0
            existingAimData.each{ existingTopic ->
                JSONObject existingTopicObject = new JSONObject()
                existingTopicObject.put("topicId",existingTopic)
                def existingRelationArr = [:]
                if(existingRelations.size() > 0) {
                    existingRelationArr = existingRelations.get(existingAimIdentifier)
                }

                def existingRoleArr = [:]
                if(existingRoles.size() > 0) {
                    existingRoleArr =  existingRoles.get(existingAimIdentifier);
                }

                def existingOtherRoleArr = [:]
                if(existingOtherRoles.size() > 0) {
                    existingOtherRoleArr = existingOtherRoles.get(existingAimIdentifier)
                }



                if(existingRelationArr.size() > 0) {
                    existingTopicObject.put("relationTypeId",existingRelationArr[existingTopicCounter])
                }
                else{
                    existingTopicObject.put("relationTypeId","")
                }

                if(existingRoleArr.size() > 0){
                    existingTopicObject.put("roleTypeId1",existingRoleArr[existingTopicCounter])
                }
                else{
                    existingTopicObject.put("roleTypeId1","")
                }

                if(existingOtherRoleArr.size() > 0){
                    existingTopicObject.put("roleTypeId2",existingOtherRoleArr[existingTopicCounter])
                }
                else{
                    existingTopicObject.put("roleTypeId2","")
                }


                existingTopicsJSONArray.put(existingTopicObject)
                existingTopicCounter++
            }
            existingAimObject.put("topics",existingTopicsJSONArray)
            aimTopics.put(existingAimObject)
        }

        payload.put("aimtopics",aimTopics)
        //System.out.println("PAYLOAD: "+payload.toString());


        try {
            def data = null
            if(site.toString().equals("NDLA")){
                data = adminTopicService.saveAimHasTopic(subjectId,payload.toString(),NdlaSite.NDLA)
            }
            else if(site.toString().equals("FYR")){
                data = adminTopicService.saveAimHasTopic(subjectId,payload.toString(),NdlaSite.NYGIV)
            }
            else{
                data = adminTopicService.saveAimHasTopic(subjectId,payload.toString(),NdlaSite.DELING)
            }

            //System.out.println("WE GOT DATA BACK: "+data)
            flash.success = "The topic(s) were successfully related to the competence aim(s)."
        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to update the keyword."
        }

        redirect(controller: 'curriculum', action: 'getCurriculum',params: [subjectId: subjectId,sites: site] )
    }


    def addHierarchicalRelation(){
        def relationSubject = params.relationSubject
        def relationSite = params.relationSite

        if(relationSubject == null && relationSite == null){
            def subjects = [:]

            ArrayList<NdlaTopic> subjectArray = adminTopicService.getSubjects(50,0,NdlaSite.NDLA);
            subjectArray.each{ subject ->
                subjects.put(subject.getIdentifier(),subject.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }


            [subjects: subjects,  relationSite: relationSite,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
        else if(relationSubject != "" && relationSite != ""){
            def langMap = [:]
            def Language lang;

            ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
            languageArray.each{ languageItem ->
                langMap.put(languageItem.getIdentifier(),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }

            ArrayList<NdlaTopic> hierarchicalTopicTypes = adminTopicService.getHierarchicalAssociationTypes()
            def found = []
            def hierarchicalTypeMap = [:]
            hierarchicalTopicTypes.each { hierarchicalTypeTopic ->
                def psi = hierarchicalTypeTopic.getPsi()

                def names = hierarchicalTypeTopic.getRelationshipNames()
                def isoNames = []
                names.each { key, map ->
                    map.each{ mapKey, val ->
                        if(mapKey == "http://psi.oasis-open.org/iso/639/#eng"){
                            if(!found.contains(mapKey+"_"+val)){
                                found.add(mapKey+"_"+val)
                                isoNames.add(val)
                            }
                        }
                    }
                }
                def name = isoNames.join(" / ")
                if(name.toString().length() > 0) {
                    hierarchicalTypeMap.put(psi,name)
                }
            }

            JSONObject topics = null;
            if(relationSite == "NDLA"){
                topics = adminTopicService.getTopicsBySubjectMatter(100, 0,relationSubject,NdlaSite.NDLA)
            }
            else if(relationSite == "DELING"){
                topics = adminTopicService.getTopicsBySubjectMatter(100,0,relationSubject,NdlaSite.DELING)
            }
            else if(relationSite == "FYR"){
                topics = adminTopicService.getTopicsBySubjectMatter(100, 0,relationSubject,NdlaSite.NYGIV)
            }

            JSONArray subjectTopics = new JSONArray();
            JSONArray relations = topics.getJSONArray("relations");
            for(int i = 0; i < relations.length(); i ++) {
                JSONObject relation = relations.getJSONObject(i);
                JSONArray relatedTopics = relation.getJSONArray("relatedTopics");
                for(int j = 0; j < relatedTopics.length(); j ++) {
                    JSONObject relatedTopic = relatedTopics.getJSONObject(j);
                    String topicId = relatedTopic.getString("topicId");
                    topicId = topicId.substring(topicId.lastIndexOf("#")+1);
                    subjectTopics.put(topicId);
                }
            }//end for

            //System.out.println("hierarchicalTypeMap: "+hierarchicalTypeMap)
            [relationSubject: relationSubject, relationSite: relationSite, hierarchicalTypeMap: hierarchicalTypeMap, languages: langMap,subjectTopics: subjectTopics, searchDomain: grailsApplication.config.INDEX_SERVICE_URL, pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
    }


    def addHorizontalRelation(){
        def relationSubject = params.relationSubject
        def relationSite = params.relationSite

        if(relationSubject == null && relationSite == null){
            def subjects = [:]


            ArrayList<NdlaTopic> subjectArray = adminTopicService.getSubjects(50,0,NdlaSite.NDLA);
            subjectArray.each{ subject ->
                subjects.put(subject.getIdentifier(),subject.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }


            [subjects: subjects,  relationSite: relationSite,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
        else if(relationSubject != "" && relationSite != ""){
            def langMap = [:]
            def Language lang;

            ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
            languageArray.each{ languageItem ->
                langMap.put(languageItem.getIdentifier(),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }

            ArrayList<NdlaTopic> horizontalTopicTypes = adminTopicService.getHorizontalAssociationTypes()
            def found = []
            def horizontalTypeMap = [:]
            horizontalTopicTypes.each { horizontalTypeTopic ->
                def psi = horizontalTypeTopic.getPsi()

                def names = horizontalTypeTopic.getRelationshipNames()

                def isoNames = []
                def nameCount = 0
                names.each { key, map ->
                    map.each{ mapKey, val ->
                        if(mapKey == "http://psi.oasis-open.org/iso/639/#eng"){
                            if(!found.contains(mapKey+"_"+val)){
                                found.add(mapKey+"_"+val)
                                if(nameCount++ <= 1){
                                    isoNames.add(val)
                                }

                            }
                        }
                    }
                }
                def name = isoNames.join(" / ")
                if(name.toString().length() > 0) {
                    horizontalTypeMap.put(psi,name)
                }
            }

            JSONObject topics = null;
            if(relationSite == "NDLA"){
                topics = adminTopicService.getTopicsBySubjectMatter(100, 0,relationSubject,NdlaSite.NDLA)
            }
            else if(relationSite == "DELING"){
                topics = adminTopicService.getTopicsBySubjectMatter(100,0,relationSubject,NdlaSite.DELING)
            }
            else if(relationSite == "FYR"){
                topics = adminTopicService.getTopicsBySubjectMatter(100, 0,relationSubject,NdlaSite.NYGIV)
            }

            JSONArray subjectTopics = new JSONArray();
            JSONArray relations = topics.getJSONArray("relations");
            for(int i = 0; i < relations.length(); i ++) {
                JSONObject relation = relations.getJSONObject(i);
                JSONArray relatedTopics = relation.getJSONArray("relatedTopics");
                for(int j = 0; j < relatedTopics.length(); j ++) {
                    JSONObject relatedTopic = relatedTopics.getJSONObject(j);
                    String topicId = relatedTopic.getString("topicId");
                    topicId = topicId.substring(topicId.lastIndexOf("#")+1);
                    subjectTopics.put(topicId);
                }
            }//end for

            //System.out.println("horizontalTypeMap: "+horizontalTypeMap)
            [relationSubject: relationSubject, relationSite: relationSite, horizontalTypeMap: horizontalTypeMap, languages: langMap,subjectTopics: subjectTopics, searchDomain: grailsApplication.config.INDEX_SERVICE_URL, pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
    }


    def doAddHierarchicalRelation(){
        def chosenTopicRelations = params.chosenTopicRelations
        def site = params.site
        def subjectId = params.subjectId
        JSONArray chosenTopicRelationsArray = new JSONArray(chosenTopicRelations)
        for(int i = 0; i < chosenTopicRelationsArray.length(); i++){
            JSONObject relationObject = chosenTopicRelationsArray.getJSONObject(i);
            JSONArray topics = relationObject.getJSONArray("topics")
            for(int j = 0; j < topics.length(); j++){
                JSONObject topicObject = topics.getJSONObject(j);
                JSONArray roles = topicObject.getJSONArray("roles");
                for(int k = 0; k < roles.length(); k++){
                    JSONObject role = roles.getJSONObject(k);
                    String roleName = role.getString("roleName")
                    if(roleName.contains("§")){
                        roleName = roleName.substring(0,(roleName.length() -2))
                    }
                    role.put("roleName",roleName);
                }
            }
        }
        JSONObject payload = new JSONObject()
        payload.put("associations",chosenTopicRelationsArray)
        //System.out.println("PAYLOAD: "+payload.toString())
        //def result = adminTopicService.saveOntologyTopicAssociations()

        try {
            def data = null
            if(site.toString().equals("NDLA")){
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.udir.no/ontologi/lkt/#laereplan",payload.toString(),NdlaSite.NDLA)
            }
            else if(site.toString().equals("FYR")){
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.udir.no/ontologi/lkt/#laereplan",payload.toString(),NdlaSite.NYGIV)
            }
            else{
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.udir.no/ontologi/lkt/#laereplan",payload.toString(),NdlaSite.DELING)
            }

            flash.success = "The topic(s) were successfully related to the competence aim(s)."
        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to update the keyword."
        }

        redirect(controller: 'curriculum', action: 'addHierarchicalRelation',params: [relationSubject: subjectId,relationSite: site] )
    }


    def doAddHorisontalRelation(){
        def chosenTopicRelations = params.chosenTopicRelations
        def site = params.site
        def subjectId = params.subjectId
        JSONArray chosenTopicRelationsArray = new JSONArray(chosenTopicRelations)

        for(int i = 0; i < chosenTopicRelationsArray.length(); i++){
            JSONObject relationObject = chosenTopicRelationsArray.getJSONObject(i);
            JSONArray topics = relationObject.getJSONArray("topics")
            for(int j = 0; j < topics.length(); j++){
                JSONObject topicObject = topics.getJSONObject(j);
                JSONArray roles = topicObject.getJSONArray("roles");
                for(int k = 0; k < roles.length(); k++){
                    JSONObject role = roles.getJSONObject(k);
                    String roleName = role.getString("roleName")
                    if(roleName.contains("§")){
                        roleName = roleName.substring(0,(roleName.length() -2))
                    }
                    role.put("roleName",roleName);
                }
            }
        }
        JSONObject payload = new JSONObject()
        payload.put("associations",chosenTopicRelationsArray)

        //def result = adminTopicService.saveOntologyTopicAssociations()

        try {
            def data = null
            if(site.toString().equals("NDLA")){
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.udir.no/ontologi/lkt/#laereplan",payload.toString(),NdlaSite.NDLA)
            }
            else if(site.toString().equals("FYR")){
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.udir.no/ontologi/lkt/#laereplan",payload.toString(),NdlaSite.NYGIV)
            }
            else{
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.udir.no/ontologi/lkt/#laereplan",payload.toString(),NdlaSite.DELING)
            }


            flash.success = "The topic(s) were successfully related to the competence aim(s)."
        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to update the keyword."
        }

        redirect(controller: 'curriculum', action: 'addHorizontalRelation',params: [relationSubject: subjectId,relationSite: site] )
    }

}


