package no.ndla.topics.admin

import grails.plugins.springsecurity.Secured
import org.codehaus.jettison.json.JSONArray
import org.codehaus.jettison.json.JSONException
import org.codehaus.jettison.json.JSONObject

import groovyx.net.http.HTTPBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.Method
import groovyx.net.http.RESTClient

import javax.annotation.PostConstruct

import static groovyx.net.http.Method.GET
import static groovyx.net.http.ContentType.JSON

import groovy.json.*

import net.ontopia.topicmaps.core.*

import no.ndla.topics.service.NdlaServiceException
import no.ndla.topics.service.NdlaSite
import no.ndla.topics.service.model.NdlaTopic

@Secured(['ROLE_ADMIN'])
class IndexSOLRController {

    def adminTopicService // Dependency Injection (DI) by the Spring container.

    @PostConstruct
    void init() {
        if(null == adminTopicService.getTopicService()){
            adminTopicService.setTopicService();
        }
    }

    def index() { }

    def updateIndexAll() {
    }

    def deleteIndexAll() {
    }

    def updateIndexAllTopics(){}

    def deleteIndexAllTopics(){}

    def doUpdateIndexTopics(){
        try {
            def site = params.siteSelect
            String result = adminTopicService.updateTopicIndexAll()
            JSONObject resultjson = new JSONObject(result);
            String success = resultjson.getString("success");
            String resultmsg = resultjson.getString("message");

            if(success.equals("true")) {
                flash.success = resultmsg;
            }
            else{
                flash.error = "An error occurred while trying to index the topics."
            }



        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to index the topics."
        }
        redirect(controller: 'indexSOLR', action: 'updateIndexAllTopics')
    }

    def doUpdateIndex(){
        withForm {
            try {
                def site = params.siteSelect
                String result = adminTopicService.updateKeywordIndexAll(site)
                JSONObject resultjson = new JSONObject(result);
                String success = resultjson.getString("success");
                String resultmsg = resultjson.getString("message");

                if(success.equals("true")) {
                    flash.success = resultmsg;
                }
                else{
                    flash.error = "An error occurred while trying to index the keywords."
                }



            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to index the keywords."
            }
            redirect(controller: 'indexSOLR', action: 'updateIndexAll')
        }.invalidToken {
            // Bad request.
        }
    }

    def doDeleteIndexAllTopics(){
        try {
            String result = adminTopicService.deleteKeywordIndexAll("topics")

            JSONObject resultjson = new JSONObject(result);
            String success = resultjson.getString("success");
            String resultmsg = resultjson.getString("message");

            if(success.equals("true")) {
                flash.success = resultmsg;
            }
            else{
                flash.error = "An error occurred while trying to delete the topics index."
            }



        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to delete the topics index."
        }
        redirect(controller: 'indexSOLR', action: 'deleteIndexAllTopics')
    }

    def doDeleteIndexAll(){
        withForm {
            try {
                String result = adminTopicService.deleteKeywordIndexAll("keywords")

                JSONObject resultjson = new JSONObject(result);
                String success = resultjson.getString("success");
                String resultmsg = resultjson.getString("message");

                if(success.equals("true")) {
                    flash.success = resultmsg;
                }
                else{
                    flash.error = "An error occurred while trying to delete the keywords index."
                }



            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to delete the keywords index."
            }
            redirect(controller: 'indexSOLR', action: 'deleteIndexAll')
        }.invalidToken {
            // Bad request.
        }
    }
}
