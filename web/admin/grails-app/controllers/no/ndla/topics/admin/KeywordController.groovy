package no.ndla.topics.admin

import grails.plugins.springsecurity.Secured
import no.ndla.topics.service.model.NdlaWord
import org.codehaus.jettison.json.JSONArray
import org.codehaus.jettison.json.JSONObject

import groovyx.net.http.HTTPBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.Method
import groovyx.net.http.RESTClient

import javax.annotation.PostConstruct

import static groovyx.net.http.Method.GET
import static groovyx.net.http.ContentType.JSON

import groovy.json.*

import net.ontopia.topicmaps.core.*

import no.ndla.topics.service.NdlaServiceException
import no.ndla.topics.service.NdlaSite
import no.ndla.topics.service.model.NdlaTopic


@Secured(['ROLE_ADMIN','ROLE_KEYWORD_USER'])
class KeywordController{


    def adminTopicService // Dependency Injection (DI) by the Spring container.

    @PostConstruct
    void init() {
        if(null == adminTopicService.getTopicService()){
            adminTopicService.setTopicService();
        }
    }

    def index() {

    }

    def removeKeyword() {
        def langMap = [:]
        def shortMap = [:]
        def Language lang;

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            langMap.put(languageItem.getIdentifier(),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
        }

        [languages: langMap, shortMap: shortMap, searchDomain: grailsApplication.config.INDEX_SERVICE_URL, pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
    }

    def lookupKeyword() {

        def langMap = [:]

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
                langMap.put(languageItem.getIdentifier(),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
        }

        [languages: langMap, searchDomain: grailsApplication.config.INDEX_SERVICE_URL, pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix, resourceHost: grailsApplication.config.RESOURCE_URI_HOST]
    }

    // Form processing.
    def doRemoveKeyword(RemoveKeywordCommand command) {
        withForm {
            def keywordIdentifier = params.keywordIdentifier

            // Validate form.
            if (command.hasErrors()) {
                flash.warning = "The keyword field cannot be empty."
                redirect(controller: 'keyword', action: 'removeKeyword')
                return
            }
            try {
                def keywordIdentifiers = [keywordIdentifier] // Instance of java.util.List.
                def result = adminTopicService.deleteKeywordsByKeywordId(keywordIdentifiers)
                flash.success = "The keyword was successfully removed."

                [keywordIdentifier: keywordIdentifier] // Hash map is made available to the view for rendering purposes.
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to remove the keyword."
            }
            redirect(controller: 'keyword', action: 'removeKeyword')
        }.invalidToken {
           // Bad request.
        }
    }

    def updateKeyword() {
        //def http = new HTTPBuilder('http://api.topic.test.ndla.no/')
        def http = new HTTPBuilder('http://localhost:8080/no.ndla.topics/')
        def wordClasses = []

        ArrayList<NdlaTopic> wordClassArray = adminTopicService.getWordClasses()
        wordClassArray.each{ wc ->
            wordClasses.add(capitalize(wc.getIdentifier()))
        }

        def langMap = [:]
        def shortMap = [:]

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            HashMap<String,String> names = new HashMap<>();
            if(languageItem.psi != "http://psi.topic.ndla.no/#language-neutral" && languageItem.psi != "http://psi.mycurriculum.no/#universal-type"){
                langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
                shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1));
            }

        }

        [wordClasses: wordClasses, langMap: langMap, shortMap: shortMap, searchDomain: grailsApplication.config.INDEX_SERVICE_URL]
    }

    // Form processing.

    def doUpdateKeyword(UpdateKeywordCommand command) {
        withForm {
            def keywordIdentifier = params.keywordIdentifier
            def wordClass = params.wordClass.toLowerCase()
            def visibility = params.visibilityRadios == "visibilityOption1" ? "1" : "0"
            def approvedStatus = params.approvedRadios;
            def processStatus = params.processStateRadios;
            def processMap = [:]
            processMap["0"] = "Not processed"
            processMap["1"] = "Pending"
            processMap["2"] = "Processed"
            if(null == processStatus || "null" == processStatus){
                processStatus = "2";
            }

            def initialProcessState = params.initialProcessState
            def initialApprovalState = params.initialApprovalState;
            def languageSelect = []
            def types = params.typeSelect;

            languageSelect = params.languageSelect;
            //System.out.println("PARAMS: "+params)
            // Validate form.
            if (command.hasErrors()) {
                flash.warning = "The keyword, word class, visibility and approvedStatus fields cannot be empty: "+command.errors.toString()
                redirect(controller: 'keyword', action: 'updateKeyword')
                return
            }

            JSONObject payload = new JSONObject()
            JSONArray outerKeywords = new JSONArray()
            JSONObject keyword = new JSONObject()
            JSONArray innerKeywords = new JSONArray()
            JSONObject keywordStructure = new JSONObject()
            keywordStructure.put('visibility', visibility)
            keywordStructure.put('wordclass', wordClass)

            JSONArray names = new JSONArray()

            params.each { key, val ->
                if(key.contains("#")){
                    JSONObject nametmp = new JSONObject();

                    key = key.substring(key.lastIndexOf("#")+1)
                    if(key != "neutral"){
                        nametmp.put("language","http://psi.oasis-open.org/iso/639/#"+key)
                    }


                    nametmp.put("keyword",val)
                    names.put(nametmp)
                }
            }

            keywordStructure.put('names', names)
            JSONArray jsonTypes = new JSONArray()



            if(types instanceof java.lang.String){
                JSONObject jsonType = new JSONObject()
                jsonType.put("typeId",types)
                jsonTypes.put(jsonType)
            }
            else{
                types.each{ type ->
                    JSONObject jsonType = new JSONObject()
                    jsonType.put("typeId",type)
                    jsonTypes.put(jsonType)
                }
            }


            keywordStructure.put('types', jsonTypes)

            innerKeywords.put(keywordStructure)
            keyword.put("${keywordIdentifier}", innerKeywords)
            outerKeywords.put(keyword)
            payload.put("keywords", outerKeywords)
            //System.out.println("PAYLOAD: "+payload);
            //System.out.println("APPROVED: "+approvedStatus)
            //System.out.println("processStatus: "+processStatus)

            boolean updateSuccess = false;

            try {
                def result = adminTopicService.updateKeywordsByKeywordId(payload.toString())
                updateSuccess = true;
                flash.success = "The keyword was successfully updated."
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to update the keyword."
            }

            String successMessage = ""
            if (updateSuccess) {
                successMessage += "<li>The keyword was successfully updated</li>"
            }


            boolean wasApproved = false;

            if(!initialApprovalState.equals(approvedStatus)) {
                successMessage += "<ul>"
                try{
                    boolean success = false;
                    if(approvedStatus.equals("true")){
                        success = adminTopicService.approveKeyword(keywordIdentifier)
                        if(success) {
                            successMessage  += "<li>The keyword was successfully approved</li>";
                            flash.success = successMessage
                        }
                        else{
                            flash.error = "An error occurred while trying to approve the keyword."
                        }
                        wasApproved = true;
                    }
                    else{
                        success = adminTopicService.disapproveKeyword(keywordIdentifier);
                        if(success) {
                            successMessage  += "<li>The keyword was successfully disapproved</li>";
                            flash.success = successMessage
                        }
                        else{
                            flash.error = "An error occurred while trying to disapprove the keyword."
                        }
                    }
                }
                catch (NdlaServiceException approveError) {
                    flash.error = "An error occurred while trying to set approval state for the keyword."
                }
            }

            if(!wasApproved){
                if(!initialProcessState.equals(processStatus) && processStatus != "2") {
                    try {
                        boolean success = false;
                        success = adminTopicService.setProcessState(keywordIdentifier, processStatus,"keywords")
                        if (success) {
                            successMessage += "<li>The process state of was successfully set to '"+ processMap[processStatus] +"'</li></ul>";
                            flash.success = successMessage
                        } else {
                            flash.error = "An error occurred while trying to set process state for the keyword."
                        }

                    }
                    catch (NdlaServiceException processError) {
                        flash.error = "An error occurred while trying to set process state for the keyword."
                    }

                }
            }


            redirect(controller: 'keyword', action: 'updateKeyword')
        }.invalidToken {
           // Bad request.
        }
    }

    def removeKeywordByKeywordId() {
        NdlaTopic keyword = adminTopicService.getKeywordByKeywordId(params.keywordId)
        def keywordName = ""
        if(null != keyword) {
            ArrayList<NdlaWord> wordNames= keyword.getWordNames();
            for(NdlaWord word : wordNames){
                Iterator<String> it = word.getNames().keySet().iterator();
                while(it.hasNext()){
                    def wordclass = (String) it.next();
                    if(wordclass.equals("http://psi.oasis-open.org/iso/639/#eng")){
                        keywordName = word.getNames().get(wordclass);
                        break;
                    }
                }

            }
            [keywordIdentifier: params.keywordId,keywordName: keywordName]
        }
        else{
            flash.error = "An error occurred while trying to get the keyword by keywordid "+params.keywordId
            redirect(controller: 'keyword', action: 'lookupKeyword')
        }
    }

    def updateKeywordByKeywordId() {
        def wordClasses = []
        def langMap = [:]
        def missingLangs = [:]
        def keywordNames = [:]
        def wordClass = ""

        ArrayList<NdlaTopic> wordClassArray = adminTopicService.getWordClasses()
        wordClassArray.each{ wc ->
            wordClasses.add(wc.names.get("http://psi.oasis-open.org/iso/639/#eng"))
        }

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
        }


        NdlaTopic keyword = adminTopicService.getKeywordByKeywordId(params.keywordId)


        for(NdlaWord nw in keyword.getWordNames()){
            wordClass = capitalize(nw.getWordClass());
            Iterator it = nw.getNames().keySet().iterator();
            while(it.hasNext()) {
                String nwLang = (String) it.next();
                String nwName = nw.getNames().get(nwLang);
                keywordNames.put(nwLang,nwName);
            }
        }

        for( l in langMap){
            if(null == keywordNames.get(l.key)){
               missingLangs.put(l.key,l.value);
            }
        }

        //[keyword: keywordFacade, wordClasses: wordClasses,language: lang, langMap: langMap, shortMap: shortMap]
        [wordclasses: wordClasses,languages: langMap, missinglangs : missingLangs, keyword : keyword, wordClass: wordClass, keywordNames : keywordNames, searchDomain: grailsApplication.config.INDEX_SERVICE_URL]
    }



    def addKeyword() {
        def http = new HTTPBuilder('http://api.topic.test.ndla.no/')
        def wordClasses = []
        def langMap = [:]
        def nameLangs = [:]

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            def psi = languageItem.psi.toString()
            def lang = psi.substring(psi.lastIndexOf("#")+1)
            langMap.put(lang,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
        }

        ArrayList<NdlaTopic> wordClassArray = adminTopicService.getWordClasses()
        wordClassArray.each{ wc ->
            wordClasses.add(capitalize(wc.getIdentifier()))
        }

        langMap.each{langKey, langString ->
            if(!langKey.toString().contains("#eng") && !langKey.toString().contains("#nob")){
                nameLangs.put(langKey, langString)
            }
        }


        [wordClasses: wordClasses, langMap: langMap, nameLangs: nameLangs, searchDomain: grailsApplication.config.INDEX_SERVICE_URL]
    }

    def doAddKeyword(AddKeywordCommand command) {
        withForm {
            def keywordIdentifier = params.keywordIdentifier
            def wordClass = params.wordClass.toLowerCase()
            def visibility = params.visibilityRadios == "visibilityOption1" ? "1" : "0"
            def name_eng = params.name_eng
            def name_nob = params.name_nob
            def types = params.typeSelect;

            //System.out.println("OARAOMS: "+params)
            // Validate form.
            if (command.hasErrors()) {
                flash.warning = "The word class, English title and  Bokmål title fields cannot be empty."
                redirect(controller: 'keyword', action: 'addKeyword')
                return
            }

            JSONObject payload = new JSONObject();
            JSONArray keywords = new JSONArray();
            JSONObject keywordObject = new JSONObject();
            JSONArray keywordSet = new JSONArray();
            JSONObject theKeyword = new JSONObject();

            theKeyword.put("visibility",visibility);
            theKeyword.put("wordclass",wordClass);

            JSONArray names = new JSONArray();
            JSONArray jsonTypes = new JSONArray();

            params.each { key, val ->
                if(key.toString().contains("name_") && val.toString().length() > 0){
                    JSONObject nametmp = new JSONObject();
                    key = key.substring(key.lastIndexOf("_")+1)
                    nametmp.put("language","http://psi.oasis-open.org/iso/639/#"+key)
                    nametmp.put("keyword",val)
                    names.put(nametmp)
                }
            }

            if(types instanceof java.lang.String){
                JSONObject jsonType = new JSONObject()
                jsonType.put("typeId",types)
                jsonTypes.put(jsonType)
            }
            else{
                types.each{ type ->
                    JSONObject jsonType = new JSONObject()
                    jsonType.put("typeId",type)
                    jsonTypes.put(jsonType)
                }
            }

            theKeyword.put("names",names);
            theKeyword.put("types",jsonTypes);
            keywordSet.put(theKeyword)
            keywordObject.put("keywordset",keywordSet);
            keywords.put(keywordObject);
            payload.put("keywords",keywords);

            try {
                def result = adminTopicService.createKeyword(payload.toString())
                if(result.size() > 0){
                    flash.success = "The keyword was successfully created."
                }
                else{
                    flash.error = "An error occurred while trying to create the keyword."
                }

            } catch (NdlaServiceException error) {

            }

            redirect(controller: 'keyword', action: 'addKeyword')
        }.invalidToken {
           // Bad request.
        }
    }

    def removeKeywordNodes() { // Remove nodes by keyword identifier.
        // TopicService getNodesByKeywordId method.
    }

    // Form processing.
    def doRemoveKeywordNodes() {
        withForm {
            def keywordIdentifier = params.keywordIdentifier
            def relatedTopicIdentifiers = params.relatedTopicIdentifiers.split(/,/)

            JSONObject payload = new JSONObject()
            payload.put("topicId", keywordIdentifier)
            payload.put("topicType", "ndla-node")
            payload.put("relationType", "ndlanode-keyword")

            JSONArray jsonRelatedTopics = new JSONArray()
            relatedTopicIdentifiers.each {
                JSONObject jsonRelatedTopic = new JSONObject()
                jsonRelatedTopic.put("relatedTopicId", it)
                jsonRelatedTopic.put("relatedTopicType", "keyword")
                jsonRelatedTopics.put(jsonRelatedTopic)
            }
            payload.put("related_topicids", jsonRelatedTopics)
            //println payload.toString()

            try {
                def result = adminTopicService.removeNodesByKeywordId(payload.toString())
                flash.success = "The associations between keywords and nodes were successfully removed."
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to remove the associations."
            }
            redirect(controller: 'keyword', action: 'removeKeywordNodes')
        }.invalidToken {
           // Bad request.
        }
    }

    def getKeywordDuplicates(){
        def langMap = [:]
        //JSONArray duplicates = adminTopicService.getDuplicateTopics(10,0,"http://psi.topic.ndla.no/#keyword")
        //System.out.println("DUPICTAES: "+duplicates)
        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            langMap.put(languageItem.getIdentifier(),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
        }
        [languages: langMap,searchDomain: grailsApplication.config.INDEX_SERVICE_URL, pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix, resourceHost: grailsApplication.config.RESOURCE_URI_HOST]
    }

    def doMergeKeywordDuplicates(){
        JSONArray payload = new JSONArray();
        def merges = [:]
        def chosens = [:]

        //System.out.println("PARAMS: "+params);
        params.each{key,val ->
            if(key.toString().contains("merge_")){
                merges.put(key,val)
            }

            if(key.toString().contains("chosenTo_")){
                chosens.put(key,val)
            }
        }

        chosens.each{chosenKey, chosenVal ->
            def chosenKeyArr = chosenKey.toString().split("_")
            def chosenIndex = chosenKeyArr[1]
            JSONObject mergeObject = new JSONObject();
            JSONArray mergeArray = new JSONArray();
            def toTopicId = chosenVal.toString().substring(chosenVal.toString().lastIndexOf("#")+1)
            mergeObject.put("toTopic",toTopicId);
            mergeObject.put("type","keyword");
            merges.each{mergerKey,mergerVal ->
                def mergerKeyArr = mergerKey.toString().split("_");
                def mergerIndex = mergerKeyArr[1]
                JSONObject fromObject = new JSONObject()
                if(mergerIndex == chosenIndex && mergerVal != chosenVal){
                    def fromTopicId = mergerVal.toString().substring(mergerVal.toString().lastIndexOf("#")+1)
                    fromObject.put("topicId",fromTopicId)
                    mergeArray.put(fromObject)
                }
            }
            mergeObject.put("fromTopics",mergeArray);
            payload.put(mergeObject);
        }



        try {
            def result = adminTopicService.mergeDuplicateTopics(payload.toString())
            def message = "The following keywords were successfully merged."

            flash.success = message;

        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to merge the keywords."
        }

        redirect(controller: 'keyword', action: 'getKeywordDuplicates')
    }

    def removeNodeKeywords() { // Remove keywords by node identifier.
        // TopicService getKeywordsByNodeId method.
    }

    // Form processing.
    def doRemoveNodeKeywords() {

    }

    def removeKeywordSubjects() { // Remove subjects by keyword identifier.
        // TopicService getSubjectsByKeywordId method.
    }

    // Form processing.
    def doRemoveKeywordSubjects() {

    }

    def removeSubjectKeywords() { // Remove keywords by subject (matter) identifier.
        // TopicService getKeywordsBySubjectMatter method.
    }

    // Form processing.
    def doRemoveSubjectKeywords() {

    }

    def addNodeKeywords() {
        // TopicService HashMap<String, TopicIF> createKeywordsByNodeId(String nodeId, String data, NdlaSite site) throws NdlaServiceException

    }

    // Form processing.
    def doAddNodeKeywords() {

    }

    def addSubjectKeywords() {

    }

    // Form processing.
    def doAddSubjectKeywords() {

    }

    def addHierarchicalRelation() {

    }

    // Form processing.
    def doAddHierarchicalRelation() {

    }

    def addHorizontalRelation() {

    }

    // Form processing.
    def doAddHorizontalRelation() {

    }

    def merge() {

    }

    // Form processing.
    def doMerge() {

    }

    // ***** TEST ACTIONS *****

    def test() {

    }

    def doTest() {
        def labelArray = request.getParameterValues("label")
        print labelArray
        print params.label
    }

    def getLanguageName(lang,list) {
        def languageString = ""
        list.each { key, value ->
            if(key == lang) {
                languageString = value
                return true
            }
        }
        return languageString
    }

    def capitalize(String string) {
        return Character.toUpperCase(string.charAt(0)).toString() + string.substring(1);
    }

}

// ***** MISCELLANEOUS CLASSES *****

// Command classes (for validation purposes).

@grails.validation.Validateable
class RemoveKeywordCommand {
    String keywordIdentifier

    static constraints = {
        keywordIdentifier(blank: false)
    }
}

@grails.validation.Validateable
class UpdateKeywordCommand {
    String keywordIdentifier
    String wordClass
    String approvedRadios
    String visibilityRadios
    static constraints = {
        keywordIdentifier(blank: false)
        wordClass(blank: false)
        approvedRadios(blank: false)
        visibilityRadios(blank: false)
    }
}

@grails.validation.Validateable
class AddKeywordCommand {
    String name_eng
    String name_nob
    String wordClass

    static constraints = {
        name_eng(blank: false)
        name_nob(blank: false)
        wordClass(blank: false)
    }
}

// Auxiliary facade model classes.

class KeywordNameFacade {
    String keyword
    String language
    String wordclass
}

class KeywordTypeFacade {
    String typeId
    ArrayList<KeywordNameFacade> names = new ArrayList<KeywordNameFacade>()
}
class KeywordFacade {
    String visibility
    String approvedStatus
    String topicId
    ArrayList<KeywordNameFacade> names = new ArrayList<KeywordNameFacade>()
    ArrayList<KeywordTypeFacade> types = new ArrayList<KeywordTypeFacade>()
}

