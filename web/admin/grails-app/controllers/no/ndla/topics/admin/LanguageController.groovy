package no.ndla.topics.admin

import grails.plugins.springsecurity.Secured
import org.codehaus.jettison.json.JSONArray
import org.codehaus.jettison.json.JSONException
import org.codehaus.jettison.json.JSONObject

import groovyx.net.http.HTTPBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.Method
import groovyx.net.http.RESTClient

import javax.annotation.PostConstruct

import static groovyx.net.http.Method.GET
import static groovyx.net.http.ContentType.JSON


import net.ontopia.topicmaps.core.*

import no.ndla.topics.service.NdlaServiceException
import no.ndla.topics.service.NdlaSite
import no.ndla.topics.service.model.NdlaTopic


@Secured(['ROLE_ADMIN'])
class LanguageController {

    def adminTopicService // Dependency Injection (DI) by the Spring container.

    @PostConstruct
    void init() {
        if(null == adminTopicService.getTopicService()){
            adminTopicService.setTopicService();
        }
    }

    def index() { }

    def updateIndexAll() {
    }

    def deleteIndexAll() {
    }

    def getLanguages(){
        def languageMap = [:]

        ArrayList<NdlaTopic> languages = adminTopicService.getLanguages()

        for(NdlaTopic language: languages) {
            def names = language.getNames();
            def languageName = ""
            names.each{lang, name ->
                if(lang == "http://psi.oasis-open.org/iso/639/#nob"){
                    languageName = name;
                }
                else if(lang == "http://psi.oasis-open.org/iso/639/#eng" && languageName == ""){
                    languageName = name;
                }
                else if(lang == "http://psi.topic.ndla.no/#language-neutral" && languageName == ""){
                    languageName = name;
                }
            }

            languageMap.put(language.getIdentifier(),languageName)
        }

         [languages: languageMap, removeLanguage: params.removeLanguage]
    }

    def doSetLanguage(){
        def languageId = params.language
        withForm {
            try {

                if(params.removeLanguage.equals("true")) {
                    redirect(controller: 'language', action: 'removeLanguage', params: [languageId: languageId])
                }
                else if(params.removeLanguage.equals("false")) {
                    redirect(controller: 'language', action: 'editLanguage', params: [languageId: languageId])
                }
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to index the keywords."
            }


        }.invalidToken {
            // Bad request.
        }
    }

    def doUpdateLanguage(UpdateLanguageCommand command){
        def languageId = ""
        withForm {
            languageId = params.languageId
            JSONObject payload = new JSONObject()
            JSONArray names = new JSONArray()

            params.each { key, val ->
                if(key.toString().contains("namestring_")) {
                    def keyArr = key.toString().split("_")
                    def keyId = keyArr[1]
                    JSONObject nametmp = new JSONObject();
                    nametmp.put("language-id","http://psi.oasis-open.org/iso/639/#"+keyId)
                    nametmp.put("name",val)
                    names.put(nametmp)
                }
                else if(key.toString().contains("languageId")) {
                    payload.put("languageId",val)
                }
            }
            payload.put("names",names)
            languageId = languageId.toString().substring(languageId.toString().lastIndexOf("#")+1)
            try {
                def result = adminTopicService.updateLanguage(payload.toString())
                flash.success = "The language was successfully updated."
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to update the language."
            }

            redirect(controller: 'language', action: 'editLanguage', params: [languageId: languageId])
        }.invalidToken {
            // Bad request.
        }
    }

    def addLanguage(){
        def allLangMap = [:]
        def langMap = [:]
        def shortMap = [:]


        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            allLangMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1));
        }


        [langMap: langMap,allLangMap: allLangMap, shortMap: shortMap]
    }


    def doAddLanguage(AddLanguageCommand command){

        withForm {

            // Validate form.
            if (command.hasErrors()) {
                flash.warning = "The ISO-639-3 PSI  field cannot be empty."
                redirect(controller: 'language', action: 'addLanguage')
                return
            }

            JSONObject payload = new JSONObject()
            JSONArray names = new JSONArray()

            params.each { key, val ->
                if(key.toString().contains("namestring_")) {
                    def keyArr = key.toString().split("_")
                    def keyId = keyArr[1]
                    JSONObject nametmp = new JSONObject();
                    nametmp.put("language-id","http://psi.oasis-open.org/iso/639/#"+keyId)
                    nametmp.put("name",val)
                    names.put(nametmp)
                }
                else if(key.toString().contains("languageId")) {
                    payload.put("languageId",val)
                }

            }
            payload.put("names",names)

            try {
                def result = adminTopicService.addLanguage(payload.toString())
                flash.success = "The language was successfully added."
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to adding the language."
            }
            redirect(controller: 'language', action: 'addLanguage')
        }.invalidToken {
            // Bad request.
        }
    }

    def editLanguage(){
        def allLangMap = [:]
        def languageId = params.languageId
        NdlaTopic language = adminTopicService.getLanguage(languageId)
        def names = language.getNames();
        def languageName = language.names.get("http://psi.oasis-open.org/iso/639/#eng")
        def languageTopicId = language.getPsi();
        def langMap = [:]
        def shortMap = [:]

        def missingLangs = [:]

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            allLangMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1));
        }

        names.each{langString, name ->
            if(langString != "http://psi.topic.ndla.no/#language-neutral" && langString != "http://psi.mycurriculum.no/#universal-type"){
                langMap.put(langString,name)
            }
        }


        for( l in allLangMap){
            if(null == langMap.get(l.key)){
                missingLangs.put(l.key,l.value);
            }
        }

        [languageTopicId: languageTopicId,languageName:languageName, langMap: langMap,allLangMap: allLangMap, shortMap: shortMap,missingLangs:missingLangs]
    }

    def getLanguageName(lang,list) {
        def languageString = ""
        list.each { key, value ->
            if(key == lang) {
                languageString = value
                return true
            }
        }
        return languageString
    }

    def removeLanguage(){
        def languageMap = [:]
        def languageId = params.languageId
        NdlaTopic language = adminTopicService.getLanguage(languageId)


        def names = language.getNames();
        def languageName = ""
        names.each{lang, name ->
            if(lang == "http://psi.oasis-open.org/iso/639/#nob"){
                languageName = name;
            }
            else if(lang == "http://psi.oasis-open.org/iso/639/#eng" && languageName == ""){
                languageName = name;
            }
            else if(lang == "http://psi.topic.ndla.no/#language-neutral" && languageName == ""){
                languageName = name;
            }
        }
        languageMap.put(language.getIdentifier(),languageName)
        [languages: languageMap,languageId : languageId]
    }

    def doRemoveLanguage(){
        def languageId = ""
        withForm {
            languageId = params.languageId
            TopicIF result = null
            try {
                String payload = '["http://psi.oasis-open.org/iso/639/#'+languageId+'"]'
                result = adminTopicService.removeLanguage(payload)
                if(null != result){
                    flash.success = "The language was successfully removed."
                }
                else{
                    flash.error = "An error occurred while trying to remove the language."
                }

            } catch (NdlaServiceException error) {

            }
            redirect(controller: 'language', action: 'getLanguages',params: ['removeLanguage' : 'true'])
        }.invalidToken {
            // Bad request.
        }
    }
}

// Command classes (for validation purposes).

@grails.validation.Validateable
class UpdateLanguageCommand {
    String languagePsi
    String languageId


    static constraints = {
        languagePsi(blank: false)
    }
}

class AddLanguageCommand {
    String languageId


    static constraints = {
        languageId(blank: false)
    }
}
