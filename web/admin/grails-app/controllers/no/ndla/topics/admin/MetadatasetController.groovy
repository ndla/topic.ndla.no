package no.ndla.topics.admin

import grails.plugins.springsecurity.Secured

@Secured(['ROLE_ADMIN'])
class MetadatasetController {

    def index() {
    	flash.warning = "The metadata sets-related functionality has not been implemented, yet."
        redirect(controller: 'keyword', action: 'lookupKeyword')
    }
}
