package no.ndla.topics.admin

import com.sun.tools.javac.comp.Todo
import grails.plugins.springsecurity.Secured
import groovy.json.JsonSlurper
import org.codehaus.jettison.json.JSONArray
import org.codehaus.jettison.json.JSONObject

import groovyx.net.http.HTTPBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.Method
import groovyx.net.http.RESTClient

import javax.annotation.PostConstruct

import static groovyx.net.http.Method.GET
import static groovyx.net.http.ContentType.JSON


import net.ontopia.topicmaps.core.*

import no.ndla.topics.service.NdlaServiceException
import no.ndla.topics.service.NdlaSite
import no.ndla.topics.service.model.NdlaTopic


@Secured(['ROLE_ADMIN','ROLE_ONTOLOGY_USER','ROLE_CURRICULAONTOLOGY_USER'])
class OntologyController {

    def adminTopicService // Dependency Injection (DI) by the Spring container.

    @PostConstruct
    void init() {
        if(null == adminTopicService.getTopicService()){
            adminTopicService.setTopicService();
        }
    }

    def index() { }

    def updateIndexAll() {
    }

    def deleteIndexAll() {
    }

    def removeSubject(){
        def subjectId = params.subjectId
        def Subject subj;
        NdlaTopic subjectTopic = null
        switch (params.sites) {
            case "NDLA":
                subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NDLA)
                break;
            case "DELING":
                subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.DELING)
                break;
            case "FYR":
                subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NYGIV)
                break;
        }

        if(null != subjectTopic  && null != params.sites){
            def name = subjectTopic.getNames().get("http://psi.oasis-open.org/iso/639/#eng");
            [subjectName: name, subjectId: subjectId]
        }
        else{
            flash.error = "An error occurred while trying to fetch the subject for the site "+params.sites+". It was probably not configured for this site"
            redirect(controller: 'ontology', action: 'getSubjects')
        }


    }

    def doRemoveSubject(){
        withForm {
            def subjectId = params.subjectId
            JSONArray payload = new JSONArray();
            payload.put("http://psi.topic.ndla.no/#"+subjectId)
            try {
                def data = adminTopicService.removeSubject(payload.toString())
                flash.success = "The subject was successfully deleted."
                redirect(controller: 'ontology', action: 'getSubjects',params: [removeSubject: 'true'])
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to delete the subject."
                redirect(controller: 'ontology', action: 'getSubjects',params: [removeSubject: 'true'])
            }
        }
    }

    def addSubject() {

        def langMap = [:]

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            if(!languageItem.psi.equals("http://psi.topic.ndla.no/#language-neutral") && !languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#eng")) {
                langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }
        }
        [langMap: langMap]
    }

    def doAddSubject(AddSubjectCommand command){

        withForm {
            def subjectId = params.subjectId
            def UDIRsubjectId = params.UDIRsubjectId
            def englishTitle = params.englishTitle

            def names = [:]
            params.each { key, val ->
                if(key.contains("iso/639")){
                    if(key != "http://psi.oasis-open.org/iso/639/#language-neutral"){
                        names.put(key,val)
                    }
                }
            }

            def sites = params.list("sites")
            def psis = params.list("psis")

            // Validate form.
            if (command.hasErrors()) {
                flash.warning = "The subjectId, UDIRSubjectID, english name, psis and site fields cannot be empty: "//+command.errors.toString()
                redirect(controller: 'ontology', action: 'addSubject')
                return
            }

            JSONObject payload = new JSONObject()
            JSONArray jsonNames = new JSONArray()
            JSONArray jsonSites = new JSONArray()
            JSONArray jsonPsis = new JSONArray()

            JSONObject englishTitleJSON = new JSONObject();
            englishTitleJSON.put("language-id","http://psi.oasis-open.org/iso/639/#eng")
            englishTitleJSON.put("name",englishTitle)
            jsonNames.put(englishTitleJSON)

            names.each{ language, name ->
                JSONObject temporaryName = new JSONObject();
                temporaryName.put("language-id",language)
                temporaryName.put("name",name)
                jsonNames.put(temporaryName)
            }

            payload.put('udir-psi', "http://psi.topic.ndla.no/#subject_"+UDIRsubjectId)
            payload.put('subjectId',    subjectId)


            sites.each{ site ->
                jsonSites.put(site)
            }

            psis.each{ psi ->
                jsonPsis.put(psi)
            }




            payload.put("names",jsonNames)
            payload.put("psis",jsonPsis);
            payload.put("originating-sites",jsonSites);


            try {
                def data = adminTopicService.createSubject(payload.toString())
                flash.success = "The subject was successfully created."
                redirect(controller: 'ontology', action: 'addSubject')
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to create the subject."
                redirect(controller: 'ontology', action: 'addSubject')
            }
        }

    }

    def editSubject(){
        def subjectId = params.subjectId
        def Subject subj;

        def missingLangs = [:]
        def langMap = [:]
        NdlaTopic subjectTopic = null

        if(null != params.sites){
            switch (params.sites) {
                case "NDLA":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NDLA)
                    break;
                case "DELING":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.DELING)
                    break;
                case "FYR":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NYGIV)
                    break;
            }
        }


        if(null != subjectTopic && null != params.sites) {
            def names = subjectTopic.getNames()

            ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
            languageArray.each{ languageItem ->
                if(!languageItem.psi.equals("http://psi.topic.ndla.no/#language-neutral")) {
                    langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
                    if(!names.containsKey(languageItem.psi)) {
                        missingLangs.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"));
                    }
                }

            }
            subj =  new Subject(subjectId,subjectTopic.getUdirPsi(),subjectTopic.getPsis(),names,subjectTopic.getOriginatingSites())

            [subject: subj, missinglangs : missingLangs, langMap: langMap, sites: params.sites]
        }
        else{
            flash.message = "Chose the site you want to edit subjects for"
            redirect(controller: 'ontology', action: 'getSubjects', params: [removeSubject: "false"])
        }


    }


        def getSubjects() {
        def subjects = [:]


        if(!null == params.sites){
            ArrayList<NdlaTopic> subjectArray = adminTopicService.getSubjects(50,0,NdlaSite.NDLA);
            subjectArray.each{ subject ->
                subjects.put(subject.getIdentifier(),subject.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }
        }

        if (null == params.removeSubject && null == params.addOntologyTopics){
            [subjects: subjects, removeSubject: "false",addOntologyTopics: "false", sites: params.sites,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
        else if(null != params.removeSubject && null == params.addOntologyTopics){
            [subjects: subjects, removeSubject: params.removeSubject ,addOntologyTopics: "false", sites: params.sites,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }else if(null == params.removeSubject && null != params.addOntologyTopics){
            [subjects: subjects, removeSubject: "false",addOntologyTopics: "true", sites: params.sites,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
    }

    def getSubjectName(lang,list) {
        def languageString = ""
        list.each { key, value ->
            if(key == lang) {
                languageString = value
                return true
            }
        }
        return languageString
    }

    def doSetSubject(SetSubjectTopicCommand command){
        def subject = ""
        //withForm {

            subject = params.subject


            if (command.hasErrors()) {
                flash.warning = "You have to choose a subject."
                redirect(controller: 'ontology', action: 'getSubjects')
                return
            }

            if(params.removeSubject.equals("true") && params.addOntologyTopics.equals("false")) {
                redirect(controller: 'ontology', action: 'removeSubject', params: [subjectId: subject, sites: params.sites])
            }
            else if(params.removeSubject.equals("false") && params.addOntologyTopics.equals("false")) {
                redirect(controller: 'ontology', action: 'editSubject', params: [subjectId: subject, sites: params.sites])
            }
            else if(params.removeSubject.equals("false") && params.addOntologyTopics.equals("true")){
                redirect(controller: 'ontology', action: 'addSubjectTopics', params: [subjectId: subject, sites: params.sites])
            }



        //}.invalidToken {
            // Bad request.
        //}
    }

    def doUpdateSubject() {
        def subjectId = ""
        withForm {

            JSONObject payload = new JSONObject()
            JSONArray names = new JSONArray()
            JSONArray sites = new JSONArray()
            JSONArray psis = new JSONArray()

            payload.put('udir-psi', params.UDIRPsi)
            subjectId = params.subjectId
            def subjectIdFinal = subjectId.substring(subjectId.indexOf("_")+1);
            payload.put('subjectId', subjectIdFinal)

            def subjectSites = params.list("subjectSites")
            def subjectPsis = params.list("subjectPsis")


            subjectSites.each{ site ->
                sites.put(site)
            }

            subjectPsis.each{ psi ->
                psis.put(psi)
            }

            params.each { key, val ->
                JSONObject nametmp = new JSONObject();
                if(key.contains("#")){
                    key = key.substring(key.lastIndexOf("#")+1)
                    if(key != "language-neutral"){
                        nametmp.put("language-id","http://psi.oasis-open.org/iso/639/#"+key)
                        nametmp.put("name",val)
                    }

                    names.put(nametmp)
                }
                else if(key.contains("addedSiteurl")) {
                    sites.put(val)
                }
                else if(key.contains("addedSubjectPsi")) {
                    psis.put(val)
                }

            }


            payload.put("names",names)
            payload.put("psis",psis);
            payload.put("originating-sites",sites);


            try {
                def data = adminTopicService.updateSubject(payload.toString());
                flash.success = "The subject was successfully updated."
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to update the keyword."
            }
        }
        redirect(controller: 'ontology', action: 'getSubjects')
    }

    def doUpdateIndex(){
        withForm {
            try {
                def site = params.siteSelect
                String result = adminTopicService.updateKeywordIndexAll(site)
                JSONObject resultjson = new JSONObject(result);
                String success = resultjson.getString("success");
                String resultmsg = resultjson.getString("message");

                if(success.equals("true")) {
                    flash.success = resultmsg;
                }
                else{
                    flash.error = "An error occurred while trying to index the keywords."
                }



            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to index the keywords."
            }
            redirect(controller: 'Ontology', action: 'updateIndexAll')
        }.invalidToken {
            // Bad request.
        }
    }

    def doDeleteIndexAll(){
        withForm {
            try {
                String result = adminTopicService.deleteKeywordIndexAll()

                JSONObject resultjson = new JSONObject(result);
                String success = resultjson.getString("success");
                String resultmsg = resultjson.getString("message");

                if(success.equals("true")) {
                    flash.success = resultmsg;
                }
                else{
                    flash.error = "An error occurred while trying to index the keywords."
                }



            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to index the keywords."
            }
            redirect(controller: 'indexSOLR', action: 'deleteIndexAll')
        }.invalidToken {
            // Bad request.
        }
    }

    /*** ONTOLOGY TOPIC EDITING ***/
    def editSubjectTopics(){
        def subjectId = params.subjectId
        def sites = params.sites
        NdlaTopic subjectTopic = null
        def Subject subj

        if(null != params.sites){
            switch (params.sites) {
                case "NDLA":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NDLA)
                    break;
                case "DELING":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.DELING)
                    break;
                case "FYR":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NYGIV)
                    break;
            }
        }

        if(null != subjectTopic && null != params.sites) {
            def names = subjectTopic.getNames()
            subj =  new Subject(subjectTopic.getIdentifier(),subjectTopic.getUdirPsi(),subjectTopic.getPsis(),names,subjectTopic.getOriginatingSites())

            def langMap = [:]
            def shortMap = [:]
            def Language lang;

            ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
            languageArray.each{ languageItem ->
                langMap.put(languageItem.getIdentifier(),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }

            JSONObject subjectTopics = null;
            switch (params.sites) {
                case "NDLA":
                    subjectTopics = adminTopicService.getOntologyTopics(subjectId,NdlaSite.NDLA)
                    break;
                case "DELING":
                    subjectTopics = adminTopicService.getOntologyTopics(subjectId,NdlaSite.DELING)
                    break;
                case "FYR":
                    subjectTopics = adminTopicService.getOntologyTopics(subjectId,NdlaSite.NYGIV)
                    break;
            }

            def slurper = new JsonSlurper()
            def subjectTopicsObject = slurper.parseText(subjectTopics.toString())
            def subjectTopicArray = []

            subjectTopicsObject.relations.each {rel ->
                def map = [:]
                map.put("topicId",rel.topicId)
                def name = "";
                rel.topicNames.each{topicName ->
                    if(topicName["http://psi.oasis-open.org/iso/639/#nob"] != null){
                        name = topicName["http://psi.oasis-open.org/iso/639/#nob"]
                    }
                    else if(topicName["http://psi.oasis-open.org/iso/639/#eng"] != null && name == ""){
                        name = topicName["http://psi.oasis-open.org/iso/639/#eng"]
                    }
                    else if(topicName["http://psi.topic.ndla.no/#language-neutral"] != null && name == ""){
                        map.put("name",topicName["http://psi.topic.ndla.no/#language-neutral"])
                    }

                }
                map.put("name",name)
                rel.associationTypeNames.each{associationTypeName ->
                    if(associationTypeName["language"] == "http://psi.oasis-open.org/iso/639/#nob" && associationTypeName["role"]  == "http://psi.topic.ndla.no/#ndla-topic"){
                        map.put("associationTypeName",associationTypeName["name"])
                    }
                    else if(associationTypeName["language"] == "http://psi.oasis-open.org/iso/639/#eng" && associationTypeName["role"]  == "http://psi.topic.ndla.no/#ndla-topic"){
                        map.put("associationTypeName",associationTypeName["name"])
                    }
                    else if(associationTypeName["language"] == "http://psi.topic.ndla.no/#language-neutral" && associationTypeName["role"]  == "http://psi.topic.ndla.no/#ndla-topic"){
                        map.put("associationTypeName",associationTypeName["name"])
                    }

                }
                subjectTopicArray.add(map)
            }


            [subject: subj, subjectId: subjectId, site: sites, languages: langMap,subjectTopics: subjectTopics.relations,subjectTopicsObject: subjectTopicArray, searchDomain: grailsApplication.config.INDEX_SERVICE_URL, pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
        else{
            flash.error = "An error occurred while trying to fetch the subject for the site "+params.sites+". It was probably not configured for this site"
            redirect(controller: 'ontology', action: 'editSubjectTopics', params: [subjectId: subjectId, sites: sites])
        }
    }

    def editSubjectTopic() {
        def topicId = params.topicId
        def subjectId = params.subjectId
        def site = params.site

        NdlaTopic topic = adminTopicService.getTopicByTopicId(topicId)
        def names = topic.getNames()
        def descriptions = topic.getDescriptions()
        def images = topic.getImages()
        def approvedState = topic.getApproved()

        def langMap = [:]
        def shortMap = [:]
        def missingLangs = [:]
        def topicNames = [:]

        def wordClassMap = [:]
        def wordClass = ""

        def nameMap = [:]

        names.each{ namePsi, nameString ->
            if(namePsi.toString().contains("iso")){
                nameMap.put(namePsi,nameString)
            }
            else{
                if(!namePsi.toString().contains("language-neutral")){
                    wordClass = namePsi.toString().substring(namePsi.toString().lastIndexOf("#")+1)
                }

            }
        }

        ArrayList<NdlaTopic> wordClasses = adminTopicService.getWordClasses();
        wordClasses.each{wordClassItem ->
            def psi = wordClassItem.getPsi()
            def wordClassPsi = psi.substring(psi.lastIndexOf("#")+1)
            def name = wordClassItem.getNames().get("http://psi.oasis-open.org/iso/639/#eng")
            wordClassMap.put(wordClassPsi,name)
        }

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1))
        }



        for( l in langMap){
            if(null == nameMap.get(l.key)){
                missingLangs.put(l.key,l.value);
            }
        }

        [topicId: topicId, subjectId: subjectId,site: site, languages: langMap, shortMap: shortMap, missinglangs : missingLangs, names : nameMap,descriptions: descriptions, images: images, approvedState: approvedState,wordClassMap : wordClassMap,wordClass : wordClass]
    }

    def doUpdateSubjectTopic(UpdateSubjectTopicCommand command){
        def topicId = params.topicId
        def subjectId = params.subjectId
        def site = params.site

        def approvedStatus = params.approvedRadios;
        def initialApprovalState = params.initialApprovalState;
        def wordClass = params.topicTitleWordClass


        //validation
        if (command.hasErrors()) {
            flash.warning = "The topicId and wordclass  fields cannot be empty."
            redirect(controller: 'ontology', action: 'editSubjectTopics',params: [subjectId: subjectId, sites: site])
            return
        }

        def names = [:]
        def descriptions = [:]
        def images = [:]


        params.each {key , val ->
            if(key.toString().contains("name_")){
                def lang = key.toString().substring(key.toString().lastIndexOf("_")+1);
                if(!lang.empty){
                    names.put("http://psi.oasis-open.org/iso/639/#"+key.toString().substring(key.toString().lastIndexOf("_")+1),val)
                }
            }
            else if(key.toString().contains("description_")){
                descriptions.put("http://psi.oasis-open.org/iso/639/#"+key.toString().substring(key.toString().lastIndexOf("_")+1),val)
            }
            else if(key.toString().contains("image_")){
                images.put("http://psi.oasis-open.org/iso/639/#"+key.toString().substring(key.toString().lastIndexOf("_")+1),val)
            }
        }


        JSONObject payload = new JSONObject();
        JSONArray topicsArray = new JSONArray();

        JSONObject topicWrapper = new JSONObject();
        JSONArray topicDataArray = new JSONArray();

        JSONObject thisTopic = new JSONObject();
        thisTopic.put("approved",approvedStatus)

        thisTopic.put("wordClass",wordClass)

        JSONArray topicJSONNames = new JSONArray();
        JSONArray topicJSONDescriptions = new JSONArray();
        JSONArray topicJSONImages = new JSONArray();
        JSONArray topicJSONTypes = new JSONArray();

        names.each {lang,name ->
            JSONObject nameTmp = new JSONObject()
            nameTmp.put("name",name)
            nameTmp.put("language",lang)
            topicJSONNames.put(nameTmp)
        }

        thisTopic.put("names",topicJSONNames)

        descriptions.each {lang,description ->
            JSONObject descTmp = new JSONObject()

            descTmp.put("language",lang)
            descTmp.put("content",description)

            topicJSONDescriptions.put(descTmp);
        }

        thisTopic.put("descriptions",topicJSONDescriptions)

        images.each {lang,imageURL ->
            JSONObject imgTmp = new JSONObject()

            imgTmp.put("url",imageURL)
            imgTmp.put("language",lang)

            topicJSONImages.put(imgTmp)
        }

        thisTopic.put("images",topicJSONImages)
        thisTopic.put("types",topicJSONTypes)

        topicDataArray.put(thisTopic)

        topicWrapper.put(topicId,topicDataArray);

        topicsArray.put(topicWrapper);
        payload.put("topics",topicsArray);

        boolean updateSuccess = false;
        try{
            if(payload.length() > 0) {
                def keymap = adminTopicService.updateTopicsByTopicId(payload.toString())
                if(keymap.size() > 0) {
                    updateSuccess = true;
                }
            }
        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to update the keyword."
        }


        String successMessage = "<ul>"
        if(updateSuccess) {
            successMessage  += "<li>The topic "+topicId+" was successfully updated</li>"
        }

        if(!initialApprovalState.equals(approvedStatus)) {
            try{
                boolean success = false;
                if(approvedStatus.equals("true")){
                    success = adminTopicService.approveTopic(topicId)
                    if(success) {
                        successMessage  += "<li>The topic "+topicId+" was successfully approved</li></ul>";
                        flash.success = successMessage
                    }
                    else{
                        flash.error = "An error occurred while trying to approve the topic "+topicId
                    }
                }
                else{
                    success = adminTopicService.disapproveTopic(topicId)
                    if(success) {
                        successMessage  += "<li>The topic "+topicId+" was successfully disapproved</li></ul>";
                        flash.success = successMessage
                    }
                    else{
                        flash.error = "An error occurred while trying to disapprove the topic "+topicId
                    }
                }
            }
            catch (NdlaServiceException approveError) {
                flash.error = "An error occurred while trying to set approval state for the keyword."
            }
        }
        else{
            if(updateSuccess) {
                successMessage += "</ul>"
                flash.success = successMessage
            }

        }

        redirect(controller: 'ontology', action: 'editSubjectTopics', params: [subjectId: subjectId, sites: site])
    }



    def addSubjectTopics(){
        def subjectId = params.subjectId
        def sites = params.sites
        NdlaTopic subjectTopic = null
        def Subject subj

        def shortMap = [:]
        def langMap = [:]
        def missingLangs = [:]
        def wordClassMap = [:]

        ArrayList<NdlaTopic> wordClasses = adminTopicService.getWordClasses();
        wordClasses.each{wordClassItem ->
            def psi = wordClassItem.getPsi()
            def wordClassPsi = psi.substring(psi.lastIndexOf("#")+1)
            def name = wordClassItem.getNames().get("http://psi.oasis-open.org/iso/639/#eng")
            wordClassMap.put(wordClassPsi,name)
        }

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            if(!languageItem.psi.equals("http://psi.topic.ndla.no/#language-neutral") && !languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#eng")) {
                if(!languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#nob")) {
                    missingLangs.put(languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
                }
            }
            langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1))
        }

        if(null != params.sites){
            switch (params.sites) {
                case "NDLA":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NDLA)
                    break;
                case "DELING":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.DELING)
                    break;
                case "FYR":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NYGIV)
                    break;
            }
        }

        if(null != subjectTopic && null != params.sites) {
            def names = subjectTopic.getNames()
            subj =  new Subject(subjectTopic.getIdentifier(),subjectTopic.getUdirPsi(),subjectTopic.getPsis(),names,subjectTopic.getOriginatingSites())
            [subject: subj, subjectId: subjectId, site: sites, langMap: langMap, shortMap: shortMap,missinglangs: missingLangs,wordClassMap : wordClassMap, searchDomain: grailsApplication.config.INDEX_SERVICE_URL]
        }
        else{
            flash.error = "An error occurred while trying to fetch the subject for the site "+params.sites+". It was probably not configured for this site"
            redirect(controller: 'ontology', action: 'addSubjectTopics', params: [subjectId: subjectId, sites: sites])
        }
    }

    def doAddSubjectTopics(){
        withForm {
            def addedTopics = params.list("hiddenTopics")
            def site = params.site
            def subjectId = params.subjectId

            def existingKeywords = []
            def existingTopics = []
            def newTopicNames = []
            def newTopicDescriptions = []
            def newTopicImages = []
            def isoBaseUrl = "http://psi.oasis-open.org/iso/639/#";

            addedTopics.each { topic ->
                if (topic.toString().contains("keyword-")) {
                    existingKeywords.add(topic.toString())
                } else if (topic.toString().contains("topic-")) {
                    existingTopics.add(topic.toString())
                }
                else {
                    def tempNameData = [:]
                    def tempDescriptionData = [:]
                    def tempImageData = [:]
                    def topicData = topic.toString().split("§")
                    topicData.each { data ->
                        def nameData = data.toString().split(";")

                        if(nameData[0].contains("description_")){
                            def langId = nameData[0].split("_")
                            tempDescriptionData.put(isoBaseUrl+langId[1], nameData[1])
                        }
                        else if(nameData[0].contains("image_")){
                            def langId = nameData[0].split("_")
                            tempImageData.put(isoBaseUrl+langId[1], nameData[1])
                        }
                        else if(nameData[0].contains("wordClass")){
                            def wordClassTopicId = nameData[1]
                            tempNameData.put("wordClass", wordClassTopicId)
                        }
                        else{
                            tempNameData.put(isoBaseUrl+nameData[0], nameData[1])
                        }

                    }

                    newTopicNames.add(tempNameData)
                    if(tempDescriptionData.size() > 0) {
                        newTopicDescriptions.add(tempDescriptionData)
                    }

                    if(tempImageData.size() > 0) {
                        newTopicImages.add(tempImageData)
                    }
                }
            }


            def newTopics = []

            if(existingKeywords.size() > 0) {
                existingKeywords.each {keywordId ->
                    JSONArray topicJSONNames = new JSONArray();
                    JSONArray topicJSONDescriptions = new JSONArray();
                    JSONArray topicJSONImages = new JSONArray();
                    JSONArray topicJSONTypes = new JSONArray();
                    JSONObject newTopic = new JSONObject();

                    def keyword = adminTopicService.getKeywordByKeywordId(keywordId);
                    def names = keyword.getWordNames();

                    names.each { nameObj ->
                        def nameObjNames = nameObj.getNames()
                        nameObjNames.each { lang, val ->
                            JSONObject nameTmp = new JSONObject()
                            nameTmp.put("language", lang)
                            nameTmp.put("name", val)
                            topicJSONNames.put(nameTmp)
                        }
                    }
                    newTopic.put("fromKeyword",keywordId);
                    newTopic.put("names",topicJSONNames);
                    newTopic.put("descriptions",topicJSONDescriptions);
                    newTopic.put("images",topicJSONImages);
                    newTopic.put("types",topicJSONTypes)

                    newTopics.add(newTopic)
                }
            }


            if(newTopicNames.size() > 0) {
                def counter = 0;
                def WC = ""
                newTopicNames.each { nameObj ->
                    JSONArray topicJSONNames = new JSONArray();
                    JSONArray topicJSONDescriptions = new JSONArray();
                    JSONArray topicJSONImages = new JSONArray();
                    JSONArray topicJSONTypes = new JSONArray();
                    JSONObject newTopic = new JSONObject();

                    nameObj.each { lang, val ->
                        if(lang == "wordClass"){
                            WC = val;
                        }
                        else{
                            JSONObject nameTmp = new JSONObject()
                            nameTmp.put("language",lang)
                            nameTmp.put("name",val)
                            topicJSONNames.put(nameTmp)
                        }

                    }

                    newTopic.put("names",topicJSONNames);
                    newTopic.put("wordClass",WC);

                    if(newTopicDescriptions.size() > 0) {
                        def descObject = newTopicDescriptions[counter]
                        if(null != descObject) {
                            descObject.each { descLang, descVal ->
                                JSONObject descTmp = new JSONObject()
                                descTmp.put("language",descLang)
                                descTmp.put("content",descVal)
                                topicJSONDescriptions.put(descTmp)
                            }
                        }
                    }//end if descriptions

                    if(newTopicImages.size() > 0) {
                        def imgObject = newTopicImages[counter]
                        if(null != imgObject) {
                            imgObject.each { imgLang, imgVal ->
                                JSONObject imgTmp = new JSONObject()
                                imgTmp.put("language",imgLang)
                                imgTmp.put("url",imgVal)
                                topicJSONImages.put(imgTmp)
                            }
                        }
                    }//end if descriptions

                    newTopic.put("fromKeyword","newTopic");
                    newTopic.put("descriptions",topicJSONDescriptions);
                    newTopic.put("images",topicJSONImages);
                    newTopic.put("types",topicJSONTypes)

                    newTopics.add(newTopic)
                    counter++
                }//end each names
            }

            def createdTopics = []
            def errors = []
            JSONArray newTopicIds = new JSONArray();
            try{
                String ids = "";
                if(newTopics.size() > 0) {
                    newTopics.each { newPayload ->

                def result = adminTopicService.createTopic(newPayload.toString())


                        if(null != result){
                            if(!result.containsKey("error")){
                                result.each{topicId, topicIf ->
                                    createdTopics.add(topicId)
                                    ids += topicId+", "
                                }
                            }
                            else{
                                def names = newPayload.getJSONArray("names")
                                def name = names.get(0).get("name")
                                def errorString = result.get("error")+" : ("+name+")";
                                errors.add(errorString)
                            }
                        }
                    }
                }


                if(existingTopics.size() > 0) {
                    existingTopics.each {topicId ->
                        createdTopics.add(topicId)
                        ids += topicId+", "
                    }
                }

                def message = "<ul>"
                if(createdTopics.size() > 0) {

                    message += "<li>Successfully created topics with the following topic identificators: "+ids+"</li>"
                    if(errors.size() > 0){
                        for(String error:errors){
                            message += "<li>"+error+"</li>"
                        }

                    }
                    //add relations to th subject
                    createdTopics.each { createdTopicId ->
                        newTopicIds.put(createdTopicId);
                    }
                    def associatedTopics = adminTopicService.saveTopicsBySubjectMatter(subjectId,newTopicIds.toString(),NdlaSite.NDLA)

                    if(associatedTopics.size() > 0) {
                        def associatedIds = []
                        associatedTopics.each { associatedTID ->
                            def associatedTopicId = associatedTID.substring(associatedTID.lastIndexOf("#")+1)
                            associatedIds.add(associatedTopicId)
                        }

                        if(associatedIds.containsAll(createdTopics)) {
                            message += "<li>Successfully associated topics with the following topic identificators: "+ids+" to the subject id: "+subjectId+"</li>"
                        }
                        else{
                            message += "<li>Could not successfully associate topics with the following topic identificators: "+ids+" to the subject id: "+subjectId+". Contact the administrator</li>"
                        }

                    }
                    flash.success = message
                }
                else{
                    if(errors.size() > 0){
                        for(String error:errors){
                            message += "<li>"+error+"</li>"
                        }

                    }
                    flash.error = message
                }
                message = "</ul>"
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to update the keyword."
            }

            redirect(controller: 'ontology', action: 'addSubjectTopics', params: [subjectId: subjectId, sites: site])
        }
        .invalidToken {
            // Bad request.
        }

    }


    def removeSubjectTopicByTopicId() {
        def topicId = params.topicId
        def site = params.site
        def subjectId = params.subjectId
        def subjectName = params.subjectName

        NdlaTopic topic = adminTopicService.getTopicByTopicId(topicId);
        def topicName = ""
        if (null != topic) {
            def names = topic.getNames()
            if(null != names){
                if(names.get("http://psi.oasis-open.org/iso/639/#nob") != ""){
                    topicName = names.get("http://psi.oasis-open.org/iso/639/#nob");
                }
                else if(names.get("http://psi.oasis-open.org/iso/639/#eng") && topicName == ""){
                    topicName = names.get("http://psi.oasis-open.org/iso/639/#eng");
                }
                else if(names.get("http://psi.topic.ndla.no/#language-neutral") && topicName == ""){
                    topicName = names.get("http://psi.topic.ndla.no/#language-neutral");
                }
            }
            else{
                names = topic.getWordNames();
                names.each { nameObj ->
                    def nameObjNames = nameObj.getNames()
                    nameObjNames.each { lang, val ->
                        if(lang == "http://psi.oasis-open.org/iso/639/#nob"){
                            topicName = val;
                        }
                        else if(lang == "http://psi.oasis-open.org/iso/639/#eng" && topicName == ""){
                            topicName = val;
                        }
                        else if(lang == "http://psi.topic.ndla.no/#language-neutral" && topicName == ""){
                            topicName = val;
                        }
                    }
                }
            }


            //does it have connected subjects ?


            //does it have connected nodes ?
            ArrayList<NdlaTopic> nodes = null;

            if(site == "NDLA"){
                nodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.NDLA)
            }
            else if(site == "DELING"){
                nodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.DELING)
            }
            else if(site == "FYR"){
                nodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.NYGIV)
            }

            def nodeCount = nodes.size()

            [topicId: topicId,topicName: topicName, site: site, subjectId: subjectId, nodeCount: nodeCount, subjectName: subjectName]
        }
        else{
            flash.error = "An error occurred while trying to get the topic by topicId "+topicId
            redirect(controller: 'ontology', action: 'removeSubjectTopics')
        }

    }

    /**
     * @Todo Not sure if this is really a sustainable functionality to develop
     *
     */
    def removeRelationRoleTopicByTopicId() {
        def topicId = params.topicId
        def roleName = params.roleName
        def assocType = params.assocType
        def relationCount = params.relationCount

        NdlaTopic topic = adminTopicService.getTopicByTopicId(topicId);

        def topicName = ""
        if (null != topic) {
            [topicId: topicId,roleName: roleName, assocType: assocType, relationCount: relationCount]
        }
        else{
            flash.error = "An error occurred while trying to get the topic by topicId "+topicId
            redirect(controller: 'ontology', action: 'removeRelationRoleTopics')
        }

    }

    def removeSubjectTopics(){
        def subjectId = params.subjectId
        def sites = params.sites
        NdlaTopic subjectTopic = null
        def Subject subj

        def shortMap = [:]
        def langMap = [:]
        def missingLangs = [:]

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            if(!languageItem.psi.equals("http://psi.topic.ndla.no/#language-neutral") && !languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#eng")) {
                if(!languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#nob")) {
                    missingLangs.put(languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
                }
            }
            langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1))
        }

        if(null != params.sites){
            switch (params.sites) {
                case "NDLA":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NDLA)
                    break;
                case "DELING":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.DELING)
                    break;
                case "FYR":
                    subjectTopic = adminTopicService.getSubjectBySubjectId(subjectId,NdlaSite.NYGIV)
                    break;
            }
        }

        if(null != subjectTopic && null != params.sites) {
            def names = subjectTopic.getNames()
            subj =  new Subject(subjectTopic.getIdentifier(),subjectTopic.getUdirPsi(),subjectTopic.getPsis(),names,subjectTopic.getOriginatingSites())

            [subject: subj, subjectId: subjectId, site: sites, langMap: langMap, shortMap: shortMap,missinglangs: missingLangs,searchDomain: grailsApplication.config.INDEX_SERVICE_URL,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
        else{
            flash.error = "An error occurred while trying to fetch the subject for the site "+params.sites+". It was probably not configured for this site"
            redirect(controller: 'ontology', action: 'removeSubjectTopics', params: [subjectId: subjectId, sites: sites])
        }
    }

    def doRemoveSubjectTopic(RemoveSubjectTopicCommand command){
        withForm {
            def topicId = params.topicId
            def subjectId = params.subjectId
            def site = params.site

            //validation
            if (command.hasErrors()) {
                flash.warning = "The topicId,subjectId and site fields cannot be empty."
                redirect(controller: 'ontology', action: 'removeSubjectTopics')
                return
            }
            JSONArray payload = new JSONArray();
            payload.put(topicId);
            def data

            if(site == "NDLA"){
                data = adminTopicService.deleteTopicsFromSubjectOntology(payload.toString(),subjectId,NdlaSite.NDLA)
            }
            else if(site == "DELING"){
                data = adminTopicService.deleteTopicsFromSubjectOntology(payload.toString(),subjectId,NdlaSite.DELING)
            }
            else if(site == "FYR"){
                data = adminTopicService.deleteTopicsFromSubjectOntology(payload.toString(),subjectId,NdlaSite.NYGIV)
            }

            if(null != data && data.size() > 0 && data.contains(topicId)){
                flash.success = "The topic with id "+topicId+" was successfully deleted from subject "+subjectId;
            }
            else{
                flash.error = "An error occurred while trying to delete the topic "+topicId+" from the subject "+subjectId
            }

            redirect(controller: 'ontology', action: 'removeSubjectTopics', params: [subjectId: subjectId, sites: site])

        }
        .invalidToken {
            // Bad request.
        }
    }
    /*** END ONTOLOGY TOPIC EDITING ***/


    /*** RELATIONS ***/
    def createRelationshipType(){
        def shortMap = [:]
        def langMap = [:]
        def missingLangs = [:]

        def ontologyRoleTopics = [
                "http://psi.topic.ndla.no/#topic" : "Topic",
                "http://psi.topic.ndla.no/#keyword" : "Keyword",
                "http://psi.topic.ndla.no/#ndla-node" : "Ndla node",
                "http://psi.topic.ndla.no/#nygiv-node" : "Fyr node",
                "http://psi.topic.ndla.no/#deling-node" : "Deling node",
                "http://psi.topic.ndla.no/#subjectmatter" : "Subject matter",
                "http://psi.udir.no/ontologi/lkt/#laereplan" : "Curriculum",
                "http://psi.udir.no/ontologi/lkt/#hovedomraade" : "Curriculum: Main area",
                "http://psi.udir.no/ontologi/#kompetansemaalsett" : "Curriculum: Competence aim set",
                "http://psi.udir.no/ontologi/lkt/#kompetansemaal" : "Curriculum: Competence aim"
        ]

        def relationTypeTypes = [
                "http://psi.topic.ndla.no/#hierarchical-relation-type" : "Hierarchical relation type",
                "http://psi.topic.ndla.no/#horisontal-relation-type" : "Horisontal relation type"
        ]
        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            if(!languageItem.psi.equals("http://psi.topic.ndla.no/#language-neutral") && !languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#eng")) {
                if(!languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#nob")) {
                    missingLangs.put(languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
                }
            }
            langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1))
        }
        [langMap: langMap, shortMap: shortMap,missinglangs: missingLangs,ontologyRoleTopics: ontologyRoleTopics,relationTypeTypes:relationTypeTypes,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix, searchDomain: grailsApplication.config.INDEX_SERVICE_URL]

    }

    def doCreateRelationType(){
        def selectedRoleTopicId1 = params.selectedRoleTopicId1
        def selectedRoleTopicId2 = params.selectedRoleTopicId2
        def computerName = params.computerName
        def relationType = params.relationType

        JSONObject payload = new JSONObject()
        JSONArray relationTypes = new JSONArray()

        JSONObject relationTypeJSON = new JSONObject()
        relationTypeJSON.put("data-name",computerName)
        relationTypeJSON.put("relationType-type",relationType.substring(relationType.indexOf("#")+1))

        JSONArray relationTypeNames = new JSONArray()

        params.each {key , val ->
            if(key.toString().contains("role1Name") || key.toString().contains("role2Name")){
                def lang = "http://psi.oasis-open.org/iso/639/#"+key.toString().substring(0,key.toString().lastIndexOf("_"))
                JSONObject relationTypeName = new JSONObject()
                relationTypeName.put("language",lang)
                relationTypeName.put("name",val)

                JSONArray roleTopics = new JSONArray()
                JSONObject roleTopic1 = new JSONObject()
                JSONObject roleTopic2 = new JSONObject()
                JSONArray roleTopicNames = new JSONArray()

                if(key.toString().contains("role1Name")){
                    if(selectedRoleTopicId1.contains("http")){
                        roleTopic1.put("roleTopicPsi",selectedRoleTopicId1)
                    }
                    else{
                        roleTopic1.put("roleTopicPsi","http://psi.topic.ndla.no/topics/#"+selectedRoleTopicId1)
                    }

                    roleTopic1.put("roleTopicType","http://psi.topic.ndla.no/#superordinate-role-type")
                    roleTopic1.put("names",roleTopicNames)
                    roleTopics.put(roleTopic1)
                }
                else{
                    if(selectedRoleTopicId2.contains("http")){
                        roleTopic2.put("roleTopicPsi",selectedRoleTopicId2)
                    }
                    else{
                        //eksisterende rolletopics har ikke topics i namespace PSI
                        roleTopic2.put("roleTopicPsi","http://psi.topic.ndla.no/topics/#"+selectedRoleTopicId2)
                    }

                    roleTopic2.put("roleTopicType","http://psi.topic.ndla.no/#subordinate-role-type")
                    roleTopic2.put("names",roleTopicNames)
                    roleTopics.put(roleTopic2)
                }


                relationTypeName.put("role-topics",roleTopics)
                relationTypeNames.put(relationTypeName)
            }
        }

        relationTypeJSON.put("names",relationTypeNames)

        relationTypes.put(relationTypeJSON)
        payload.put("relationTypes",relationTypes)

        try{
            def data = adminTopicService.createRelationShipTypes(payload.toString())

            flash.success = "The "+computerName+" relationship type was successfully saved"
        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to update the save the "+computerName+" relationship type"
        }

        redirect(controller: 'ontology', action: 'createRelationshipType')
    }

    def editRelationShipTypes() {
        ArrayList<NdlaTopic> hierarchicalTypes = adminTopicService.getHierarchicalAssociationTypes()
        ArrayList<NdlaTopic> horizontalTypes = adminTopicService.getHorizontalAssociationTypes()

        def hierarchicalTypeMap = [:]
        def horizontalTypeMap = [:]

        hierarchicalTypes.each { hierarchicalType ->
            hierarchicalTypeMap.put(hierarchicalType.getPsi(),hierarchicalType.getIdentifier())
        }

        horizontalTypes.each { horizontalType ->
            horizontalTypeMap.put(horizontalType.getPsi(),horizontalType.getIdentifier())
        }

        [hierarchicalTypeMap: hierarchicalTypeMap,horizontalTypeMap:horizontalTypeMap]
    }

    def editRelationShipType() {
        def horizontalTypeMap = params.horizontalTypeMap
        def hierarchicalTypeMap = params.hierarchicalTypeMap
        NdlaTopic relationshipType = null;
        def typeId = ""
        def typeTypes = [:]
        def typeNames = [:]
        def found = [:]
        def roleFound = [:]
        def roleMapper = [:]
        def roleCounter = 1;
        def alternativeTypeNames = [:]


        def relationshipTypeType = ""
        def relationTypeTypes = [
                "http://psi.topic.ndla.no/#hierarchical-relation-type" : "Hierarchical relation type",
                "http://psi.topic.ndla.no/#horisontal-relation-type" : "Horisontal relation type"
        ]

        if(horizontalTypeMap != ""){
            def horizontalTypeId = horizontalTypeMap.substring(horizontalTypeMap.lastIndexOf("#")+1)
            if(horizontalTypeId.contains("/")){
                horizontalTypeId = horizontalTypeId.substring(horizontalTypeId.lastIndexOf("/")+1)
            }
            relationshipType = adminTopicService.getHorizontalAssociationTypeById(horizontalTypeId)
            relationshipTypeType = "http://psi.topic.ndla.no/#horisontal-relation-type"
        }
        else if(hierarchicalTypeMap != ""){
            def hierarchicalTypeId = hierarchicalTypeMap.substring(hierarchicalTypeMap.lastIndexOf("#")+1)
            if(hierarchicalTypeId.contains("/")){
                hierarchicalTypeId = hierarchicalTypeId.substring(hierarchicalTypeId.lastIndexOf("/")+1)
            }
            relationshipType = adminTopicService.getHierarchicalAssociationTypeById(hierarchicalTypeId)
            relationshipTypeType = "http://psi.topic.ndla.no/#hierarchical-relation-type"
        }

        if(null != relationshipType) {
            typeId = relationshipType.getPsi()

            def types = relationshipType.getTypes()
            types.each {key, type ->
                if(!relationTypeTypes[key]){
                    typeTypes.put(key,type)
                }

            }

            def names = relationshipType.getRelationshipNames()

            names.each { key, map ->
                def mapper = [:]
                def isoNames = [:]
                def roleNames = [:]
                map.each{ mapKey, val ->


                    if(mapKey.contains("iso")){
                        isoNames.put(mapKey,val)
                    }
                    else{
                        if(!found.containsKey(mapKey)){
                            def typeKey = mapKey.substring(mapKey.indexOf("#")+1)
                            def typeTopic = adminTopicService.getRelationTopicByTopicId(mapKey)


                            def theName = ""
                            if (null != typeTopic) {
                                names = typeTopic.getNames()
                                if (null != names) {
                                    if (names.get("http://psi.oasis-open.org/iso/639/#nob") != "") {
                                        theName = names.get("http://psi.oasis-open.org/iso/639/#nob");
                                    } else if (names.get("http://psi.oasis-open.org/iso/639/#eng") && theName == "") {
                                        theName = names.get("http://psi.oasis-open.org/iso/639/#eng");
                                    } else if (names.get("http://psi.topic.ndla.no/#language-neutral") && theName == "") {
                                        theName = names.get("http://psi.topic.ndla.no/#language-neutral");
                                    }
                                } else {
                                    names = typeTopic.getWordNames();
                                    names.each { nameObj ->
                                        def nameObjNames = nameObj.getNames()
                                        nameObjNames.each { lang, wordNameVal ->
                                            if (lang == "http://psi.oasis-open.org/iso/639/#nob") {
                                                theName = wordNameVal;
                                            } else if (lang == "http://psi.oasis-open.org/iso/639/#eng" && theName == "") {
                                                theName = wordNameVal;
                                            } else if (lang == "http://psi.topic.ndla.no/#language-neutral" && theName == "") {
                                                theName = wordNameVal;
                                            }
                                        }
                                    }
                                }

                            }

                            roleNames.put(mapKey,theName)
                            found.put(typeKey,theName)
                            roleFound.put(mapKey,theName)
                        }
                        else{
                            roleNames.put(mapKey,found.get(val))
                        }

                    }
                    mapper.put("isoNames",isoNames)
                    mapper.put("roleNames",roleNames)
                }
                typeNames.put(key,mapper)
            }
        }


        roleFound.each{ foundKey, foundName ->
            roleMapper.put(foundKey,roleCounter)
            roleCounter++
        }

        def shortMap = [:]
        def langMap = [:]
        def missingLangs = [:]



        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            if(!languageItem.psi.equals("http://psi.topic.ndla.no/#language-neutral") && !languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#eng")) {
                if(!languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#nob")) {
                    //missingLangs.put(languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
                    missingLangs.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
                }
            }
            langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1))
        }

        def ontologyRoleTopics = [
                "http://psi.topic.ndla.no/#topic" : "Topic",
                "http://psi.topic.ndla.no/#keyword" : "Keyword",
                "http://psi.topic.ndla.no/#ndla-node" : "Ndla node",
                "http://psi.topic.ndla.no/#nygiv-node" : "Fyr node",
                "http://psi.topic.ndla.no/#deling-node" : "Deling node",
                "http://psi.topic.ndla.no/#subjectmatter" : "Subject matter",
                "http://psi.udir.no/ontologi/lkt/#laereplan" : "Curriculum",
                "http://psi.udir.no/ontologi/lkt/#hovedomraade" : "Curriculum: Main area",
                "http://psi.udir.no/ontologi/#kompetansemaalsett" : "Curriculum: Competence aim set",
                "http://psi.udir.no/ontologi/lkt/#kompetansemaal" : "Curriculum: Competence aim"
        ]


        [typeId : typeId, typeTypes: typeTypes,relationshipTypeType: relationshipTypeType, relationTypeTypes: relationTypeTypes,typeNames: typeNames, roleMapper: roleMapper,ontologyRoleTopics: ontologyRoleTopics, langMap: langMap, shortMap: shortMap,missinglangs: missingLangs,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix, searchDomain: grailsApplication.config.INDEX_SERVICE_URL]

    }

    def addRelationRole(){
        def shortMap = [:]
        def langMap = [:]
        def missingLangs = [:]

        def roleTypes = [
                "http://psi.topic.ndla.no/#subordinate-role-type" : "Subordinate / preceding role type",
                "http://psi.topic.ndla.no/#superordinate-role-type" : "Superordinate / following role type"
        ]

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            if(!languageItem.psi.equals("http://psi.topic.ndla.no/#language-neutral") && !languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#eng") && !languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#nob")) {
                missingLangs.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }
            langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1))
        }
        [roleTypes: roleTypes,langMap: langMap, shortMap: shortMap,missinglangs: missingLangs,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix, searchDomain: grailsApplication.config.INDEX_SERVICE_URL]
    }


    def editRelationRoleTopics(){
        def roleTypes = [
                "http://psi.topic.ndla.no/#subordinate-role-type" : "Subordinate / preceding role type",
                "http://psi.topic.ndla.no/#superordinate-role-type" : "Superordinate / following role type"
        ]
        [roleTypes: roleTypes,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix, searchDomain: grailsApplication.config.INDEX_SERVICE_URL]
    }

    def editRelationRoleTopic() {
        def topicId = params.topicId

        NdlaTopic topic = adminTopicService.getTopicByTopicId(topicId)
        def names = topic.getNames()
        def nameMap = [:]

        if(null != names){
            names.each{ namePsi, nameString ->
                if(namePsi.toString().contains("iso")){
                    nameMap.put(namePsi,nameString)
                }
            }
        }
        else{
            def wordNames = topic.getWordNames()
            wordNames.each{wordName ->
                def nameTmps = wordName.names
                nameTmps.each{nameTmpPsi,nameTmpString ->
                    if(nameTmpPsi.toString().contains("iso")){
                        nameMap.put(nameTmpPsi,nameTmpString)
                    }
                }
            }
        }

        def descriptions = topic.getDescriptions()
        def images = topic.getImages()

        def topicPsis = topic.getPsis()
        def topicTypes = topic.getTypes().keySet().toArray()
        def topicType = topicTypes[0]

        def langMap = [:]
        def shortMap = [:]
        def missingLangs = [:]
        def topicNames = [:]



        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1))
        }


        for( l in langMap){
            if(null == nameMap.get(l.key)){
                missingLangs.put(l.key,l.value);
            }
        }
        [topicId: topicId, languages: langMap, shortMap: shortMap, missinglangs : missingLangs, names : nameMap,descriptions: descriptions, images: images,psis: topicPsis,topicType: topicType]
    }


    def doUpdateRelationRole(){
        withForm {
            def roleType = params.topicType
            def newTopicNames = []
            def newTopicDescriptions = []
            def newTopicImages = []
            def isoBaseUrl = "http://psi.oasis-open.org/iso/639/#"
            def psis = []
            def topicId = ""


            def tempNameData = [:]
            def tempDescriptionData = [:]
            def tempImageData = [:]
            params.each{param, val ->
                if(param.toString().contains("description_")){
                    def descLangId = param.split("_")
                    tempDescriptionData.put(isoBaseUrl+descLangId[1], val)
                }
                else if(param.toString().contains("name_")){
                    def langId = param.split("_")
                    tempNameData.put(isoBaseUrl+langId[1],val)
                }
                else if(param.toString().contains("image_")){
                    def langId = param.split("_")
                    tempImageData.put(isoBaseUrl+langId[1],val)
                }
                else if(param.toString().contains("subjId_")){
                    topicId = val.toString().substring(val.toString().lastIndexOf("#")+1)
                    psis.add(val)
                }
            }


            JSONArray topicJSONNames = new JSONArray();
            JSONArray topicJSONDescriptions = new JSONArray();
            JSONArray topicJSONImages = new JSONArray();
            JSONArray topicJSONPsis = new JSONArray();

            JSONArray topicJSONTypes = new JSONArray();
            JSONObject topicJSONType = new JSONObject()
            topicJSONType.put("typeId",roleType)
            topicJSONTypes.put(topicJSONType)

            JSONObject newTopic = new JSONObject()

            tempNameData.each{nameLanguage, nameString ->
                JSONObject nameTmp = new JSONObject()
                nameTmp.put("language",nameLanguage)
                nameTmp.put("name",nameString)
                topicJSONNames.put(nameTmp)
            }
            newTopic.put("names",topicJSONNames)

            tempDescriptionData.each{descriptionLanguage, descriptionString ->
                JSONObject descTmp = new JSONObject()
                descTmp.put("language",descriptionLanguage)
                descTmp.put("content",descriptionString)
                topicJSONDescriptions.put(descTmp)
            }

            tempImageData.each{imageLanguage, imageString ->
                JSONObject descTmp = new JSONObject()
                descTmp.put("language",imageLanguage)
                descTmp.put("url",imageString)
                topicJSONImages.put(descTmp)
            }

            psis.each{psi ->
                topicJSONPsis.put(psi)
            }

            newTopic.put("wordClass","noun")
            newTopic.put("approved","false")
            newTopic.put("descriptions",topicJSONDescriptions)
            newTopic.put("images",topicJSONImages)
            newTopic.put("types",topicJSONTypes)
            newTopic.put("psis",topicJSONPsis)

            def updatedTopics = []
            String ids = "";
            JSONArray wrapper = new JSONArray();
            wrapper.put(newTopic);
            JSONObject updateObject = new JSONObject();

            JSONObject topicIdObject = new JSONObject()
            topicIdObject.put(topicId,wrapper)

            JSONArray topicIdWrapper = new JSONArray()
            topicIdWrapper.put(topicIdObject)
            updateObject.put("topics",topicIdWrapper);

            try{
                def result = adminTopicService.updateTopicsByTopicId(updateObject.toString())
                result.each{updateTopicId, topicIf ->
                    updatedTopics.add(updateTopicId)
                    ids += topicId+", "
                }

                def message = "<ul>"
                if(updatedTopics.size() > 0) {
                    message += "<li>Successfully UPDATED topics with the following topic identificators: "+ids+"</li>"
                    message += "</ul>"

                }
                flash.success = message


            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to create the topic."
            }

            redirect(controller: 'ontology', action: 'editRelationRoleTopics')


        }
        .invalidToken {
            // Bad request.
        }
    }

    def removeRelationRoleTopics(){
        def roleTypes = [
                "http://psi.topic.ndla.no/#subordinate-role-type" : "Subordinate / preceding role type",
                "http://psi.topic.ndla.no/#superordinate-role-type" : "Superordinate / following role type"
        ]
        [roleTypes: roleTypes,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix, searchDomain: grailsApplication.config.INDEX_SERVICE_URL]
    }


    def doCreateRelationRole(){
        withForm {
            def relationType = params.roleType
            def newTopicNames = []
            def newTopicDescriptions = []
            def newTopicImages = []
            def isoBaseUrl = "http://psi.oasis-open.org/iso/639/#"

            def tempNameData = [:]
            def tempDescriptionData = [:]
            params.each{param, val ->
                if(param.toString().contains("description_")){
                    def descLangId = param.split("_")
                    tempDescriptionData.put(isoBaseUrl+descLangId[1], val)
                }
                else if(param.toString().contains("title_")){
                    def langId = param.split("_")
                    tempNameData.put(isoBaseUrl+langId[1],val)
                }
            }

            JSONArray topicJSONNames = new JSONArray();
            JSONArray topicJSONDescriptions = new JSONArray();
            JSONArray topicJSONImages = new JSONArray();

            JSONArray topicJSONTypes = new JSONArray();
            JSONObject topicJSONType = new JSONObject()
            topicJSONType.put("typeId",relationType)
            topicJSONTypes.put(topicJSONType)

            JSONObject newTopic = new JSONObject()

            tempNameData.each{nameLanguage, nameString ->
                JSONObject nameTmp = new JSONObject()
                nameTmp.put("language",nameLanguage)
                nameTmp.put("name",nameString)
                topicJSONNames.put(nameTmp)
            }
            newTopic.put("names",topicJSONNames)

            tempDescriptionData.each{descriptionLanguage, descriptionString ->
                JSONObject descTmp = new JSONObject()
                descTmp.put("language",descriptionLanguage)
                descTmp.put("content",descriptionString)
                topicJSONDescriptions.put(descTmp)
            }
            newTopic.put("fromKeyword","newTopic")
            newTopic.put("wordClass","noun")
            newTopic.put("descriptions",topicJSONDescriptions)
            newTopic.put("images",topicJSONImages)
            newTopic.put("types",topicJSONTypes)

            def createdTopics = []
            String ids = "";

            try{
                def result = adminTopicService.createTopic(newTopic.toString())
                result.each{topicId, topicIf ->
                    createdTopics.add(topicId)
                    ids += topicId+", "
                }

                def message = "<ul>"
                if(createdTopics.size() > 0) {
                    message += "<li>Successfully created topics with the following topic identificators: "+ids+"</li>"
                    message += "</ul>"

                }
                flash.success = message


            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to create the topic."
            }
            redirect(controller: 'ontology', action: 'addRelationRole')


        }
        .invalidToken {
            // Bad request.
        }
    }

    def doUpdateRelationType() {
        def relationShipTypeId = params.relationShipTypeId
        def selectedRoleTopicId1 = params.selectedRoleTopicId1
        def selectedRoleTopicId2 = params.selectedRoleTopicId2
        def relationType = params.relationType

        withForm {

            JSONObject payload = new JSONObject()

            payload.put("topicId",relationShipTypeId)
            JSONArray relationTypeIds = new JSONArray()
            relationTypeIds.put(relationType)
            payload.put("relationTypeIds",relationTypeIds)

            JSONArray relationTypeNames = new JSONArray()

            def names = [:]
            def roles = [:]
            params.each {key , val ->
                if(key.toString().contains("iso")){
                    def nameMap = [:]
                    def keyArray = key.toString().split("_");
                    def language = keyArray[0]
                    def commonId = keyArray[1]
                    nameMap.put(language,val)
                    names.put(commonId,nameMap)
                }

                if(key.toString().contains("roleId")) {
                    def roleArray = key.toString().split("_")
                    def roleCommonId = roleArray[1]
                    roles.put(roleCommonId,val)
                }
            }
            JSONArray roleTopics = new JSONArray()
            JSONObject roleTopic1 = new JSONObject()
            JSONObject roleTopic2 = new JSONObject()

            names.each {id, nameData ->
                nameData.each{ lang, NameVal ->
                    JSONObject relationTypeName = new JSONObject()
                    relationTypeName.put("language",lang)
                    relationTypeName.put("name",NameVal)
                    relationTypeName.put("roleTopicPsi",roles.get(id))
                    relationTypeNames.put(relationTypeName)
                }

            }

            payload.put("names",relationTypeNames)

            try{
                def data = adminTopicService.updateAssociationTypeByAssocTypeId(payload.toString())

                flash.success = "The "+relationShipTypeId+" relationship type was successfully saved"
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to update the save the "+relationShipTypeId+" relationship type"
            }

            redirect(controller: 'ontology', action: 'editRelationShipTypes')

        }.invalidToken {
            // Bad request.
        }
    }


    def removeRelationShipTypes(){

        def hierarchicalRelationShipTypes = adminTopicService.getHierarchicalAssociationTypes()
        def horizontalRelationShipTypes = adminTopicService.getHorizontalAssociationTypes()
        def relationTypes = []
        def relationObjects = []
        def found = [:]
        def roleFound = [:]
        relationTypes.addAll(hierarchicalRelationShipTypes)
        relationTypes.addAll(horizontalRelationShipTypes)


        relationTypes.each { currentRelationshipType ->
            if(null != currentRelationshipType) {
                RelationShipType typeObject = new RelationShipType()
                typeObject.psi = currentRelationshipType.getPsi()
                def names = currentRelationshipType.getRelationshipNames()
                def typeNames = [:]
                def role1Id = ""
                def role2Id = ""
                def currentRoleId = ""
                def nameRoles = []
                names.each { key, map ->

                    def mapper = [:]
                    def isoNames = [:]
                    def roleNames = [:]

                    map.each{ mapKey, val ->
                        if(mapKey.contains("iso")){
                            isoNames.put(mapKey,val)
                        }
                        else{
                            if(!found.containsKey(mapKey)){

                                def typeKey = mapKey.substring(mapKey.indexOf("#")+1)

                                if(currentRoleId != typeKey){
                                    currentRoleId = typeKey
                                    nameRoles.add(typeKey)
                                    if(nameRoles.size() == 1){
                                        role1Id = typeKey
                                    }
                                    else if (nameRoles.size() == 2){
                                        role2Id = typeKey
                                    }

                                }


                                def typeTopic = adminTopicService.getRelationTopicByTopicId(mapKey)
                                def theName = typeTopic.getNames().get("http://psi.oasis-open.org/iso/639/#eng")
                                roleNames.put(mapKey,theName)
                                found.put(typeKey,theName)
                                roleFound.put(mapKey,theName)
                            }
                            else{
                                roleNames.put(mapKey,found.get(val))
                            }

                        }
                        mapper.put("isoNames",isoNames)
                        mapper.put("roleNames",roleNames)
                    }
                    typeNames.put(key,mapper)

                    typeObject.names = typeNames
                }
                relationObjects.add(typeObject)
            }
        }

        def shortMap = [:]
        def langMap = [:]
        def missingLangs = [:]



        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            if(!languageItem.psi.equals("http://psi.topic.ndla.no/#language-neutral") && !languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#eng")) {
                if(!languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#nob")) {
                    //missingLangs.put(languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
                    missingLangs.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
                }
            }
            langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1))

        }
        [relationObjects: relationObjects, langMap: langMap, shortMap: shortMap,missinglangs: missingLangs,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix, searchDomain: grailsApplication.config.INDEX_SERVICE_URL]
    }

    def removeRemoveRelationsTypeByTypeId(){
        def relationTypeId = params.relationTypeId.toString().replace("§","#")
        def relationTypeName = params.relationTypeName
        def role1 = params.role1
        def role2 = params.role2
        if(role1.contains("§")) {
            role1 = role1.substring(role1.lastIndexOf("§")+1)
        }
        else{
            role1 = role1.substring(role1.lastIndexOf("/")+1)
        }

        if(role2.contains("§")) {
            role2 = role2.substring(role2.lastIndexOf("§")+1)
        }
        else{
            role2 = role2.substring(role2.lastIndexOf("/")+1)
        }

        def relationImplementCount = adminTopicService.getTopicPlayersByRelationTypeId(relationTypeId,role1,role2)
        def assocCount = relationImplementCount.get(role1).size()
        [relationTypeName:relationTypeName,relationTypeId:relationTypeId,relationImplementCount : assocCount]
    }

    def doRemoveRelationType(){
        def relationTypeId = params.relationTypeId
        def relationTypeName = params.relationTypeName

        JSONArray payload = new JSONArray()
        payload.put(relationTypeId)
        try {
            def data = adminTopicService.deleteAssociationTypeByTopicId(payload.toString())
            flash.success = "The relationship type "+relationTypeName+" was successfully deleted."
            redirect(controller: 'ontology', action: 'removeRelationShipTypes')
        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to delete the relationship type "+relationTypeName
            redirect(controller: 'ontology', action: 'removeRelationShipTypes')
        }
    }



    def doSetSubjectForAddHierarchicalRelation(){
        def relationSubject = ""
        def relationSite = ""
        //withForm {
            try {

                relationSubject = params.relationSubjects
                relationSite = params.relationSites


            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to index the keywords."
            }

            redirect(controller: 'ontology', action: 'addHierarchicalRelation', params: [relationSubject: relationSubject, relationSite: relationSite])


        //}.invalidToken {
            // Bad request.
        //}
    }

    def addHierarchicalRelation(){
        def relationSubject = params.relationSubject
        def relationSite = params.relationSite

        if(relationSubject == null && relationSite == null){
            def subjects = [:]


            ArrayList<NdlaTopic> subjectArray = adminTopicService.getSubjects(50,0,NdlaSite.NDLA);
            subjectArray.each{ subject ->
                subjects.put(subject.getIdentifier(),subject.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }


            [subjects: subjects,  relationSite: relationSite,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
        else if(relationSubject != "" && relationSite != ""){
            def langMap = [:]
            def Language lang;

            ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
            languageArray.each{ languageItem ->
                langMap.put(languageItem.getIdentifier(),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }

            ArrayList<NdlaTopic> hierarchicalTopicTypes = adminTopicService.getHierarchicalAssociationTypes()
            def found = []
            def hierarchicalTypeMap = [:]
            hierarchicalTopicTypes.each { hierarchicalTypeTopic ->
                def psi = hierarchicalTypeTopic.getPsi()

                def names = hierarchicalTypeTopic.getRelationshipNames()

                def isoNames = []
                names.each { key, map ->
                    map.each{ mapKey, val ->
                        if(mapKey == "http://psi.oasis-open.org/iso/639/#eng"){
                            if(!found.contains(mapKey+"_"+val)){
                                found.add(mapKey+"_"+val)
                                isoNames.add(val)
                            }
                        }
                    }
                }
                def name = isoNames.join(" / ")
                if(name.toString().length() > 0) {
                    hierarchicalTypeMap.put(psi,name)
                }
            }

            JSONObject topics = null;
            if(relationSite == "NDLA"){
                topics = adminTopicService.getTopicsBySubjectMatter(100, 0,relationSubject,NdlaSite.NDLA)
            }
            else if(relationSite == "DELING"){
                topics = adminTopicService.getTopicsBySubjectMatter(100,0,relationSubject,NdlaSite.DELING)
            }
            else if(relationSite == "FYR"){
                topics = adminTopicService.getTopicsBySubjectMatter(100, 0,relationSubject,NdlaSite.NYGIV)
            }

            JSONArray subjectTopics = new JSONArray();
            JSONArray relations = topics.getJSONArray("relations");
            for(int i = 0; i < relations.length(); i ++) {
                JSONObject relation = relations.getJSONObject(i);
                JSONArray relatedTopics = relation.getJSONArray("relatedTopics");
                for(int j = 0; j < relatedTopics.length(); j ++) {
                    JSONObject relatedTopic = relatedTopics.getJSONObject(j);
                    String topicId = relatedTopic.getString("topicId");
                    topicId = topicId.substring(topicId.lastIndexOf("#")+1);
                    subjectTopics.put(topicId);
                }
            }//end for
            //System.out.println("hierarchicalTypeMap: "+hierarchicalTypeMap)
            [relationSubject: relationSubject, relationSite: relationSite, hierarchicalTypeMap: hierarchicalTypeMap, languages: langMap,subjectTopics: subjectTopics, searchDomain: grailsApplication.config.INDEX_SERVICE_URL, pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
    }


    def doAddHierarchicalRelation(){
        def chosenTopicRelations = params.chosenTopicRelations
        def site = params.site
        def subjectId = params.subjectId
        JSONArray chosenTopicRelationsArray = new JSONArray(chosenTopicRelations)
        for(int i = 0; i < chosenTopicRelationsArray.length(); i++){
            JSONObject relationObject = chosenTopicRelationsArray.getJSONObject(i);
            JSONArray topics = relationObject.getJSONArray("topics")
            for(int j = 0; j < topics.length(); j++){
                JSONObject topicObject = topics.getJSONObject(j);
                JSONArray roles = topicObject.getJSONArray("roles");
                for(int k = 0; k < roles.length(); k++){
                    JSONObject role = roles.getJSONObject(k);
                    String roleName = role.getString("roleName")
                    if(roleName.contains("§")){
                        roleName = roleName.substring(0,(roleName.length() -2))
                    }
                    role.put("roleName",roleName);
                }
            }
        }

        JSONObject payload = new JSONObject()
        payload.put("associations",chosenTopicRelationsArray)

        //def result = adminTopicService.saveOntologyTopicAssociations()

        try {
            def data = null
            if(site.toString().equals("NDLA")){
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.topic.ndla.no/#subjectmatter",payload.toString(),NdlaSite.NDLA)
            }
            else if(site.toString().equals("FYR")){
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.topic.ndla.no/#subjectmatter",payload.toString(),NdlaSite.NYGIV)
            }
            else{
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.topic.ndla.no/#subjectmatter",payload.toString(),NdlaSite.DELING)
            }


            flash.success = "The topic(s) were successfully related to the competence aim(s)."
        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to update the keyword."
        }

        redirect(controller: 'ontology', action: 'addHierarchicalRelation',params: [relationSubject: subjectId,relationSite: site] )
    }

    def doSetSubjectForAddHorisontalRelation(){
        def relationSubject = ""
        def relationSite = ""
        //withForm {
            try {

                relationSubject = params.relationSubjects
                relationSite = params.relationSites


            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to index the keywords."
            }

            redirect(controller: 'ontology', action: 'addHorisontalRelation', params: [relationSubject: relationSubject, relationSite: relationSite])


        //}.invalidToken {
            // Bad request.
        //}
    }

    def addHorisontalRelation(){
        def relationSubject = params.relationSubject
        def relationSite = params.relationSite

        if(relationSubject == null && relationSite == null){
            def subjects = [:]

            ArrayList<NdlaTopic> subjectArray = adminTopicService.getSubjects(50,0,NdlaSite.NDLA);
            subjectArray.each{ subject ->
                subjects.put(subject.getIdentifier(),subject.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }


            [subjects: subjects,  relationSite: relationSite,pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
        else if(relationSubject != "" && relationSite != ""){
            def langMap = [:]
            def Language lang;

            ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
            languageArray.each{ languageItem ->
                langMap.put(languageItem.getIdentifier(),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            }

            ArrayList<NdlaTopic> horizontalTopicTypes = adminTopicService.getHorizontalAssociationTypes()
            def found = []
            def horizontalTypeMap = [:]
            horizontalTopicTypes.each { horizontalTypeTopic ->
                def psi = horizontalTypeTopic.getPsi()

                def names = horizontalTypeTopic.getRelationshipNames()

                def isoNames = []
                def nameCount = 0
                names.each { key, map ->
                    map.each{ mapKey, val ->
                        if(mapKey == "http://psi.oasis-open.org/iso/639/#eng"){
                            if(!found.contains(mapKey+"_"+val)){
                                found.add(mapKey+"_"+val)
                                if(nameCount++ <= 1){
                                    isoNames.add(val)
                                }

                            }
                        }
                    }
                }
                def name = isoNames.join(" / ")
                if(name.toString().length() > 0) {
                    horizontalTypeMap.put(psi,name)
                }
            }

            JSONObject topics = null;
            if(relationSite == "NDLA"){
                topics = adminTopicService.getTopicsBySubjectMatter(100, 0,relationSubject,NdlaSite.NDLA)
            }
            else if(relationSite == "DELING"){
                topics = adminTopicService.getTopicsBySubjectMatter(100,0,relationSubject,NdlaSite.DELING)
            }
            else if(relationSite == "FYR"){
                topics = adminTopicService.getTopicsBySubjectMatter(100, 0,relationSubject,NdlaSite.NYGIV)
            }

            JSONArray subjectTopics = new JSONArray();
            JSONArray relations = topics.getJSONArray("relations");
            for(int i = 0; i < relations.length(); i ++) {
                JSONObject relation = relations.getJSONObject(i);
                JSONArray relatedTopics = relation.getJSONArray("relatedTopics");
                for(int j = 0; j < relatedTopics.length(); j ++) {
                    JSONObject relatedTopic = relatedTopics.getJSONObject(j);
                    String topicId = relatedTopic.getString("topicId");
                    topicId = topicId.substring(topicId.lastIndexOf("#")+1);
                    subjectTopics.put(topicId);
                }
            }//end for

            //System.out.println("horizontalTypeMap: "+horizontalTypeMap)
            [relationSubject: relationSubject, relationSite: relationSite, horisontalTypeMap: horizontalTypeMap, languages: langMap,subjectTopics : subjectTopics, searchDomain: grailsApplication.config.INDEX_SERVICE_URL, pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix]
        }
    }

    def doAddHorisontalRelation(){

        def chosenTopicRelations = params.chosenTopicRelations
        def site = params.site
        def subjectId = params.subjectId
        JSONArray chosenTopicRelationsArray = new JSONArray(chosenTopicRelations)

        for(int i = 0; i < chosenTopicRelationsArray.length(); i++){
            JSONObject relationObject = chosenTopicRelationsArray.getJSONObject(i);
            JSONArray topics = relationObject.getJSONArray("topics")
            for(int j = 0; j < topics.length(); j++){
                JSONObject topicObject = topics.getJSONObject(j);
                JSONArray roles = topicObject.getJSONArray("roles");
                for(int k = 0; k < roles.length(); k++){
                    JSONObject role = roles.getJSONObject(k);
                    String roleName = role.getString("roleName")
                    if(roleName.contains("§")){
                        roleName = roleName.substring(0,(roleName.length() -2))
                    }
                    role.put("roleName",roleName);
                }
            }
        }
        JSONObject payload = new JSONObject()
        payload.put("associations",chosenTopicRelationsArray)

        try {
            def data = null
            if(site.toString().equals("NDLA")){
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.topic.ndla.no/#subjectmatter",payload.toString(),NdlaSite.NDLA)
            }
            else if(site.toString().equals("FYR")){
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.topic.ndla.no/#subjectmatter",payload.toString(),NdlaSite.NYGIV)
            }
            else{
                data = adminTopicService.saveOntologyTopicAssociations(subjectId,"http://psi.topic.ndla.no/#subjectmatter",payload.toString(),NdlaSite.DELING)
            }

            flash.success = "The topic(s) were successfully related to the competence aim(s)."
        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to update the keyword."
        }

        redirect(controller: 'ontology', action: 'addHorisontalRelation',params: [relationSubject: subjectId,relationSite: site] )
    }

    /*** END RELATIONS ***/
}

// ***** MISCELLANEOUS CLASSES *****

// Command classes (for validation purposes).

@grails.validation.Validateable
class RemoveSubjectTopicCommand {
    String topicId
    String site
    String subjectId

    static constraints = {
        topicId(blank: false)
        site(blank: false)
        subjectId(blank: false)
    }
}

@grails.validation.Validateable
class UpdateSubjectTopicCommand {
    String topicId
    String topicTitleWordClass

    static constraints = {
        topicId(blank: false)
        topicTitleWordClass(blank: false)
    }
}

@grails.validation.Validateable
class SetSubjectTopicCommand {
    String subject

    static constraints = {
        subject(blank: false)
    }
}

@grails.validation.Validateable
class AddSubjectCommand {
    String subjectId
    String UDIRsubjectId
    String englishTitle

    List<String> psis
    List<String> sites
    HashMap<String,String> names

    static constraints = {
        subjectId(blank: false)
        UDIRsubjectId(blank: false)
        englishTitle(blank: false)
        psis minSize: 1
        sites minSize: 1
        names minSize: 1
    }
}

class RelationShipType{
    String psi
    HashMap<String,HashMap<String,String>> names
}
