package no.ndla.topics.admin

import groovy.json.JsonSlurper
import no.ndla.topics.service.NdlaServiceException
import no.ndla.topics.service.model.NdlaTopic
import org.codehaus.jettison.json.JSONArray
import org.codehaus.jettison.json.JSONException
import org.codehaus.jettison.json.JSONObject

import groovyx.net.http.HTTPBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.Method
import groovyx.net.http.RESTClient

import javax.annotation.PostConstruct

import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.Method.GET
import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.ContentType.TEXT

import grails.converters.*

import no.ndla.topics.service.NdlaSite

class ServiceController {

    static final def NDLA_SITE = 1
    static final def DELING_SITE = 2
    static final def NYGIV_SITE = 4

    def adminTopicService // Dependency Injection (DI) by the Spring container.

    // ***** SERVICE END-POINTS (FOR AJAX CALLS) *****

    @PostConstruct
    void init() {
        if(null == adminTopicService.getTopicService()){
            adminTopicService.setTopicService();
        }
    }

    def autoCompleteKeywords() {
        def queryTerm = params.q
        def lang = params.language

        def http = new HTTPBuilder('http://search.test.ndla.no/')
        def keywords = []

        http.request(GET, JSON) {
            uri.path = "/ndla_search/keywords/${queryTerm}"
            uri.query = [language: lang]

            // Response handler for a successful response code.
            response.success = { resp, json ->
                println "Query response: "
                json.responseData.results.each {
                    println "  ${it}"
                }
                /*
                json.each {
                    keywords.add(it.title.toLowerCase())
                }
                */
            }

            // Response handler for any failure status codes.
            response.failure = { resp ->
                println "Error"
            }
        }
        render keywords as JSON
    }

    def lookUpKeywords(){
        def queryTerm = params.q
        def lang = params.language
        def offset = params.offset
        def limit = params.limit

        def searchDomain = grailsApplication.config.INDEX_SERVICE_URL
        def counter = 0;
        def qString = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + queryTerm
        qString += "?"
        if(lang instanceof Collection){
            lang.each{ langstr ->
                qString += "language[]="+langstr+"&"
            }
        }
        else{
            qString += "language="+lang+"&"
        }
        //qString += "type=start&index=keywords&start="+offset+"&limit=10"
        qString += "type=start&index=keywords&start="+offset+"&limit="+limit
        //System.out.println(qString)
        def uri = qString.toURL()

        def keywords = new JsonSlurper().parseText(uri.text)

        render keywords as JSON
    }


    def lookUpTopics(){
        def queryTerm = params.q
        def lang = params.language
        def offset = params.offset

        def searchDomain = grailsApplication.config.INDEX_SERVICE_URL
        def counter = 0;
        def qString = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + queryTerm

        qString += "?"
        if(lang instanceof Collection){
            lang.each{ langstr ->
                qString += "language[]="+langstr+"&"
            }
        }
        else{
            qString += "language="+lang+"&"
        }
        qString += "type=start&index=topics&start="+offset+"&limit=10"

        def uri = qString.toURL()
        def topics = new JsonSlurper().parseText(uri.text)
        render topics as JSON
    }

    def getNodesByKeywordId() {
        def keywordIdentifier = params.keywordIdentifier
        def sites = params.sites.toInteger()

        // ArrayList<NdlaTopic>
        def ndlaNodes = null
        def delingNodes = null
        def nygivNodes = null



        if ((sites & NDLA_SITE) == NDLA_SITE) {
            ndlaNodes = adminTopicService.getNodesByKeywordId(100, 0, keywordIdentifier, NdlaSite.NDLA)
        }

        if ((sites & DELING_SITE) == DELING_SITE) {
            delingNodes = adminTopicService.getNodesByKeywordId(100, 0, keywordIdentifier, NdlaSite.DELING)
        }

        if ((sites & NYGIV_SITE) == NYGIV_SITE) {
            nygivNodes = adminTopicService.getNodesByKeywordId(100, 0, keywordIdentifier, NdlaSite.NYGIV)
        }

        JSONArray jsonNodes = new JSONArray()
        JSONObject jsonNode

        if (ndlaNodes != null && ndlaNodes["nodes"].size() > 0) {
            ndlaNodes["nodes"].each { node ->
                jsonNode = new JSONObject()
                jsonNode.put("site", "NDLA")
                jsonNode.put("identifier", node.identifier)

                def nodeNames = [:]
                def topicNames = node.getNames()
                if(null != topicNames){
                    topicNames.each{ namePsi, nameString ->
                        nodeNames.put(namePsi,nameString)
                    }
                }
                else{
                    def wordNames = topic.getWordNames()
                    wordNames.each{wordName ->
                        def nameTmps = wordName.names
                        nameTmps.each{nameTmpPsi,nameTmpString ->
                            nodeNames.put(nameTmpPsi,nameTmpString)
                        }
                    }
                }
                jsonNode.put("names",nodeNames)

                jsonNodes.put(jsonNode)
            }
        }
        if (delingNodes != null && delingNodes["nodes"].size() > 0) {
            delingNodes["nodes"].each { node ->
                jsonNode = new JSONObject()
                jsonNode.put("site", "Deling")
                jsonNode.put("identifier", node.identifier)
                def nodeNames = [:]
                def topicNames = node.getNames()
                if(null != topicNames){
                    topicNames.each{ namePsi, nameString ->
                        nodeNames.put(namePsi,nameString)
                    }
                }
                else{
                    def wordNames = topic.getWordNames()
                    wordNames.each{wordName ->
                        def nameTmps = wordName.names
                        nameTmps.each{nameTmpPsi,nameTmpString ->
                            nodeNames.put(nameTmpPsi,nameTmpString)
                        }
                    }
                }
                jsonNode.put("names",nodeNames)
                jsonNodes.put(jsonNode)
            }
        }
        if (nygivNodes != null && nygivNodes["nodes"].size() > 0) {
            nygivNodes["nodes"].each { node ->
                jsonNode = new JSONObject()
                jsonNode.put("site", "Nygiv")
                jsonNode.put("identifier", node.identifier)
                def nodeNames = [:]
                def topicNames = node.getNames()
                if(null != topicNames){
                    topicNames.each{ namePsi, nameString ->
                        nodeNames.put(namePsi,nameString)
                    }
                }
                else{
                    def wordNames = topic.getWordNames()
                    wordNames.each{wordName ->
                        def nameTmps = wordName.names
                        nameTmps.each{nameTmpPsi,nameTmpString ->
                            nodeNames.put(nameTmpPsi,nameTmpString)
                        }
                    }
                }
                jsonNode.put("names",nodeNames)
                jsonNodes.put(jsonNode)
            }
        }

        render jsonNodes.toString()
    }

    def getSubjectsBySiteString() {
        def site = params.site
        def sitemapping = [NDLA : "http://ndla.no/",DELING : "http://deling.ndla.no/", FYR : "http://fyr.ndla.no/"]
        ArrayList<NdlaTopic> subjects = null;

        if(site == "NDLA"){
            subjects = adminTopicService.getSubjects(50, 0,NdlaSite.NDLA)
        }
        else if(site == "DELING"){
            subjects = adminTopicService.getSubjects(50, 0,NdlaSite.DELING)
        }
        else if(site == "FYR"){
            subjects = adminTopicService.getSubjects(50, 0,NdlaSite.NYGIV)
        }
        JSONArray jsonSubjects = new JSONArray()
        JSONObject jsonSubject

        if(subjects.size() > 0) {
            def sites = []
            subjects.each { subject ->
                jsonSubject = new JSONObject();
                def identifiers = subject.getIdentifiers();
                def subjectId = "";
                if(identifiers.size() > 0){
                    identifiers.each{identifier ->
                        def idArr = identifier.split("_")
                        if(!idArr[1].isNumber()){
                            subjectId = identifier;
                        }
                    }
                }
                else{
                    def identifier = subject.getIdentifier();
                    subjectId = identifier;
                }


                jsonSubject.put("identifier",subjectId)
                jsonSubject.put("name",subject.getNames().get("http://psi.oasis-open.org/iso/639/#eng"))
                sites = subject.getOriginatingSites();
                if(sites.contains(sitemapping.get(site))){
                    jsonSubjects.put(jsonSubject)
                }

            }
        }
        render jsonSubjects.toString()
    }

    def getSubjectsByTopicId() {
        def site = params.site
        def topicId = params.topicId

        ArrayList<NdlaTopic> subjects = null;

        if(site == "NDLA"){
            subjects = adminTopicService.getSubjectsByTopicId(50, 0,topicId,NdlaSite.NDLA)
        }
        else if(site == "DELING"){
            subjects = adminTopicService.getSubjectsByTopicId(50, 0,topicId,NdlaSite.DELING)
        }
        else if(site == "FYR"){
            subjects = adminTopicService.getSubjectsByTopicId(50, 0,topicId,NdlaSite.NYGIV)
        }
        JSONArray jsonSubjects = new JSONArray()
        JSONObject jsonSubject

        if(null != subjects && subjects.size() > 0) {
            def sites = []
            subjects.each { subject ->
                jsonSubject = new JSONObject();
                jsonSubject.put("identifier",subject.identifier)
                jsonSubject.put("name",subject.getNames().get("http://psi.oasis-open.org/iso/639/#eng"))
                jsonSubjects.put(jsonSubject)
            }
        }
        render jsonSubjects.toString()

    }

    def getNodesByTopicId() {
        def topicId = params.topicId
        def site = params.site

        // ArrayList<NdlaTopic>
        def ndlaNodes = null
        def delingNodes = null
        def nygivNodes = null

        if (site == "NDLA") {
            ndlaNodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.NDLA)
        }

        if (site =="DELING") {
            delingNodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.DELING)
        }

        if (site == "NYGIV") {
            nygivNodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.NYGIV)
        }



        JSONArray jsonNodes = new JSONArray()
        JSONObject jsonNode

        if (ndlaNodes != null && ndlaNodes.size() > 0) {
            ndlaNodes.each { node ->
                jsonNode = new JSONObject()
                jsonNode.put("site", "NDLA")
                jsonNode.put("identifier", node.identifier)
                //jsonNode.put("name",node.getNames().get("http://psi.oasis-open.org/iso/639/#eng"))
                def nodeNames = [:]
                def topicNames = node.getNames()
                if(null != topicNames){
                    topicNames.each{ namePsi, nameString ->
                        nodeNames.put(namePsi,nameString)
                    }
                }
                else{
                    def wordNames = topic.getWordNames()
                    wordNames.each{wordName ->
                        def nameTmps = wordName.names
                        nameTmps.each{nameTmpPsi,nameTmpString ->
                            nodeNames.put(nameTmpPsi,nameTmpString)
                        }
                    }
                }
                jsonNode.put("names",nodeNames)
                jsonNodes.put(jsonNode)
            }
        }
        if (delingNodes != null && delingNodes.size() > 0) {
            delingNodes.each { node ->
                jsonNode = new JSONObject()
                jsonNode.put("site", "Deling")
                jsonNode.put("identifier", node.identifier)
                //jsonNode.put("name",node.getNames().get("http://psi.oasis-open.org/iso/639/#eng"))
                def nodeNames = [:]
                def topicNames = node.getNames()
                if(null != topicNames){
                    topicNames.each{ namePsi, nameString ->
                        nodeNames.put(namePsi,nameString)
                    }
                }
                else{
                    def wordNames = topic.getWordNames()
                    wordNames.each{wordName ->
                        def nameTmps = wordName.names
                        nameTmps.each{nameTmpPsi,nameTmpString ->
                            nodeNames.put(nameTmpPsi,nameTmpString)
                        }
                    }
                }
                jsonNode.put("names",nodeNames)
                jsonNodes.put(jsonNode)
            }
        }
        if (nygivNodes != null && nygivNodes.size() > 0) {
            nygivNodes.each { node ->
                jsonNode = new JSONObject()
                jsonNode.put("site", "Nygiv")
                jsonNode.put("identifier", node.identifier)
                //jsonNode.put("name",node.getNames().get("http://psi.oasis-open.org/iso/639/#eng"))
                def nodeNames = [:]
                def topicNames = node.getNames()
                if(null != topicNames){
                    topicNames.each{ namePsi, nameString ->
                        nodeNames.put(namePsi,nameString)
                    }
                }
                else{
                    def wordNames = topic.getWordNames()
                    wordNames.each{wordName ->
                        def nameTmps = wordName.names
                        nameTmps.each{nameTmpPsi,nameTmpString ->
                            nodeNames.put(nameTmpPsi,nameTmpString)
                        }
                    }
                }
                jsonNode.put("names",nodeNames)
                jsonNodes.put(jsonNode)
            }
        }

        render jsonNodes.toString()
    }

    def getTopicsBySubjectMatterId() {
        def site = params.site
        def subjectId = params.subjectId
        def limit = params.limit
        def offset = params.offset
        JSONObject topics = null;
        if(site == "NDLA"){
            topics = adminTopicService.getTopicsBySubjectMatter(limit.toInteger(), offset.toInteger(),subjectId,NdlaSite.NDLA)
        }
        else if(site == "DELING"){
            topics = adminTopicService.getTopicsBySubjectMatter(limit.toInteger(), offset.toInteger(),subjectId,NdlaSite.DELING)
        }
        else if(site == "FYR"){
            topics = adminTopicService.getTopicsBySubjectMatter(limit.toInteger(), offset.toInteger(),subjectId,NdlaSite.NYGIV)
        }
        def slurper = new JsonSlurper()
        def subjectTopicsObject = slurper.parseText(topics.toString())


        JSONArray jsonTopics = new JSONArray()
        JSONObject jsonTopic


        subjectTopicsObject.relations.each {rel ->

            def name = ""
            def topicId = ""
            def assocName = ""
            rel.associationTypeNames.each{associationTypeName ->
                if(associationTypeName["language"] == "http://psi.oasis-open.org/iso/639/#nob" && associationTypeName["role"]  == "http://psi.topic.ndla.no/#ndla-topic"){
                    assocName = associationTypeName["name"]
                }
                else if(associationTypeName["language"] == "http://psi.oasis-open.org/iso/639/#eng" && associationTypeName["role"]  == "http://psi.topic.ndla.no/#ndla-topic" && assocName == ""){
                    assocName = associationTypeName["name"]
                }
                else if(associationTypeName["language"] == "http://psi.topic.ndla.no/#language-neutral" && associationTypeName["role"]  == "http://psi.topic.ndla.no/#ndla-topic" && assocName == ""){
                    assocName = associationTypeName["name"]
                }

            }

            rel.relatedTopics.each{relatedTopic ->
                def map = [:]
                topicId = relatedTopic.topicId.toString().substring(relatedTopic.topicId.toString().lastIndexOf("#")+1)
                relatedTopic.topicNames.each{topicName ->
                    if(topicName["http://psi.oasis-open.org/iso/639/#nob"] != null){
                        name = topicName["http://psi.oasis-open.org/iso/639/#nob"]
                    }
                    else if(topicName["http://psi.oasis-open.org/iso/639/#eng"] != null && name == ""){
                        name = topicName["http://psi.oasis-open.org/iso/639/#eng"]
                    }
                    else if(topicName["http://psi.topic.ndla.no/#language-neutral"] != null && name == ""){
                        map.put("name",topicName["http://psi.topic.ndla.no/#language-neutral"])
                    }

                }

                jsonTopic = new JSONObject();
                jsonTopic.put("identifier",topicId)
                jsonTopic.put("associationTypeName",assocName)
                jsonTopic.put("name",name)
                if(relatedTopic.approved != ""){
                    jsonTopic.put("approved",relatedTopic.approved)
                    if(relatedTopic.approval_date != ""){
                        def approval_date = relatedTopic.approval_date.toString()
                        if(approval_date.contains(".")){
                            approval_date = approval_date.substring(0,approval_date.lastIndexOf("."))
                        }

                        jsonTopic.put("approval_date",approval_date)
                    }
                }

                def nodes = null
                if (site == "NDLA") {
                    nodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.NDLA)
                }

                if (site =="DELING") {
                    nodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.DELING)
                }

                if (site == "NYGIV") {
                    nodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.NYGIV)
                }
                jsonTopic.put("nodeCount",nodes.size())
                jsonTopics.put(jsonTopic)
            }

        }

        render jsonTopics.toString()
    }

    def getTopicByTopicId(){
        def topicId = params.topicId
        def topic = adminTopicService.getTopicByTopicId(topicId);
        JSONObject jsonTopic = new JSONObject();
        jsonTopic.put("identifier",topic.getIdentifier())
        jsonTopic.put("name",topic.getNames().get("http://psi.oasis-open.org/iso/639/#eng"))
        jsonTopic.put("psis",topic.getPsis())
        render jsonTopic.toString()
    }

    def searchSubjectTopicsByName(){
        def search = params.search
        def subjectId = params.subjectId
        def site = params.site
        def result = ""
        if(site == "NDLA"){
            result = adminTopicService.getTopicsBySubjectMatterAndNameString(search,subjectId,NdlaSite.NDLA)
        }
        else if(site == "DELING"){
            result = adminTopicService.getTopicsBySubjectMatterAndNameString(search,subjectId,NdlaSite.DELING)
        }
        else if(site == "NYGIV"){
            result = adminTopicService.getTopicsBySubjectMatterAndNameString(search,subjectId,NdlaSite.NYGIV)
        }

        def slurper = new JsonSlurper()
        def subjectTopicsObject = slurper.parseText(result.toString())


        JSONArray jsonTopics = new JSONArray()
        JSONObject jsonTopic


        subjectTopicsObject.relations.each {rel ->

            def name = ""
            def topicId = ""
            def assocName = ""
            rel.associationTypeNames.each{associationTypeName ->
                if(associationTypeName["language"] == "http://psi.oasis-open.org/iso/639/#nob" && associationTypeName["role"]  == "http://psi.topic.ndla.no/#ndla-topic"){
                    assocName = associationTypeName["name"]
                }
                else if(associationTypeName["language"] == "http://psi.oasis-open.org/iso/639/#eng" && associationTypeName["role"]  == "http://psi.topic.ndla.no/#ndla-topic" && assocName == ""){
                    assocName = associationTypeName["name"]
                }
                else if(associationTypeName["language"] == "http://psi.topic.ndla.no/#language-neutral" && associationTypeName["role"]  == "http://psi.topic.ndla.no/#ndla-topic" && assocName == ""){
                    assocName = associationTypeName["name"]
                }

            }

            rel.relatedTopics.each{relatedTopic ->
                def map = [:]
                topicId = relatedTopic.topicId.toString().substring(relatedTopic.topicId.toString().lastIndexOf("#")+1)
                relatedTopic.topicNames.each{topicName ->
                    if(topicName["http://psi.oasis-open.org/iso/639/#nob"] != null){
                        name = topicName["http://psi.oasis-open.org/iso/639/#nob"]
                    }
                    else if(topicName["http://psi.oasis-open.org/iso/639/#eng"] != null && name == ""){
                        name = topicName["http://psi.oasis-open.org/iso/639/#eng"]
                    }
                    else if(topicName["http://psi.topic.ndla.no/#language-neutral"] != null && name == ""){
                        map.put("name",topicName["http://psi.topic.ndla.no/#language-neutral"])
                    }

                }

                jsonTopic = new JSONObject();
                jsonTopic.put("identifier",topicId)
                jsonTopic.put("associationTypeName",assocName)
                jsonTopic.put("name",name)
                if(relatedTopic.approved != ""){
                    jsonTopic.put("approved",relatedTopic.approved)
                    if(relatedTopic.approval_date != ""){
                        def approval_date = relatedTopic.approval_date.toString()
                        if(approval_date.contains(".")){
                            approval_date = approval_date.substring(0,approval_date.lastIndexOf("."))
                        }

                        jsonTopic.put("approval_date",approval_date)
                    }
                }

                def nodes = null
                if (site == "NDLA") {
                    nodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.NDLA)
                }

                if (site =="DELING") {
                    nodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.DELING)
                }

                if (site == "NYGIV") {
                    nodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.NYGIV)
                }
                jsonTopic.put("nodeCount",nodes.size())
                jsonTopics.put(jsonTopic)
            }

        }
        render jsonTopics.toString()
    }


    def getDuplicateTopics(){
        def offset = params.offset
        def type = params.type
        def duplicates = adminTopicService.getDuplicateTopics(10,Integer.parseInt(offset),"http://psi.topic.ndla.no/#"+type)
        render duplicates
    }


    def mergeDuplicateTopics(){

        JSONArray message = adminTopicService.mergeDuplicateTopics("");
    }

    def searchTopicsByName(){
        def search = params.search
        def limit =  params.limit
        def offset = params.offset
        def type = params.type
        def searchType = params.searchType
        def language = params.language


        def result = adminTopicService.lookUpTopicsBySearchTerm(search,type,searchType,"http://psi.oasis-open.org/iso/639/#"+language, Integer.parseInt(limit),Integer.parseInt(offset))

        JSONArray jsonTopics = new JSONArray()
        if(null != result){
            def slurper = new JsonSlurper()

            def topicObjects = slurper.parseText(result.toString())
            def counter = 0;

            topicObjects.topics.each {topic ->
                    JSONObject jsonTopic = new JSONObject();
                    def name = ""
                    def topicId = ""
                    topic.names.each{topicName ->
                        if(topicName["language"] == "http://psi.oasis-open.org/iso/639/#nob"){
                            name = topicName["name"]
                        }
                        else if(topicName["language"] == "http://psi.oasis-open.org/iso/639/#eng" && name == ""){
                            name = topicName["name"]
                        }
                        else if(topicName["language"] == "http://psi.topic.ndla.no/#language-neutral" && name == ""){
                            name = topicName["name"]
                        }

                    }

                    topicId = topic.topicId.toString().substring(topic.topicId.toString().lastIndexOf("#")+1)


                    jsonTopic.put("id",topicId)
                    jsonTopic.put("name",name)
                    jsonTopic.put("processState",topic.processState.toString())

                    if(topic.approved != ""){
                        jsonTopic.put("approved",topic.approved)
                        if(topic.approval_date != ""){
                            def approval_date = topic.approval_date.toString()
                            if(approval_date.contains(".")){
                                approval_date = approval_date.substring(0,approval_date.lastIndexOf("."))
                            }

                            jsonTopic.put("approval_date",approval_date)
                        }
                    }

                    def ndlaNodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.NDLA)
                    def delingNodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.DELING)
                    def nygivNodes = adminTopicService.getNodesByTopicId(100, 0,topicId,NdlaSite.NYGIV)
                    def nodeCount = (ndlaNodes.size().toInteger() + delingNodes.size().toInteger() + nygivNodes.size().toInteger())
                    jsonTopic.put("nodeCount",nodeCount)
                    jsonTopics.put(jsonTopic)
                counter++
            }
        }


        render jsonTopics.toString()
    }

    def createTopic(){
        def payload = params.payload
        def data = adminTopicService.createTopic(payload);
        JSONArray jsonData = new JSONArray();
        data.each{key,topic ->
            jsonData.put(key)
        }
        render jsonData.toString()
    }

    def getNdlaNodeContent() {
        def http = new HTTPBuilder('http://ndla.no/node/')
        def nodeIdentifier = params.nodeIdentifier

        http.request(GET, TEXT) {
            uri.path = nodeIdentifier

            response.success = { resp, reader ->
                render reader.text
            }

            // Response handler for any failure status codes.
            response.failure = { resp ->
                render "Error"
            }
        }
    }

    def getLanguages() {
        ArrayList<NdlaTopic> languages = adminTopicService.getLanguages()
        def languageMap = [:]

        for(NdlaTopic language: languages) {
            def names = language.getNames();
            def languageName = ""
            names.each{lang, name ->
                if(lang == "http://psi.oasis-open.org/iso/639/#nob"){
                    languageName = name;
                }
                else if(lang == "http://psi.oasis-open.org/iso/639/#eng" && languageName == ""){
                    languageName = name;
                }
                else if(lang == "http://psi.topic.ndla.no/#language-neutral" && languageName == ""){
                    languageName = name;
                }
            }

            languageMap.put(language.getIdentifier(),languageName)
        }
    }

    def getLanguage(){
        def  languageId = params.languageId
        NdlaTopic language = null;
        try {
            language = adminTopicService.getLanguage(languageId)
            return language;
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def getISO639Content() {
        File myFile = grailsApplication.mainContext.getResource("isolangs.json").file
        String fileContents = myFile.getText('UTF-8')
        JSONArray jData = new JSONArray(fileContents);
        def html = "<table><tr><th>Language</th></tr>";
        for(int i = 0; i < jData.length();i++) {
            JSONObject lang = jData.get(i);
            String langString = lang.getString("639-3");

            if(langString.contains("+")){
                langString = langString.substring(0,langString.lastIndexOf("+"));
            }
            html += "<tr><td><a href=\"javascript:\" onclick=\"setIsoCode('http://psi.oasis-open.org/iso/639/#"+langString+"')\">"+lang.getString("language-name")+"</a></td></tr>";
        }
        html += "<table>";

        render html
    }

    def getAssociationCount() {
        def psiUrl = params.psiUrl
        def psiId = URLDecoder.decode(params.psiId,"UTF-8")

        def role1Id = params.role1Id
        def role2Id = params.role2Id
        def name = params.name
        def uri = ""

        if(psiId.contains("§")) {
            uri = "http://"+psiUrl.toString().replaceAll("_","/")+psiId.replace("§","#")
        }
        else{
            uri = "http://"+psiUrl.toString().replaceAll("_","/")+"/"+psiId
        }

        def data = adminTopicService.getTopicPlayersByRelationTypeId(uri,role1Id,role2Id)
        def assocCount = data.get(role1Id).size()
        def html = '<div id="assocCount">There are '+assocCount+' relations created with the '+name+' relationship type</div>';
        render html
    }

    def getAssociationTypes(){
        ArrayList<NdlaTopic> hierarchicalTopicTypes = adminTopicService.getHierarchicalAssociationTypes()
        ArrayList<NdlaTopic> horizontalTopicTypes = adminTopicService.getHorizontalAssociationTypes()

        JSONObject types = new JSONObject()
        JSONArray hierarchicalTypes = new JSONArray()
        JSONArray horizontalTypes = new JSONArray()

        hierarchicalTopicTypes.each { hierarchicalTypeTopic ->

            JSONObject tempHierarchicalType = new JSONObject()
            tempHierarchicalType.put("psi",hierarchicalTypeTopic.getPsi())

            def names = hierarchicalTypeTopic.getRelationshipNames()
            def isoNames = []
            names.each { key, map ->
                map.each{ mapKey, val ->
                    if(mapKey == "http://psi.oasis-open.org/iso/639/#eng"){
                        isoNames.add(val)
                    }
                }
            }
            def name = isoNames.join(" / ")
            if(name.toString().length() > 0) {
                tempHierarchicalType.put("name",name)
            }

            hierarchicalTypes.put(tempHierarchicalType)
        }

        horizontalTopicTypes.each { horizontalTypeTopic ->

            JSONObject tempHorizontalType = new JSONObject()
            tempHorizontalType.put("psi",horizontalTypeTopic.getPsi())

            def names = horizontalTypeTopic.getRelationshipNames()
            def isoNames = []
            names.each { key, map ->
                map.each{ mapKey, val ->
                    if(mapKey == "http://psi.oasis-open.org/iso/639/#eng"){
                        isoNames.add(val)
                    }
                }
            }
            def name = isoNames.join(" / ")
            if(name.toString().length() > 0) {
                tempHorizontalType.put("name", name)
            }

            horizontalTypes.put(tempHorizontalType)
        }
        types.put("hierarchical",hierarchicalTypes)
        types.put("horizontal",horizontalTypes)

        render types.toString()
    }

    def getAssociationRoles(){
        def psiId = params.psiId

        def associationType = params.associationType
        NdlaTopic associationTopic


        if(psiId.contains("§")) {
            psiId = psiId.replace("§", "#")
        }


        if(associationType == "hierarchical"){
            associationTopic = adminTopicService.getHierarchicalAssociationTypeById(psiId)
        }
        else{
            associationTopic = adminTopicService.getHorizontalAssociationTypeById(psiId)
        }

        def names = associationTopic.getRelationshipNames()
        def roleNames = associationTopic.getRelationshipRoleNames()

        JSONArray roles = new JSONArray()
        def found = []
        names.each { key, map ->
            def roleMap = roleNames.get(key)
            JSONObject role = new JSONObject()
            if(map.containsKey('http://psi.oasis-open.org/iso/639/#eng')){

                map.each{ mapKey, val ->
                    def roleName = ""
                    role.put("roleName",map['http://psi.oasis-open.org/iso/639/#eng'])
                    if (roleMap.get(mapKey + "_http://psi.oasis-open.org/iso/639/#eng") != "") {
                        role.put("roleId", mapKey)
                        roleName = roleMap.get(mapKey + "_http://psi.oasis-open.org/iso/639/#eng")
                        if(!found.contains(map['http://psi.oasis-open.org/iso/639/#eng'])) {
                            role.put("roleMap", roleName)
                            roles.put(role)
                        }
                    }
                    found.add(map['http://psi.oasis-open.org/iso/639/#eng'])
                }
            }
        }
        render roles.toString()
    }

    def getAimTopicAssociations() {
        def aimId = params.aimId
        def lang = "http://psi.oasis-open.org/iso/639/#"+params.lang
        def result = adminTopicService.getAimTopicAssociations(aimId,lang)
        render result
    }


    def getSubjectOntologyTopicRelations(){
        def assocType = params.assocType
        def subjectId = params.subjectId
        def site = params.site
        def relations = [:]
        def roleNameMap = [:]
        def topics = [:]
        def result = null;
        def foundAssocs = []

        if(site == "NDLA"){
            result = adminTopicService.getSubjectOntologyTopicRelations(subjectId,NdlaSite.NDLA)
        }
        else if(site == "DELING"){
            result = adminTopicService.getSubjectOntologyTopicRelations(subjectId,NdlaSite.DELING)
        }
        else if(site == "FYR"){
            result = adminTopicService.getSubjectOntologyTopicRelations(subjectId,NdlaSite.NYGIV)
        }


        //JSONObject data = new JSONObject(result)

        JSONArray associations = result.getJSONArray("associations")
        JSONArray roleNameMapping = result.getJSONArray("roleNameMapping")

        for(int i = 0; i < associations.length(); i++){
            JSONObject topic = associations.getJSONObject(i);
            def topic1Name = "";
            def topic1Id = "";
            def topicIdString = topic.getString("topicId");
            topic1Id = topicIdString.toString().substring(topicIdString.lastIndexOf("#")+1);
            JSONArray topic1Names = topic.getJSONArray("topicNames");
            for(int l = 0; l < topic1Names.length(); l++){
                JSONObject topic1NameObject = topic1Names.getJSONObject(l);
                Iterator it = topic1NameObject.keys().iterator();
                while(it.hasNext()) {
                    def langKey = (String) it.next();
                    if(langKey.equals("http://psi.oasis-open.org/iso/639/#nob")){
                        topic1Name = topic1NameObject.get(langKey)
                        break;
                    }
                }
            }//end for l

            JSONArray topicRelations = topic.getJSONArray("relations");
            for(int j = 0; j < topicRelations.length(); j++){
                JSONObject topicRelation = topicRelations.getJSONObject(j);
                def assocId = topicRelation.get("associationId")
                def currentAssocType = topicRelation.get("associationType")
                def currentReifierId = topicRelation.get("reifierId")
                if(currentAssocType.toString().contains(assocType)){
                    if(!relations.containsKey(assocId)) {
                        relations[assocId] = [:]
                        topics[assocId] = []
                    }
                    relations[assocId]["relationId"] = assocId
                    JSONArray assocNames = topicRelation.getJSONArray("associationTypeNames")
                    int foundLang = 0;
                    def relationName = "";
                    for(int k = 0; k < assocNames.length(); k++){
                        JSONObject assocName = assocNames.getJSONObject(k);
                        if(assocName.get("language").toString().equals("http://psi.oasis-open.org/iso/639/#eng")){
                            if(foundLang == 0){
                                relationName += assocName.getString("name")+"/"
                            }
                            else if(foundLang == 1){
                                relationName += assocName.getString("name")
                            }
                            foundLang++;
                        }
                    }//end for
                    relations[assocId]["relationName"] = relationName;

                    def topicObject = [:]
                    def topic2Name = ""
                    JSONArray topic2Names = topicRelation.getJSONArray("topicNames");
                    for(int l = 0; l < topic2Names.length(); l++){
                        JSONObject topic2NameObject = topic2Names.getJSONObject(l);
                        Iterator it2 = topic2NameObject.keys().iterator();
                        while(it2.hasNext()) {
                            def langKey2 = (String) it2.next();
                            if(langKey2.equals("http://psi.oasis-open.org/iso/639/#nob")){
                                topic2Name = topic2NameObject.get(langKey2)
                                break;
                            }
                        }
                    }//end for l
                    def topic2Id = "";
                    def topic2IdString = topicRelation.getString("topicId");
                    topic2Id = topic2IdString.toString().substring(topic2IdString.lastIndexOf("#")+1);
                    topicObject['ids'] = topic1Id+"_"+topic2Id;
                    topicObject['assocType'] = currentAssocType;
                    topicObject['reifierId'] = currentReifierId;
                    topicObject['names'] = topic1Name+"/"+topic2Name;


                    def roles = [];
                    def roleObject1 = [:]
                    roleObject1['topicId'] = topic1Id
                    roleObject1['roleName'] = topicRelation.getString("rolePlayedByTopic")
                    roles.add(roleObject1);
                    def roleObject2 = [:];
                    roleObject2['topicId'] = topic2Id;
                    roleObject2['roleName'] = topicRelation.getString("rolePlayedByOtherTopic");
                    roles.add(roleObject2)
                    topicObject['roles'] = roles;
                    if(!foundAssocs.contains(assocId+"_"+topic1Id+"_"+topic2Id)){
                        topics[assocId].add(topicObject)
                        foundAssocs.add(assocId+"_"+topic1Id+"_"+topic2Id)
                    }


                }//end if
            }//end for j
        }// end for i

        topics.each{topicRelationTypeKey, topicArray ->
            relations[topicRelationTypeKey]["topics"] = topicArray
        }


        JSONArray relationArray = new JSONArray();

        relations.each{ relationTypeKey,relationData ->
            JSONObject currentRelationObject = new JSONObject();
            currentRelationObject.put("relationId",relationTypeKey);
            currentRelationObject.put("relationName",relationData.relationName);

            JSONArray currentRelationObjectTopicsArray = new JSONArray();
            relationData.topics.each{currentTopicObject ->
                JSONObject topicJSONObject = new JSONObject();
                topicJSONObject.put("ids",currentTopicObject.ids);
                topicJSONObject.put("assocType",currentTopicObject.assocType);
                topicJSONObject.put("reifierId",currentTopicObject.reifierId);
                topicJSONObject.put("names",currentTopicObject.names);
                JSONArray roleArray = new JSONArray();
                currentTopicObject.roles.each{ currenRoleObject ->
                    JSONObject roleJSONObject = new JSONObject();
                    roleJSONObject.put("topicId",currenRoleObject.topicId);
                    roleJSONObject.put("roleName",currenRoleObject.roleName);
                    roleArray.put(roleJSONObject);
                }
                topicJSONObject.put("roles",roleArray);
                currentRelationObjectTopicsArray.put(topicJSONObject);
            }
            currentRelationObject.put("topics",currentRelationObjectTopicsArray);
            relationArray.put(currentRelationObject);
        }
        JSONObject resultObject = new JSONObject()
        resultObject.put("roleNameMapping",roleNameMapping)
        resultObject.put("topics",relationArray)

        render resultObject.toString();
    }

    def getSubjectCurriculaOntologyRelations(){
        def assocType = params.assocType
        def subjectId = params.subjectId
        def site = params.site
        def relations = [:]
        def roleNameMap = [:]
        def topics = [:]
        def result = null;
        def foundAssocs = []

        if(site == "NDLA"){
            result = adminTopicService.getCurriculaOntologyTopics(subjectId,NdlaSite.NDLA)
        }
        else if(site == "DELING"){
            result = adminTopicService.getCurriculaOntologyTopics(subjectId,NdlaSite.DELING)
        }
        else if(site == "FYR"){
            result = adminTopicService.getCurriculaOntologyTopics(subjectId,NdlaSite.NYGIV)
        }

        //JSONObject data = new JSONObject(result)

        JSONArray associations = result.getJSONArray("associations")
        JSONArray roleNameMapping = result.getJSONArray("roleNameMapping")

        for(int i = 0; i < associations.length(); i++){
            JSONObject topic = associations.getJSONObject(i);
            def topic1Name = "";
            def topic1Id = "";
            def topicIdString = topic.getString("topicId");
            topic1Id = topicIdString.toString().substring(topicIdString.lastIndexOf("#")+1);
            JSONArray topic1Names = topic.getJSONArray("topicNames");
            for(int l = 0; l < topic1Names.length(); l++){
                JSONObject topic1NameObject = topic1Names.getJSONObject(l);
                Iterator it = topic1NameObject.keys().iterator();
                while(it.hasNext()) {
                    def langKey = (String) it.next();
                    if(langKey.equals("http://psi.oasis-open.org/iso/639/#nob")){
                        topic1Name = topic1NameObject.get(langKey)
                        break;
                    }
                }
            }//end for l

            JSONArray topicRelations = topic.getJSONArray("relations");
            for(int j = 0; j < topicRelations.length(); j++){
                JSONObject topicRelation = topicRelations.getJSONObject(j);
                def assocId = topicRelation.get("associationId")
                def currentAssocType = topicRelation.get("associationType")
                def currentReifierId = topicRelation.get("reifierId")
                if(currentAssocType.toString().contains(assocType)){
                    if(!relations.containsKey(assocId)) {
                        relations[assocId] = [:]
                        topics[assocId] = []
                    }
                    relations[assocId]["relationId"] = assocId
                    JSONArray assocNames = topicRelation.getJSONArray("associationTypeNames")
                    int foundLang = 0;
                    def relationName = "";
                    for(int k = 0; k < assocNames.length(); k++){
                        JSONObject assocName = assocNames.getJSONObject(k);
                        if(assocName.get("language").toString().equals("http://psi.oasis-open.org/iso/639/#eng")){
                            if(foundLang == 0){
                                relationName += assocName.getString("name")+"/"
                            }
                            else if(foundLang == 1){
                                relationName += assocName.getString("name")
                            }
                            foundLang++;
                        }
                    }//end for
                    relations[assocId]["relationName"] = relationName;

                    def topicObject = [:]
                    def topic2Name = ""
                    JSONArray topic2Names = topicRelation.getJSONArray("topicNames");
                    for(int l = 0; l < topic2Names.length(); l++){
                        JSONObject topic2NameObject = topic2Names.getJSONObject(l);
                        Iterator it2 = topic2NameObject.keys().iterator();
                        while(it2.hasNext()) {
                            def langKey2 = (String) it2.next();
                            if(langKey2.equals("http://psi.oasis-open.org/iso/639/#nob")){
                                topic2Name = topic2NameObject.get(langKey2)
                                break;
                            }
                        }
                    }//end for l
                    def topic2Id = "";
                    def topic2IdString = topicRelation.getString("topicId");
                    topic2Id = topic2IdString.toString().substring(topic2IdString.lastIndexOf("#")+1);
                    topicObject['ids'] = topic1Id+"_"+topic2Id;
                    topicObject['assocType'] = currentAssocType;
                    topicObject['reifierId'] = currentReifierId;
                    topicObject['names'] = topic1Name+"/"+topic2Name;
                    def roles = [];
                    def roleObject1 = [:]
                    roleObject1['topicId'] = topic1Id
                    roleObject1['roleName'] = topicRelation.getString("rolePlayedByTopic")
                    roles.add(roleObject1);
                    def roleObject2 = [:];
                    roleObject2['topicId'] = topic2Id;
                    roleObject2['roleName'] = topicRelation.getString("rolePlayedByOtherTopic");
                    roles.add(roleObject2)
                    topicObject['roles'] = roles;
                    if(!foundAssocs.contains(assocId+"_"+topic1Id+"_"+topic2Id)){
                        topics[assocId].add(topicObject)
                        foundAssocs.add(assocId+"_"+topic1Id+"_"+topic2Id)
                    }


                }//end if
            }//end for j
        }// end for i

        topics.each{topicRelationTypeKey, topicArray ->
            relations[topicRelationTypeKey]["topics"] = topicArray
        }


        JSONArray relationArray = new JSONArray();

        relations.each{ relationTypeKey,relationData ->
            JSONObject currentRelationObject = new JSONObject();
            currentRelationObject.put("relationId",relationTypeKey);
            currentRelationObject.put("relationName",relationData.relationName);

            JSONArray currentRelationObjectTopicsArray = new JSONArray();
            relationData.topics.each{currentTopicObject ->
                JSONObject topicJSONObject = new JSONObject();
                topicJSONObject.put("ids",currentTopicObject.ids);
                topicJSONObject.put("assocType",currentTopicObject.assocType);
                topicJSONObject.put("reifierId",currentTopicObject.reifierId);
                topicJSONObject.put("names",currentTopicObject.names);
                JSONArray roleArray = new JSONArray();
                currentTopicObject.roles.each{ currenRoleObject ->
                    JSONObject roleJSONObject = new JSONObject();
                    roleJSONObject.put("topicId",currenRoleObject.topicId);
                    roleJSONObject.put("roleName",currenRoleObject.roleName);
                    roleArray.put(roleJSONObject);
                }
                topicJSONObject.put("roles",roleArray);
                currentRelationObjectTopicsArray.put(topicJSONObject);
            }
            currentRelationObject.put("topics",currentRelationObjectTopicsArray);
            relationArray.put(currentRelationObject);
        }
        JSONObject resultObject = new JSONObject()
        resultObject.put("roleNameMapping",roleNameMapping)
        resultObject.put("topics",relationArray)

        render resultObject.toString();
    }

    def getSubjectOntologyTopicRelations2(){
        def assocType = params.assocType
        def subjectId = params.subjectId
        def site = params.site
        def relations = [:]
        def topics = [:]
        def result = null;
        def foundAssocs = []

        if(site == "NDLA"){
            result = adminTopicService.getSubjectOntologyTopicRelations(subjectId,NdlaSite.NDLA)
        }
        else if(site == "DELING"){
            result = adminTopicService.getSubjectOntologyTopicRelations(subjectId,NdlaSite.DELING)
        }
        else if(site == "FYR"){
            result = adminTopicService.getSubjectOntologyTopicRelations(subjectId,NdlaSite.NYGIV)
        }

        //JSONObject data = new JSONObject(result)
        //System.out.println(result)
        JSONArray associations = result.getJSONArray("associations")

        for(int i = 0; i < associations.length(); i++){
            JSONObject topic = associations.getJSONObject(i);
            def topic1Name = "";
            def topic1Id = "";
            def topicIdString = topic.getString("topicId");
            topic1Id = topicIdString.toString().substring(topicIdString.lastIndexOf("#")+1);
            JSONArray topic1Names = topic.getJSONArray("topicNames");
            for(int l = 0; l < topic1Names.length(); l++){
                JSONObject topic1NameObject = topic1Names.getJSONObject(l);
                Iterator it = topic1NameObject.keys().iterator();
                while(it.hasNext()) {
                    def langKey = (String) it.next();
                    if(langKey.equals("http://psi.oasis-open.org/iso/639/#nob")){
                        topic1Name = topic1NameObject.get(langKey)
                        break;
                    }
                }
            }//end for l

            JSONArray topicRelations = topic.getJSONArray("relations");
            for(int j = 0; j < topicRelations.length(); j++){
                JSONObject topicRelation = topicRelations.getJSONObject(j);
                def assocId = topicRelation.get("associationId")
                def currentAssocType = topicRelation.get("associationType")
                if(currentAssocType.toString().contains(assocType)){
                    if(!relations.containsKey(assocId)) {
                        relations[assocId] = [:]
                        topics[assocId] = []
                    }
                    relations[assocId]["relationId"] = assocId
                    JSONArray assocNames = topicRelation.getJSONArray("associationTypeNames")
                    int foundLang = 0;
                    def relationName = "";
                    for(int k = 0; k < assocNames.length(); k++){
                        JSONObject assocName = assocNames.getJSONObject(k);
                        if(assocName.get("language").toString().equals("http://psi.oasis-open.org/iso/639/#eng")){
                            if(foundLang == 0){
                                relationName += assocName.getString("name")+"/"
                            }
                            else if(foundLang == 1){
                                relationName += assocName.getString("name")
                            }
                            foundLang++;
                        }
                    }//end for
                    relations[assocId]["relationName"] = relationName;

                    def topicObject = [:]
                    def topic2Name = ""
                    JSONArray topic2Names = topicRelation.getJSONArray("topicNames");
                    for(int l = 0; l < topic2Names.length(); l++){
                        JSONObject topic2NameObject = topic2Names.getJSONObject(l);
                        Iterator it2 = topic2NameObject.keys().iterator();
                        while(it2.hasNext()) {
                            def langKey2 = (String) it2.next();
                            if(langKey2.equals("http://psi.oasis-open.org/iso/639/#nob")){
                                topic2Name = topic2NameObject.get(langKey2)
                                break;
                            }
                        }
                    }//end for l
                    def topic2Id = "";
                    def topic2IdString = topicRelation.getString("topicId");
                    topic2Id = topic2IdString.toString().substring(topic2IdString.lastIndexOf("#")+1);
                    topicObject['ids'] = topic1Id+"_"+topic2Id;
                    topicObject['names'] = topic1Name+"/"+topic2Name;
                    def roles = [];
                    def roleObject1 = [:]
                    roleObject1['topicId'] = topic1Id
                    roleObject1['roleName'] = topicRelation.getString("rolePlayedByTopic")
                    roles.add(roleObject1);
                    def roleObject2 = [:];
                    roleObject2['topicId'] = topic2Id;
                    roleObject2['roleName'] = topicRelation.getString("rolePlayedByOtherTopic");
                    roles.add(roleObject2)
                    topicObject['roles'] = roles;
                    if(!foundAssocs.contains(assocId+"_"+topic1Id+"_"+topic2Id)){
                        topics[assocId].add(topicObject)
                        foundAssocs.add(assocId+"_"+topic1Id+"_"+topic2Id)
                    }


                }//end if
            }//end for j
        }// end for i

        topics.each{topicRelationTypeKey, topicArray ->
            relations[topicRelationTypeKey]["topics"] = topicArray
        }


        JSONArray relationArray = new JSONArray();

        relations.each{ relationTypeKey,relationData ->
            JSONObject currentRelationObject = new JSONObject();
            currentRelationObject.put("relationId",relationTypeKey);
            currentRelationObject.put("relationName",relationData.relationName);

            JSONArray currentRelationObjectTopicsArray = new JSONArray();
            relationData.topics.each{currentTopicObject ->
                JSONObject topicJSONObject = new JSONObject();
                topicJSONObject.put("ids",currentTopicObject.ids);
                topicJSONObject.put("names",currentTopicObject.names);
                JSONArray roleArray = new JSONArray();
                currentTopicObject.roles.each{ currenRoleObject ->
                    JSONObject roleJSONObject = new JSONObject();
                    roleJSONObject.put("topicId",currenRoleObject.topicId);
                    roleJSONObject.put("roleName",currenRoleObject.roleName);
                    roleArray.put(roleJSONObject);
                }
                topicJSONObject.put("roles",roleArray);
                currentRelationObjectTopicsArray.put(topicJSONObject);
            }
            currentRelationObject.put("topics",currentRelationObjectTopicsArray);
            relationArray.put(currentRelationObject);
        }

        render relationArray.toString();
    }

    def removeTopicsByAimId() {
        def topicId = params.topicId
        def relatedTopicId = params.relatedTopicId
        def relationType = params.relationType
        def site = params.site

        JSONObject jsonPayloadTopicHasAim = new JSONObject()
        jsonPayloadTopicHasAim.put("topicId",topicId)
        jsonPayloadTopicHasAim.put("topicType","kompetansemaal")
        JSONArray relatedTopicIds = new JSONArray()
        JSONObject relatedTopic = new JSONObject()
        relatedTopic.put("relatedTopicId",relatedTopicId)
        relatedTopic.put("relatedTopicType","topic")
        relatedTopicIds.put(relatedTopic)
        jsonPayloadTopicHasAim.put("related_topicIds",relatedTopicIds)
        jsonPayloadTopicHasAim.put("relationType",relationType)


        def result = null
        if(site == "NDLA"){
            result = adminTopicService.removeTopicsByAimId(jsonPayloadTopicHasAim.toString(),NdlaSite.NDLA)
        }
        else if(site == "DELING"){
            result = adminTopicService.removeTopicsByAimId(jsonPayloadTopicHasAim.toString(),NdlaSite.DELING)
        }
        else if(site == "FYR"){
            result = adminTopicService.removeTopicsByAimId(jsonPayloadTopicHasAim.toString(),NdlaSite.NYGIV)
        }

        JSONArray resultData = new JSONArray()
        result.each{key,val ->
            resultData.put(key)
        }

        //delete primary-topic-association
        JSONObject jsonPayloadPrimaryTopic = new JSONObject()
        jsonPayloadPrimaryTopic.put("topicId",topicId)
        jsonPayloadPrimaryTopic.put("topicType","kompetansemaal")
        jsonPayloadPrimaryTopic.put("related_topicIds",relatedTopicIds)
        jsonPayloadPrimaryTopic.put("relationType","primary-topic")

        def primaryResult = null
        if(site == "NDLA"){
            primaryResult = adminTopicService.removeTopicsByAimId(jsonPayloadPrimaryTopic.toString(),NdlaSite.NDLA)
        }
        else if(site == "DELING"){
            primaryResult = adminTopicService.removeTopicsByAimId(jsonPayloadPrimaryTopic.toString(),NdlaSite.DELING)
        }
        else if(site == "FYR"){
            primaryResult = adminTopicService.removeTopicsByAimId(jsonPayloadPrimaryTopic.toString(),NdlaSite.NYGIV)
        }


        primaryResult.each{key,val ->
            resultData.put(key)
        }




        render resultData.toString()
    }


    def getRelationRoleTypes(){
        JSONArray subordinateRelationRoleTypes = adminTopicService.getSubordinateRoleTypes()
        JSONArray superordinateRelationRoleTypes = adminTopicService.getSuperordinateRoleTypes()
        JSONArray JSONRoleTypes = new JSONArray();
        def lastType = ""
        JSONArray tempAssocs = new JSONArray();
        JSONObject JSONRelationRole = null;

        for(int i = 0; i < subordinateRelationRoleTypes.length(); i++) {
            JSONObject subordinateRelationRoleType = subordinateRelationRoleTypes.get(i);
            def roleIdentifier = subordinateRelationRoleType.getString("roleIdentifier");
            def assocIdentifier = subordinateRelationRoleType.getString("assocIdentifier");
            def relationCount = subordinateRelationRoleType.getString("relationCount");
            JSONArray names = subordinateRelationRoleType.getJSONArray("roleNames");
            def subordinateRoleTitle = ""
            for(int j = 0; j < names.length(); j++) {
                JSONObject subordinateRoleName = names.get(j);
                if(subordinateRoleName["language"] == "http://psi.oasis-open.org/iso/639/#nob"){
                    subordinateRoleTitle = subordinateRoleName["name"]
                }
                else if(subordinateRoleName["language"] == "http://psi.oasis-open.org/iso/639/#eng" && subordinateRoleTitle == ""){
                    subordinateRoleTitle = subordinateRoleName["name"]
                }
                else if(subordinateRoleName["language"] == "http://psi.topic.ndla.no/#language-neutral" &&  subordinateRoleTitle == ""){
                    subordinateRoleTitle = subordinateRoleName["name"]
                }
            }

            def subordinateRoleTypeIdentifierString = "";
            JSONArray subordinateRoleTypes = subordinateRelationRoleType.getJSONArray("roleTypes");
            for(int k = 0 ; k < subordinateRoleTypes.length(); k++) {
                JSONObject subordinateRoleType = subordinateRoleTypes.get(k);
                subordinateRoleTypeIdentifierString = subordinateRoleType.get("roleTypeIdentifier")
            }

            JSONRelationRole = new JSONObject();
            JSONRelationRole.put("identifier",roleIdentifier);
            JSONRelationRole.put("type",subordinateRoleTypeIdentifierString);
            JSONRelationRole.put("typeName",assocIdentifier);
            JSONRelationRole.put("relationCount",relationCount);
            JSONRelationRole.put("name",subordinateRoleTitle);
            JSONRoleTypes.put(JSONRelationRole);
        }

        for(int i = 0; i < superordinateRelationRoleTypes.length(); i++) {
            JSONObject superordinateRelationRoleType = superordinateRelationRoleTypes.get(i);
            def superordinateRoleIdentifier = superordinateRelationRoleType.getString("roleIdentifier");
            def superordinateAssocIdentifier = superordinateRelationRoleType.getString("assocIdentifier");
            def superordinateRelationCount = superordinateRelationRoleType.getString("relationCount");
            JSONArray superordinateNames = superordinateRelationRoleType.getJSONArray("roleNames");
            def superordinateRoleTitle = ""
            for(int j = 0; j < superordinateNames.length(); j++) {
                JSONObject superordinateRoleName = superordinateNames.get(j);
                if(superordinateRoleName["language"] == "http://psi.oasis-open.org/iso/639/#nob"){
                    superordinateRoleTitle = superordinateRoleName["name"]
                }
                else if(superordinateRoleName["language"] == "http://psi.oasis-open.org/iso/639/#eng" && superordinateRoleTitle == ""){
                    superordinateRoleTitle = superordinateRoleName["name"]
                }
                else if(superordinateRoleName["language"] == "http://psi.topic.ndla.no/#language-neutral" &&  superordinateRoleTitle == ""){
                    superordinateRoleTitle = superordinateRoleName["name"]
                }
            }

            def superordinateRoleTypeIdentifierString = "";
            JSONArray superordinateRoleTypes = superordinateRelationRoleType.getJSONArray("roleTypes");
            for(int k = 0 ; k < superordinateRoleTypes.length(); k++) {
                JSONObject superordinateRoleType = superordinateRoleTypes.get(k);
                superordinateRoleTypeIdentifierString = superordinateRoleType.get("roleTypeIdentifier")
            }

            JSONRelationRole = new JSONObject();
            JSONRelationRole.put("identifier",superordinateRoleIdentifier);
            JSONRelationRole.put("type",superordinateRoleTypeIdentifierString);
            JSONRelationRole.put("typeName",superordinateAssocIdentifier);
            JSONRelationRole.put("relationCount",superordinateRelationCount);
            JSONRelationRole.put("name",superordinateRoleTitle);
            JSONRoleTypes.put(JSONRelationRole);
        }


        JSONRoleTypes.put(JSONRelationRole)
        render JSONRoleTypes.toString()
    }

    def removeAssociationByReifier(){
        def reifier = params.reifier
        def topic1 = params.topic1
        def topicType1 = params.topicType1
        def topic2 = params.topic2
        def topicType2 = params.topicType2
        def relationType = params.relationType
        def site = params.site

        JSONObject payload = new JSONObject()
        payload.put("topicId",topic1)
        payload.put("topicType",topicType1)

        JSONArray relatedTopics = new JSONArray()
        JSONObject relatedTopic = new JSONObject()
        relatedTopic.put("relatedTopicId",topic2)
        relatedTopic.put("relatedTopicType",topicType2)
        relatedTopics.put(relatedTopic)
        payload.put("related_topicIds",relatedTopics)

        payload.put("relationType",relationType)
        payload.put("reifierId","http://psi.topic.ndla.no/reifiers/#"+reifier)
        //System.out.println("PAYLOAD: "+payload)


        def result = null
        if(site == "NDLA"){
            result = adminTopicService.removeAssociationByReifier(payload.toString(),NdlaSite.NDLA)
        }
        else if(site == "DELING"){
            result = adminTopicService.removeAssociationByReifier(payload.toString(),NdlaSite.DELING)
        }
        else if(site == "FYR"){
            result = adminTopicService.removeAssociationByReifier(payload.toString(),NdlaSite.NYGIV)
        }

        JSONArray resultData = new JSONArray()
        result.each{val ->
            resultData.put(val)
        }
        render resultData.toString()
    }
}
