package no.ndla.topics.admin

import grails.plugins.springsecurity.Secured
import no.ndla.topics.service.NdlaServiceException
import no.ndla.topics.service.model.NdlaTopic
import org.codehaus.jettison.json.JSONArray
import org.codehaus.jettison.json.JSONObject

import javax.annotation.PostConstruct

@Secured(['ROLE_ADMIN','ROLE_ONTOLOGY_USER','ROLE_CURRICULAONTOLOGY_USER'])
class TopicController {
    def adminTopicService // Dependency Injection (DI) by the Spring container.

    @PostConstruct
    void init() {
        if(null == adminTopicService.getTopicService()){
            adminTopicService.setTopicService();
        }
    }

    def index() { }
    def addTopics() {
        def shortMap = [:]
        def langMap = [:]
        def missingLangs = [:]
        def wordClassMap = [:]

        ArrayList<NdlaTopic> wordClasses = adminTopicService.getWordClasses();
        wordClasses.each{wordClassItem ->
            def psi = wordClassItem.getPsi()
            def wordClassPsi = psi.substring(psi.lastIndexOf("#")+1)
            def name = wordClassItem.getNames().get("http://psi.oasis-open.org/iso/639/#eng")
            wordClassMap.put(wordClassPsi,name)
        }

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            if(!languageItem.psi.equals("http://psi.topic.ndla.no/#language-neutral") && !languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#eng")) {
                if(!languageItem.psi.equals("http://psi.oasis-open.org/iso/639/#nob")) {
                    missingLangs.put(languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
                }
            }
            langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1))
        }

        [langMap: langMap, shortMap: shortMap,missinglangs: missingLangs,wordClassMap: wordClassMap, searchDomain: grailsApplication.config.INDEX_SERVICE_URL]
    }

    def doAddTopics(){
        withForm {
            def addedTopics = params.list("hiddenTopics")
            def newTopicNames = []
            def newTopicDescriptions = []
            def newTopicImages = []
            def isoBaseUrl = "http://psi.oasis-open.org/iso/639/#";

            addedTopics.each { topic ->

                def tempNameData = [:]
                def tempDescriptionData = [:]
                def tempImageData = [:]
                def topicData = topic.toString().split("§")
                topicData.each { data ->
                    def nameData = data.toString().split(";")
                    if(nameData[0].contains("description_")){
                        def langId = nameData[0].split("_")
                        tempDescriptionData.put(isoBaseUrl+langId[1], nameData[1])
                    }
                    else if(nameData[0].contains("image_")){
                        def langId = nameData[0].split("_")
                        tempImageData.put(isoBaseUrl+langId[1], nameData[1])
                    }
                    else if(nameData[0].contains("wordClass")){
                        def wordClassTopicId = nameData[1]
                        tempNameData.put("wordClass", wordClassTopicId)
                    }
                    else{
                        tempNameData.put(isoBaseUrl+nameData[0], nameData[1])
                    }

                }

                newTopicNames.add(tempNameData)
                if(tempDescriptionData.size() > 0) {
                    newTopicDescriptions.add(tempDescriptionData)
                }

                if(tempImageData.size() > 0) {
                    newTopicImages.add(tempImageData)
                }
            }



            def newTopics = []

            if(newTopicNames.size() > 0) {
                def counter = 0;
                def WC = ""
                newTopicNames.each { nameObj ->
                    JSONArray topicJSONNames = new JSONArray();
                    JSONArray topicJSONDescriptions = new JSONArray();
                    JSONArray topicJSONImages = new JSONArray();
                    JSONArray topicJSONTypes = new JSONArray();
                    JSONObject newTopic = new JSONObject();

                    nameObj.each { lang, val ->
                        if(lang == "wordClass"){
                            WC = val;
                        }
                        else{
                            JSONObject nameTmp = new JSONObject()
                            nameTmp.put("language",lang)
                            nameTmp.put("name",val)
                            topicJSONNames.put(nameTmp)
                        }
                    }

                    newTopic.put("names",topicJSONNames);
                    newTopic.put("wordClass",WC);

                    if(newTopicDescriptions.size() > 0) {
                        def descObject = newTopicDescriptions[counter]
                        if(null != descObject) {
                            descObject.each { descLang, descVal ->
                                JSONObject descTmp = new JSONObject()
                                descTmp.put("language",descLang)
                                descTmp.put("content",descVal)
                                topicJSONDescriptions.put(descTmp)
                            }
                        }
                    }//end if descriptions

                    if(newTopicImages.size() > 0) {
                        def imgObject = newTopicImages[counter]
                        if(null != imgObject) {
                            imgObject.each { imgLang, imgVal ->
                                JSONObject imgTmp = new JSONObject()
                                imgTmp.put("language",imgLang)
                                imgTmp.put("url",imgVal)
                                topicJSONImages.put(imgTmp)
                            }
                        }
                    }//end if descriptions

                    newTopic.put("fromKeyword","newTopic");
                    newTopic.put("descriptions",topicJSONDescriptions);
                    newTopic.put("images",topicJSONImages);
                    newTopic.put("types",topicJSONTypes)

                    newTopics.add(newTopic)
                    counter++
                }//end each names
            }

            def createdTopics = []
            String ids = "";
            JSONArray newTopicIds = new JSONArray();
            try{

                if(newTopics.size() > 0) {
                    newTopics.each { newPayload ->
                        //System.out.println("TRYING TO SAVE:: " + newPayload.toString())
                        def result = adminTopicService.createTopic(newPayload.toString())
                        result.each{topicId, topicIf ->
                            createdTopics.add(topicId)
                            ids += topicId+", "
                        }
                    }
                }

                def message = "<ul>"
                if(createdTopics.size() > 0) {
                    message += "<li>Successfully created topics with the following topic identificators: "+ids+"</li>"
                    message += "</ul>"

                }
                flash.success = message
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to create the topic."
            }

            redirect(controller: 'topic', action: 'addTopics')

        }
        .invalidToken {
            // Bad request.
        }

    }


    def editTopics(){
        def langMap = [:]
        def shortMap = [:]
        def Language lang;

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            langMap.put(languageItem.getIdentifier(),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
        }


        [languages: langMap, searchDomain: grailsApplication.config.INDEX_SERVICE_URL, pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix,resourceHost: grailsApplication.config.RESOURCE_URI_HOST]
    }


    def editTopic() {
        def topicId = params.topicId

        NdlaTopic topic = adminTopicService.getTopicByTopicId(topicId)
        def names = topic.getNames()
        def nameMap = [:]
        def wordClassMap = [:]
        def wordClass = ""

        if(null != names){
            names.each{ namePsi, nameString ->
                if(namePsi.toString().contains("iso")){
                    nameMap.put(namePsi,nameString)
                }
                else{
                    if(!namePsi.toString().contains("language-neutral")){
                        wordClass = namePsi.toString().substring(namePsi.toString().lastIndexOf("#")+1)
                    }

                }
            }
        }
        else{
            def wordNames = topic.getWordNames()
            wordNames.each{wordName ->
                def nameTmps = wordName.names
                wordClass = wordName.wordClass
                nameTmps.each{nameTmpPsi,nameTmpString ->
                    if(nameTmpPsi.toString().contains("iso")){
                        nameMap.put(nameTmpPsi,nameTmpString)
                    }
                }
            }
        }

        def descriptions = topic.getDescriptions()
        def images = topic.getImages()
        def approvedState = topic.getApproved()
        def processState = topic.getProcessState()
        def visibility = topic.getVisibility()

        def topicPsis = topic.getPsis()

        def langMap = [:]
        def shortMap = [:]
        def missingLangs = [:]
        def topicNames = [:]


        ArrayList<NdlaTopic> wordClasses = adminTopicService.getWordClasses();
        wordClasses.each{wordClassItem ->
            def psi = wordClassItem.getPsi()
            def wordClassPsi = psi.substring(psi.lastIndexOf("#")+1)
            def name = wordClassItem.getNames().get("http://psi.oasis-open.org/iso/639/#eng")
            wordClassMap.put(wordClassPsi,name)
        }

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            langMap.put(languageItem.psi,languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
            shortMap.put(languageItem.psi,languageItem.psi.substring(languageItem.psi.lastIndexOf("#")+1))
        }


        for( l in langMap){
            if(null == nameMap.get(l.key)){
                missingLangs.put(l.key,l.value);
            }
        }
        [topicId: topicId, languages: langMap, shortMap: shortMap, missinglangs : missingLangs, names : nameMap,descriptions: descriptions, images: images, approvedState: approvedState,visibility: visibility, processState: processState,wordClassMap : wordClassMap,wordClass : wordClass,psis: topicPsis]
    }



    def doUpdateTopic(UpdateTopicCommand command){
        def topicId = params.topicId
        def subjectId = params.subjectId
        def site = params.site
        def approvedStatus = params.approvedRadios;
        def initialApprovalState = params.initialApprovalState;
        def wordClass = params.topicTitleWordClass
        def processStatus = params.processStateRadios;
        def processMap = [:]
        processMap["0"] = "Not processed"
        processMap["1"] = "Pending"
        processMap["2"] = "Processed"

        if(null == processStatus || "null" == processStatus){
            processStatus = "2";
        }

        def initialProcessState = params.initialProcessState
        def visibility = params.visibilityRadios == "visibilityOption1" ? "1" : "0"

        //validation
        if (command.hasErrors()) {
            flash.warning = "The topicId and word class fields cannot be empty."
            redirect(controller: 'topic', action: 'editTopics')
            return
        }

        def names = [:]
        def descriptions = [:]
        def images = [:]
        JSONArray topicJSONPSIS = new JSONArray();
        params.each {key , val ->
            if(key.toString().contains("name_")){
                names.put("http://psi.oasis-open.org/iso/639/#"+key.toString().substring(key.toString().lastIndexOf("_")+1),val)
            }
            else if(key.toString().contains("description_")){
                descriptions.put("http://psi.oasis-open.org/iso/639/#"+key.toString().substring(key.toString().lastIndexOf("_")+1),val)
            }
            else if(key.toString().contains("image_")){
                images.put("http://psi.oasis-open.org/iso/639/#"+key.toString().substring(key.toString().lastIndexOf("_")+1),val)
            }
            else if(key.toString().contains("subjId_") || key.toString().contains("newPsi_")){
                topicJSONPSIS.put(val)
            }
        }


        JSONObject payload = new JSONObject();
        JSONArray topicsArray = new JSONArray();

        JSONObject topicWrapper = new JSONObject();
        JSONArray topicDataArray = new JSONArray();

        JSONObject thisTopic = new JSONObject();
        thisTopic.put("approved",approvedStatus)
        thisTopic.put("visibility",visibility)
        thisTopic.put("wordClass",wordClass)

        JSONArray topicJSONNames = new JSONArray();
        JSONArray topicJSONDescriptions = new JSONArray();
        JSONArray topicJSONImages = new JSONArray();
        JSONArray topicJSONTypes = new JSONArray();

        names.each {lang,name ->
            JSONObject nameTmp = new JSONObject()
            nameTmp.put("name",name)
            nameTmp.put("language",lang)
            topicJSONNames.put(nameTmp)
        }

        thisTopic.put("names",topicJSONNames)

        descriptions.each {lang,description ->
            JSONObject descTmp = new JSONObject()

            descTmp.put("language",lang)
            descTmp.put("content",description)

            topicJSONDescriptions.put(descTmp);
        }

        thisTopic.put("descriptions",topicJSONDescriptions)

        images.each {lang,imageURL ->
            JSONObject imgTmp = new JSONObject()

            imgTmp.put("url",imageURL)
            imgTmp.put("language",lang)

            topicJSONImages.put(imgTmp)
        }

        thisTopic.put("images",topicJSONImages)
        thisTopic.put("types",topicJSONTypes)
        thisTopic.put("psis",topicJSONPSIS)

        topicDataArray.put(thisTopic)

        topicWrapper.put(topicId,topicDataArray);

        topicsArray.put(topicWrapper);
        payload.put("topics",topicsArray);


        boolean updateSuccess = false;
        try{
            if(payload.length() > 0) {
                def keymap = adminTopicService.updateTopicsByTopicId(payload.toString())
                if(keymap.size() > 0) {
                    updateSuccess = true;
                }
            }
        } catch (NdlaServiceException error) {
            flash.error = "An error occurred while trying to update the topic."
        }


        String successMessage = "<ul>"
        boolean wasApproved = false;

        if(updateSuccess) {
            successMessage  += "<li>The topic "+topicId+" was successfully updated</li>"
        }



        if(!initialApprovalState.equals(approvedStatus)) {
            try{
                boolean success = false;
                if(approvedStatus.equals("true")){
                    success = adminTopicService.approveTopic(topicId)
                    if(success) {
                        successMessage  += "<li>The topic "+topicId+" was successfully approved</li>";
                        flash.success = successMessage
                    }
                    else{
                        flash.error = "An error occurred while trying to approve the topic "+topicId
                    }
                    wasApproved = true;
                }
                else{
                    success = adminTopicService.disapproveTopic(topicId)
                    if(success) {
                        successMessage  += "<li>The topic "+topicId+" was successfully disapproved</li>";
                        flash.success = successMessage
                    }
                    else{
                        flash.error = "An error occurred while trying to disapprove the topic "+topicId
                    }
                }
            }
            catch (NdlaServiceException approveError) {
                flash.error = "An error occurred while trying to set approval state for the topic."
            }

        }

        if(!wasApproved){
            if(!initialProcessState.equals(processStatus) && processStatus != "2") {
                try {
                    boolean success = false;
                    success = adminTopicService.setProcessState(topicId, processStatus,"topics")
                    if (success) {
                        successMessage += "<li>The process state of was successfully set to '"+ processMap[processStatus] +"'</li></ul>";
                        flash.success = successMessage
                    } else {
                        flash.error = "There was a problem trying to set process state for the topic."
                    }

                }
                catch (NdlaServiceException approveError) {
                    flash.error = "An error occurred while trying to set process state for the topic."
                }

            }
            else{
                if(updateSuccess) {
                    successMessage += "</ul>"
                    flash.success = successMessage
                }
            }
        }





        redirect(controller: 'topic', action: 'editTopics')
    }


    def removeTopics(){
        def langMap = [:]
        def shortMap = [:]
        def Language lang;

        ArrayList<NdlaTopic> languageArray = adminTopicService.getLanguages();
        languageArray.each{ languageItem ->
            langMap.put(languageItem.getIdentifier(),languageItem.names.get("http://psi.oasis-open.org/iso/639/#eng"))
        }


        [languages: langMap, searchDomain: grailsApplication.config.INDEX_SERVICE_URL, pathPrefix: grailsApplication.config.ndla.topicservice.pathPrefix,resourceHost: grailsApplication.config.RESOURCE_URI_HOST]
    }

    def removeTopicByTopicId() {
        NdlaTopic topic = adminTopicService.getTopicByTopicId(params.topicId)
        def topicName = ""
        if(null != topic) {
            def topicNames = topic.getNames()

            if(null != topicNames){
                topicNames.each{ namePsi, nameString ->
                    if(namePsi.toString().equals("http://psi.oasis-open.org/iso/639/#eng")) {
                        topicName = nameString;
                        return true;
                    }
                }
            }
            else{
                def wordNames = topic.getWordNames()
                wordNames.each{wordName ->
                    def nameTmps = wordName.names
                    nameTmps.each{nameTmpPsi,nameTmpString ->
                        if(nameTmpPsi.toString().equals("http://psi.oasis-open.org/iso/639/#eng")) {
                            topicName = nameTmpString;
                            return true;
                        }
                    }
                }
            }



            [topicIdentifier: params.topicId,topicName: topicName]
        }
        else{
            flash.error = "An error occurred while trying to get the topic by topicId "+params.topicId
            redirect(controller: 'topic', action: 'removeTopics')
        }
    }

    def doRemoveTopic(RemoveTopicCommand command) {
        withForm {
            def topicIdentifier = params.topicIdentifier

            // Validate form.
            if (command.hasErrors()) {
                flash.warning = "The topic field cannot be empty."
                redirect(controller: 'topic', action: 'removeTopics')
                return
            }
            try {
                JSONArray payload = new JSONArray();
                payload.put(topicIdentifier);
                def result = adminTopicService.deleteTopicByTopicId(payload.toString())
                flash.success = "The topic was successfully removed."

                [topicIdentifier: topicIdentifier] // Hash map is made available to the view for rendering purposes.
            } catch (NdlaServiceException error) {
                flash.error = "An error occurred while trying to remove the topic."
            }
            redirect(controller: 'topic', action: 'removeTopics')
        }.invalidToken {
            // Bad request.
        }
    }
}


// ***** MISCELLANEOUS CLASSES *****

// Command classes (for validation purposes).

@grails.validation.Validateable
class RemoveTopicCommand {
    String topicIdentifier

    static constraints = {
        topicIdentifier(blank: false)
    }
}

@grails.validation.Validateable
class UpdateTopicCommand {
    String topicId
    String topicTitleWordClass

    static constraints = {
        topicId(blank: false)
        topicTitleWordClass(blank: false)
    }
}