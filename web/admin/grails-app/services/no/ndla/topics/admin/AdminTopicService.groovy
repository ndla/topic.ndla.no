package no.ndla.topics.admin

import grails.converters.JSON
import groovyx.net.http.HTTPBuilder
import net.ontopia.topicmaps.core.*

import no.ndla.topics.service.NdlaServiceException
import no.ndla.topics.service.NdlaSite
import no.ndla.topics.service.TopicService
import no.ndla.topics.service.model.NdlaTopic
import org.codehaus.jettison.json.JSONArray
import org.codehaus.jettison.json.JSONObject

import org.codehaus.groovy.grails.commons.ConfigurationHolder

import groovyx.net.http.HTTPBuilder
import groovyx.net.http.ContentType
import groovyx.net.http.Method
import groovyx.net.http.RESTClient


import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.Method.GET
import static groovyx.net.http.ContentType.JSON
import static groovyx.net.http.ContentType.TEXT


import grails.converters.*

class AdminTopicService {

	// Class constants.
    static def CONFIG_FILE_PATH = ""
    static def TOPICMAP_REFERENCE_KEY = ""
    static def INDEX_SERVICE_URL = ""
    static def INDEX_SOLR_URL = ""
    static def INDEX_SERVICE_USER = ""
    static def INDEX_SERVICE_PASS = ""
    static def MYCURRICULUM_SERVICE_URL = ""
    static def RESOURCE_URI_HOST = ""
    static def pathPrefix = ""



	def grailsApplication // Dependency injection of grailsApplication object.

	TopicService topicService

    def AdminTopicService() {
        //topicService = new TopicService(CONFIG_FILE_PATH, TOPICMAP_REFERENCE_KEY,INDEX_SERVICE_URL,INDEX_SERVICE_USER,INDEX_SERVICE_PASS);
    }

    def setTopicService(){
        topicService = new TopicService(
                grailsApplication.config.ndla.topicservice.configFilePath,
                grailsApplication.config.ndla.topicservice.topicmapReferenceKey,
                grailsApplication.config.INDEX_SERVICE_URL,
                grailsApplication.config.INDEX_SOLR_URL,
                grailsApplication.config.INDEX_SERVICE_USER,
                grailsApplication.config.INDEX_SERVICE_PASS);
        this.CONFIG_FILE_PATH = grailsApplication.config.ndla.topicservice.configFilePath;
        this.TOPICMAP_REFERENCE_KEY = grailsApplication.config.ndla.topicservice.topicmapReferenceKey;
        this.INDEX_SERVICE_URL = grailsApplication.config.INDEX_SERVICE_URL;
        this.INDEX_SOLR_URL = grailsApplication.config.INDEX_SOLR_URL;
        this.INDEX_SERVICE_USER = grailsApplication.config.INDEX_SERVICE_USER;
        this.INDEX_SERVICE_PASS = grailsApplication.config.INDEX_SERVICE_PASS;
        this.MYCURRICULUM_SERVICE_URL = grailsApplication.config.MYCURRICULUM_SERVICE_URL;
        this.RESOURCE_URI_HOST = grailsApplication.config.RESOURCE_URI_HOST;
        this.pathPrefix = grailsApplication.config.ndla.topicservice.pathPrefix;
    }

    def getTopicService(){
        return topicService;
    }

    /** KEYWORDS **/
    def updateKeywordIndexAll(String site) {
        try {
            // ArrayList<NdlaTopic> getKeywordsByNodeId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException
            def jsonString = topicService.reIndexAllKeywords(site);
            return jsonString;
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }


    def createKeyword(String jsonPayload) {
        try {
            def data = topicService.createKeyword(jsonPayload)
            //System.out.println("CREATEDATA: "+data.toString())
            return data
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def deleteKeywordIndexAll(String index) {
        try {
            //System.out.println("SEARCHSERVER: "+this.INDEX_SERVICE_URL);
            // ArrayList<NdlaTopic> getKeywordsByNodeId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException
            def jsonString = topicService.deleteAllKeywordsFromAutoCompleteIndex(index);
            //System.out.println("GOT FROM DELETE: "+jsonString);
            return jsonString;
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

	// TODO: Remove temporary code (for testing purposes to determine that everything is correctly wired up).
	def getKeywordsByNodeId(limit, offset, nodeIdentifier, site) {
    	try {
            // ArrayList<NdlaTopic> getKeywordsByNodeId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException
            def keywords = topicService.getKeywordsByNodeId(limit, offset, nodeIdentifier, NdlaSite.NDLA)
            return [keywords: keywords] // Return dictionary for view rendering.
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
	}

    def getKeywordByKeywordId(keywordId) {
        NdlaTopic keyword = null;
        try{
            keyword = topicService.getKeywordByKeywordId(keywordId);
            return keyword
        } catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def disapproveKeyword(String keywordId) {
        JSONObject message = null;
        String psi = "http://psi.topic.ndla.no/keywords/#"+keywordId;
        boolean success = false;
        try{
            message = topicService.disApproveTopic(psi,"keywords")
            if(message.getString("success").length() > 0) {
                success = true;
            }
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return success
    }

    def approveKeyword(String keywordId){
        JSONObject message = null;
        String psi = "http://psi.topic.ndla.no/keywords/#"+keywordId;
        boolean success = false;
        try{
            java.util.Date date= new java.util.Date();
            message = topicService.approveTopic(psi,date,"keywords")
            if(message.getString("success").length() > 0) {
                success = true;
            }
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return success
    }

    def setProcessState(String topicId, String state, String index){
        JSONObject message = null;

        String psi = ""
        if(index == "keywords"){
            psi = "http://psi.topic.ndla.no/keywords/#"+topicId;
        }
        else if(index == "topics"){
            psi = "http://psi.topic.ndla.no/topics/#"+topicId;
        }


        boolean success = false;
        try{
            message = topicService.setProcessState(psi,state,index)
            if(message.getString("success").length() > 0) {
                success = true;
            }
            return success
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }


    /** TOPICS **/

    def updateTopicIndexAll(String site) {
        try {
            // ArrayList<NdlaTopic> getKeywordsByNodeId(int limit, int offset, String identifier, NdlaSite site) throws NdlaServiceException
            def jsonString = topicService.reIndexAllTopics();
            return jsonString;
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def approveTopic(String topicId){
        JSONObject message = null;
        String psi = "http://psi.topic.ndla.no/topics/#"+topicId;
        boolean success = false;
        try{
            java.util.Date date= new java.util.Date();
            message = topicService.approveTopic(psi,date,"topics")
            if(message.getString("success").length() > 0) {
                success = true;
            }
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return success
    }

    def disapproveTopic(String topicId) {
        JSONObject message = null;
        String psi = "http://psi.topic.ndla.no/topics/#"+topicId;
        boolean success = false;
        try{
            message = topicService.disApproveTopic(psi,"topics")
            if(message.getString("success").length() > 0) {
                success = true;
            }
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return success
    }

    def getDuplicateTopics(limit, offset, identifier){
        try {
            def duplicates = topicService.getDuplicateTopics(limit,offset,identifier)
            return duplicates
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def getNodesByKeywordId(limit, offset, keywordIdentifier, site) {
        try {
            def nodes = topicService.getNodesByKeywordId(limit, offset, keywordIdentifier, site) // TODO: Pass in appropriate site parameter.
            return [nodes: nodes] // Return dictionary for view rendering.
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }


	def deleteKeywordsByKeywordId(keywordIdentifiers) {
		/*
		def configFilePath = grailsApplication.config.ndla.topicservice.configFilePath
		def topicmapReferenceKey = grailsApplication.config.ndla.topicservice.topicmapReferenceKey
		*/
    	try {
            // ArrayList<TopicIF> deleteKeywordsByKeywordId(List<String> identifiers, NdlaSite site) throws NdlaServiceException
            def keywords = topicService.deleteKeywordsByKeywordId(keywordIdentifiers, NdlaSite.NDLA)
            return [keywords: keywords] // Return dictionary for view rendering.
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def updateKeywordsByKeywordId(String jsonPayload) {
    	try {
            // HashMap<String, TopicIF> updateKeywordsByKeywordId(String data, NdlaSite site) throws NdlaServiceException
            def data = topicService.updateKeywordsByKeywordId(jsonPayload);

            System.out.println("UPDATEDATA: "+data.toString())
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def removeNodesByKeywordId(String jsonPayload) {
        try {
            def data = topicService.removeAssociation(jsonPayload, NdlaSite.NDLA)
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def mergeDuplicateTopics(String jsonPayload) {
        JSONArray message = null;
        try {
            message = topicService.mergeDuplicateTopics(jsonPayload)
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return message;
    }

    /** END KEYWORDS **/



    /** LANGUAGES **/

    def getLanguages() {
        ArrayList<NdlaTopic> languages = null;
        try {
            languages = topicService.getLanguages()
            return languages;
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def getLanguage(languageId){
        NdlaTopic language = null;
        try {
            language = topicService.getLanguage(languageId)
            return language;
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def updateLanguage(String jsonPayload) {
        try {
            def data = topicService.updateLanguage(jsonPayload)
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def addLanguage(String jsonPayload) {
        try {
            def data = topicService.createLanguage(jsonPayload)
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def removeLanguage(languageId){
        def data = null
        try {
            data = topicService. deleteLanguageByLanguageId(languageId)
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
       return data;
    }

    /** END LANGUAGES **/

    /** WORDCLASSES **/
    def getWordClasses() {
        ArrayList<NdlaTopic> wordClasses = null;
        try {
            wordClasses = topicService.getWordClasses();
            return wordClasses;
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }
    /** END WORDCLASSES **/


    /** SUBJETCS **/
    def getSubjects(limit, offset,site) {
        ArrayList<NdlaTopic> subjects = null;
        try {
            subjects = topicService.getSubjects(limit, offset,site)
            return subjects;
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }


    def getSubjectBySubjectId(subjectId, site) {
        NdlaTopic subject = null;
        try{
            subject = topicService.getSubjectBySubjectId(subjectId,site)
            return subject
        }catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def getSubjectsByTopicId(limit, offset,topicId,site) {
        ArrayList<NdlaTopic> subjects = null;
        try {
            subjects = topicService.getSubjectsByTopicId(limit, offset,topicId,site)
            return subjects;
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def getTopicsBySubjectMatter(limit, offset,subjectId,site) {
        JSONObject topics = null;
        try {
            topics = topicService.getTopicsBySubjectMatter(limit, offset,subjectId,site)
            return topics;
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def getTopicByTopicId(topicID) {
        NdlaTopic topic = null;
        try{
            topic = topicService.getTopicByTopicId(topicID)
            return topic
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def getRelationTopicByTopicId(topicID) {
        NdlaTopic topic = null;
        try{
            topic = topicService.getRelationTopicByTopicId(topicID)
            return topic
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }



    def getNodesByTopicId(limit, offset, topicIdentifier, site) {
        ArrayList<NdlaTopic> nodes = null;
        try {
            nodes = topicService.getNodesByTopicId(limit,offset,topicIdentifier,site)
            return nodes;
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def updateSubject(String jsonPayload) {
        try {
            def data = topicService.updateSubject(jsonPayload)
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def createSubject(String jsonPayload) {
        try {
            def data = topicService.createSubject(jsonPayload)
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }

    def removeSubject(String payload){
        try{
            def data = topicService.deleteSubjectBySubjectId(payload)
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
    }
    /** END SUBJECTS **/


    /** TOPICS **/
    def createTopic(String payload) {
        HashMap<String,TopicIF> data = null;
        try{
            data = topicService.createTopic(payload)
            if(data.keySet().size() < 1) {
                data = [:]
                data.put("error","The topic already exists")
            }
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }

        return data
    }

    def saveTopicsBySubjectMatter(subjectMatterId, payload, site) {
        ArrayList<String> savedIds = null;
        try{
            savedIds = topicService.saveTopicsBySubjectMatter(subjectMatterId,payload,site)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return savedIds;
    }

    def deleteTopicsFromSubjectOntology(payload, subjectMatterId, site) {
        ArrayList<String> deletedIds = null;
        try{
            //deletedIds = topicService.deleteTopics
            return topicService.deleteTopicsFromSubjectOntology(payload,subjectMatterId,site);
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }

        return deletedIds;
    }

    def deleteTopicByTopicId(payload) {
        ArrayList<String> deletedIds = null;
        try{
            return topicService.deleteTopicByTopicId(payload);
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }

        return deletedIds;
    }


    def updateTopicsByTopicId(payload) {
        HashMap<String,TopicIF> data = null;
        try{
            data = topicService.updateTopicsByTopicId(payload)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return data
    }
    /** END TOPICS **/

    /** RELATION TYPES **/
    def createRelationShipTypes(payload) {
        def data = null
        try{
           data = topicService.createAssociationTypes(payload)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return data
    }

    def updateAssociationTypeByAssocTypeId(payload) {
        def data = null
        try{
            data = topicService.updateAssociationTypeByAssocTypeId(payload)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return data
    }

    def deleteAssociationTypeByTopicId(payload){
        def data = null
        try{
            data = topicService.deleteAssociationTypeByTopicId(payload)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return data
    }

    def getHierarchicalAssociationTypes(){
        def data = null
        try{
            data = topicService.getHierarchicalAssociationTypes()
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return data
    }

    def getHierarchicalAssociationTypeById(typeId) {
        def data = null
        try {
            data = topicService.getHierarchicalAssociationType(typeId)
        }
        catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return data
    }

    def getHorizontalAssociationTypes(){
        def data = null
        try{
            data = topicService.getHorizontalAssociationTypes()
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return data
    }

    def getHorizontalAssociationTypeById(typeId) {
        def data = null
        try {
            data = topicService.getHorizontalAssociationType(typeId)
        }
        catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return data
    }

    def getTopicPlayersByRelationTypeId(assoctype, role1, role2) {
        def data = null
        try {
            data = topicService.getTopicPlayersByRelationTypeId(assoctype,role1,role2)
        }
        catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return data
    }
    /** END RELATION TYPES **/

    /** CURRICULA **/

     def getCourses(siteSubjectId){
         def path = "/v1/users/ndla/courses/"+siteSubjectId
         def http = new HTTPBuilder(MYCURRICULUM_SERVICE_URL)
         http.request(GET, ContentType.JSON) {
             uri.path = path
             headers.'User-Agent' = 'Mozilla/5.0 Ubuntu/8.10 Firefox/3.0.4'
             headers.Accept = 'application/json'
             headers.ContentType = 'application/json'

             response.success = { resp, json ->
                 return json.course
             }

             // Response handler for any failure status codes.
             response.failure = { resp ->
                 JSONObject messageObject = new JSONObject()
                 messageObject.put("message","An error occurred trying to fetch the courses from MyCurriculum: "+resp.status)
                 return messageObject
             }
         }
     }

    def getCurriculumData(udirId) {
        def path = "v1/users/ndla/curriculums/"+udirId
        def http = new HTTPBuilder(MYCURRICULUM_SERVICE_URL)

        http.request(GET, ContentType.JSON) {
            uri.path = path
            headers.Accept = 'application/json'
            headers.ContentType = 'application/json'

            response.success = { resp, json ->
                return json.curriculum
            }

            // Response handler for any failure status codes.
            response.failure = { resp ->
                JSONObject messageObject = new JSONObject()
                messageObject.put("message","An error occurred trying to fetch the "+udirId+" curriculumfrom MyCurrculum: "+resp.status)
                return messageObject
            }
        }
    }

    def saveAimHasTopic(subjectMatterIdentifier, payload, site){
        def result = null
        try{
            result = topicService.saveAimHasTopic(subjectMatterIdentifier,payload,site)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }

    def removeTopicsByAimId(jsonPayload, site) {
        def data
        try {
            data = topicService.removeAssociation(jsonPayload, site)
        } catch (NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return data
    }


    def getAimTopicAssociations(aimId,lang) {
        def result = null
        try{
            result = topicService.getAimTopicAssociations(aimId,lang)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }

    /** END CURRICULA **/

    /** ONTOLOGYTOPICS **/
    def getOntologyTopics(subjectId, site){
        def result = null
        try{
            result = topicService.getOntologyTopics(subjectId,site)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }

    def getSubjectOntologyTopicRelations(subjectId, site) {
        def result = null
        try{
            result = topicService.getSubjectOntologyTopicRelations(subjectId,site)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }

    def getCurriculaOntologyTopics(subjectId, site) {
        def result = null
        try{
            result = topicService.getCurriculaOntologyTopics(subjectId,site)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }

    def saveOntologyTopicAssociations(subjectId,reifierMarker,payload,site){
        def result = null
        try{
            result = topicService.saveOntologyTopicAssociations(subjectId,reifierMarker,payload,site)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }


    def getTopicsBySubjectMatterAndNameString(search, subjectId, site) {
        def result = null
        try{
            result = topicService.getTopicsBySubjectMatterAndNameString(search,subjectId,site)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }

    def lookUpTopicsBySearchTerm(search,type,searchType,language, limit,offset){
        def result = null
        try{
            result = topicService.lookUpTopicsBySearchTerm(search,type,searchType,language, limit,offset)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }



    def  getSubordinateRoleTypes(){
        def result = null
        try{
            result = topicService.getSubordinateRoleTypes()
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }

    def  getSuperordinateRoleTypes(){
        def result = null
        try{
            result = topicService.getSuperordinateRoleTypes()
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }

    def  getRoleType(){
        def result = null
        try{
            result = topicService.getRoleType()
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }


    def  removeAssociationByReifier(payload,site){
        def result = null
        try{
            result = topicService.removeAssociationByReifier(payload,site)
        }
        catch(NdlaServiceException e) {
            System.out.println(e.getMessage())
        }
        return result
    }


    /** END ONTOLOGYTOPICS **/
}
