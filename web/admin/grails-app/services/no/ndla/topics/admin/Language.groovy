package no.ndla.topics.admin;

/**
 * Created by rolfguescini on 22.04.14.
 */
public class Language {
    private String languageId;
    private String languagePsi;
    private HashMap<String,String> languageNames;


    Language(String languageId, String languagePsi,HashMap<String, String> languageNames) {
        this.languageId = languageId
        this.languagePsi = languagePsi
        this.languageNames = languageNames
    }

    String getLanguageId() {
        return languageId
    }

    void setLanguageId(String languageId) {
        this.languageId = languageId
    }

    String getLanguagePsi() {
        return languagePsi
    }

    void setLanguagePsi(String languagePsi) {
        this.languagePsi = languagePsi
    }


    HashMap<String, String> getLanguageNames() {
        return languageNames
    }

    String getLanguageName(String psi){
        return languageNames.get(psi)
    }

    void setLanguageNames(HashMap<String, String> languageNames) {
        this.languageNames = languageNames
    }
}
