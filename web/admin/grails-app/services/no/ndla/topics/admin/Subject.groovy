package no.ndla.topics.admin;

/**
 * Created by rolfguescini on 16.04.14.
 */
public class Subject {
    private String subjectId;
    private String UDIRPsi;
    private ArrayList<String> subjectPsis;
    private HashMap<String,String> subjectNames;
    private ArrayList<String> originatingSites;

    Subject(String subjectId, String UDIRPsi, ArrayList<String> subjectPsis, HashMap<String, String> subjectNames, ArrayList<String> originatingSites) {
        this.subjectId = subjectId
        this.UDIRPsi = UDIRPsi
        this.subjectPsis = subjectPsis
        this.subjectNames = subjectNames
        this.originatingSites = originatingSites
    }

    String getSubjectId() {
        return subjectId
    }

    void setSubjectId(String subjectId) {
        this.subjectId = subjectId
    }

    String getUDIRPsi() {
        return UDIRPsi
    }

    void setUDIRPsi(String UDIRPsi) {
        this.UDIRPsi = UDIRPsi
    }

    ArrayList<String> getSubjectPsis() {
        return subjectPsis
    }

    void setSubjectPsis(ArrayList<String> subjectPsis) {
        this.subjectPsis = subjectPsis
    }

    HashMap<String, String> getSubjectNames() {
        return subjectNames
    }

    String getSubjectName(String psi) {
        return null != subjectNames.get(psi) ? subjectNames.get(psi): subjectNames.get("http://psi.topic.ndla.no/#language-neutral");
    }

    void setSubjectNames(HashMap<String, String> subjectNames) {
        this.subjectNames = subjectNames
    }

    ArrayList<String> getOriginatingSites() {
        return originatingSites
    }

    void setOriginatingSites(ArrayList<String> originatingSites) {
        this.originatingSites = originatingSites
    }
}
