package no.ndla.topics.admin

class ApplicationTagLib {
	def icon = { attrs, body ->
        out << body() << "<i class='icon-${attrs.name}'></i>"
    }
}
