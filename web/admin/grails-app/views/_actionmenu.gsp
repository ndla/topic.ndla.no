<div class="navbar-inner">
    <!--a class="brand" href="#">Actions</a-->
    <ul class="nav">
        <g:if test="${param == 1}">
            <li class="active"><g:link controller="keyword" action="lookupKeyword">Keywords</g:link></li>
            <li><g:link controller="topic" action="editTopics">Topics</g:link></li>
            <li><g:link controller="curriculum" action="getSubjects">Curriculum Ontologies</g:link></li>
            <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']">Subject Ontologies</g:link></li>
            <li><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab', 'removeLanguage' : 'false']">Languages</g:link></li>
            <li><g:link controller="indexSOLR" action="updateIndexAll">SOLR index</g:link></li>
        </g:if>
        <g:if test="${param == 2}">
            <li><g:link controller="keyword" action="lookupKeyword">Keywords</g:link></li>
            <li class="active"><g:link controller="topic" action="editTopics">Topics</g:link></li>
            <li><g:link controller="curriculum" action="getSubjects">Curriculum Ontologies</g:link></li>
            <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']">Subject Ontologies</g:link></li>
            <li><g:link controller="language" action="getLanguages"params="['data-toggle': 'tab', 'removeLanguage' : 'false']">Languages</g:link></li>
            <li><g:link controller="indexSOLR" action="updateIndexAll">SOLR index</g:link></li>
        </g:if>
        <g:if test="${param == 3}">
            <li><g:link controller="keyword" action="lookupKeyword">Keywords</g:link></li>
            <li><g:link controller="topic" action="editTopics">Topics</g:link></li>
            <li class="active"><g:link controller="curriculum" action="getSubjects">Curriculum Ontologies</g:link></li>
            <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']" >Subject Ontologies</g:link></li>
            <li><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab', 'removeLanguage' : 'false']">Languages</g:link></li>
            <li><g:link controller="indexSOLR" action="updateIndexAll">SOLR index</g:link></li>
        </g:if>
        <g:if test="${param == 4}">
            <li><g:link controller="keyword" action="lookupKeyword">Keywords</g:link></li>
            <li><g:link controller="topic" action="editTopics">Topics</g:link></li>
            <li><g:link controller="curriculum" action="getSubjects">Curriculum Ontologies</g:link></li>
            <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']">Subject Ontologies</g:link></li>
            <li><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab', 'removeLanguage' : 'false']">Languages</g:link></li>
            <li><g:link controller="indexSOLR" action="updateIndexAll">SOLR index</g:link></li>
        </g:if>
        <g:if test="${param == 5}">
            <li><g:link controller="keyword" action="lookupKeyword">Keywords</g:link></li>
            <li><g:link controller="topic" action="editTopics">Topics</g:link></li>
            <li><g:link controller="curriculum" action="getSubjects">Curriculum Ontologies</g:link></li>
            <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']">Subject Ontologies</g:link></li>
            <li><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab', 'removeLanguage' : 'false']">Languages</g:link></li>
            <li class="active"><g:link controller="indexSOLR" action="updateIndexAll">SOLR index</g:link></li>
        </g:if>
        <g:if test="${param == 6}">
            <li><g:link controller="keyword" action="lookupKeyword">Keywords</g:link></li>
            <li><g:link controller="topic" action="editTopics">Topics</g:link></li>
            <li><g:link controller="curriculum" action="getSubjects">Curriculum Ontologies</g:link></li>
            <li class="active"><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']">Subject Ontologies</g:link></li>
            <li><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab', 'removeLanguage' : 'false']">Languages</g:link></li>
            <li><g:link controller="indexSOLR" action="updateIndexAll">SOLR index</g:link></li>
        </g:if>
        <g:if test="${param == 7}">
            <li><g:link controller="keyword" action="lookupKeyword">Keywords</g:link></li>
            <li><g:link controller="topic" action="editTopics">Topics</g:link></li>
            <li><g:link controller="curriculum" action="getSubjects">Curriculum Ontologies</g:link></li>
            <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']">Subject Ontologies</g:link></li>
            <li class="active"><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab', 'removeLanguage' : 'false']">Languages</g:link></li>
            <li><g:link controller="indexSOLR" action="updateIndexAll">SOLR index</g:link></li>
        </g:if>
    </ul>
</div>