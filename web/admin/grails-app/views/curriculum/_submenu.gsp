<ul class="nav nav-tabs">
    <g:if test="${param == 1}">
        <li class="active"><a href="#tab1" data-toggle="tab">Curricula Subject topics</a></li>
        <li><g:link controller="curriculum" action="addHierarchicalRelation" params="['data-toggle': 'tab']">Hierarchical Topic  Relations</g:link></li>
        <li><g:link controller="curriculum" action="addHorizontalRelation" params="['data-toggle': 'tab']">Horizontal Topic  Relations</g:link></li>
    </g:if>
    <g:if test="${param == 2}">
        <li><g:link controller="curriculum" action="getSubjects" params="['data-toggle': 'tab']">Curricula Subject topics</g:link></li>
        <li class="active"><a href="#tab4" data-toggle="tab">Hierarchical Topic  Relations</a></li>
        <li><g:link controller="curriculum" action="addHorizontalRelation" params="['data-toggle': 'tab']">Horizontal Topic  Relations</g:link></li>
    </g:if>
    <g:if test="${param == 3}">
        <li><g:link controller="curriculum" action="getSubjects" params="['data-toggle': 'tab']">Curricula Subject topics</g: link></li>
        <li><g:link controller="curriculum" action="addHierarchicalRelation" params="['data-toggle': 'tab']">Hierarchical Topic  Relations</g:link></li>
        <li class="active"><a href="#tab5" data-toggle="tab">Horizontal Topic  Relations</a></li>
    </g:if>
</ul>