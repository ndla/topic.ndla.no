<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Curriculum Ontologies</title>
    <r:script>
	        $(document).ready(function() {
                searchDomain = $("#searchDomain").val()
                pathPrefix = $("#pathPrefix").val()
                existingCounter = 0;
                existingAssociations = {}
                getTopicsBySubjectMatter();
                $('.accordion-body').on('show', function () {
                    var $theAccordion = $(this);
                    var idArr = $theAccordion.children(".accordion-inner").attr("id").split("_");
                    var aimId = idArr[1]+"_"+idArr[2]+"_"+idArr[3];
                    getAimTopicAssociations(aimId,"eng");
                    $theAccordion.children(".accordion-inner").css("height","400px");
                    $theAccordion.children(".accordion-inner").css("background-color", "#EFEFEF");
                    $theAccordion.children(".accordion-inner").css("border", "2px solid #E5E5E5");
                    $theAccordion.children(".accordion-inner").css("padding", "10px");
                    $theAccordion.children(".accordion-inner").css("overflow", "auto");

                });

                $(':input[data-typeahead-target]').each(function(){
                    var $this = $(this);
                    if($this.hasClass("topicTitleSearch")){
                        $this.typeahead ({
                            source: function (query, process) {
                                keywords = [];
                                map = {};
                                langMap = [];
                                langNames = []
                                var jsonLanguages = $.parseJSON($("#languages").val());
                                var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                                var searchLanguage = ""
                                var searchType = "start"//$('#typeSelect').val();
                                for(var langKey in jsonLanguages){
                                    if (!jsonLanguages.hasOwnProperty(langKey)) {
                                        continue;
                                    }
                                    searchLanguage += "language[]="+langKey+"&";
                                    langMap.push(langKey)
                                    langNames[langKey] = jsonLanguages[langKey];
                                }


                                searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
                                serviceUrl += "?" + searchLanguage + "&type=" + searchType+ "&index=topics";
                                var ontologyTopics = $.parseJSON($("#ontologyTopics").val());

                                $.ajax({
                                    url: serviceUrl,
                                    type: 'GET',
                                    dataType: 'json',
                                    success: function (response) {
                                        //console.log("RESPONSE: "+response)
                                        var data = processJson(response, langMap);

                                        $.each(data, function (i, keyword) {
                                            var key = keyword.title;
                                            var keyId = keyword.identifier;
                                            if (!(key in map) && keyId.indexOf("topic") > -1) {
                                                if(ontologyTopics.indexOf(keyId) > -1){
                                                map[key] = keyword;
                                                keywords.push(key);
                                                }

                                            }
                                        });
                                        process(keywords);
                                    }
                                }, 300);
                            },
                            matcher: function (item) {
                                return true;
                            },
                            updater: function (item) {
                                var topicObject = map[item];
                                var typeAheadId = $this.attr("id");
                                var typeAheadIdArr = typeAheadId.split("_");

                                var aimId = typeAheadIdArr[1]+"_"+typeAheadIdArr[2]+"_"+typeAheadIdArr[3]
                                var topicCount;
                                if(existingCounter > 0) {
                                    topicCount = existingCounter++;
                                }
                                else{
                                    topicCount = (parseInt($("#navPills_"+aimId).children("span").length)+1)
                                }
                                var assocTypes = getAssociationTypes(aimId,topicCount)

                                $("#navPills_"+aimId).append('<br /><div class="label label-info">'+item+'</div><span id="assocTypeWrapper_'+aimId+'_'+topicCount+'" class="assocTypeWrapper">'+assocTypes+'</span><a href="javascript: " class=""><!--a id="showSwap_'+aimId+'_'+topicCount+'" href="javascript:" onclick="showSelect(\''+aimId+'_'+topicCount+'\')"><i class="icon-plus-sign"></i></a--><div id="assocRoleWrapper_'+aimId+'_'+topicCount+'"></div>');
                                var checkBox = '<g:checkBox name="chosenTopics" value="'+aimId+'_'+topicCount+'_'+map[item].identifier+'" checked="true"/>';
                                $("#chosenTopics_"+aimId).append(checkBox);
                                return "";
                                $this.on('mousedown', function(e) {
                                    e.preventDefault();
                                });
                                $('#topicTitle').typeahead('close');
                            }
                        });
                    }

                });



	        });

            function showSelect(id){
                $("#assocTypeWrapper_"+id).css("display","block")
                $("#showSwap_"+id).css("display","none")
            }

        function getAssociationTypes(aimId,topicCount){
            var host = window.location.protocol + '//' + window.location.host;
            var serviceUrl = host + pathPrefix+'/service/getAssociationTypes'

            var result = "";
            $.ajax({
                async: false,
                url: serviceUrl,
                dataType: 'json'
            }).done(function (data) {
                var hierarchicalTypes = data.hierarchical;
                var horizontalTypes = data.horizontal;

                result = '&nbsp;&nbsp;&nbsp;<select id="hierarchicalRelationtypeSelect_'+aimId+'_'+topicCount+'" onChange="hideSelect(this)">';
                result += '<option value="">Select a hierarchical relationshipType</option>';
                for(var i = 0; i < hierarchicalTypes.length; i++){
                    var hierarchicalType = hierarchicalTypes[i];
                    result += '<option value="'+hierarchicalType.psi+'">'+hierarchicalType.name+'</option>';
                }
                result += '</select>';

                result += '&nbsp;&nbsp;&nbsp;<select id="horizontalRelationtypeSelect_'+aimId+'_'+topicCount+'" onChange="hideSelect(this)">';
                result += '<option value="">Select a horizontal relationshipType</option>';
                for(var i = 0; i < horizontalTypes.length; i++){
                    var horizontalType = horizontalTypes[i];
                    result += '<option value="'+horizontalType.psi+'">'+horizontalType.name+'</option>';
                }
                result += '</select>';

            });
            return result;
        }

        function getAimTopicAssociations(aimId,lang){
            var host = window.location.protocol + '//' + window.location.host;
            var serviceUrl = host + pathPrefix+'/service/getAimTopicAssociations'
            var aimArr = aimId.split("_");
            var aimBit = aimArr[2];
            var result = "";
            if(typeof existingAssociations[aimBit] == 'undefined'){
                existingAssociations[aimBit] = new Array();
            }
            $.ajax({
                async: false,
                url: serviceUrl,
                dataType: 'json',
                data: {
                'aimId' : aimBit,
                'lang' : lang
                }
            }).done(function (data) {
                var html = '';
                var found = [];
                for(var i = 0; i < data.associations.length; i++) {
                    existingCounter++;
                    var association = data.associations[i];
                    var assocNames = association.associationTypeNames;
                    var assocNameString = "";
                    var associationId = association.associationType.substring(association.associationType.indexOf("#")+1);

                    if(typeof assocNames != 'undefined' && assocNames.length > 0) {
                        for (var n = 0; n < assocNames.length; n++) {
                            assocName = assocNames[n];
                            if (n == 0) {
                                assocNameString += assocName.name;
                            }
                            else{
                                assocNameString += ' / '+assocName.name
                            }
                        }
                    }
                    var rolePlayers = association.rolePlayers;
                    var roleType = ""
                    var roleName = ""
                    var roleId = ""
                    var rolePSI = ""
                    var otherRolePSI
                    if(associationId != "http://psi.topic.ndla.no/aim_has_topic"){
                        html += '<div>';
                    }
                    for (var j = 0 ; j < rolePlayers.length; j++){

                        var rolePlayer = rolePlayers[j];
                        if(rolePlayer.rolePlayer.indexOf("competence-aim#") == -1 && rolePlayer.rolePlayer.indexOf("psi.udir.no") == -1){
                            roleId = rolePlayer.rolePlayer;
                            roleIdCheck = roleId.substring(roleId.indexOf("#")+1);
                            rolePSI = rolePlayer.roleType;
                            //rolePSI = rolePSI.substring(rolePSI.indexOf("#")+1);
                            roleType = rolePlayer.roleType;
                            var rolePlayerNames = rolePlayer.rolePlayerNames;
                            if (typeof rolePlayerNames != 'undefined' && rolePlayerNames.length > 0){
                                for(var k = 0; k < rolePlayerNames.length;k++){
                                    var rolePlayerName = rolePlayerNames[k];
                                    if(rolePlayerName.language.contains(lang)) {
                                        roleName = rolePlayerName.name;
                                    }
                                    else{
                                        if(rolePlayerName.language.contains("nob") && roleName == ""){
                                            roleName = rolePlayerName.name;
                                        }
                                        else if(rolePlayerName.language.contains("nno") && roleName == ""){
                                            roleName = rolePlayerName.name;
                                        }
                                        else if(rolePlayerName.language.contains("eng") && roleName == ""){
                                            roleName = rolePlayerName.name;
                                        }
                                        else if(rolePlayerName.language.contains("language-neutral") && roleName == ""){
                                            roleName = rolePlayerName.name;
                                        }

                                    }
                                }
                            }
                        }
                        else{
                            otherRolePSI = rolePlayer.roleType;
                        }
                    }//end for players

                    if(hasDoubleRelations(data.associations,aimId,roleIdCheck)){

                            if(associationId != "http://psi.topic.ndla.no/aim_has_topic" && associationId != "primary-topic"){
                                html += '<div class="btn-group" id="existingButtonGroup_'+aimBit+'_'+i+'"><button class="btn btn-small btn-info dropdown-toggle"><em>'+assocNameString+' (' +roleType+')</em> <strong>'+roleName+'</strong></button>';
                                html += '<button class="btn btn-small btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>';
                                html += '<ul class="dropdown-menu"><!--li><a href="javascript:" onclick="editAimTopicAssociation(\''+aimId+'\',\''+roleName+'\',\''+roleIdCheck+'\',\''+i+'\')">edit</a></li--><li><a href="javascript:" onclick="deleteAimTopicAssociation(\''+aimId+'\',\''+roleIdCheck+'\',\''+associationId+'\',\''+i+'\')">delete</a></li></ul>';
                                html += '</div>';
                            if(existingAssociations[aimBit].indexOf(roleIdCheck) == -1){
                                var topicCheckBox = '<g:checkBox name="existingTopics" value="'+aimId+'_'+i+'_'+roleIdCheck+'" checked="true"/>';
                                $("#existingTopicsHidden_"+aimId).append(topicCheckBox);

                                var relationCheckBox = '<g:checkBox name="existingTopicRelations" value="'+aimId+'_'+i+'_'+association.associationType+'" checked="true"/>';
                                $("#existingTopicRelationHidden_"+aimId).append(relationCheckBox)

                                var roleCheckBox = '<g:checkBox name="existingTopicRelationRoles" value="'+aimId+'_'+i+'_'+rolePSI+'" checked="true"/>';
                                var checkOtherRoleBox = '<g:checkBox name="existingTopicOtherRelationRoles" value="'+aimId+'_'+i+'_'+otherRolePSI+'" checked="true"/>';
                                $("#existingTopicRelationRoleHidden_"+aimId).append(roleCheckBox)
                                $("#existingTopicOtherRelationRoleHidden_"+aimId).append(checkOtherRoleBox)
                            }
                        }

                    }
                    else{
                        if(associationId != "primary-topic"){
                            html += '<div class="btn-group" id="existingButtonGroup_'+aimBit+'_'+i+'"><button class="btn btn-small btn-info dropdown-toggle"><em>'+assocNameString+' (' +roleType+')</em> <strong>'+roleName+'</strong></button>';
                            html += '<button class="btn btn-small btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>';
                            html += '<ul class="dropdown-menu"><!--li><a href="javascript:" onclick="editAimTopicAssociation(\''+aimId+'\',\''+roleName+'\',\''+roleIdCheck+'\',\''+i+'\')">edit</a></li--><li><a href="javascript:" onclick="deleteAimTopicAssociation(\''+aimId+'\',\''+roleIdCheck+'\',\''+associationId+'\',\''+i+'\')">delete</a></li></ul>';
                            html += '</div>';
                        }
                            if(existingAssociations[aimBit].indexOf(roleIdCheck) == -1){
                                var topicCheckBox = '<g:checkBox name="existingTopics" value="'+aimId+'_'+i+'_'+roleIdCheck+'" checked="true"/>';
                                $("#existingTopicsHidden_"+aimId).append(topicCheckBox);

                                var relationCheckBox = '<g:checkBox name="existingTopicRelations" value="'+aimId+'_'+i+'_'+association.associationType+'" checked="true"/>';
                                $("#existingTopicRelationHidden_"+aimId).append(relationCheckBox)

                                var roleCheckBox = '<g:checkBox name="existingTopicRelationRoles" value="'+aimId+'_'+i+'_'+rolePSI+'" checked="true"/>';
                                var checkOtherRoleBox = '<g:checkBox name="existingTopicOtherRelationRoles" value="'+aimId+'_'+i+'_'+otherRolePSI+'" checked="true"/>';
                                $("#existingTopicRelationRoleHidden_"+aimId).append(roleCheckBox)
                                $("#existingTopicOtherRelationRoleHidden_"+aimId).append(checkOtherRoleBox)
                            }




                    }
                    html += '</div>';
                    existingAssociations[aimBit].push(roleIdCheck)
                }//end for assocs

                $("#existingTopics_"+aimId).html(html)

            });
        }

        function getTopicsBySubjectMatter(){
                var subjectId = $("#subjectId").val()
                var site = $("#site").val()

                var limit = 10;
                var start = 0;

                var host = window.location.protocol + '//' + window.location.host;
                var serviceUrl = host + pathPrefix+'/service/getTopicsBySubjectMatterId'

                $.ajax({
                    async: false,
                    url: serviceUrl,
                    data: {
                        'subjectId': subjectId,
                        'site': site,
                        'limit': limit,
                        'offset' : start
                    },
                    dataType: 'json',
                }).done(function (data) {
                    var topics = processOntologyJson(data)
                    $("#ontologyTopics").val(JSON.stringify(topics))
                });
            }

        function hasDoubleRelations(associations,aimId,topicId){
            var hasDoubleRelations = false;
            var found = {};
            for(var i = 0; i < associations.length; i++) {
                var association = associations[i];
                var associationId = association.associationType

                if(typeof found[associationId] == 'undefined'){
                  found[associationId+"_"+i] = {}
                }

                var rolePlayers = association.rolePlayers;
                for (var j = 0 ; j < rolePlayers.length; j++){
                    var rolePlayer = rolePlayers[j];
                    if(rolePlayer.rolePlayer.indexOf("competence-aim#") == -1){
                        var topicRoleId = rolePlayer.rolePlayer.substring(rolePlayer.rolePlayer.indexOf("#")+1);
                        if(topicRoleId == topicId){
                            found[associationId+"_"+i]['topicid'] = topicRoleId;
                        }
                    }
                    else{
                        var aimRoleId = rolePlayer.rolePlayer.substring(rolePlayer.rolePlayer.indexOf("#")+1);
                        if(aimRoleId == aimId){
                            found[associationId+"_"+i]['aimid'] = aimRoleId;
                        }
                    }
                }
            }//end for

            var foundcounter = 0;
            var onlyAimTopicCounter = 0;
            $.each(found,function(foundAssocId,foundData){
                if(foundData.topicid == topicId && foundData.aimid == aimId){
                    foundcounter++;
                }
                else if(typeof foundData.aimid == 'undefined' && foundAssocId.contains("aim_has_topic")){
                    onlyAimTopicCounter++;
                }

            });
            //console.log("GOT FOUNDCOUNTER "+foundcounter)
            //console.log("GOT onlyAimTopicCounter "+onlyAimTopicCounter)
            if(foundcounter > 1 && onlyAimTopicCounter == 0){
                hasDoubleRelations = true;
            }
            return hasDoubleRelations;
        }

        function editAimTopicAssociation(aimId,item, itemId,counter){
            //var topicCount = (parseInt($("#navPills_"+aimId).children("span").length)+1)
            var assocTypes = getAssociationTypes(aimId,counter)

            $("#navPills_"+aimId).append('<br /><div class="label label-info">'+item+'</div><span id="assocTypeWrapper_'+aimId+'_'+counter+'" class="assocTypeWrapper">'+assocTypes+'</span><a href="javascript: " class=""><a id="showSwap_'+aimId+'_'+counter+'" href="javascript:" onclick="showSelect(\''+aimId+'_'+counter+'\')"><i class="icon-plus-sign"></i></a><div id="assocRoleWrapper_'+aimId+'_'+counter+'"></div>');
                var checkBox = '<g:checkBox name="chosenTopics" value="'+aimId+'_'+counter+'_'+itemId+'" checked="true"/>';
            $("#chosenTopics_"+aimId).append(checkBox);
            $("#existingButtonGroup_"+counter).remove();

            /*
            $("#existingTopicRelationHidden_"+aimId).find("input").each(function(){
                if(typeof $(this).val() != "undefined"){
                    if($(this).val().indexOf(aimId+"_"+counter) > -1) {
                        $(this).remove();
                    }
                }
                else{
                    $(this).remove();
                }
            });

            $("#existingTopicRelationRoleHidden_"+aimId).find("input").each(function(){
                if(typeof $(this).val() != "undefined"){
                    if($(this).val().indexOf(aimId+"_"+counter) > -1) {
                        $(this).remove();
                    }
                }
                else{
                    $(this).remove();
                }
            });

            $("#existingTopicOtherRelationRoleHidden_"+aimId).find("input").each(function(){
                if(typeof $(this).val() != "undefined"){
                    if($(this).val().indexOf(aimId+"_"+counter) > -1) {
                        $(this).remove();
                    }
                }
                else{
                    $(this).remove();
                }
            });
            */
        }

        function deleteAimTopicAssociation(aimId,itemId, relationType,counter) {
            var site = $("#site").val()
            var host = window.location.protocol + '//' + window.location.host;
            var serviceUrl = host + pathPrefix+'/service/removeTopicsByAimId'
            var conjoinedId = aimId;
            var idArr = aimId.split("_");
            var aimId = idArr[2]
            $("#existingButtonGroup_"+aimId+'_'+counter).remove();
            $.ajax({
                async: false,
                url: serviceUrl,
                dataType: 'json',
                data: {
                'topicId' : aimId,
                'relatedTopicId' : itemId.substring(itemId.indexOf("#")+1),
                'relationType' : relationType,
                'site' : site
                }

            }).done(function (data) {
                var html = '<div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">×</button><strong>Success!</strong> The assocciation to topic ';
                for(var i = 0; i <  data.length; i ++) {
                    html += data[i];
                }
                html += ' was sucessfully removed from the aim '+aimId+' for relation '+relationType+'</div>';
                $('#flash').html(html);
            });

            $.ajax({
                async: false,
                url: serviceUrl,
                dataType: 'json',
                data: {
                'topicId' : aimId,
                'relatedTopicId' : itemId.substring(itemId.indexOf("#")+1),
                'relationType' : 'aim_has_topic',
                'site' : site
                }

            }).done(function (data) {
                var html = '<div class="alert alert-success"><button class="close" data-dismiss="alert" type="button">×</button><strong>Success!</strong> The assocciation to topic ';
                for(var i = 0; i <  data.length; i ++) {
                    html += data[i];
                }
                html += ' was sucessfully removed from the aim '+aimId+' for relation aim_has_topic</div>';
                $('#flash').html(html);
            });

            $("#existingTopicsHidden_"+conjoinedId).find("input").each(function(){
                if(typeof $(this).val() != "undefined"){
                    if($(this).val().indexOf(aimId+"_"+counter) > -1) {
                        $(this).remove();
                    }
                }
                else{
                    $(this).remove();
                }
            });


            $("#existingTopicRelationHidden_"+conjoinedId).find("input").each(function(){
                if(typeof $(this).val() != "undefined"){
                    if($(this).val().indexOf(aimId+"_"+counter) > -1) {
                        $(this).remove();
                    }
                }
                else{
                    $(this).remove();
                }
            });

            $("#existingTopicRelationRoleHidden_"+conjoinedId).find("input").each(function(){
                if(typeof $(this).val() != "undefined"){
                    if($(this).val().indexOf(aimId+"_"+counter) > -1) {
                        $(this).remove();
                    }
                }
                else{
                    $(this).remove();
                }
            });

            $("#existingTopicOtherRelationRoleHidden_"+conjoinedId).find("input").each(function(){
                if(typeof $(this).val() != "undefined"){
                    if($(this).val().indexOf(aimId+"_"+counter) > -1) {
                        $(this).remove();
                    }
                }
                else{
                    $(this).remove();
                }
            });
        }

         function getAssociationRoles(associationId,associationType, id, topicCount,selectId){
            var host = window.location.protocol + '//' + window.location.host;
            var serviceUrl = host + pathPrefix+'/service/getAssociationRoles'

            var theId = ""
            console.log("ASSOCID:  => "+associationId+" ASSOCTYPE: "+associationType+" ID: "+id+" TOPICCOUNT: "+topicCount+" SELECTNAME: "+selectName+" SELECTID: "+selectId)
            if(associationId.indexOf("#") > -1){
                theId = associationId.substring(associationId.indexOf("#")+1);
            }
            else{
                theId = associationId.substring(associationId.lastIndexOf("/")+1);
            }

            //var selectName = $("#"+associationType+"RelationtypeSelect_"+id+"_"+topicCount+" option:selected").text()
            var selectName = $("#"+associationType+"RelationtypeSelect_"+selectId+" option:selected").text()
            var selectNames = selectName.split(" / ")

            var result = [];
            var found = []
            $.ajax({
                async: false,
                url: serviceUrl,
                dataType: 'json',
                data: {
                'psiId' : theId,
                'associationType' : associationType
                }

            }).done(function (data) {
                for(var i = 0; i <  data.length; i ++) {
                    if(selectNames.indexOf(data[i].roleName) > -1 && found.indexOf(data[i].roleId) == -1){
                        result.push(data[i])
                        found.push(data[i].roleId)
                    }
                }

            });
            return result;
        }


        function hideSelect(elm){

            var selectId = ""
            if(typeof elm == "string"){
                selectId = elm
            }
            else{
                selectId = $(elm).attr("id");
            }
            console.log("selectId: "+selectId)
            var idarr = selectId.split("_");
            var newSelectId = idarr[1]+"_"+idarr[2]+"_"+idarr[3]+"_"+idarr[4];
            console.log("newSelectId: "+newSelectId)
            var id = idarr[3]
            var topicCount = idarr[4]
            var selectedRelation = ""
            $("#swap_"+id+'_'+topicCount).remove();

            $("#chosenTopicRelation_"+id).find("input").each(function (){

                if(typeof $(this).val() != "undefined"){
                    var thisId = $(this).val();
                    thisIdArray = thisId.split("_");
                    thisAimId = thisIdArray[0];
                    thisTopicCount = thisIdArray[1];
                    if(thisAimId ==  idarr[1] && thisTopicCount == idarr[2]) {
                        $(this).remove();
                    }
                    if($(this).attr("name") == "_chosenTopicRelations"){
                        $(this).remove();
                    }
                }

            });

            var roleTypes = null

            if(selectId.contains("hierarchicalRelationtypeSelect")){
                //selectedRelation = $('#hierarchicalRelationtypeSelect_'+id+'_'+topicCount+' option:selected').val()
                //var selectedName = $('#hierarchicalRelationtypeSelect_'+id+'_'+topicCount+' option:selected').text()
                selectedRelation = $('#hierarchicalRelationtypeSelect_'+newSelectId+' option:selected').val()
                var selectedName = $('#hierarchicalRelationtypeSelect_'+newSelectId+' option:selected').text()
                roleTypes = getAssociationRoles(selectedRelation,"hierarchical", id, topicCount,newSelectId)

                $("#horizontalRelationtypeSelect_"+newSelectId).css("display","none");
                $("#horizontalRelationtypeSelect_"+newSelectId).val($("#horizontalRelationtypeSelect_"+"_"+topicCount+" option:first").val());

                if($("#hierarchicalRelationtypeSelect_"+newSelectId).css("display") == "none") {
                    $("#hierarchicalRelationtypeSelect_"+newSelectId).css("display","inline");
                }
                $("#assocTypeWrapper_"+newSelectId).append('<a id="swap_'+newSelectId+'" href="javascript:" onclick="hideSelect(\'horizontalRelationtypeSelect_'+newSelectId+'\')"><i class="icon-refresh"></i></a>');
            }
            else if(selectId.contains("horizontalRelationtypeSelect")){
                //selectedRelation = $('#horizontalRelationtypeSelect_'+id+'_'+topicCount+' option:selected').val()
                //var selectedName = $('#hierarchicalRelationtypeSelect_'+id+'_'+topicCount+' option:selected').text()
                selectedRelation = $('#hierarchicalRelationtypeSelect_'+newSelectId+' option:selected').val()
                var selectedName = $('#hierarchicalRelationtypeSelect_'+newSelectId+' option:selected').text()
                roleTypes = getAssociationRoles(selectedRelation,"horizontal", id, topicCount,newSelectId)

                $("#hierarchicalRelationtypeSelect_"+newSelectId).css("display","none");
                $("#hierarchicalRelationtypeSelect_"+newSelectId).val($("#hierarchicalRelationtypeSelect_"+newSelectId+" option:first").val());
                if($("#horizontalRelationtypeSelect_"+newSelectId).css("display") == "none") {
                    $("#horizontalRelationtypeSelect_"+newSelectId).css("display","inline");
                }
                $("#assocTypeWrapper_"+newSelectId).append('<a id="swap_'+newSelectId+'" href="javascript:" onclick="hideSelect(\'hierarchicalRelationtypeSelect_'+newSelectId+'\')"><i class="icon-refresh"></i></a>');
            }

            if(selectedRelation != ""){
                var checkBox = '<g:checkBox name="chosenTopicRelations" value="'+newSelectId+'_'+selectedRelation+'" checked="true"/>';
                $("#chosenTopicRelation_"+id).append(checkBox)
            }

            if(roleTypes != "") {
                var radios = '';
                for(var i = 0; i < roleTypes.length; i ++) {
                    var rolePlayerName = roleTypes[i].roleId.substring(roleTypes[i].roleId.lastIndexOf("#")+1)
                    radios += '<label class="radio"><input type="radio" name="roleRadios_'+newSelectId+'_'+i+'" id="roleRadios_'+newSelectId+'_'+i+'" value="'+newSelectId+'§'+selectedRelation+'§'+roleTypes[i].roleId+'" onchange="setRole(\'roleRadios_'+newSelectId+'_'+i+'\')" />'+roleTypes[i].roleName+' ('+rolePlayerName+')</label>'
                }
                $("#assocRoleWrapper_"+newSelectId).html(radios)
            }


        }

        function setRole(id) {
            var idArray = id.split("_")
            var aimId = idArray[1]
            var topicCount = idArray[2]
            var roleURI = $("#"+id).val()
            var roleUriArray = roleURI.split("§")
            var rolePSI = roleUriArray[3]
            var otherRolePSI = ""
            var parentId = "assocRoleWrapper_"+idArray[1]+"_"+idArray[2]
            $("#"+parentId+" label > input").each(function (){
             var otherRoleUriArray = $(this).val().split("§")
             var otherRolePSITemp = otherRoleUriArray[3]
              if(otherRolePSITemp != rolePSI){
                otherRolePSI = otherRolePSITemp;
              }
            })



            var checkBox = '<g:checkBox name="chosenTopicRelationRoles" value="'+aimId+'_'+topicCount+'_'+rolePSI+'" checked="true"/>';
            var checkOtherBox = '<g:checkBox name="chosenTopicOtherRelationRoles" value="'+aimId+'_'+topicCount+'_'+otherRolePSI+'" checked="true"/>';
            $("#chosenTopicRelationRole_"+aimId).append(checkBox)
            $("#chosenTopicOtherRelationRole_"+aimId).append(checkOtherBox)
        }

        function processJson(json,langMap) {
                    var result = new Array();
                    for(var i = 0; i < json.length; ++i) {
                        dataObject = {}

                        var neutral_title = json[i].title_language_neutral
                        jQuery.each(json[i], function(key, val) {
                            if(key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                                var split_arr = key.split("_");
                                if(langMap.indexOf(split_arr[1]) > -1){
                                    dataObject[key] = val;
                                }
                            }
                        });

                        neutral_title = typeof (neutral_title) ==  'undefined' ? json[i].title_nob : json[i].title_language_neutral;

                        dataObject['identifier'] = json[i].id
                        dataObject['title'] = neutral_title
                        dataObject['wordclass'] = json[i].wordclass;
                        dataObject['visibility'] = json[i].visibility;
                        dataObject['types'] = json[i].type_id;
                        dataObject['approved'] = json[i].approved;

                        result.push(dataObject);
                    }
                    return result;
                }

        function processOntologyJson(json) {
            var result = new Array();
            for(var i = 0; i < json.length; ++i) {
                result.push(json[i].identifier);
            }
            return result;
        }
</r:script>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:3]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:1]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">

                <div class="tab-content">
                    <div class="tab-pane active" id="tab1-1">
                        <g:form name="getAimTopicsForm" useToken="true" action="doSaveAimTopics">
                            <g:hiddenField name="subjectId" value="${subjectId}" />
                            <g:hiddenField name="subjectCurriculumId" value="${subjectCurriculumId}" />
                            <g:hiddenField name="curriculumId" value="${curriculumRenderArray.id}" />
                            <g:hiddenField name="searchDomain" value="${searchDomain}" />
                            <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />
                            <g:hiddenField name="site" id="site" value="${site}" />
                            <g:hiddenField name="languages" id="languages" value="${langMap}" />
                            <g:hiddenField name="ontologyTopics" id="ontologyTopics" value="" />
                            <g:link controller="curriculum" action="getSubjects" params="['data-toggle': 'tab']">Back</g:link>
                            <fieldset>
                                <legend>${curriculumRenderArray.curriculumName}</legend>
                                <g:set var="levelCounter" value="0" />
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs">
                                       <g:each in="${curriculumRenderArray.levels}" var="curriculumLevel">
                                           <g:if test="${levelCounter.toString().equals("0")}">
                                                <li class="active"><a data-toggle="tab" href="#${curriculumLevel.key}">${nameMap[curriculumLevel.key]}</a></li>
                                           </g:if>
                                           <g:else>
                                               <li><a data-toggle="tab" href="#${curriculumLevel.key}">${nameMap[curriculumLevel.key]}</a></li>
                                           </g:else>
                                           <g:set var="levelCounter" value="${levelCounter = (Integer.parseInt(levelCounter.toString())+1).toString()}" />
                                       </g:each>
                                    </ul>

                                <g:set var="setCounter" value="0" />
                                <div class="tab-content">
                                    <g:each in="${curriculumRenderArray.levels}" var="curriculumLevelContent">
                                        <g:if test="${setCounter.toString().equals("0")}">
                                            <div class="tab-pane active" id="${curriculumLevelContent.key}">
                                        </g:if>
                                        <g:else>
                                            <div class="tab-pane" id="${curriculumLevelContent.key}">
                                        </g:else>



                                        <g:set var="setMainCounter" value="0" />
                                        <div class="tabbable clearfix">
                                            <ul class="nav nav-tabs">
                                                <g:each in="${curriculumLevelContent.value}" var="curriculumSet">
                                                    <g:if test="${curriculumSet.value != null}">
                                                        <g:if test="${setMainCounter.toString().equals("0")}">
                                                            <li class="active"><a data-toggle="tab" href="#${curriculumSet.key}">${nameMap[curriculumSet.key]}</a></li>
                                                        </g:if>
                                                        <g:else>
                                                            <li><a data-toggle="tab" href="#${curriculumSet.key}">${nameMap[curriculumSet.key]}</a></li>
                                                        </g:else>
                                                        <g:set var="setMainCounter" value="${setMainCounter = (Integer.parseInt(setMainCounter.toString())+1).toString()}" />
                                                    </g:if>

                                                </g:each>
                                            </ul>
                                        </div>

                                        <g:set var="setAimCounter" value="0" />
                                        <div class="tab-content">
                                        <g:each in="${curriculumLevelContent.value}" var="curriculumTab">
                                            <g:if test="${setAimCounter.toString().equals("0")}">
                                                <div class="tab-pane active" id="${curriculumTab.key}">
                                                <g:set var="uniqueParentArr" value="${curriculumTab.key.toString().split("_")}" />
                                            </g:if>
                                            <g:else>
                                                <div class="tab-pane" id="${curriculumTab.key}">
                                            </g:else>
                                            <g:set var="setAimCounter" value="${setAimCounter = (Integer.parseInt(setAimCounter.toString())+1).toString()}" />
                                            <div class="accordion" id="${curriculumTab.key}_aimAccordion">
                                                <g:each in="${curriculumTab.value}" var="competenceAim">
                                                    <div class="accordion-group">
                                                        <div class="accordion-heading">
                                                            <a class="accordion-toggle" data-toggle="collapse"  href="#collapse${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}" data-parent="#${curriculumTab.key}_aimAccordion">
                                                                ${competenceAim.value}
                                                            </a>
                                                        </div>

                                                        <div id="collapse${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}" class="accordion-body collapse" style="height: 0px;">
                                                            <div class="accordion-inner" id="inner_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}">
                                                                <div id="existingTopics_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}" class="dropdown"></div>
                                                                <label>Search for topic</label>
                                                                <input type="text" data-typeahead-target="chosenTopics_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}" class="topicTitleSearch" name="topicTitle_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}" id="topicTitle_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}" placeholder="Type a topic title..." autocomplete="off">
                                                                <br />

                                                                <div class="nav" id="navPills_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}">

                                                                </div>
                                                                <div class="chosenTopicsHidden" id="chosenTopics_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}">
                                                                </div>
                                                                <div class="chosenTopicsRelationHidden" id="chosenTopicRelation_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}">
                                                                </div>
                                                                <div class="chosenTopicsRelationRolesHidden" id="chosenTopicRelationRole_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}">
                                                                </div>
                                                                <div class="chosenTopicsRelationOtherRolesHidden" id="chosenTopicOtherRelationRole_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}">
                                                                </div>

                                                                <div class="existingTopicsHidden" id="existingTopicsHidden_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}">
                                                                </div>
                                                                <div class="chosenTopicsRelationHidden" id="existingTopicRelationHidden_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}">
                                                                </div>
                                                                <div class="chosenTopicsRelationRolesHidden" id="existingTopicRelationRoleHidden_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}">
                                                                </div>
                                                                <div class="chosenTopicsRelationOtherRolesHidden" id="existingTopicOtherRelationRoleHidden_${uniqueParentArr[1]+"_"+uniqueParentArr[2]+"_"+competenceAim.key}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </g:each>
                                             </div>
                                            </div>
                                        </g:each>
                                        </div>



                                        <g:set var="setCounter" value="${setCounter = (Integer.parseInt(setCounter.toString())+1).toString()}" />
                                        </div>



                                    </g:each>
                                </div>

                                </div>

                                    <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                            </fieldset>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>