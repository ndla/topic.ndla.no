<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Welcome to NDLA Topics Admin Application</title>
	</head>
	<body>
		<ul class="nav nav-list">
			<li class="nav-header">Actions</li>
			<li><g:link controller="keyword" action="updateKeyword" params="[lang: 'en']">Keywords</g:link></li>
			<li><g:link controller="topic" action="editTopics" params="[lang: 'en']">Topics</g:link></li>
            <li><g:link controller="ontology" action="getSubjects" params="[lang: 'en',,'data-toggle': 'tab', 'removeSubject' : 'false']">Subject Ontologies</g:link></li>
			<li><g:link controller="curriculum" action="getSubjects" params="[lang: 'en']">Curriculum Ontologies</g:link></li>
            <li><g:link controller="language" action="getLanguages" params="[lang: 'en']">Language</g:link></li>
            <li><g:link controller="indexSOLR" action="updateIndexAll" params="[lang: 'en']">SOLR index</g:link></li>
		</ul>
	</body>
</html>