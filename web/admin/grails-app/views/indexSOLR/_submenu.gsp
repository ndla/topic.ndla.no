<ul class="nav nav-tabs">
    <g:if test="${param == 1}">
        <li class="active"><a href="#tab1" data-toggle="tab">Update all keywords</a></li>
        <li><g:link controller="indexSOLR" action="deleteIndexAll" params="['data-toggle': 'tab']">Delete all keywords</g:link></li>
        <li><g:link controller="indexSOLR" action="updateIndexAllTopics" params="['data-toggle': 'tab']">Update all topics</g:link></li>
        <li><g:link controller="indexSOLR" action="deleteIndexAllTopics" params="['data-toggle': 'tab']">Delete all topics</g:link></li>
    </g:if>

    <g:if test="${param == 2}">
        <li><g:link controller="indexSOLR" action="updateIndexAll" params="['data-toggle': 'tab']">Update all keywords</g:link></li>
        <li class="active"><a href="#tab1" data-toggle="tab">Delete all keywords</a></li>
        <li><g:link controller="indexSOLR" action="updateIndexAllTopics" params="['data-toggle': 'tab']">Update all topics</g:link></li>
        <li><g:link controller="indexSOLR" action="deleteIndexAllTopics" params="['data-toggle': 'tab']">Delete all topics</g:link></li>

    </g:if>

    <g:if test="${param == 3}">
        <li><g:link controller="indexSOLR" action="updateIndexAll" params="['data-toggle': 'tab']">Update all keywords</g:link></li>
        <li><g:link controller="indexSOLR" action="deleteIndexAll" params="['data-toggle': 'tab']">Delete all keywords</g:link></li>
        <li class="active"><a href="#tab1" data-toggle="tab">Update all topics</a></li>
        <li><g:link controller="indexSOLR" action="deleteIndexAllTopics" params="['data-toggle': 'tab']">Delete all topics</g:link></li>
    </g:if>

    <g:if test="${param == 4}">
        <li><g:link controller="indexSOLR" action="updateIndexAll" params="['data-toggle': 'tab']">Update all keywords</g:link></li>
        <li><g:link controller="indexSOLR" action="deleteIndexAll" params="['data-toggle': 'tab']">Delete all keywords</g:link></li>
        <li><g:link controller="indexSOLR" action="updateIndexAllTopics" params="['data-toggle': 'tab']">Update all topics</g:link></li>
        <li class="active"><a href="#tab1" data-toggle="tab">Delete all topics</a></li>

    </g:if>
</ul>