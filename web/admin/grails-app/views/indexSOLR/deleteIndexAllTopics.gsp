<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Keywords</title>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:5]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:4]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tab-content">
                <g:form name="deleteIndexAllForm" useToken="true"  action="doDeleteIndexAllTopics" autocomplete="off">
                    <g:submitButton name="submit" class="btn btn-danger" value="Delete Autocomplete Index" />
                </g:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>