<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Keywords</title>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:5]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:1]"  />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tab-content">
                <g:form name="updateIndexAllForm" useToken="true"   action="doUpdateIndex" autocomplete="off">
                    <label>Site</label>
                    <g:select  id="siteSelect" name="siteSelect" from="${['NDLA', 'DELING', 'NYGIV']}"
                               noSelection="${['null':'Select One...']}" />
                    <br />
                    <br />
                    <g:submitButton name="submit" class="btn btn-danger" value="Update Autocomplete Index" />
                </g:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>