<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Keywords</title>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:5]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:3]"  />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tab-content">
                <g:form name="updateIndexAllForm" useToken="true"   action="doUpdateIndexTopics" autocomplete="off">

                    <g:submitButton name="submit" class="btn btn-danger" value="Update Topics Autocomplete Index" />
                </g:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>