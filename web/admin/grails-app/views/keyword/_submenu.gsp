<ul class="nav nav-tabs">
    <g:if test="${param == 1}">
        <li class="active"><a href="#tab1" data-toggle="tab">Lookup keyword</a></li>
        <li><g:link controller="keyword" action="updateKeyword" params="['data-toggle': 'tab']">Edit keyword</g:link></li>
        <!--li><g:link controller="keyword" action="removeKeywordNodes" params="['data-toggle': 'tab']">Remove Content Relation</g:link></li-->
        <li><g:link controller="keyword" action="getKeywordDuplicates" params="['data-toggle': 'tab']">Find duplicates</g:link></li>
     </g:if>
    <g:if test="${param == 2}">
        <li><g:link controller="keyword" action="lookupKeyword" params="['data-toggle': 'tab']">Lookup keyword</g:link></li>
        <li class="active"><g:link controller="keyword" action="updateKeyword" params="['data-toggle': 'tab']">Edit keyword</g:link></li>
        <!--li><g:link controller="keyword" action="removeKeywordNodes" params="['data-toggle': 'tab']">Remove Content Relation</g:link></li-->
        <li><g:link controller="keyword" action="getKeywordDuplicates" params="['data-toggle': 'tab']">Find duplicates</g:link></li>
    </g:if>
    <g:if test="${param == 3}">
        <li><g:link controller="keyword" action="lookupKeyword" params="['data-toggle': 'tab']">Lookup keyword</g:link></li>
        <li><g:link controller="keyword" action="updateKeyword" params="['data-toggle': 'tab']">Edit keyword</g:link></li>
        <!--li class="active"><a href="#tab3" data-toggle="tab">Remove Content Relation</a></li-->
        <li><g:link controller="keyword" action="getKeywordDuplicates" params="['data-toggle': 'tab']">Find duplicates</g:link></li>
    </g:if>
    <g:if test="${param == 4}">
        <li><g:link controller="keyword" action="lookupKeyword" params="['data-toggle': 'tab']">Lookup keyword</g:link></li>
        <li><g:link controller="keyword" action="updateKeyword" params="['data-toggle': 'tab']">Edit keyword</g:link></li>
        <!--li><g:link controller="keyword" action="removeKeywordNodes" params="['data-toggle': 'tab']">Remove Content Relation</g:link></li-->
        <li class="active"><a href="#tab3" data-toggle="tab">Find duplicates</a></li>
    </g:if>
</ul>
