<!doctype html>
<html>
	<head>
	    <meta name="layout" content="main"/>
	    <title>NDLA Topics Admin Application - Keywords</title>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<a class="brand" href="#">Actions</a>
				<ul class="nav">
					<li class="active"><g:link controller="keyword" action="removeKeyword">Keywords</g:link></li>
					<li><g:link controller="topic" action="index">Topics</g:link></li>
					<li><g:link controller="curriculum" action="index">Curricula</g:link></li>
					<li><g:link controller="metadataset" action="index">Metadata Sets</g:link></li>
				</ul>
			</div>
		</div>
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li><g:link controller="keyword" action="removeKeyword" params="['data-toggle': 'tab']">Keyword</g:link></li>
				<li><g:link controller="keyword" action="removeKeywordNodes" params="['data-toggle': 'tab']">Remove Content Relation</g:link></li>
				<li><g:link controller="keyword" action="addNodeKeywords" params="['data-toggle': 'tab']">Add Content Relation</g:link></li>
				<li><g:link controller="keyword" action="addHierarchicalRelation" params="['data-toggle': 'tab']">Hierarchical Relation</g:link></li>
				<li class="active"><a href="#tab1" data-toggle="tab">Horizontal Relation</a></li>
				<li><g:link controller="keyword" action="merge" params="['data-toggle': 'tab']">Merge</g:link></li>
			</ul>
			<p>Add Horizontal Relation</p>
		</div>
	</body>
</html>