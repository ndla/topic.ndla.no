<!doctype html>
<html>
	<head>
	    <meta name="layout" content="main"/>
	    <title>NDLA Topics Admin Application - Keywords</title>
        <r:script>
            $(document).ready(function () {
                searchDomain = $("#searchDomain").val()
                $('#addLanguageButton').click(function () {
                    var selectedLanguageId = $("#languages option:selected").val()
                    var selectedLanguageText = $("#languages option:selected").text()
                    $("#keywordNames").append('<label>' + selectedLanguageText + ' title</label><input type="text" name="name_' + selectedLanguageId + '" id="name_' + selectedLanguageId + '" placeholder="type a ' + selectedLanguageText + ' title" autocomplete="off" value="">');
                    $("#languages option:selected").remove();

                });

                $('body').on('click', "input[name=typeSelect]" ,function(){
                    if(!$(this).is(':checked')) {
                        $(this).parent().remove();
                    }
                });

                $('#keywordByIdTypeTitle').typeahead ({
                    source: function (query, process) {
                    keywords = [];
                    map = {};
                    langMap = [];
                    //var serviceUrl = "http://search.test.ndla.no/ndla_search/keywords/" + query;
                    var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                    var searchLanguage = ""
                    var searchType = $('#typeSelect').val();
                    $( "input[name=languageSelect]" ).each(function(){
                        if($(this).is(':checked') && $(this).val() != ""){
                            var langId = $(this).val().substr($(this).val().indexOf("#")+1)
                            searchLanguage += "language[]="+langId+"&";
                            langMap.push($(this).val())
                        }
                    });
                    searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
                    serviceUrl += "?" + searchLanguage + "&type=" + searchType;

                    $.ajax({
                        url: serviceUrl,
                        type: 'GET',
                        dataType: 'json',
                        success: function (response) {
                            var data = processJson(response, langMap);
                            $.each(data, function (i, keyword) {
                                var key = keyword.title;

                                if (!(key in map)) {
                                    map[key] = keyword;
                                    keywords.push(key);
                                }
                            });
                            process(keywords);
                        }

                    }, 300);
                    },
                     matcher: function (item) {
                            return true;
                    },
                    updater: function (item) {
                        var typeCheckBoxes = ""
                        for(mkey in map[item]){
                        var stringVal = map[item][mkey];
                        if(typeof(stringVal) != "undefinded"){
                        console.log(mkey)
                            if(mkey.contains('title_eng')) {
                                var langKey = mkey.substr(mkey.lastIndexOf("_")+1);
                                 var checkBox = '<label>'+stringVal+' <g:checkBox name="typeSelect"
                                                                                  value="'+map[item].identifier+'"
                                                                                  checked="true"/></label>';
                                 typeCheckBoxes += checkBox;
                            }
                        }
                      }
                      $("#keyword_types").append(typeCheckBoxes);
                    }
                });

            });

            function processJson(json,langMap) {
                var result = new Array();
                for(var i = 0; i < json.length; ++i) {
                    dataObject = {}

                    var neutral_title = json[i].title_language_neutral
                    jQuery.each(json[i], function(key, val) {
                        if(key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                            var split_arr = key.split("_");
                            if(langMap.indexOf(split_arr[1]) > -1){
                                dataObject[key] = val;
                            }
                        }
                    });

                    neutral_title = typeof (neutral_title) ==  'undefined' ? json[i].title_nob : json[i].title_language_neutral;

                    dataObject['identifier'] = json[i].id
                    dataObject['title'] = neutral_title
                    dataObject['wordclass'] = json[i].wordclass;
                    dataObject['visibility'] = json[i].visibility;
                    dataObject['types'] = json[i].type_id;
                    dataObject['approved'] = json[i].approved;

                    result.push(dataObject);
                }
                return result;
            }
        </r:script>
	</head>
	<body>
    <div class="navbar">
        <g:render template="/actionmenu"model="[param:1]" />
    </div>
		<div class="tabbable">
            <g:render template="submenu" model="[param:2]" />
			<div class="tab-content">
				<div class="tab-pane active" id="tab1">
					<div class="tabbable tabs-right">
						<ul class="nav nav-tabs">
							<li><g:link controller="keyword" action="removeKeyword" params="['data-toggle': 'tab']">Remove</g:link></li>
							<li><g:link controller="keyword" action="updateKeyword" params="['data-toggle': 'tab']">Update</g:link></li>
							<li class="active"><a href="#tab1-3" data-toggle="tab">Add</a></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab1-3">
								<g:form name="addKeywordForm" useToken="true" action="doAddKeyword">
                                    <g:hiddenField name="searchDomain" value="${searchDomain}" />
                                    <g:each in="${langMap}" var="lang">
                                        <g:checkBox name="languageSelect" class="hiddenLanguageSelect" value="${lang.key}" /></label>
                                    </g:each>
									<fieldset>
										<legend>Add Keyword</legend>
                                        <div id="keywordNames">
                                            <label>English Title</label>
                                            <input type="text" name="name_eng" placeholder="Type an English title...">

                                            <label>Bokmål Title</label>
                                            <input type="text" name="name_nob" placeholder="Type a Bokmål title...">
                                        </div>
                                        <g:select id="languages" from="${nameLangs.entrySet()}" name="languages" optionKey="key" optionValue="value" noSelection="['':'Add a language']"/> <button class="btn" id="addLanguageButton" type="button">Go!</button>
                                        <label>Word Class</label>
                                        <g:select name="wordClass" from="${wordClasses}" noSelection="['':'-Choose word class-']" />
                                        <label>Visible?</label>
                                        <label class="radio">
                                            <g:radio name="visibilityRadios" id="visibilityRadios1" value="visibilityOption1" checked="true" />Yes
                                        </label>
                                        <label class="radio">
                                            <g:radio name="visibilityRadios" id="visibilityRadios2" value="visibilityOption2" />No
                                        </label>


                                        <div id="keywordTypesWrapper">
                                            <div id="keyword_types">
                                                <p id="keyword_type_label">Keyword types</p>
                                            </div>
                                            <label>Add a new type <input type="text" name="keywordByIdTypeTitle" id="keywordByIdTypeTitle" placeholder="Type a type identifier..." autocomplete="off"> </label>
                                        </div>

										<br />
										<br />
										<g:submitButton name="submit" class="btn btn-success" value="Add" />
									</fieldset>
								</g:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>