<!doctype html>
<html>
    <head>
        <meta name="layout" content="main"/>
        <title>NDLA Topics Admin Application - Keywords</title>
        <style>
        #myModal {
            height: auto;
            margin: 0 0 0 -400px;
            max-height: 600px;
            max-width: 800px;
            width: auto;
            overflow: scroll;
        }
        </style>
        <r:script>
            function processLink(identifier) {
                var resourceHost = $("#resourceHost").val();
                var nodeData = JSON.parse($("#nodeData").val());
                var currentNodeData = nodeData[identifier];
                console.log("currentNodeData: "+JSON.stringify(currentNodeData))


                var html = "<ul>";
                for(var i = 0; i < currentNodeData.length; i++){
                    var currentNode =  currentNodeData[i];
                    var nodeidArray = currentNode.identifier.split("_");
                    var siteId = nodeidArray[0]
                    var nid = nodeidArray[1]
                    if(siteId.indexOf("nygiv") > -1){
                        resourceHost = "http://fyr.ndla.no";
                    }
                    var nodeid = "node/"+nid
                    html += '<li><a href="'+resourceHost+'/'+nodeid+'" target="_blank">'+currentNode.name+'</a> (node : '+nid+')</li>';
                }
                html += "</ul>";
                //console.log("html: "+html)

                var labelContent = "NDLA node preview [" + identifier + "]";
                $("#myModalLabel").html(labelContent);
                $(".modal-body").html(html);
            }


        </r:script>
        <r:script>
            $(document).ready(function () {
                var tokens = getTokens();
                searchDomain = $("#searchDomain").val()
                pathPrefix = $("#pathPrefix").val()
                getTable();

                $('#myModal').on('show', function () {
                    $(this).find('.modal-body').css({width: 'auto', height: 'auto', 'max-height': '100%'});
                });

                if (typeof tokens['query'] != 'undefined') {
                    searchTopics(tokens)
                }

                $('body').on('click', "input[data-merge-aim-radio]", function () {
                    var $theRadio = $(this);
                    var radioVal = $theRadio.attr('data-merge-aim-radio');
                    console.log("THERADIO: "+$theRadio+" RADIOVAL: "+radioVal)
                    $("body input[data-merge-aim-checkbox]").each(function(){
                        var $theCheck = $(this);
                        var checkVal = $theCheck.attr('data-merge-aim-checkbox');
                        if(checkVal == radioVal){
                            if(!$theCheck.is(':checked')){
                                $theCheck[0].checked = true;
                            }
                        }
                    });

                });

                $("#toggle-button")
                        .click(
                        function (event) {
                            $("#more-options").slideToggle();
                        });



                $(document).keypress(function(e) {
                    if(e.which == 13) {
                        e.preventDefault();
                    }
                });

            });

            function getTable(){
                $('#data-table-duplicates').html('<div class="cogspinner" />')
                var host = window.location.protocol + '//' + window.location.host;
                var serviceUrl = host + pathPrefix+'/service/getDuplicateTopics'
                var limit = 10;
                var start = 0;
                var queryParams = {}

                if (typeof tokens == 'undefined') {
                    urlTokens = getTokens();
                }
                else {
                    urlTokens = tokens;
                }


                if (typeof urlTokens['start'] != 'undefined') {
                    start = urlTokens['start'];
                }

                if(typeof urlTokens["limit"] != 'undefined') {
                    limit = urlTokens["limit"]
                }
                else{
                    /*
                    if(approvedState == "false" && typeof $('#limitUp').val() != 'undefined') {
                        limit = $('#limitUp').val();
                    }
                    else{
                        limit = 10;
                    }
                            */
                }

                var language = ""
                if(typeof urlTokens["lang"] != "undefined"){
                    language = urlTokens["lang"];
                }
                else{
                    language = $('input:radio[name=languageSelect]:checked').val();
                    queryParams['lang'] = language
                }


                queryParams['limit'] = limit


                $.ajax({
                    async: false,
                    url: serviceUrl,
                    data: {
                        'offset': start,
                        'type': 'keyword'
                    },
                    dataType: 'json'
                }).done(function (data) {
                    var duplicates = processJson(data, language)
                    //console.log(JSON.stringify(duplicates))
                    var dataHtml = renderDataTable(duplicates, start);
                    console.log(dataHtml)
                    $('#data-table-duplicates').html(dataHtml);

                    counter = data.length;
                });
            }

            Array.prototype.getRandom= function(cut){
                var i= Math.floor(Math.random()*this.length);
                if(cut && i in this){
                    return this.splice(i, 1)[0];
                }
                return this[i];
            }

            function renderDataTable(json, start) {
                var dataSize = json.length;
                var processMap = {};
                processMap["0"] = "Not processed";
                processMap["1"] = "Pending";
                processMap["2"] = "Processed";

                var colors= ['aqua', 'fuchsia', 'gray', 'green',
                    'lime', 'olive', 'orange', 'purple', 'red',
                    'silver', 'teal', 'white', 'yellow'];

                if (dataSize == 0) {
                    return '<span class="label label-warning">No Data!</span>';
                }
                var buffer = ['<table id="data-table" class="table table-bordered table-hover"><thead><tr><th>#</th><th>Names</th><!--th>PSIS</th--><th>ApprovedState</th><th>ProcessState</th><th>Has nodes</th></tr></thead><tbody>'];
                var counter;
                var listedSize = 0;
                for (var i = 0; i < dataSize; i++) {
                    counter = i + 1 + parseInt(start);
                    //var connectedNodes = getConnectedNodes(json[i].identifier,queryParams['sites']);
                    //var nodeCount = connectedNodes.length;
                    var nodeCount = 0;
                    //var subjectCount = getSubjectCount(json[i].identifier,'NDLA')
                    var row = ""
                    var duplicateObjects = json[i].duplicateObjects
                    var dupliLength = duplicateObjects.length
                    //console.log(JSON.stringify(json[i]));
                    row += '<tr><td>' + counter.toString() + '</td>';

                    var currentColor = ""

                    row +="<td> Hit on: <strong>"+json[i].duplicateString+"</strong><br />";
                    for (var j = 0; j < duplicateObjects.length;j++){
                        var duplicateObject = duplicateObjects[j];
                        var psis = duplicateObject.psis;
                        console.log(JSON.stringify(psis));
                        var names = duplicateObject.names;
                        row +='<div style="background-color: '+colors[j]+'">To:  <input type="radio" data-merge-aim-radio="'+psis[0]+'" name="chosenTo_'+i+'" value="'+psis[0]+'" /> <input type="checkbox" data-merge-aim-checkbox="'+psis[0]+'" name="merge_'+i+'_'+j+'"';
                        row += ' value="'+psis[0]+'"/>';
                        var identifier = psis[0].substring(psis[0].indexOf("#")+1)
                        for (var l = 0; l < names.length;l++){
                            var name = names[l];
                            var tempLanguage = name.language;
                            var language = tempLanguage.substring(tempLanguage.indexOf("#")+1);
                            var nameString = name.name
                            row += ' '+nameString+' ('+language+')';
                        }
                        row += ' <a href="updateKeywordByKeywordId?keywordId=' + identifier+'"><input type="button" value="Edit" class="btn btn-warning btn-mini"/> </a>'

                        row +="</div>";
                    }
                    row += '</td>';

                    var currentPsis = []
                    for (var k = 0; k < duplicateObjects.length;k++){
                        var duplicateObject = duplicateObjects[k];
                        var psis = duplicateObject.psis;
                        //row +='<ul>';
                        for (var l = 0; l < psis.length;l++){
                            var psi = psis[l];
                            var topicId = psi.substring(psi.indexOf("#")+1);
                            currentPsis.push(topicId)
                        }
                    }


                    row +="<td><br />";
                    for (var m = 0; m < duplicateObjects.length;m++){
                        var duplicateObject = duplicateObjects[m];
                        var approvedState = ""
                        if(typeof duplicateObject.approvalDate != 'undefinded'){
                            approvedState = duplicateObject.approvalDate;
                        }
                        row += '<div style="background-color: '+colors[m]+'">' + duplicateObject.approvedState + ' : ' + approvedState + '</div>';
                    }
                    row += '</td>';

                    row +="<td><br />";
                    for (var n = 0; n < duplicateObjects.length;n++){
                        var duplicateObject = duplicateObjects[n];
                        row += '<div  style="background-color: '+colors[n]+'">' + processMap[duplicateObject.processState] + '</div>';
                    }
                    row += '</td>';


                    row +="<td><br />";
                    for (var p = 0; p < currentPsis.length;p++){
                        var currentPsi = currentPsis[p];
                        var connectedNodes = getConnectedNodes(currentPsi);
                        var nodeCount = connectedNodes.length;
                        row += '<div style="background-color: '+colors[p]+'">' + nodeCount + ' <a href="#" onclick="processLink(\'' + currentPsi + '\');" data-target="#myModal" role="button" data-toggle="modal"><i class="icon-eye-open"></i></a></div>';
                        //row += '<li  style="background-color: '+colors[p]+'">' + duplicateObject.processState + '</li>';
                    }
                    row += '</td>'


                    //row += '<td> &nbsp;&nbsp;<a href="updateKeywordByKeywordId?keywordId=' + nodeCount+'"><input type="button" value="Merge" class="btn btn-warning"/> </a></td></tr>';

                    listedSize ++;

                    buffer.push(row + "\n");
                }
                buffer.push("</tbody></table>");
                if (start > 0) {
                    if(start >= 10){
                        buffer.push('<span id="pager-back"><a href="getKeywordDuplicates?start=' + (parseInt(start) - 10) + '&type=keyword">&lt;&lt; Previous 10 </a></span>');
                    }
                    if (listedSize >= 10) {
                        buffer.push('<span id="pager-forward"><a href="getKeywordDuplicates?start=' + (parseInt(start) + 10) + '&type=keyword">Next 10 &gt;&gt;</a></span>');
                    }
                }
                else {
                    if (listedSize >= 10) {
                        buffer.push('<span id="pager-forward"><a href="getKeywordDuplicates?start=' + (parseInt(start) + 10) + '&type=keyword">Next 10 &gt;&gt;</a></span>');
                    }
                }

                return buffer.join('');
            }



            function processJson(json, language) {
                var result = new Array();

                for (var i = 0; i < json.length; ++i) {
                    dataObject = {}

                    dataObject['duplicateString'] = json[i].duplicateString


                    var duplicateObjects = new Array()
                    var dups = json[i].duplicateObjects;
                    for(var j = 0; j < dups.length; j++) {
                        var duplicateObject = dups[j]
                        var tempDuplicateObject = {}
                        tempDuplicateObject['wordClass'] = duplicateObject.wordClass
                        var tempPsis = new Array();
                        for(var k = 0; k < duplicateObject.psis.length; k++) {
                            tempPsis.push(duplicateObject.psis[k])
                        }
                        tempDuplicateObject['psis'] = tempPsis
                        tempDuplicateObject['names'] = duplicateObject.names

                        if(typeof duplicateObject.approvedState == 'undefined' || duplicateObject.approvedState == ""){
                            tempDuplicateObject['approvedState'] = 'N/A';
                            tempDuplicateObject['approvalDate'] = 'N/A';
                        }
                        else{
                            tempDuplicateObject['approvedState'] = duplicateObject.approvedState
                            if(typeof duplicateObject.approvalDate != 'undefined'){
                                var dateArr = duplicateObject.approvalDate.split(" ")
                                tempDuplicateObject['approvalDate'] = dateArr[0]
                            }
                            else{
                                tempDuplicateObject['approvalDate'] = 'N/A';
                            }


                        }
                        if(typeof duplicateObject.processState == 'undefined' || duplicateObject.processState == ""){
                            tempDuplicateObject['processState'] = '0';
                        }
                        else{
                            tempDuplicateObject['processState'] = duplicateObject.processState
                        }

                        duplicateObjects.push(tempDuplicateObject)
                    }
                    dataObject['duplicateObjects'] = duplicateObjects

                    result.push(dataObject);
                }

                return result;
            }

            function getTokens() {
                var tokens = [];
                var query = location.search;
                query = query.slice(1);
                query = query.split('&');
                $.each(query, function (i, value) {
                    var token = value.split('=');
                    var key = decodeURIComponent(token[0]);
                    var data = decodeURIComponent(token[1]);
                    tokens[key] = data;
                });
                return tokens;
            }

            function getConnectedNodes(identifier) {
                var host = window.location.protocol + '//' + window.location.host;
                var serviceUrl = host + pathPrefix+'/service/getNodesByKeywordId'
                var siteBit = 0;
                if(identifier.indexOf("fyr") > -1){
                    siteBit = 4;
                }
                else{
                    siteBit = 1;
                }

                var nodeData = [];
                $.ajax({
                    async: false,
                    url: serviceUrl,
                    data: {
                        'keywordIdentifier': identifier,
                        'sites': siteBit
                    },
                    dataType: 'json'
                }).done(function (data) {
                    for(var i = 0; i < data.length; i++) {
                        var nodeObject = data[i];
                        var tempObject = {}
                        tempObject.identifier = nodeObject.identifier
                        tempObject.site = nodeObject.site
                        if(typeof nodeObject.names["http://psi.oasis-open.org/iso/639/#nob"] != 'undefined'){
                            tempObject.name = nodeObject.names["http://psi.oasis-open.org/iso/639/#nob"]
                        }
                        else if(typeof nodeObject.names["http://psi.oasis-open.org/iso/639/#eng"] != 'undefined'){
                            tempObject.name = nodeObject.names["http://psi.oasis-open.org/iso/639/#eng"]
                        }else if(typeof nodeObject.names["http://psi.topic.ndla.no/#language-neutral"] != 'undefined'){
                            tempObject.name = nodeObject.names["http://psi.topic.ndla.no/#language-neutral"]
                        }

                        nodeData.push(tempObject)
                    }
                });
                var currentNodeData = JSON.parse($("#nodeData").val());
                currentNodeData[identifier] = nodeData;
                $("#nodeData").val(JSON.stringify(currentNodeData));
                return nodeData;
            }

        </r:script>
    </head>
    <body>
        <div class="navbar">
            <g:render template="/actionmenu"model="[param:1]" />
        </div>
        <div class="tabbable">
            <g:render template="submenu" model="[param:4]" />
            <div class="tab-content">
            <g:form name="getDuplicatesForm" useToken="true" action="doMergeKeywordDuplicates">
                <g:hiddenField name="searchDomain" id="searchDomain" value="${searchDomain}" />
                <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />
                <g:hiddenField name="resourceHost" id="resourceHost" value="${resourceHost}" />
                <g:hiddenField name="nodeData" id="nodeData" value="{}" />
                <span class="muted" id="toggle-button"><small>Toggle advanced search options</small></span>
                <div id="more-options" style="display: none">
                    <g:each in="${languages}" var="lang">
                        <label>${lang.value} <g:radio name="languageSelect" class="LanguageSelect" value="${lang.key}" checked="${lang.key == 'nob'}" /></label>
                    </g:each>
                </div>
                <br />
                <br />
                <g:submitButton name="submit"  class="btn btn-warning pull-right" value="merge chosen" />
                <br />
                <br />
                <div id="data-table-duplicates">

                </div>
            </g:form>
            </div>
        </div>
        <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 id="myModalLabel">NDLA node preview</h3>
            </div>
            <div class="modal-body" style="min-width:1000px;">
                <p>NDLA node content</p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
    </body>
</html>