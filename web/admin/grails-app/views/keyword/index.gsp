<!doctype html>
<html>
	<head>
	    <meta name="layout" content="main"/>
	    <title>NDLA Topics Admin Application - Keywords</title>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<a class="brand" href="#">Actions</a>
				<ul class="nav">
					<li class="active"><g:link controller="keyword" action="removeKeyword">Keywords</g:link></li>
					<li><g:link controller="topic" action="index">Topics</g:link></li>
					<li><g:link controller="curriculum" action="index">Curricula</g:link></li>
					<li><g:link controller="metadataset" action="index">Metadata Sets</g:link></li>
                    <li><g:link controller="indexSOLR" action="updateIndexAll">SOLR index</g:link></li>
                    <li><g:link controller="ontology" action="getSubjects">Ontology</g:link></li>
				</ul>
			</div>
		</div>
	</body>
</html>