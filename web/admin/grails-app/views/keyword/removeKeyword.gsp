<!doctype html>
<html>
	<head>
	    <meta name="layout" content="main"/>
	    <title>NDLA Topics Admin Application - Keywords</title>
	    <r:script>
	        $(document).ready(function() {
	        	function getQueryParameterByName(name) {
				    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
				    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
				        results = regex.exec(location.search);
				    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
				}
                chosenLanguage = ""
                searchDomain = $("#searchDomain").val()
                pathPrefix = $("#pathPrefix").val()

				$("#toggle-button").click(function (event) {
                    $("#more-options").slideToggle();
                });

				var keywords;
				var map;
/*
				$('#keywordTitle').typeahead ({
					source: function (query, process) {
						keywords = [];
						map = {};
                        langMap = [];
                        var serviceUrl = searchDomain + "/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                        var searchLanguage = ""
                        var searchType = $('#typeSelect').val();

                        $("input[name=languageSelect]").each(function () {
                            if ($(this).is(':checked') && $(this).val() != "") {
                                searchLanguage += "language[]=" + $(this).val() + "&";
                                langMap.push($(this).val())
                            }
                        });

                        searchLanguage = searchLanguage.substr(0, searchLanguage.length - 1)
                        serviceUrl += "?" + searchLanguage + "&type=" + searchType+ "&index=keywords";


						$.ajax({
							url: serviceUrl,
							type: 'GET',
							dataType: 'json',
							success: function (response) {
								var data = processJson(response, searchLanguage);
								$.each(data, function (i, keyword) {
									var key = keyword.title+" ["+keyword.identifier+"]";
									if (!(key in map)) {
										map[key] = keyword;
										keywords.push(key);
									}
								});
							    process(keywords);
							}
						});
					},
					updater: function (item) {
						$('#keywordId').val(map[item].identifier);
						alert(map[item].identifier);
						return item;
					}
				});

*/

	        });

            $('#keywordTitle').typeahead ({
                source: function (query, process) {
                    $("#nameItems").html("");
                    $("#keyword_types").html("");
                    $(".accordion").css("display","none")
                    keywords = [];
                    map = {};
                    langMap = [];
                    langNames = {};
                    usedLangs = {}

                    //var serviceUrl = "http://search/ndla_search/keywords/" + query;
                    var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                    var searchLanguage = ""
                    var searchType = $('#typeSelect').val();
                    $( "input[name=languageSelect]" ).each(function(){
                        if($(this).is(':checked') && $(this).val() != ""){
                            chosenLanguage = $(this).val();
                            searchLanguage += "language[]="+$(this).val()+"&";
                            langMap.push($(this).val())
                        }
                        langNames[$(this).val()] = $(this).attr("text");
                    });

                    searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
                    serviceUrl += "?" + searchLanguage + "&type=" + searchType;

                    $.ajax({
                        url: serviceUrl,
                        type: 'GET',
                        dataType: 'json',
                        success: function (response) {
                            var data = processJson(response, langMap);
                            $.each(data, function (i, keyword) {
                                var key = keyword.title;
                                if(chosenLanguage != 'undefined' && chosenLanguage != ""){
                                    var theTitle = "title_"+chosenLanguage;

                                    for (var keyp in keyword) {
                                        if (keyword.hasOwnProperty(keyp)) {
                                            if(keyp == theTitle){
                                                key = keyword[keyp]+" ["+keyword.identifier+"]";
                                                break;
                                            }
                                        }
                                    }

                                    //key = keyword.theTitle
                                }

                                if (!(key in map)) {
                                    map[key] = keyword;
                                    keywords.push(key);
                                }
                            });
                            process(keywords);
                        }
                    }, 300);
                },
                matcher: function (item) {
                    return true;
                },
                updater: function (item) {
                    $('#keywordId').val(map[item].identifier);
                    return item;
                }
            });
/*
            function processJson(json, langMap) {
                var result = new Array();
                for (var i = 0; i < json.length; ++i) {
                    dataObject = {}

                    var neutral_title = json[i].title_language_neutral
                    jQuery.each(json[i], function (key, val) {
                        if (key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                            var split_arr = key.split("_");
                            if (langMap.indexOf(split_arr[1]) > -1) {
                                dataObject[key] = val;
                            }
                        }
                    });

                    neutral_title = typeof (neutral_title) == 'undefined' ? json[i].title_nob : json[i].title_language_neutral;

                    dataObject['identifier'] = json[i].id
                    dataObject['title'] = neutral_title
                    dataObject['wordclass'] = json[i].wordclass;
                    dataObject['visibility'] = json[i].visibility;
                    dataObject['types'] = json[i].type_id;
                    dataObject['approved'] = json[i].approved;
                    var new_format = 'D MMM YYYY [at] h:mm A';
                    var changed = moment(json[i].timestamp, 'YYYY-MM-DD HH:mm:ss').format(new_format);
                    dataObject['changed'] = changed;
                    var nice = moment(json[i].approval_date, 'YYYY-MM-DD HH:mm:ss').format(new_format);
                    dataObject['approval_date'] = nice;

                    result.push(dataObject);
                }
                return result;
            }
            */
            function processJson(json,langMap) {
                var result = new Array();
                for(var i = 0; i < json.length; ++i) {
                    dataObject = {}

                    var neutral_title = json[i].title_language_neutral
                    jQuery.each(json[i], function(key, val) {
                        if(key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                            var split_arr = key.split("_");
                            dataObject[key] = val;
                        }
                    });

                    neutral_title = typeof (neutral_title) ==  'undefined' ? json[i].title_nob : json[i].title_language_neutral;

                    dataObject['identifier'] = json[i].id
                    dataObject['title'] = neutral_title
                    dataObject['wordclass'] = json[i].wordclass;
                    if(typeof json[i].visibility == 'undefined'){
                        dataObject['visibility'] = json[i].visible;
                    }
                    else{
                        dataObject['visibility'] = json[i].visibility;
                    }

                    var type = []
                    if(typeof json[i].type_id != 'undefined'){
                        var typeString = json[i].type_id;
                        type.push(typeString)
                    }

                    dataObject['types'] = type;
                    dataObject['approved'] = json[i].approved;
                    dataObject['processState'] = json[i].process_state;
                    result.push(dataObject);
                }
                return result;
            }
        </r:script>
	</head>
	<body>
		<div class="navbar">
            <g:render template="/actionmenu" model="[param:1]" />
		</div>
		<div class="tabbable">
            <g:render template="submenu" model="[param:2]" />
			<div class="tab-content">
				<div class="tab-pane active" id="tab1">
					<div class="tabbable tabs-right">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab1-1" data-toggle="tab">Remove</a></li>
							<li><g:link controller="keyword" action="updateKeyword" params="['data-toggle': 'tab']">Update</g:link></li>
							<li><g:link controller="keyword" action="addKeyword" params="['data-toggle': 'tab']">Add</g:link></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab1-1">
								<g:form name="removeKeywordForm" useToken="true" action="removeKeywordByKeywordId" autocomplete="off">
									<g:hiddenField name="keywordId" value="" />
                                    <g:hiddenField name="searchDomain" id="searchDomain" value="${searchDomain}" />
                                    <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />

									<fieldset>
										<legend>Remove Keyword</legend>
                                        <span class="muted" id="toggle-button"><small>Toggle advanced search options</small></span>
                                        <div id="more-options" style="display: none">
                                            <g:each in="${languages}" var="lang">
                                                <label>${lang.value}<g:checkBox name="languageSelect" class="LanguageSelect" value="${lang.key}" checked="${lang.key.toString().equals("nob")}" /></label>
                                            </g:each>

                                            <br />
                                            <label>Type</label>
                                            <select id="typeSelect">
                                                <option value="any">Any</option>
                                                <option value="start">Start</option>
                                                <option value="end">End</option>
                                            </select>
                                            <br />
                                            <br />
                                            <label>Approval</label>
                                            <select id="approvedSelect">
                                                <option value="all">All</option>
                                                <option value="true">Approved</option>
                                                <option value="false">Disapproved</option>
                                            </select>
                                            <div id="approvedLimit" style="display: none">
                                                <label>Insert a higher number of returned results : (10 is default): <br /><input type="text" name="limitUp" id="limitUp" placeholder="Type a number larger than 10" autocomplete="off">  </label>
                                            </div>

                                            <label>Sites</label>
                                            <label class="checkbox inline">
                                                <input type="checkbox" name="siteCheckbox" id="siteCheckbox" value="ndla" checked="1">NDLA
                                            </label>

                                            <label class="checkbox inline">
                                                <input type="checkbox"  name="siteCheckbox"  id="siteCheckbox" value="nygiv">Nygiv
                                            </label>
                                        </div>
										<label>Keyword</label>
										<input type="text" class="autocomplete" name="keywordTitle" id="keywordTitle" placeholder="Type a keyword..." autocomplete="off">
										<br />
										<br />
										<g:submitButton name="submit" class="btn btn-danger" value="Delete" />
									</fieldset>
								</g:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>