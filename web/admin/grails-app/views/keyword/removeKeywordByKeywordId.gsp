<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Keywords</title>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu" model="[param:1]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:2]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1-1" data-toggle="tab">Remove</a></li>
                    <li><g:link controller="keyword" action="updateKeyword" params="['data-toggle': 'tab']">Update</g:link></li>
                    <li><g:link controller="keyword" action="addKeyword" params="['data-toggle': 'tab']">Add</g:link></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1-1">
                        <g:form name="removeKeywordForm" useToken="true" action="doRemoveKeyword" autocomplete="off">
                            <g:hiddenField name="keywordIdentifier" value="${keywordIdentifier}" />
                            <fieldset>
                                <legend>Remove Keyword</legend>

                                <p>Are you sure you want to delete ${keywordName}?</p>

                                <br />
                                <br />
                                <g:link action="lookupKeyword" controller="keyword"><input type="button" value="Cancel" class="btn btn-warning"/></g:link>  <g:submitButton name="submit" class="btn btn-danger" value="Delete" />
                            </fieldset>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>