<!doctype html>
<html>
	<head>
	    <meta name="layout" content="main"/>
	    <title>NDLA Topics Admin Application - Keywords</title>
	    <style>
			#myModal {
				width: 800px;
				margin: 0 0 0 -400px;
			}
	    </style>
	    <r:script>
	    	function processLink(identifier) {
	    		var nodeIdentifier = "ndlanode_" + identifier;
	    		var labelContent = "NDLA node preview [" + identifier + "]";
				$("#myModalLabel").html(labelContent);
				var loadingContent = "<h2>Loading...</h2>";
				$(".modal-body").html(loadingContent);

				var url = "/admin/service/getNdlaNodeContent?nodeIdentifier=" + identifier + " #page-content";
				$(".modal-body").load(url);
			}

			$('#myModal').on('show', function () {
				$(this).find('.modal-body').css({width:'auto', height:'auto', 'max-height':'100%'});
			});
	    </r:script>
	    <r:script>
	        $(document).ready(function() {
	        	/*
	        	function getParameterByName(name) {
				    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
				    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
				        results = regex.exec(location.search);
				    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
				}
				*/

	        	$("#toggle-button").click(function (event) {
                    $("#more-options").slideToggle();
                });

	        	var keywords;
				var map;

				$('#keywordTitle').typeahead ({
					source: function (query, process) {
						keywords = [];
						map = {};

						var serviceUrl = "http://search.test.ndla.no/ndla_search/keywords/" + query;
						//var languageRadio = $('input[name=languageRadios]:checked').val();
						var searchLanguage = $('#languageSelect').val();
						var searchType = $('#typeSelect').val();

						$.ajax({
							url: serviceUrl,
							type: 'GET',
							data: {language: searchLanguage, type: searchType},
							dataType: 'json',
							success: function (response) {
								var data = processJson(response, searchLanguage);
								$.each(data, function (i, keyword) {
									var key = keyword.title+" (" + keyword.title_nb + ") [" + keyword.identifier + "]";
									if (!(key in map)) {
										map[key] = keyword;
										keywords.push(key);
									}
								});
							    process(keywords);
							}
						});
					},
					updater: function (item) {
						$('#keywordIdentifier').val(map[item].identifier);
						return item;
					}
				});


				$('#keywordTitleButton').click(function() {
					var host = window.location.protocol + '//' + window.location.host;

					// http://topic.test.ndla.no/no.ndla.topics/rest/v1/nodes/?filter[keyword]=keyword-59807&filter[site]=nygiv
					// http://topic.test.ndla.no/no.ndla.topics/rest/v1/nodes/?filter[keyword]=keyword-59807&filter[site]=ndla

					var serviceUrl = host + '/admin/service/getNodesByKeywordId'
					var keywordIdentifier = $('input#keywordIdentifier').val();
					alert(keywordIdentifier);
					//alert(keywordIdentifier);

					var ndlaSite = $("#siteCheckbox1").is(':checked') ? 1 : 0; 	 // NDLA   = 1
					var delingSite = $("#siteCheckbox2").is(':checked') ? 2 : 0; // DELING = 2
					var nygivSite = $("#siteCheckbox3").is(':checked') ? 4 : 0;	 // NYGIV  = 4

					var sites = ndlaSite | delingSite | nygivSite;

					$.ajax({
						url: serviceUrl,
						data: {
							'keywordIdentifier': keywordIdentifier,
							'sites': sites
						},
						dataType: 'json',
					}).done(function(data) {
						var dataHtml = renderDataTable(data);
						$('#data-table').html(dataHtml);
					});
				});

				$('#submit-button').click(function () {
					var nodes = new Array();
					var i = 0;
					$('#data-table :checked').each(function () {
						nodes[i] = $(this).val();
						i++;
					});
					//alert(nodes);
					$("input[id=relatedTopicIdentifiers]").val(nodes);
				});

                $(document).keypress(function(e) {
                    if(e.which == 13) {
                        e.preventDefault();
                    }
                });
			});

            function renderDataTable(json) {
                var dataSize = json.length;
                if (dataSize == 0) {
                    return '<span class="label label-warning">No Data!</span>';
                }
                var buffer = ['<table id="data-table" class="table table-bordered table-hover"><thead><tr><th>#</th><th>Site</th><th>Node</th><th>Remove</th></tr></thead><tbody>'];
                var counter;
                for (var i = 0; i < dataSize; i++) {
                    counter = i + 1;
                    var nodeIdentifier = json[i].identifier.split("_")[1];
                    var row = '<tr><td>' + counter.toString() + '</td><td>' + json[i].site + '</td><td><a href="#" onclick="processLink(' + nodeIdentifier + ');" data-target="#myModal" role="button"data-toggle="modal">' + json[i].identifier + '</a></td><td><label class="checkbox"><input type="checkbox" name="nodes[]" id="' + json[i].identifier + '" value="' + json[i].identifier + '">Yes</label></td></tr>';
                    buffer.push(row + "\n");
                }
                buffer.push("</tbody></table>");
                return buffer.join('');
            }

            function processJson(json, language) {
                var result = new Array();
                var titleFieldName = language === 'und' ? 'title' : 'title_' + language;
                for(var i = 0; i < json.length; ++i) {
                    result.push({'identifier': json[i].id, 'title': json[i][titleFieldName]});
                }
                return result;
            }
		</r:script>
	</head>
	<body>
		<div class="navbar">
        <g:render template="/actionmenu"model="[param:1]" />
		</div>
		<div class="tabbable">
			<ul class="nav nav-tabs">
                <g:render template="submenu"  model="[param:2]" />
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab1">
					<div class="tabbable tabs-right">
						<ul class="nav nav-tabs">
							<li class="active"><a href="#tab2-1" data-toggle="tab">Keyword-Nodes</a></li>
							<li><g:link controller="keyword" action="removeNodeKeywords" params="['data-toggle': 'tab']">Node-Keywords</g:link></li>
							<li><g:link controller="keyword" action="removeKeywordSubjects" params="['data-toggle': 'tab']">Keyword-Subjects</g:link></li>
							<li><g:link controller="keyword" action="removeSubjectKeywords" params="['data-toggle': 'tab']">Subject-Keywords</g:link></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab2-1">
								<g:form name="removeKeywordNodesForm" useToken="true" action="doRemoveKeywordNodes">
									<g:hiddenField name="keywordIdentifier" id="keywordIdentifier" value="" />
									<g:hiddenField name="relatedTopicIdentifiers" id="relatedTopicIdentifiers" value="" />
									<fieldset>
										<legend>Remove Keyword-Nodes relations</legend>
										<label>Site</label>
										<label class="checkbox inline">
											<input type="checkbox" id="siteCheckbox1" value="site1" checked>NDLA
										</label>
										<label class="checkbox inline">
											<input type="checkbox" id="siteCheckbox2" value="site2">Deling
										</label>
										<label class="checkbox inline">
											<input type="checkbox" id="siteCheckbox3" value="site3">Nygiv
										</label>
										<br />
										<br />
										<span class="muted" id="toggle-button"><small>Toggle advanced search options</small></span>
										<div id="more-options" style="display: none">
											<br />
											<label>Language</label>
											<select id="languageSelect">
												<option value="und">Undefined</option>
												<option value="en">English</option>
												<option value="nb">Bokmål</option>
												<option value="nn">Nynorsk</option>
											</select>
											<label>Type</label>
											<select id="typeSelect">
												<option value="any">Any</option>
												<option value="start">Start</option>
												<option value="end">End</option>
											</select>
											<br />
											<br />
										</div>
										<label>Keyword</label>
										<div class="input-append">
											<input class="span10" name="keywordTitle" id="keywordTitle" type="text" autocomplete="off">
											<button class="btn" id="keywordTitleButton" type="button">Go!</button>
										</div>
										<br />
										<div id="data-table">
											<p>&nbsp;</p>
										</div>
										<br />
										<g:submitButton name="submit" class="btn btn-danger" id="submit-button" value="Submit" />
									</fieldset>
								</g:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 id="myModalLabel">NDLA node preview</h3>
            </div>
            <div class="modal-body" style="min-width:1000px;">
                <p>NDLA node content</p>
            </div>
            <div class="modal-footer">
                <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
            </div>
        </div>
	</body>
</html>