<!doctype html>
<html>
	<head>
	    <meta name="layout" content="main"/>
	    <title>NDLA Topics Admin Application - Keywords</title>
	</head>
	<body>
		<div class="navbar">
			<div class="navbar-inner">
				<a class="brand" href="#">Actions</a>
				<ul class="nav">
					<li class="active"><g:link controller="keyword" action="removeKeyword">Keywords</g:link></li>
					<li><g:link controller="topic" action="index">Topics</g:link></li>
					<li><g:link controller="curriculum" action="index">Curricula</g:link></li>
					<li><g:link controller="metadataset" action="index">Metadata Sets</g:link></li>
				</ul>
			</div>
		</div>
		<div class="tabbable">
			<ul class="nav nav-tabs">
				<li><g:link controller="keyword" action="removeKeyword" params="['data-toggle': 'tab']">Keywords</g:link></li>
				<li class="active"><a href="#tab2" data-toggle="tab">Remove Content Relation</a></li>
				<li><g:link controller="keyword" action="addNodeKeywords" params="['data-toggle': 'tab']">Add Content Relation</g:link></li>
				<li><g:link controller="keyword" action="addHierarchicalRelation" params="['data-toggle': 'tab']">Hierarchical Relation</g:link></li>
				<li><g:link controller="keyword" action="addHorizontalRelation" params="['data-toggle': 'tab']">Horizontal Relation</g:link></li>
				<li><g:link controller="keyword" action="merge" params="['data-toggle': 'tab']">Merge</g:link></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab1">
					<div class="tabbable tabs-right">
						<ul class="nav nav-tabs">
							<li><g:link controller="keyword" action="removeKeywordNodes" params="['data-toggle': 'tab']">Keyword-Nodes</g:link></li>
							<li><g:link controller="keyword" action="removeNodeKeywords" params="['data-toggle': 'tab']">Node-Keywords</g:link></li>
							<li class="active"><a href="#tab2-3" data-toggle="tab">Keyword-Subjects</a></li>
							<li><g:link controller="keyword" action="removeSubjectKeywords" params="['data-toggle': 'tab']">Subject-Keywords</g:link></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab2-3">
								<p>Keyword-Subjects</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>