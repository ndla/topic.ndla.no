<!doctype html>
<html>
	<head>
	    <meta name="layout" content="main"/>
	    <title>NDLA Topics Admin Application - Test</title>
    </head>
	<body>
		<g:form name="testForm" useToken="true" action="doTest">
			<fieldset>
		    	<legend>Create link</legend>
				<label>Label #1</label>
				<g:textField name="label" />
				<label>Label #2</label>
				<g:textField name="label" />
				<label>Label #3</label>
				<g:textField name="label" />
				<br />
				<br />
			    <g:submitButton name="submit" class="btn btn-success" value="Submit" />
			</fieldset>
		</g:form>
	</body>
</html>