<!doctype html>
<html>
	<head>
	    <meta name="layout" content="main"/>
	    <title>NDLA Topics Admin Application - Keywords</title>
	    <r:script>
	        $(document).ready(function() {
                chosenLanguage = ""
	            searchDomain = $("#searchDomain").val()
                $('body').on('click', "input[name=typeSelect]" ,function(){
                    if(!$(this).is(':checked')) {
                        $(this).parent().remove();
                    }
                });

                $('body').on('click', "#addLanguageButton" ,function(){
                   var selectedLanguageId = $("#missingLanguages option:selected").val()
                   var selectedLanguageText = $("#missingLanguages option:selected").text()
                   $("#nameItems").append('<label>'+selectedLanguageText+' title</label><input type="text" name="http://psi.oasis-open.org/iso/639/#'+selectedLanguageId+'" id="http://psi.oasis-open.org/iso/639/#'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+' title" autocomplete="off" value="">');
                   $("#missingLanguages option:selected").remove();
                });


                 $('body').on('click', "input[name=nameRemove]" ,function(){
                    if($(this).is(':checked')) {
                        $("#wordSpan_"+$(this).val()).remove();
                    }
                });


	        	$("#toggle-button").click(function (event) {
                    $("#more-options").slideToggle();
                });


	        	var keywords;
				var map;

				    $('#keywordTypeTitle').typeahead ({
				        source: function (query, process) {
                            keywords = [];
                            map = {};
                            langMap = [];
                            //var serviceUrl = "http://search/ndla_search/keywords/" + query;
                            var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                            var searchLanguage = ""
                            var searchType = $('#typeSelect').val();
						$( "input[name=languageSelect]" ).each(function(){
						    if($(this).is(':checked') && $(this).val() != ""){
						        searchLanguage += "language[]="+$(this).val()+"&";
						        langMap.push($(this).val())
						    }
						});
						searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
                        serviceUrl += "?" + searchLanguage + "&type=" + searchType;

						$.ajax({
							url: serviceUrl,
							type: 'GET',
							dataType: 'json',
							success: function (response) {
								var data = processJson(response, langMap);
								$.each(data, function (i, keyword) {
									var key = keyword.title;

									if (!(key in map)) {
										map[key] = keyword;
										keywords.push(key);
									}
								});
							    process(keywords);
							}

						}, 300);
					},
					 matcher: function (item) {
                            return true;
                    },
                    updater: function (item) {
                        var typeCheckBoxes = ""
					    for(mkey in map[item]){
					    var stringVal = map[item][mkey];
					    if(typeof(stringVal) != "undefinded"){
                            if(mkey.contains('title_nob')) {
                                var langKey = mkey.substr(mkey.lastIndexOf("_")+1);
                                 var checkBox = '<label>'+stringVal+' <g:checkBox name="typeSelect" value="'+map[item].identifier+'" checked="true" /></label>';
                                 typeCheckBoxes += checkBox;
                            }
					    }
					  }
					  $("#keyword_types").append(typeCheckBoxes);
					}
				});



				$('#keywordTitle').typeahead ({
					source: function (query, process) {
					    $("#nameItems").html("");
					    $("#keyword_types").html("");
					    $(".accordion").css("display","none")
						keywords = [];
						map = {};
                        langMap = [];
                        langNames = {};
                        usedLangs = {}

						//var serviceUrl = "http://search/ndla_search/keywords/" + query;
						var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
						var searchLanguage = ""
						var searchType = $('#typeSelect').val();
						$( "input[name=languageSelect]" ).each(function(){
						    if($(this).is(':checked') && $(this).val() != ""){
						    chosenLanguage = $(this).val();
						        searchLanguage += "language[]="+$(this).val()+"&";
						        langMap.push($(this).val())
						    }
						    langNames[$(this).val()] = $(this).attr("text");
						});

						searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
                        serviceUrl += "?" + searchLanguage + "&type=" + searchType;

						$.ajax({
							url: serviceUrl,
							type: 'GET',
							dataType: 'json',
							success: function (response) {
								var data = processJson(response, langMap);
								$.each(data, function (i, keyword) {
								    var key = keyword.title;
								    if(chosenLanguage != 'undefined' && chosenLanguage != ""){
								        var theTitle = "title_"+chosenLanguage;

								        for (var keyp in keyword) {
                                          if (keyword.hasOwnProperty(keyp)) {
                                            if(keyp == theTitle){
                                                key = keyword[keyp]
                                                break;
                                            }
                                          }
                                        }

                                        //key = keyword.theTitle
								    }

									if (!(key in map)) {
										map[key] = keyword;
										keywords.push(key);
									}
								});
							    process(keywords);
							}
						}, 300);
					},
					 matcher: function (item) {
                        return true;
                    },
					updater: function (item) {
					types = null;
                    var toggleHeader = "";
					  for(mkey in map[item]){
					    var stringVal = map[item][mkey];
					    if(typeof(stringVal) != "undefinded"){
                            if(mkey.contains('title_')) {
                                var langKey = mkey.substr(mkey.lastIndexOf("_")+1);
                                //if(langKey in langNames){
                                    $("#nameItems").append('<span id="wordSpan_'+langKey+'"><label>'+langNames[langKey]+' title</label><input type="text" name="http://psi.oasis-open.org/iso/639/#'+langKey+'" id="http://psi.oasis-open.org/iso/639/#'+langKey+'" placeholder="type a '+langNames[langKey]+' title" autocomplete="off" value="'+stringVal+'"> <span>Remove <g:checkBox name="nameRemove" value="'+langKey+'" checked="false" /></span></span>');
                                    usedLangs[langKey] = langNames[langKey];
                                //}
                            }

                            if(mkey == 'title_'+chosenLanguage) {
                                toggleHeader = stringVal;
                            }

                            if(mkey == "wordclass") {
                                toggleHeader+= " ("+stringVal+") ";
                            }

                            if(mkey == "types") {
                                types = stringVal;
                            }
					    }
					  }
                      $("#accordion-header1").html(toggleHeader);

					  var html = '<select id="missingLanguages" name="missingLanguages"><option value="">Add a language</option>';
					  for(langNameKey in langNames) {
                        var missingLangString = usedLangs[langNameKey];
                        if(typeof missingLangString  === 'undefined') {
                            html += '<option value="'+langNameKey+'">'+langNames[langNameKey]+'</option>'
                        }
                      }
                    html += '</select> <button class="btn" id="addLanguageButton" type="button">Go!</button>';
                    $("#missingLanguageWrapper").html(html)
                    $("#updateKeywordNames").css("display","block");

					  if(types != null && types.length > 0){
					    var typeCheckBoxes = ""
					    for(var i = 0; i < types.length; i++){
					        if(types[i].indexOf('\"names\":null') == -1){
					            var type = JSON.parse(types[i]);
                                var type_id = type.type_id;
                                var type_names = type.names;
                                for (var j = 0; j < type_names.length; j++) {
                                     for(var langId in type_names[j]){
                                        if(langId.contains("nob")){
                                            var checkBox = '<label>'+type_names[j][langId]+' <g:checkBox name="typeSelect" value="'+type_id+'" checked="true" /></label>';
                                            typeCheckBoxes += checkBox;
                                        }
                                    }
                                }
					        }
					    }
					  }

					  $("#keyword_types").append(typeCheckBoxes);
					    $("#keyword_type_label").css("display","block")
					    $("#keywordTypeTitle").css("display","block")
					    $("#updateKeywordTypes").css("display","block");


					  if(map[item].visibility != 'undefined' && map[item].visibility == 0){
    				    $("input[name=visibilityRadios][value=visibilityOption2]").attr('checked', 'checked');
    				    $("#updateKeywordVisible").css("display","block");
                      }
                      else{
    				    $("#updateKeywordVisible").css("display","block");
                      }


                      if(map[item].wordclass != 'undefined'){
                        var cap_wordclas = map[item].wordclass.capitalize();
                        $("#wordClass option[value="+cap_wordclas+"]").attr('selected', 'selected');
                        $("#updateKeywordWordClass").css("display","block");
                      }

                      if(map[item].identifier != 'undefined') {
                        $("#keywordIdentifier").val(map[item].identifier)
                      }


                      if(map[item].processState != 'undefined'){
                        if(map[item].processState == 0){
                            $("input[name=processStateRadios][value=0]").attr('checked', 'checked');
                            $("#initialprocessState").val("true");
                        }
                        else if(map[item].processState == 1){
                            $("input[name=processStateRadios][value=1]").attr('checked', 'checked');
                            $("#initialprocessState").val("true");
                        }
                        else if(map[item].processState == 2){
                            $("input[name=processStateRadios][value=2]").attr('checked', 'checked');
                            $("#initialprocessState").val("true");
                        }
                     }
                     else{
                        $("input[name=processStateRadios][value=0]").attr('checked', 'checked');
                     }
                     $("#updateKeywordProcessState").css("display","block");

                     if(map[item].approved != 'undefined'){
                        if(map[item].approved){
                            $("input[name=approvedRadios][value=true]").attr('checked', 'checked');
                            $("#initialApprovalState").val("true");
                        }
                        else{
                            $("input[name=approvedRadios][value=false]").attr('checked', 'checked');
                            $("#initialApprovalState").val("false");
                        }

                     }
                     else{
                        $("input[name=approvedRadios][value=false]").attr('checked', 'checked');
                     }
                     $("#updateKeywordApproved").css("display","block");
                     $(".accordion").css("display","block")
				return item;
					}
				});

					$('#addKeywordAlternativeButton').click(function() {

  						var dataHtml = appendFormItem();
  						$('#keywordFieldSet').append(dataHtml);

  				});


                $(document).keypress(function(e) {
                    if(e.which == 13) {
                        e.preventDefault();
                    }
                });


        });

            function appendFormItem(){
  		        var word_class_options = $("#wordClass").html();
                var numberOfSets = parseInt($("#numberOfSets").val());
                numberOfSets++;

  			  var buffer = ['<br /> <br /><fieldset><legend>Keyword names alternative '+numberOfSets+'</legend><label>Word Class</label><select name="wordClass'+numberOfSets+'">'+word_class_options]
        buffer.push('</select><label>Visible?</label>')
        buffer.push('<label class="radio"><g:radio name="visibilityRadios'+numberOfSets+'" id="visibilityRadios1" value="visibilityOption1" checked="true" />Yes</label>')
        buffer.push('<label class="radio"><g:radio name="visibilityRadios'+numberOfSets+'" id="visibilityRadios2" value="visibilityOption2" />No</label>')
        buffer.push('<label>English Title</label><input type="text" name="englishTitle'+numberOfSets+'"  placeholder="Type an English title..." />')
        buffer.push('<label>Bokmål Title</label><input type="text" name="bokmalTitle'+numberOfSets+'"  placeholder="Type a Bokmål title..." />')
        buffer.push('<label>Nynorsk Title</label><input type="text" name="nynorskTitle'+numberOfSets+'" placeholder="Type a Nynorsk title..."></fieldset>');
    			$("#numberOfSets").val(numberOfSets++)
                return buffer.join('');
  			}

            function processJson(json,langMap) {
                var result = new Array();
                for(var i = 0; i < json.length; ++i) {
                    dataObject = {}

                    var neutral_title = json[i].title_language_neutral
                    jQuery.each(json[i], function(key, val) {
                        if(key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                            var split_arr = key.split("_");
                            dataObject[key] = val;
                        }
                    });

                    neutral_title = typeof (neutral_title) ==  'undefined' ? json[i].title_nob : json[i].title_language_neutral;

                    dataObject['identifier'] = json[i].id
                    dataObject['title'] = neutral_title
                    dataObject['wordclass'] = json[i].wordclass;
                    if(typeof json[i].visibility == 'undefined'){
                        dataObject['visibility'] = json[i].visible;
                    }
                    else{
                        dataObject['visibility'] = json[i].visibility;
                    }

                    var type = []
                    if(typeof json[i].type_id != 'undefined'){
                        var typeString = json[i].type_id;
                        type.push(typeString)
                    }

                    dataObject['types'] = type;
                    dataObject['approved'] = json[i].approved;
                    dataObject['processState'] = json[i].process_state;
                    result.push(dataObject);
                }
                return result;
            }

				String.prototype.capitalize = function() {
            return this.charAt(0).toUpperCase() + this.slice(1);
        }
		</r:script>
	</head>
	<body>
    <div class="navbar">
        <g:render template="/actionmenu"model="[param:1]" />
    </div>
		<div class="tabbable">
            <g:render template="submenu" model="[param:2]" />
			<div class="tab-content">
				<div class="tab-pane active" id="tab1">
					<div class="tabbable tabs-right">
						<ul class="nav nav-tabs">
							<li><g:link controller="keyword" action="removeKeyword" params="['data-toggle': 'tab']">Remove</g:link></li>
							<li class="active"><a href="#tab1-2" data-toggle="tab">Update</a></li>
							<li><g:link controller="keyword" action="addKeyword" params="['data-toggle': 'tab']">Add</g:link></li>
						</ul>
						<div class="tab-content">
							<div class="tab-pane active" id="tab1-2">
								<g:form name="updateKeywordForm" useToken="true" action="doUpdateKeyword">
									<g:hiddenField name="keywordIdentifier" value="" />
								    <g:hiddenField name="numberOfSets" id="numberOfSets" value="1" />
                                    <g:hiddenField name="searchDomain" value="${searchDomain}" />
									<div id="keywordFieldSet">
									<fieldset>
										<legend>Update Keyword</legend>
										<span class="muted" id="toggle-button"><small>Toggle advanced search options</small></span>
										<div id="more-options" style="display: none">
											<br />
											<label>Language</label>
                                            <g:each in="${langMap}" var="langItem">
                                                <label>${langItem.value} <g:checkBox name="languageSelect" value="${shortMap.get(langItem.key)}" checked="${shortMap.get(langItem.key).toString().equals("nob")}" text="${langItem.value}" /></label>
                                            </g:each>

											<br />
											<br />
                                            <label>Type</label>
                                            <select id="typeSelect">
                                                <option value="any">Any</option>
                                                <option value="start">Start</option>
                                                <option value="end">End</option>
                                            </select>
										</div>
										<label>Search for keyword</label>
										<input type="text" name="keywordTitle" id="keywordTitle" placeholder="Type a keyword identifier..." autocomplete="off">

                                        <br />
                                        <br />

                                        <div class="accordion" id="accordion1">
                                            <div class="accordion-group">
                                                <div class="accordion-heading">
                                                    <a class="accordion-toggle" id="accordion-header1" data-toggle="collapse" data-parent="#accordion2" href="#collapse1">
                                                        Click me to expand. Click me again to collapse. Part I.
                                                    </a>
                                                </div>

                                                <div id="collapse1" class="accordion-body collapse" style="height: 0px; ">
                                                    <div class="accordion-inner">

                                                        <div id="updateKeywordWordClass">
                                                            <label>Word Class</label>
                                                            <g:select name="wordClass" id="wordClass" from="${wordClasses}" noSelection="['':'-Choose word class-']" />
                                                        </div>


                                                        <div id="updateKeywordVisible">
                                                            <label>Visible?</label>
                                                            <label class="radio">
                                                                <g:radio name="visibilityRadios" id="visibilityRadios" value="visibilityOption1" checked="true" />Yes
                                                            </label>
                                                            <label class="radio">
                                                                <g:radio name="visibilityRadios" id="visibilityRadios" value="visibilityOption2" />No
                                                            </label>
                                                            <br />
                                                        </div>

                                                        <div id="updateKeywordTypes">
                                                            <label id="keyword_type_label">Keyword types</label>
                                                            <div id="keyword_types">

                                                            </div>
                                                            <input type="text" name="keywordTypeTitle" id="keywordTypeTitle" placeholder="Type a type identifier..." autocomplete="off">
                                                        </div>

                                                        <div id="updateKeywordNames">
                                                            <label id="keyword_name_label">Keyword names</label>
                                                            <div id="nameItems">
                                                            </div>
                                                            <div id="missingLanguageWrapper">
                                                            </div>
                                                        </div>

                                                        <br />


                                                        <div id="updateKeywordProcessState">
                                                            <label>Process state</label>
                                                            <g:hiddenField name="initialProcessState" id="initialProcessState" value="" />
                                                            <label class="radio">
                                                                <g:radio name="processStateRadios" id="processStateRadios" value="0" />Not processed
                                                            </label>
                                                            <label class="radio">
                                                                <g:radio  name="processStateRadios" id="processStateRadios" value="1" />Pending
                                                            </label>
                                                            <label class="radio">
                                                                <g:radio  name="processStateRadios" id="processStateRadios" value="2" disabled="disabled" />Processed
                                                            </label>
                                                        </div>

                                                        <div id="updateKeywordApproved">
                                                            <label>Approved?</label>
                                                            <g:hiddenField name="initialApprovalState" id="initialApprovalState" value="" />
                                                            <label class="radio">
                                                                <g:radio name="approvedRadios" id="approvedRadios" value="false" />No
                                                            </label>
                                                            <label class="radio">
                                                                <g:radio  name="approvedRadios" id="approvedRadios" value="true" />Yes
                                                            </label>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>



    									</fieldset>
    									</div>
    									<!--
    									<g:img dir="images" file="add.png" id="addKeywordAlternativeButton" />
    									-->

										<br />
										<br />
										<g:submitButton name="submit" class="btn btn-warning" value="Submit" />

								</g:form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>