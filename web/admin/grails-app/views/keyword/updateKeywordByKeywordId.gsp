<!doctype html>
<html>
<head>
<meta name="layout" content="main"/>
<title>NDLA Topics Admin Application - Keywords</title>
    <r:script>
    $("#updateKeywordApproved").css("display","block");
    $("#updateKeywordVisible").css("display","block");
    $("#updateKeywordTypes").css("display","block");
    $("#updateKeywordNames").css("display","block");
    $("#updateKeywordProcessState").css("display","block");
    $(document).ready(function() {
        searchDomain = $("#searchDomain").val()

         $('body').on('click', "input[name=typeSelect]" ,function(){
            if(!$(this).is(':checked')) {
                $(this).parent().remove();
            }
        });

         $('body').on('click', "input[name=nameRemove]" ,function(){
            if($(this).is(':checked')) {
                console.log($(this).val())
                $("#wordSpan_"+$(this).val()).remove();
            }
        });


        $('#addLanguageButton').click(function() {
           var selectedLanguageId = $("#missingLanguages option:selected").val()
           var selectedLanguageText = $("#missingLanguages option:selected").text()
           $("#keywordNames").append('<label>'+selectedLanguageText+' title</label><input type="text" name="'+selectedLanguageId+'" id="'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+' title" autocomplete="off" value="">');
           $("#missingLanguages option:selected").remove();

        });


        $('#keywordByIdTypeTitle').typeahead ({
            source: function (query, process) {
                keywords = [];
                map = {};
                langMap = [];
                //var serviceUrl = "http://search.test.ndla.no/ndla_search/keywords/" + query;
                var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                var searchLanguage = ""
                var searchType = $('#typeSelect').val();
                $( "input[name=languageSelect]" ).each(function(){
                    if($(this).is(':checked') && $(this).val() != ""){
                        var langId = $(this).val().substr($(this).val().indexOf("#")+1)
                        searchLanguage += "language[]="+langId+"&";
                        langMap.push($(this).val())
                    }
                });
            searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
            serviceUrl += "?" + searchLanguage + "&type=" + searchType;

            $.ajax({
                url: serviceUrl,
                type: 'GET',
                dataType: 'json',
                success: function (response) {
                    var data = processJson(response, langMap);
                    $.each(data, function (i, keyword) {
                        var key = keyword.title;

                        if (!(key in map)) {
                            map[key] = keyword;
                            keywords.push(key);
                        }
                    });
                    process(keywords);
                }

            }, 300);
        },
         matcher: function (item) {
                return true;
        },
        updater: function (item) {
            var typeCheckBoxes = ""
            for(mkey in map[item]){
            var stringVal = map[item][mkey];
            if(typeof(stringVal) != "undefinded"){
                if(mkey.contains('title')) {
                    var langKey = mkey.substr(mkey.lastIndexOf("_")+1);
                     var checkBox = '<label>'+stringVal+' <g:checkBox name="typeSelect"
                                                                      value="'+map[item].identifier+'"
                                                                      checked="true"/></label>';
                     typeCheckBoxes += checkBox;
                }
            }
          }
          $("#keyword_types").append(typeCheckBoxes);
        }
    });
});
                function processJson(json,langMap) {
                var result = new Array();
                for(var i = 0; i < json.length; ++i) {
                    dataObject = {}

                    var neutral_title = json[i].title_language_neutral
                    jQuery.each(json[i], function(key, val) {
                        if(key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                            var split_arr = key.split("_");
                            if(langMap.indexOf(split_arr[1]) > -1){
                                dataObject[key] = val;
                            }
                        }
                    });

                    neutral_title = typeof (neutral_title) ==  'undefined' ? json[i].title_nob : json[i].title_language_neutral;

                    dataObject['identifier'] = json[i].id
                    dataObject['title'] = neutral_title
                    dataObject['wordclass'] = json[i].wordclass;
                    dataObject['visibility'] = json[i].visibility;
                    dataObject['types'] = json[i].type_id;
                    dataObject['approved'] = json[i].approved;

                    result.push(dataObject);
                }
                return result;
            }
    </r:script>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:1]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:2]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1-2">
            <g:form name="updateKeywordForm" useToken="true" action="doUpdateKeyword">
                <g:hiddenField name="keywordIdentifier" value="${keyword.getIdentifier()}" />
                <g:hiddenField name="numberOfSets" id="numberOfSets" value="1" />
                <g:hiddenField name="searchDomain" value="${searchDomain}" />
                <div id="keywordFieldSet">
                    <fieldset>
                        <legend>Update Keyword</legend>
                        <div id="updateKeywordWordClass">
                            <label>Word Class ${wordClass}</label>
                            <g:select name="wordClass" id="wordClass" from="${wordclasses}" value="${wordClass}" noSelection="['':'-Choose word class-']" />
                        </div>

                        <g:each in="${languages}" var="lang">
                            <g:checkBox name="languageSelect" class="hiddenLanguageSelect" value="${lang.key}" /></label>
                        </g:each>

                        <div id="updateKeywordVisible">
                            <label>Visible? : ${keyword.getVisibility()}</label>
                            <g:if test="${keyword.getVisibility() == '1'}">
                                <label class="radio">
                                    <g:radio name="visibilityRadios" id="visibilityRadios" value="visibilityOption1" checked="checked" />Yes
                                </label>
                                <label class="radio">
                                    <g:radio name="visibilityRadios" id="visibilityRadios" value="visibilityOption2" />No
                                </label>
                            </g:if>
                            <g:else>
                                <label class="radio">
                                    <g:radio name="visibilityRadios" id="visibilityRadios" value="visibilityOption1"/>Yes
                                </label>
                                <label class="radio">
                                    <g:radio name="visibilityRadios" id="visibilityRadios" value="visibilityOption2"  checked="checked"  />No
                                </label>
                            </g:else>
                        </div>

                        <div id="updateKeywordTypes">
                            <div id="keyword_types">
                                <p id="keyword_type_label">Keyword types</p>
                                    <g:each in="${keyword.getTypes()}" var="type">
                                        <g:if test="${type.key != 'keyword'}">
                                            <g:each in="${type.value}" var="typeName">
                                                <g:if test="${typeName.key.contains("#eng")}">
                                                    <label>${typeName.value} <g:checkBox name="typeSelect" value="${type.key}" checked="true" /></label>
                                                </g:if>
                                            </g:each>
                                        </g:if>
                                    </g:each>
                            </div>
                            <label>Add a new type <input type="text" name="keywordByIdTypeTitle" id="keywordByIdTypeTitle" placeholder="Type a type identifier..." autocomplete="off"> </label>
                        </div>

                        <div id="updateKeywordNames">
                            <label>Names</label>
                            <div id="keywordNames">
                                <g:each in="${keywordNames}" var="wordName">
                                    <g:if test="${wordName.key != 'http://psi.topic.ndla.no/#language-neutral'}">
                                        <span id="wordSpan_${wordName.key.toString().substring(wordName.key.toString().indexOf("#")+1)}"><label>${languages.get(wordName.key)} title</label><input type="text" name="${wordName.key}" id="${wordName.key}" placeholder="type a ${wordName.value} title" autocomplete="off" value="${wordName.value}">  <span>Remove <g:checkBox name="nameRemove" value="${wordName.key.toString().substring(wordName.key.toString().indexOf("#")+1)}" checked="false" /></span></span>
                                    </g:if>
                                </g:each>
                            </div>
                            <br />
                            <br />
                            <g:select id="missingLanguages" from="${missinglangs.entrySet()}" name="missingLanguages" optionKey="key" optionValue="value" noSelection="['':'Add a language']"/> <button class="btn" id="addLanguageButton" type="button">Go!</button>
                        </div>


                        <div id="updateKeywordProcessState">
                            <label>Process state</label>
                            <g:hiddenField name="initialProcessState" id="initialProcessState" value="${keyword.getProcessState()}" />
                            <g:if test="${keyword.getProcessState().equals("0")}">
                                <label class="radio">
                                    <g:radio name="processStateRadios" id="processStateRadios" value="0" checked="checked" />Not processed
                                </label>
                                <label class="radio">
                                    <g:radio  name="processStateRadios" id="processStateRadios" value="1" />Pending
                                </label>
                                <label class="radio">
                                    <g:radio  name="processStateRadios" id="processStateRadios" value="2" />Processed
                                </label>
                            </g:if>
                            <g:elseif test="${keyword.getProcessState().equals("1")}">
                                <label class="radio">
                                    <g:radio name="processStateRadios" id="processStateRadios" value="0"/>Not processed
                                </label>
                                <label class="radio">
                                    <g:radio  name="processStateRadios" id="processStateRadios" value="1" checked="checked" />Pending
                                </label>
                                <label class="radio">
                                    <g:radio  name="processStateRadios" id="processStateRadios" value="2" />Processed
                                </label>
                            </g:elseif>
                            <g:elseif test="${keyword.getProcessState().equals("2")}">
                                <label class="radio">
                                    <g:radio name="processStateRadios" id="processStateRadios" value="0" disabled="disabled" />Not processed
                                </label>
                                <label class="radio">
                                    <g:radio  name="processStateRadios" id="processStateRadios" value="1" disabled="disabled" />Pending
                                </label>
                                <label class="radio">
                                    <g:radio  name="processStateRadios" id="processStateRadios" value="2" disabled="disabled"  checked="checked" />Processed
                                </label>
                            </g:elseif>
                        </div>

                        <div id="updateKeywordApproved">
                            <label>Approved?</label>
                            <g:hiddenField name="initialApprovalState" id="initialApprovalState" value="${keyword.getApproved()}" />
                            <g:if test="${keyword.getApproved().equals('false') || null == keyword.getApproved()}">
                                <label class="radio">
                                    <g:radio name="approvedRadios" id="approvedRadios" value="false" checked="checked" />No
                                </label>
                                <label class="radio">
                                    <g:radio  name="approvedRadios" id="approvedRadios" value="true" />Yes
                                </label>
                            </g:if>
                            <g:else>
                                <label class="radio">
                                    <g:radio name="approvedRadios" id="approvedRadios" value="false" />No
                                </label>
                                <label class="radio">
                                    <g:radio  name="approvedRadios" id="approvedRadios" value="true" checked="checked" />Yes
                                </label>
                            </g:else>
                        </div>
                    </fieldset>
                </div>
                <!--
                <g:img dir="images" file="add.png" id="addKeywordAlternativeButton" />
                -->

                <br />
                <br />
                <g:submitButton name="submit" class="btn btn-warning" value="Submit" />

            </g:form>
        </div>
    </div>
</div>
</body>
</html>