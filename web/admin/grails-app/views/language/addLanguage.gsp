<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Languages</title>
    <style>
    #myModal {
        width: 600px;
        height: 400px;
        margin: 0 0 0 -400px;
        overflow: auto;
    }

    #languageId {
        width: 300px;
    }
    </style>
    <r:script>
        function processLink(){
            var loadingContent = "<h2>Loading...</h2>";
            $(".modal-body").html(loadingContent);

            var url = "/admin/service/getISO639Content";
            $(".modal-body").load(url)
        }

        function setIsoCode(isostring) {
            $('#languageId').val(isostring);
            $('#myModal').fadeOut("fast");
            $('#myModal').modal('hide');
        }

        $('#myModal').on('show', function () {
            $(this).find('.modal-body').css({width:'auto', height:'auto', 'max-height':'100%'});
        });

	</r:script>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu" model="[param:7]" />
</div>
<div class="tabbable">

    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab','removeLanguage':'false']">Update</g:link></li>
                    <li class="active"><a href="#tab1-2" data-toggle="tab">Add</a></li>
                    <li><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab','removeLanguage':'true']">Remove</g:link></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1-2">
                        <g:form name="addLanguageForm" useToken="true" action="doAddLanguage">
                            <div id="languageFieldSet">
                                <fieldset>
                                    <label>ISO-639-3 PSI </label>
                                    <input type="text" name="languageId" id="languageId" placeholder="Enter a language identifier...">
                                    &nbsp;<a href="#" onclick="processLink();" data-target="#myModal" role="button"data-toggle="modal">Choose ISO code</a>
                                    <br />
                                    <br />
                                    <g:each in="${allLangMap}" var="name">
                                        <label>${name.value}</label><input type="text" name="namestring_${shortMap.get(name.key)}" id="namestring_${shortMap.get(name.key)}" placeholder="" autocomplete="off" value="">
                                    </g:each>
                                </fieldset>
                            </div>
                            <br />
                            <br />

                            <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                        </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="myModalLabel">ISO 639-3 codes</h3>
    </div>
    <div class="modal-body" style="min-width:1000px;">
        <p>ISO 639-3 codes</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
</body>
</html>