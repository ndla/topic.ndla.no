<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Languages</title>
    <r:script>
        $(document).ready(function() {

            $('#addLanguageButton').click(function() {
                var selectedLanguageId = $("#missingLanguages option:selected").val()
                selectedLanguageId = selectedLanguageId.substring(selectedLanguageId.indexOf("#")+1)
                var selectedLanguageText = $("#missingLanguages option:selected").text()
                $("#languageFieldSet").append('<label>'+selectedLanguageText+' title</label><input type="text" name="namestring_'+selectedLanguageId+'" id="namestring_'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+' title" autocomplete="off" value="">');
            });
            $('body').on('click', "input[name=nameRemove]" ,function(){
                if($(this).is(':checked')) {
                    console.log($(this).val())
                    $("#nameSpan_"+$(this).val()).remove();
                }
            });
        });
    </r:script>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu" model="[param:7]" />
</div>
<div class="tabbable">

    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab1-1" data-toggle="tab">Update</a></li>
                    <li><g:link controller="language" action="addLanguage" params="['data-toggle': 'tab','removeLanguage':'false']">Add</g:link></li>
                    <li><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab','removeLanguage':'true']">Remove</g:link></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1-1">
                        <g:form name="updateLanguageForm" useToken="true" action="doUpdateLanguage">
                            <div id="languageFieldSet">
                                <fieldset>
                                    <legend>Update <em>${languageName}</em></legend>
                                    <g:each in="${langMap}" var="name">
                                        <span id="nameSpan_${shortMap.get(name.key.toString())}"><label>${allLangMap.get(name.key)}</label><input type="text" name="namestring_${shortMap.get(name.key)}" id="namestring_${shortMap.get(name.key)}" placeholder="" autocomplete="off" value="${name.value}"><span>Remove <g:checkBox name="nameRemove" value="${shortMap.get(name.key.toString())}" checked="false" /></span></span>
                                    </g:each>
                                </fieldset>
                            </div>

                            <br />
                            <g:select id="missingLanguages" from="${missingLangs.entrySet()}" name="missingLanguages" optionKey="key" optionValue="value" noSelection="['':'Add a language']"/> <button class="btn" id="addLanguageButton" type="button">Go!</button>
                            <br />
                            <br />
                            <g:hiddenField name="languageId" value="${languageTopicId}" />
                            <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                        </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>