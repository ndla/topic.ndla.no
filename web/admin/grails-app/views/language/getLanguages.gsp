<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:7]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:1]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <g:if test="${removeLanguage == 'false'}">
                        <li class="active"><a href="#tab1-1" data-toggle="tab">Update</a></li>
                        <li><g:link controller="language" action="addLanguage" params="['data-toggle': 'tab']">Add</g:link></li>
                        <li><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab','removeLanguage':'true']">Remove</g:link></li>
                    </g:if>
                    <g:else>
                        <li><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab','removeLanguage':'false']">Update</g:link></li>
                        <li><g:link controller="language" action="addLanguage" params="['data-toggle': 'tab']">Add</g:link></li>
                        <li class="active"><a href="#tab1-1" data-toggle="tab">Remove</a></li>
                    </g:else>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1-1">
                        <g:form name="getLanguageForm" useToken="true" action="doSetLanguage">
                            <g:hiddenField name="removeLanguage" value="${removeLanguage}" />
                            <label>Languages</label>
                            <g:select id="language" from="${languages.entrySet()}" name="language" optionKey="key" optionValue="value" noSelection="['':'Choose a language']"/>

                            <br />
                            <br />
                            <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>