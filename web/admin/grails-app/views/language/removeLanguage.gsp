<%--
  Created by IntelliJ IDEA.
  User: rolfguescini
  Date: 27.05.14
  Time: 16:06
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
</head>

<body>

<div class="navbar">
    <g:render template="/actionmenu"model="[param:7]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:1]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li><g:link controller="language" action="getLanguages" params="['data-toggle': 'tab','removeLanguage':'false']">Update</g:link></li>
                    <li><g:link controller="language" action="addLanguage" params="['data-toggle': 'tab','removeLanguage':'false']">Add</g:link></li>
                    <li class="active"><a href="#tab1-1" data-toggle="tab">Remove</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1-1">
                        <g:form name="updateSubjectForm" useToken="true" action="doRemoveLanguage">
                            <g:hiddenField name="languageId" value="${languageId}" />
                            <p>Are you sure you want to delete ${languages[languageId]}?</p>
                            <g:submitButton name="submit" class="btn btn-danger" value="Delete" />
                        </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>