<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="en" class="no-js"><!--<![endif]-->
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<title><g:layoutTitle default="Grails"/></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<g:layoutHead />
        <!--script src="/admin/static/js/jqueryDownoaded.js" type="text/javascript" ></script-->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
		<r:require modules="core, fontawesome, moment" />
		<r:layoutResources />
        <!--link href="/admin/static/css/main.css" type="text/css" rel="stylesheet" media="screen, projection" /-->
    <style media="all">
    #toggle-button {
        cursor: pointer;
    }

    .keywordFieldSet {
        border: solid 1px #ebebeb;
    }

    #keyword_type_label,
    #keywordTypeTitle,
    .hiddenLanguageSelect,
    #updateKeywordApproved,
    #updateKeywordVisible,
    #updateKeywordTypes,
    #updateKeywordWordClass,
    #updateKeywordNames,
    #accordion1{
        display: none;
    }

    #keywordTitle {
        margin-top: 4px;
    }

    #keyword_types {
        margin-left: 20px;
        width: 200;
        height: 150;
        overflow: auto;
        padding: 4px;
    }

    #updateKeywordTypes,
    #updateKeywordNames,
    #updateKeywordApproved,
    #updateKeywordVisible {
        background-color: #EFEFEF;
        border: 2px solid #E5E5E5;
        padding: 10px;
        width: 600px;
        margin-top: 20px;
    }

    #siteFieldSet,
    #psiFieldSet,
    #updateSubjectNames,
    #addSubjectNames{
        background-color: #EFEFEF;
        border: 2px solid #E5E5E5;
        padding: 10px;
        width: 600px;
        margin-top: 20px;
    }

    #add-button {
        float: right;
    }

    #updateKeywordTypes label,
    #updateKeywordNames label,
    #updateSubjectNames label,
    #updateKeywordApproved label,
    #updateKeywordVisible label{
        font-weight: bold;
    }

    #keywordTypeTitle,#addLanguageButton{
        margin-top: 20px;
    }

    #pager-back{
        float: left;
    }

    #pager-forward,.input-append-removeSubjectTopics{
        float: right;
    }

    /* ONTOLOGY */
    .hiddenTopics,#newTopicWrapper,#requiredWarning,#hierarchicalWrapper,#horizontalWrapper,.updateRoleSpan,.chosenTopicsHidden,.assocTypeWrapper {
        display: none;
    }


    .updateRelationWrapper{
        background-color: #EFEFEF;
        border: 2px solid #E5E5E5;
        padding: 10px;
        clear: both;
        overflow: auto;
        height: auto;
    }

    .nameWrapper{
        float: left;
        padding: 10px;
        width: 49%;
    }

    .addNameWrapper{
        clear: both;
        float: left;
        padding: 10px;
        width: 49%;
    }

    .roleWrapper{
        float: right;
        padding: 10px;
        width: 49%;
    }


    #data-table-subjectTopics{
        clear: both;
    }



    #updateTopicNames,
    #updateTopicDescriptions,
    #updateTopicApproved,
    #updateTopicImages {
        background-color: #EFEFEF;
        border: 2px solid #E5E5E5;
        padding: 10px;
        width: 600px;
        margin-top: 20px;
    }

    #updateTopicDescriptions label,
    #updateTopicNames label,
    #updateSubjectNames label,
    #updateTopicApproved label,
    #updateTopicImages label{
        font-weight: bold;
    }

    /* END ONTOLOGY */
    /** bootstrap addons **/

    .panel-primary {
        border-color: #428BCA;
    }
    .panel {
        background-color: #FFFFFF;
        border: 1px solid rgba(0, 0, 0, 0);
        border-radius: 4px;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.05);
        margin-bottom: 20px;
    }
    * {
        box-sizing: border-box;
    }

    .panel-heading {
        border-bottom: 1px solid rgba(0, 0, 0, 0);
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
        padding: 2px 15px;
        background-color: #428BCA;
    }

    .panel-heading > .table, .panel-heading > .table th {
        border: 0 none;
        margin: 0;
    }

    .table-condensed > thead > tr > th,
    .table-condensed > tbody > tr > th,
    .table-condensed > tfoot > tr > th,
    .table-condensed > thead > tr > td,
    .table-condensed > tbody > tr > td,
    .table-condensed > tfoot > tr > td {
        padding: 5px;
    }

    </style>
	</head>
	<body>
	<div class="container">
		<div class="masthead">
            <ul class="nav nav-pills pull-right">
                <li class="active"><a href="/admin"><g:message code="default.home.label" /></a></li>
                <li><a href="#"><g:message code="default.about.label" /></a></li>
                <li><a href="#"><g:message code="default.contact.label" /></a></li>
                <sec:ifNotLoggedIn>
					<li><g:link controller="login" action="auth"><g:message code="default.signin.label" /></g:link></a></li>
				</sec:ifNotLoggedIn>
				<sec:ifLoggedIn>
					<li><g:link controller="logout"><g:message code="default.signout.label" /></g:link></li>
				</sec:ifLoggedIn>
            </ul>
            <h2 class="text-info">NDLA Topics Admin</h2>
            <p>
            	<small>
            		<g:link controller="${params.controller}" action="${params.action}" params="[lang: 'en']">English</g:link>&nbsp;|
            		<g:link controller="${params.controller}" action="${params.action}" params="[lang: 'nb']">Bokmål</g:link>
        		</small>
            </p>
        </div>
        <hr />
		<div class="row-fluid">
			<div class="span12">
				<div id="flash">
					<g:if test="${flash.success}">
						<div class="alert alert-success">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Well done!</strong>&nbsp;${flash.success}
						</div>
					</g:if>
					<g:if test="${flash.warning}">
						<div class="alert alert">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Warning!</strong>&nbsp;${flash.warning}
						</div>
					</g:if>
					<g:if test="${flash.error}">
						<div class="alert alert-error">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<strong>Oh snap!</strong>&nbsp;${flash.error}
						</div>
					</g:if>
				</div> <!-- /flash -->
				<div id="content">
					<g:layoutBody />
				</div> <!-- /content -->
			</div> <!-- /span10 -->
		</div> <!-- /row-fluid -->
		<hr />
		<div class="footer">
			<div class="container">
				<a href="#">About NDLA Topics Admin Application</a>&nbsp;|
				<a href="#">Frequently Asked Questions</a>
				<br/>
				Copyright &copy; 2013 by NDLA. All rights reserved.
				<br />
				<br />
			</div> <!-- /container -->
		</div> <!-- /footer -->
	</div> <!-- /container -->
	<r:layoutResources />
	</body>
</html>
