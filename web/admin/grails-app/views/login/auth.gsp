<html>
<head>
	<meta name='layout' content='main'/>
	<title><g:message code="springSecurity.login.title"/></title>
</head>
<body>
<div>
	<div>
		<g:if test='${flash.message}'>
			<div class='alert alert-error'>${flash.message}</div>
		</g:if>
		<div>
			<h3><g:message code="springSecurity.login.header"/>
		</div>
		<form action='${postUrl}' method='POST' id='loginForm' class='nice' autocomplete='off'>
			<p>
				<label for='username'><g:message code="springSecurity.login.username.label"/></label>
				<input type='text' class="input-text six" name='j_username' id='username' />
			</p>
			<p>
				<label for='password'><g:message code="springSecurity.login.password.label"/></label>
				<input type='password' class="input-text six" name='j_password' id='password' />
			</p>
			<p id="remember_me_holder">
				<input type='checkbox' name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if>/>
				<label for='remember_me'><g:message code="springSecurity.login.remember.me.label"/></label>
			</p>
			<p>
				<input type='submit' class="btn btn-info" id="submit" value='${message(code: "springSecurity.login.button")}'/>
			</p>
		</form>
	</div>
</div>
<script type='text/javascript'>
	<!--
	(function() {
		document.forms['loginForm'].elements['j_username'].focus();
	})();
	// -->
</script>
</body>
</html>