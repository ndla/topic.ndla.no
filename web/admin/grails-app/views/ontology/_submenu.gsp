<ul class="nav nav-tabs">
    <g:if test="${param == 1}">
        <li class="active"><a href="#tab1" data-toggle="tab">Subjects</a></li>
        <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'addOntologyTopics' : 'true']">Subject topics</g:link></li>
        <li><g:link controller="ontology" action="createRelationshipType" params="['data-toggle': 'tab']">Relation types</g:link></li>
        <li><g:link controller="ontology" action="addRelationRole" params="['data-toggle': 'tab']">Relation role types</g:link></li>
        <li><g:link controller="ontology" action="addHierarchicalRelation" params="['data-toggle': 'tab']">Hierarchical Topic Relations</g:link></li>
        <li><g:link controller="ontology" action="addHorisontalRelation" params="['data-toggle': 'tab']">Horizontal Topic Relations</g:link></li>
    </g:if>
    <g:if test="${param == 2}">
        <li><g:link controller="ontology" action="addSubject" params="['data-toggle': 'tab']">Subjects</g:link></li>
        <li class="active"><a href="#tab2" data-toggle="tab">Subject topics</a></li>
        <li><g:link controller="ontology" action="createRelationshipType" params="['data-toggle': 'tab']">Relation types</g:link></li>
        <li><g:link controller="ontology" action="addRelationRole" params="['data-toggle': 'tab']">Relation role types</g:link></li>
        <li><g:link controller="ontology" action="addHierarchicalRelation" params="['data-toggle': 'tab']">Hierarchical Topic Relations</g:link></li>
        <li><g:link controller="ontology" action="addHorisontalRelation" params="['data-toggle': 'tab']">Horizontal Topic Relations</g:link></li>
    </g:if>
    <g:if test="${param == 3}">
        <li><g:link controller="ontology" action="addSubject" params="['data-toggle': 'tab']">Subjects</g:link></li>
        <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'addOntologyTopics' : 'true']">Subject topics</g:link></li>
        <li class="active"><a href="#tab3" data-toggle="tab">Relation types</a></li>
        <li><g:link controller="ontology" action="addRelationRole" params="['data-toggle': 'tab']">Relation role types</g:link></li>
        <li><g:link controller="ontology" action="addHierarchicalRelation" params="['data-toggle': 'tab']">Hierarchical Topic Relations</g:link></li>
        <li><g:link controller="ontology" action="addHorisontalRelation" params="['data-toggle': 'tab']">Horizontal Topic Relations</g:link></li>
    </g:if>
    <g:if test="${param == 7}">
        <li><g:link controller="ontology" action="addSubject" params="['data-toggle': 'tab']">Subjects</g:link></li>
        <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'addOntologyTopics' : 'true']">Subject topics</g:link></li>
        <li><g:link controller="ontology" action="createRelationshipType" params="['data-toggle': 'tab']">Relation types</g:link></li>
        <li class="active"><a href="#tab4" data-toggle="tab">Relation role types</a></li>
        <li><g:link controller="ontology" action="addHierarchicalRelation" params="['data-toggle': 'tab']">Hierarchical Topic Relations</g:link></li>
        <li><g:link controller="ontology" action="addHorisontalRelation" params="['data-toggle': 'tab']">Horizontal Topic Relations</g:link></li>
    </g:if>
    <g:if test="${param == 4}">
        <li><g:link controller="ontology" action="addSubject" params="['data-toggle': 'tab']">Subjects</g:link></li>
        <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'addOntologyTopics' : 'true']">Subject topics</g:link></li>
        <li><g:link controller="ontology" action="createRelationshipType" params="['data-toggle': 'tab']">Relation types</g:link></li>
        <li><g:link controller="ontology" action="addRelationRole" params="['data-toggle': 'tab']">Relation role types</g:link></li>
        <li class="active"><a href="#tab5" data-toggle="tab">Hierarchical Topic Relations</a></li>
        <li><g:link controller="ontology" action="addHorisontalRelation" params="['data-toggle': 'tab']">Horizontal Topic Relations</g:link></li>
    </g:if>
    <g:if test="${param == 5}">
        <li><g:link controller="ontology" action="addSubject" params="['data-toggle': 'tab']">Subjects</g: link></li>
        <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'addOntologyTopics' : 'true']">Subject topics</g:link></li>
        <li><g:link controller="ontology" action="createRelationshipType" params="['data-toggle': 'tab']">Relation types</g:link></li>
        <li><g:link controller="ontology" action="addRelationRole" params="['data-toggle': 'tab']">Relation role types</g:link></li>
        <li><g:link controller="ontology" action="addHierarchicalRelation" params="['data-toggle': 'tab']">Hierarchical Topic Relations</g:link></li>
        <li class="active"><a href="#tab6" data-toggle="tab">Horizontal Topic Relations</a></li>
    </g:if>
    <g:if test="${param == 6}">
        <li><g:link controller="ontology" action="addSubject" params="['data-toggle': 'tab']">Subjects</g: link></li>
        <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'addOntologyTopics' : 'true']">Subject topics</g:link></li>
        <li><g:link controller="ontology" action="createRelationshipType" params="['data-toggle': 'tab']">Relation types</g:link></li>
        <li><g:link controller="ontology" action="addRelationRole" params="['data-toggle': 'tab']">Relation role types</g:link></li>
        <li><g:link controller="ontology" action="addHierarchicalRelation" params="['data-toggle': 'tab']">Hierarchical Topic Relations</g:link></li>
        <li><g:link controller="ontology" action="addHorisontalRelation" params="['data-toggle': 'tab']">Horizontal Topic Relations</g:link></li>
    </g:if>
</ul>