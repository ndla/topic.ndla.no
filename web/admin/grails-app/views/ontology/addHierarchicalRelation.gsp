<!doctype html>
<html>
<head>
<meta name="layout" content="main"/>
<title>NDLA Topics Admin Application - Curriculum Ontologies</title>
<style>
#myModal {
    height: auto;
    margin: 0 0 0 -400px;
    max-height: 600px;
    max-width: 1200px;
    width: auto;
    overflow: scroll;
}
</style>
<r:script>
    $(document).ready(function () {
        searchDomain = $("#searchDomain").val()
        pathPrefix = $("#pathPrefix").val()
        getSubjectOntologyRelations();


        $("input[name=relationSites]").click(function () {
            var site = $("input:radio[name=relationSites]:checked").val();
            var subjects = []
            $("#relationSubjects").empty()

            subjects = getSubjects(site);
            if (subjects.length > 0) {
                var options = "";
                for (var i = 0; i < subjects.length; i++) {
                    var subject = subjects[i];
                    options += '<option value="' + subject.identifier + '">' + subject.name + '</option>';
                }
                $("#relationSubjects").append(options);
                $("#subjectSelector").removeClass("hiddenTopics")
            }
        });


        $("#toggle-button").click(function (event) {
            $("#more-options").slideToggle();
        });

        $('#myModal').on('show', function () {
            $(this).find('.modal-body').css({width: 'auto', height: 'auto', 'max-height': '100%'});
        });


        $('body').on('click', "a[data-reifierid]", function () {
            var result = "";
            var $theTrashCan = $(this);
            var reifierIdUrl = $theTrashCan.attr('data-reifierid');
            reifierId = reifierIdUrl.substring(reifierIdUrl.indexOf("#") + 1)
            var theTopicIdString = $theTrashCan.parent().attr('data-related-topicid')
            var topicIdArray = theTopicIdString.split("_");
            var topicId1 = topicIdArray[0];
            var topicId2 = topicIdArray[1];
            var topicType = 'topic'
            var theHeader = $theTrashCan.parent().prevAll('li.nav-header:first');
            //console.log(theHeader.text())
            var relationType = theHeader.attr('data-relationtypeid').substring(theHeader.attr('data-relationtypeid').indexOf("#") + 1)
            /*console.log("reifierId : " + reifierId
                    + "\ntopicId1: " + topicId1
                    + "\ntopicId2: " + topicId2
                    + "\nrelationType: " + relationType)
             */
            if (reifierIdUrl != 'undefined') {
                var host = window.location.protocol + '//' + window.location.host;
                var serviceUrl = host + pathPrefix + '/service/removeAssociationByReifier';
                var site = $("#site").val()
                $.ajax({
                    async: false,
                    url: serviceUrl,
                    dataType: 'json',
                    data: {
                        'reifier': reifierId,
                        'topic1': topicId1,
                        'topicType1': 'topic',
                        'topic2': topicId2,
                        'topicType2': 'topic',
                        'relationType': relationType,
                        'site': site
                    }

                }).done(function (data) {
                    result = data;
                    if (result == reifierIdUrl) {
                        $theTrashCan.parent().remove();
                        var thisContainerLength = theHeader.next('.associationElement').length;
                        var nextContainerLength = theHeader.nextAll('.associationElement').length;
                        if (thisContainerLength < 1 || nextContainerLength < 1) {
                            theHeader.remove()
                        }
                    }

                });
            }
            else {

                $theTrashCan.parent().remove();
                var thisContainerLength = theHeader.next('.associationElement').length;
                var nextContainerLength = theHeader.nextAll('.associationElement').length;
                if (thisContainerLength < 1 || nextContainerLength < 1) {
                    theHeader.remove()
                }
            }

        });


        $('body').on('click', "button[data-role-associationId]", function () {
            var $theButton = $(this);
            var theAssocId = $theButton.attr('data-role-associationid');
            var theTopicIdString = $theButton.attr('data-role-id');
            var topicIdArray = theTopicIdString.split('_');
            var roleId = ''
            var topicId = topicIdArray[0]
            if (topicIdArray[1].indexOf("#") > -1) {
                roleId = topicIdArray[1].substring(topicIdArray[1].indexOf("#") + 1);
            }
            else {
                roleId = topicIdArray[1].substring(topicIdArray[1].lastIndexOf("/") + 1);
            }
            var $theParent = $theButton.parent().parent().parent();

            $theParent.find('button[data-role-associationId="' + theAssocId + '"]').each(function () {
                var $thatButton = $(this);
                $thatButton.removeClass('active');
                var thatTopicIdString = $thatButton.attr('data-role-id');
                var thatTopicIdArray = thatTopicIdString.split('_');
                var thatRoleId = ''
                var thatTopicId = thatTopicIdArray[0]
                if (thatTopicIdArray[1].indexOf("#") > -1) {
                    thatRoleId = thatTopicIdArray[1].substring(thatTopicIdArray[1].indexOf("#") + 1);
                }
                else {
                    thatRoleId = thatTopicIdArray[1].substring(thatTopicIdArray[1].lastIndexOf("/") + 1);
                }


                if (topicId != thatTopicId && roleId != thatRoleId) {
                    $thatButton.addClass('active');
                    return true;
                }

            });
        });

        $("#addRelationButton").click(function () {

                    var topicIds = []
                    var relations = []
                    $("div[data-topicId]").each(function () {
                        var topicId = $(this).attr("data-topicId")
                        var topicName = $(this).text()
                        var topicObject = {}
                        topicObject.topicId = topicId;
                        topicObject.topicName = topicName
                        topicIds.push(topicObject)
                    });

                    $("#relationSelect :selected").each(function () {
                        var relationObject = {}
                        relationObject.relationId = $(this).val();
                        relationObject.relationName = $(this).text()
                        relations.push(relationObject)
                    });


                    var existing = $("#chosenTopicRelations").val();
                    if (typeof existing == 'undefined' || existing.length == 0) {
                        existing = []
                    }
                    else {
                        existing = $.parseJSON(existing);
                    }

                    if (topicIds.length > 0 && relations.length > 0) {
                        openRoleChooser(existing, topicIds, relations);
                    }
                    else {
                        alert("You have to choose topics and / or relationship types");
                    }

                }
        );


        $(':input[data-typeahead-target]').each(function () {
            var $this = $(this);
            var subjectTopics = $.parseJSON($("#subjectTopics").val());
            if ($this.hasClass("topicTitleSearch")) {

                $this.typeahead({
                    source: function (query, process) {
                        keywords = [];
                        map = {};
                        langMap = [];
                        langNames = {};
                        usedLangs = {}
                        //var serviceUrl = "http://search/ndla_search/keywords/" + query;
                        var serviceUrl = searchDomain + "/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                        var searchLanguage = ""
                        var searchType = $('#typeSelect').val();
                        $("input[name=languageSelect]").each(function () {
                            if ($(this).is(':checked') && $(this).val() != "") {
                                searchLanguage += "language[]=" + $(this).val() + "&";
                                langMap.push($(this).val())
                            }
                            langNames[$(this).val()] = $(this).attr("text");
                        });

                        searchLanguage = searchLanguage.substr(0, searchLanguage.length - 1)
                        serviceUrl += "?" + searchLanguage + "&type=" + searchType + "&index=topics";

                        $.ajax({
                            url: serviceUrl,
                            type: 'GET',
                            dataType: 'json',
                            success: function (response) {
                                //console.log("RESPONSE: "+response)
                                var data = processJson(response, langMap);

                                $.each(data, function (i, keyword) {
                                    var key = keyword.title;
                                    var keyId = keyword.identifier;
                                    if (!(key in map) && keyId.indexOf("topic") > -1) {
                                        if ($.inArray(keyId, subjectTopics) !== -1) {
                                            map[key] = keyword;
                                            keywords.push(key);
                                        }
                                    }
                                });
                                process(keywords);
                            }
                        }, 300);
                    },
                    matcher: function (item) {
                        return true;
                    },
                    updater: function (item) {
                        var topicObject = map[item];
                        var typeAheadId = $this.attr("id");
                        var number = typeAheadId.substring(typeAheadId.length - 1);
                        if ($("#chosenTopicHolder" + number).children().length > 0) {
                            $("#chosenTopicHolder" + number).empty()
                        }
                        $("#chosenTopicHolder" + number).append('<br /><div data-topicId="' + topicObject.identifier + '" class="label label-info">' + item + '</div>');
                        return "";
                        $this.on('mousedown', function (e) {
                            e.preventDefault();
                        });
                        $this.typeahead('close');
                    }
                });
            }
        });

        $(document).keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();
            }
        });
    });

    function saveRoles() {
        var existing = $.parseJSON($("#chosenTopicRelations").val());
        var topicIds = $.parseJSON($("#currentTopicIds").val());
        var relations = $.parseJSON($("#currentRelationIds").val());
        var roleNameMapping = $.parseJSON($("#chosenTopicRelationRoleMap").val())

        var roleMapping = {}
        var foundAssocIds = []
        var hasRoles = false;
        var hasRolesInListing = 0;

        $("#myModal").find('button[data-role-associationId]').each(function () {
            if ($(this).hasClass('active')) {
                var roles = {}
                var theAssocId = $(this).attr('data-role-associationid');
                var theTopicIdString = $(this).attr('data-role-id');
                var topicIdArray = theTopicIdString.split('_');
                roles.topicId = topicIdArray[0];
                roles.roleName = topicIdArray[1];
                if (foundAssocIds.indexOf(theAssocId + "_" + roles.topicId) == -1) {
                    roleMapping[theAssocId + "_" + roles.topicId] = new Array()
                    foundAssocIds.push(theAssocId + "_" + roles.topicId)
                }

                roleMapping[theAssocId + "_" + roles.topicId].push(roles);
            }

        });


        for (var i = 0; i < relations.length; i++) {
            var relation = relations[i];
            if (existing.length > 0) {
                var relationNum = hasRelation(existing, relation.relationId)
                if (relationNum > -1) {
                    var addedRelationObject = existing[relationNum]
                    addedRelationObject = addTopics(addedRelationObject, topicIds)
                    existing[relationNum] = addedRelationObject;
                }
                else {
                    var addedRelationObject = {}
                    addedRelationObject.relationId = relation.relationId;
                    addedRelationObject.relationName = relation.relationName
                    addedRelationObject = addTopics(addedRelationObject, topicIds)
                    existing.push(addedRelationObject)
                }
            }
            else {
                var addedRelationObject = {}
                addedRelationObject.relationId = relation.relationId;
                addedRelationObject.relationName = relation.relationName
                addedRelationObject = addTopics(addedRelationObject, topicIds)
                existing.push(addedRelationObject)
            }

        }

        if (Object.keys(roleMapping).length > 0 && Object.keys(roleMapping).length == foundAssocIds.length) {
            for (var k = 0; k < existing.length; k++) {
                var relationMapObject = existing[k];
                var topics = relationMapObject.topics
                for (var l = 0; l < topics.length; l++) {
                    var topic = topics[l];
                    var ids = topic.ids.split("_")
                    if (typeof topic.roles == 'undefined') {
                        topic.roles = []
                    }
                    for (var m = 0; m < ids.length; m++) {
                        if (foundAssocIds.indexOf(relationMapObject.relationId + "_" + ids[m]) != -1) {
                            topic.roles.push(roleMapping[relationMapObject.relationId + "_" + ids[m]][0])
                        }
                    }


                }


            }

            $("#chosenTopicRelations").val(JSON.stringify(existing));
            renderChosenRelations(null, null);
            $("#currentRelationIds").val("")
            $("#currentTopicIds").val("")
            $("#chosenTopicHolder1").empty()
            $("#chosenTopicHolder2").empty()
            $('#myModal').modal('hide');
        }
        else {
            alert("You have to chose roles for all the relationships!")
        }


    }

    function openRoleChooser(existing, topicIds, relations) {
        var labelContent = "Choose topic roles";
        var roleMapping = []
        if ($("#chosenRoleMap").val() != "") {
            roleMapping = $.parseJSON($("#chosenRoleMap").val());
        }

        $("#myModalLabel").html(labelContent);
        var html = '<div id="roleTopicChooser">'
        html += '<table class="table table-striped">';
        if (existing.length > 0) {
            var foundRole = []
            for (var k = 0; k < relations.length; k++) {
                html += '<tr id="trk_' + k + '"><td colspan="2">' + relations[k].relationName + '</td></tr>';
                var roles = getAssociationRoles(relations[k].relationId, 'hierarchical')
               // console.log("ROLES: " + JSON.stringify(roles));
                var existingTopics = existingTopicRelations(existing, topicIds, relations[k].relationId);

                var topicIdObject = topicIds[k];
                //if (existingTopics == 0) {
                    for (var l = 0; l < topicIds.length; l++) {
                        var relatedTopicObject = topicIds[l]
                        html += '<td>' + relatedTopicObject.topicName + '&nbsp; <div class="btn-group" data-toggle="buttons-radio">';
                        /*
                         for (var m = 0; m < roles.length; m++) {
                         var role1 = roles[m];
                         if(!containsRole(roleMapping,role1)){
                         roleMapping.push(role1)
                         }
                         html += '<button type="button" class="btn btn-info btn-small roleButton" data-role-associationId="' + relations[k].relationId + '" data-role-id="' + relatedTopicObject.topicId+ '_' + role1.roleId + '">' + role1.roleName + '</button>';
                         }
                         html += '</div></td>';
                         */
                        var role1 = roles[0];

                        if (foundRole[role1.roleName] == null) {
                            roleMapping.push(role1)
                            html += '<button type="button" class="btn btn-info btn-small roleButton" data-role-associationId="' + relations[k].relationId + '" data-role-id="' + relatedTopicObject.topicId + '_' + role1.roleId + '§1">' + role1.roleName + '</button>';
                        }
                        foundRole.push(role1.roleName)

                        var role2 = roles[1];
                        if (foundRole[role2.roleName] == null) {
                            roleMapping.push(role2)
                            html += '<button type="button" class="btn btn-info btn-small roleButton" data-role-associationId="' + relations[k].relationId + '" data-role-id="' + relatedTopicObject.topicId + '_' + role2.roleId + '§2">' + role2.roleName + '</button>';
                        }
                        foundRole.push(role2.roleName)
                        html += '</div></td>';
                    }
                //}
                html += '</tr>';
            }
        }
        else {
            var foundRole = []
            for (var k = 0; k < relations.length; k++) {
                html += '<tr id="trk_' + k + '"><td colspan="2">' + relations[k].relationName + '</td></tr>';
                var roles = getAssociationRoles(relations[k].relationId, 'hierarchical')
                //console.log("ROLES: " + JSON.stringify(roles));
                html += '<tr id="trl_' + k + '">';
                for (var l = 0; l < topicIds.length; l++) {
                    var relatedTopicObject = topicIds[l]
                    html += '<td>' + relatedTopicObject.topicName + '&nbsp; <div class="btn-group" data-toggle="buttons-radio">';
                    /*
                     for (var m = 0; m < roles.length; m++) {
                     var role1 = roles[m];
                     if(!containsRole(roleMapping,role1)){
                     roleMapping.push(role1)
                     }
                     html += '<button type="button" class="btn btn-info btn-small roleButton" data-role-associationId="' + relations[k].relationId + '" data-role-id="' + relatedTopicObject.topicId+ '_' + role1.roleId + '">' + role1.roleName + '</button>';
                     }
                     html += '</div></td>';
                     */
                    var role1 = roles[0];

                    if (foundRole[role1.roleName] == null) {
                        roleMapping.push(role1)
                        html += '<button type="button" class="btn btn-info btn-small roleButton" data-role-associationId="' + relations[k].relationId + '" data-role-id="' + relatedTopicObject.topicId + '_' + role1.roleId + '§1">' + role1.roleName + '</button>';
                    }
                    foundRole.push(role1.roleName)

                    var role2 = roles[1];
                    if (foundRole[role2.roleName] == null) {
                        roleMapping.push(role2)
                        html += '<button type="button" class="btn btn-info btn-small roleButton" data-role-associationId="' + relations[k].relationId + '" data-role-id="' + relatedTopicObject.topicId + '_' + role2.roleId + '§2">' + role2.roleName + '</button>';
                    }
                    foundRole.push(role2.roleName)
                    html += '</div></td>';
                }
            }
            html += '</tr>';
        }

        $("#chosenTopicRelations").val(JSON.stringify(existing))
        $("#currentRelationIds").val(JSON.stringify(relations));
        $("#currentTopicIds").val(JSON.stringify(topicIds));
        $("#chosenRoleMap").val(JSON.stringify(roleMapping));
        html += '</table></div>';
        $(".modal-body").html(html);
        $('#myModal').modal();
    }

    function containsRole(roleMapper, role) {
        var containsRoleId = false;
        for (var i = 0; i < roleMapper.length; i++) {
            roleObject = roleMapper[i]
            if (roleObject.roleId == role.roleId) {
                containsRoleId = true;
                break;
            }
        }
        return containsRoleId;
    }

    function getSubjectOntologyRelations() {
        var subjectId = $("#subjectId").val()
        if (subjectId != "") {
            var host = window.location.protocol + '//' + window.location.host;
            var serviceUrl = host + pathPrefix + '/service/getSubjectOntologyTopicRelations';
            var site = $("#site").val()

            var result = [];
            var found = []
            $.ajax({
                async: false,
                url: serviceUrl,
                dataType: 'json',
                data: {
                    'assocType': 'hierarchical-relation-type',
                    'subjectId': subjectId,
                    'site': site,
                    'reifierType': 'subjectmatter'
                }

            }).done(function (data) {
                result = data;

            });

            $("#chosenTopicRelations").val(JSON.stringify(result.topics));
            $("#chosenTopicRelationRoleMap").val(JSON.stringify(result.roleNameMapping));
            renderChosenRelations(result.topics, result.roleNameMapping)
        }

    }

    function getAssociationRoles(associationId, associationType) {
        var host = window.location.protocol + '//' + window.location.host;
        var serviceUrl = host + pathPrefix + '/service/getAssociationRoles'

        var theId = ""
        if (associationId.indexOf("#") > -1) {
            theId = associationId.substring(associationId.indexOf("#") + 1);
        }
        else {
            theId = associationId.substring(associationId.lastIndexOf("/") + 1);
        }

        var result = [];
        var found = []
        $.ajax({
            async: false,
            url: serviceUrl,
            dataType: 'json',
            data: {
                'psiId': theId,
                'associationType': associationType
            }

        }).done(function (data) {
            for (var i = 0; i < data.length; i++) {
                // if (found.indexOf(data[i].roleId) == -1) {
                result.push(data[i])
                //found.push(data[i].roleId)
                //}
            }

        });
        return result;
    }

    function renderChosenRelations(existingData, roleNameMapping) {
        var existing = {}
        var roleNames = {}
        if (null != existingData) {
            existing = existingData
        }
        else {
            existing = $.parseJSON($("#chosenTopicRelations").val());
        }

        if (null != roleNameMapping) {
            roleNames = roleNameMapping
        }
        else {
            roleNames = $.parseJSON($("#chosenTopicRelationRoleMap").val());

            var missingRoleNames = ""
            if ($("#chosenRoleMap").val() != "") {
                missingRoleNames = $.parseJSON($("#chosenRoleMap").val())
                for (l = 0; l < missingRoleNames.length; l++) {
                    var roleNameElement = missingRoleNames[l];
                    if (!containsRole(roleNames, roleNameElement)) {
                        var newRoleName = {};
                        newRoleName.psi = roleNameElement.roleId;
                        var newRoleNameArr = [];
                        var newRoleNameElement = {}
                        newRoleNameElement.language = "http://psi.oasis-open.org/iso/639/#eng";
                        newRoleNameElement.name = roleNameElement.roleMap;
                        newRoleNameArr.push(newRoleNameElement)
                        newRoleName.roleNames = newRoleNameArr
                        roleNames.push(newRoleName);
                    }

                }
            }//end if roleMap not empty
        }


        $("#chosenRelationsNav").empty();
        for (var k = 0; k < existing.length; k++) {
            var relationMapObject = existing[k];
            $("#chosenRelationsNav").append('<li class="nav-header" data-relationtypeid="' + relationMapObject.relationId + '">' + relationMapObject.relationName + '</li>');
            for (var l = 0; l < relationMapObject.topics.length; l++) {
                var relatedTopicObject = relationMapObject.topics[l];
                var idArray = relatedTopicObject.ids.split('_');
                var nameArray = relatedTopicObject.names.split("/");
                var namestring = renderTopicRoles(idArray, nameArray, relatedTopicObject.roles, roleNames)
                //$("#chosenRelationsNav").append('<li data-related-topicId="' + relatedTopicObject.ids + '"><input type="checkbox" name="check_'+relationMapObject.relationId+'" id="check_'+relationMapObject.relationId+'" /> <a class="parentRelationCheck" href="javascript:">' + namestring + '</a></li>');
                $("#chosenRelationsNav").append('<li data-related-topicId="' + relatedTopicObject.ids + '" class="associationElement"> ' + namestring + '<a href="#" data-reifierId="' + relatedTopicObject.reifierId + '" class="trash"><i class="icon-trash" /></a></li>');
            }
        }
    }

    function renderTopicRoles(ids, names, roles, mapping) {
        var nameString = "";
        for (var i = 0; i < names.length; i++) {
            var topicId = ids[i];
            var name = names[i];
            var foundNameString = false;
            for (var j = 0; j < roles.length; j++) {
                role = roles[j]
                if (role.topicId == topicId) {
                    var roleName = "";
                    if (role.roleName.indexOf("#") > -1) {
                        roleName = role.roleName.substring(role.roleName.indexOf("#") + 1);
                    }
                    else {
                        roleName = role.roleName.substring(role.roleName.lastIndexOf("/") + 1);
                    }

                    for (var k = 0; k < mapping.length; k++) {
                        var mapElement = mapping[k];
                        var mapPSI = mapElement.psi;
                        if (mapPSI.indexOf("#") > -1) {
                            mapPSI = mapPSI.substring(mapPSI.indexOf("#") + 1);
                        }
                        else {
                            mapPSI = mapPSI.substring(mapPSI.indexOf("/") + 1);
                        }

                        if (mapPSI == roleName) {

                            for (l = 0; l < mapElement.roleNames.length; l++) {
                                roleNameElement = mapElement.roleNames[l];
                                if (roleNameElement.language == "http://psi.oasis-open.org/iso/639/#eng") {
                                    roleName = roleNameElement.name;
                                    foundNameString = true;
                                    break;
                                }
                            }
                        }
                        if (foundNameString) {
                            break;
                        }
                    }

                    if (roleName.indexOf("§") > -1) {
                        roleName = roleName.substring(0, (roleName.length - 2));
                    }

                    if (i == 0) {
                        nameString += name + " (" + roleName + ")/";
                    }
                    else {
                        nameString += name + " (" + roleName + ")";
                    }
                }
            }
        }
        return nameString;
    }

    function existingTopicRelations(existingData, topics, relationId) {
        var topicMapper = []
        var topicIndex = hasRelation(existingData, relationId);
        if (topicIndex != -1) {
            var existingTopicRelations = existingData[topicIndex]
            for (var i = 0; i < existingTopicRelations.topics.length; i++) {
                var existingTopic = existingTopicRelations.topics[i];
                //for(var j = 0; j < existingTopics.length;j++) {
                //var existingTopic = existingTopics[j];
                var existingTopicIds = existingTopic.ids.split('_');
                for (var k = 0; k < topics.length; k++) {
                    var topic = topics[k];
                    if (existingTopicIds.indexOf(topic.topicId) != -1) {
                        topicMapper.push(topic.topicId);
                    }
                }

                //}
            }
        }

        return topicMapper;
    }

    function hasRelation(existing, relationId) {
        var hasRelationship = -1;
        for (var j = 0; j < existing.length; j++) {
            var existingRelObject = existing[j];
            if (existingRelObject.relationId == relationId) {
                hasRelationship = j;
                break;
            }
        }
        return hasRelationship;
    }

    function relationHasTopics(chosenTopicIds, topics) {
        var hasTopics = false;
        for (var j = 0; j < topics.length; j++) {
            if (topics[j].ids == chosenTopicIds) {
                hasTopics = true;
                break;
            }
        }
        return hasTopics;
    }


    function addTopics(jsonObject, topicIds) {
        if (typeof jsonObject.topics == 'undefined' || jsonObject.topics == null) {
            jsonObject.topics = []
        }

        var chosenTopicIds = ""
        var chosenTopicNames = ""
        var newObject = {}
        for (var j = 0; j < topicIds.length; j++) {
            var theTopicObject = topicIds[j]
            if (j == 0) {
                chosenTopicIds += theTopicObject.topicId + "_"
                chosenTopicNames += theTopicObject.topicName + "/"
            }
            else {
                chosenTopicIds += theTopicObject.topicId
                chosenTopicNames += theTopicObject.topicName
            }

        }
        newObject.ids = chosenTopicIds
        newObject.names = chosenTopicNames;
        if (!relationHasTopics(chosenTopicIds, jsonObject.topics)) {
            jsonObject.topics.push(newObject)
        }

        return jsonObject;
    }

    function processJson(json, langMap) {
        var result = new Array();
        for (var i = 0; i < json.length; ++i) {
            dataObject = {}

            var neutral_title = json[i].title_language_neutral
            jQuery.each(json[i], function (key, val) {
                if (key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                    var split_arr = key.split("_");
                    if (langMap.indexOf(split_arr[1]) > -1) {
                        dataObject[key] = val;
                    }
                }
            });

            neutral_title = typeof (neutral_title) == 'undefined' ? json[i].title_nob : json[i].title_language_neutral;

            dataObject['identifier'] = json[i].id
            dataObject['title'] = neutral_title
            dataObject['wordclass'] = json[i].wordclass;
            dataObject['visibility'] = json[i].visibility;
            dataObject['types'] = json[i].type_id;
            dataObject['approved'] = json[i].approved;

            result.push(dataObject);
        }
        return result;
    }

    function getSubjects(site) {
        var host = window.location.protocol + '//' + window.location.host;
        var serviceUrl = host + pathPrefix + '/service/getSubjectsBySiteString'
        var subjects = []
        $.ajax({
            async: false,
            url: serviceUrl,
            data: {
                'site': site
            },
            dataType: 'json',
        }).done(function (data) {
            subjects = data;
        });
        return subjects;
    }
</r:script>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:4]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">

                <div class="tab-content">
                    <div class="tab-pane active" id="tab2-1">
                            <g:if test="${relationSite == null}">
                                <g:form name="addHierarchicalTopicsForm" useToken="true" action="doSetSubjectForAddHierarchicalRelation">
                                    <g:hiddenField name="subjectId" value="${relationSubject}" />
                                    <g:hiddenField name="site" id="site" value="${relationSite}" />
                                    <g:hiddenField name="searchDomain" value="${searchDomain}" />
                                    <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />
                                <label>Choose a Site</label>
                                <label class="checkbox inline">
                                    <input type="radio" name="relationSites" value="NDLA"> NDLA
                                </label>
                                <label class="checkbox inline">
                                    <input type="radio" name="relationSites" value="FYR"> FYR
                                </label>
                                <br />
                                <br />

                                <div id="subjectSelector" class="hiddenTopics">
                                    <label>Choose subject</label>
                                    <g:select id="relationSubjects" from="${subjects.entrySet()}" name="relationSubjects" optionKey="key" optionValue="value" noSelection="['':'']"/>
                                </div>
                                <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                                </g:form>
                            </g:if>
                            <g:else>
                                <g:link controller="ontology" action="addHierarchicalRelation" params="['data-toggle': 'tab']">Back</g:link>
                                <br />
                                <g:form name="addHierarchicalTopicsForm" useToken="true" action="doAddHierarchicalRelation">
                                    <g:hiddenField name="subjectId" value="${relationSubject}" />
                                    <g:hiddenField name="site" id="site" value="${relationSite}" />
                                    <g:hiddenField name="searchDomain" value="${searchDomain}" />
                                    <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />
                                <div id="relationWrapper" class="pull-left">
                                    <span class="muted" id="toggle-button"><small>Toggle advanced search options</small></span>
                                    <div id="more-options" style="display: none">
                                        <g:each in="${languages}" var="lang">
                                            <label>${lang.value}<g:checkBox name="languageSelect" class="LanguageSelect" value="${lang.key}"/></label>
                                        </g:each>

                                        <br />
                                        <label>Type</label>
                                        <select id="typeSelect">
                                            <option value="any">Any</option>
                                            <option value="start">Start</option>
                                            <option value="end">End</option>
                                        </select>
                                        <br />
                                        <br />
                                        <label>Approval</label>
                                        <select id="approvedSelect">
                                            <option value="all">All</option>
                                            <option value="true">Approved</option>
                                            <option value="false">Disapproved</option>
                                        </select>


                                        <label>Sites</label>
                                        <label class="checkbox inline">
                                            <input type="checkbox" name="siteCheckbox" id="siteCheckbox" value="ndla" checked="1">NDLA
                                        </label>
                                        <label class="checkbox inline">
                                            <input type="checkbox"  name="siteCheckbox"  id="siteCheckbox" value="deling">Deling
                                        </label>
                                        <label class="checkbox inline">
                                            <input type="checkbox"  name="siteCheckbox"  id="siteCheckbox" value="nygiv">Nygiv
                                        </label>
                                    </div>                                <div>
                                    <label>Search for topic 1</label>
                                    <input type="text" data-typeahead-target="chosenTopics_1" class="topicTitleSearch" name="topicTitleSearch1" id="topicTitleSearch1" placeholder="Type a topic title..." autocomplete="off">
                                    <div id="chosenTopicHolder1"></div>
                                </div>
                                    <div>
                                        <div id="hierarchicalTypeWrapper">
                                            <select multiple="multiple" id="relationSelect" name="relationSelect" class="input-xxlarge">
                                                <option value="">Choose relationship type</option>
                                                <g:each in="${hierarchicalTypeMap.entrySet()}" var="type">
                                                    <option value="${type.key}">${type.value}</option>
                                                </g:each>
                                            </select>
                                        </div>
                                    </div>

                                    <div>
                                        <label>Search for topic 2</label>
                                        <input type="text" data-typeahead-target="chosenTopics_2" class="topicTitleSearch" name="topicTitleSearch2" id="topicTitleSearch2" placeholder="Type a topic title..." autocomplete="off">
                                        <div id="chosenTopicHolder2"></div>
                                    </div>

                                    <div class="chosenTopicsHidden" id="chosenTopics1">
                                    </div>
                                    <div class="chosenTopicsHidden" id="chosenTopics2">
                                    </div>

                                    <g:hiddenField name="chosenTopicRelationRoleMap" id="chosenTopicRelationRoleMap" value="" />
                                    <g:hiddenField name="subjectTopics" id="subjectTopics" value="${subjectTopics}" />
                                    <g:hiddenField name="chosenRoleMap" id="chosenRoleMap" value="" />
                                    <g:hiddenField name="chosenTopicRelations" id="chosenTopicRelations" value="" />
                                    <g:hiddenField name="currentTopicIds" id="currentTopicIds" value="" />
                                    <g:hiddenField name="currentRelationIds" id="currentRelationIds" value="" />

                                    <button type="button" id="addRelationButton" class="btn">Add relation</button>


                                    <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                                </div>

                                <div id="relationWellWrapper" class="pull-right">
                                    <div class="well">
                                        <ul class="nav nav-list" id="chosenRelationsNav"></ul>
                                    </div>
                                </div>
                                </g:form>
                            </g:else>



                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="myModalLabel">Choose role topics</h3>
        </div>
        <div class="modal-body">
            <p>Choose role topics</p>
        </div>
        <div class="modal-footer">
            <a href="javascript:" onclick="saveRoles()" class="btn btn-primary">Save changes</a>
        </div>
    </div>
</div>
</body>
</html>