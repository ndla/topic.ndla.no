<%--
  Created by IntelliJ IDEA.
  User: rolfguescini
  Date: 15.08.14
  Time: 16:04
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
        <title>NDLA Topics Admin Application - Ontologies</title>
        <style>
        #myModal {
            width: 600px;
            height: 600px;
            margin: 0 0 0 -400px;
            overflow: auto;
        }
        </style>
        <r:script>
            $(document).ready(function () {
                searchDomain = $("#searchDomain").val()


                $('#addLanguageButton').click(function() {
                    var selectedLanguageId = $("#missingTitleLanguages option:selected").val()
                    var selectedLanguageText = $("#missingTitleLanguages option:selected").text()
                    $("#newTopicTitles").append('<label>'+selectedLanguageText+' title</label><input type="text" name="title_'+selectedLanguageId+'" id="title_'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+' title" autocomplete="off" value="">');
                });

                $('#addDescriptionLanguageButton').click(function(){
                    var selectedLanguageId = $("#missingDescriptionLanguages option:selected").val()
                    var selectedLanguageText = $("#missingDescriptionLanguages option:selected").text()
                    $("#newTopicDescriptions").append('<label>'+selectedLanguageText+' description</label><textarea id="description_'+selectedLanguageId+'" cols="50" rows="5" name="description_'+selectedLanguageId+'"></textarea>');
                });
            });
        </r:script>
</head>

<body>
<div class="navbar">
<g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
<g:render template="submenu" model="[param:7]" />
<div class="tab-content">
    <div class="tab-pane active" id="tab1">
        <div class="tabbable tabs-right">
            <ul class="nav nav-tabs">
                <li><g:link controller="ontology" action="editRelationRoleTopics" >Update</g:link></li>
                <li class="active"><a href="#tab6-1" data-toggle="tab">Add</a></li>
                <!--li><g:link controller="ontology" action="removeRelationRoleTopics">Remove</g:link></li-->
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab-1">
                    <g:form name="addRelationRoleForm" useToken="true" action="doCreateRelationRole">
                        <div id="newTopic">
                            <g:select id="roleType" name='roleType'
                                      noSelection="${['null':'Select a role type...']}"
                                      from='${roleTypes}'
                                      optionKey="key" optionValue="value"></g:select>
                            <div class="well well-small" id="newTopicTitle">
                                <p class="modal-header"><strong>Topic titles</strong></p>
                                <div class="alert alert-block" id="requiredWarning">
                                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                                    <h4>Error!</h4>
                                    The Required English and Bokmål titles cannot be empty
                                </div>
                                <label>Required English Title</label>
                                <input type="text" id="title_eng" name="title_eng" placeholder="Type an English title...">
                                <label>Required Norwegian Bokmål Title</label>
                                <input type="text" id="title_nob" name="title_nob" placeholder="Type a Norwegian Bokmål title...">
                                <div id="newTopicTitles">

                                </div>
                            <select id="missingTitleLanguages" name="missingTitleLanguages">
                                <option value="">Add a language</option>
                                <g:each in="${missinglangs}" var="langItem">
                                    <option value="${shortMap.get(langItem.key)}">${langItem.value}</option>
                                </g:each>
                            </select>
                            <button class="btn-mini" id="addLanguageButton" type="button">Go!</button>
                            </div>
                            <br />

                            <div class="well well-small" id="newTopicDescriptions">
                                <p class="modal-header"><strong>Topic descriptions</strong></p>
                            </div>
                            <select id="missingDescriptionLanguages" name="missingDescriptionLanguages">
                                <option value="">Add a language</option>
                                <g:each in="${langMap}" var="langItem">
                                    <option value="${shortMap.get(langItem.key)}">${langItem.value}</option>
                                </g:each>
                            </select>
                            <button class="btn-mini" id="addDescriptionLanguageButton" type="button">Go!</button>
                            <br />

                        </div>
                        <br />
                        <br />
                        <br />
                        <g:submitButton name="submit" class="btn btn-warning pull-right" value="Submit" />
                    </g:form>
                </div>
            </div>
         </div>
    </div>
</div>
</body>
</html>