<%--
  Created by IntelliJ IDEA.
  User: rolfguescini
  Date: 27.05.14
  Time: 10:35
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
    <r:script>
        $(document).ready(function() {


            $("#subjectId").change(function(){
               var subjectId =  $("#subjectId").val();
                var amount = $("#siteFieldSet").children().length
                $("#psiFieldSet").append('<label>Subject PSI <input type="text" name="psis" id="psis"  autocomplete="off" value="http://psi.topic.ndla.no/#subject_'+subjectId+'"></label>');
            });

            $("#UDIRsubjectId").change(function(){
               var UDIRId = $("#UDIRsubjectId").val();
               var amount = $("#siteFieldSet").children().length
                $("#psiFieldSet").append('<label>Subject PSI <input type="text" name="psis" id="psis"  autocomplete="off" value="http://psi.topic.ndla.no/#subject_'+UDIRId+'"></label>');
            });

            $('#addLanguageButton').click(function() {
                var selectedLanguageId = $("#missingSubjectNameLanguages option:selected").val()
                var selectedLanguageText = $("#missingSubjectNameLanguages option:selected").text()
                if(selectedLanguageId != ""){
                    $("#subjectNames").append('<label>'+selectedLanguageText+' title</label><input type="text" name="'+selectedLanguageId+'" id="'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+'" autocomplete="off" value="">');
                    $("#missingSubjectNameLanguages option:selected").remove();
                }

            });

            $('#addSubjectSiteButton').click(function() {
                var amount = $("#siteFieldSet").children().length
                $("#siteFieldSet").append('<label>Site URL <input type="text" name="sites" id="sites" placeholder="http://" autocomplete="off" value="http://"></label>');
            });

            $('#addSubjectPsiButton').click(function() {
                var amount = $("#psiFieldSet").children().length
                $("#psiFieldSet").append('<label>Subject PSI <input type="text" name="psis" id="psis" placeholder="http://" autocomplete="off" value="http://psi.topic.ndla.no/#subject_"></label>');
            });

            $('body').on('click', "input[name=subjectSites]" ,function(){
                if($(this).is(':checked')) {
                    var site = $(this).val();
                    spanid = ""
                    if(site == "http://ndla.no/"){
                        var spanid = $(this).val().substring(7, $(this).val().indexOf(".no"));
                    }
                    else{
                        var spanid = $(this).val().substring(7, $(this).val().indexOf(".ndla.no"));
                    }

                    $("#siteSpan_"+spanid).remove();
                }
            });

        });

    </r:script>
</head>

<body>
    <div class="navbar">
        <g:render template="/actionmenu"model="[param:6]" />
    </div>
    <div class="tabbable">
        <g:render template="submenu" model="[param:1]" />
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div class="tabbable tabs-right">
                    <ul class="nav nav-tabs">
                        <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']">Update</g:link></li>
                        <li class="active"><a href="#tab1-1" data-toggle="tab">Add</a></li>
                        <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'true']">Remove</g:link></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1-1">
                            <g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']">Back</g:link>
                            <g:form name="addSubjectForm" useToken="true" action="doAddSubject">

                                <div id="subjectFieldSet">
                                    <fieldset>
                                        <legend>Add subject </legend>
                                        <label>Subject Id</label><input type="text" name="subjectId" id="subjectId" placeholder="Add a subject Id" autocomplete="off" value="">

                                        <label>UDIRSubject Id</label><input type="text" name="UDIRsubjectId" id="UDIRsubjectId" placeholder="Add a UDIR code" autocomplete="off" value="">
                                        <div>Ex: 'NAT1' : <a href="http://www.udir.no/Lareplaner/Finn-lareplan/" target="_blank">Look up curriculum codes at UDIR</a></div>

                                        <fieldset>
                                            <legend>Names</legend>
                                        <div id="addSubjectNames">
                                            <div id="subjectNames">
                                                <label>English Name</label><input type="text" name="englishTitle" id="englishTitle" placeholder="type the obligatory English  name" autocomplete="off" value="">
                                            </div>
                                            <g:select id="missingSubjectNameLanguages" from="${langMap.entrySet()}" name="missingSubjectNameLanguages" optionKey="key" optionValue="value" noSelection="['':'Add a language']"/> <button class="btn" id="addLanguageButton" type="button">Go!</button>
                                        </div>
                                        </fieldset>
                                        <br />
                                        <br />
                                        <fieldset>
                                            <legend>Sites</legend>
                                            <div id="siteFieldSet">
                                                <span id="siteSpan_ndla">
                                                    <label>
                                                        Site URL
                                                        <input id="sites" type="text" value="http://ndla.no" autocomplete="off" placeholder="http://" name="sites" value="http://ndla.no">
                                                        - Delete
                                                        <input id="subjectSites" type="checkbox" value="http://ndla.no/" name="subjectSites">
                                                    </label>
                                                </span>
                                            </div>
                                            <br /> Example: http://ndla.no, http://fyr.ndla.no
                                            <br />
                                            <br />
                                            <div id="add-button"><label>Add a site: <g:img dir="images" file="add.png" id="addSubjectSiteButton" /></label></div>
                                        </fieldset>

                                        <fieldset>
                                            <legend>PSIs</legend>
                                            <div id="psiFieldSet">
                                            </div>
                                            <div id="add-button"><label>Add a psi: <g:img dir="images" file="add.png" id="addSubjectPsiButton" /></label></div>
                                        </fieldset>
                                        Example: http://psi.topic.ndla.no/#subject_ENG1
                                        <br />
                                        <br />
                                            <g:submitButton name="submit" class="btn btn-success" value="Submit" />
                                    </fieldset>
                                </div>
                            </g:form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>