<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
    <r:script>
	        $(document).ready(function() {
	            searchDomain = $("#searchDomain").val()

                $("#toggle-button").click(function (event) {
                    $("#more-options").slideToggle();
                });


                $("#addNewTopicTitleButton").click(function(){
                    $('#newTopicWrapper').fadeIn();
                });


                $(document).keypress(function(e) {
                    if(e.which == 13) {
                        e.preventDefault();
                    }
                });

                $("#addNewTopicButton").click(function(){
                    var newTitleEng = $("#newTopicTitleEng").val();
                    var newTitleNob = $("#newTopicTitleNob").val();
                    var wordClass = $("#newTopicTitleWordClass").val();

                    if(newTitleEng != "" && newTitleNob != "") {
                        var table_row = '<tr>';
                        table_row += '<td class="muted">new topic;</td>';
                        table_row += '<td class="pull-left"><strong>'+newTitleEng+'</strong></td>';
                        table_row += '<td><span class="label label-info">new topic</span></td>';
                        table_row +='</tr>';
                        $('#addedTopicsBody').append(table_row);

                        var newTopicVal = "wordClass;"+wordClass+"§eng;"+newTitleEng+"§nob;"+newTitleNob
                        $("#newTopicTitles").children("input").each(function(){
                            if($(this).val() != ""){
                                newTopicVal += "§"+$(this).attr("id")+";"+$(this).val()
                            }

                        });

                       $("#newTopicDescriptions").children("textarea").each(function(){

                        if($(this).val() != ""){
                            newTopicVal += "§"+$(this).attr("id")+";"+$(this).val()
                        }
                      });

                      $("#newTopicImages").children("input").each(function(){
                        if($(this).val() != ""){
                            newTopicVal += "§"+$(this).attr("id")+";"+$(this).val()
                        }
                      });

                        var checkBox = '<g:checkBox name="hiddenTopics" value="'+newTopicVal+'" checked="true"/>';

                        $('#topicWrapper').append(checkBox);
                        $("#newTopicTitleEng").val("");
                        $("#newTopicTitleNob").val("");
                        $('#newTopicTitles').html("");
                        $('#newTopicWrapper').fadeOut();
                        $("#requiredWarning").fadeOut();
                    }
                    else{
                        $("#requiredWarning").fadeIn();
                    }

                })

                $('#addLanguageButton').click(function() {
                   var selectedLanguageId = $("#missingLanguages option:selected").val()
                   var selectedLanguageText = $("#missingLanguages option:selected").text()
                   $("#newTopicTitles").append('<label>'+selectedLanguageText+' title</label><input type="text" name="'+selectedLanguageId+'" id="'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+' title" autocomplete="off" value="">');
                });

                $('#addImageLanguageButton').click(function() {
                   var selectedLanguageId = $("#missingImageLanguages option:selected").val()
                   var selectedLanguageText = $("#missingImageLanguages option:selected").text()
                   $("#newTopicImages").append('<label>'+selectedLanguageText+' Image URL</label><input type="text" name="image_'+selectedLanguageId+'" id="image_'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+' image URL" autocomplete="off" value="">');
                });


                $('#addDescriptionLanguageButton').click(function(){
                    var selectedLanguageId = $("#missingDescriptionLanguages option:selected").val()
                   var selectedLanguageText = $("#missingDescriptionLanguages option:selected").text()
                   $("#newTopicDescriptions").append('<label>'+selectedLanguageText+' description</label><textarea id="description_'+selectedLanguageId+'" cols="50" rows="5" name="description_'+selectedLanguageId+'"></textarea>');
                });

                $('#keywordTitle').typeahead ({
					source: function (query, process) {
					    $("#nameItems").html("");
					    $("#keyword_types").html("");
					    $(".accordion").css("display","none")
						keywords = [];
						map = {};
                        langMap = [];
                        langNames = {};
                        usedLangs = {}
						//var serviceUrl = "http://search/ndla_search/keywords/" + query;
						var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
						var searchLanguage = ""
						var searchType = $('#typeSelect').val();
						$( "input[name=languageSelect]" ).each(function(){
						    if($(this).is(':checked') && $(this).val() != ""){
						        searchLanguage += "language[]="+$(this).val()+"&";
						        langMap.push($(this).val())
						    }
						    langNames[$(this).val()] = $(this).attr("text");
						});

						searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
                        serviceUrl += "?" + searchLanguage + "&type=" + searchType+ "&index=keywords";

						$.ajax({
							url: serviceUrl,
							type: 'GET',
							dataType: 'json',
							success: function (response) {
							//console.log("RESPONSE: "+response)
								var data = processJson(response, langMap);

								$.each(data, function (i, keyword) {
									var key = keyword.title;
									if (!(key in map)) {
										map[key] = keyword;
										keywords.push(key);
									}
								});
							    process(keywords);
							}
						}, 300);
					},
					 matcher: function (item) {
                        return true;
                    },
					updater: function (item) {
                        var keywordObject = map[item];
                        console.log("ITEM: "+JSON.stringify(map[item]))
                        var table_row = '<tr>';
                        table_row += '<td class="muted">'+keywordObject.identifier+'</td>';
                        table_row += '<td class="pull-left"><strong>'+keywordObject.title+'</strong></td>';
                        table_row += '<td><span class="label label-info">keyword</span></td>';
                        table_row +='</tr>';
                        var checkBox = '<g:checkBox name="hiddenTopics" value="'+keywordObject.identifier+'" checked="true"/>';
                        $('#addedTopicsBody').append(table_row);
                        $('#topicWrapper').append(checkBox);

				        return item;
                        $('#keywordTitle').typeahead('close');
                        $('#keywordTitle').val("");
					}
				});


                $('#topicTitle').typeahead ({
                    source: function (query, process) {
                        $("#nameItems").html("");
                        $("#keyword_types").html("");
                        $(".accordion").css("display","none")
                        keywords = [];
                        map = {};
                        langMap = [];
                        langNames = {};
                        usedLangs = {}
                        //var serviceUrl = "http://search/ndla_search/keywords/" + query;
                        var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                        var searchLanguage = ""
                        var searchType = $('#typeSelect').val();
                        $( "input[name=languageSelect]" ).each(function(){
                            if($(this).is(':checked') && $(this).val() != ""){
                                searchLanguage += "language[]="+$(this).val()+"&";
                                langMap.push($(this).val())
                            }
                            langNames[$(this).val()] = $(this).attr("text");
                        });

                        searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
                        serviceUrl += "?" + searchLanguage + "&type=" + searchType+ "&index=topics";

                        $.ajax({
                            url: serviceUrl,
                            type: 'GET',
                            dataType: 'json',
                            success: function (response) {
                                //console.log("RESPONSE: "+response)
                                var data = processJson(response, langMap);

                                $.each(data, function (i, keyword) {
                                    var key = keyword.title;
                                    var keyId = keyword.identifier;
                                    if (!(key in map) && keyId.indexOf("topic") > -1) {
                                        map[key] = keyword;
                                        keywords.push(key);
                                    }
                                });
                                process(keywords);
                            }
                        }, 300);
                    },
                    matcher: function (item) {
                        return true;
                    },
                    updater: function (item) {
                        var topicObject = map[item];
                        var table_row = '<tr>';
                        table_row += '<td class="muted">'+topicObject.identifier+'</td>';
                        table_row += '<td class="pull-left"><strong>'+topicObject.title+'</strong></td>';
                        table_row += '<td><span class="label label-info">topic</span></td>';
                        table_row +='</tr>';

                        var checkBox = '<g:checkBox name="hiddenTopics" value="'+topicObject.identifier+'" checked="true"/>';
                        $('#addedTopicsBody').append(table_row);
                        $('#topicWrapper').append(checkBox);
                        return item;
                        $('#topicTitle').typeahead('close');
                        $('#topicTitle').val("");
                    }
                });
	        });

            function processJson(json,langMap) {
                var result = new Array();
                for(var i = 0; i < json.length; ++i) {
                    dataObject = {}

                    var neutral_title = json[i].title_language_neutral
                    jQuery.each(json[i], function(key, val) {
                        if(key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                            var split_arr = key.split("_");
                            if(langMap.indexOf(split_arr[1]) > -1){
                                dataObject[key] = val;
                            }
                        }
                    });

                    neutral_title = typeof (neutral_title) ==  'undefined' ? json[i].title_nob : json[i].title_language_neutral;

                    dataObject['identifier'] = json[i].id
                    dataObject['title'] = neutral_title
                    dataObject['wordclass'] = json[i].wordclass;
                    dataObject['visibility'] = json[i].visibility;
                    dataObject['types'] = json[i].type_id;
                    dataObject['approved'] = json[i].approved;

                    result.push(dataObject);
                }
                return result;
            }
    </r:script>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:2]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab6-1" data-toggle="tab">Add</a></li>
                    <li><g:link controller="ontology" action="removeSubjectTopics" params="['data-toggle': 'tab', 'subjectId' : subjectId, 'sites' : site]">Remove</g:link></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab6-1">
                                <g:form name="updateSubjectForm" useToken="true" action="doAddSubjectTopics">
                            <g:hiddenField name="subjectId" value="${subjectId}" />
                            <g:hiddenField name="site" value="${site}" />
                            <g:hiddenField name="searchDomain" value="${searchDomain}" />
                            <g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'addOntologyTopics' : 'true']">Back</g:link></li>
                            <fieldset>
                                <legend>Add subject topics to the ${subject.getSubjectName("http://psi.topic.ndla.no/#language-neutral")} ontology</legend>
                                <span class="muted" id="toggle-button"><small>Toggle advanced search options</small></span>
                                <div id="more-options" style="display: none">
                                    <br />
                                    <label>Language</label>
                                    <g:each in="${langMap}" var="langItem">
                                        <label>${langItem.value} <g:checkBox name="languageSelect" value="${shortMap.get(langItem.key)}" text="${langItem.value}" /></label>
                                    </g:each>

                                    <br />
                                    <br />
                                    <label>Type</label>
                                    <select id="typeSelect">
                                        <option value="any">Any</option>
                                        <option value="start" selected="selected">Start</option>
                                        <option value="end">End</option>
                                    </select>
                                </div>

                                <label>Search in keywords</label>
                                <input type="text" name="keywordTitle" id="keywordTitle" placeholder="Type a keyword title..." autocomplete="off">

                                <label>Search for topic</label>
                                <input type="text" name="topicTitle" id="topicTitle" placeholder="Type a topic title..." autocomplete="off">
                                <br />
                                <br />
                                <button class="btn" id="addNewTopicTitleButton" type="button">Add a non existing topic</button>
                                <br />
                                <br />
                                <div id="newTopicWrapper">

                                    <div class="well well-small" id="newTopicTitle">
                                        <p class="modal-header"><strong>Topic titles</strong></p>
                                        <div class="alert alert-block" id="requiredWarning">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <h4>Error!</h4>
                                            The Required English and Bokmål titles cannot be empty
                                        </div>
                                        <label>Required English Title</label>
                                        <input type="text" id="newTopicTitleEng" name="newTopicTitleEng" placeholder="Type an English title...">
                                        <label>Required Norwegian Bokmål Title</label>
                                        <input type="text" id="newTopicTitleNob" name="newTopicTitleNob" placeholder="Type a Norsk bokmål title...">

                                        <div id="newTopicTitles">

                                        </div>

                                        <br />
                                        <g:select name="newTopicTitleWordClass" id="newTopicTitleWordClass" from="${wordClassMap}" optionKey="key" optionValue="value" noSelection="['':'Add a wordClass']"/>
                                    </div>
                                    <g:select id="missingLanguages" from="${missinglangs.entrySet()}" name="missingLanguages" optionKey="key" optionValue="value" noSelection="['':'Add a language']"/> <button class="btn-mini" id="addLanguageButton" type="button">Go!</button>
                                    <br />

                                    <div class="well well-small" id="newTopicDescriptions">
                                        <p class="modal-header"><strong>Topic descriptions</strong></p>
                                    </div>
                                    <select id="missingDescriptionLanguages" name="missingDescriptionLanguages">
                                        <option value="">Add a language</option>
                                        <g:each in="${langMap}" var="langItem">
                                            <option value="${shortMap.get(langItem.key)}">${langItem.value}</option>
                                        </g:each>
                                    </select>
                                    <button class="btn-mini" id="addDescriptionLanguageButton" type="button">Go!</button>
                                    <br />
                                    <div class="well well-small" id="newTopicImages">
                                        <p class="modal-header"><strong>Topic images</strong></p>
                                    </div>
                                    <select id="missingImageLanguages" name="missingImageLanguages">
                                        <option value="">Add a language</option>
                                        <g:each in="${langMap}" var="langItem">
                                            <option value="${shortMap.get(langItem.key)}">${langItem.value}</option>
                                        </g:each>
                                    </select>
                                    <button class="btn-mini" id="addImageLanguageButton" type="button">Go!</button>
                                    <br />
                                    <button class="btn-warning pull-right" id="addNewTopicButton" type="button">Add topic!</button>
                                </div>
                                <br />
                                <br />
                                <br />
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Topic Id</th>
                                                <th>Topic title</th>
                                                <th>Type</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="panel-body">
                                        <!--table-->
                                        <table class="table table-condensed" id="addedTopics">
                                            <tbody id="addedTopicsBody">
                                            </tbody>
                                        </table>
                                        <!--end of table-->
                                    </div>
                                </div>
                                <!--select id="hiddenTopics" name="hiddenTopics" multiple="multiple">

                                </select-->
                                <div id="topicWrapper" class="hiddenTopics">

                                </div>

                                <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                            </fieldset>
                        </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>