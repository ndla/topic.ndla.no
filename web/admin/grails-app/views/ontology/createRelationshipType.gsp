<%--
  Created by IntelliJ IDEA.
  User: rolfguescini
  Date: 15.08.14
  Time: 16:04
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
        <title>NDLA Topics Admin Application - Ontologies</title>
        <style>
        #myModal {
            width: 600px;
            height: 600px;
            margin: 0 0 0 -400px;
            overflow: auto;
        }
        </style>
        <r:script>
            $(document).ready(function() {
                searchDomain = $("#searchDomain").val()

                $("#toggle-button").click(function (event) {
                    $("#more-options").slideToggle();
                });

                pathPrefix = $("#pathPrefix").val()

                $('#myModal').on('show', function () {
                    $(this).find('.modal-body').css({width:'auto', height:'auto', 'max-height':'100%'});
                });

                $('#addLanguageButton').click(function() {
                    var selectedLanguageId = $("#missingLanguages option:selected").val()
                    var selectedLanguageText = $("#missingLanguages option:selected").text()
                    $("#role1Name").append('<label>'+selectedLanguageText+' title</label><input type="text" name="'+selectedLanguageId+'_role1Name" id="'+selectedLanguageId+'_role1Name" placeholder="type a '+selectedLanguageText+' title" autocomplete="off" value="">');
                    $("#role2Name").append('<label>'+selectedLanguageText+' title</label><input type="text" name="'+selectedLanguageId+'_role2Name" id="'+selectedLanguageId+'_role1Name" placeholder="type a '+selectedLanguageText+' title" autocomplete="off" value="">');
                    $("#missingLanguages option:selected").remove();
                });


                $("#ontologyRoleTopics1").on("change", function(){
                    var selectedRoleTopic = $("#ontologyRoleTopics1 option:selected").val()
                    $("#selectedRoleTopicId1").val(selectedRoleTopic)
                    $('#createRoleTopics1Wrapper').css("display","none")
                    $('#searchRoleTopics1Wrapper').css("display","none")

                });

                $("#ontologyRoleTopics2").on("change", function(){
                    var selectedRoleTopic = $("#ontologyRoleTopics2 option:selected").val()
                    $("#selectedRoleTopicId2").val(selectedRoleTopic)
                    $('#createRoleTopics2Wrapper').css("display","none")
                    $('#searchRoleTopics2Wrapper').css("display","none")

                });

                $("#addNewRoleTopic1").click(function(){
                    openTopicCreator(pathPrefix,"1");
                });

                $("#addNewRoleTopic2").click(function(){
                    openTopicCreator(pathPrefix,"2");
                });


                $('#topicSearch1').typeahead ({
                    source: function (query, process) {
                        topics = [];
                        map = {};
                        langMap = [];
                        langNames = {};
                        usedLangs = {}

                        var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                        var searchLanguage = ""
                        var searchType = $('#typeSelect').val();
                        $( "input[name=languageSelect]" ).each(function(){
                            if($(this).is(':checked') && $(this).val() != ""){
                                searchLanguage += "language[]="+$(this).val()+"&";
                                langMap.push($(this).val())
                            }
                            langNames[$(this).val()] = $(this).attr("text");
                        });

                        searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
                        serviceUrl += "?" + searchLanguage + "&type=" + searchType+ "&index=topics";

                        $.ajax({
                            url: serviceUrl,
                            type: 'GET',
                            dataType: 'json',
                            success: function (response) {

                                var data = processJson(response, langMap);
                                $.each(data, function (i, topic) {
                                    var key = topic.title;
                                    var keyId = topic.identifier;
                                    if (!(key in map)) {
                                        map[key] = topic;
                                        topics.push(key);
                                    }
                                });
                                process(topics);
                            }
                        }, 300);
                    },
                    matcher: function (item) {
                        return true;
                    },
                    updater: function (item) {
                        var topicObject = map[item];

                        $("#selectedRoleTopicId1").val(topicObject.identifier)
                        $('#ontologyRoleTopics1Wrapper').css("display","none")
                        $('#createRoleTopics1Wrapper').css("display","none")
                        return item;
                        $('#topicSearch1').typeahead('close');
                        $('#topicSearch1').val("");

                    }
                });


                $('#topicSearch2').typeahead ({
                    source: function (query, process) {
                        topics = [];
                        map = {};
                        langMap = [];
                        langNames = {};
                        usedLangs = {}

                        var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                        var searchLanguage = ""
                        var searchType = $('#typeSelect').val();
                        $( "input[name=languageSelect]" ).each(function(){
                            if($(this).is(':checked') && $(this).val() != ""){
                                searchLanguage += "language[]="+$(this).val()+"&";
                                langMap.push($(this).val())
                            }
                            langNames[$(this).val()] = $(this).attr("text");
                        });

                        searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
                        serviceUrl += "?" + searchLanguage + "&type=" + searchType+ "&index=topics";

                        $.ajax({
                            url: serviceUrl,
                            type: 'GET',
                            dataType: 'json',
                            success: function (response) {
                                //console.log("RESPONSE: "+response)
                                var data = processJson(response, langMap);

                                $.each(data, function (i, topic) {
                                    var key = topic.title;
                                    var keyId = topic.identifier;
                                    if (!(key in map)) {
                                        map[key] = topic;
                                        topics.push(key);
                                    }
                                });
                                process(topics);
                            }
                        }, 300);
                    },
                    matcher: function (item) {
                        return true;
                    },
                    updater: function (item) {
                        var topicObject = map[item];

                        $("#selectedRoleTopicId2").val(topicObject.identifier)
                        $('#ontologyRoleTopics2Wrapper').css("display","none")
                        $('#createRoleTopics2Wrapper').css("display","none")
                        return item;
                        $('#topicSearch2').typeahead('close');
                        $('#topicSearch2').val("");

                    }
                });

                $(document).keypress(function(e) {
                    if(e.which == 13) {
                        e.preventDefault();
                    }
                });
            });




            function processJson(json,langMap) {
                var result = new Array();
                for(var i = 0; i < json.length; ++i) {
                    dataObject = {}

                    var neutral_title = json[i].title_language_neutral
                    jQuery.each(json[i], function(key, val) {
                        if(key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                            var split_arr = key.split("_");
                            if(langMap.indexOf(split_arr[1]) > -1){
                                dataObject[key] = val;
                            }
                        }
                    });

                    neutral_title = typeof (neutral_title) ==  'undefined' ? json[i].title_nob : json[i].title_language_neutral;

                    dataObject['identifier'] = json[i].id
                    dataObject['title'] = neutral_title

                    result.push(dataObject);
                }
                return result;
            }

            function openTopicCreator(pathPrefix,id) {

                var labelContent = "Create a role topic";
                $("#myModalLabel").html(labelContent);
                var loadingContent = "<h2>Loading...</h2>";
                var html = '<div id="newRoleTopic"><div id="newRoleTopicTitle" class="well-small">';
                html += '<p class="modal-header"><strong>Topic titles</strong></p>';
                html += '<label>Required English Title</label><input type="text" placeholder="Type an English title..." name="newTopicTitleEng" id="newTopicTitleEng">';
                html += '<label>Required Norwegian Bokmål Title</label><input type="text" placeholder="Type an English title..." name="newTopicTitleNob" id="newTopicTitleNob"></div>';
                html += '<div id="newTopicDescriptions" class="well-small">';
                html += '<label>English description</label><textarea id="description_eng" cols="50" rows="5" name="description_eng"></textarea>';
                html += '<label>Norsk Bokmål description</label><textarea id="description_nob" cols="50" rows="5" name="description_nob"></textarea></div>';
                html += '<button type="button" id="addRoleTopicButton" class="btn-mini" onclick="saveRoleTopic('+id+')">Add topic</button></div>';
                $(".modal-body").html(html);
            }

            function saveRoleTopic(id){
                var topicObject = {}
                topicObject['names'] = new Array();
                topicObject['descriptions'] = new Array();
                topicObject['images'] = new Array();
                topicObject['types'] = new Array();
                topicObject['fromKeyword'] = 'newTopic';
                topicObject['wordClass'] = 'noun';

                $(".modal-body input").each(function(){
                    var nameObject = {}
                    nameENG = ""
                    nameNOB = ""

                    if($(this).attr("id") == "newTopicTitleEng"){
                        nameObject["name"] = $(this).val();
                        nameENG = $(this).val();
                        nameObject["language"] = "http://psi.oasis-open.org/iso/639/#eng";
                    }
                    else if($(this).attr("id") == "newTopicTitleNob"){
                        nameObject["name"] = $(this).val();
                        nameNOB = $(this).val();
                        nameObject["language"] = "http://psi.oasis-open.org/iso/639/#nob";
                    }
                    topicObject["names"].push(nameObject);
                });

                $(".modal-body textarea").each(function(){
                    var descriptionObject = {}
                    if($(this).attr("id") == "description_eng"){
                        descriptionObject["content"] = $(this).val();
                        descriptionObject["language"] = "http://psi.oasis-open.org/iso/639/#eng";
                    }
                    else if($(this).attr("id") == "description_nob"){
                        descriptionObject["content"] = $(this).val();
                        descriptionObject["language"] = "http://psi.oasis-open.org/iso/639/#nob";
                    }
                    topicObject['descriptions'].push(descriptionObject);
                });
                console.log(JSON.stringify(topicObject))



                var host = window.location.protocol + '//' + window.location.host;
                var serviceUrl = host + pathPrefix+'/service/createTopic'
                $.ajax({
                    async: false,
                    url: serviceUrl,
                    data: {
                        'payload': JSON.stringify(topicObject)
                    },
                    dataType: 'json',
                }).done(function (data) {
                    for(var i = 0; i < data.length; i++) {
                        var key = data[i];
                        $("#selectedRoleTopicId"+id).val(key)
                        console.log(key+ " NAME: "+nameNOB)
                        $("#selectedRoleTopicName"+id).val(nameNOB)
                    }
                    console.log("GOT CREATETOPIC DATA: "+data)
                    $("#selectedRoleTopicName"+id).css("display","block")
                    $('#searchRoleTopics1Wrapper').css("display","none")
                    $('#searchRoleTopics2Wrapper').css("display","none")
                    $('#ontologyRoleTopics2Wrapper').css("display","none")
                    $('#ontologyRoleTopics1Wrapper').css("display","none")
                    $('#createRoleTopics'+id+'Wrapper').css("display","none")
                    $('#myModal').modal('hide')
                });
            }


            function myTrim(x) {
                return x.replace(/^\s+|\s+$/gm,'');
            }
        </r:script>
</head>

<body>
<div class="navbar">
<g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
<g:render template="submenu" model="[param:3]" />
<div class="tab-content">
    <div class="tab-pane active" id="tab1">
        <div class="tabbable tabs-right">
            <ul class="nav nav-tabs">
                <li><g:link controller="ontology" action="editRelationShipTypes" >Update</g:link></li>
                <li class="active"><a href="#tab6-1" data-toggle="tab">Add</a></li>
                <li><g:link controller="ontology" action="removeRelationShipTypes">Remove</g:link></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab6-1">
                    <g:form name="createRelationTypeForm" useToken="true" action="doCreateRelationType">
                        <g:hiddenField name="searchDomain" id="searchDomain" value="${searchDomain}" />
                        <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />
                        <span class="muted" id="toggle-button"><small>Toggle advanced search options</small></span>
                        <div id="more-options" style="display: none">
                            <br />
                            <label>Language</label>
                            <g:each in="${langMap}" var="langItem">
                                <label>${langItem.value} <g:checkBox name="languageSelect" value="${shortMap.get(langItem.key)}" text="${langItem.value}" /></label>
                            </g:each>

                            <br />
                            <br />
                            <label>Type</label>
                            <select id="typeSelect">
                                <option value="any">Any</option>
                                <option value="start" selected="selected">Start</option>
                                <option value="end">End</option>
                            </select>
                        </div>
                        <br /><br />

                        <label>Computer friendly id (Ex: has-employee )</label>
                        <g:textField name="computerName" id="computerName" placeholder="Enter a computer friendly id" />
                        <br />
                        <label>Select the type of relationship type you are creating</label>
                        <g:select id="relationType" from="${relationTypeTypes.entrySet()}" name="relationType" optionKey="key" optionValue="value" noSelection="['':'Choose a type']"/>

                        <table width="800" cellpadding="2" cellspacing="2">
                            <tr><th>Role 1</th><th>Role 2</th></tr>
                            <tr>
                                <td>
                                    <label>Required English Title (Ex: Has employee)</label>
                                    <input type="text" id="eng_role1Name" name="eng_role1Name" placeholder="Type an English title...">
                                    <label>Required Norwegian Bokmål Title (Ex: Har ansatt)</label>
                                    <input type="text" id="nob_role1Name" name="nob_role1Name" placeholder="Type a Bokmål title...">
                                    <div id="role1Name">
                                    </div>
                                </td>
                                <td>
                                    <label>Required English Title (Ex: Employed by)</label>
                                    <input type="text" id="eng_role2Name" name="eng_role2Name" placeholder="Type an English title...">
                                    <label>Required Norwegian Bokmål Title (Ex: Ansatt hos)</label>
                                    <input type="text" id="nob_role2Name" name="nob_role2Name" placeholder="Type a Bokmål title...">
                                    <div id="role2Name">
                                    </div>
                                </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td><g:select id="missingLanguages" from="${missinglangs.entrySet()}" name="missingLanguages" optionKey="key" optionValue="value" noSelection="['':'Add a new language']"/> <button class="btn-mini" id="addLanguageButton" type="button">Go!</button></td>
                                </tr>
                                <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                                <tr>
                                    <td>
                                        <div id="ontologyRoleTopics1Wrapper"><label>Select role played for player 1</label><g:select id="ontologyRoleTopics1" from="${ontologyRoleTopics.entrySet()}" name="ontologyRoleTopics1" optionKey="key" optionValue="value" noSelection="['':'Choose a role']"/></div>
                                        <div id="searchRoleTopics1Wrapper"><label>Search for an existing role topic played for player 1</label>
                                        <input class="span10" name="topicSearch1" id="topicSearch1" type="text" autocomplete="off"></div>
                                        <br /><br />
                                        <div id="createRoleTopics1Wrapper"><a href="#" data-target="#myModal" role="button" data-toggle="modal"><g:img dir="images" file="add.png" id="addNewRoleTopic1" /></a> Create a new role topic played for player 1</div>
                                        <input class="span10 hide" name="selectedRoleTopicName1" id="selectedRoleTopicName1" type="text" autocomplete="off" value="">
                                    </td>
                                    <td>
                                        <div id="ontologyRoleTopics2Wrapper"><label>Select role played for player 2</label><g:select id="ontologyRoleTopics2" from="${ontologyRoleTopics.entrySet()}" name="ontologyRoleTopics2" optionKey="key" optionValue="value" noSelection="['':'Choose a role']"/></div>
                                        <div id="searchRoleTopics2Wrapper"><label>Search for an existing role topic played for player 2</label>
                                        <input class="span10" name="topicSearch2" id="topicSearch2" type="text" autocomplete="off"></div>
                                        <br /><br />
                                        <div id="createRoleTopics2Wrapper"><a href="#" data-target="#myModal" role="button"data-toggle="modal"><g:img dir="images" file="add.png" id="addNewRoleTopic2" /></a> Create a new role topic played for player 2</div>
                                        <input class="span10 hide" name="selectedRoleTopicName2" id="selectedRoleTopicName2" type="text" autocomplete="off" value="">

                                    </td>
                                </tr>
                        </table>
                        <g:hiddenField name="selectedRoleTopicId1" id="selectedRoleTopicId1" value="" />
                        <g:hiddenField name="selectedRoleTopicId2" id="selectedRoleTopicId2" value="" />
                        <div id="newRoleTopic">
                        </div>

                        <br />
                        <br />
                        <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                    </g:form>
                </div>
            </div>
         </div>
    </div>
</div>
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="myModalLabel">Create a role topic</h3>
        </div>
        <div class="modal-body">
            <p>Create a node topic</p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>
</body>
</html>