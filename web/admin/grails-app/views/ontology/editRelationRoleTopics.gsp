<!doctype html>
<html>
<head>
<meta name="layout" content="main"/>
<title>NDLA Topics Admin Application - Ontologies</title>
<r:script>
    $(document).ready(function() {

        searchDomain = $("#searchDomain").val()
        pathPrefix = $("#pathPrefix").val()
        getTable();


        $("#updateTopicListButton").click(function(){
            getTable();
        })

        $("#roleType").change(function(){
            var roleType = $("#roleType").val()
            roleType = roleType.substring(roleType.indexOf("#")+1)
            //var topics = $('#roleData').val(JSON.stringify(topics))
            var topics = jQuery.parseJSON($('#roleData').val());
            var newTopics = []

            for(i = 0; i < topics.length;i++){
                if(roleType == topics[i].type){
                    newTopics.push(topics[i])
                }
            }

            console.log("newTopics: "+newTopics)

            if(roleType != "all"){
                var dataHtml = renderDataTable(newTopics,0,roleType)
            }
            else{
                var dataHtml = renderDataTable(topics,0,roleType)
            }

            $('#data-table-relationRoles').html(dataHtml);
        });

        $('#topicTitleButton').click('click', searchTopics);

        $(document).keypress(function(e) {
            if(e.which == 13) {
                e.preventDefault();
            }
        });

    });

    function getTable(){
        $('#data-table-relationRoles').html('<div class="cogspinner" />')
        var host = window.location.protocol + '//' + window.location.host;
        var serviceUrl = host + pathPrefix+'/service/getRelationRoleTypes'
        var limit = 10;
        var start = 0;
        $.ajax({
            async: false,
            url: serviceUrl,
            dataType: 'json'
        }).done(function (data) {
            var topics = processJson(data)
            $('#roleData').val(JSON.stringify(topics))
            var dataHtml = renderDataTable(topics, start,'all');
            $('#data-table-relationRoles').html(dataHtml);
            counter = data.length;
        });
    }

    function searchTopics(){
        var search = $("#topicTitleSearch").val();
        var topics = jQuery.parseJSON($('#roleData').val());
        var newTopics = []
        for(i = 0; i < topics.length;i++){
            console.log(topics[i])
            if(topics[i].title.indexOf(search) != -1 || topics[i].identifier.indexOf(search) != -1){
                newTopics.push(topics[i])
            }
        }

        var dataHtml = renderDataTable(newTopics,0,roleType)
        $('#data-table-relationRoles').html(dataHtml);

    }

    function fetchdata(tokens) {
        $('#data-table-relationRoles').html('<div class="cogspinner" />')
        var urlTokens;
        var subjectId = $("#subjectId").val()
        var subjectName = $("#subjectName").val()
        var site = $("#site").val()

        if (tokens.length == 0) {
            urlTokens = getTokens();
        }
        else {
            urlTokens = tokens;
            subjectId = urlTokens['subjectId'];
            subjectName = urlTokens['subjectName'];
            site = urlTokens['site'];
        }


        var limit = 10;
        var start = 0;
        var approvedState = ""
        if (typeof urlTokens['start'] != 'undefined') {
            start = urlTokens['start'];
        }

        if (typeof urlTokens['approved'] != 'undefined') {
            approvedState = urlTokens['approved'];
        }
        else {
            approvedState = $("#approvedSelect").val()
        }

        langMap = [];
        var searchLanguage = ""
        $("input[name=languageSelect]").each(function () {
            if ($(this).is(':checked') && $(this).val() != "") {
                searchLanguage += "language[]=" + $(this).val() + "&";
                langMap.push($(this).val())
            }
        });


        var host = window.location.protocol + '//' + window.location.host;
        var serviceUrl = host + pathPrefix+'/service/getRelationRoleTypes'

        $.ajax({
            async: false,
            url: serviceUrl,
            dataType: 'json'
        }).done(function (data) {
            var topics = processJson(data)
            var dataHtml = renderDataTable(topics, start, 'all');
            $('#data-table-relationRoles').html(dataHtml);
            counter = data.length;
        });
    }

    function renderDataTable(json, start, type) {
        var dataSize = json.length;
        if (dataSize == 0) {
            return '<span class="label label-warning">No Data!</span>';
        }
        var buffer = ['<table id="data-table" class="table table-bordered table-hover"><thead><tr><th>#</th><th>Name</th><th>RelationType</th><th>Type</th><th>Used in relations</th></tr></thead><tbody>'];
        var counter;
        var listedSize = 0;
        for (var i = 0; i < dataSize; i++) {
            counter = i + 1 + parseInt(start);
            var row = ""
            if(i < 20){
                if(type = 'all'){
                    row = '<tr><td>' + counter.toString() + '</td><td>'+json[i].title+'(' + json[i].identifier + ')</td><td>' + json[i].relationType + '</td><td>' + json[i].type + '</td><td>' + json[i].relationCount + '</td><td> &nbsp;&nbsp;<a href="editRelationRoleTopic?topicId='+json[i].identifier+'&roleName='+json[i].title+'&assocType='+json[i].type+'&relationCount='+json[i].relationCount+'"><input type="button" value="Edit" class="btn btn-warning"/> </a></td></tr>';
                }
                else if(type == json[i].type){
                    row = '<tr><td>' + counter.toString() + '</td><td>'+json[i].title+'(' + json[i].identifier + ')</td><td>' + json[i].relationType + '</td><td>' + json[i].type + '</td><td>' + json[i].relationCount + '</td><td> &nbsp;&nbsp;<a href="editRelationRoleTopic?topicId='+json[i].identifier+'&roleName='+json[i].title+'&assocType='+json[i].type+'&relationCount='+json[i].relationCount+'"><input type="button" value="Edit" class="btn btn-warning"/> </a></td></tr>';
                }

            }


            listedSize ++;
            buffer.push(row + "\n");
        }
        buffer.push("</tbody></table>");
        if (start > 0) {
            if (listedSize >= 20) {
                buffer.push('<span id="pager-back"><a href="editRelationRoleTopics?start=' + (parseInt(start) - 10) +'">&lt;&lt; Previous 10 </a></span>');
                //buffer.push('<span id="pager-middle">'+start+'.....'+(parseInt(start)+10)+'</span>');
                buffer.push('<span id="pager-forward"><a href="editRelationRoleTopics?start=' + (parseInt(start) + 10) +'">Next 10 &gt;&gt;</a></span>');
            }
            buffer.push('<span id="pager-back"><a href="editRelationRoleTopics?start=' + (parseInt(start) - 10) + '">&lt;&lt; Previous 10 </a></span>');
        }
        else {
            if (listedSize >= 20) {
                buffer.push('<span id="pager-forward"><a href="editRelationRoleTopics?start=' + (parseInt(start) + 10) + '">Next 10 &gt;&gt;</a></span>');
            }

        }

        return buffer.join('');
    }


    function processJson(json, langMap) {
        var result = new Array();

        for (var i = 0; i < json.length; ++i) {
            dataObject = {}

            dataObject['identifier'] = json[i].identifier
            dataObject['title'] = json[i].name
            dataObject['relationType'] = json[i].typeName
            dataObject['relationCount'] = json[i].relationCount
            dataObject['type'] = json[i].type

            result.push(dataObject);
        }

        return result;
    }


    function getTokens() {
        var tokens = [];
        var query = location.search;
        query = query.slice(1);
        query = query.split('&');
        $.each(query, function (i, value) {
            var token = value.split('=');
            var key = decodeURIComponent(token[0]);
            var data = decodeURIComponent(token[1]);
            tokens[key] = data;
        });
        return tokens;
    }

    function getNodeCount(identifier,site) {
        var host = window.location.protocol + '//' + window.location.host;
        var serviceUrl = host + pathPrefix+'/service/getNodesByTopicId'

        var counter = 0;
        $.ajax({
            async: false,
            url: serviceUrl,
            data: {
                'topicId': identifier,
                'site': site
            },
            dataType: 'json',
        }).done(function (data) {
            counter = data.length;

        });

        return counter;
    }

    function getSubjectCount(identifier, site) {
        var host = window.location.protocol + '//' + window.location.host;
        var serviceUrl = host + pathPrefix+'/service/getSubjectsByTopicId'

        var counter = 0;
        $.ajax({
            async: false,
            url: serviceUrl,
            data: {
                'topicId': identifier,
                'site': site
            },
            dataType: 'json',
        }).done(function (data) {
            counter = data.length;

        });
        return counter;
    }
</r:script>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:7]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab6-1" data-toggle="tab">Update</a></li>
                    <li><g:link controller="ontology" action="addRelationRole">Add</g:link></li>
                    <!--li class="active"><a href="#tab6-1" data-toggle="tab">Remove</a></li-->
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab6-1">
                        <g:form name="editRelationRoleTopicForm" useToken="true" action="doRemoveSubjectTopics">
                            <g:hiddenField name="searchDomain" id="searchDomain" value="${searchDomain}" />
                            <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />
                            <g:hiddenField name="roleData" id="roleData" value="" />


                            <fieldset>
                                <legend>Edit relation role topics</legend>
                            </fieldset>
                            <br />
                            <br />
                            <label>Search for topic</label>
                            <g:select id="roleType" name='roleType'
                                      noSelection="${['all':'Alle']}"
                                      from='${roleTypes}'
                                      optionKey="key" optionValue="value"></g:select>
                            <div class="input-append">
                                <input class="span10" name="topicTitleSearch" id="topicTitleSearch" type="text" autocomplete="off">
                                <button class="btn" id="topicTitleButton" type="button">Go!</button>
                            </div>
                            <br />
                            <br />
                            <div class="input-append-editRelationRoleTopics">
                                <button class="btn" id="updateTopicListButton" type="button">Update the list!</button>
                            </div>
                            <br />
                            <div id="data-table-relationRoles">

                            </div>
                        </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>