<%--
  Created by IntelliJ IDEA.
  User: rolfguescini
  Date: 15.08.14
  Time: 16:04
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
        <title>NDLA Topics Admin Application - Ontologies</title>
        <style>
        #myModal {
            width: 600px;
            height: 600px;
            margin: 0 0 0 -400px;
            overflow: auto;
        }
        </style>
        <r:script>
            $(document).ready(function() {
                searchDomain = $("#searchDomain").val()

                $("#toggle-button").click(function (event) {
                    $("#more-options").slideToggle();
                });

                pathPrefix = $("#pathPrefix").val()

                $('#myModal').on('show', function () {
                    $(this).find('.modal-body').css({width:'auto', height:'auto', 'max-height':'100%'});
                });

                $('.language-button').click(function() {
                    var currentId = $(this).attr("id")
                    currentId = currentId.substring(currentId.indexOf("_")+1);

                    var selectedLanguageId = $("#missingLanguages_"+currentId+" option:selected").val()
                    var selectedLanguageText = $("#missingLanguages_"+currentId+" option:selected").text()

                    var currentCounter = $("#currentCounter").val()
                    var html = '<span id="nameSpan_'+selectedLanguageId+'_'+currentId+'"><label>'+selectedLanguageText+' title</label><input type="text" value="" autocomplete="off" placeholder="type a title" id="'+selectedLanguageId+'_'+currentId+'" name="'+selectedLanguageId+'_'+currentId+'">'

                    $("#newNameWrapper_"+currentId).append(html);
                    $("#missingLanguages_"+currentId+" option:selected").remove();
                });

                $('.role-button').click(function() {
                    var currentId = $(this).attr("id")
                    currentId = currentId.substring(currentId.indexOf("_")+1);
                    if($("#roleSpan_"+currentId).css("display") == "block"){
                        $("#roleSpan_"+currentId).css("display","none")
                    }
                    else{
                        $("#roleSpan_"+currentId).css("display","block")
                    }

                });


                $("#ontologyRoleTopics1").on("change", function(){
                    var selectedRoleTopic = $("#ontologyRoleTopics1 option:selected").val();
                    var selectedLanguageText = $("#ontologyRoleTopics1 option:selected").text();
                    var parentId = $(this).parent().parent().attr("id");
                    var roleCounter = parentId.substring(parentId.indexOf("_")+1);
                    console.log("PARENTID: "+roleCounter);
                    $(".roleHidden_"+roleCounter).children().each(function(){
                        console.log("ROLE: "+$(this).attr("id"))
                        if($(this).attr("id").contains("Name")){
                            $(this).val(selectedLanguageText)

                        }
                        else if($(this).attr("id").contains("Id")){
                            $(this).val(selectedRoleTopic)
                        }

                    });
                    $("#roleName1_Wrapper > span").html("<em>"+selectedLanguageText+"</em>");
                    $("#selectedRoleTopicId1").val(selectedRoleTopic)
                    $("#selectedRoleTopicName1").val(selectedLanguageText)
                    $('#createRoleTopics1Wrapper').css("display","none")
                    $('#searchRoleTopics1Wrapper').css("display","none")

                });

                $("#ontologyRoleTopics2").on("change", function(){
                    var selectedRoleTopic = $("#ontologyRoleTopics2 option:selected").val()
                    var selectedLanguageText = $("#ontologyRoleTopics2 option:selected").text()
                    var parentId = $(this).parent().parent().attr("id");
                    var roleCounter = parentId.substring(parentId.indexOf("_")+1);
                    console.log("PARENTID: "+roleCounter);
                    $(".roleHidden_"+roleCounter).children().each(function(){
                        console.log("ROLE: "+$(this).attr("id"))
                        if($(this).attr("id").contains("Name")){
                            $(this).val(selectedLanguageText)

                        }
                        else if($(this).attr("id").contains("Id")){
                            $(this).val(selectedRoleTopic)
                        }

                    });


                    $("#roleName2_Wrapper > span").html("<em>"+selectedLanguageText+"</em>");
                    $("#selectedRoleTopicId2").val(selectedRoleTopic)
                    $("#selectedRoleTopicName2").val(selectedLanguageText)
                    $('#createRoleTopics2Wrapper').css("display","none")
                    $('#searchRoleTopics2Wrapper').css("display","none")

                });

                $("#addNewRoleTopic1").click(function(){
                    openTopicCreator(pathPrefix,"1");
                });

                $("#addNewRoleTopic2").click(function(){
                    openTopicCreator(pathPrefix,"2");
                });

                
                var selectedLanguageId = $("#relationType option:selected").val()


                $('#topicSearch1').typeahead ({
                    source: function (query, process) {
                        topics = [];
                        map = {};
                        langMap = [];
                        langNames = {};
                        usedLangs = {}

                        var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                        var searchLanguage = ""
                        var searchType = $('#typeSelect').val();
                        $( "input[name=languageSelect]" ).each(function(){
                            if($(this).is(':checked') && $(this).val() != ""){
                                searchLanguage += "language[]="+$(this).val()+"&";
                                langMap.push($(this).val())
                            }
                            langNames[$(this).val()] = $(this).attr("text");
                        });

                        searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
                        serviceUrl += "?" + searchLanguage + "&type=" + searchType+ "&index=topics";

                        $.ajax({
                            url: serviceUrl,
                            type: 'GET',
                            dataType: 'json',
                            success: function (response) {
                                //console.log("RESPONSE: "+response)
                                var data = processJson(response, langMap);

                                $.each(data, function (i, topic) {
                                    var key = topic.title;
                                    var keyId = topic.identifier;
                                    if (!(key in map) && keyId.indexOf("topic") > -1) {
                                        map[key] = topic;
                                        topics.push(key);
                                    }
                                });
                                process(topics);
                            }
                        }, 300);
                    },
                    matcher: function (item) {
                        return true;
                    },
                    updater: function (item) {
                        var topicObject = map[item];

                        $(".roleHidden_1").children().each(function(){
                            console.log("ROLE: "+$(this).attr("id"))
                            if($(this).attr("id").contains("Name")){
                                $(this).val(topicObject.title)

                            }
                            else if($(this).attr("id").contains("Id")){
                                $(this).val(topicObject.identifier)
                            }

                        });
                        $("#roleName1_Wrapper > span").html("<em>"+topicObject.title+"</em>");
                        $("#selectedRoleTopicId1").val(topicObject.identifier)
                        $("#selectedRoleTopicName1").val(topicObject.title)
                        $('#createRoleTopics1Wrapper').css("display","none")
                        $('#searchRoleTopics1Wrapper').css("display","none")
                        $("#roleSpan_1").css("display","none")

                        return item;
                        $('#topicSearch1').typeahead('close');
                        $('#topicSearch1').val("");

                    }
                });


                $('#topicSearch2').typeahead ({
                    source: function (query, process) {
                        topics = [];
                        map = {};
                        langMap = [];
                        langNames = {};
                        usedLangs = {}

                        var serviceUrl = searchDomain+"/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
                        var searchLanguage = ""
                        var searchType = $('#typeSelect').val();
                        $( "input[name=languageSelect]" ).each(function(){
                            if($(this).is(':checked') && $(this).val() != ""){
                                searchLanguage += "language[]="+$(this).val()+"&";
                                langMap.push($(this).val())
                            }
                            langNames[$(this).val()] = $(this).attr("text");
                        });

                        searchLanguage = searchLanguage.substr(0,searchLanguage.length -1)
                        serviceUrl += "?" + searchLanguage + "&type=" + searchType+ "&index=topics";

                        $.ajax({
                            url: serviceUrl,
                            type: 'GET',
                            dataType: 'json',
                            success: function (response) {
                                //console.log("RESPONSE: "+response)
                                var data = processJson(response, langMap);

                                $.each(data, function (i, topic) {
                                    var key = topic.title;
                                    var keyId = topic.identifier;
                                    if (!(key in map) && keyId.indexOf("topic") > -1) {
                                        map[key] = topic;
                                        topics.push(key);
                                    }
                                });
                                process(topics);
                            }
                        }, 300);
                    },
                    matcher: function (item) {
                        return true;
                    },
                    updater: function (item) {
                        var topicObject = map[item];
                        console.log(topicObject)
                        $(".roleHidden_2").children().each(function(){
                            console.log("ROLE: "+$(this).attr("id"))
                            if($(this).attr("id").contains("Name")){
                                $(this).val(topicObject.title)

                            }
                            else if($(this).attr("id").contains("Id")){
                                $(this).val(topicObject.identifier)
                            }

                        });
                        $("#roleName2_Wrapper > span").html("<em>"+topicObject.title+"</em>");
                        $("#selectedRoleTopicId2").val(topicObject.identifier)
                        $("#selectedRoleTopicName2").val(topicObject.title)
                        $('#createRoleTopics2Wrapper').css("display","none")
                        $('#searchRoleTopics2Wrapper').css("display","none")
                        $("#roleSpan_2").css("display","none")
                        return item;
                        $('#topicSearch2').typeahead('close');
                        $('#topicSearch2').val("");

                    }
                });

                $('body').on('click', "input[name=nameRemove]" ,function(){
                    if($(this).is(':checked')) {
                        var roleId1 = $("#roleId1").val()
                        var roleId2 = $("#roleId2").val()
                        $("#selectedRoleTopicId1").val(roleId1)
                        $("#selectedRoleTopicId2").val(roleId2)
                        var roleName1 = $("#roleName1").val()
                        var roleName2 = $("#roleName2").val()
                        console.log("ROLENAME1: "+roleName1+" ROLENAME2: "+roleName2);
                        if(roleName1 != "" && roleName2 != "") {
                            $("#selectedRoleTopicName1").val(roleName1)
                            $("#selectedRoleTopicName2").val(roleName2)
                        }

                        console.log("selectedRoleTopicName1: "+$("#selectedRoleTopicName1").val()+" selectedRoleTopicName2: "+$("#selectedRoleTopicName2").val());
                        $("#nameSpan_"+$(this).val()).parent().parent().remove()
                    }
                });



            });




            function processJson(json,langMap) {
                var result = new Array();
                for(var i = 0; i < json.length; ++i) {
                    dataObject = {}

                    var neutral_title = json[i].title_language_neutral
                    jQuery.each(json[i], function(key, val) {
                        if(key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                            var split_arr = key.split("_");
                            if(langMap.indexOf(split_arr[1]) > -1){
                                dataObject[key] = val;
                            }
                        }
                    });

                    neutral_title = typeof (neutral_title) ==  'undefined' ? json[i].title_nob : json[i].title_language_neutral;

                    dataObject['identifier'] = json[i].id
                    dataObject['title'] = neutral_title

                    result.push(dataObject);
                }
                return result;
            }

            function openTopicCreator(pathPrefix,id) {

                var labelContent = "Create a role topic";
                $("#myModalLabel").html(labelContent);
                var loadingContent = "<h2>Loading...</h2>";
                var html = '<div id="newRoleTopic"><div id="newRoleTopicTitle" class="well-small">';
                html += '<p class="modal-header"><strong>Topic titles</strong></p>';
                html += '<label>Required English Title</label><input type="text" placeholder="Type an English title..." name="newTopicTitleEng" id="newTopicTitleEng">';
                html += '<label>Required Norwegian Bokmål Title</label><input type="text" placeholder="Type an English title..." name="newTopicTitleNob" id="newTopicTitleNob"></div>';
                html += '<div id="newTopicDescriptions" class="well-small">';
                html += '<label>English description</label><textarea id="description_eng" cols="50" rows="5" name="description_eng"></textarea>';
                html += '<label>Norsk Bokmål description</label><textarea id="description_nob" cols="50" rows="5" name="description_nob"></textarea></div>';
                html += '<button type="button" id="addRoleTopicButton" class="btn-mini" onclick="saveRoleTopic('+id+')">Add topic</button></div>';
                $(".modal-body").html(html);
            }

            function saveRoleTopic(id){
                var topicObject = {}
                topicObject['names'] = new Array();
                topicObject['descriptions'] = new Array();
                topicObject['images'] = new Array();
                topicObject['types'] = new Array();

                $(".modal-body input").each(function(){
                    var nameObject = {}
                    nameENG = ""
                    nameNOB = ""

                    if($(this).attr("id") == "newTopicTitleEng"){
                        nameObject["name"] = $(this).val();
                        nameENG = $(this).val();
                        nameObject["language"] = "http://psi.oasis-open.org/iso/639/#eng";
                    }
                    else if($(this).attr("id") == "newTopicTitleNob"){
                        nameObject["name"] = $(this).val();
                        nameNOB = $(this).val();
                        nameObject["language"] = "http://psi.oasis-open.org/iso/639/#nob";
                    }
                    topicObject["names"].push(nameObject);
                });

                $(".modal-body textarea").each(function(){
                    var descriptionObject = {}
                    if($(this).attr("id") == "description_eng"){
                        descriptionObject["content"] = $(this).val();
                        descriptionObject["language"] = "http://psi.oasis-open.org/iso/639/#eng";
                    }
                    else if($(this).attr("id") == "description_nob"){
                        descriptionObject["content"] = $(this).val();
                        descriptionObject["language"] = "http://psi.oasis-open.org/iso/639/#nob";
                    }
                    topicObject['descriptions'].push(descriptionObject);
                });


                var host = window.location.protocol + '//' + window.location.host;
                var serviceUrl = host + pathPrefix+'/service/createTopic'
                $.ajax({
                    async: false,
                    url: serviceUrl,
                    data: {
                        'payload': JSON.stringify(topicObject)
                    },
                    dataType: 'json',
                }).done(function (data) {
                    for(var i = 0; i < data.length; i++) {
                        var key = data[i];
                        $("#selectedRoleTopicId"+id).val(key)
                        console.log(key+ " NAME: "+nameNOB)
                        $("#selectedRoleTopicName"+id).val(nameNOB)
                    }
                    console.log("GOT CREATETOPIC DATA: "+data)
                    $("#selectedRoleTopicName"+id).css("display","block")
                    $('#searchRoleTopics1Wrapper').css("display","none")
                    $('#searchRoleTopics2Wrapper').css("display","none")
                    $('#ontologyRoleTopics2Wrapper').css("display","none")
                    $('#ontologyRoleTopics1Wrapper').css("display","none")
                    $('#createRoleTopics'+id+'Wrapper').css("display","none")
                    $('#myModal').modal('hide')
                });
            }


            function myTrim(x) {
                return x.replace(/^\s+|\s+$/gm,'');
            }
        </r:script>
</head>

<body>
<div class="navbar">
<g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
<g:render template="submenu" model="[param:3]" />
<div class="tab-content">
    <div class="tab-pane active" id="tab1">
        <div class="tabbable tabs-right">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab6-1" data-toggle="tab">Update</a></li>
                <li class="active"><li><g:link controller="ontology" action="createRelationshipType">Add</g:link></li>
                <li><g:link controller="ontology" action="removeRelationShipTypes">Remove</g:link></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab6-1">
                    <g:form name="editRelationTypeForm" useToken="true" action="doUpdateRelationType">
                        <g:hiddenField name="searchDomain" id="searchDomain" value="${searchDomain}" />
                        <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />
                        <g:hiddenField name="relationShipTypeId" id="relationShipTypeId" value="${typeId}" />

                        <div id="relationshipTypeData" class="hidden">
                            <p>${relationshipTypeType}</p>
                            <g:select id="relationType" from="${relationTypeTypes.entrySet()}" name="relationType" optionKey="key" optionValue="value" value="${relationshipTypeType}"/>
                            <br />

                            <g:set var="previousRoleKey" value="" />

                            <g:each in="${typeNames}" var="typeName">
                                <div class="updateRelationWrapper">
                                <g:each in="${typeName.value}" var="nameData">
                                    <g:if test="${nameData.key == "isoNames"}">
                                        <div class="nameWrapper">
                                        <g:each in="${nameData.value}" var="isoName">
                                            <g:set var="iso" value="${isoName.key}_${typeName.key}" />
                                            <div>
                                                <span id="nameSpan_${iso}"><label>${langMap.get(isoName.key)} title</label><input type="text" name="${iso}" id="${iso}" placeholder="type a ${nameData.value} title" autocomplete="off" value="${isoName.value}"></span>
                                                <span>Remove <g:checkBox name="nameRemove" value="${iso}" checked="false" /></span></span>
                                            </div>
                                        </g:each>
                                        </div>
                                        <div class="addNameWrapper">
                                            <div id="newNameWrapper_${typeName.key}">
                                            </div>
                                            <g:select id="missingLanguages_${typeName.key}" from="${missinglangs.entrySet()}" name="missingLanguages_${typeName.key}" optionKey="key" optionValue="value" noSelection="['':'Add a new language']"/>
                                            <button class="btn-mini language-button" id="addLanguageButton_${typeName.key}" type="button">Go!</button>
                                        </div>
                                    </g:if>
                                    <g:elseif test="${nameData.key == "roleNames"}">
                                        <div class="roleWrapper">
                                            <g:each in="${nameData.value}" var="roleName">
                                                <g:set var="roleKey" value="${roleMapper.get(roleName.key)}" />
                                                <div id="roleName${roleKey}_Wrapper">
                                                    Role: <span><em>${roleName.value}</em></span>
                                                </div>
                                                <!--g:if test="${roleKey != previousRoleKey}"-->
                                                    <span><button class="btn-mini role-button" id="roleChangeButton_${roleKey}" type="button">Change role!</button></span>
                                                    <div id="roleSpan_${roleKey}" class="updateRoleSpan">
                                                        <div id="ontologyRoleTopics${roleKey}Wrapper"><label>Select role played</label><g:select id="ontologyRoleTopics${roleKey}" from="${ontologyRoleTopics.entrySet()}" name="ontologyRoleTopics${roleKey}" optionKey="key" optionValue="value" noSelection="['':'Choose a role']"/></div>
                                                        <div id="searchRoleTopics${roleKey}Wrapper"><label>Search for an existing role topic played for player ${roleKey}</label>
                                                            <input class="span10" name="topicSearch${roleKey}" id="topicSearch${roleKey}" type="text" autocomplete="off"></div>
                                                        <br /><br />
                                                        <div id="createRoleTopics${roleKey}Wrapper"><a href="#" data-target="#myModal" role="button"data-toggle="modal"><g:img dir="images" file="add.png" id="addNewRoleTopic_${roleKey}" /></a> Create a new role topic</div>
                                                        <input class="span10 hide" name="selectedRoleTopicName${roleKey}" id="selectedRoleTopicName${roleKey}" type="text" autocomplete="off" value="">
                                                    </div>
                                                    <br />
                                                <!--/g:if-->
                                                <g:set var="previousRoleKey" value="${roleKey}" />
                                                <span class="roleHidden_${roleKey}">
                                                    <g:hiddenField name="roleId${roleKey}_${typeName.key}" id="roleId${roleKey}_${typeName.key}" value="${roleName.key}" />
                                                    <g:hiddenField name="roleName${roleKey}_${typeName.key}" id="roleName${roleKey}_${typeName.key}" value="${roleName.value}" />
                                                </span>
                                            </g:each>
                                        </div>
                                    </g:elseif>
                                </g:each>



                                    </div>
                                </g:each>

                            <g:hiddenField name="selectedRoleTopicId1" id="selectedRoleTopicId1" value="" />
                            <g:hiddenField name="selectedRoleTopicId2" id="selectedRoleTopicId2" value="" />
                            <g:hiddenField name="selectedRoleTopicName1" id="selectedRoleTopicName1" value="" />
                            <g:hiddenField name="selectedRoleTopicName2" id="selectedRoleTopicName2" value="" />


                            <br />
                            <br />
                            <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                        </div>

                    </g:form>
                </div>
            </div>
         </div>
    </div>
</div>
    <div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="myModalLabel">Create a role topic</h3>
        </div>
        <div class="modal-body">
            <p>Create a node topic</p>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
        </div>
    </div>
</body>
</html>