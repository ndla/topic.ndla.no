<%--
  Created by IntelliJ IDEA.
  User: rolfguescini
  Date: 15.08.14
  Time: 16:04
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
        <title>NDLA Topics Admin Application - Ontologies</title>
        <r:script>
            $(document).ready(function () {
                $( "input[name=types]").click(function(){
                    var type = $( "input:radio[name=types]:checked").val();
                    $("#"+type+"Wrapper").css("display", "block");
                    if(type == "horizontal" && $("#hierarchicalWrapper").css("display") == "block"){
                        $("#hierarchicalWrapper").css("display", "none");
                    }
                    else if(type == "hierarchical" && $("#horizontalWrapper").css("display") == "block"){
                        $("#horizontalWrapper").css("display", "none");
                    }

                });
            });
        </r:script>
</head>

<body>
<div class="navbar">
<g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
<g:render template="submenu" model="[param:2]" />
<div class="tab-content">
    <div class="tab-pane active" id="tab1">
        <div class="tabbable tabs-right">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab6-1" data-toggle="tab">Update</a></li>
                <li><g:link controller="ontology" action="createRelationshipType">Add</g:link></li>
                <li><g:link controller="ontology" action="removeRelationShipTypes">Remove</g:link></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab6-2">
                    <g:form name="editRelationTypeForm" useToken="true" action="editRelationShipType">
                        <label>Select the type of relationship type you are editing</label>
                        <label class="checkbox inline">
                            <input type="radio" name="types" value="hierarchical"> Hierarchical relationship types
                        </label>
                        <label class="checkbox inline">
                            <input type="radio" name="types"  value="horizontal"> Horizontal relationship types
                        </label>

                        <br /><br />


                        <div id="horizontalWrapper"><label>Horizontal relationship types</label><g:select id="horizontalTypeMap" from="${horizontalTypeMap.entrySet()}" name="horizontalTypeMap" optionKey="key" optionValue="value" noSelection="['':'Choose the type to edit']"/></div>
                        <div id="hierarchicalWrapper"><label>Hierarchical relationship types</label><g:select id="hierarchicalTypeMap" from="${hierarchicalTypeMap.entrySet()}" name="hierarchicalTypeMap" optionKey="key" optionValue="value" noSelection="['':'Choose the type to edit']"/></div>
                        <g:submitButton name="submit" class="btn btn-success" value="Submit" />
                    </g:form>
                </div>
            </div>
         </div>
    </div>
</div>
</body>
</html>