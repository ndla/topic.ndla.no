<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
    <r:script>
        $(document).ready(function() {

            $('body').on('click', "input[name=nameRemove]" ,function(){
                if($(this).is(':checked')) {
                    console.log($(this).val())
                    $("#wordSpan_"+$(this).val()).remove();
                }
            });

            $('body').on('click', "input[name=subjectSites]" ,function(){
                if(!$(this).is(':checked')) {
                    var site = $(this).val();
                    spanid = ""
                    if(site == "http://ndla.no/"){
                        var spanid = $(this).val().substring(7, $(this).val().indexOf(".no"));
                    }
                    else{
                        var spanid = $(this).val().substring(7, $(this).val().indexOf(".ndla.no"));
                    }

                    $("#siteSpan_"+spanid).remove();
                }
            });

            $('body').on('click', "input[name=subjectPsis]" ,function(){
                var psilength = $("#psiFieldSet").children().filter("span").length;
                if(!$(this).is(':checked')) {
                    var psi = $(this).val();
                    subjectId = psi.substring(psi.indexOf("#")+1)
                    if(psilength > 1){
                        $("#psiSpan_"+subjectId).remove();
                    }
                    else{
                        alert("A subject matter has to have at least one PSI")
                    }

                }
            });

            $('#addLanguageButton').click(function() {
                var selectedLanguageId = $("#missingSubjectNameLanguages option:selected").val()
                var selectedLanguageText = $("#missingSubjectNameLanguages option:selected").text()
                $("#subjectNames").append('<label>'+selectedLanguageText+' title</label><input type="text" name="'+selectedLanguageId+'" id="'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+' title" autocomplete="off" value="">');
                $("#missingSubjectNameLanguages option:selected").remove();

            });

            $('#addSubjectSiteButton').click(function() {
                var amount = $("#siteFieldSet").children().length
                console.log(amount)
                $("#siteFieldSet").append('<label>Site URL <input type="text" name="addedSiteurl'+amount+'" id="addedSiteurl'+amount+'" placeholder="http://" title" autocomplete="off" value="http://"></label>');
            });

            $('#addSubjectPsiButton').click(function() {
                var amount = $("#psiFieldSet").children().length
                console.log(amount)
                $("#psiFieldSet").append('<label>Subject PSI <input type="text" name="addedSubjectPsi'+amount+'" id="addedSubjectPsi'+amount+'" placeholder="http://" title" autocomplete="off" value="http://psi.topic.ndla.no/#"></label>');
            });



        });

    </r:script>
</head>
<body>
    <div class="navbar">
        <g:render template="/actionmenu"model="[param:6]" />
    </div>
    <div class="tabbable">
        <g:render template="submenu" model="[param:1]" />
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div class="tabbable tabs-right">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab6-1" data-toggle="tab">Update</a></li>
                        <li><g:link controller="ontology" action="addSubject" params="['data-toggle': 'tab']">Add</g:link></li>
                        <li><g:link controller="ontology" action="removeSubject" params="['data-toggle': 'tab', subjectId : subject.subjectId, sites : sites]">Remove</g:link></li>
                        <!--li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'true']">Remove</g:link></li-->
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab6-1">
                             <g:form name="updateSubjectForm" useToken="true" action="doUpdateSubject">
                                 <g:hiddenField name="subjectId" value="${subject.subjectId}" />
                                 <div id="subjectFieldSet">
                                     <g:link  controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']">&lt;&lt; Back</g:link>
                                    <fieldset>
                                        <legend>Update <em> ${subject.subjectNames.get("http://psi.oasis-open.org/iso/639/#eng")}</em></legend>
                                        <label>Subject UDIRPSI</label><input type="text" name="UDIRPsi" id="UDIRPsi" placeholder="" autocomplete="off" value="${subject.UDIRPsi}">

                                        <div id="updateSubjectNames">
                                            <div id="subjectNames">
                                                <g:each in="${subject.subjectNames}" var="name">
                                                    <g:if test="${name.key.toString() != 'http://psi.topic.ndla.no/#language-neutral'}">
                                                        <span id="wordSpan_${name.key.toString().substring(name.key.toString().indexOf("#")+1)}"><label>${langMap.get(name.key)} title</label><input type="text" name="${name.key}" id="${name.key}" placeholder="type a ${name.value} title" autocomplete="off" value="${name.value}">  <span>Remove <g:checkBox name="nameRemove" value="${name.key.toString().substring(name.key.toString().indexOf("#")+1)}" checked="false" /></span></span>
                                                    </g:if>
                                                </g:each>
                                            </div>
                                            <g:select id="missingSubjectNameLanguages" from="${missinglangs.entrySet()}" name="missingSubjectNameLanguages" optionKey="key" optionValue="value" noSelection="['':'Add a language']"/> <button class="btn" id="addLanguageButton" type="button">Go!</button>
                                        </div>
                                        <br />
                                        <br />

                                        <fieldset>
                                            <legend>Sites</legend>
                                            <div id="siteFieldSet">
                                                <g:each in="${subject.originatingSites}" var="site">
                                                    <g:if test="${site == 'http://ndla.no/'}">
                                                        <span id="siteSpan_${site.substring(7,site.indexOf(".no"))}"><label>${site} - Delete <g:checkBox name="subjectSites" id="subjectSites" value="${site}" /></label></span>
                                                    </g:if>
                                                    <g:else>
                                                        <span id="siteSpan_${site.substring(7,site.indexOf(".ndla.no"))}"><label>${site} - Delete <g:checkBox name="subjectSites" id="subjectSites" value="${site}" /></label></span>
                                                    </g:else>
                                                </g:each>
                                            </div>
                                            <br /> Example: http://ndla.no, http://fyr.ndla.no
                                            <div id="add-button"><label>Add a site: <g:img dir="images" file="add.png" id="addSubjectSiteButton" /></label></div>
                                        </fieldset>

                                        <fieldset>
                                            <legend>PSIs</legend>
                                            <div id="psiFieldSet">
                                                <g:each in="${subject.subjectPsis}" var="psi">
                                                    <span id="psiSpan_${psi.substring(psi.indexOf("#")+1)}"><label>${psi} - Delete <g:checkBox name="subjectPsis" id="subjectPsis" value="${psi}" /></label></span>
                                                </g:each>
                                            </div>
                                            <div id="add-button"><label>Add a psi: <g:img dir="images" file="add.png" id="addSubjectPsiButton" /></label></div>
                                        </fieldset>
                                        <br />
                                        <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                                    </fieldset>
                                 </div>
                             </g:form>

                        </div>
                    </div>
                </div>
             </div>
        </div>
    </div>
</body>
</html>