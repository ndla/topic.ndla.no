<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <r:script>
        $(document).ready(function() {

            $('#addLanguageButton').click(function() {
                var selectedLanguageId = $("#missingLanguages option:selected").val()
                selectedLanguageId = "name_"+selectedLanguageId.substring(selectedLanguageId.indexOf("#")+1)
                var selectedLanguageText = $("#missingLanguages option:selected").text()
                $("#topicNames").append('<label>'+selectedLanguageText+' title</label><input type="text" name="'+selectedLanguageId+'" id="'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+' title" autocomplete="off" value="">');
            });

            $('#updateTopicImageLanguageButton').click(function() {
                var selectedLanguageId = $("#updateTopicImageLanguages option:selected").val()
                selectedLanguageId = "image_"+selectedLanguageId.substring(selectedLanguageId.indexOf("#")+1)
                var selectedLanguageText = $("#updateTopicImageLanguages option:selected").text()
                $("#updateTopicImages").append('<label>'+selectedLanguageText+' Image URL</label><input type="text" name="image_'+selectedLanguageId+'" id="image_'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+' image URL" autocomplete="off" value="">');
            });


            $('#updateTopicDescriptionLanguageButton').click(function(){
                var selectedLanguageId = $("#updateTopicDescriptionLanguages option:selected").val()
                selectedLanguageId = "description_"+selectedLanguageId.substring(selectedLanguageId.indexOf("#")+1)
                var selectedLanguageText = $("#updateTopicDescriptionLanguages option:selected").text()
                $("#updateTopicDescriptions").append('<label>'+selectedLanguageText+' description</label><textarea id="description_'+selectedLanguageId+'" cols="50" rows="5" name="description_'+selectedLanguageId+'"></textarea>');
            });

            $('body').on('click', "input[name=nameRemove]" ,function(){
                if($(this).is(':checked')) {
                    console.log($(this).val())
                    $("#nameSpan_"+$(this).val()).remove();
                }
            });

            $('body').on('click', "input[name=descriptionRemove]" ,function(){
                if($(this).is(':checked')) {
                    console.log($(this).val())
                    $("#descriptionSpan_"+$(this).val()).remove();
                }
            });

            $('body').on('click', "input[name=imageRemove]" ,function(){
                if($(this).is(':checked')) {
                    console.log($(this).val())
                    $("#imageSpan_"+$(this).val()).remove();
                }
            });
        });
    </r:script>
</head>

<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:2]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab6-1" data-toggle="tab"><li>Update</a></li>
                    <li><g:link controller="ontology" action="addSubjectTopics" params="['data-toggle': 'tab', 'subjectId' : subjectId, 'sites' : site]">Add</g:link></li>
                    <li><g:link controller="ontology" action="removeSubjectTopics" params="['data-toggle': 'tab', 'subjectId' : subjectId, 'sites' : site]">Remove</g:link></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab6-1">
                        <g:form name="updateSubjectTopicForm" useToken="true" action="doUpdateSubjectTopic">
                            <g:hiddenField name="site" value="${site}" />
                            <g:hiddenField name="topicId" value="${topicId}" />
                            <g:hiddenField name="subjectId" value="${subjectId}" />

                            <g:each in="${languages}" var="lang">
                                <g:checkBox name="languageSelect" class="hiddenLanguageSelect" value="${lang.key}" /></label>
                            </g:each>

                            <div id="updateTopicNames">
                                <label>Names</label>
                                <div id="topicNames">
                                    <g:each in="${names}" var="topicName">
                                        <g:if test="${topicName.key != 'http://psi.topic.ndla.no/#language-neutral'}">
                                            <span id="nameSpan_${shortMap.get(topicName.key.toString())}"><label>${languages.get(topicName.key)} title</label><input type="text" name="name_${shortMap.get(topicName.key)}" id="name_${shortMap.get(topicName.key)}" placeholder="type a ${topicName.value} title" autocomplete="off" value="${topicName.value}">  <span>Remove <g:checkBox name="nameRemove" value="${shortMap.get(topicName.key.toString())}" checked="false" /></span></span>
                                        </g:if>
                                    </g:each>
                                </div>
                                <br />
                                <g:select name="topicTitleWordClass" id="topicTitleWordClass" from="${wordClassMap}" optionKey="key" optionValue="value" value="${wordClass}"  />
                            </div>

                            <br />
                            <g:select id="missingLanguages" from="${missinglangs.entrySet()}" name="missingLanguages" optionKey="key" optionValue="value" noSelection="['':'Add a language']"/> <button class="btn" id="addLanguageButton" type="button">Go!</button>


                            <div id="updateTopicDescriptions">
                                <label>Descriptions</label>
                                <div id="topicDescriptions">
                                    <g:each in="${descriptions}" var="topicDescription">
                                        <g:if test="${topicDescription.key != 'http://psi.topic.ndla.no/#language-neutral'}">
                                            <span id="descriptionSpan_${shortMap.get(topicDescription.key.toString())}"><label>${languages.get(topicDescription.key)} description</label><g:textArea name="description_${shortMap.get(topicDescription.key)}" id="description_${shortMap.get(topicDescription.key)}" placeholder="type a ${shortMap.get(topicDescription.value)} title" autocomplete="off" value="${topicDescription.value}" />  <span>Remove <g:checkBox name="descriptionRemove" value="${shortMap.get(topicDescription.key.toString())}" checked="false" /></span></span>
                                        </g:if>
                                    </g:each>
                                </div>
                            </div>
                            <br />
                            <select id="updateTopicDescriptionLanguages" name="updateTopicDescriptionLanguages">
                                <option value="">Add a language</option>
                                <g:each in="${languages}" var="langItem">
                                    <option value="${shortMap.get(langItem.key)}">${langItem.value}</option>
                                </g:each>
                            </select>
                            <button class="btn-mini" id="updateTopicDescriptionLanguageButton" type="button">Go!</button>

                            <div id="updateTopicImages">
                                <label>Images</label>
                                <div id="topicImages">
                                    <g:each in="${images}" var="topicImage">
                                        <g:if test="${topicImage.key != 'http://psi.topic.ndla.no/#language-neutral'}">
                                            <span id="imageSpan_${shortMap.get(topicImage.key.toString())}"><label>${languages.get(topicImage.key)} image URL</label><input type="text" name="image_${shortMap.get(topicImage.key)}" id="image_${shortMap.get(topicImage.key)}" placeholder="type a ${topicImage.value} image URL" autocomplete="off" value="${topicImage.value}">  <span>Remove <g:checkBox name="imageRemove" value="${shortMap.get(topicImage.key.toString())}" checked="false" /></span></span>
                                        </g:if>
                                    </g:each>
                                </div>
                            </div>
                            <br />
                            <select id="updateTopicImageLanguages" name="updateTopicImageLanguages">
                                <option value="">Add a language</option>
                                <g:each in="${languages}" var="langItem">
                                    <option value="${shortMap.get(langItem.key)}">${langItem.value}</option>
                                </g:each>
                            </select>
                            <button class="btn-mini" id="updateTopicImageLanguageButton" type="button">Go!</button>


                            <div id="updateTopicApproved">
                                <label>Approved?</label>
                                <g:hiddenField name="initialApprovalState" id="initialApprovalState" value="${approvedState}" />
                                <g:if test="${approvedState.equals('false') || null == approvedState}">
                                    <label class="radio">
                                        <g:radio name="approvedRadios" id="approvedRadios" value="false" checked="checked" />No
                                    </label>
                                    <label class="radio">
                                        <g:radio  name="approvedRadios" id="approvedRadios" value="true" />Yes
                                    </label>
                                </g:if>
                                <g:else>
                                    <label class="radio">
                                        <g:radio name="approvedRadios" id="approvedRadios" value="false" />No
                                    </label>
                                    <label class="radio">
                                        <g:radio  name="approvedRadios" id="approvedRadios" value="true" checked="checked" />Yes
                                    </label>
                                </g:else>
                            </div>
                            </fieldset>
                        </div>
                            <br />
                            <br />
                            <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                    </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>