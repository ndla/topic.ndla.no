<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
    <r:script>
        $(document).ready(function() {
            searchDomain = $("#searchDomain").val()

            $("#toggle-button").click(function (event) {
                $("#more-options").slideToggle();
            });

            /*
            var tokens = getTokens();
            searchDomain = $("#searchDomain").val()
            pathPrefix = $("#pathPrefix").val()


            if (typeof tokens['query'] != 'undefined') {
                fetchdata(tokens)
            }
*/
            $("#toggle-button")
                    .click(
                    function (event) {
                        $("#more-options").slideToggle();
                    });

            $('#topicTitleButton').bind('click', fetchdata);
        });

        function fetchdata(tokens) {
            var urlTokens;
            var subjectId = $("#subjectId").val()
            var site = $("#site").val()

            if (typeof tokens == 'undefined') {
                urlTokens = getTokens();
            }
            else {
                urlTokens = tokens;
            }

            if (typeof urlTokens['query'] != 'undefined') {
                var query = urlTokens['query'];
            }
            else {
                var query = $('#topicTitle').val();
            }

            var limit = 10;
            var start = 0;
            var approvedState = ""
            if (typeof urlTokens['start'] != 'undefined') {
                start = urlTokens['start'];
            }

            if (typeof urlTokens['approved'] != 'undefined') {
                approvedState = urlTokens['approved'];
            }
            else {
                approvedState = $("#approvedSelect").val()
            }

            langMap = [];
            var serviceUrl = searchDomain + "/sites/all/modules/ndla_search/plugins/ndla_search_ord.ac.php/" + query;
            var searchLanguage = ""
            var searchType = $('#typeSelect').val();

            $("input[name=languageSelect]").each(function () {
                if ($(this).is(':checked') && $(this).val() != "") {
                    searchLanguage += "language[]=" + $(this).val() + "&";
                    langMap.push($(this).val())
                }
            });

            searchLanguage = searchLanguage.substr(0, searchLanguage.length - 1)
            serviceUrl += "?" + searchLanguage + "&type=" + searchType+ "&index=topics";
            serviceUrl += "&start=" + start + "&limit=" + limit;

            $.ajax({
                url: serviceUrl,
                type: 'GET',
                dataType: 'json',
            }).done(function (data) {
                var keywords = processJson(data, langMap)
                var dataHtml = renderDataTable(keywords, start, query, approvedState,site,subjectId);
                $('#data-table').html(dataHtml);
            });
        }

        function renderDataTable(json, start, query, approved,site,subjectId) {
            var dataSize = json.length;
            if (dataSize == 0) {
                return '<span class="label label-warning">No Data!</span>';
            }
            var buffer = ['<table id="data-table" class="table table-bordered table-hover"><thead><tr><th>#</th><th>Name</th><th>topicId</th><th>Approved</th><th>Approvaldate</th><th>Connected nodes</th><th>Connected subjects</th><th>Action</th></tr></thead><tbody>'];
            var counter;
            var listedSize = 0;
            for (var i = 0; i < dataSize; i++) {
                counter = i + 1 + parseInt(start);
                var nodeCount = getNodeCount(json[i].identifier,site);
                var subjectCount = getSubjectCount(json[i].identifier,site)
                var row = ""

                if (approved == 'all') {
                    if(nodeCount == 0 && subjectCount == 0) {
                        row = '<tr><td>' + counter.toString() + '</td><td>' + json[i].title + '</td><td>' + json[i].identifier + '</td><td>' + json[i].approved + '</td><td>' + json[i].approval_date + '</td><td>' + nodeCount + '</td><td>' + subjectCount + '</td><td> &nbsp;&nbsp;<a href="editSubjectTopic?topicId='+json[i].identifier+'&site='+site+'&subjectId='+subjectId+'"><input type="button" value="Edit" class="btn btn-warning"/> </a></td></tr>';
                    }
                    else{
                        row = '<tr><td>' + counter.toString() + '</td><td>' + json[i].title + '</td><td>' + json[i].identifier + '</td><td>' + json[i].approved + '</td><td>' + json[i].approval_date + '</td><td>' + nodeCount + '</td><td>' + subjectCount + '</td><td> &nbsp;&nbsp;<a href="editSubjectTopic?topicId='+json[i].identifier+'&site='+site+'&subjectId='+subjectId+'"><input type="button" value="Edit" class="btn btn-warning"/> </a></td></tr>';
                    }

                    listedSize ++;
                }
                else if (approved.toString() == json[i].approved.toString()) {
                    if(nodeCount == 0 && subjectCount == 0) {
                        row = '<tr><td>' + counter.toString() + '</td><td>' + json[i].title + '</td><td>' + json[i].identifier + '</td><td>' + json[i].approved + '</td><td>' + json[i].approval_date + '</td><td>' + nodeCount + '</td><td>' + subjectCount + '</td><td> &nbsp;&nbsp;<a href="editSubjectTopic?topicId='+json[i].identifier+'&site='+site+'&subjectId='+subjectId+'"><input type="button" value="Edit" class="btn btn-warning"/> </a></td></tr>';
                    }
                    else{
                        row = '<tr><td>' + counter.toString() + '</td><td>' + json[i].title + '</td><td>' + json[i].identifier + '</td><td>' + json[i].approved + '</td><td>' + json[i].approval_date + '</td><td>' + nodeCount + '</td><td>' + subjectCount + '</td><td> &nbsp;&nbsp;<a href="editSubjectTopic?topicId='+json[i].identifier+'&site='+site+'&subjectId='+subjectId+'"><input type="button" value="Edit" class="btn btn-warning"/> </a></td></tr>';
                    }

                    listedSize ++;
                }

                buffer.push(row + "\n");
            }
            buffer.push("</tbody></table>");
            if (start > 0 && typeof query != 'undefined') {
                if (listedSize >= 10) {
                    buffer.push('<span id="pager-back"><a href="editSubjectTopics?query=' + query + '&start=' + (parseInt(start) - 10) + '&approved=' + approved + '">&lt;&lt; Previous 10 </a></span>');
                    //buffer.push('<span id="pager-middle">'+start+'.....'+(parseInt(start)+10)+'</span>');
                    buffer.push('<span id="pager-forward"><a href="editSubjectTopics?query=' + query + '&start=' + (parseInt(start) + 10) + '&approved=' + approved + '">Next 10 &gt;&gt;</a></span>');
                }
            }
            else {
                if (listedSize >= 10) {
                    buffer.push('<span id="pager-forward"><a href="editSubjectTopics?query=' + query + '&start=' + (parseInt(start) + 10) + '&approved=' + approved + '">Next 10 &gt;&gt;</a></span>');
                }
            }

            return buffer.join('');
        }

        function processJson(json, langMap) {
            var result = new Array();
            for (var i = 0; i < json.length; ++i) {
                dataObject = {}

                var neutral_title = json[i].title_language_neutral
                jQuery.each(json[i], function (key, val) {
                    if (key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                        var split_arr = key.split("_");
                        if (langMap.indexOf(split_arr[1]) > -1) {
                            dataObject[key] = val;
                        }
                    }
                });
                console.log()
                neutral_title = typeof (neutral_title) == 'undefined' ? json[i].title_nob : json[i].title_language_neutral;

                dataObject['identifier'] = json[i].id
                dataObject['title'] = neutral_title
                dataObject['approved'] = json[i].approved;
                var new_format = 'D MMM YYYY [at] h:mm A';
                var nice = moment(json[i].approval_date, 'YYYY-MM-DD HH:mm:ss').format(new_format);
                dataObject['approval_date'] = nice;
                result.push(dataObject);
            }
            return result;
        }

        function getTokens() {
            var tokens = [];
            var query = location.search;
            query = query.slice(1);
            query = query.split('&');
            $.each(query, function (i, value) {
                var token = value.split('=');
                var key = decodeURIComponent(token[0]);
                var data = decodeURIComponent(token[1]);
                tokens[key] = data;
            });
            return tokens;
        }

        function getNodeCount(identifier,site) {
            var host = window.location.protocol + '//' + window.location.host;
            var serviceUrl = host + pathPrefix+'/service/getNodesByTopicId'

            var counter = 0;
            $.ajax({
                async: false,
                url: serviceUrl,
                data: {
                    'topicId': identifier,
                    'site': site
                },
                dataType: 'json',
            }).done(function (data) {
                counter = data.length;

            });
            return counter;
        }

        function getSubjectCount(identifier, site) {
            var host = window.location.protocol + '//' + window.location.host;
            var serviceUrl = host + pathPrefix+'/service/getSubjectsByTopicId'

            var counter = 0;
            $.ajax({
                async: false,
                url: serviceUrl,
                data: {
                    'topicId': identifier,
                    'site': site
                },
                dataType: 'json',
            }).done(function (data) {
                counter = data.length;

            });
            return counter;
        }
    </r:script>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:2]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab6-1" data-toggle="tab">Update</a></li>
                    <li><g:link controller="ontology" action="addSubjectTopics" params="['data-toggle': 'tab', 'subjectId' : subjectId, 'sites' : site]">Add</g:link></li>
                    <li><g:link controller="ontology" action="removeSubjectTopics" params="['data-toggle': 'tab', 'subjectId' : subjectId, 'sites' : site]">Remove</g:link></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab6-1">
                        <g:form name="updateSubjectTopicForm" useToken="true" action="">
                            <g:hiddenField name="searchDomain" id="searchDomain" value="${searchDomain}" />
                            <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />
                            <g:hiddenField name="subjectId" value="${subjectId}" />
                            <g:hiddenField name="site" value="${site}" />
                            <g:hiddenField name="subjectTopics" value="${subjectTopics}" />

                            <span class="muted" id="toggle-button"><small>Toggle advanced search options</small></span>
                            <div id="more-options" style="display: none">
                                <g:each in="${languages}" var="lang">
                                    <label>${lang.value}<g:checkBox name="languageSelect" class="LanguageSelect" value="${lang.key}" /></label>
                                </g:each>

                                <br />
                                <label>Type</label>
                                <select id="typeSelect">
                                    <option value="start">Start</option>
                                    <option value="end">End</option>
                                    <option value="any">Any</option>
                                </select>
                                <br />
                                <br />
                                <label>Approval</label>
                                <select id="approvedSelect">
                                    <option value="all">All</option>
                                    <option value="true">Approved</option>
                                    <option value="false">Disapproved</option>
                                </select>

                                <label>Sites</label>
                                <label class="checkbox inline">
                                    <input type="checkbox" id="siteCheckbox1" value="site1" checked>NDLA
                                </label>
                                <label class="checkbox inline">
                                    <input type="checkbox" id="siteCheckbox2" value="site2">Deling
                                </label>
                                <label class="checkbox inline">
                                    <input type="checkbox" id="siteCheckbox3" value="site3">Nygiv
                                </label>
                            </div>
                            <fieldset>
                                <legend>Edit subject topics for ${subject.getSubjectName("http://psi.oasis-open.org/iso/639/#nob")}</legend>
                                <label>Search for topic</label>
                                <div class="input-append">
                                    <input class="span10" name="topicTitle" id="topicTitle" type="text" autocomplete="off">
                                    <button class="btn" id="topicTitleButton" type="button">Go!</button>
                                </div>
                                <br />
                                <div id="data-table">

                                </div>
                            </fieldset>
                        </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>