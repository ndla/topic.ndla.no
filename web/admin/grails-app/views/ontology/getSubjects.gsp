<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
    <r:script>
        $(document).ready(function () {
            pathPrefix = $("#pathPrefix").val()
            $( "input[name=sites]").click(function(){
                var site = $( "input:radio[name=sites]:checked").val();
                var subjects = []
                $("#subject").empty()

                subjects = getSubjects(site);
                if(subjects.length > 0) {
                    var options = "";
                    for(var i = 0; i < subjects.length; i++) {
                        var subject = subjects[i];
                        options += '<option value="'+subject.identifier+'">'+subject.name+'</option>';
                    }
                    $("#subject").append(options);
                    $("#subjectSelector").removeClass("hiddenTopics")
                    $("#add_link").removeClass("hide")
                }
            });


            $("#getSubjectForm").submit(function(){
                var len = $("#subject").children().filter("option").length
                console.log($("#subject").children())
                if(len < 2){
                    alert("You have to choose a site and a subject matter")
                    return false;
                }
            });

        });

        function getSubjects(site) {
            var host = window.location.protocol + '//' + window.location.host;
            var serviceUrl = host + pathPrefix+'/service/getSubjectsBySiteString'
            var subjects = []
            $.ajax({
                async: false,
                url: serviceUrl,
                data: {
                    'site': site
                },
                dataType: 'json',
            }).done(function (data) {
                subjects = data;
            });
            return subjects;
        }
    </r:script>
</head>
<body>
    <div class="navbar">
        <g:render template="/actionmenu"model="[param:6]" />
    </div>
    <div class="tabbable">
        <g:if test="${addOntologyTopics == 'true'}">
            <g:render template="submenu" model="[param:2]" />
        </g:if>
        <g:else>
            <g:render template="submenu" model="[param:1]" />
        </g:else>
        <div class="tab-content">
            <div class="tab-pane active" id="tab1">
                <div class="tabbable tabs-right">
                    <ul class="nav nav-tabs">


                    <li class="hide" id="add_link"><g:link controller="ontology" action="addSubject" params="['data-toggle': 'tab']">Add</g:link></li>
                        <!--
                        <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']">Update</g:link>
                    <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'true']">Remove</g:link></li>

                    -->
                </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1-1">
                            <g:form name="getSubjectForm" id="getSubjectForm" useToken="true" action="doSetSubject">
                                <g:hiddenField name="removeSubject" value="${removeSubject}" />
                                <g:hiddenField name="addOntologyTopics" value="${addOntologyTopics}" />
                                <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />
                                <g:if test="${sites == null}">
                                    <label>Choose a Site</label>
                                    <label class="checkbox inline">
                                        <input type="radio" name="sites" value="NDLA"> NDLA
                                    </label>
                                    <!--label class="checkbox inline">
                                        <input type="radio" name="sites"  value="DELING"> Deling
                                    </label-->
                                    <label class="checkbox inline">
                                        <input type="radio" name="sites" value="FYR"> FYR
                                    </label>
                                    <br />
                                    <br />

                                    <div id="subjectSelector" class="hiddenTopics">
                                        <label>Choose subject</label>
                                        <g:select id="subject" from="${subjects.entrySet()}" name="subject" optionKey="key" optionValue="value" noSelection="['':'']"/>
                                    </div>
                                </g:if>
                                <g:else>
                                    <div id="subjectSelector">
                                        <label>Choose subject</label>
                                        <g:select id="subject" from="${subjects.entrySet()}" name="subject" optionKey="key" optionValue="value" noSelection="['':'']"/>
                                    </div>
                                </g:else>



                                <br />
                                <br />
                                <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                            </g:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>