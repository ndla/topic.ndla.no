<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:2]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1-1">
                        <g:form name="removeSubjectTopicForm" useToken="true" action="doRemoveRoleTopic" autocomplete="off">
                            <g:hiddenField name="topicId" value="${topicId}" />
                            <fieldset>
                                <legend>Remove RoleTopic</legend>


                                <p>Are you sure you want to delete "<em>${roleName}</em>" ?</p>
                                 <g:if test="${relationCount.toString() != '0'}">
                                     <p>The role topic is part of the relation definition of the <em>${assocType}</em> relationship type with <strong>${relationCount}</strong> relation instances</p>
                                     <p>Deleting the role will delete all these relations</p>
                                 </g:if>

                                <br />
                                <br />
                                <g:link action="removeRelationRoleTopics" controller="ontology"><input type="button" value="Cancel" class="btn btn-warning"/></g:link>  <g:submitButton name="submit" class="btn btn-danger" value="Submit" />
                            </fieldset>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>