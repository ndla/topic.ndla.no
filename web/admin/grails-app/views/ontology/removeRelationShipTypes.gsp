<%--
  Created by IntelliJ IDEA.
  User: rolfguescini
  Date: 03.09.14
  Time: 09:23
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title></title>
<r:script>
    $(document).ready(function () {
        searchDomain = $("#searchDomain").val()
        pathPrefix = $("#pathPrefix").val()

        $(".assocButton").click(function(){
            var id = $(this).attr("id");

            var idArray  = new Array();
            var theId = ""
            var psiUri = ""
            if(id.indexOf("#") > -1){
                theId = id.substring(id.indexOf("#"));
                var uri = id.substring(0,id.lastIndexOf("#"));
                var idArray = uri.split("_");
                psiUri = idArray[1]
            }
            else{
                theId = id.substring(id.lastIndexOf("/")+1);
                var uri = id.substring(0,id.lastIndexOf("/"));
                var idArray = uri.split("_");
                psiUri = idArray[1]
            }

            var role1 = ""
            var role2 = ""
            $(this).parent().parent().find("input").each(function(){
                var thisId = $(this).attr("id");
                if(thisId == "role1"){
                    role1 = $(this).val().substring($(this).val().lastIndexOf("#")+1);
                }
                else if(thisId == "role2"){
                    role2 = $(this).val().substring($(this).val().lastIndexOf("#")+1);
                }
            });
            var relationName = $(this).parent().parent().prev().html()
            openTopicCreator(psiUri,theId,role1,role2, relationName,pathPrefix)
        });




    });

    function openTopicCreator(uri,psi, role1, role2, title) {
        var labelContent = "Relations using the "+title+" relationshiptype" ;
        $("#myModalLabel").html(labelContent);
        if(psi.contains("ndlanode-keyword")){
            var loadingContent = "<h2>Wait... this one is a biggie. Loading...</h2>";
        }
        else{
            var loadingContent = "<h2>Loading...</h2>";
        }
        $(".modal-body").html(loadingContent);
        var host = window.location.protocol + '//' + window.location.host;
        var theUri = uri.substring(7).replace(/\//gi,"_");
        var theId = psi.replace("#","§");

        var serviceUrl = pathPrefix+'/service/getAssociationCount'
        $.ajax({
            url : serviceUrl,
            data: {
                'psiUrl': theUri,
                'psiId' : encodeURIComponent(theId),
                'role1Id': role1,
                'role2Id' : role2,
                'name' : title
            },
            type: "GET",
            success: function(response) {
                $(".modal-body").html(response).modal(  );
            }
        });


        /*

        var serviceUrl = '/admin/service/getAssociationCount?psiUrl='+theUri+"&psiId="+theId+"&role1Id="+role1+"&role2Id="+role2+"&name="+title.replace(/\//gi,"_")+ " #page-content";
        console.log("ID: "+theUri+" R1: "+role1+" R2: "+role2+" RELATION: "+title+" URL: "+serviceUrl)
        $(".modal-body").load(serviceUrl);
                */

    }



    function getAssociationCount(identifier,role1,role2) {
        var host = window.location.protocol + '//' + window.location.host;
        var serviceUrl = host + pathPrefix+'/service/getAssociationCount'

        var result = "";
        $.ajax({
            async: false,
            url: serviceUrl,
            data: {
                'psi': identifier,
                'role1Id': role1,
                'role2Id' : role2
            },
            dataType: 'json',
        }).done(function (data) {
            result = data;

        });
        return result;
    }
</r:script>
</head>

<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:3]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li><g:link controller="ontology" action="editRelationShipTypes">Update</g:link></li>
                    <li><g:link controller="ontology" action="createRelationshipType">Add</g:link></li>
                    <li class="active"><a href="#tab6-1" data-toggle="tab">Remove</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab6-1">
                        <g:form name="removeSubjectTopicByTopicIdForm" useToken="true" action="doRemoveSubjectTopics">
                            <g:hiddenField name="searchDomain" id="searchDomain" value="${searchDomain}" />
                            <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />

                       
                            <div id="data-table-subjectTopics">
                                <table>
                                    <g:each in="${relationObjects}" var="relationObject">
                                        <tr>
                                            <g:set var="relationNameString" value="" />
                                            <g:set var="currentRole" value="" />
                                            <g:set var="currentRole1" value="" />
                                            <g:set var="currentRole2" value="" />
                                            <g:each in="${relationObject.names}" var="nameData">
                                                <g:each in="${nameData.value}" var="name">
                                                    <g:if test="${name.key == "isoNames"}">
                                                        <g:if test="${name.value.get("http://psi.oasis-open.org/iso/639/#eng") != null}">
                                                            <g:if test="${currentRole == ""}">
                                                                <g:set var="relationNameString" value="${name.value.get("http://psi.oasis-open.org/iso/639/#eng")+ " / "}" />

                                                            </g:if>
                                                            <g:else>
                                                                <g:set var="relationNameString" value="${relationNameString+name.value.get("http://psi.oasis-open.org/iso/639/#eng")}" />
                                                            </g:else>

                                                        </g:if>
                                                    </g:if>
                                                    <g:elseif test="${name.key == "roleNames"}">
                                                        <g:each in="${name.value}" var="roleName">
                                                            <g:if test="${currentRole == ""}">
                                                                <g:set var="currentRole1" value="${roleName.key}" />
                                                            </g:if>
                                                            <!--g:elseif test="${currentRole != "" && currentRole != roleName.value}"-->
                                                                <g:set var="currentRole2" value="${roleName.key}" />
                                                            <!--/g:elseif-->
                                                            <g:set var="currentRole" value="${roleName.value}" />
                                                        </g:each>
                                                    </g:elseif>
                                                </g:each>
                                            </g:each>
                                            <g:if test="${relationNameString.contains("Node has keyword")}">
                                                <td>${relationNameString} (${relationObject.psi.substring(relationObject.psi.indexOf("#")+1)})</td>
                                            </g:if>
                                            <g:else>
                                                <td>${relationNameString}</td>
                                            </g:else>

                                            <td>

                                                <a href="#" data-target="#myModal" role="button" data-toggle="modal" class="modalLink"><input type="button" id="assocCheck_${relationObject.psi}" value="Check number of relations" class="btn assocButton btn-info"/></a>
                                                <g:hiddenField name="role1" id="role1" value="${currentRole1}" />
                                                <g:hiddenField name="role2" id="role2" value="${currentRole2}" />
                                            </td>
                                            <td><a href="removeRemoveRelationsTypeByTypeId?relationTypeId=${relationObject.psi.toString().replace("#","§")}&relationTypeName=${relationNameString}&role1=${currentRole1.replace("#","§")}&role2=${currentRole2.replace("#","§")}"><input type="button" value="Delete" class="btn btn-warning"/></a></td>
                                        </tr>
                                    </g:each>
                                </table>
                            </div>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"  class="modal-dialog">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="myModalLabel"  class="modal-title">Association count</h3>
    </div>
    <div class="modal-body">
        <p>Create a node topic</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
</body>
</html>