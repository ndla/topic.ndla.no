<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:3]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1-1">
                        <g:form name="removeRelationTypeForm" useToken="true" action="doRemoveRelationType" autocomplete="off">
                            <g:hiddenField name="relationTypeId" value="${relationTypeId}" />
                            <g:hiddenField name="relationTypeName" value="${relationTypeName}" />
                            <g:hiddenField name="relationImplementCount" value="${relationTypeName}" />
                            <fieldset>
                                <legend>Remove Relationship Type</legend>


                                <p>Are you sure you want to delete "<em>${relationTypeName}</em>"

                                <g:if test="${relationImplementCount > 0}">
                                    <p>The relationship type is used in ${relationImplementCount} relations</p>
                                </g:if>

                                <br />
                                <br />
                                <g:link action="removeRelationShipTypes" controller="ontology"><input type="button" value="Cancel" class="btn btn-warning"/></g:link>  <g:submitButton name="submit" class="btn btn-danger" value="Submit" />
                            </fieldset>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>