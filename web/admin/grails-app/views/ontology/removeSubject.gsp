<%--
  Created by IntelliJ IDEA.
  User: rolfguescini
  Date: 27.05.14
  Time: 16:06
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
</head>

<body>

<div class="navbar">
    <g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:1]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li><g:link controller="ontology" action="getSubjects" params="['data-toggle': 'tab', 'removeSubject' : 'false']">Update</g:link></li>
                    <li><g:link controller="ontology" action="addSubject" params="['data-toggle': 'tab']">Add</g:link></li>
                    <li class="active"><a href="#tab1-1" data-toggle="tab">Remove</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1-1">
                        <g:form name="updateSubjectForm" useToken="true" action="doRemoveSubject">
                            <g:hiddenField name="subjectId" value="${subjectId}" />
                            <p>Are you sure you want to delete ${subjectName}?</p>
                            <g:submitButton name="submit" class="btn btn-danger" value="Delete" />
                        </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>