<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:6]" />
</div>
<div class="tabbable">
    <g:render template="submenu" model="[param:2]" />
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab1-1">
                        <g:form name="removeSubjectTopicForm" useToken="true" action="doRemoveSubjectTopic" autocomplete="off">
                            <g:hiddenField name="topicId" value="${topicId}" />
                            <g:hiddenField name="subjectId" value="${subjectId}" />
                            <g:hiddenField name="site" value="${site}" />
                            <fieldset>
                                <legend>Remove SubjectTopic</legend>


                                <p>Are you sure you want to delete "<em>${topicName}</em>" from ${subjectName}'s subject ontology?</p>

                                <g:if test="${nodeCount > 0}">
                                    <p>The topic is connected to ${nodeCount} nodes, which will lose the relation</p>
                                </g:if>

                                <br />
                                <br />
                                <g:link action="removeSubjectTopics" controller="ontology" params="['subjectId': subjectId, 'sites' : site]"><input type="button" value="Cancel" class="btn btn-warning"/></g:link>  <g:submitButton name="submit" class="btn btn-danger" value="Submit" />
                            </fieldset>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>