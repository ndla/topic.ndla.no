<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Ontologies</title>
    <r:script>
	        $(document).ready(function() {
                $("#addNewTopicTitleButton").click(function(){
                    $('#newTopicWrapper').fadeIn();
                });


                $("#addNewTopicButton").click(function(){
                    var newTitleEng = $("#newTopicTitleEng").val();
                    var newTitleNob = $("#newTopicTitleNob").val();
                    var wordClass = $("#newTopicTitleWordClass").val();

                    if(newTitleEng != "" && newTitleNob != "") {
                        var table_row = '<tr>';
                        table_row += '<td class="muted">new topic;</td>';
                        table_row += '<td class="pull-left"><strong>'+newTitleEng+'</strong></td>';
                        table_row += '<td><span class="label label-info">new topic</span></td>';
                        table_row +='</tr>';
                        $('#addedTopicsBody').append(table_row);

                        var newTopicVal = "wordClass;"+wordClass+"§eng;"+newTitleEng+"§nob;"+newTitleNob

                        $("#newTopicTitles").children("input").each(function(){
                            if($(this).val() != ""){
                                newTopicVal += "§"+$(this).attr("id")+";"+$(this).val()
                            }

                        });

                       $("#newTopicDescriptions").children("textarea").each(function(){
                        if($(this).val() != ""){
                            newTopicVal += "§"+$(this).attr("id")+";"+$(this).val()
                        }
                      });

                      $("#newTopicImages").children("input").each(function(){
                        if($(this).val() != ""){
                            newTopicVal += "§"+$(this).attr("id")+";"+$(this).val()
                        }
                      });

                        var checkBox = '<g:checkBox name="hiddenTopics" value="'+newTopicVal+'" checked="true"/>';

                        $('#topicWrapper').append(checkBox);
                        $("#newTopicTitleEng").val("");
                        $("#newTopicTitleNob").val("");
                        $('#newTopicTitles').html("");
                        $('#newTopicWrapper').fadeOut();
                        $("#requiredWarning").fadeOut();
                    }
                    else{
                        $("#requiredWarning").fadeIn();
                    }

                })

                $('#addLanguageButton').click(function() {
                   var selectedLanguageId = $("#missingLanguages option:selected").val()
                   var selectedLanguageText = $("#missingLanguages option:selected").text()
                   $("#newTopicTitles").append('<label>'+selectedLanguageText+' title</label><input type="text" name="'+selectedLanguageId+'" id="'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+' title" autocomplete="off" value="">');
                });

                $('#addImageLanguageButton').click(function() {
                   var selectedLanguageId = $("#missingImageLanguages option:selected").val()
                   var selectedLanguageText = $("#missingImageLanguages option:selected").text()
                   $("#newTopicImages").append('<label>'+selectedLanguageText+' Image URL</label><input type="text" name="image_'+selectedLanguageId+'" id="image_'+selectedLanguageId+'" placeholder="type a '+selectedLanguageText+' image URL" autocomplete="off" value="">');
                });


                $('#addDescriptionLanguageButton').click(function(){
                    var selectedLanguageId = $("#missingDescriptionLanguages option:selected").val()
                   var selectedLanguageText = $("#missingDescriptionLanguages option:selected").text()
                   $("#newTopicDescriptions").append('<label>'+selectedLanguageText+' description</label><textarea id="description_'+selectedLanguageId+'" cols="50" rows="5" name="description_'+selectedLanguageId+'"></textarea>');
                });


	        });
    </r:script>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:2]" />
</div>
<div class="tabbable">
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li><g:link controller="topic" action="editTopics">Update</g:link></li>
                    <li class="active"><a href="#tab2-1" data-toggle="tab">Add</a></li>
                    <li><g:link controller="topic" action="removeTopics">Remove</g:link></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab6-1">
                        <g:form name="addTopicsForm" useToken="true" action="doAddTopics">
                            <g:hiddenField name="searchDomain" value="${searchDomain}" />
                            <g:hiddenField name="modal" value="${modal}" />
                            <g:hiddenField name="selectId" value="${select}" />
                            <fieldset>
                                <legend>Add topics</legend>
                                <div id="newTopic">

                                    <div class="well well-small" id="newTopicTitle">
                                        <p class="modal-header"><strong>Topic titles</strong></p>
                                        <div class="alert alert-block" id="requiredWarning">
                                            <button type="button" class="close" data-dismiss="alert">&times;</button>
                                            <h4>Error!</h4>
                                            The Required English and Bokmål titles cannot be empty
                                        </div>
                                        <label>Required English Title</label>
                                        <input type="text" id="newTopicTitleEng" name="newTopicTitleEng" placeholder="Type an English title...">
                                        <label>Required Norwegian Bokmål Title</label>
                                        <input type="text" id="newTopicTitleNob" name="newTopicTitleNob" placeholder="Type an English title...">
                                        <div id="newTopicTitles">

                                        </div>
                                        <br />
                                        <g:select name="newTopicTitleWordClass" id="newTopicTitleWordClass" from="${wordClassMap}" optionKey="key" optionValue="value" noSelection="['':'Add a wordClass']"/>
                                    </div>
                                    <g:select id="missingLanguages" from="${missinglangs.entrySet()}" name="missingLanguages" optionKey="key" optionValue="value" noSelection="['':'Add a language']"/> <button class="btn-mini" id="addLanguageButton" type="button">Go!</button>
                                    <br />

                                    <div class="well well-small" id="newTopicDescriptions">
                                        <p class="modal-header"><strong>Topic descriptions</strong></p>
                                    </div>
                                    <select id="missingDescriptionLanguages" name="missingDescriptionLanguages">
                                        <option value="">Add a language</option>
                                        <g:each in="${langMap}" var="langItem">
                                            <option value="${shortMap.get(langItem.key)}">${langItem.value}</option>
                                        </g:each>
                                    </select>
                                    <button class="btn-mini" id="addDescriptionLanguageButton" type="button">Go!</button>
                                    <br />
                                    <div class="well well-small" id="newTopicImages">
                                        <p class="modal-header"><strong>Topic images</strong></p>
                                    </div>
                                    <select id="missingImageLanguages" name="missingImageLanguages">
                                        <option value="">Add a language</option>
                                        <g:each in="${langMap}" var="langItem">
                                            <option value="${shortMap.get(langItem.key)}">${langItem.value}</option>
                                        </g:each>
                                    </select>
                                    <button class="btn-mini" id="addImageLanguageButton" type="button">Go!</button>
                                    <br />
                                    <button class="btn-warning pull-right" id="addNewTopicButton" type="button">Add topic!</button>
                                </div>
                                <br />
                                <br />
                                <br />
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <table class="table table-condensed">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                <th>Topic Id</th>
                                                <th>Topic title</th>
                                                <th>Type</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="panel-body">
                                        <!--table-->
                                        <table class="table table-condensed" id="addedTopics">
                                            <tbody id="addedTopicsBody">
                                            </tbody>
                                        </table>
                                        <!--end of table-->
                                    </div>
                                    <div class="panel-footer">Panel footer</div>
                                </div>
                                <div id="topicWrapper" class="hiddenTopics">

                                </div>

                                <g:submitButton name="submit" class="btn btn-warning" value="Submit" />
                            </fieldset>
                        </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>