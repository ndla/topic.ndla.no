<!doctype html>
<html>
<head>
<meta name="layout" content="main"/>
<title>NDLA Topics Admin Application - Ontologies</title>
<style>
#myModal {
    height: auto;
    margin: 0 0 0 -400px;
    max-height: 600px;
    max-width: 800px;
    width: auto;
    overflow: scroll;
}
</style>
<r:script>
    function processLink(identifier) {
        var resourceHost = $("#resourceHost").val();
        var nodeData = JSON.parse($("#nodeData").val());
        var currentNodeData = nodeData[identifier];

        var html = "<ul>";
        for(var i = 0; i < currentNodeData.length; i++){
            var currentNode =  currentNodeData[i];
            var nodeidArray = currentNode.identifier.split("_");
            var siteId = nodeidArray[0]
            var nid = nodeidArray[1]
            if(siteId.indexOf("nygiv") > -1){
                resourceHost = "http://fyr.ndla.no";
            }
            var nodeid = "node/"+nid
            html += '<li><a href="'+resourceHost+'/'+nodeid+'" target="_blank">'+currentNode.name+'</a> (node : '+nid+')</li>';
        }
        html += "</ul>";
        //console.log("html: "+html)

        var labelContent = "NDLA node preview [" + identifier + "]";
        $("#myModalLabel").html(labelContent);
        $(".modal-body").html(html);
    }


</r:script>
<r:script>
    $(document).ready(function() {
        searchDomain = $("#searchDomain").val()

        $("#toggle-button")
                .click(
                function (event) {
                    $("#more-options").slideToggle();
                });
        var tokens = getTokens();
        searchDomain = $("#searchDomain").val()
        pathPrefix = $("#pathPrefix").val()


        if (typeof tokens['query'] != 'undefined') {
            //searchTopics(tokens)
            lookupIndex(tokens)
        }


        $('#topicTitleButton').bind('click', lookupIndex);

        $(document).keypress(function(e) {
            if(e.which == 13) {
                e.preventDefault();
            }
        });

        $('#myModal').on('show', function () {
            $(this).find('.modal-body').css({width: 'auto', height: 'auto', 'max-height': '100%'});
        });
    });

    function lookupIndex(tokens){
        var host = window.location.protocol + '//' + window.location.host;
        var serviceUrl = host + pathPrefix+'/service/lookUpTopics'

        $('#data-table').html('<div class="cogspinner" />')
        var approvedState = $("#approvedSelect").val()
        var queryParams = {}

        if (typeof tokens == 'undefined') {
            urlTokens = getTokens();
        }
        else {
            urlTokens = tokens;
        }

        if (typeof urlTokens['query'] != 'undefined') {
            var search = urlTokens['query'];
            queryParams['query'] = search
        }
        else {
            var search = $("#topicTitle").val();
        }


        var site = ""
        if(typeof urlTokens["site"] != 'undefined') {
            site = urlTokens["site"]
            console.log("urlTokensWTF: "+site)
        }
        else{
            site = $('input[name=siteRadio]:checked').val();
            console.log("WTF: "+site)
            queryParams['site'] = site
        }

        queryParams['site'] = site

        var limit = 10;
        var start = 0;
        var approvedState = ""
        if (typeof urlTokens['start'] != 'undefined') {
            start = urlTokens['start'];
        }

        if (typeof urlTokens['approved'] != 'undefined') {
            approvedState = urlTokens['approved'];
        }
        else {
            approvedState = $("#approvedSelect").val()
        }

        if(typeof urlTokens["limit"] != 'undefined') {
            limit = urlTokens["limit"]
        }
        else{
            if(approvedState == "false" && typeof $('#limitUp').val() != 'undefined') {
                limit = $('#limitUp').val();
            }
            else{
                limit = 10;
            }
        }

        var language = ""
        if(typeof urlTokens["lang"] != "undefined"){
            language = urlTokens["lang"];
        }
        else{
            language = $('input:radio[name=languageSelect]:checked').val();
            queryParams['lang'] = language
        }

        var searchType = ""
        if(typeof urlTokens["searchType"] != "undefined"){
            searchType = urlTokens["searchType"];
        }
        else{
            searchType = $("#typeSelect").val();
            queryParams['searchType'] = searchType
        }



        queryParams['limit'] = limit
        queryParams['approvedState'] = approvedState

        $.ajax({
            url: serviceUrl,
            type: 'GET',
            dataType: 'json',
            data: {
                'q': search,
                'language' : language,
                'offset' : start
            },
            success: function (response) {
                var data = processIndex(response,language);
                var dataHtml = renderSearchDataTable(data, start,search, approvedState,queryParams);

                $('#data-table').html(dataHtml);
            }

        }, 300);
    }


    function processIndex(json,lang) {
        var result = new Array();
        for(var i = 0; i < json.length; ++i) {
            dataObject = {}

            var neutral_title = json[i].title_language_neutral
            jQuery.each(json[i], function(key, val) {
                if(key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                    if(key.indexOf(lang) > -1){
                        var split_arr = key.split("_");
                        dataObject[key] = val;
                    }
                }
            });


            dataObject['identifier'] = json[i].id
            dataObject['title'] = dataObject['title_'+lang]
            dataObject['wordclass'] = json[i].wordclass;
            if(typeof json[i].visibility == 'undefined'){
                dataObject['visibility'] = json[i].visible;
            }
            else{
                dataObject['visibility'] = json[i].visibility;
            }

            var type = []
            if(typeof json[i].type_id != 'undefined'){
                var typeString = json[i].type_id;
                type.push(typeString)
            }

            dataObject['types'] = type;
            dataObject['approved'] = json[i].approved;
            dataObject['approval_date'] = 'N/A';

            if(typeof json[i].approval_date != 'undefined'){
                dataObject['approval_date'] = json[i].approval_date.substring(0,10);
            }
            dataObject['processState'] = '0';
            if(typeof json[i].process_state != 'undefined'){
                dataObject['processState'] = json[i].process_state;
            }
            result.push(dataObject);
        }
        return result;
    }

    function searchTopics(tokens){
        $('#data-table').html('<div class="cogspinner" />')
        var approvedState = $("#approvedSelect").val()

        var host = window.location.protocol + '//' + window.location.host;

        var serviceUrl = host + pathPrefix+'/service/searchTopicsByName'
        var queryParams = {}

        if (typeof tokens == 'undefined') {
            urlTokens = getTokens();
        }
        else {
            urlTokens = tokens;
        }

        if (typeof urlTokens['query'] != 'undefined') {
            var search = urlTokens['query'];
            queryParams['query'] = search
        }
        else {
            var search = $("#topicTitle").val();
        }

        var site = ""
        if(typeof urlTokens["site"] != 'undefined') {
            site = urlTokens["site"]
            console.log("urlTokensWTF: "+site)
        }
        else{
            site = $('input[name=siteRadio]:checked').val();
            console.log("WTF: "+site)
            queryParams['site'] = site
        }


        var limit = 10;
        var start = 0;
        var approvedState = ""
        if (typeof urlTokens['start'] != 'undefined') {
            start = urlTokens['start'];
        }

        if (typeof urlTokens['approved'] != 'undefined') {
            approvedState = urlTokens['approved'];
        }
        else {
            approvedState = $("#approvedSelect").val()
        }

        if(typeof urlTokens["limit"] != 'undefined') {
            limit = urlTokens["limit"]
        }
        else{
            if(approvedState == "false" && typeof $('#limitUp').val() != 'undefined') {
                limit = $('#limitUp').val();
            }
            else{
                limit = 10;
            }
        }


        var language = ""
        if(typeof urlTokens["lang"] != "undefined"){
            language = urlTokens["lang"];
        }
        else{
            language = $('input:radio[name=languageSelect]:checked').val();
            queryParams['lang'] = language
        }

        var searchType = ""
        if(typeof urlTokens["searchType"] != "undefined"){
            searchType = urlTokens["searchType"];
        }
        else{
            searchType = $("#typeSelect").val();
            queryParams['searchType'] = searchType
        }




        queryParams['limit'] = limit
        queryParams['approvedState'] = approvedState



        $.ajax({
            async: false,
            url: serviceUrl,
            data: {
                'search': search,
                'limit': limit,
                'offset' : start,
                'type' : "topic",
                'language' : language,
                'searchType' : searchType
            },
            dataType: 'json'
        }).done(function (data) {
            var topics = processJson(data)

            var dataHtml = renderSearchDataTable(topics, start,search, approvedState,queryParams);

            $('#data-table').html(dataHtml);
            counter = data.length;
        });

    }


    function renderSearchDataTable(json, start, query, approved,queryParams) {
        var dataSize = json.length;
        var processMap = {};
        processMap["0"] = "Not processed";
        processMap["1"] = "Pending";
        processMap["2"] = "Processed";

        if (dataSize == 0) {
            return '<span class="label label-warning">No Data!</span>';
        }
        var buffer = ['<table id="data-table" class="table table-bordered table-hover"><thead><tr><th>#</th><th>Name</th><th>topicId</th><th>Approved</th><th>Approvaldate</th><th>ProcessState</th><th>Connected nodes</th><th>Connected subjects</th><th>Action</th></tr></thead><tbody>'];
        var counter;
        var listedSize = 0;
        for (var i = 0; i < dataSize; i++) {
            counter = i + 1 + parseInt(start);
            //var nodeCount = json[i].nodeCount;
            var connectedNodes = getConnectedNodes(json[i].identifier,queryParams['site']);
            var nodeCount = connectedNodes.length;
            var subjectCount = getSubjectCount(json[i].identifier,'NDLA')
            var row = ""

            if (approved == 'all') {
                if(nodeCount == 0 && subjectCount == 0) {
                    row = '<tr><td>' + counter.toString() + '</td><td>' + json[i].title + '</td><td>' + json[i].identifier + '</td><td>' + json[i].approved + '</td><td>' + json[i].approval_date + '</td><td>'+processMap[json[i].processState]+'</td><td>' + nodeCount + ' <a href="#" onclick="processLink(\'' + json[i].identifier + '\');" data-target="#myModal" role="button" data-toggle="modal"><i class="icon-eye-open"></i></a></td><td>' + subjectCount + '</td><td> &nbsp;&nbsp;<a href="editTopic?topicId='+json[i].identifier+'"><input type="button" value="Edit" class="btn btn-warning"/> </a></td></tr>';
                }
                else{
                    row = '<tr><td>' + counter.toString() + '</td><td>' + json[i].title + '</td><td>' + json[i].identifier + '</td><td>' + json[i].approved + '</td><td>' + json[i].approval_date + '</td><td>'+processMap[json[i].processState]+'</td><td>' + nodeCount + ' <a href="#" onclick="processLink(\'' + json[i].identifier + '\');" data-target="#myModal" role="button" data-toggle="modal"><i class="icon-eye-open"></i></a></td><td>' + subjectCount + '</td><td> &nbsp;&nbsp;<a href="editTopic?topicId='+json[i].identifier+'"><input type="button" value="Edit" class="btn btn-warning"/> </a></td></tr>';
                }

                listedSize ++;
            }
            else if (approved.toString() == json[i].approved.toString()) {
                if(nodeCount == 0 && subjectCount == 0) {
                    row = '<tr><td>' + counter.toString() + '</td><td>' + json[i].title + '</td><td>' + json[i].identifier + '</td><td>' + json[i].approved + '</td><td>' + json[i].approval_date + '</td><td>'+processMap[json[i].processState]+'</td><td>' + nodeCount + ' <a href="#" onclick="processLink(\'' + json[i].identifier + '\');" data-target="#myModal" role="button" data-toggle="modal"><i class="icon-eye-open"></i></a></td><td>' + subjectCount + '</td><td> &nbsp;&nbsp;<a href="editTopic?topicId='+json[i].identifier+'"><input type="button" value="Edit" class="btn btn-warning"/> </a></td></tr>';
                }
                else{
                    row = '<tr><td>' + counter.toString() + '</td><td>' + json[i].title + '</td><td>' + json[i].identifier + '</td><td>' + json[i].approved + '</td><td>' + json[i].approval_date + '</td><td>'+processMap[json[i].processState]+'</td><td>' + nodeCount + ' <a href="#" onclick="processLink(\'' + json[i].identifier + '\');" data-target="#myModal" role="button" data-toggle="modal"><i class="icon-eye-open"></i></a></td><td>' + subjectCount + '</td><td> &nbsp;&nbsp;<a href="editTopic?topicId='+json[i].identifier+'"><input type="button" value="Edit" class="btn btn-warning"/> </a></td></tr>';
                }

                listedSize ++;
            }

            buffer.push(row + "\n");
        }
        buffer.push("</tbody></table>");
        if (start > 0 && typeof query != 'undefined') {
            if(start >= 10){
                buffer.push('<span id="pager-back"><a href="editTopics?query=' + query + '&start=' + (parseInt(start) - 10) + '&approved=' + approved + '">&lt;&lt; Previous 10 </a></span>');
            }
            if (listedSize >= 10) {
                buffer.push('<span id="pager-forward"><a href="editTopics?query=' + query + '&start=' + (parseInt(start) + 10) + '&approved=' + approved + '">Next 10 &gt;&gt;</a></span>');
            }
        }
        else {
            if (listedSize >= 10) {
                buffer.push('<span id="pager-forward"><a href="editTopics?query=' + query + '&start=' + (parseInt(start) + 10) + '&approved=' + approved + '">Next 10 &gt;&gt;</a></span>');
            }
        }

        return buffer.join('');
    }



    function processJson(json, langMap) {
        var result = new Array();
        for (var i = 0; i < json.length; ++i) {
            dataObject = {}

            var neutral_title = json[i].title_language_neutral
            jQuery.each(json[i], function (key, val) {
                if (key.indexOf("title_") > -1 && key.indexOf('_title_') == -1) {
                    var split_arr = key.split("_");
                    if (langMap.indexOf(split_arr[1]) > -1) {
                        dataObject[key] = val;
                    }
                }
            });
            dataObject['identifier'] = json[i].id
            dataObject['title'] = json[i].name
            dataObject['approved'] = json[i].approved;
            dataObject['nodeCount'] = json[i].nodeCount;
            var new_format = 'D MMM YYYY [at] h:mm A';
            var nice = moment(json[i].approval_date, 'YYYY-MM-DD HH:mm:ss').format(new_format);
            if(nice == "Invalid date"){
                nice = " - "
            }
            dataObject['approval_date'] = nice;
            dataObject['processState'] = json[i].processState;

            result.push(dataObject);
        }
        return result;
    }

    function getTokens() {
        var tokens = [];
        var query = location.search;
        query = query.slice(1);
        query = query.split('&');
        $.each(query, function (i, value) {
            var token = value.split('=');
            var key = decodeURIComponent(token[0]);
            var data = decodeURIComponent(token[1]);
            tokens[key] = data;
        });
        return tokens;
    }

    function getConnectedNodes(identifier,site) {
        var host = window.location.protocol + '//' + window.location.host;
        var serviceUrl = host + pathPrefix+'/service/getNodesByTopicId'
        var nodeData = new Array();
        var counter = 0;
        $.ajax({
            async: false,
            url: serviceUrl,
            data: {
                'topicId': identifier,
                'site': site
            },
            dataType: 'json',
        }).done(function (data) {
            for(var i = 0; i < data.length; i++) {
                var nodeObject = data[i];
                var tempObject = {}
                tempObject.identifier = nodeObject.identifier
                tempObject.site = nodeObject.site
                if(typeof nodeObject.names["http://psi.oasis-open.org/iso/639/#nob"] != 'undefined'){
                    tempObject.name = nodeObject.names["http://psi.oasis-open.org/iso/639/#nob"]
                }
                else if(typeof nodeObject.names["http://psi.oasis-open.org/iso/639/#eng"] != 'undefined'){
                    tempObject.name = nodeObject.names["http://psi.oasis-open.org/iso/639/#eng"]
                }else if(typeof nodeObject.names["http://psi.topic.ndla.no/#language-neutral"] != 'undefined'){
                    tempObject.name = nodeObject.names["http://psi.topic.ndla.no/#language-neutral"]
                }

                nodeData.push(tempObject)
            }

        });
        var currentNodeData = JSON.parse($("#nodeData").val());
        currentNodeData[identifier] = nodeData;
        $("#nodeData").val(JSON.stringify(currentNodeData));

        return nodeData;
    }

    function getSubjectCount(identifier, site) {
        var host = window.location.protocol + '//' + window.location.host;
        var serviceUrl = host + pathPrefix+'/service/getSubjectsByTopicId'

        var counter = 0;
        $.ajax({
            async: false,
            url: serviceUrl,
            data: {
                'topicId': identifier,
                'site': site
            },
            dataType: 'json',
        }).done(function (data) {
            counter = data.length;

        });
        return counter;
    }
</r:script>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu"model="[param:2]" />
</div>
<div class="tabbable">

    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab2-1" data-toggle="tab">Update</a></li>
                    <li><g:link controller="topic" action="addTopics">Add</g:link></li>
                    <li><g:link controller="topic" action="removeTopics">Remove</g:link></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2-1">
                        <g:form name="updateSubjectTopicForm" useToken="true" action="">
                            <g:hiddenField name="searchDomain" id="searchDomain" value="${searchDomain}" />
                            <g:hiddenField name="pathPrefix" id="pathPrefix" value="${pathPrefix}" />
                            <g:hiddenField name="subjectId" value="${subjectId}" />
                            <g:hiddenField name="nodeData" id="nodeData" value="{}" />
                            <g:hiddenField name="resourceHost" id="resourceHost" value="${resourceHost}" />
                            <fieldset>
                                <legend>Edit topics</legend>
                            </fieldset>
                            <span class="muted" id="toggle-button"><small>Toggle advanced search options</small></span>
                            <div id="more-options" style="display: none">
                                <g:each in="${languages}" var="lang">
                                    <label>${lang.value}<g:radio name="languageSelect" class="LanguageSelect" value="${lang.key}" checked="${lang.key == 'nob'}" /></label>
                                </g:each>
                                <br />
                                <label>Type</label>
                                <select id="typeSelect">
                                    <option value="start">Start</option>
                                    <option value="any">Any</option>
                                    <option value="end">End</option>
                                </select>
                                <br />
                                <br />
                                <label>Approval</label>
                                <select id="approvedSelect">
                                    <option value="all">All</option>
                                    <option value="true">Approved</option>
                                    <option value="false">Disapproved</option>
                                </select>


                                <label>Site</label>
                                <label class="checkbox inline">
                                    <input type="radio" id="siteRadio"  name="siteRadio" value="NDLA" checked>NDLA
                                </label>
                                <label class="checkbox inline">
                                    <input type="radio" id="siteRadio" name="siteRadio" value="NYGIV">Nygiv
                                </label>

                            </div>
                            <label>Search for topic</label>
                            <div class="input-append">
                                <input class="span10" name="topicTitle" id="topicTitle" type="text" autocomplete="off">
                                <button class="btn" id="topicTitleButton" type="button">Go!</button>
                            </div>
                            <br />

                            <div id="data-table">
                                <p>&nbsp;</p>
                            </div>
                        </g:form>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h3 id="myModalLabel">NDLA node preview</h3>
    </div>
    <div class="modal-body" style="min-width:1000px;">
        <p>NDLA node content</p>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>
</body>
</html>