<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>NDLA Topics Admin Application - Topics</title>
</head>
<body>
<div class="navbar">
    <g:render template="/actionmenu" model="[param:2]" />
</div>
<div class="tabbable">
    <div class="tab-content">
        <div class="tab-pane active" id="tab1">
            <div class="tabbable tabs-right">
                <div class="tab-content">
                    <div class="tab-pane active" id="tab2-1">
                        <g:form name="removeTopicForm" useToken="true" action="doRemoveTopic" autocomplete="off">
                            <g:hiddenField name="topicIdentifier" value="${topicIdentifier}" />
                            <fieldset>
                                <legend>Remove Topic</legend>

                                <p>Are you sure you want to delete ${topicName}?</p>

                                <br />
                                <br />
                                <g:link action="removeTopics" controller="topic"><input type="button" value="Cancel" class="btn btn-warning"/></g:link>  <g:submitButton name="submit" class="btn btn-danger" value="Submit" />
                            </fieldset>
                        </g:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>